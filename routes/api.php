<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function(){
	Route::post('/register', 'AuthController@register');
	Route::post('/verifyUsername', 'AuthController@verifyUsername');
	Route::post('/login', 'AuthController@login');
	Route::post('/loginSSO', 'AuthController@loginSSO');
	Route::post('/logout', 'AuthController@logout');
	Route::post('/changeForgotPassword', 'AuthController@forgotPassword');
	Route::post('/sendOTPReset', 'AuthController@sendOTPResetPassword');
	Route::post('/resendOTP', 'AuthController@sendRequestOTP');
	Route::post('/verifyOTP', 'AuthController@verifyOTP');
});

Route::group(['middleware' => 'check-token'], function(){
	Route::post('/auth/changePassword', 'AuthController@changePassword');

	Route::group(['prefix' => 'profile'], function(){
		Route::post('/updatePhoto', 'ProfileController@updatePhoto');
		Route::post('/deletePhoto', 'ProfileController@deletePhoto');
		Route::post('/updateIdentity', 'ProfileController@updateIdentity');
		
		Route::post('/addAddress', 'ProfileController@addAddress');
		Route::post('/updateAddress', 'ProfileController@updateAddress');
		Route::post('/deleteAddress', 'ProfileController@deleteAddress');
		Route::post('/setMainAddress', 'ProfileController@setMainAddress');
		Route::post('/getProfile', 'ProfileController@getProfile');
		Route::post('/updateProfile', 'ProfileController@updateProfile');
		Route::post('/updatePhone', 'ProfileController@updatePhoneNumber');
		Route::post('/getBalanceDetail','ProfileController@getBalanceDetail');
		Route::post('/updateProfileNew','ProfileController@updateProfileNew');
	});
	
});
Route::get('profile/address/{id}', 'ProfileController@getAddress');
Route::get('/profile/detailAddress/{id}', 'ProfileController@getDetailAddress');

Route::get('/category', 'CategoryProductController@getCategory');
Route::get('/getLocation', 'CategoryProductController@getLocation');
Route::post('/product/{id}', 'CategoryProductController@getProduct');
Route::post('/product2/{id}', 'CategoryProductController@getProduct2');
Route::post('/productWh/{id}', 'CategoryProductController@getProductWh');
Route::post('/productbyid/{id}', 'CategoryProductController@getProductById');
Route::post('/productbyName', 'CategoryProductController@getProductbyName');
Route::post('/productPriceType/{id}', 'CategoryProductController@getProductPriceType');
Route::post('/searchProduct', 'CategoryProductController@searchProduct');
Route::post('/searchProduct2', 'CategoryProductController@searchProduct2');
Route::post('/getVarian', 'CategoryProductController@getVarianProduct');
Route::post('/getKeyword', 'CategoryProductController@getProductKeyword');
Route::post('/deleteKeyword', 'CategoryProductController@deleteProductKeyword');

Route::get('/banner', 'BannerController@getBanner');
Route::get('/banner/{id}', 'BannerController@getBannerById');
Route::get('/shipmentMethod','ShipmentController@getShipmentMethod');
Route::post('/voucher','VoucherController@getVoucher');
Route::get('/paymentMethod','PaymentController@getPaymentMethod');


Route::post('/getPurchasedProduct','CartController@getPurchasedProduct');
Route::post('/doPayment','CartController@doPayment');
Route::post('/getPaymentStatus','CartController@getPaymentStatus');
Route::post('/checkout','CartController@checkout');
Route::post('/getVoucher','CartController@getVoucher');
Route::post('/getCart', 'CartController@getCart');
Route::post('/shoppingdetail', 'CartController@shoppingDetail');
Route::post('/addUpdateCart', 'CartController@addOrUpdateCart');
Route::post('/addToCart', 'CartController@addToCart');
Route::post('/deleteItemInCart', 'CartController@deleteItemFromCart');
Route::post('/getqtyproduct', 'CartController@getQtyProduct');
Route::post('/addCart', 'CartController@addCart');


// Route::get('/product', 'CategoryProductController@getAllProduct');
// Route::get('/product/{id}', 'ProductController@getDetailProduct');

Route::get('/provinsi', 'DistrictController@getProvinsi');
Route::get('/kabkot/{id}', 'DistrictController@getKabKot');
Route::get('/kecamatan/{id}', 'DistrictController@getKecamatan');
Route::get('/kelurahan/{id}', 'DistrictController@getKelurahan');


Route::post('/autoRegister', 'AuthController@autoRegister');
Route::post('/order', 'OrderController@addNewOrder');
Route::post('/confirmOrder', 'OrderController@confirmOrderStatus');
Route::post('/cancelOrder', 'OrderController@cancelOrder');
Route::get('/getOrder/{id}', 'OrderController@getOrder');
Route::post('/getParentOrderStatus','OrderController@getParentOrderStatus');
Route::post('/getTrackingHistory','OrderController@getTrackingHistory');
Route::post('/transaction','OrderController@getTransaction');
Route::get('/updatestock/{id}','OrderController@updateStock');


Route::post('/complain', 'ComplaintController@form');
Route::post('/solution', 'ComplaintController@solution');
Route::post('/complaint/category', 'ComplaintController@category');
Route::post('/complaintHistory', 'ComplaintController@complaintHistory');
Route::post('/addDiscuss', 'ComplaintController@addDiscuss');

Route::post('/notification', 'NotificationController@getNotification');
Route::post('/updateNotificationRead', 'NotificationController@updateNotification');


Route::post('/settings/global','GlobalSettingsController@get');

Route::get('/aboutus','AboutUsController@getAboutUs');

Route::get('/testrans/{id}', 'OrderController@getPaymentStatus');

Route::get('/tespay', 'TransactionController@getSnapToken');
Route::get('/deleteTokpedProduct/{id}', 'ProductEcommerceController@deleteProductTokped');
//integration with tokopedia -------------------------------
Route::get('/scanTokopediaStatus', 'IntegrationTokopediaController@enableScanTokopedia');
Route::get('/scanProduct', 'IntegrationTokopediaController@scanProduct');
Route::get('/changeImage', 'IntegrationTokopediaController@changeImage');
Route::get('/requestPickup/{id}', 'OrderTokopediaController@requestPickup');
Route::get('/pushStockEcommerce/{prodId}/{stock}', 'IntegrationECommerceController@pushStockEcommerce');
Route::get('/acceptOrderTokped/{orderId}', 'OrderTokopediaController@acceptOrder');
Route::get('/rejectOrderTokped/{orderId}', 'OrderTokopediaController@rejectOrder');
Route::get('/inactiveProduct/{id}', 'ProductEcommerceController@inactive');
Route::get('/activeProduct/{id}', 'ProductEcommerceController@active');

Route::get('/getHeader','HeaderController@getHeader');
Route::get('/getHelp','HelpController@getHelp');
Route::get('/getPrivacy','PrivacyController@getPrivacy');
Route::get('/getTerm','TermController@getTerm');

//search category level 2
Route::get('/getCategoryDua', 'CategoryProductController@getCategoryDua');
//search category level 3
Route::get('/getCategoryTiga', 'CategoryProductController@getCategoryTiga');