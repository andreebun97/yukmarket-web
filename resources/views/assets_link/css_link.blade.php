<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 
<link href="{{ asset('css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
@if($role == 'customer')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
@else
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
<!-- css style frontend ui -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/customresponsive.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/plugins/carousel/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/plugins/bootstrap-toggle/bootstrap-toggle.min.css') }}" rel="stylesheet" type="text/css">
@endif
@if(isset($page) && $page == 'dashboard')
<link href="{{ asset('js/chart.js/Chart.bundle.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/chart.js/Chart.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/chart.js/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endif

<!-- end global stylesheets -->