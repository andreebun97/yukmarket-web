<!-- Core JS files tes -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
@if(isset($role) && $role == 'customer')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
@endif

<!-- /core JS files -->

@if(isset($page) && $page == 'dashboard')
<script type="text/javascript" src="{{ asset('js/plugins/visualization/c3/c3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/visualization/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/c3-chart-custom.js') }}"></script>
<!-- chart -->
<script type="text/javascript" src="{{ asset('js/chart.js/Chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chart.js/Chart.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chart.js/Chart.min.js') }}"></script>
@endif
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="{{ asset('js/core/libraries/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/select2/select2.full.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script type="text/javascript" src="{{ asset('js/plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/ui/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/anytime.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/pickadate/picker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/pickadate/picker.date.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/pickadate/picker.time.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pickers/pickadate/legacy.js') }}"></script>
@if(Session::get('message_alert') != null)
<script>
    $('#notificationPopup').modal('show');  
</script>
@endif
@if(Session::get('redirect_to_edit') != null)
<script>
    $('#roleMenuAccess').modal('show');
</script>
@endif
@if(Session::get('error_promo') != null)
<script>
    $('#promoModalForm').modal('show');
</script>
@endif
@if(Session::get('error_role') != null)
<script>
    $('#roleMenuAccess').modal('show');
</script>
@endif
@if(Request::get('order_id') != null)
<script>
    $('#transactionDetailModal').modal('show');
    $('#confirmShipmentModalForm').modal('show');

    // $('#confirmShipmentModalForm').on('hidden.bs.modal', function(){
    //     window.location.href = "{{ route('admin/shipment/history') }}";
    // });
    $('#transactionDetailModal').on('hidden.bs.modal', function(){
        window.location.href = "{{ route('admin/yukmarket/user/customer','?id='.Request::get('id')) }}";
    })
</script>
@endif

@if(Session::get('password_alert') != null)
<script>
    $('#passwordPopup').modal('show');
</script>
@endif
<!--  Frontend library -->
<script type="text/javascript" src="{{ asset('js/plugins/carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/general-custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/notification-custom.js') }}"></script>