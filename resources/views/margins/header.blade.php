<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="{{ asset($favicon) }}" type="image/x-icon" />
        <link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" type="text/css"  href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" />
        @include('assets_link.css_link',['role' => isset($role) ? $role : ''])

        <title>@yield('title')</title>
    </head>
    <script>
        function numeric(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            return (k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 43);
        }
    </script>
    <body>
        @yield('content')
    
