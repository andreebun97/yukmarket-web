<!-- Modal -->
<div class="modal fade" id="cancelPopupModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog width-auto modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title" id="cancelPopupModalLabel"></h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ __('notification.cancel_question') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('notification.no') }}</button>
                <button type="button" class="btn btn-primary exit_modal_button" data-dismiss="modal">{{ __('notification.yes') }}</button>
            </div>
        </div>
    </div>
</div>