    <!-- Main navbar -->
	<div class="navbar navbar-default menu-clean navbar-fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ route('admin') }}"><img src="{{ asset($logo) }}" alt=""></a>
			<!-- <a class="navbar-brand brand-bawang" href="{{ route('admin') }}"><img src="{{ asset('img/logo-yukmarket-bawang-landscape.png') }}" alt=""></a> -->

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="ic-menu-user"></i></a></li>
				<li><a data-toggle="collapse" data-target="#sideBar" class="sidebar-mobile-main-toggle"><i class="ic-menu"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav hamburger-menu">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="ic-menu"></i></a></li>
			</ul>

			<!-- Search Form -->
			<!-- <form class="navbar-form navbar-left search-wrapper">
				<div class="form-group has-feedback-left">
					<input type="search" class="form-control search-form" placeholder="Search">
					<i class="icon-search4 text-muted text-size-base"></i>
				</div>
			</form> -->
			<!-- Search Form End -->

			<!-- <p class="navbar-text"><span class="label bg-success">Online</span></p> -->

			<ul class="nav navbar-nav navbar-right">
				<!-- <li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/flags/gb.png" class="position-left" alt="">
						English
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						<li><a class="deutsch"><img src="assets/images/flags/de.png" alt=""> Deutsch</a></li>
						<li><a class="ukrainian"><img src="assets/images/flags/ua.png" alt=""> Українська</a></li>
						<li><a class="english"><img src="assets/images/flags/gb.png" alt=""> English</a></li>
						<li><a class="espana"><img src="assets/images/flags/es.png" alt=""> España</a></li>
						<li><a class="russian"><img src="assets/images/flags/ru.png" alt=""> Русский</a></li>
					</ul>
				</li> -->

				<li class="dropdown">
					<!-- <a href="#" class="dropdown-toggle notif-btn" data-toggle="dropdown">
						<i class="ic-notif"><span class="badge bg-warning-400"> </span></i>
						<span class="visible-xs-inline-block position-right">Notifications</span>
						
					</a> -->
					
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							Notifications
							<!-- <ul class="icons-list">
								<li><a href="#"><i class="icon-compose"></i></a></li>
							</ul> -->
						</div>

						<ul class="media-list dropdown-content-body">
							<li class="media">
								<!-- <div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div> -->

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">James Alexander</span>
										<span class="media-annotation pull-right">04:58</span>
									</a>

									<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
								</div>
							</li>

							<li class="media">
								<!-- <div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">4</span>
								</div> -->

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Margo Baker</span>
										<span class="media-annotation pull-right">12:16</span>
									</a>

									<span class="text-muted">That was something he was unable to do because...</span>
								</div>
							</li>

							<li class="media">
								<!-- <div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div> -->
								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Jeremy Victorino</span>
										<span class="media-annotation pull-right">22:48</span>
									</a>

									<span class="text-muted">But that would be extremely strained and suspicious...</span>
								</div>
							</li>
						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>
				<li class="dropdown notif-bell">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="bellsInvisible()">
						<input type="hidden" id="temp" />
						<i class="fa fa-bell"></i>
						<span class="visible-xs-inline-block position-right">Notifications</span>
						<!-- span class="badge bg-warning-400">2</span -->
						<span id="bells" class="badge"></span>
					</a>
					
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							Notifications
							<!-- <ul class="icons-list">
								<li><a href="#"><i class="icon-compose"></i></a></li>
							</ul> -->
						</div>

						<ul class="media-list dropdown-content-body" id="listNotif">
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">12 Maret 2020 04:58 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li-->
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">31 Februari 2020 12:45 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li-->
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">02 Januari 2020 12:30 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li-->
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">02 Januari 2020 12:30 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li -->
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">02 Januari 2020 12:30 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li-->
							<!--li class="media">
								<div class="media-left">
									<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
									<span class="badge bg-danger-400 media-badge">5</span>
								</div>

								<div class="media-body">
									<a href="#" class="media-heading">
										<span class="text-semibold">Pesanan Selesai</span>
									</a>
									<div class="text-muted margin-b5">Pesanan sudah dikonformasi pembeli...</div>
									<div class="clearfix"></div>
									<span class="media-annotation">02 Januari 2020 12:30 WIB</span>
									<div class="clearfix"></div>
									<div class="border-dotted"></div>
								</div>
							</li-->
						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="" data-original-title="All messages"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle avatar-wrap" data-toggle="dropdown">
						<img src="{{ asset($user['profile_picture']) }}" class="rounded-circle mr-2 thumb-img" onError="this.onerror=null;this.src='{{ asset('img/img-avatar.svg') }}';" width="30" height="30" alt="">
						<!-- <span class="profile-thumb"></span> -->
						<span>{{ $user['fullname'] }}<br><small>{{ $user['warehouse_name'] == null ? __('page.all_warehouse') : $user['warehouse_name'] }}</small></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right w100">
						<li><a href="{{ route('admin/profile') }}"><i class="ic-view-profile"></i> {{ __('menubar.profile') }}</a></li>
						<li><a href="{{ route('admin/logout') }}"><i class="ic-logout"></i> {{ __('menubar.logout') }}</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->