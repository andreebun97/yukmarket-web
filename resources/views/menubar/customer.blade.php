<!-- navbar -->
<nav class="navbar navbar-light justify-content-between">
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3">
            <div class="row">
              <a href="{{ route('customer') }}" class="navbar-brand"><img class="logo" src="{{ asset('img/img-logo-yukmarket-landscape.svg') }}"></a>
            </div>
          </div>
          <form class="col-md-6 search-product">
            <div class="row">
              <div class="inner-addon left-addon">
                <i class="ic-search" aria-hidden="true"></i>
                <input class="form-control" type="search" placeholder="Cari barang" aria-label="Search">
              </div>
            </div>
          </form>
          @if(Session::get('users') != null)
          <div class="col-md-3">
            <div class="row btn-member">
              <div class="">
                <ul class="navbar-nav nav-notif">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="notif"></span><i class="ic-notif"></i></a>

                    <ul class="dropdown-menu notify-drop">
                      <div class="drop-content">
                        <li class="new">
                          <a href="detail-order.html">
                            <p class="notif-title">Pesanan sedang diproses #1588531350</p>
                            <p class="notif-status">Pesanan Anda sedang dalam pengemasan oleh penjual.</p>
                            <label class="text-muted notif-date">12 Maret 2020, 15.16 WIB</label>
                          </a>
                        </li>
                        <li class="new">
                          <a href="detail-order.html">
                            <p class="notif-title">Pesanan sedang diproses #1588531350</p>
                            <p class="notif-status">Pesanan Anda sedang dalam pengemasan oleh penjual.</p>
                            <label class="text-muted notif-date">12 Maret 2020, 15.16 WIB</label>
                          </a>
                        </li>
                        <li>
                          <a href="detail-order.html">
                            <p class="notif-title">Pesanan sedang diproses #1588531350</p>
                            <p class="notif-status">Pesanan Anda sedang dalam pengemasan oleh penjual.</p>
                            <label class="text-muted notif-date">12 Maret 2020, 15.16 WIB</label>
                          </a>
                        </li>
                        <li>
                          <a href="detail-order.html">
                            <p class="notif-title">Pesanan sedang diproses #1588531350</p>
                            <p class="notif-status">Pesanan Anda sedang dalam pengemasan oleh penjual.</p>
                            <label class="text-muted notif-date">12 Maret 2020, 15.16 WIB</label>
                          </a>
                        </li>
                        <li>
                          <a href="detail-order.html">
                            <p class="notif-title">Pesanan sedang diproses #1588531350</p>
                            <p class="notif-status">Pesanan Anda sedang dalam pengemasan oleh penjual.</p>
                            <label class="text-muted notif-date">12 Maret 2020, 15.16 WIB</label>
                          </a>
                        </li>
                        <div class="notify-drop-footer">
                          <a href=""><i class="fa fa-eye"></i> Sudah dibaca semua</a>
                          <a href="" class="right"><i class="fa fa-eye"></i> Lihat semua</a>
                        </div>
                      </div>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="">
                <ul class="navbar-nav nav-profil">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0px">
                      <img src="{{ asset('img/img-avatar.svg') }}" width="30" height="30" class="rounded-circle mr-2">
                      Budi Setiawan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="{{ route('customer/transaction') }}">Riwayat Transaksi</a>
                      <a class="dropdown-item mb-4" href="{{ route('customer') }}">Pengaturan</a>
                      <a class="dropdown-item" onclick="document.getElementById('logoutForm').submit()">Keluar
                        <form id="logoutForm" action="{{ route('customer/auth/logout') }}" method="POST">
                          @csrf
                          <input type="hidden" name="user_id" value="{{ Session::get('users')['id'] }}">
                        </form>
                      </a>
                    </div>
                  </li>   
                </ul>
              </div>
            </div>
          </div>
          @else
          <div class="col-md-3">
            <div class="row btn-member">
              <div class="col">
                <a href="{{ route('customer/auth/register') }}" class="btn btn-outline-warning btn-block">Register</a>
              </div>
              <div class="col">
                <a href="{{ route('customer/auth/login') }}" class="btn btn-warning btn-block">Login</a>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </nav>
  <!-- promo -->