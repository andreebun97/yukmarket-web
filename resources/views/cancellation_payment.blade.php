<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <title>Email!</title>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Roboto);
        body{
            background: #f1f7f2;
            color: #333333;
            padding-left:30px;
            padding-right:30px;
            font-family: 'Roboto', sans-serif;
            font-weight: lighter;
        }

        *{
            color: black;
            font-family: 'Roboto', sans-serif;
        }

        p{
            font-size:14px;
        }

        .label-table{
            font-weight: bold;
        }

        .deadline{
            color: #AD1D2D;
        }

        .customer_name_opening{
            font-size:22px;
        }

        .reminder_notification{
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .reminder_notification h4 {
            font-size: 18px;
            margin: auto;
        }
        table tr td{
            font-size:14px;
        }

        .reminder_notification p{
            color: #F44336;
            background: #f7e1e1;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }

        tr {
            padding: 0 10px;
        }

        td{
            vertical-align:baseline;
            padding: 0 10px;
        }

        .email-wrap {
            width: auto;
            max-width: 570px;
            background-color: #fff;
            margin: 20px auto;
            padding:0px;
            border: 1px solid #ddd;
            border-radius: 4px;
            text-align: center;
        }

        .copyright-wrap {
            width: auto;
            max-width: 570px;
            margin: 0 auto;
            padding: 0 20px;
            text-align: center;
        }



        .email-head > h2 {
            padding-bottom: 10px;
            margin-bottom: 0 !important;
        }

        .yukmarket-logo-email {
            padding: 20px;
            border-bottom: 1px solid #ddd;
        }

        .yukmarket-logo-email img {
            width: auto;
            /* height: 35px; */
            height: 120px;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        table.email-detail-order {
            text-align: left;

        }
        table.email-detail-order tr {
            border-bottom: 4px solid;
        }
        table tr td{
            padding: 5px 0px 0px 0px;
            margin: auto;
            line-height:19px;


        }
        table.email-detail-order tr td {
            border-top:1px dotted #ddd;
            padding:8px 0px 8px 0px;
            line-height:20px;
            font-weight:normal !important;
        }
        table.email-list-order {
            text-align: left;
            padding: 0px;
        }

        table.email-list-order td {
            border-top: 1px dotted #ddd;
            padding: 5px 10px;
            color: black ;
        }
        table.email-list-order td span {
            color: black ;
        }
        table.email-list-order td table {
            width:50%;
            float:right;
            margin:10px auto;
        }
        table.email-list-order td table tr td {
            background: #f5f5f5;
            padding: 5px 10px;
            font-weight:normal !important;
        }
        .email-total {
            font-weight: bold;
        }

        .email-price {
            text-align: right;

        }

        .email-voucher {
            color: #AD1D2D;
        }

        .font-bold {
            font-weight: bold !important;
        }

        .email-foot {

        }
        .email-foot p {
            padding:0px 20px ;
            color:#666
        }
        .email-foot p:first-child {
            text-align: left;
            font-size: 13px;
            margin-top: 0;
            padding: 0 10px;
        }

        .color-green {
            color: #00a00d !important;
        }

        .text-center {
            text-align: center;
        }

        .copyright-email {
            border: 0 none;
            border-collapse: separate;
            vertical-align: middle;
            font-family: " HelveticaNeue","HelveticaNeue","HelveticaNeueRoman","HelveticaNeue-Roman","HelveticaNeueRoman","HelveticaNeue-Regular","HelveticaNeueRegular",Helvetica,Arial,"LucidaGrande",sans-serif;
            font-weight: 400;
            color: #444;
            font-size: 12.5px;
            text-transform: lowercase;
        }

        /*separator*/


        @media screen and (max-width: 720px) {
          body .c-v84rpm {
            width: 100% !important;
            max-width: 720px !important;
            background:#eee
          }
          body .c-v84rpm .c-7bgiy1 .c-1c86scm {
            display: none !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-pekv9n .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-1c9o9ex .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-90qmnj .c-1qv5bbj {
            border-width: 1px 0 0 !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-183lp8j .c-1qv5bbj {
            border-width: 1px 0 !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-pekv9n .c-1qv5bbj {
            padding-left: 12px !important;
            padding-right: 12px !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-1c9o9ex .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-90qmnj .c-1qv5bbj {
            padding-left: 8px !important;
            padding-right: 8px !important;
          }
          body .c-v84rpm .c-ry4gth .c-1dhsbqv {
            display: none !important;
          }
        }


        @media screen and (max-width: 720px) {
          body .c-v84rpm .c-ry4gth .c-1vld4cz {
            padding-bottom: 10px !important;
          }
        }

        .padding-b10 {
            padding:0px 0px 10px 0px
        }
        .color-gray {
            color:#666
        }
        .color-green {
            color:#00a00d
        }
        .font-weight500 {
            font-weight:500 !important;
        }
        .font-bold {
            font-weight:bold;
        }
        .wrap {
            padding: 20px;
            border-top: 1px solid #ddd;
            /* border-bottom: 1px dotted #ddd; */
        }
        .wrap h4 {
            color: #333;
            margin: 0px 0px 20px 0px;
            font-size: 16px;
            font-weight:bold;
        }
        .copyright {
            background:#00a00d;
            padding:20px;
            color:#fff;
            text-align:center;
        }
        .copyright p {
            margin:auto;
            color:#fff;
            font-size:14px;
        }
        .clearfix {
            clear:both;
        }
        .no-border {
            border:none !important
        }
        .noted {
            padding:0px 20px;

        }
        .noted p {
            font-size: 12px;
            line-height: 22px;
            color:#333;

        }
        .color-red {
            color:red !important;
        }
        .color-black {
            color:#000!important;
        }
        .email-container {
            background-color:#f1f7f2;
            width:100%;
            padding:30px 0px
        }
        .alert_notification {
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .alert_notification p {
            color: #007582;
            background: #eafbfd;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }
        .btn-green {
            background-color:#00a00d;
            color:#fff;
            padding:5px 10px;
            border-radius:4px;
            font-size:14px;
            text-decoration:none;
        }


        .alert_notification h4 {
            font-size: 18px;
            margin: auto;
        }
        .success_notification {
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .success_notification p {
            color: #1469b3;
            background: #eafbfd;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }

        .this-item-none td{
            padding: 10px !important;
            border-top: none !important;
        }


        .success_notification h4 {
            font-size: 18px;
            margin: auto;
        }
        @media only screen and  (max-width:500px) {
            body {
                background:#fff;
            }
            body .success_notification p {
                padding: 10px 10px !important;
            }
            .success_notification {
                padding: 10px 10px 5px 10px !important;
            }
           table.email-list-order tr td table {
                width:100%;
            }
            table.email-detail-order tr td {
                width:50% !important;
            }
            .email-container {
                background-color: #fff;
            }
            .btn-green {
                display:inline;
                margin-top:10px
            }
        }
    </style>
</head>
<body>
    <div class="email-container">
        <div class="email-wrap">
            <div class="email-head">
                <div class="yukmarket-logo-email">
                <center><img src="{{ $logo }}" alt=""></center>
                </div>
            </div>
            <div class="email-body">

                <div class="reminder_notification">
                    <h4 class="color-green">Pesanan dibatalkan</h4>
                    <p class="color-gray">Hello, <u>{{ $payment['customer_name'] }}</u>! Pesanan dibatalkan karena melewati batas waktu pembayaran <a class="btn-green" href="#">Yuk beli lagi</a></p>
                </div>
                <div class="wrap ">
                    <h4 >Detail Transaksi</h4>
                    <table class="email-detail-order">
                        <tr>
                            <td width="30%" class="label-table  color-gray">Kode Pembayaran</td>
                            <td width="70%" class="  font-weight500">{{ $payment['invoice_no'] }}</td>
                        </tr>
                        <tr>
                            <td width="30%" class="label-table  color-gray">Tanggal Transaksi</td>
                            <td width="70%" class="  font-weight500">{{ $payment['created_date'] }}</td>
                        </tr>
                        <tr>

                            <td width="30%" class="label-table  color-gray">Metode Pembayaran</td>
                            <td width="70%" class="  font-weight500">{{ $payment['payment_method_name'] }}</td>
                        </tr>
                        <tr>

                        </tr>
                        <tr>
                            <td width="30%" class="label-table  color-gray">Status Pembayaran</td>
                            <td width="70%" class="  font-weight500">{{ $payment['status'] }}</td>
                        </tr>
                        <tr>
                            <td width="30%" class="label-table  color-gray">Alamat Pelanggan</td>

                            <td width="70%" class="  font-weight500">{{ $payment['address_detail'] . ', ' . $payment['kelurahan_desa_name'] . ', ' . $payment['kecamatan_name'] . ', ' . $payment['kabupaten_kota_name'] . ', ' . $payment['provinsi_name'] . ', ' . $payment['kode_pos'] . ' (' . $payment['address_name'] . ', '.($payment['contact_person'] == null ? $payment['customer_name'] : $payment['contact_person']).', '.($payment['phone_number'] == null ? $payment['customer_phone_number'] : $payment['phone_number']).')' }}</td>
                        </tr>
                    </table>
                </div>
                <div class="wrap ">
                    <h4>Detail Pesanan</h4>
                    @php $total_per_warehouse = 0; $total = 0; $shipment_price = 0; @endphp
                    <table  class="email-list-order">
                        @for($b = 0; $b < count($product_array); $b++)
                            <tr class="list-item">
                                <td style="font-weight:bold">{{ $product_array[$b]['warehouse_name'] }}</td>
                            </tr>
                            @php
                                $product_detail = $product_array[$b]['detail'];
                                $shipment_price += $product_array[$b]['shipment_price'];
                            @endphp
                            @for($c = 0; $c < count($product_detail); $c++)
                                @php
                                    $total_per_warehouse += ($product_detail[$c]['quantity'] * $product_detail[$c]['price']);
                                    $total += ($product_detail[$c]['quantity'] * $product_detail[$c]['price']);
                                @endphp
                                <tr class="list-item">
                                    <td>{{ $product_detail[$c]['prod_name'] . ($product_detail[$c]['sku_status'] == 0 ? '' : (' '.($product_detail[$c]['uom_value']+0).' '.$product_detail[$c]['uom_name'])) . ' '.$currency->convertToCurrency($product_detail[$c]['price']).'/1 pc'.' (Jumlah: '.$product_detail[$c]['quantity'].' Pcs)' }}</td>
                                    <td class="email-price">
                                        <span>{{ $currency->convertToCurrency($product_detail[$c]['quantity'] * $product_detail[$c]['price']) }}</span>
                                    </td>
                                </tr>
                            @endfor
                            <tr class="list-item" style="background:#e2dddd">
                                <td>Biaya Pengiriman</td>
                                <td class="email-price">{{ $currency->convertToCurrency($product_array[$b]['shipment_price']) }}</td>
                            </tr>
                            <tr class="list-item">
                                <td>{{ __('field.total') }}</td>
                                <td class="email-price">{{ $currency->convertToCurrency($total_per_warehouse) }}</td>
                            </tr>
                            <tr class="this-item-none">
                                <td></td>
                                <td></td>
                            </tr>
                            @php $total_per_warehouse = 0; @endphp
                        @endfor
                        <tr>
                            <td colspan="2">
                                <table width="50%">
                                    <tr>
                                        <td class="email-total no-border">Subtotal @if($payment['pricing_include_tax'] == 1) {{ '(+PPN)' }} @endif</td>
                                        <td class="email-price no-border "><span class="font-weight500 color-black">{{ $currency->convertToCurrency($total) }}</span></td>
                                    </tr>
                                        @php
                                            $tax = 0;
                                            $balance = 0;
                                            if($payment['pricing_include_tax'] == 0){
                                                $tax = ROUND($total * $payment['national_income_tax']);
                                            }

                                            if($payment['return_amount'] != null){
                                                $balance = $payment['return_amount'];
                                            }
                                        @endphp
                                        @if($payment['pricing_include_tax'] == 0)
                                        <tr>
                                            <td style="email-total">Pajak</td>
                                            <td class="email-price"><span class="font-weight500 color-black">{{ $currency->convertToCurrency($tax) }}</span></td>
                                        </tr>
                                        @endif
                                        @php
                                            $voucher_amount = 0;
                                            if($payment['voucher_id'] != null){
                                                $voucher_amount = $payment['voucher_amount'];
                                            }
                                        @endphp
                                        <tr>
                                            <td class="email-total">Voucher</td>
                                            <td class="email-price"><span class="email-voucher font-weight500 color-red">-{{ $voucher_amount == 0 ? "" : $currency->convertToCurrency($voucher_amount) }}</span></td>
                                        </tr>
                                        @php $total += ($tax - $voucher_amount); @endphp
                                        @php
                                            $admin_fee = 0;
                                            if($payment['admin_fee'] != null){
                                                $admin_fee = $payment['admin_fee'];
                                            }

                                            if($payment['admin_fee_percentage'] != null){
                                                $admin_fee = ROUND($total * $payment['admin_fee_percentage']);
                                            }
                                        @endphp
                                        <tr>
                                            <td class="email-total">Total Biaya Pengiriman</td>
                                            <td class="email-price"><span class="font-weight500 color-black">{{ $currency->convertToCurrency($shipment_price) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="email-total">Biaya Admin</td>
                                            <td class="email-price"><span class="font-weight500 color-black">{{ $currency->convertToCurrency($admin_fee) }}</span></td>
                                        </tr>
                                        @php $total += $shipment_price + $admin_fee - $balance; @endphp
                                        <tr>
                                            <td class="email-total">Saldo</td>
                                            <td class="email-price"><span class="font-weight500 color-red">-{{ $balance  == 0 ? '' : $currency->convertToCurrency($balance) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="email-total">Total</td>
                                            <td class="email-price"><span class="font-weight500 color-black">{{ $currency->convertToCurrency($total) }}</span></td>
                                        </tr>
                                </table>
                                <div class="clearfix"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="noted">
                    <p><span class="color-red">* </span> Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke alamat email ini</p>
                </div>
            </div>
            <div class="email-footer">
                <div class="copyright">
                    <p>© 2020 YukMarket. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
