<div class="row profile-login mb-4">
    <div class="col-md-4">
        <img class="rounded-circle img-responsive" alt="" src="{{ asset('img/img-avatar.svg') }}">
    </div>
    <div class="col-md-8">
        <p class="profile-name">Budi Setiawan</p>
        <a href="{{ route('customer/profile') }}" class="profile-link">Edit Profil</a>
    </div>
</div>
<div class="list-group">
    <a href="{{ route('customer/transaction') }}" class="list-group-item list-group-item-action{{ isset($menubar) && $menubar == 'transaction' ? ' active' : '' }}"><i class="ic-clock"></i> Riwayat Transaksi</a>
    <a href="{{ route('customer/profile/address') }}" class="list-group-item list-group-item-action{{ isset($menubar) && $menubar == 'shipment_address' ? ' active' : '' }}"><i class="ic-pin-user"></i> Alamat Pengiriman</a>
    <a href="{{ route('customer/password') }}" class="list-group-item list-group-item-action"><i class="ic-lock-open"></i> Atur Ulang Sandi</a>
</div>