

<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default menu-clean sidebar-fixed" id="sideBar">
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion custom-accordion">
                    @for($b = 0; $b < count($all_menu); $b++)
                        @if($all_menu[$b]['access_status'] == 1 || $all_menu[$b]['total_active_children'] > 0)
                        <li class="@if($all_menu[$b]['menu_url'] == null) @endif  @if(isset($sidebar) && $sidebar == $all_menu[$b]['menu_label'])  active  @endif">
                        @if($all_menu[$b]['parent_id'] == null)
                            <a {{ $all_menu[$b]['menu_url'] != null ? "href=".route($all_menu[$b]['menu_url'])."" : " href=#".$all_menu[$b]['menu_id'] }} class="@if($all_menu[$b]['menu_url'] == null) has-ul @endif"><i class="{{ $all_menu[$b]['menu_icon'] }}"></i> <span>{{ __('menubar.'.$all_menu[$b]['menu_label']) }}</span></a>
                            @if($all_menu[$b]['menu_url'] == null && $all_menu[$b]['total_active_children'] > 0)
                            <ul id="{{ $all_menu[$b]['menu_id'] }}" class="hidden-ul @if($all_menu[$b]['menu_url'] == null) @endif  @if(isset($sidebar) && $sidebar == $all_menu[$b]['menu_label'])  active  @endif"" >
                                @for($a = 0; $a < count($all_menu); $a++)
                                    @if($all_menu[$a]['parent_id'] == $all_menu[$b]['menu_id'] && $all_menu[$a]['parent_id'] !== null && $all_menu[$a]['access_status'] == 1)
                                        <li ><a href="{{ route($all_menu[$a]['menu_url']) }}" ><i class="{{ $all_menu[$a]['menu_icon'] }}"></i> <span>{{ __('menubar.'.$all_menu[$a]['menu_label']) }}</span></a></li>
                                    @endif
                                @endfor
                            </ul>
                            @endif
                        </li>
                        @endif
                        @endif
                    @endfor
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->