<!-- Modal -->
<div class="modal fade" id="notificationPopup" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            @php
                $tokopedia_error_list = Session::get('tokopedia_error_list')
            @endphp
            <div class="modal-header  {{ ($errors->any() || (isset($tokopedia_error_list) && count($tokopedia_error_list) > 0) || ($tokopedia_error_list != null && count($tokopedia_error_list) > 0)) ? ' error' : 'success' }} ">
                <h5 class="modal-title" id="notificationPopupLabel">{{ ($errors->any() || (isset($tokopedia_error_list) && count($tokopedia_error_list) > 0) || ($tokopedia_error_list != null && count($tokopedia_error_list) > 0)) ? __('popup.error') : __('popup.success') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-custom-text">
                <p>{{ Session::get('message_alert') }}</p>
            </div>
        </div>
    </div>
</div>