<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <title>Email!</title>
    <style>
         @import url(http://fonts.googleapis.com/css?family=Roboto);
        body{
            background: #f1f7f2;
            color: #333333;
            padding-left:30px;
            padding-right:30px;
            font-family: 'Roboto', sans-serif;
            font-weight: lighter;
        }

        *{
            color: black;
            font-family: 'Roboto', sans-serif;
        }

        p{
            font-size:14px;
        }

        .label-table{
            font-weight: bold;
        }

        .deadline{
            color: #AD1D2D;
        }

        .customer_name_opening{
            font-size:22px;
        }

        .reminder_notification{
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .reminder_notification h4 {
            font-size: 18px;
            margin: auto;
        }
        table tr td{
            font-size:14px;
        }

        .reminder_notification p{
            color: #F44336;
            background: #f7e1e1;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }

        tr {
            padding: 0 10px;
        }

        td{
            vertical-align:baseline;
            padding: 0 10px;
        }

        .email-wrap {
            width: auto;
            max-width: 570px;
            background-color: #fff;
            margin: 20px auto;
            padding:0px;
            border: 1px solid #ddd;
            border-radius: 4px;
            text-align: center;
        }

        .copyright-wrap {
            width: auto;
            max-width: 570px;
            margin: 0 auto;
            padding: 0 20px;
            text-align: center;
        }

        .separator-email {
            max-width: 570px;
        }

        .email-head > h2 {
            padding-bottom: 10px;
            margin-bottom: 0 !important;
        }

        .yukmarket-logo-email {
            padding: 20px;
            border-bottom: 1px solid #ddd;
        }

        .yukmarket-logo-email img {
            width: auto;
            height: 120px;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        table.email-detail-order {
            text-align: left;

        }
        table.email-detail-order tr {
            border-bottom: 4px solid;
        }
        table tr td{
            padding: 5px 0px 0px 0px;
            margin: auto;
            line-height:19px;


        }
        table.email-detail-order tr td {
            border-top:1px dotted #ddd;
            padding:8px 0px 8px 0px;
            line-height:20px;
            font-weight:normal !important;
        }
        table.email-list-order {
            text-align: left;
            padding: 0px;
        }

        table.email-list-order td {
            border-top: 1px dotted #ddd;
            padding: 5px 0px;
            color:#666 ;
        }
        table.email-list-order td span {
            color:#666 ;
        }
        table.email-list-order td table {
            width:50%;
            float:right;
            margin:10px auto;
        }
        table.email-list-order td table tr td {
            background: #f5f5f5;
            padding: 5px 10px;
            font-weight:normal !important;
        }
        .email-total {
            font-weight: bold;
        }

        .email-price {
            text-align: right;

        }

        .email-voucher {
            color: #AD1D2D;
        }

        .font-bold {
            font-weight: bold !important;
        }

        .email-foot {

        }
        .email-foot p {
            padding:0px 20px ;
            color:#666
        }
        .email-foot p:first-child {
            text-align: left;
            font-size: 13px;
            margin-top: 0;
            padding: 0 10px;
        }

        .color-green {
            color: #00a00d !important;
        }

        .text-center {
            text-align: center;
        }

        .copyright-email {
            border: 0 none;
            border-collapse: separate;
            vertical-align: middle;
            font-family: " HelveticaNeue","HelveticaNeue","HelveticaNeueRoman","HelveticaNeue-Roman","HelveticaNeueRoman","HelveticaNeue-Regular","HelveticaNeueRegular",Helvetica,Arial,"LucidaGrande",sans-serif;
            font-weight: 400;
            color: #444;
            font-size: 12.5px;
            text-transform: lowercase;
        }

        /*separator*/


        @media screen and (max-width: 720px) {
          body .c-v84rpm {
            width: 100% !important;
            max-width: 720px !important;
            background:#eee
          }
          body .c-v84rpm .c-7bgiy1 .c-1c86scm {
            display: none !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-pekv9n .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-1c9o9ex .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-90qmnj .c-1qv5bbj {
            border-width: 1px 0 0 !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-183lp8j .c-1qv5bbj {
            border-width: 1px 0 !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-pekv9n .c-1qv5bbj {
            padding-left: 12px !important;
            padding-right: 12px !important;
          }
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-1c9o9ex .c-1qv5bbj,
          body .c-v84rpm .c-7bgiy1 .c-f1bud4 .c-90qmnj .c-1qv5bbj {
            padding-left: 8px !important;
            padding-right: 8px !important;
          }
          body .c-v84rpm .c-ry4gth .c-1dhsbqv {
            display: none !important;
          }
        }


        @media screen and (max-width: 720px) {
          body .c-v84rpm .c-ry4gth .c-1vld4cz {
            padding-bottom: 10px !important;
          }
        }

        .padding-b10 {
            padding:0px 0px 10px 0px
        }
        .color-gray {
            color:#666
        }
        .color-green {
            color:#00A00C
        }
        .font-weight500 {
            font-weight:500 !important;
        }
        .font-bold {
            font-weight:bold;
        }
        .wrap {
            padding: 20px;
            border-top: 1px solid #ddd;
            /* border-bottom: 1px dotted #ddd; */
        }
        .wrap h4 {
            color: #333;
            margin: 0px 0px 20px 0px;
            font-size: 16px;
            font-weight:bold;
        }
        .copyright {
            background:#00A00C;
            padding:20px;
            color:#fff;
            text-align:center;
        }
        .copyright p {
            margin:auto;
            color:#fff;
            font-size:14px;
        }
        .clearfix {
            clear:both;
        }
        .no-border {
            border:none !important
        }
        .noted {
            padding:0px 20px;

        }
        .noted p {
            font-size: 12px;
            line-height: 22px;
            color:#333;

        }
        .color-red {
            color:red !important;
        }
        .color-black {
            color:#000!important;
        }
        .email-container {
            background-color:#f1f7f2;
            width:100%;
            padding:30px 0px
        }
        .attention_notification {
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .attention_notification p {
            color: #ff8901;
            background: #fbf9e8;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }
        .attention_notification h4 {
            font-size: 18px;
            margin:auto
        }
        .success_notification {
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 7px;
            border-radius: 4px;
            padding: 10px 20px 5px 20px;
        }
        .success_notification p {
            color: #1469b3;
            background: #eafbfd;
            padding: 10px;
            border-radius: 3px;
            font-size: 14px;
            line-height: 20px;
        }


        .success_notification h4 {
            font-size: 18px;
            margin: auto;
        }
        .btn-orange {
            background-color:orange;
            color:#fff;
            padding:8px 10px;
            border-radius:4px;
        }
        .btn-green {
            background-color:#00A00C;
            color:#fff;
            padding:5px 10px;
            border-radius:4px;
            font-size:14px;
            text-decoration:none;
        }
        a.btn-green {
            color:#fff;
        }
        @media only screen and  (max-width:500px) {
            body {
                background:#fff;
            }
            .success_notification {
                padding: 10px 10px 5px 10px !important;
            }
            body .success_notification p {
                padding: 10px 10px !important;
            }
            .success_notification {
                padding: 10px 10px 5px 10px !important;
            }
            table.email-list-order tr td table {
                width:100%;
            }
            table.email-detail-order tr td {
                width:50% !important;
            }
            .email-container {
                background-color: #fff;
            }
            .btn-green {
                display: block;
                margin-top:10px
            }
        }
    </style>
</head>
<body>



<div class="email-container">
        <div class="email-wrap">
            <div class="email-head">
                <div class="yukmarket-logo-email">
                    <center><img src="{{ $logo }}" alt=""></center>
                </div>
            </div>
            <div class="success_notification">
                    <h4 class="color-green">Pemrosesan Keluhan</h4>
                    <p class="color-gray">Hello, <u>{{ $complaint['customer_name'] }}</u>! Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak <u>{{ $complaint['customer_name'] }}</u>, kami ingin menginformasikan bahwa keluhan Anda sedang diproses oleh pihak YukMarket! Mohon menunggu hingga kabar berikutnya.</p>

                </div>
            <div class="wrap">
                <h4 >Detail Transaksi</h4>
                <table class="email-detail-order">
                    <tr>
                        <td  width="30%" class="label-table  color-gray">Kode Pesanan</td>
                        <td width="70%" class="  font-weight500">{{ $complaint['order_code'] }}</td>

                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Tanggal Transaksi</td>
                        <td width="70%" class="  font-weight500">{{ $complaint['order_date'] }}</td>

                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Tanggal Keluhan</td>
                        <td width="70%" class="  font-weight500">{{ $complaint['issue_date'] }}</td>

                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Metode Pembayaran</td>
                        <td width="70%" class="  font-weight500">{{ $complaint['payment_method_name'] }}</td>
                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Alamat Pelanggan</td>
                        <td width="70%" class="  font-weight500">{{ $complaint['address_detail'] . ', ' . ucwords(strtolower($complaint['kelurahan_desa_name'])) . ', ' . ucwords(strtolower($complaint['kecamatan_name'])) . ', ' . ucwords(strtolower($complaint['kabupaten_kota_name'])) . ', ' . ucwords(strtolower($complaint['provinsi_name'])) . ', ' . $complaint['kode_pos'] . ' (' . $complaint['address_name'] . ', '.($complaint['contact_person'] == null ? $complaint['customer_name'] : $complaint['contact_person']).', '.($complaint['phone_number'] == null ? $complaint['customer_phone_number'] : $complaint['phone_number']).')' }}</td>
                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Dibeli di</td>
                        <td width="70%" class="font-weight500">{{ $complaint['warehouse_name'] }}</td>
                    </tr>
                    <tr>
                        <td width="30%" class="label-table  color-gray">Solusi yang diajukan</td>
                        <td width="70%" class="font-weight500">{{ $complaint['issue_solution_name'] }}</td>
                    </tr>
                </table>
            </div>
            <div class="wrap">
                <h4>Detail Barang Yang Dikeluhkan</h4>
                @php
                    $total = 0;
                    $total_complained_weight = 0;
                    $total_complained_price = 0;
                @endphp
                <table  class="email-list-order">
                @for($b = 0; $b < count($complained_products); $b++)
                    <tr class="list-item">
                        <td >
                            @php
                                $total += ($complained_products[$b]['purchased_quantity'] * $complained_products[$b]['price']);
                                $total_complained_price += ($complained_products[$b]['complained_quantity'] * $complained_products[$b]['price']);
                                $total_complained_weight += ($complained_products[$b]['complained_quantity'] * $complained_products[$b]['uom_value']);
                            @endphp
                            <span>{{ $complained_products[$b]['prod_name']. ($complained_products[$b]['sku_status'] == 0 ? '' : (' '.($complained_products[$b]['uom_value']+0).' '.$complained_products[$b]['uom_name'])).' ('.$complained_products[$b]['complained_quantity'].' Pcs)' . ' x '.$currency->convertToCurrency($complained_products[$b]['price']) }}</span>
                        </td>
                        <td class="email-price">
                            <span>{{ $currency->convertToCurrency($complained_products[$b]['complained_quantity'] * $complained_products[$b]['price']) }}</span>
                        </td>
                    </tr>
                    <tr class="list-item">
                        <td><span>Kategori Keluhan: {{ $complained_products[$b]['issue_category_name'] }}</span></td>
                    </tr>
                    <tr class="list-item">
                        <td><span>Detail Keluhan: {{ ($complained_products[$b]['issue_detail_notes'] == null ? '-' : $complained_products[$b]['issue_detail_notes']) }}</span></td>
                    </tr>
                    @endfor
                    <tr>
                        <td colspan="2">
                            <table width="50%">
                                @if($complaint['issue_solution_id'] == 2)
                                <tr>
                                    <td style="email-total">Jumlah yang dikeluhkan</td>
                                    <td class="email-price"><span class="font-weight500 color-black">{{ $currency->convertToCurrency($total_complained_price) }}</span></td>
                                </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="noted">
                <p><span class="color-red">* </span> Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke alamat email ini</p>
            </div>
            <div class="email-footer">
                <div class="copyright">
                    <p>© 2020 YukMarket. All rights reserved.</p>
                </div>
            </div>
        </div>
</body>
</html>
