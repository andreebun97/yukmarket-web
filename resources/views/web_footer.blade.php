    @isset($auth)
    <!-- footer -->
    <footer class="section-footer mt-3">
        <div class="container">
            <div class="row border-top py-3">
                <a class="navbar-brand"><img class="logo" src="{{ asset('img/img-logo-yukmarket-landscape.svg') }}" ></a>
                <nav class="nav mr-md-auto">
                    <a class="nav-link active" href="#">Tentang YukMarket</a>
                    <a class="nav-link" href="#">Kontak</a>
                    <a class="nav-link" href="#">Bantuan</a>
                </nav>
                <a href="#"><img class="img-responsive" src="{{ asset('img/download-google-play.png') }}"></a>
                <a href="#"><img class="img-responsive" src="{{ asset('img/download-app-store.png') }}"></a>
            </div>
            <div class="row border-top pt-3">
                <p class="col-md-12 text-center">© 2020 YukMarket. All right reserved.</p>
            </div>
        </div><!-- //container -->
    </footer>
    @else
    <!-- footer -->
    <footer class="section-footer mt-5">
        <div class="container">
            <div class="row pt-3">
                <p class="col-md-12 text-center">© 2020 YukMarket. All right reserved.</p>
            </div>
        </div><!-- //container -->
    </footer>
    @endif