<div class="prohibited_page_div">
    <div class="prohibited_page_wrap">
        <img src="{{asset('img/img-access.png') }}">
        <h6>{{ __('notification.unavailable_feature') }}</h6>
    </div>
</div>