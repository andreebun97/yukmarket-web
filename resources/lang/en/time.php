<?php

    return [
        'yesterday' => 'Yesterday',
        'today' => 'Today',
        'last_week' => 'Last Week',
        'last_month' => 'Last Month',
        'this_month' => 'This Month',
        'this_week' => 'This Week'
    ]
?>