<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Kata Sandimu telah diperbarui!',
    'sent' => 'Kami telah mengirim link perbarui kata sandi ke email Anda',
    'throttled' => 'Mohon menunggu sebelum mencoba lagi',
    'token' => 'Token perbarui kata sandi tidak sesuai',
    'user' => "Kita tidak dapat menemukan pengguna dengan alamat email yang dimasukkan",

];
