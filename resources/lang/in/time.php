<?php

    return [
        'yesterday' => 'Kemarin',
        'today' => 'Hari Ini',
        'last_week' => 'Minggu Lalu',
        'last_month' => 'Bulan Lalu',
        'this_month' => 'Bulan Ini',
        'this_week' => 'Minggu Ini'
    ]
?>