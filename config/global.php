<?php 
return [
	'url_image' => 'http://34.87.29.119:8080/',
	'middleware_endpoint' => 'http://35.198.248.188:8280/api/mobile',
	'middleware_endpoint_staging' => 'http://35.198.248.188:8280/api/mobilestaging',	
	'middleware_endpoint_live' => 'http://35.198.248.188:8280/api/mobilelive',
	'tokopedia_integrator_url' => 'https://api.ecomm.inergi.id/tokopedia',
	'integrator_url_token'=>'https://api.ecomm.inergi.id/api/user',
	'ecommerce'=>[
		'TOKOPEDIA'
	],	
];
 ?>