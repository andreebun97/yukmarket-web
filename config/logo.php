<?php

    return [
        'default' => env('DEFAULT_COMPANY_LOGO'),
        'email' => env('COMPANY_LOGO_IN_EMAIL'),
        'auth' => env('COMPANY_LOGO_IN_AUTH'),
        'menubar' => env('COMPANY_LOGO_IN_MENUBAR'),
        'favicon' => env('FAVICON')
    ];
?>