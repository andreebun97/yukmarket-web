<?php

    return [
        'token' => env('FCM_AUTH'),
        'url' => env('FCM_URL')
    ];
?>