<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashback extends Model
{
    protected $table = '10_cashback';
    
    protected $primaryKey = 'cashback_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_id', 'order_id', 'customer_id', 'agent_id', 'claimed_amount', 'return_amount', 'transaction_flag', 'active_flag', 'created_by', 'updated_by'
    ];
}
