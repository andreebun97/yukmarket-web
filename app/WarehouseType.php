<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseType extends Model
{
    protected $table = '00_warehouse_type';
    
    protected $primaryKey = 'warehouse_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_name', 'active_flag', 'created_by', 'updated_by'
    ];
}
