<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    // use Notifiable;

    protected $table = '00_vouchers';
    
    protected $primaryKey = 'voucher_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'voucher_code', 'voucher_name', 'voucher_count_uses', 'max_uses', 'max_uses_user', 'voucher_type_id', 'amount', 'minimum_transaction_req', 'starts_at', 'expires_at', 'is_fixed', 'voucher_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
