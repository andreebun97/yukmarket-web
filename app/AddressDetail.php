<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressDetail extends Model
{
    protected $table = '00_address';
    
    protected $primaryKey = 'address_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'user_id', 'address_name', 'kelurahan_desa_id', 'kecamatan_id', 'kabupaten_kota_id', 'provinsi_id', 'address_detail', 'contact_person', 'phone_number', 'gps_point', 'isMain', 'address_info', 'banner_desc', 'created_by', 'updated_by'
    ];
}