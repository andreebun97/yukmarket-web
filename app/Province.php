<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = '00_provinsi';
    
    protected $primaryKey = 'provinsi_id';

    protected $fillable = [
        'provinsi_name', 'active_flag', 'created_by', 'updated_by'
    ];
}
