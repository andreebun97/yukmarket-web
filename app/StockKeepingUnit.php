<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockKeepingUnit extends Model
{
    protected $table = '00_product_sku';
    
    protected $primaryKey = 'product_sku_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_sku_name', 'product_sku_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
