<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preparation extends Model
{
    protected $table = '10_preparation';
    
    protected $primaryKey = 'preparation_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preparation_status_id', 'order_id', 'operator_name', 'operator_phone_no', 'start_preparation_date', 'end_preparation_date', 'preparation_notes', 'preparation_attachment', 'preparation_label', 'active_flag', 'created_by', 'updated_by'
    ];
}
