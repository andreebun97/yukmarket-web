<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GlobalSettingsController extends Controller
{
    public function __construct(){
        $this->middleware('check-token');
    }

    //USING MIDDLEWARE ENDPOINT -------------------------------------------------------------------------------------
    public function get(Request $request){
        
        $data = DB::table('99_global_parameter')->whereIn('global_parameter_name',['pricing_include_tax','tax_value'])->get();      
        $pricing_include_tax = true;
        $tax_value = 0;
        if(count($data) > 0){
            $pricing_include_tax = $data[0]->global_parameter_value == 0 ? false : true;
            $tax_value = ($data[1]->global_parameter_value == "" || $data[1]->global_parameter_value == null) ? 0 : $data[1]->global_parameter_value;
        }
        $tax_value = number_format($tax_value,2);
        $response = array(
            "pricing_include_tax" => $pricing_include_tax,
            "national_income_tax" => $tax_value
        );

        return response()->json(['isSuccess' => true, 'message' => 'OK!', 'data' => $response], 200);
    }
    public function getShopName(){
        $shop=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokped_shop')
        ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
        ->first();
        return $shop;
    }

    //ORIGINAL -----------------------------------------------------------------------------------------------------------
    // public function get(Request $request){
        
    //     $data = DB::table('99_global_parameter')->whereIn('global_parameter_name',['pricing_include_tax','tax_value'])->get();      
    //     $pricing_include_tax = true;
    //     $tax_value = 0;
    //     if(count($data) > 0){
    //         $pricing_include_tax = $data[0]->global_parameter_value == 0 ? false : true;
    //         $tax_value = ($data[1]->global_parameter_value == "" || $data[1]->global_parameter_value == null) ? 0 : $data[1]->global_parameter_value;
    //     }
    //     $tax_value = number_format($tax_value,2);
    //     $response = array(
    //         "pricing_include_tax" => $pricing_include_tax,
    //         "national_income_tax" => $tax_value
    //     );

    //     return response()->json(['isSuccess' => true, 'message' => 'OK!', 'data' => $response], 200);
    // }
    public function getGlobalSetting($name){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$name)
        ->first();
    }
}
