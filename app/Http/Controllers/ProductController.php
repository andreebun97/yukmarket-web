<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	//-----INTEGRATION TOKOPEDIA START
    public function getAllProductList(){
    	$products = DB::table('00_product')
    	->select('prod_name')
    	->where('active_flag',1)
    	->get();
    	return $products;
    }
    public function checkProduct($id){
    	$compare = DB::table('00_product')
    	->select('prod_id','prod_name','prod_desc','product_sku_id','prod_price','uom_value','uom_id','stockable_flag','parent_prod_id')
    	->where('prod_id',$id)
    	->first();
		return $compare;    	
    }
    public function addProduct($request){    	
    	return DB::table('00_product')
    	->insertGetId($request);
    }
    public function updateProduct($request){        
        DB::table('00_product')
        ->where('prod_id',$request['prod_id'])
        ->update($request);
    }
    public function updateProductSKUOnly($prodId,$sku){
        DB::table('00_product')
        ->where('prod_id',$prodId)
        ->update(['product_sku_id'=>$sku]);
    }
    public function checkProductSKU($name){
    	$check = DB::table('00_product_sku')
    	->select('product_sku_id','product_sku_name')
    	->where('product_sku_name',$name)
    	->first();
    	return $check;
    }
    public function addProductSKU($name){
    	$id = DB::table('00_product_sku')->insertGetId([
    		'product_sku_name'=>$name,
    		'product_sku_desc'=>'Ecommerce'
    	]);    	
    	return $id;    	
    }
    public function updateProductSKU($id,$name){
        DB::table('00_product_sku')
        ->where('product_sku_id',$id)
        ->update('product_sku_name',$name);
    }    
    public function checkProductPrice($id){
        return DB::table('00_product_ecommerce')
        ->join('00_product','00_product.prod_id','=','00_product_ecommerce.prod_id')
        ->select('00_product.prod_price','00_product.prod_id')
        ->where('00_product_ecommerce.prod_ecom_id',$id)
        ->first();
    }
    public function convertBookingStock($parentUomValue,$parentUomId,$childUomValue,$childUomId,$qty){
        if($parentUomId==1){
            $parentUom=$parentUomValue*1000;
        }else{
            $parentUom=$parentUomValue;
        }
        if($childUomId==1){
            $childUom=$childUomValue*1000*$qty;
        }else{
            $childUom=$childUomValue*$qty;
        }
        $qty=$childUom/$parentUom;
        return $qty;
        // $qty=ROUND($childUom/$parentUom);
        // return $qty < 1 ? 1 : $qty;
    }
    //-----INTEGRATION TOKOPEDIA END
}
