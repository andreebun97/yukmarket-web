<?php

namespace App\Http\Controllers;

use App\AuthComp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class DistrictController extends Controller
{
    public function getResponse($data){
        if($data == null){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK','data' => $data], 200);                      
        }
    }
    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function getProvinsi()
    {
        $q = DB::table('00_provinsi')
            ->select('provinsi_id', 'provinsi_name')
            ->where('active_flag', '1')
            ->whereRaw('provinsi_id')
            ->get();
        return $this->getResponse($q);                
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function getKabKot($id)
    {            
        $q = DB::table('00_kabupaten_kota')
        ->select('kabupaten_kota_id', 'kabupaten_kota_name')
        ->where('provinsi_id', $id)
        ->where('active_flag', '1')
        ->whereRaw('kabupaten_kota_id NOT IN (select kk.kabupaten_kota_id from 00_kabupaten_kota kk JOIN 00_kabupaten_kota_ecommerce kke ON kk.kabupaten_kota_id=kke.kabupaten_kota_id where kk.active_flag = 1)')
        ->get();
       
        return $this->getResponse($q);  
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function getKabKot($id)
    // {
        //     $url = "http://35.198.248.188:8280/api/mobile/kabkot";
        //     $body = array(
        //             "provinsi_id" => $id
        //         );
        //     $request = Http::post($url, $body);
        //     $response = json_decode($request->getBody()->getContents(), true);
                    
        //     // dd($response);
        //     // $q = DB::table('00_kabupaten_kota')
        //     // ->select('kabupaten_kota_id', 'kabupaten_kota_name')
        //     // ->where([
        //  //        ['provinsi_id', $id],
        //  //        ['active_flag', 1]
        //  //    ])
        //     // ->get();
        //     $data=array();
        //     if(!$response){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>$data], 500);
        //     }else{
        //         $validator = Validator::make($response, [
        //             'data' => 'required',            
        //         ]);
        //         if ($validator->fails()) {
        //           return response()->json(['isSuccess' => false, 'message' => 'Data not found','data' => $data], 500);  
        //         }else{
        //           return response()->json($response, 200);  
        //         }
                
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function getKecamatan($id)
    {
            $q = DB::table('00_kecamatan')
            ->select('kecamatan_id', 'kecamatan_name')
            ->where('kabupaten_kota_id', $id)
            ->where('active_flag', '1')
            ->get();

        return $this->getResponse($q);
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function getKecamatan($id)
    // {
        //     // $q = DB::table('00_kecamatan')
        //     // ->select('kecamatan_id', 'kecamatan_name')
        //     // ->where([
        //  //        ['kabupaten_kota_id', $id],
        //  //        ['active_flag', 1]
        //  //    ])
        //     // ->get();
        //     $url = "http://35.198.248.188:8280/ims/mobile/kecamatan";
        //     $body = array(
        //             "kabupaten_kota_id" => $id
        //         );
        //     $request = Http::post($url, $body);
        //     $response = json_decode($request->getBody()->getContents(), true);

        //     if(!$response){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        //     }else{
        //         return response()->json($response, 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function getKelurahan($id)
    {
        $q = DB::table('00_kelurahan_desa')
        ->select('kelurahan_desa_id', 'kelurahan_desa_name', 'kode_pos')
        ->where('kecamatan_id', $id)
        ->where('active_flag', '1')
        ->get();

        return $this->getResponse($q);
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function getKelurahan($id)
    // {
        //     // $q = DB::table('00_kelurahan_desa')
        //     // ->select('kelurahan_desa_id', 'kelurahan_desa_name', 'kode_pos')
        //     // ->where([
        //  //        ['kecamatan_id', $id],
        //  //        ['active_flag', 1]
        //  //    ])
        //     // ->get();

        //     $url = "http://35.198.248.188:8280/ims/mobile/kelurahan";
        //     $body = array(
        //             "kecamatan_id" => $id
        //         );
        //     $request = Http::post($url, $body);
        //     $response = json_decode($request->getBody()->getContents(), true);

        //     if(!$response){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        //     }else{
        //         return response()->json($response, 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------
}
