<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Console\Commands\IntegrationECommerceCommand;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Events\GenerateRelationProductTokped;
use App\Events\GenerateCategoryEvent;
use Carbon\Carbon;
use App\Order;
use App\OrderDetail;
use App\Payment;
use App\Inventory;
use App\StockCard;
use App\Preparation;
use App\Warehouse;
use App\Customer;
use App\Province;
use App\AddressDetail;
use App\Http\Controllers\OrderTokopediaController;
use GuzzleHttp\Client;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;

class IntegrationECommerceController extends Controller
{    
    public $tokpedConfig;
	public function sync($ecommerce){         
		//-------------------call event untuk sync relation product					        
            // get shop ID
        echo "in";
        $tokpedID=$this->getShopIdTokped();
        // $shopList =$this->getShopList();                
        if($tokpedID=='timeout'){
            return 'timeout';
        }
         
        $page=1;
        do {            
            //-------------------------sync master product  

            $scanProduct =$this->scanProduct($page,$tokpedID->organization_code);
            $page++;
        } while ( $scanProduct!='end');             
    }
    public function getCategoryList(){//----------------------------------------get daftar category tokopedia			$timeOutCounter=0;
    	$url = config('global.tokopedia_integrator_url').'/getCategories';    	
    	try {
            return Http::withToken(IntegrationECommerceCommand::$token)->timeout(3)->retry(2, 100)->get($url)->json();    
        } catch (ConnectionException $e) {
            return null;
        }    	    	
	}
    public function getShopList(){//---------------------------------------------get daftar shop tokopedia	
        $url = config('global.tokopedia_integrator_url').'/getAllShop';            
        try {
            return Http::withToken(IntegrationECommerceCommand::$token)->timeout(5)->retry(3, 100)->get($url)->json();    
        } catch (ConnectionException $e) {
            return 'timeout';
        }
	}
	public function scanProduct($page,$shop){//---------------------------------------------scan product yang ada di tokopedia
		//----------------------------------------------------------------get product from yukmarket db
        //-------------------------------------------------get organization shop id
        $orgShop=app('App\Http\Controllers\OrganizationController')->getOrgShop($shop,"TOKPED-STORE");		
		$tokpedProducts = $this->getAllProductList($page,$shop);//-------------------------------data product dari tokopedia		   
		if($tokpedProducts['data']==null){//jika timeout atau data tidak ada		
			return 'end';
		}else{			            
			for($i=0;$i<count($tokpedProducts['data']);$i++){   
                echo "\n|||||||||||||||||||||||||||||CHECK : ".$tokpedProducts['data'][$i]['name'];
				//---------------------------------------------check apakah data sudah ada atau belum
				$checkProduct =app('App\Http\Controllers\ProductECommerceController')->checkProductEcommerce($tokpedProducts['data'][$i]['product_id'],IntegrationECommerceCommand::$ecommerceId);
				if($checkProduct==null){//---------------------------------------------jika data belum ada
                    echo "\n-----------------------------belum ada: ".$tokpedProducts['data'][$i]['product_id'];
							//---------------------------------------------insert ke db yukmarket	
					//-------------------------------------------insert to ecommerce
                    if($tokpedProducts['data'][$i]['variant']['childrenID']!=null&&count($tokpedProducts['data'][$i]['variant']['childrenID'])==1){
                        echo "\nvarian jadi parent";
                        $this->generateVariant($tokpedProducts['data'][$i]['variant']['childrenID'][0],null,$orgShop->organization_id);
                    }else{
                        echo "\ntambah:".$tokpedProducts['data'][$i]['name']." id:".$tokpedProducts['data'][$i]['product_id'];
                        $addProductEcommerce= array(
                            'prod_ecom_name'=>$tokpedProducts['data'][$i]['name'],
                            'prod_ecom_code'=>$tokpedProducts['data'][$i]['product_id'],
                            'ecommerce_id'=>IntegrationECommerceCommand::$ecommerceId,
                            'organization_id'=>$orgShop->organization_id,
                            'description'=>$tokpedProducts['data'][$i]['desc'],                        
                            'price'=>$tokpedProducts['data'][$i]['price'],
                            'active_flag'=>$tokpedProducts['data'][$i]['active_flag'],
                            'uom_value'=>$tokpedProducts['data'][$i]['weight'],
                            'uom_id'=>$tokpedProducts['data'][$i]['weight_type'],  
                            'stock'=>$tokpedProducts['data'][$i]['stock'],                     
                            'category_id'=>$tokpedProducts['data'][$i]['category_id'],
                            'prod_image'=>$tokpedProducts['data'][$i]['prod_image'],
                            'etalase_id'=>$tokpedProducts['data'][$i]['etalase_id'],
                            'etalase_name'=>$tokpedProducts['data'][$i]['etalase_name']
                        );     
                        app('App\Http\Controllers\ProductECommerceController')->addProduct($addProductEcommerce);
                        // store tokped image product
                        
                        for($a=0;$a<count($tokpedProducts['data'][$i]['prod_image_list']);$a++){
                            app('App\Http\Controllers\ProductImageController')->addProductImageEcommerce($tokpedProducts['data'][$i]['prod_image_list'][$a]);
                        }
                        
                        //---------------------------------check variant
                        if($tokpedProducts['data'][$i]['variant']['childrenID']!=null){                    
                            //-------------------generate variant                        
                            for($x=0;$x<count($tokpedProducts['data'][$i]['variant']['childrenID']);$x++){
                                $this->generateVariant($tokpedProducts['data'][$i]['variant']['childrenID'][$x],$tokpedProducts['data'][$i]['product_id'],$orgShop->organization_id);
                            }
                        }
                    }               
				}else{//----------------------------------------jika data product sudah ada
                    echo "\nsudah ada : ".$checkProduct->prod_ecom_name;
					//--------------------------cek apakah data sku update atau tidak        
                    //---------------------------------check variant    
                    if($tokpedProducts['data'][$i]['variant']['childrenID']!=null){
                        //-------------------generate variant
                        for($x=0;$x<count($tokpedProducts['data'][$i]['variant']['childrenID']);$x++){
                            $this->generateVariant($tokpedProducts['data'][$i]['variant']['childrenID'][$x],$tokpedProducts['data'][$i]['product_id'],$orgShop->organization_id);
                        }
                    }                                               
				}                
			}
            return 'next';
		}					
	}	
	public function updateStatusScanTokopedia($status){				
		$update = DB::table('99_global_parameter')
		->where('global_parameter_name','tokopedia')
		->update(['global_parameter_value'=>$status =="on" ? "off" : "on"]);
		return $update;
	}
	public function enableScanTokopedia(){		
		$prevStatus = $this->getStatusScanTokopedia();		
		$updateStatus = $this->updateStatusScanTokopedia($prevStatus->global_parameter_value);
		$currentStatus = $this->getStatusScanTokopedia();
		if(!$updateStatus){
			return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
		}else{
			return response()->json(['isSuccess' => true, 'message' => 'success changing scan tokopedia from '.$prevStatus->global_parameter_value.' to '.$currentStatus->global_parameter_value], 200);
		}
	}
    public function getAllProductList($page,$shop){//---------------------------------------------get api productlist tokopedia
        $url = config('global.tokopedia_integrator_url').'/getAllProductByShopId?page='.$page.'&pageSize=50&shopId='.$shop;
        try {
            echo "\n".$url;
            $dataProduct = Http::withToken(IntegrationECommerceCommand::$token)->timeout(15)->retry(2, 1000)->get($url)->json();
            if($dataProduct['data']==null){
                return $response=array(
                    'data'=>null
                );
            }
            for($i=0;$i<count($dataProduct['data']);$i++){
                $imageList=array();
                for($a=0;$a<count($dataProduct['data'][$i]['pictures']);$a++){
                    $arrayImage=array(
                        'prod_ecom_code'=>$dataProduct['data'][$i]['basic']['productID'],
                        'prod_image'=>$dataProduct['data'][$i]['pictures'][$a]['OriginalURL']
                    );
                    array_push($imageList, $arrayImage);
                }
                $temp[$i]=array(
                 'product_id'=> $dataProduct['data'][$i]['basic']['productID'],
                  'name'=> $dataProduct['data'][$i]['basic']['name'],
                  'sku'=> $dataProduct['data'][$i]['other']['sku'],
                  'shop_id'=> $shop,                  
                  'category_id'=> $dataProduct['data'][$i]['basic']['childCategoryID'],
                  'desc'=> $dataProduct['data'][$i]['basic']['shortDesc'],
                  'stock'=> $dataProduct['data'][$i]['main_stock'],
                  'price'=> $dataProduct['data'][$i]['price']['value'],
                  'weight_type'=>$dataProduct['data'][$i]['weight']['unit']==1 ? 2 : 1,
                  'weight'=>$dataProduct['data'][$i]['weight']['value'],
                  'active_flag'=> $dataProduct['data'][$i]['basic']['status']==3 ? 0 : 1,
                  'variant'=>$dataProduct['data'][$i]['variant'] == null ? null : $dataProduct['data'][$i]['variant'],
                  'prod_image'=>$dataProduct['data'][$i]['pictures'][0]['OriginalURL'],
                  'prod_image_list'=>$imageList,
                  'etalase_id'=>$dataProduct['data'][$i]['menu']['id'],
                  'etalase_name'=>$dataProduct['data'][$i]['menu']['name'],
                );                
            }     
            $response=array(
                    'data'=>$temp
                );                   
            return $response;    
        } catch (ConnectionException $e) {
            $response=array(
                'data'=>null
            );            
            return $response;
        }                
    }
    public function scanProductSKU($product){
    	$skuId = app('App\Http\Controllers\ProductController')->checkProductSKU($product['sku']);	    	
    	if($skuId==null){//--------------------------------------------jika sku belum ada maka insert sku
			app('App\Http\Controllers\ProductController')->addProductSKU($product['sku']);
			$skuId = app('App\Http\Controllers\ProductController')->checkProductSKU($product['sku']);
			$checkProduct=app('App\Http\Controllers\ProductController')->checkProduct($product['product_id']);
			if($checkProduct!=null){//update product sku id
				app('App\Http\Controllers\ProductController')->updateProductSKUOnly($product['product_id'],$skuId->product_sku_id);
			}	 
		}
		return $skuId;
    }
    public function validateUpdateStockTokped($shopId,$prodId,$stock,$price,$updateStatus){    
    	$shop=app('App\Http\Controllers\WarehouseController')->checkWarehouse($shopId);
    	if($shop!=null){
    		$product =app('App\Http\Controllers\ProductController')->checkProduct($prodId);
    		if($product!=null){
    			$this->pushUpdateStockTokped($shopId,$product->prod_ecom_id,$stock);    			
    			if($updateStatus=='update_price'){
	    			$this->pushUpdatePriceTokped($shopId,$product->prod_ecom_id,$price);    				
	    		}
    		}    		 
    	}
    }
    public function pushUpdateStockTokped($shopId,$prodId,$stock){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$prodId.'&productStocks='.$stock.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$updateStok = Http::post($url);
    		if($updateStok!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $updateStok;
    	}
    }
    public function pushUpdatePriceTokped($shopId,$prodId,$price){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/updateProductPrice?productIdsOrSkus='.$prodId.'&productPrices='.$price.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$updatePrice = Http::post($url);
    		if($updatePrice!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $updatePrice;
    	}
    }
    public function activeOrInactiveProduct($prodId,$shopId,$action){    	
    	if($action=='in_active'){
    		$this->setInActiveProduct($prodId,$shopId);
    	}elseif($action=='active'){
    		$this->setActiveProduct($prodId,$shopId);
    	}
    }
    public function setActiveProduct($prodId,$shopId){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/setActiveProduct?productId='.$prodId.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$active = Http::post($url);
    		if($active!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $active;
    	}
    }
    public function setInActiveProduct($prodId,$shopId){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/setInactiveProduct?productId='.$prodId.'&shopId='.$shopId;    	    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$inActive = Http::post($url);
    		if($inActive!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $inActive;
    	}
    }
    public function scanProductWarehouse($prodId,$warehouseId){
        //---------------------------------------check product warehouse sudah ada atau belum
        $productWarehouse=app('App\Http\Controllers\ProductWarehouseController')->checkProductWarehouse($prodId,$warehouseId);
        if($productWarehouse==null){//------------------jika belum ada maka insert
            app('App\Http\Controllers\ProductWarehouseController')->addProductWarehouse($prodId,$warehouseId);
        }
    }
    // ----------------SYNC ORDER TOKPED START
    public function syncOrder($ecommerce){
        switch ($ecommerce) {
            case 'TOKOPEDIA':             
                $tokped=(new OrderTokopediaController);
                $tokped->syncOrder();           
                break;
            
            default:                        
                break;
        } 
    }
    public function generateVariant($idVariant,$parentId,$orgShopId){

        $tokpedProducts=$this->getDataVariant($idVariant);
        //rollback product ecommerce parent
        if(!$tokpedProducts){
            echo "\nproduct variant :".$idVariant." gagal GET data";
        }else{
        //---------------------check data variant
            $checkProduct =app('App\Http\Controllers\ProductECommerceController')->checkProductEcommerce($tokpedProducts['data'][0]['basic']['productID'],IntegrationECommerceCommand::$ecommerceId);
            if($checkProduct==null&&$tokpedProducts){
                //product image
                $imageList=array();
                for($a=0;$a<count($tokpedProducts['data'][0]['pictures']);$a++){
                    $arrayImage=array(
                        'prod_ecom_code'=>$tokpedProducts['data'][0]['basic']['productID'],
                        'prod_image'=>$tokpedProducts['data'][0]['pictures'][$a]['OriginalURL']
                    );
                    array_push($imageList, $arrayImage);
                }
                $addProductEcommerce= array(
                    'prod_ecom_code'=>$tokpedProducts['data'][0]['basic']['productID'],
                    'prod_ecom_name'=>$tokpedProducts['data'][0]['basic']['name'],
                    'ecommerce_id'=>IntegrationECommerceCommand::$ecommerceId,
                    'organization_id'=>$orgShopId,
                    'description'=>$tokpedProducts['data'][0]['basic']['shortDesc'],                        
                    'price'=>$tokpedProducts['data'][0]['price']['value'],
                    'active_flag'=> $tokpedProducts['data'][0]['basic']['status']==3 ? 0 : 1,
                    'uom_value'=>$tokpedProducts['data'][0]['weight']['value'],
                    'uom_id'=>$tokpedProducts['data'][0]['weight']['unit']==1 ? 2 : 1,
                    'stock'=>$tokpedProducts['data'][0]['stock']['value'],                     
                    'category_id'=>$tokpedProducts['data'][0]['basic']['childCategoryID'],
                    'variant_id'=>$parentId,
                    'etalase_id'=>$tokpedProducts['data'][0]['menu']['id'],
                    'etalase_name'=>$tokpedProducts['data'][0]['menu']['name'],
                );
                app('App\Http\Controllers\ProductECommerceController')->addProduct($addProductEcommerce);
                // store tokped image product
                for($b=0;$b<count($tokpedProducts['data'][0]['pictures']);$b++){
                    app('App\Http\Controllers\ProductImageController')->addProductImageEcommerce($imageList[$b]);
                }
            }
        }
    }
    public function getDataVariant($idVariant){
        $url = config('global.tokopedia_integrator_url').'/getSingleProductByProductId?productId='.$idVariant;
        try {
            $req = Http::withToken(IntegrationECommerceCommand::$token)->timeout(3)->retry(2, 100)->get($url)->json();                
        } catch (ConnectionException $e) {
            $req=null;            
        }        
        return $req;
    }
    public function singleOrderTokpedSync(){
        $orderList=DB::table('10_order_ecommerce')
        ->select('order_id','order_ecom_code','order_status_ecom_id','organization_id')
        ->where([
            ['order_status_ecom_id','<',700],
            ['order_status_ecom_id','>',15],
            ['order_status_ecom_id','!=',550],
    ])
        ->get();
        $shopName=app('App\Http\Controllers\GlobalSettingsController')->getShopName();
        $shopIdList=app('App\Http\Controllers\OrganizationController')->getShopByEcomName($shopName->global_parameter_value);
        //------------------validation
        if(count($orderList)>0){
            for($i=0;$i<count($orderList);$i++){
                echo "\n| compare order status single order:".$orderList[$i]->order_id." |";
                $singleOrder=$this->hitSingleOrder($orderList[$i]->order_ecom_code);
                if($singleOrder!=null){
                    echo "\n| cek perubahan status |";
                    //------------------------------jika status berubah
                    if($singleOrder['data']['order_info']['order_history'][0]['hist_status_code']!=$orderList[$i]->order_status_ecom_id){
                        echo "\n| status perlu update |";
                        //----------------------------------update status
                        app('App\Http\Controllers\OrderTokopediaController')->updateStatusOrder($orderList[$i]->order_ecom_code,$singleOrder['data']['order_info']['order_history'][0]['hist_status_code'],$orderList[$i]->order_id,$orderList[$i]->organization_id);
                        //-------------------------------------validasi update preparation
                        app('App\Http\Controllers\PreparationController')->addOrUpdatePreparation($orderList[$i]->order_id,$singleOrder['data']['order_info']['order_history'][0]['hist_status_code'],'update');
                        //--------------------------------------------jika status update ke pengiriman maka kurangi stok
                        //-----------------------------------check resi
                        if($orderList[$i]->order_status_ecom_id<400&&$singleOrder['data']['order_info']['order_history'][0]['hist_status_code']>=400){
                            echo "\nUpdate Resi";
                            $resi=app('App\Http\Controllers\OrderTokopediaController')->getResiTokped($orderList[$i]->order_ecom_code);
                            $currentResi=$this->checkResi($orderList[$i]->order_id);
                            if($currentResi!=$resi['resi']){
                                //-------------------------------update resi
                                $this->updateResi($orderList[$i]->order_id,$resi['resi']);
                            }
                            //-----------------------update label
                            $cekLabel=app('App\Http\Controllers\OrderTokopediaController')->cekLabel($orderList[$i]->order_id);
                            if($cekLabel->shipping_receipt_label==null){
                                app('App\Http\Controllers\OrderTokopediaController')->updateLabel($orderList[$i]->order_id,$resi['label']);
                            }
                        }elseif($orderList[$i]->order_status_ecom_id<500&&$singleOrder['data']['order_info']['order_history'][0]['hist_status_code']>=500&&$orderList[$i]->order_status_ecom_id<600){
                            // $warehouseId=app('App\Http\Controllers\OrganizationController')->getWarehouse(IntegrationECommerceCommand::$ecommerceId);
                            // app('App\Http\Controllers\InventoryController')->updateStockOrderTokped($orderList[$i]->order_id,$warehouseId->warehouse_id,$shopIdList[0]->organization_id);
                            //goods in transit
                            echo "\ngoods_in_transit";
                            app('App\Http\Controllers\InventoryController')->goodsInTransit($orderList[$i]->order_id,$shopIdList[0],'goods_in_transit');
                        }elseif($orderList[$i]->order_status_ecom_id<600&&$singleOrder['data']['order_info']['order_history'][0]['hist_status_code']>=600&&$orderList[$i]->order_status_ecom_id<700){
                            echo "\ngoods_arrived";
                            app('App\Http\Controllers\InventoryController')->goodsInTransit($orderList[$i]->order_id,$shopIdList[0],'goods_arrived');
                        }elseif($orderList[$i]->order_status_ecom_id<700&&$singleOrder['data']['order_info']['order_history'][0]['hist_status_code']>=700){
                            echo "\ndone";
                            app('App\Http\Controllers\InventoryController')->goodsInTransit($orderList[$i]->order_id,$shopIdList[0],'done');
                        }
                        echo "\nEND";
                    }
                }
            }    
        }else{
            echo "\n| single order pending tidak ada |";
        }
    }    
    public function hitSingleOrder($id){        
        $url=config('global.tokopedia_integrator_url').'/getSingleOrderEncrypted?orderId='.$id;
        try {
            return Http::withToken(IntegrationECommerceCommand::$token)->timeout(3)->retry(2, 100)->get($url)->json();
        } catch (ConnectionException $e) {
            echo "\nurl: ".$url." TIMEOUT";
            return null;
        }
    }
    public function checkResi($orderId){
        $resi=Order::where('order_id',$orderId)
        ->select('shipping_receipt_num')
        ->first();
        return $resi->shipping_receipt_num;
    }
    public function updateResi($orderId,$resi){
        Order::where('order_id',$orderId)
        ->update([
            'shipping_receipt_num'=>$resi
        ]);
    }
    public function getShopIdTokped(){
        $tokpedShop=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokped_shop');
        $shopId=app('App\Http\Controllers\OrganizationController')->organizationByName($tokpedShop->global_parameter_value);
        return $shopId;
    }
    public function pushStockEcommerce($prodId,$stock){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $tokpedShopId=$this->getShopIdTokped()->organization_code;
        $prodEcom=app('App\Http\Controllers\ProductECommerceController')->getProductEcom($prodId);
        if($prodEcom){
            $pushToTokped=$this->pushStockTokped($prodEcom,$stock,$tokpedShopId,$token);
            if($pushToTokped!=null&&$pushToTokped!='inActive'){
                $isSuccess=true;
                $msg='';
            }elseif($pushToTokped!=null&&$pushToTokped=='inActive'){
                $isSuccess=false;
                $msg='inActive';
            }
            else{
                $isSuccess=false;
                $msg='';
            }
            return response()->json(['isSuccess' => $isSuccess, 'message' => $msg, 'data'=>[]], 200);
        }
    }
    public function pushStockTokped($prodEcom,$stock,$shopId,$token){
        $setting=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('mobile_tokped');
        if($setting->global_parameter_value!='on'){
            return 'inActive';
        }else{
            $url=config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$prodEcom.'&productStocks='.$stock.'&shopId='.$shopId;
            try {
                return Http::withToken($token)->timeout(10)->retry(2, 2000)->post($url)->json();
            } catch (ConnectionException $e) {
                echo "\nurl: ".$url." TIMEOUT";
                return null;
            }
        }
    }
    // ----------------SYNC ORDER TOKPED END
}
