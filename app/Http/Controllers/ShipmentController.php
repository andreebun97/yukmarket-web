<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ShipmentController extends Controller
{
    public function getShipmentMethod(){
        $q = DB::table('00_shipment_method')
            ->select('shipment_method_id', 'shipment_method_name', 'price')
            ->where('active_flag','=','1')
            ->get();

        if (!$q) {
            return response()->json(['isSuccess' => false, 'message' => 'Data not found', 'data'=>[]], 500);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        }
    }
    public function checkShipmentEcommerce($name){
    	return DB::table('00_shipment_method_ecommerce')
    	->select('shipment_method_ecom_id','shipment_method_name')
    	->where('shipment_method_name',$name)
    	->first();
    }
    public function addShipmentEcommerce($data){
    	return DB::table('00_shipment_method_ecommerce')
    	->insertGetId($data);
    }
}
