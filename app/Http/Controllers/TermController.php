<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TermController extends Controller {

    public function getTerm() {
        $term = DB::table('98_privacy_term')
                ->select('key', 'value')
                ->where('key', 'term_condition')
                ->get();

        if (count($term) < 1) {
            return response()->json(['isSuccess' => true, 'message' => 'Data not found', 'data' => []], 200);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $term], 200);
        }
    }

}
