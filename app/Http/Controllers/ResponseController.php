<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function getResponseData($data){
        if($data == null){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK','data' => $data], 200);                      
        }
    }

    public function customOutput($validation, $data){
    	if($validation == null){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK','data' => $data], 200);                      
        }
    }
}
