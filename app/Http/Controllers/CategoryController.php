<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller {

    public function addCategory($request) {
        return DB::table('00_category')
                        ->insertGetId($request);
    }

    public function getIdCategoryFromEcommerce($id, $orgId) {
        return DB::table('00_category_per_ecommerce')
                        ->select('category_id')
                        ->where([
                                ['category_id_ecommerce', $id],
                                ['organization_id', $orgId]
                        ])
                        ->first();
    }

    public function getCategoryDua($id) {
        try {
            $data = DB::table('00_category')
                    ->select('category_id', 'category_name')
                    ->where([
                            ['active_flag', $id],
                            ['organization_id', $orgId]
                    ])
                    ->first();
            $status_code = 200;
            $status = 200;
            $message = 'Success';
        } catch (\Exception $e) {
            $status_code = 400;
            $status = 400;
            $message = $e->getMessage();
            $data = array();
        }
        $array = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($array, $status_code);
    }

    public function getCategoryTiga($id) {
        
    }

}
