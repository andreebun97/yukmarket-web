<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class MiddlewareEndpointController extends Controller
{
	//BASE URL
    public function getUrl(String $url){
  	// return config('global.middleware_endpoint').$url;
//         return config('global.middleware_endpoint_staging').$url;
         // return config('global.middleware_endpoint_live').$url;
        return env('MIDDLEWARE_ENDPOINT').$url;
    }

    //CART START ----------------------------------------------------------------
    public function checkDataCart(Request $request){
    	$urlCheck = $this->getUrl('/cart/getIdCart');        
        $bodyCheck = array(
                "buyer_user_id" => $request->user_id,
                "prod_id" => $request->product_id
            );
        $responseCheck = Http::post($urlCheck, $bodyCheck);
        return $responseCheck;
    }
    public function AddCart($requestBody){ //ARRAY REQUEST BODY
        $url = $this->getUrl('/cart/addCart');               
        $add = Http::post($url, $requestBody);
        return $add;
    }
    public function updateQtyCart($request){
    	$urlUpdate = $this->getUrl('/cart/updateQtyCart');        
        $responseAction = Http::post($urlUpdate, $request);
        return $responseAction;
    }    
    public function deleteProductCart($request){
    	$urlDelete = $this->getUrl('/cart/deleteFromCart');            
        $bodyDelete = array(                    
                "buyer_user_id" => $request->user_id,
                "prod_id" => $request->product_id
            );        
        $delete = Http::post($urlDelete, $bodyDelete);
        return $delete;
    }
    //CART END ----------------------------------------------------------------

    //PROFILE START ----------------------------------------------------------
    public function getProfile($request){
    	 if($request->is_agent == 0){
            $url = $this->getUrl('/profile/getProfileCustomer');
            $body = array(
                    "customer_id" => $request->user_id                    
            );                    
        }else{
            $url = $this->getUrl('/profile/getProfileAgent');
            $body = array(
                    "user_id" => $request->user_id                    
            );
        }
        $response = Http::post($url, $body);
        return $response;
    }
    public function updatePhoto($request){    	
    	 if($request['is_agent'] == 0){
            $url = $this->getUrl('/profile/updatePhotoCustomer');
            $body = array(
                    "customer_id" => $request['user_id'],
                    "customer_image" => $request['image']                    
            );                    
        }else{
            $url = $this->getUrl('/profile/updatePhotoAgent');
            $body = array(
                    "user_id" => $request['user_id'],
                    "user_image" => $request['image']                    
            );
        }
        $response = Http::post($url, $body);
        return $response;
    }
    public function addAddressCustomer(array $request){
        $url = $this->getUrl('/profile/addAddressCustomer');        
        return Http::post($url, $request);
    }
    //PROFILE END ----------------------------------------------------------

    //DISTRICT START ----------------------------------------------------------
    public function getProvinsi(){
        $url = $this->getUrl('/district/provinsi');
        return json_decode(file_get_contents($url), true);
    }
    public function getKabKot($id){
        $url = $this->getUrl('/district/kabkot');
        $body = array(
                "provinsi_id" => $id
        );  
        return Http::post($url, $body);
    }
    public function getKecamatan($id){
        $url = $this->getUrl('/district/kecamatan');
        $body = array(
                "kabupaten_kota_id" => $id
        );
        return Http::post($url, $body);
    }
    public function getKelurahan($id){
        $url = $this->getUrl('/district/kelurahan');
        $body = array(
                "kecamatan_id" => $id
        );
        return Http::post($url, $body);
    }
    //DISTRICT END ----------------------------------------------------------

    //SHIPMENT START ----------------------------------------------------------
    public function getShipmentMethod(){
        $url = $this->getUrl('/shipment/shipmentMethod');
        return json_decode(file_get_contents($url), true);
    }
    //SHIPMENT END ----------------------------------------------------------

    //PAYMENT START ----------------------------------------------------------
    public function getPaymentMethod(){
        $url = $this->getUrl('/payment/paymentMethod');
        return json_decode(file_get_contents($url), true);
    }
    //PAYMENT END ----------------------------------------------------------

    //TAG START ----------------------------------------------------------
    public function getTag(){
        $url = $this->getUrl('/tag/tag');
        return json_decode(file_get_contents($url), true);
    }
    //TAG END ----------------------------------------------------------

    //COMPLAINT START ----------------------------------------------------------
   public function getComplaintHistory(Request $request){         
        if($request->is_agent == 0){
            $url = $this->getUrl('/complaint/complaintHistoryCustomer');    
        }else{
            $url = $this->getUrl('/complaint/complaintHistoryAgent');    
        }               
        $body = array(
                "customer_id"=>$request['user_id'],
                "order_id"=>$request['order_id']
        );
        return Http::post($url, $body);
    }

    
    public function getProductComplaint($id){        
        $url = $this->getUrl('/complaint/getProduct');
        $body = array(
                "issue_id"=>$id                
        );
        $product = Http::post($url, $body);        
        
        $url2 = $this->getUrl('/complaint/countTotalProduct');
        $body2 = array(
                "issue_id"=>$id                
        );
        $total = Http::post($url2, $body2);   
        $res = array(
            "data"=>$product['data'],
            "total"=>$total['data']['total']
        );     
        return $res;
    }
    public function getAttachment($id){        
        $url = $this->getUrl('/complaint/getAttachment');
        $body = array(
                "issue_id"=>$id                
        );
        return Http::post($url, $body);
    }
    
    public function solution(){
        $url = $this->getUrl('/complaint/getSolution');
        return json_decode(file_get_contents($url), true);
    }

    public function complaintCategory(){
        $url = $this->getUrl('/complaint/getCategory');
        return json_decode(file_get_contents($url), true);
    }
    //COMPLAINT END ----------------------------------------------------------

    //VOUCHER START ----------------------------------------------------------
    public function getVoucher($voucher_code){
        if($voucher_code != null && $voucher_code != ""){ 
            $url = $this->getUrl('/voucher/getVoucherByCode');
            $body = array(
                    "voucher_code" => $voucher_code
                );
            $response = Http::post($url, $body);           
            $voucher = array(); 
            array_push($voucher, $response['data']);
        }else{            
             $url = $this->getUrl('/voucher/getValidVoucher');
            $response = json_decode(file_get_contents($url), true);
            $voucher = $response['data'];
        }
        return $voucher; 
    }       
    //VOUCHER END ----------------------------------------------------------

    //AUTH START ----------------------------------------------------------
    public function addCustomer(array $request){
        $url = $this->getUrl('/auth/addCustomer');
        $body = array(
                "customer_name" => $request['username'],
                "customer_phone_number" => $request['phone'],
                "customer_email"=> $request['email'],
                "customer_password"=>Hash::make($request['password']),
                "last_login" => $request['last_login']
        );
        return Http::post($url, $body);
    }
    public function checkCustomer($username,$phone_number){
        $url = $this->getUrl('/auth/logincheckcustomer');                           
        $body = array(
                "customer_email" => $username,
                "customer_phone_number" => $phone_number
        );
        return Http::post($url, $body);
    }
    public function login(Request $request){        
        $response = array();
        //login cust                
        $request2 = $this->checkCustomer($request->username,$request->username);
        if($request2['data']!=null){
            $response = array(
                "active_flag"=> $request2['data']['active_flag'],
                "id"=> $request2['data']['customer_id'],
                "name"=> $request2['data']['customer_name'],
                "email"=> $request2['data']['customer_email'],
                "phone_number"=> $request2['data']['customer_phone_number'],
                "password"=> $request2['data']['customer_password'],
                "is_agent"=>0                
            );
        }else{
            //login agent
            $url2 = $this->getUrl('/auth/logincheckagent');                    
            $body2= array(
                    "user_email" => $request->username,
                    "user_phone_number" => $request->username
                );
            $request1 = Http::post($url2, $body2);
            if($request1['data']!=null){
                $response = array(
                    "active_flag"=> $request1['data']['active_flag'],
                    "id"=> $request1['data']['user_id'],
                    "name"=> $request1['data']['user_name'],
                    "email"=> $request1['data']['user_email'],
                    "phone_number"=> $request1['data']['user_phone_number'],
                    "password"=> $request1['data']['user_password'],
                    "organization_type_id"=> $request1['data']['organization_type_id'],
                    "organization_name"=> $request1['data']['organization_name'],
                    "organization_desc"=> $request1['data']['organization_desc'],
                    "is_agent"=>1
                );
            }       
        }        
        return $response;        
    }
    public function addOTP($id, $otp, $is_agent){
        if($is_agent==0){
            $url = $this->getUrl('/auth/insertotpcustomer');
            $body = array(
                "customer_id"=>$id,
                "otp"=>$otp
            );    
        }else{
            $url = $this->getUrl('/auth/insertotpagent');    
            $body = array(
                "user_id"=>$id,
                "otp"=>$otp
            );
        }        
        return Http::post($url, $body);
    }
    public function checkLoginSSO($email){
        $url = $this->getUrl('/auth/loginSSOCheckCustomer');
        $body = array(
            "customer_email" => $email,
            );
        return Http::post($url, $body);
    }
    public function addCustomerSSO(Request $request){
        $url = $this->getUrl('/auth/addCustomerSSO');
        $body = array(
                'customer_email' => $request->email,
                'last_login' => date('Y-m-d H:i:s'),
                'customer_name' => $request->name,
                'fcm_token' => $request->fcm_token
        );
        return Http::post($url, $body);
    }
    // public function getLastCustomer($email){
    //     $url_last_id = $this->getUrl('/auth/getLastCustomer');
    //     $body = array(
    //             'customer_email' => $email                
    //     );
    //     return Http::post($url_last_id, $body);
    // }
    public function getCustomerByEmail($email){
        $url_last_id = $this->getUrl('/profile/getCustomerByEmail');
        $body = array(
                'email' => $email                
        );
        return Http::post($url_last_id, $body);
    }
    public function getLastAddressId($id){
        $url_last_id = $this->getUrl('/profile/getLastAddress');
        $body = array(
                'customer_id' => $id,
                'user_id' => $id
        );
        return Http::post($url_last_id, $body);
    }
    public function updateCustomerSSO($id, $fcm_token, $generateToken){
         $url_update = $this->getUrl('/auth/updateCustomerSSO');
         $body_update = array(
          'fcm_token' => $fcm_token,
          'remember_token' => $generateToken,
          'customer_id' => $id
         );
        return Http::post($url_update, $body_update);
    }
    //AUTH END ----------------------------------------------------------

    //ORDER START ----------------------------------------------------------
    public function updateOrderStatus($is_agent, $user_id, $order_id, $order_status_id){
        if($is_agent==0){
            $url_update = $this->getUrl('/order/updateOrderStatusCustomer');
            $body_update = array(
                "buyer_user_id"=>$user_id,
                "order_id"=>$order_id,
                "order_status_id"=>$order_status_id
            );
        }else{
            $url_update = $this->getUrl('/order/updateOrderStatusAgent');
            $body_update = array(
                "agent_user_id"=>$user_id,
                "order_id"=>$order_id,
                "order_status_id"=>$order_status_id
            );
        }     
        
         return Http::post($url_update, $body_update);        
    }
    public function addOrderHistory($order_id,$order_status_id,$active_flag){
        $url = $this->getUrl('/order/addOrderHistory');
        $body = array(
            "order_status_id"=>$order_status_id,
            "order_id"=>$order_id,
            "active_flag"=>$active_flag
        );
        return Http::post($url, $body);
    }
    public function getParentOrderStatusId($order_id){
        $url = $this->getUrl('/order/getParentOrderStatusId');
        $body = array(            
            "order_id"=>$order_id            
        );
        return Http::post($url, $body);
    }
    public function getOrderByBillingNumber($invoice_no){
        $url = $this->getUrl('/order/getOrderByBillingNumber');
        $body = array(            
            "invoice_no"=>$invoice_no           
        );
        return Http::post($url, $body);
    }
    //ORDER END ----------------------------------------------------------


}
