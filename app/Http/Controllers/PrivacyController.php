<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrivacyController extends Controller {

    public function getPrivacy() {
        $privacy = DB::table('98_privacy_term')
                ->select('key', 'value')
                ->where('key', 'privacy_policy')
                ->get();

        if (count($privacy) < 1) {
            return response()->json(['isSuccess' => true, 'message' => 'Data not found', 'data' => []], 200);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $privacy], 200);
        }
    }

}
