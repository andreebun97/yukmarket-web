<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Organization;

class OrganizationController extends Controller
{
    public function getOrganizationByName($name){
    	$org=DB::table('00_organization')
    	->select('organization_id','organization_code','organization_desc','organization_name')
    	->where('organization_name',$name)
    	->first();    	
    	return $org->organization_id;
    }
    public function checkOrganizationEcommerce($shopId,$orgDesc){
    	$org =Organization::where([
    		['organization_desc',$orgDesc],
    		['organization_code',$shopId]
    	])->first();
        return $org;
    }
    public function addOrganization($req){
        Organization::insert($req);
    }
    public function getOrgShop($shop,$orgDesc){
        return Organization::where([
            ['organization_code',$shop],
            ['organization_desc',$orgDesc]
        ])->first();
    }
    public function getShopListByEcomName($ecomName){
        $ecomId=DB::table('00_organization')
        ->select('organization_id','organization_code','organization_desc','organization_name')
        ->where('organization_name',$ecomName)
        ->first();

        return DB::table('00_organization AS o')        
        ->join('00_product_ecommerce AS pe','pe.organization_id','=','o.organization_id')
        ->where('pe.ecommerce_id',$ecomId->organization_id)
        ->select('o.organization_id','o.organization_code','o.organization_desc','o.organization_name')        
        ->groupBy('o.organization_id')
        ->get();
    }
    public function getWarehouse($id){
        return DB::table('00_organization')
        ->select('warehouse_id','parent_organization_id')
        ->where('organization_id',$id)
        ->first();
    }
    public function getShopByEcomName($ecomName){
        return DB::table('00_organization')
        ->select('organization_id','organization_code','organization_desc','organization_name','parent_organization_id','warehouse_id')
        ->where('organization_name',$ecomName)
        ->get();
    }
    public function organizationByName($name){
        $org=DB::table('00_organization')
        ->select('organization_id','organization_code','organization_desc','organization_name','warehouse_id','parent_organization_id')
        ->where('organization_name',$name)
        ->first();      
        return $org;
    }
}
