<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Console\Commands\IntegrationECommerceCommand;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Order;
use App\OrderDetail;
use App\Payment;
use App\Inventory;
use App\StockCard;
use App\Preparation;
use App\Warehouse;
use App\Customer;
use App\Province;
use App\AddressDetail;
use GuzzleHttp\Client;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;

class OrderTokopediaController extends Controller
{
    public function syncOrder(){                  
        // dd(config('global.ecommerce.0'));            
        // $shopIdList=app('App\Http\Controllers\OrganizationController')->getShopListByEcomName('TOKOPEDIA');
        $shopName=app('App\Http\Controllers\GlobalSettingsController')->getShopName();
        $shopIdList=app('App\Http\Controllers\OrganizationController')->getShopByEcomName($shopName->global_parameter_value);
        //-----------------looping per shop id
        for($i=0;$i<count($shopIdList);$i++){            
        $page=1;    
            do {
                echo "\n| order page".$page." tokopedia - shop: ".$shopIdList[$i]->organization_name." |";    
                //-----------------------------------generate parameter            
                $reqTime =$this->getScheduleTime($shopIdList[$i]->organization_code,$page);    
                $url = config('global.tokopedia_integrator_url').'/getAllOrderEncrypted?endDate='.$reqTime['EndDate'].'&page='.$reqTime['Page'].'&pageSize='.$reqTime['pageSize'].'&shopId='.$reqTime['shopId'].'&startDate='.$reqTime['StartDate'];                                
                // dd($url);
            echo "\n".'| url:'.$url.' page: '.$page;
                $order =$this->getAllOrderAPI($url);
                //-----------------jika tidak timeout maka eksekusi
                if($order['data']!=null){
                    echo "\n".'| ada order baru |';                       
                    //-----------------------sync order data
                    //---------------------loop order
			    	for($x=0;$x<count($order['data']);$x++){
				        //--------------------check apakah order sudah ada atau belum
				        $check = Order::where('order_code', 'ECM-TOKPED-'.$order['data'][$x]['order_id'])->first();
				        if(!$check){
                            //------------check status order 220
                            if($order['data'][$x]['order_status']==220||$order['data'][$x]['order_status']==221||$order['data'][$x]['order_status']==400){
                                //---------------------check product order sudah ada atau belum
                                echo "\n| valid order |";
                                $validProduct='valid';
                                for($a=0;$a<count($order['data'][$x]['products']);$a++){
                                    $checkProductOrder=app('App\Http\Controllers\ProductECommerceController')->checkProduct($order['data'][$x]['products'][$a]['id'],IntegrationECommerceCommand::$ecommerceId);
                                    if($checkProductOrder==null){
                                        $validProduct='invalid';
                                        echo "\n".'| data product:'.$order['data'][$x]['products'][$a]['id'].' invalid |';
                                        break;
                                    }
                                }
                                //---------------add new order jika product valid
                                if($validProduct=='valid'){
                                    echo "\n".'| belum ada data order: '.$order['data'][$x]['order_id'].' |';
                                    $this->distributeOrder($order['data'][$x],$shopIdList[$i]->organization_id,$shopIdList[$i]->warehouse_id,$shopIdList[$i]->parent_organization_id);
                                }else{
                                    echo "\n".'| product tidak valid, order: '.$order['data'][$x]['order_id'].' |';
                                }
                            }
                            
			        	}else{
                            echo "\n".'|order:'.$check->order_id.' sudah ada |';
                            //------------------------------------check status
                            $statusOrder=$this->checkStatusOrderEcommerce($order['data'][$x]['order_id']);
                            if($statusOrder->order_status_ecom_id!=$order['data'][$x]['order_status']){
                                //------------------------update status order
                                $this->updateStatusOrder($order['data'][$x]['order_id'],$order['data'][$x]['order_status'],$check->order_id,$shopIdList[$i]->organization_id);                                
                                //-------------------------------------validasi update preparation
                                app('App\Http\Controllers\PreparationController')->addOrUpdatePreparation($check->order_id,$order['data'][$x]['order_status'],'update');
                                //--------------------------------------------update resi
                                if($order['data'][$x]['order_status']==400){
                                    $resi=$this->getResiTokped($order['data'][$x]['order_id']);
                                    $this->updateResi($check->order_id,$resi['resi']);
                                    $this->updateLabel($check->order_id,$resi['label']);
                                }
                                //--------------------------------------------jika status update ke pengiriman maka kurangi stok
                                // if($statusOrder->order_status_ecom_id<500&&$order['data'][$x]['order_status']>=500){
                                //     $warehouseId=app('App\Http\Controllers\OrganizationController')->getWarehouse(IntegrationECommerceCommand::$ecommerceId);
                                //     // app('App\Http\Controllers\InventoryController')->updateStockOrderTokped($check->order_id,$warehouseId->warehouse_id,$shopIdList[$i]->organization_id);
                                //     //goods in transit
                                //     app('App\Http\Controllers\InventoryController')->goodsInTransit($orderList[$i]->order_id,$shopIdList[$i],'goods_in_transit');
                                //     //--------------update resi
                                //     $this->updateResi($check->order_id,$order['data'][$x]['custom_fields']['awb']);
                                // }
                            }
                        }
	    			}                    
                }else{
                    echo "\n".'| tidak ada order |';
                }
                $page++;                                        
            } while ($order['data']!=null);                    
        }
        //----------------------------sync status order        
    }
    public function distributeOrder($order,$shop,$warehouseId,$parentShop){          
        echo "\nparent shop: ".$parentShop;  	        
    	//----------------------------------------sync customer data
        $customerAndAddress=$this->syncAddressAndCustomer($order);
        //--------------------sync voucher order`
        //-------------------check apakah order menggunakan voucher
        $voucherId=0;
        if($order['promo_order_detail']['summary_promo']!=null){
        	echo "\n".'| tambah data voucher: '.$order['order_id'].' |';                       
        	$voucherId=$this->syncVoucherOrder($order['promo_order_detail']['summary_promo'],$order['amt']['ttl_amount']);
        }
        if($order['amt']['insurance_cost']>0){
        //--------------------add insurance
            $voucherHeaderId=$this->addVoucherHeader();
            $this->setInsurance($voucherHeaderId,$order['amt']['insurance_cost']);
        }
        //----------------insert invoice ref number
        //get order tokped fee
        $tokpedFee=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('order_fee_tokped');
        //---------------payment schema
        $insertPayment=array(
        	'invoice_no'=>$order['invoice_ref_num'],
            'online_flag' => 1,                
            'payment_date' => Carbon::parse($order['payment_date'])->setTimeZone('UTC')->format('Y-m-d H:i:s'),
            'invoice_status_id' => 2,                
            'grandtotal_payment' => $order['amt']['ttl_amount'],
            'is_agent' => 0,
            'voucher_id'=>$voucherId,
            'payment_method_id'=>14,
            'payment_duedate'=>Carbon::parse($order['payment_date'])->setTimeZone('UTC')->format('Y-m-d H:i:s'),
            'admin_fee_percentage'=>$tokpedFee->global_parameter_value,
            'admin_fee'=>$order['amt']['ttl_amount']*$tokpedFee->global_parameter_value/100
        );
        $payment=Payment::insertGetId($insertPayment);
        echo "\n".'| tambah data payment: '.$payment.' |';                                   
        //----------------------------insert order
        // $warehouseId=app('App\Http\Controllers\OrganizationController')->getWarehouse(IntegrationECommerceCommand::$ecommerceId);
        //------------------------------check status order
        $statusOrder=$this->checkStatusOrder($order['order_status']);
        //--------------------------------------sync shipment method
        $shipmentEcomName=$order['logistics']['shipping_agency'].'('.$order['logistics']['service_type'].')';
        $idShipment=$this->syncShipmentMethodEcommerce($shipmentEcomName);
        //------------------------dua tipe generate resi
        if($order['order_status']>=400){
            $resi=$this->getResiTokped($order['order_id']);
            $receipt=$resi['resi'];
            $label=$resi['label'];
        }else{
            $receipt=null;
            $label=null;
        }
        echo "\nbuyer: ".$order['buyer']['name'];
        $orderId=Order::insertGetId([
            'order_code'=>'ECM-TOKPED-'.$order['order_id'],
            'payment_id'=>$payment,
            'order_date'=>Carbon::parse($order['payment_date'])->setTimeZone('UTC')->format('Y-m-d H:i:s'),
            'shipment_price'=>$order['amt']['shipping_cost'],
            // 'warehouse_id'=>$warehouseId->warehouse_id,
            'warehouse_id'=>$warehouseId,
            'grand_total'=>$order['amt']['ttl_amount'],
            'buyer_name'=>$order['buyer']['name'],
            'buyer_address'=>$order['recipient']['address']['address_full'],
            'invoice_status_id'=>2,
            'order_status_id'=>$statusOrder->order_status_id,
            'buyer_user_id'=>$customerAndAddress['customer_id'],
            'destination_address'=>$customerAndAddress['address_id'],
            'agent_user_id'=>28,
            'shipment_method_id'=>$idShipment,
            'payment_duedate'=>Carbon::parse($order['payment_date'])->setTimeZone('UTC')->format('Y-m-d H:i:s'),
            'payment_method_id'=>14,
            'transfer_date'=>Carbon::parse($order['payment_date'])->setTimeZone('UTC')->format('Y-m-d H:i:s'),
            'shipping_receipt_num'=>$receipt,
            'shipping_receipt_label'=>$label
        ]);
        //---------------------------------------insert ke order ecommerce
        DB::table('10_order_ecommerce')
        ->insert([        	
        	'order_ecom_code'=>$order['order_id'],
            'order_id'=>$orderId,
            'organization_id'=>$shop,
            'order_ecom_date'=>$order['payment_date'],
            'order_status_ecom_id'=>$order['order_status']
        ]);
        echo "\n".'| tambah data order: '.$orderId.' |';
        //----------------------------------------------add history order
        $historyOrder=array(
            'order_id'=>$orderId,
            'order_status_id'=>$statusOrder->order_status_id
        );
        app('App\Http\Controllers\HistoryOrderController')->addHistoryOrder($historyOrder);
        //------------------------------------check history status 7
        if($statusOrder->order_status_id==7){
            //-------------------------------check status history 6
            app('App\Http\Controllers\HistoryOrderController')->generatePreviousHistory($historyOrder);
        }        
        //-------------------------------check preparation status
        app('App\Http\Controllers\PreparationController')->addOrUpdatePreparation($orderId,$order['order_status'],'insert');
        for($i=0;$i<count($order['products']);$i++){
            //-----------------------------check product
            $checkProduct =app('App\Http\Controllers\ProductECommerceController')->checkProduct($order['products'][$i]['id'],IntegrationECommerceCommand::$ecommerceId);            
            //----------------------------------------------insert order detail
            $detailSchema=array(
                'order_id'=>$orderId,
                'prod_id'=>$checkProduct->prod_id,
                'quantity'=>$order['products'][$i]['quantity'],
                'price'=>$order['products'][$i]['price'],
                // 'warehouse_id'=>$warehouseId->warehouse_id,
                'warehouse_id'=>$warehouseId,
                'bruto'=>$order['products'][$i]['weight'],
                'bruto_uom_id'=>1,
                'uom_id'=>1,
                'uom_value'=>$order['products'][$i]['weight']
            );            
            OrderDetail::insert($detailSchema);
            echo "\n".'| tambah data order detail: '.$checkProduct->prod_id.' |';
            //---------------------------------------check stockable product
            if($checkProduct->stockable_flag==1){//---------------jika stockable
                $inventoryProductId=$checkProduct->prod_id;
                $bookingQty=$order['products'][$i]['quantity'];
            }else{//-----------------------------------------------jika non stockable ambil dari parent
                echo "\nnon stockable: ".$checkProduct->parent_prod_id;
                $inventoryProductId=$checkProduct->parent_prod_id;
                //------------------------------------------------convert stock
                $prodParent=app('App\Http\Controllers\ProductController')->checkProduct($checkProduct->parent_prod_id);
                $bookingQty=app('App\Http\Controllers\ProductController')->convertBookingStock($prodParent->uom_value,$prodParent->uom_id,$checkProduct->uom_value,$checkProduct->uom_id,$order['products'][$i]['quantity']);
                echo "\nchild qty: ".$checkProduct->uom_value." converted: ".$bookingQty;
            }            
            //-------------------check inventory product            
            $inventoryCheck =Inventory::where([
                ['prod_id',$inventoryProductId],
                // ['warehouse_id',$warehouseId->warehouse_id],
                ['warehouse_id',$warehouseId],
                ['organization_id',$parentShop]
        ])->first();
            if(!$inventoryCheck){
                //----------------------------------jika inventory belum ada
                $inventorySchema=array(
                    'organization_id'=>$parentShop,
                    // 'warehouse_id'=>$warehouseId->warehouse_id,
                    'warehouse_id'=>$warehouseId,
                    'prod_id'=>$inventoryProductId,
                    'active_flag'=>1,
                    'booking_qty'=>$bookingQty
                );
                Inventory::insert($inventorySchema);
                echo "\n".'| tambah inventory prod: '.$inventoryProductId.' booking:'.$order['products'][$i]['quantity'].'|';
            }else{
                Inventory::where([
                    ['prod_id',$inventoryProductId],
                    ['warehouse_id',$warehouseId],
                    ['organization_id',$parentShop]
                ])
                ->update([
                    'booking_qty'=>$inventoryCheck->booking_qty+$bookingQty
                ]);
                echo "\n".'| update inventory prod: '.$checkProduct->prod_id.' booking:'.$order['products'][$i]['quantity'].'|';
            }
        }            	        
    }
    public function setInsurance($idVoucherHeader,$amount){
        $voucherDetail=array(
            'voucher_ecom_h_id'=>$idVoucherHeader,
            'voucher_type_id'=>5,
            'amount'=>$amount,
            'voucher_name_ecom'=>'insurance'
        );
        DB::table('00_voucher_ecommerce_detail')
        ->insert($voucherDetail);
        echo "\n".'| tambah asuransi |';
    }
    public function checkStatusOrder($statusOrderEcom){
        return DB::table('00_order_status_ecommerce')
        ->select('order_status_id')
        ->where('order_status_ecom_kode',$statusOrderEcom)
        ->first();
    }
    public function syncAddressAndCustomer($data){
        //-------------------check buyer
        $customer=Customer::orWhere([
            ['customer_email',$data['buyer']['email']],
            ['customer_phone_number',$data['buyer']['phone']]
    ])
        ->first();
        //--------------------jika customer belum ada maka register
        if($customer==null){
        	echo "\n".'| tambah data customer: '.$data['buyer']['email'].' |';
            $customerId=Customer::insertGetId([
                'customer_name'=>$data['buyer']['name'],
                'customer_email'=>$data['buyer']['email'],
                'customer_phone_number'=>$data['buyer']['phone'],
                'active_flag'=>1
            ]);
            echo "\n".'| tambah customer |';
        }else{
            $customerId=$customer->customer_id;
            echo "\n".'| customer sudah ada |';
        }        
        //--------------------check provinsi kabkot kecamatan
        $checkAddress=$this->syncAddress($data['recipient']);        
        //--------------------check address user
        $geo=explode(",", $data['recipient']['address']['geo']);        
        $addressDetail=AddressDetail::where([
            ['customer_id',$customerId],
            ['address_name',$data['recipient']['name']],
            ['provinsi_id',$checkAddress['provinsi_id']],
            ['kabupaten_kota_id',$checkAddress['kabupaten_kota_id']],
            ['kecamatan_id',$checkAddress['kecamatan_id']],
            ['address_detail',$data['recipient']['address']['address_full']],
            ['contact_person',$data['buyer']['name']],
            ['phone_number',$data['recipient']['phone']],
            ['gps_point',$data['recipient']['address']['geo']],
            ['latitude',$geo[0]],
            ['longitude',$geo[1]],
        ])
        ->first();
        if($addressDetail==null){
        	echo "\n".'| tambah data address detail |';
            $addressId=AddressDetail::insertGetId([
                'customer_id'=>$customerId,
                'address_name'=>$data['recipient']['name'],
                'provinsi_id'=>$checkAddress['provinsi_id'],
                'kabupaten_kota_id'=>$checkAddress['kabupaten_kota_id'],
                'kecamatan_id'=>$checkAddress['kecamatan_id'],
                'kelurahan_desa_id'=>1,
                'address_detail'=>$data['recipient']['address']['address_full'],
                'contact_person'=>$data['buyer']['name'],
                'phone_number'=>$data['recipient']['phone'],
                'gps_point'=>$data['recipient']['address']['geo'],
                'latitude'=>$geo[0],
                'longitude'=>$geo[1],
                'isMain'=>0
            ]);
            echo "\n".'| tambah detail address |';
        }else{
            $addressId=$addressDetail->address_id;
            echo "\n".'| address sudah ada |';
        }
        return $customerAndAddress=array(
            'address_id'=>$addressId,
            'customer_id'=>$customerId
        );
    }
    public function syncAddress($data){
        echo "\n".'| sync address |';
        //---------------------check province
        $province=Province::where('provinsi_name',strtoupper($data['address']['province']))->first();
        //---------------------jika provinsi belum ada
        if($province==null){
        	echo "\n".'| tambah data provinsi: '.$data['address']['province'].' |';
            $provinceId=Province::insertGetId([
                'provinsi_name'=>strtoupper($data['address']['province']),
                'active_flag'=>1
            ]);
            DB::table('00_provinsi_ecommerce')
            ->insert([
                'provinsi_ecom_id'=>IntegrationECommerceCommand::$ecommerceId,                
                'provinsi_id'=>$provinceId,
                'active_flag'=>1
            ]);
            echo "\n".'| tambah provinsi |';
        }else{
            $provinceId=$province->provinsi_id;
            echo "\n".'| provinsi sudah ada |';
        }
        //------------------check kabupaten/kota
        $kabkot=DB::table('00_kabupaten_kota')
        ->select('kabupaten_kota_id')
        ->where([
            ['kabupaten_kota_name',strtoupper($data['address']['city'])],
            ['provinsi_id',$provinceId]
        ])
        ->first();
        if($kabkot==null){
        	echo "\n".'| tambah data kabkot: '.$data['address']['city'].' |';
            $kabkotId=DB::table('00_kabupaten_kota')
            ->insertGetId([
                'kabupaten_kota_name'=>strtoupper($data['address']['city']),
                'provinsi_id'=>$provinceId,
                'active_flag'=>1
            ]);
            DB::table('00_kabupaten_kota_ecommerce')
            ->insert([
                'kabupaten_kota_ecom_id'=>IntegrationECommerceCommand::$ecommerceId,
                'kabupaten_kota_id'=>$kabkotId,
                'active_flag'=>1
            ]);
            echo "\n".'| tambah kota |';
        }else{
            $kabkotId=$kabkot->kabupaten_kota_id;
            echo "\n".'| kota sudah ada |';
        }
        //---------------------check kecamatan
        $kecamatan=DB::table('00_kecamatan')
        ->select('kecamatan_id')
        ->where([
            ['kecamatan_name',strtoupper($data['address']['district'])],
            ['kabupaten_kota_id',$kabkotId]
        ])
        ->first();
        if($kecamatan==null){
        	echo "\n".'| tambah data kabkot: '.$data['address']['district'].' |';
            $kecamatanId=DB::table('00_kecamatan')
            ->insertGetId([
                'kecamatan_name'=>strtoupper($data['address']['district']),
                'kabupaten_kota_id'=>$kabkotId,
                'active_flag'=>1
            ]);
            DB::table('00_kecamatan_ecommerce')
            ->insert([
                'kecamatan_ecom_id'=>IntegrationECommerceCommand::$ecommerceId,
                'kabupaten_kota_id'=>$kabkotId,
                'active_flag'=>1
            ]);
            echo "\n".'| tambah kecamatan |';
        }else{
            $kecamatanId=$kecamatan->kecamatan_id;
            echo "\n".'| kecamatan sudah ada |';
        }
        $address=array(
            'provinsi_id'=>$provinceId,
            'kabupaten_kota_id'=>$kabkotId,
            'kecamatan_id'=>$kecamatanId
        );
        return $address;
    }
    public function getScheduleTime($shopId,$page){
        $end = array(            
            'Hour'=>Carbon::now()->format('H'),
            // 'Hour'=>Carbon::now()->format('H'),
            'Minute'=>Carbon::now()->format('i'),
        );
        $start = array(            
            // 'Hour'=>Carbon::now()->subMinute(20)->format('H'),
            // 'Minute'=>Carbon::now()->subMinute(20)->format('i'),            
            'Hour'=>Carbon::now()->format('H'),
            'Minute'=>Carbon::now()->format('i'),            
        );                  
        $req = array(
            'EndDate'=>Carbon::today()->toDateString().'%20'.$end['Hour'].'%3A'.$end['Minute'].'%3A00',
            'Page'=>$page,
            'pageSize'=>10,
            'shopId'=>$shopId,
            'StartDate'=>Carbon::today()->subDays(1)->toDateString().'%20'.$start['Hour'].'%3A'.$start['Minute'].'%3A00',
        );                
        return $req;
    }
    public function getAllOrderAPI($url){
        try {
            $order = Http::withToken(IntegrationECommerceCommand::$token)->timeout(3)->retry(2, 100)->get($url)->json();    
        } catch (ConnectionException $e) {
            $order='timeout';
            echo "\nurl: '.$url.' TIMEOUT";
        }
        return $order;
    }
    public function tryTimeout($url){        
        try {
        	$client= new Client();
            // $getBanner = $client->request('GET', config('app.url_api') . '/banner', [
            $req = $client->request('GET', $url, [
                'headers' =>
                [
                ],
                'form_params' => [
                ]
                    ]
            );
            $response = json_decode($req->getBody()->getContents(), true);
            return $response;
            // if ($response['isSuccess'] == true) {
            //     $this->data['banner'] = (Object) $result['data'];                
            // } else {
            //     return back()->with('error', $result['message']);                                
            // }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            //return back()->with('error', $e->getMessage());
            $timeout =array(
            	'data'=>null
            );	            
            echo "\n".'| catch error api order'.' |';
            return $timeout;
        }                     
    }
    public function addVoucherHeader(){
        $voucherHeader=array(
            'voucher_id_ecom'=>IntegrationECommerceCommand::$ecommerceId,               
        );
        return app('App\Http\Controllers\VoucherController')->addVoucherHeader($voucherHeader);
    }
    public function syncVoucherOrder($data,$ttlAmount){
    	//------------------add voucher header
		$voucherHeaderId=$this->addVoucherHeader();
		//-------------------------------sync voucher detail
        //-----------------------------get global setting voucher
        $voucher_fee=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokopedia_shipping_fee');
        $voucher_fee_max=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokopedia_shipping_fee_max_amount');
    	for($i=0;$i<count($data);$i++){   
    		switch ($data[$i]['type']) {
	 			case 'discount':
	 				$voucherType=2;
	 				$amount=$data[$i]['discount_amount'];
	 				break;
	 			case 'cashback':
	 				$voucherType=4;
	 				$amount=$data[$i]['cashback_amount'];
	 				break;
	 			default:
	 				$voucherType=0;
	 				$amount=0;
	 				break;
	 		} 		    		
            $disc=0;
            $discAmount=$ttlAmount*$voucher_fee->global_parameter_value/100;
            if($discAmount>=$voucher_fee_max->global_parameter_value&&$voucherType=2){
                $voucherPercentage=$voucher_fee->global_parameter_value;
                $disc=$voucher_fee_max->global_parameter_value;
            }elseif ($voucherType==2&&$discAmount<$voucher_fee_max->global_parameter_value){
                $disc=$discAmount;
                $voucherPercentage=$voucher_fee->global_parameter_value;
            }else{
                $disc=null;
                $voucherPercentage=null;
            }
            echo "\ndiscount_percentage:".$voucher_fee->global_parameter_value;
            echo "\namount_after_discount: ".$disc;
    		$voucherDetail=array(
    			'voucher_ecom_h_id'=>$voucherHeaderId,
    			'voucher_type_id'=>$voucherType,
    			'amount'=>$amount,
    			'voucher_name_ecom'=>$data[$i]['name'],
                'discount_percentage'=>$voucher_fee->global_parameter_value,
                'amount_after_discount'=>$disc,
                'voucher_desc'=>'tokopedia:'.$ttlAmount."=".$disc,
    		);
    		DB::table('00_voucher_ecommerce_detail')
    		->insert($voucherDetail);
    	}
    	return $voucherHeaderId;
    }
    public function checkStatusOrderEcommerce($id){
        return DB::table('10_order_ecommerce')
        ->select('order_status_ecom_id')
        ->where('order_ecom_code',$id)
        ->first();
    }
    public function  updateStatusOrder($ecomId,$statusEcom,$orderId,$shopId){
        echo "\n".'| update status order start: '.$orderId.' |';
        //---------------------mapping status order
        $mappingStatusOrder=$this->getMappingStatusOrder($statusEcom);
        //----------------------add history
        $historyOrder=array(
            'order_id'=>$orderId,
            'order_status_id'=>$mappingStatusOrder->order_status_id
        );
        app('App\Http\Controllers\HistoryOrderController')->addHistoryOrder($historyOrder);
        //------------------------update order ecommerce status
        $this->updateStatusOrderEcom($ecomId,$statusEcom);
        echo "\n".'| updated status ecommerce: '.$ecomId.' |'.$statusEcom;
        //------------------------update order status
        $this->updateStatusOrderMaster($orderId,$mappingStatusOrder->order_status_id);
        echo "\n".'| updated status master: '.$orderId.' |'.$mappingStatusOrder->order_status_id;
        echo "\n".'| updated status order done: '.$orderId.' |';
        //-------------------------jik status reject / batal maka return booking qty inventory
        if($mappingStatusOrder->order_status_id==11){
            echo "\nreject / cancel order: ".$orderId;
            $orderDetail=$this->getProductByOrder($orderId);
            for($i=0;$i<count($orderDetail);$i++){
                if($orderDetail[$i]->stockable_flag==1){
                    $productStockable=$orderDetail[$i]->prod_id;
                    $qty=$orderDetail[$i]->quantity;
                }else{
                    $productStockable=$orderDetail[$i]->parent_prod_id;
                    $prodParent=app('App\Http\Controllers\ProductController')->checkProduct($orderDetail[$i]->parent_prod_id);
                    $qty=app('App\Http\Controllers\ProductController')->convertBookingStock($prodParent->uom_value,$prodParent->uom_id,$orderDetail[$i]->uom_value,$orderDetail[$i]->uom_id,$orderDetail[$i]->quantity);
                }
                $org=app('App\Http\Controllers\OrganizationController')->getWarehouse($shopId);
                // dd($productStockable,$qty,$org->warehouse_id,$org->parent_organization_id);
                app('App\Http\Controllers\InventoryController')->returnBookingQty($productStockable,$qty,$org->warehouse_id,$org->parent_organization_id);
            }
        }
    }
    public function updateStatusOrderEcom($ecomId,$statusEcom){
        DB::table('10_order_ecommerce')
        ->where('order_ecom_code',$ecomId)
        ->update([
            'order_status_ecom_id'=>$statusEcom
        ]);
    }
    public function getMappingStatusOrder($statusEcom){
        return DB::table('00_order_status_ecommerce')
        ->select('order_status_id')
        ->where([
            ['order_status_ecom_kode',$statusEcom],
            ['organization_id',IntegrationECommerceCommand::$ecommerceId]
        ])
        ->first();
    }
    public function updateStatusOrderMaster($id,$status){
        Order::where('order_id',$id)
        ->update([
            'order_status_id'=>$status
        ]);
    }
    public function syncShipmentMethodEcommerce($name){
        $shipment=app('App\Http\Controllers\ShipmentController')->checkShipmentEcommerce($name);
        if(!$shipment){
            $schema=array(
                'shipment_method_name'=>$name,
                'organization_id'=>IntegrationECommerceCommand::$ecommerceId
            );
            return app('App\Http\Controllers\ShipmentController')->addShipmentEcommerce($schema);
        }else{
            return $shipment->shipment_method_ecom_id;
        }
    }
    public function getProductByOrder($orderId){
        return OrderDetail::where('order_id',$orderId)
        ->select('00_product.prod_id','00_product.stockable_flag','00_product.parent_prod_id','quantity','00_product.uom_value','00_product.uom_id')
        ->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')
        ->get();
    }
    public function updateResi($orderId,$resi){
        Order::where('order_id',$orderId)
        ->update([
            'shipping_receipt_num'=>$resi
        ]);
    }
    public function getResiTokped($orderId){
        $orderId=$orderId;
        $url = config('global.tokopedia_integrator_url').'/getShippingLabel?orderId='.$orderId;
        $hitLabel=Http::withToken(IntegrationECommerceCommand::$token)->timeout(3)->retry(2, 100)->get($url)->json();
        $fullLabel=str_replace("\n",'', base64_decode($hitLabel['data']));
        $decodeLabel=str_replace(' ','',base64_decode($hitLabel['data']));
        if(strstr($decodeLabel,'<divclass="booking-code-text"width="50%">')){
            $removeBreakLine=str_replace("\n",'',$decodeLabel);
            $explode1=explode('<divclass="booking-code-text"width="50%">',$removeBreakLine);
            $resultExplode1=array_pop($explode1);
            $resi=explode('<divid="booking-code-message">', $resultExplode1);
            $receipt=$resi[0];
        }else {
            $receipt=null;
        }
        $response=array(
            'label'=>$fullLabel,
            'resi'=>$receipt
        );
        return $response;
    }
    public function cekLabel($orderId){
        $label=Order::where('order_id',$orderId)
        ->first();
        return $label;
    }
    public function updateLabel($orderId,$label){
        Order::where('order_id',$orderId)
        ->update([
            'shipping_receipt_label'=>$label
        ]);
    }
    public function missingBuyerData(){
        $missBuyer=DB::table('10_order')
        ->join('10_order_ecommerce','10_order_ecommerce.order_id','10_order.order_id')
        ->where('10_order.buyer_name','')
        ->orWhere('10_order.buyer_address','')
        ->orWhereNull('10_order.buyer_name')
        ->orWhereNull('10_order.buyer_address')
        ->select('10_order.order_id')
        ->get();
        return $missBuyer;
    }
    public function getOrderEcomCode($orderId){
        $orderEcomCodeList=DB::table('10_order_ecommerce')
        ->where('order_id',$orderId)
        ->select('order_ecom_code')
        ->first();
        return $orderEcomCodeList->order_ecom_code;
    }
    public function updateMissBuyerData(){
        $missBuyer=$this->missingBuyerData();
        for($i=0;$i<count($missBuyer);$i++){
            $orderEcom=$this->getOrderEcomCode($missBuyer[$i]->order_id);
            $singleOrder=app('App\Http\Controllers\IntegrationECommerceController')->hitSingleOrder($orderEcom);
            if($singleOrder!=null){
                $buyerName=$singleOrder['data']['origin_info']['receiver_name'];
                $buyerAddress=$singleOrder['data']['origin_info']['destination_address'];
                Order::where('order_id',$missBuyer[$i]->order_id)
                ->update([
                    'buyer_name'=>$buyerName,
                    'buyer_address'=>$buyerAddress
                ]);
            }
        }
    }
    public function getQtyOrderDetail($orderId){
        $orderDetail=DB::table('10_order_detail')
        ->where('order_id',$orderId)
        ->select('prod_id','quantity')
        ->get();
        return $orderDetail;
    }
    public function getOrderEcommerce($orderId){
        $order=DB::table('10_order_ecommerce')
        ->where('order_id',$orderId)
        ->select('order_ecom_code')
        ->first();
        return $order;
    }
    public function hitPostAPITokped($url,$token){
        try {
            $req = Http::withToken($token)->post($url);           
        } catch (ConnectionException $e) {
            $req=null;            
        }
        return $req;
    }
    public function requestPickup($orderId){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $shop=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
        $orderEcom=$this->getOrderEcommerce($orderId);
        $url=config('global.tokopedia_integrator_url').'/requestPickUp?orderId='.$orderEcom->order_ecom_code.'&shopId='.$shop->organization_code;
        $hit=$this->hitPostAPITokped($url,$token);
        $req=json_decode($hit->body());
        if (!$req) {
            return response()->json(['isSuccess' => false, 'message' => 'Permintaan Request Gagal'], 500);
        } else {
            if($req['data']==null){
                return response()->json(['isSuccess' => false, 'message' => 'Permintaan Request Gagal'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'Request Pickup berhasil'], 200);
            }
        }
    }
    public function acceptOrder($orderId){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $orderTokped=$this->getOrderEcommerce($orderId)->order_ecom_code;
        $url=config('global.tokopedia_integrator_url').'/acceptOrder?orderId='.$orderTokped;
        try {
            $req= Http::withToken($token)->timeout(10)->retry(2, 2000)->post($url)->json();
        } catch (ConnectionException $e) {
            echo "\nurl: ".$url." TIMEOUT";
            $req= null;
        }
        if($req){
            $isSuccess=true;
        }else{
            $isSuccess=false;
        }
        return response()->json(['isSuccess' => $isSuccess, 'message' => '', 'data'=>[]], 200);
    }
}
