<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function addTransactionHeader($data){
    	return DB::table('10_transaction_header')
    	->insertGetId($data);
    }
    public function addTransactionDetail($data){
    	DB::table('10_transaction_detail')
    	->insert($data);
    }
}
