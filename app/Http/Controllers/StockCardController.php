<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockCardController extends Controller
{
	public function addStockCard($data){
	    DB::table('00_stock_card')
	    ->insert([$data]);
	}
}
