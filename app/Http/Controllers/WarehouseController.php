<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    //-----INTEGRATION TOKOPEDIA START
    public function checkWarehouse($id){
    	$check = DB::table('00_warehouse')
    	->select('warehouse_id','warehouse_name','warehouse_desc','contact_person','phone_number')
    	->where('warehouse_id',$id)
    	->first();
    	return $check;
    }
    public function addWarehouse($request){
    	DB::table('00_warehouse')
    	->insert($request);
    }
    public function getWarehouseEcommerce($ecommerce){
        return DB::table('00_warehouse')
        ->where('warehouse_desc','like', '%'.$ecommerce.'%')        
        ->select('warehouse_id')
        ->get();        
    }
    public function getStorage($warehouseId){
        $warehouseType=$this->checkWarehouseType($warehouseId);
        if($warehouseType->warehouse_type_id==2){
            $paramWarehouseId='parent_warehouse_id';
            $paramWarehouseValue=$warehouseId;
        }else{
            $paramWarehouseId='warehouse_id';
            $paramWarehouseValue=$warehouseType->parent_warehouse_id;
        }
        $warehouseList=DB::table('00_warehouse')
        ->where($paramWarehouseId,$paramWarehouseValue)
        ->select('warehouse_id','warehouse_type_id')
        ->get();
        return $warehouseList;
    }
    public function checkInventoryColdStorage($orgId,$coldStorageId,$prodId){
        $coldInventory=DB::table('00_inventory')
        ->where([
            ['organization_id',$orgId],
            ['warehouse_id',$coldStorageId],
            ['prod_id',$prodId]
        ])
        ->select('stock','booking_qty','goods_in_transit','goods_arrived')
        ->first();
        return $coldInventory;
    }
    public function getStockColdStorage($orgId,$coldStorageId,$prodId){
        $coldStock=0;
        $coldStorageList=$this->getStorage($coldStorageId);
        for($i=0;$i<count($coldStorageList);$i++){
            $coldStorageInventory=$this->checkInventoryColdStorage($orgId,$coldStorageList[$i]->warehouse_id,$prodId);
            if($coldStorageInventory){
                $coldStock+=floor($coldStorageInventory->stock)-$coldStorageInventory->booking_qty-$coldStorageInventory->goods_in_transit-$coldStorageInventory->goods_arrived;
            }
        }
        return $coldStock;
    }
    public function checkWarehouseType($warehouseId){
        $warehouseType=DB::table('00_warehouse')
        ->where('warehouse_id',$warehouseId)
        ->select('warehouse_id','warehouse_type_id','parent_warehouse_id')
        ->first();
        return $warehouseType;
    }
    //-----INTEGRATION TOKOPEDIA END
}
