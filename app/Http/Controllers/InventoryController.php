<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Inventory;
use Carbon\Carbon;

class InventoryController extends Controller
{
    public function addInventoryFromTokoped($request){
    	DB::table('00_inventory')
    	->insert($request);
    }
    public function checkProductPrice($idProduct){
    	$check = DB::table('00_inventory')
    	->select('inventory_price')
    	->where('prod_id',$idProduct)
    	->first();
    	return $check;
    }
    public function updatePrice($idProduct,$price){        
    	DB::table('00_inventory')
    	->where('prod_id',$idProduct)
    	->update(['inventory_price'=>$price]);    	
    }
    public function decrementBooking($prodId,$qty){
        $inventory=DB::table('00_inventory')
        ->decrement('booking_qty',$qty,['prod_id'=>$prodId]);
    }
    public function returnBookingQty($prodId,$qty,$warehouse,$shop){
        //echo "\nReturn booking qty prod:".$prodId." qty:".$qty." warehouse:".$warehouse." shop:".$shop;
        $inventoryCheck =Inventory::where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouse],
            ['organization_id',$shop]
        ])->first();
        $finalStock=$inventoryCheck->booking_qty - $qty;
        Inventory::where([
            ['prod_id',$inventoryCheck->prod_id],
            ['organization_id',$shop],
            ['warehouse_id',$warehouse]
        ])
        ->update([
            'booking_qty'=>$finalStock
        ]);
    }
    public function updateStockOrderTokped($orderId,$warehouseId,$shopId,$prodId,$orderQty){
        //echo "\nupdate stock start";
            // $prodOrder=app('App\Http\Controllers\OrderController')->getProductByOrderId($orderId);
            // dd(count($prodOrder));
        //---------------------------------------------------add transaction header
        $addTransactionHeaderSchema=array(
            'transaction_header_code'=>'ECM-TOKPED-'.$orderId,
            'organization_id'=>$shopId,
            'warehouse_id'=>$warehouseId,
            'transaction_date'=>Carbon::now()->format('Y-m-d 00:00:00'),
            'transaction_desc'=>'tokopedia',
            'transaction_type_id'=>3,
            'transaction_status'=>1,
            'active_flag'=>1,
        );
        $transactionHeaderId=app('App\Http\Controllers\TransactionController')->addTransactionHeader($addTransactionHeaderSchema);
        //echo "\nadd transaction header";
        // for($i=0;$i<count($prodOrder);$i++){
            $checkProduct=app('App\Http\Controllers\ProductController')->checkProduct($prodId);
            if($checkProduct->stockable_flag==1){
                $productStockable=$checkProduct->prod_id;
                // $qty=$prodOrder[$i]->quantity;
                $qty=$orderQty;
                $uomId=$checkProduct->uom_id;
            }else{
                $productStockable=$checkProduct->parent_prod_id;
                $parentProd=app('App\Http\Controllers\ProductController')->checkProduct($checkProduct->parent_prod_id);
                $qty=app('App\Http\Controllers\ProductController')->convertBookingStock($parentProd->uom_value,$parentProd->uom_id,$checkProduct->uom_value,$checkProduct->uom_id,$orderQty);
                // echo "\nparentProd: ".$parentProd->prod_id."|parentUomValue: ".$parentProd->uom_value."|parentUomId: ".$parentProd->uom_id."|checkProduct :".$checkProduct->prod_id."|checkProductUomValue: ".$checkProduct->uom_value."|checkProductUomId".$checkProduct->uom_id."|result: ".$qty;
                $uomId=$parentProd->uom_id;
            }
            //-----------------------------------------------add transaction detail
            echo "\ncheck inventory : prod ".$productStockable." shop: ".$shopId;
            $checkInventory=$this->checkInventory($productStockable,$warehouseId,$shopId);
            $addTransactionDetailSchema=array(
                'transaction_header_id'=>$transactionHeaderId,
                'prod_id'=>$productStockable,
                'price'=>$checkInventory->inventory_price,
                'stock'=>$qty,
                'uom_id'=>$uomId,
                'active_flag'=>1
            );
            app('App\Http\Controllers\TransactionController')->addTransactionDetail($addTransactionDetailSchema);
            //echo "\nadd transaction detail";
            //-------------------------------------------------------------add stock card
            $stockcardStock=-$qty;
            //echo "\nstockcardStock: ".$stockcardStock;
            $newTotalPrice=$stockcardStock*$checkInventory->inventory_price;
            //echo "\nnewTotalPriceStockCard: ".$newTotalPrice;
            $stockCardSchema=array(
                'stock_card_code'=>'ECM-TOKPED-'.$orderId.'-'.$productStockable,
                'organization_id'=>$shopId,
                'warehouse_id'=>$warehouseId,
                'prod_id'=>$productStockable,
                'stock'=>$stockcardStock,
                'transaction_type_id'=>3,
                'price'=>$checkInventory->inventory_price,
                'total_harga'=>$newTotalPrice,
                'active_flag'=>1
            );
            app('App\Http\Controllers\StockCardController')->addStockCard($stockCardSchema);
            //echo "\nadd stock card";
            //----------------------------------------------------------------------update stock
            $this->updateStockAndPrice($productStockable,$qty,$warehouseId,$shopId,$newTotalPrice);
            //echo "\nupdate stock";
        // }
    }
    public function updateStock($prodId,$qty,$warehouse,$shop){
        $inventoryCheck =Inventory::where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouse],
            ['organization_id',$shop]
        ])->first();
        $finalStock=$inventoryCheck->stock - $qty;
        Inventory::where([
            ['prod_id',$inventoryCheck->prod_id],
            ['organization_id',$shop],
            ['warehouse_id',$warehouse]
        ])
        ->update([
            'stock'=>$finalStock
        ]);
    }
    public function updateStockAndPrice($prodId,$qty,$warehouse,$shop,$newTotalPrice){
        echo "\nupdate stock prod:".$prodId;
        //echo "\nupdate stock & price";
        $inventoryCheck =Inventory::where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouse],
            ['organization_id',$shop]
        ])->first();
        $oldTotalPrice=$inventoryCheck->stock*$inventoryCheck->inventory_price;
        $finalStock=$inventoryCheck->stock - $qty;
        $finalgoodsArrivedQty=$inventoryCheck->goods_arrived-$qty;
        $grandTotalPrice=($newTotalPrice+$oldTotalPrice)/$finalStock;
        echo "\nGIT from :".$inventoryCheck->goods_arrived."-".$qty;
        echo "\nstock from ".$inventoryCheck->stock."-".$qty;
        //echo "\ngrandTotalPrice= ".$newTotalPrice."+".$oldTotalPrice."/".$finalStock."=".$grandTotalPrice;
        Inventory::where([
            ['prod_id',$inventoryCheck->prod_id],
            ['organization_id',$shop],
            ['warehouse_id',$warehouse]
        ])
        ->update([
            'stock'=>$finalStock,
            'inventory_price'=>$grandTotalPrice,
            'goods_arrived'=>$finalgoodsArrivedQty
        ]);
        echo "\nresult setok".$finalStock." , goods arrived:".$finalgoodsArrivedQty;
    }
    public function checkInventory($prodId,$warehouseId,$orgId){
        return Inventory::where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouseId],
            ['organization_id',$orgId]
        ])->first();
    }
    public function goodsInTransit($orderId,$shop,$action){
        $orderDetail=app('App\Http\Controllers\OrderTokopediaController')->getQtyOrderDetail($orderId);
        for($i=0;$i<count($orderDetail);$i++){
            $prod=app('App\Http\Controllers\ProductController')->checkProduct($orderDetail[$i]->prod_id);
            if($prod->stockable_flag==1){
                $prodInventory=$prod->prod_id;
                $qtyOrder=$orderDetail[$i]->quantity;
            }else{
                $prodInventory=$prod->parent_prod_id;
                $parent=app('App\Http\Controllers\ProductController')->checkProduct($prodInventory);
                $qtyOrder=app('App\Http\Controllers\ProductController')->convertBookingStock($parent->uom_value,$parent->uom_id,$prod->uom_value,$prod->uom_id,$orderDetail[$i]->quantity);
            }
            switch ($action) {
                case 'goods_in_transit':
                    $this->stockGoodsInTransit($prodInventory,$shop->warehouse_id,$shop->parent_organization_id,$qtyOrder);       
                    break;
                case 'goods_arrived':
                    $this->stockGoodsArrived($prodInventory,$shop->warehouse_id,$shop->parent_organization_id,$qtyOrder);       
                    break;
                case 'done':
                    $this->updateStockOrderTokped($orderId,$shop->warehouse_id,$shop->parent_organization_id,$orderDetail[$i]->prod_id,$orderDetail[$i]->quantity);       
                break;
                default:
                    break;
            }
        }
    }
    public function stockGoodsInTransit($prodId,$warehouseId,$orgId,$qty){
        $checkInventory=$this->checkInventory($prodId,$warehouseId,$orgId);
        Inventory::where('inventory_id',$checkInventory->inventory_id)
        ->update([
            'booking_qty'=>$checkInventory->booking_qty-$qty,
            'goods_in_transit'=>$checkInventory->goods_in_transit+$qty,
        ]);
    }
    public function stockGoodsArrived($prodId,$warehouseId,$orgId,$qty){
        $checkInventory=$this->checkInventory($prodId,$warehouseId,$orgId);
        Inventory::where('inventory_id',$checkInventory->inventory_id)
        ->update([
            'goods_in_transit'=>$checkInventory->goods_in_transit-$qty,
            'goods_arrived'=>$checkInventory->goods_arrived+$qty,
        ]);
    }
    public function orderMobilePotongStok($orderId,$orgId,$warehouseId){
        $orderDetail=app('App\Http\Controllers\OrderTokopediaController')->getQtyOrderDetail($orderId);
        for($i=0;$i<count($orderDetail);$i++){
            $prodQty=$this->checkProductStockable($orderDetail[$i]->prod_id,$orderDetail[$i]->quantity);
            $inventory=$this->checkInventory($prodQty['prodInventory'],$warehouseId,$orgId);
            $goodArrived=$inventory->goods_arrived-$prodQty['qty'];
            $stock=$inventory->stock-$prodQty['qty'];
            Inventory::where('inventory_id',$inventory->inventory_id)
            ->update([
                'goods_arrived'=>$goodArrived,
                'stock'=>$stock
            ]);
            $orderCode=app('App\Http\Controllers\OrderController')->getOrderCode($orderId)->order_code;
            $stockcardStock=0-$prodQty['qty'];
            $newTotalPrice=$stock*$inventory->inventory_price;
            $stockCardSchema=array(
                'stock_card_code'=>$orderCode,
                'organization_id'=>$orgId,
                'warehouse_id'=>$warehouseId,
                'prod_id'=>$prodQty['prodInventory'],
                'stock'=>$stockcardStock,
                'transaction_type_id'=>3,
                'price'=>$inventory->inventory_price,
                'total_harga'=>$newTotalPrice,
                'active_flag'=>1
            );
            app('App\Http\Controllers\StockCardController')->addStockCard($stockCardSchema);
            //pushstock
            $org=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
            if($orgId==$org->parent_organization_id&&$warehouseId==$org->warehouse_id){
                $coldStock=app('App\Http\Controllers\WarehouseController')->getStockColdStorage($orgId,$warehouseId,$prodQty['prodInventory']);
                $decrementStock=$inventory->booking_qty+$inventory->goods_in_transit+$goodArrived;
                $showStock=$stock-$decrementStock;
                $newStock=floor($coldStock)+$showStock;
                app('App\Http\Controllers\IntegrationECommerceController')->pushStockEcommerce($prodQty['prodInventory'],$newStock);
            }
        }
    }
    public function checkProductStockable($prodId,$quantity){
        $checkProduct=app('App\Http\Controllers\ProductController')->checkProduct($prodId);
        if($checkProduct->stockable_flag==1){
            $productStockable=$checkProduct->prod_id;
            $qty=$quantity;
        }else{
            $productStockable=$checkProduct->parent_prod_id;
            $prodParent=app('App\Http\Controllers\ProductController')->checkProduct($productStockable);
            $qty=app('App\Http\Controllers\ProductController')->convertBookingStock($prodParent->uom_value,$prodParent->uom_id,$checkProduct->uom_value,$checkProduct->uom_id,$quantity);
        }
        $output=array(
            'prodInventory'=>$productStockable,
            'qty'=>$qty
        );
        return $output;
    }
    public function checkInventoryByInventoryId($inventoryId){
        $data=Inventory::where('inventory_id',$inventoryId)
        ->select('organization_id','warehouse_id','prod_id','booking_qty','goods_in_transit','goods_arrived','stock')
        ->first();
        return $data;
    }
    public function pushStockBookingMobile($inventoryId){
        $gudang1=$this->checkInventoryByInventoryId($inventoryId);
        $org=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
        if($gudang1->organization_id==$org->parent_organization_id&&$gudang1->warehouse_id==$org->warehouse_id){
            $stockGudang1=floor($gudang1->stock-$gudang1->booking_qty-$gudang1->goods_in_transit-$gudang1->goods_arrived);
            $coldStock=app('App\Http\Controllers\WarehouseController')->getStockColdStorage($gudang1->organization_id,$gudang1->warehouse_id,$gudang1->prod_id);
            $newStock=$coldStock+$stockGudang1;
                app('App\Http\Controllers\IntegrationECommerceController')->pushStockEcommerce($gudang1->prod_id,$newStock);
        }
    }
}
