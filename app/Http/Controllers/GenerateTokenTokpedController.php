<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class GenerateTokenTokpedController extends Controller
{
    public function generateTokenTokped(){
        $credential=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();                            
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);            
        } catch (ConnectionException $e) {
            $token=null;            
        }         
        return $token;
    }
}
