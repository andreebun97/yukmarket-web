<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class VoucherController extends Controller
{
    public function __construct(){
        $this->middleware('check-token');
    }

    // USING MIDDLEWARE ENDPOINT START ----------------------------------------------------------------------------------------
    public function getVoucher(Request $request){
        $voucher = DB::table('00_vouchers')->select('00_vouchers.*',DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
            ->whereNotIn('00_vouchers.voucher_id', function($query){
            $query->select('00_product_voucher.voucher_id')->from('00_product_voucher');
        });

        if($request->voucher_code != null && $request->voucher_code != ""){
            $voucher->where('voucher_code',strtoupper($request->voucher_code));
        }else{
            $voucher->where('active_flag',1)->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('starts_at'),DB::raw('expires_at')]);
        }
        $voucher = $voucher->get();
        $subtotal = $request->input('subtotal');
        $voucher_array = array();
        foreach($voucher as $data){
            $voucher_amount = 0;
            if($data->is_fixed == 'Y'){
                $voucher_amount = $data->amount;
            }else{
                $voucher_amount = ($subtotal * $data->amount / 100);
                if($voucher_amount > $data->max_value_price){
                    $voucher_amount = $data->max_value_price;
                }
            }
            $obj = array(
                'voucher_id' => $data->voucher_id,
                'voucher_code' => $data->voucher_code,
                'voucher_name' => $data->voucher_name,
                'amount' => $voucher_amount,
                'voucher_value' => $data->amount,
                'is_fixed' => $data->is_fixed,
                'max_value_price' => $data->max_value_price,
                'min_price_requirement' => $data->min_price_requirement,
                'starts_at' => $data->starts_at,
                'expires_at' => $data->expires_at,
                //'starts_at' => date(DATE_RFC3339_EXTENDED, strtotime($data->starts_at)),
                //'expires_at' => date(DATE_RFC3339_EXTENDED, strtotime($data->expires_at)),
                'flag' => ($data->expired == 1 || $data->expired == 2 || $subtotal < $data->min_price_requirement) ? false : true
            );
            array_push($voucher_array, $obj);
        }

        usort($voucher_array, function($a,$b){
            return $b['flag'] - $a['flag'];
        });

        if(!$voucher){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            $message = "OK!";
            $flag = true;
            if($request->voucher_code != null && $request->voucher_code != ""){
                if(count($voucher) > 0){
                    if($voucher[0]->expired == 2){
                        $message = "Sorry, the voucher cannot be used!";
                        $voucher_array = array();
                    }else if($voucher[0]->expired == 1){
                        $message = "Sorry, the voucher has been expired!";
                        $voucher_array = array();
                    }else{
                        if($voucher[0]->max_uses_user >= $voucher[0]->max_uses){
                            $message = "Sorry, you cannot use this voucher because the voucher has been sold out!";
                            $voucher_array = array();
                        }else if($subtotal < $voucher[0]->min_price_requirement){
                            $message = "Sorry, you cannot use this voucher because you don't exceed the limit!";
                            $voucher_array = array();
                        }
                    }
                }else{
                    $message = "Voucher not found!";
                    $flag = false;
                    $voucher_array = array();
                }
            }
            return response()->json(['isSuccess' => $flag, 'message' => $message, 'data' => $voucher_array], 200);
        }
    }
    public function addVoucherHeader($data){
        return DB::table('00_voucher_ecommerce_header')
        ->insertGetId($data);
    }
    // USING MIDDLEWARE ENDPOINT END ----------------------------------------------------------------------------------------

    // ORIGINAL START ----------------------------------------------------------------------------------------
    // public function getVoucher(Request $request){
    //     $voucher = DB::table('00_vouchers')->select('00_vouchers.*',DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
    //         ->whereNotIn('00_vouchers.voucher_id', function($query){
    //         $query->select('00_product_voucher.voucher_id')->from('00_product_voucher');
    //     });

    //     if($request->voucher_code != null && $request->voucher_code != ""){
    //         $voucher->where('voucher_code',strtoupper($request->voucher_code));
    //     }else{
    //         $voucher->where('active_flag',1)->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('starts_at'),DB::raw('expires_at')]);
    //     }
    //     $voucher = $voucher->get();
    //     $subtotal = $request->input('subtotal');
    //     $voucher_array = array();
    //     foreach($voucher as $data){
    //         $voucher_amount = 0;
    //         if($data->is_fixed == 'Y'){
    //             $voucher_amount = $data->amount;
    //         }else{
    //             $voucher_amount = ($subtotal * $data->amount / 100);
    //             if($voucher_amount > $data->max_value_price){
    //                 $voucher_amount = $data->max_value_price;
    //             }
    //         }
    //         $obj = array(
    //             'voucher_id' => $data->voucher_id,
    //             'voucher_code' => $data->voucher_code,
    //             'voucher_name' => $data->voucher_name,
    //             'amount' => $voucher_amount,
    //             'voucher_value' => $data->amount,
    //             'is_fixed' => $data->is_fixed,
    //             'max_value_price' => $data->max_value_price,
    //             'min_price_requirement' => $data->min_price_requirement,
    //             'starts_at' => $data->starts_at,
    //             'expires_at' => $data->expires_at,
    //             'flag' => ($data->expired == 1 || $data->expired == 2 || $subtotal < $data->min_price_requirement) ? false : true
    //         );
    //         array_push($voucher_array, $obj);
    //     }

    //     usort($voucher_array, function($a,$b){
    //         return $b['flag'] - $a['flag'];
    //     });

    //     if(!$voucher){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    //     }else{
    //         $message = "OK!";
    //         $flag = true;
    //         if($request->voucher_code != null && $request->voucher_code != ""){
    //             if(count($voucher) > 0){
    //                 if($voucher[0]->expired == 2){
    //                     $message = "Sorry, the voucher cannot be used!";
    //                     $voucher_array = array();
    //                 }else if($voucher[0]->expired == 1){
    //                     $message = "Sorry, the voucher has been expired!";
    //                     $voucher_array = array();
    //                 }else{
    //                     if($voucher[0]->max_uses_user >= $voucher[0]->max_uses){
    //                         $message = "Sorry, you cannot use this voucher because the voucher has been sold out!";
    //                         $voucher_array = array();
    //                     }else if($subtotal < $voucher[0]->min_price_requirement){
    //                         $message = "Sorry, you cannot use this voucher because you don't exceed the limit!";
    //                         $voucher_array = array();
    //                     }
    //                 }
    //             }else{
    //                 $message = "Voucher not found!";
    //                 $flag = false;
    //                 $voucher_array = array();
    //             }
    //         }
    //         return response()->json(['isSuccess' => $flag, 'message' => $message, 'data' => $voucher_array], 200);
    //     }
    // }
    // ORIGINAL END ----------------------------------------------------------------------------------------
}
