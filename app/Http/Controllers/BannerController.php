<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BannerController extends Controller {

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------    
    public function getBanner() {
        
        $q = DB::table('99_banner')
            ->select('banner_id', 'banner_name', 'banner_image', DB::raw('banner_desc AS banner_description'), 'active_flag')
            ->where('active_flag', '=', '1')
            ->get();

        $banner_array = array();
        for ($i = 0; $i < count($q); $i++) {
            $obj = array(
                'banner_id' => $q[$i]->banner_id,
                'banner_name' => $q[$i]->banner_name,
                'banner_image' => asset($q[$i]->banner_image),
                'banner_description' => $q[$i]->banner_description,
                'active_flag' => $q[$i]->active_flag
            );
            array_push($banner_array, $obj);
        }

        if (!$q) {
            return response()->json(['isSuccess' => false, 'message' => 'Data not found', 'data'=>[]], 500);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $banner_array], 200);
        }
    }

    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------
    // ORIGINAL START ---------------------------------------------------------------------
    // public function getBanner(){        
    //     $banner = DB::table('99_banner')
    //             ->where('active_flag',1)
    //             ->get();
    //     $banner_array = array();
    //     foreach($banner as $data){
    //         $obj = array(
    //             'banner_id' => $data->banner_id,
    //             'banner_name' => $data->banner_name,
    //             'banner_image' => asset($data->banner_image),
    //             'banner_description' => $data->banner_desc,
    //             'active_flag' => $data->active_flag
    //         );
    //         array_push($banner_array, $obj);
    //     }
    //     if(!$banner){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $banner_array], 200);
    //     }
    // }

    public function getBannerById($id) {
        $banner = DB::table('99_banner')
                ->where('active_flag', 1)
                ->where('banner_id', $id)
                ->get();

        $banner_array = array();

        foreach ($banner as $data) {
            $obj = array(
                'banner_id' => $data->banner_id,
                'banner_name' => $data->banner_name,
                'banner_image' => asset($data->banner_image),
                'banner_description' => $data->banner_desc,
                'active_flag' => $data->active_flag
            );
            array_push($banner_array, $obj);
        }

        if (!$banner) {
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $banner_array], 200);
        }
    }

    // ORIGINAL END ---------------------------------------------------------------------
}
