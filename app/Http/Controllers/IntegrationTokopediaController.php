<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Events\GenerateRelationProductTokped;
use App\Console\Commands\IntegrationTokped;
use App\Helpers\GeneralFunction;

class IntegrationTokopediaController extends Controller
{					        
	//---------------TOKOPEDIA INTEGRATION----------------------------------//
	public function testSchedule($code,$message){				
		if(IntegrationTokped::$debugMode=='on'){//-----------debug mode			
			DB::table('otp_testing_table')
			->insert([
				'otp_code'=>$code,
				'debug'=>$message
			]);
			$a=DB::table('otp_testing_table')
			// ->select('created_date','otp_id')
			// ->get();
			->where([
				['created_date','<',date(now()->subMinute(7))],
				['otp_code','=',$code]])
			// ->where('otp_code',1)
			->delete();
			// ->get();
			// ->count()
		}		
		return $message;
	}
	public function getStatusScanTokopedia(){
		$status = DB::table('99_global_parameter')
		->select('global_parameter_value','global_parameter_desc')
		->where('global_parameter_name','=','tokopedia')
		->first();
		return $status;
	}
	public function sync(){		
		//-------------------call event untuk sync relation product		
		$shopList =$this->getShopList();		
		$categoryList =$this->getCategoryList();  				
		//----------------------------------------------------------scan shop -> warehouse		
		echo $this->testSchedule(1,'GenerateRelationProductTokped start');		
		event(new GenerateRelationProductTokped($shopList,$categoryList));	
        dd('end category');					
		echo $this->testSchedule(1,'GenerateRelationProductTokped end');				
		echo $this->testSchedule(1,'scanProduct start');		
    	$page=1;
		do {			
			echo $this->testSchedule(1,"scan product looping page:".$page);		
    		$scanProduct =$this->scanProduct($page);
    		$page++;
    	} while ( $scanProduct!='end');    	    	
    	echo $this->testSchedule(1,'scanProduct end');		
    }   
    public function getCategoryList(){//----------------------------------------get daftar category tokopedia			
		$timeOutCounter=0;
    	$url = config('global.tokopedia_integrator_url').'/getCategories';    	
    	for($i=0;$i<=50;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$categoryList =json_decode(file_get_contents($url), true);      	    	
    		if($categoryList!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $categoryList;
    	}       	    	
	} 	
	public function getShopList(){//---------------------------------------------get daftar shop tokopedia				
		$timeOutCounter=0;
    	$url = config('global.tokopedia_integrator_url').'/getAllShop';    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$shopList =json_decode(file_get_contents($url), true);      	    	
    		if($shopList!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		echo $this->testSchedule(1,'getAllShop timeout');		
    		return null;
    	}else{
    		return $shopList;
    	}       	    	
	}
	public function scanProduct($page){//---------------------------------------------scan product yang ada di tokopedia
		//----------------------------------------------------------------get product from yukmarket db
		//$products = app('App\Http\Controllers\ProductController')->getAllProductList();
		$tokpedProducts = $this->getAllProductList($page);//-------------------------------data product dari tokopedia		
		if($tokpedProducts==null){//jika timeout			
			echo $this->testSchedule(1,'timeout getAllProductList');		
			return 'end';
		}else{
			if(count($tokpedProducts['data'])==0){//---------------------------------------------jika tidak ada data				
				echo $this->testSchedule(1,'tidak ada data tokpedProducts');	
				return 'end';
			}else{//jika ada data				
				for($i=0;$i<count($tokpedProducts['data']);$i++){
					//---------------------------------------------check apakah data sudah ada atau belum					
					echo $this->testSchedule(1,'compare data product');	
					$checkProduct =app('App\Http\Controllers\ProductController')->checkProduct($tokpedProducts['data'][$i]['product_id']);
					if($checkProduct==null){//---------------------------------------------jika data belum ada
						//---------------------------------------------check sku product						
						echo $this->testSchedule(1,'data belum ada, check sku produk');	
						$skuId=$this->scanProductSKU($tokpedProducts['data'][$i]);											//---------------------------------------------insert ke db yukmarket							
						$addProduct = array(
							'prod_id'=>$tokpedProducts['data'][$i]['product_id'],
							'prod_name'=>$tokpedProducts['data'][$i]['name'],
							'prod_desc'=>$tokpedProducts['data'][$i]['desc'],
							'product_sku_id'=>$skuId->product_sku_id,
                            'uom_value'=>1,
                            'uom_id'=>12,
							'active_flag'=>1 
						);												
						echo $this->testSchedule(1,'add product');	
						app('App\Http\Controllers\ProductController')->addProductFromTokped($addProduct);
						//---------------------------------------------insert stok & price ke inventory
						$addInventory = array(
							'prod_id'=>$tokpedProducts['data'][$i]['product_id'],
							'warehouse_id'=>$tokpedProducts['data'][$i]['shop_id'],
							'stock'=>$tokpedProducts['data'][$i]['stock'],
							'inventory_price'=>$tokpedProducts['data'][$i]['price'],
							'organization_id'=>28
						);						
						echo $this->testSchedule(1,'add inventory');	
						app('App\Http\Controllers\InventoryController')->addInventoryFromTokoped($addInventory);
					}else{//----------------------------------------jika data product sudah ada
						//--------------------------cek apakah data sku update atau tidak
						echo $this->testSchedule(1,'| data sudah ada |');	
						$this->scanProductSKU($tokpedProducts['data'][$i]);				
						//-------------------------------jika nama atau desc product berubah maka update						
						if($checkProduct->prod_name!=$tokpedProducts['data'][$i]['name']||$checkProduct->prod_desc!=$tokpedProducts['data'][$i]['desc']){
							$reqUpdate = array(
								'prod_id'=>$checkProduct->prod_id,
								'prod_name'=>$tokpedProducts['data'][$i]['name'],
								'prod_desc'=>$tokpedProducts['data'][$i]['desc'],
							);
							app('App\Http\Controllers\ProductController')->updateProduct($reqUpdate);
						}
						//-----------------------------------------------------check product price
						$prodPrice=app('App\Http\Controllers\InventoryController')->checkProductPrice($tokpedProducts['data'][$i]['product_id']);						
						if($prodPrice->inventory_price!=$tokpedProducts['data'][$i]['price']){
							app('App\Http\Controllers\InventoryController')->updatePrice($tokpedProducts['data'][$i]['product_id'],$tokpedProducts['data'][$i]['price']);
						}
					}
                    //-------------------------check product warehouse
                    $this->scanProductWarehouse($tokpedProducts['data'][$i]['product_id'],$tokpedProducts['data'][$i]['shop_id']);
				}					
			}			
		}	
		// dd('end');		
		return 'next';
	}	
	public function updateStatusScanTokopedia($status){				
		$update = DB::table('99_global_parameter')
		->where('global_parameter_name','tokopedia')
		->update(['global_parameter_value'=>$status =="on" ? "off" : "on"]);
		return $update;
	}
	public function enableScanTokopedia(){		
		$prevStatus = $this->getStatusScanTokopedia();		
		$updateStatus = $this->updateStatusScanTokopedia($prevStatus->global_parameter_value);
		$currentStatus = $this->getStatusScanTokopedia();
		if(!$updateStatus){
			return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
		}else{
			return response()->json(['isSuccess' => true, 'message' => 'success changing scan tokopedia from '.$prevStatus->global_parameter_value.' to '.$currentStatus->global_parameter_value], 200);
		}
	}
    public function getAllProductList($page){//---------------------------------------------get api productlist tokopedia
    	$timeOutCounter=0;
    	$url = config('global.tokopedia_integrator_url').'/getAllProductList?itemPerPage=50&page='.$page;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$dataProducts =json_decode(file_get_contents($url), true);      	    	
    		if($dataProducts!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $dataProducts;
    	}       	    	
    }
    public function scanProductSKU($product){
    	$skuId = app('App\Http\Controllers\ProductController')->checkProductSKU($product['sku']);	    	
    	if($skuId==null){//--------------------------------------------jika sku belum ada maka insert sku
			app('App\Http\Controllers\ProductController')->addProductSKU($product['sku']);
			$skuId = app('App\Http\Controllers\ProductController')->checkProductSKU($product['sku']);
			$checkProduct=app('App\Http\Controllers\ProductController')->checkProduct($product['product_id']);
			if($checkProduct!=null){//update product sku id
				app('App\Http\Controllers\ProductController')->updateProductSKUOnly($product['product_id'],$skuId->product_sku_id);
			}	 
		}
		return $skuId;
    }
    public function validateUpdateStockTokped($shopId,$prodId,$stock,$price,$updateStatus){    
    	$shop=app('App\Http\Controllers\WarehouseController')->checkWarehouse($shopId);
    	if($shop!=null){
    		$product =app('App\Http\Controllers\ProductController')->checkProduct($prodId);
    		if($product!=null){
    			$this->pushUpdateStockTokped($shopId,$prodId,$stock);    			
    			if($updateStatus=='update_price'){
	    			$this->pushUpdatePriceTokped($shopId,$prodId,$price);    				
	    		}
    		}    		 
    	}
    }
    public function pushUpdateStockTokped($shopId,$prodId,$stock){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$prodId.'&productStocks='.$stock.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$updateStok = Http::post($url);
    		if($updateStok!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $updateStok;
    	}
    }
    public function pushUpdatePriceTokped($shopId,$prodId,$price){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/updateProductPrice?productIdsOrSkus='.$prodId.'&productPrices='.$price.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$updatePrice = Http::post($url);
    		if($updatePrice!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $updatePrice;
    	}
    }
    public function activeOrInactiveProduct($prodId,$shopId,$action){    	
    	if($action=='in_active'){
    		$this->setInActiveProduct($prodId,$shopId);
    	}elseif($action=='active'){
    		$this->setActiveProduct($prodId,$shopId);
    	}
    }
    public function setActiveProduct($prodId,$shopId){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/setActiveProduct?productId='.$prodId.'&shopId='.$shopId;    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$active = Http::post($url);
    		if($active!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $active;
    	}
    }
    public function setInActiveProduct($prodId,$shopId){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/setInactiveProduct?productId='.$prodId.'&shopId='.$shopId;    	    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$inActive = Http::post($url);
    		if($inActive!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $inActive;
    	}
    }
    public function deleteProduct($prodId,$shopId){
    	$timeOutCounter=0;    	
    	$url = config('global.tokopedia_integrator_url').'/deleteProduct?productId='.$prodId.'&shopId='.$shopId;    	    	
    	for($i=0;$i<=30;$i++){//---------------------------------------------hitung timeout
    		$timeOutCounter=$i;
    		$inActive = Http::post($url);
    		if($inActive!=null){//---------------------------------------------jika data berhasil di get
    			break;
    		}
    	}
    	// dd(count($dataProducts['data']));
    	if($timeOutCounter==30){//---------------------------------------------jika timeout
    		return null;
    	}else{
    		return $inActive;
    	}
    }
    public function scanProductWarehouse($prodId,$warehouseId){
        //---------------------------------------check product warehouse sudah ada atau belum
        $productWarehouse=app('App\Http\Controllers\ProductWarehouseController')->checkProductWarehouse($prodId,$warehouseId);
        if($productWarehouse==null){//------------------jika belum ada maka insert
            app('App\Http\Controllers\ProductWarehouseController')->addProductWarehouse($prodId,$warehouseId);
        }
    }
    //---------------TOKOPEDIA INTEGRATION----------------------------------//    


    public function changeImage(){
        $res_arr = DB::table('00_product')
                ->select('prod_id','prod_image')
                ->whereNotNull('prod_image')
                ->where('prod_image', 'not like', "%img/uploads/products%")
                ->get();
        foreach ($res_arr as $res) {
            $GF = new GeneralFunction;
            $new_img = $GF->imageFromUrl($res->prod_image);
            if (isset($new_img->image)) {
                try {
                    DB::table('00_product')
                    ->where('prod_id', $res->prod_id)
                    ->update( ['prod_image'=> $new_img->image ]);
                    echo "<pre>";
                    print_r('Success Update Image product id : '.$res->prod_id);
                    echo "</pre>";
                }catch (\Exception $e){
                    echo "<pre>";
                    print_r('Failed Update Image ,'.$e);
                    echo "</pre>";
                }
            } else {
                echo "<Pre>";
                print_r('Failed Update Image ,'.$new_img->message);
                echo "</Pre>";
            }
        }

        // TEST ONE PRODUCT 
        // $res = DB::table('00_product')
        //         ->select('prod_id','prod_image')
        //         ->whereNotNull('prod_image')
        //         ->where('prod_image', 'not like', "%img/uploads/products%")
        //         ->first();
        // $GF = new GeneralFunction;
        // $new_img = $GF->imageFromUrl($res->prod_image);
        // try {
        //     DB::table('00_product')
        //     ->where('prod_id', $res->prod_id)
        //     ->update( ['prod_image'=> $new_img->image ]);
        //     echo "<pre>";
        //     print_r('Success Update Image product id : '.$res->prod_id);
        //     echo "</pre>";
        // }catch (\Exception $e){
        //     echo "<pre>";
        //     print_r('Failed Update Image ,'.$new_img->message);
        //     echo "</pre>";
        // }
    }
}
