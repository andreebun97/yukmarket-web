<?php

namespace App\Http\Controllers;

use App\AuthComp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;
use Illuminate\Support\Facades\Http;
use App\Helpers\Currency;

class ProfileController extends Controller
{
    // public function updateProfile(Request $request)
    // {
        // 	// $img_decode = base64_decode($request->get('photo'));
        //  //    $img_ext = str_replace("image/", ".", getimagesizefromstring($img_decode)['mime']);
        //     // $ext = $request->get('kk_photo')->getClientOriginalExtension();
        //     // $strname = str_replace(" ", "_", $request->get('ktp_nama'));
        //     // $filename = uniqid().$img_ext;
        //     // $url = 'http://35.186.152.189/assets/buruh/ktp/'.$filename;
        //     // $destinationPath = public_path("images/ktp/");
        //     // $moveFile = $request->file('kk_photo')->move($destinationPath, $filename);
        //     // file_put_contents($destinationPath.$filename, $img_decode);
        //     // $upload = Storage::disk('sftp')->put('buruh/ktp/'.$filename, $img_decode);

        //     $update = DB::table('00_customer')
        //     ->where('cust_id', $request->cust_id)
        //     ->update([
        //     	'cust_name' => $request->name,
        //     	'cust_email' => $request->email,
        //     	'cust_phone_no' => $request->phone_number,
        //     	'kelurahan_desa_id' => $request->kelurahan_desa_id,
        //     	'kecamatan_id' => $request->kecamatan_id,
        //     	'kabupaten_kota_id' => $request->kabupaten_kota_id,
        //     	'provinsi_id' => $request->provinsi_id,
        //     	'address_detail' => $request->alamat_lengkap,
        //     	'identify_type' => $request->tipe_identitas,
        //     	'identify_no' => $request->no_identitas,
        //     	// 'photo_profile' => $request->
        //     ]);

        //     if(!$update){
        //     	return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
        //     }else{
        //     	return response()->json(['isSuccess' => true, 'message' => 'Profile successfully changed'], 200);
        //     }
    // }

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function updatePhoto(Request $request)
    {
            $validator = Validator::make($request->all(),[
                'profile_picture' => ['required','mimes:jpg,jpeg,png,pneg,svg']
            ]);
            
            if($validator->passes()){
                $folder_name = 'img/uploads/profile_picture/';
                $file = $request->file('profile_picture');
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid().'.'.$ext;

                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
                
            if($request->get('is_agent') == 0){
                $customer = DB::table('00_customer')->where('customer_id', $request->input('user_id'))
                ->first();
                if($customer != null && $customer->customer_image != null){
                    File::delete($customer->customer_image);
                }
                $update = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->update([
                    'customer_image' => $folder_name.$name
                ]);
            }else{

                $agent = DB::table('98_user')->where('user_id', $request->input('user_id'))
                ->first();
                if($agent != null && $agent->user_image != null){
                    File::delete($agent->user_image);
                }
                $update = DB::table('98_user')->where('user_id', $request->input('user_id'))->update([
                    'user_image' => $folder_name.$name
                ]);
            }
                return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully changed'], 200);
            
            }else{
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            }
            
            // if($wrong_flag == 1){
            //     return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            // }else{
            //     return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully changed'], 200);
            // }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function updatePhoto(Request $request)
    // {
        //     $validator = Validator::make($request->all(),[
        //         'profile_picture' => ['required','mimes:jpg,jpeg,png,pneg,svg']
        //     ]);
            
        //     if($validator->passes()){
        //         $folder_name = 'img/uploads/profile_picture/';
        //         $file = $request->file('profile_picture');
        //         $ext = $file == null ? '' : $file->getClientOriginalExtension();
        //         $name = uniqid().'.'.$ext;

        //         if (!file_exists($folder_name)) {
        //             mkdir($folder_name, 777, true);
        //         }
        //         ini_set('memory_limit', '256M');
        //         Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
                
        //     if($request->get('is_agent') == 0){
        //         $customer = DB::table('00_customer')->where('customer_id', $request->input('user_id'))
        //         ->first();
        //         if($customer != null && $customer->customer_image != null){
        //             File::delete($customer->customer_image);
        //         }
        //         $update = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->update([
        //             'customer_image' => $folder_name.$name
        //         ]);
        //     }else{

        //         $agent = DB::table('98_user')->where('user_id', $request->input('user_id'))
        //         ->first();
        //         if($agent != null && $agent->user_image != null){
        //             File::delete($agent->user_image);
        //         }
        //         $update = DB::table('98_user')->where('user_id', $request->input('user_id'))->update([
        //             'user_image' => $folder_name.$name
        //         ]);
        //     }
        //         return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully changed'], 200);
            
        //     }else{
        //         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        //     }
            
        //     // if($wrong_flag == 1){
        //     //     return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        //     // }else{
        //     //     return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully changed'], 200);
        //     // }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function deletePhoto(Request $request)
    {

        if($request->get('is_agent') == 0){
            $customer = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->first();
            if($customer !== null && $customer->customer_image !== null){
                File::delete($customer->customer_image);
            }
            $update = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->update([
                'customer_image' => null
            ]);
        }else{
            $agent = DB::table('98_user')->where('user_id', $request->input('user_id'))->first();
            if($agent !== null && $agent->user_image !== null){
                File::delete($agent->user_image);
            }
            $update = DB::table('98_user')->where('user_id', $request->input('user_id'))->update([
                'user_image' => null
            ]);
        }
            
            if(!$update){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully deleted'], 200);
            }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function deletePhoto(Request $request)
    // {

        // if($request->get('is_agent') == 0){
        //     $customer = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->first();
        //     if($customer !== null && $customer->customer_image !== null){
        //         File::delete($customer->customer_image);
        //     }
        //     $update = DB::table('00_customer')->where('customer_id', $request->input('user_id'))->update([
        //         'customer_image' => null
        //     ]);
        // }else{
        //     $agent = DB::table('98_user')->where('user_id', $request->input('user_id'))->first();
        //     if($agent !== null && $agent->user_image !== null){
        //         File::delete($agent->user_image);
        //     }
        //     $update = DB::table('98_user')->where('user_id', $request->input('user_id'))->update([
        //         'user_image' => null
        //     ]);
        // }
            
        //     if(!$update){
        //         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        //     }else{
        //         return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully deleted'], 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    public function getAddress($id, Request $request)
    {
        if($request->get('is_agent') == 0){         
        	$q = DB::table('00_address AS a')
        	->leftJoin('00_kelurahan_desa AS b', 'b.kelurahan_desa_id', '=', 'a.kelurahan_desa_id')
            ->leftJoin('00_kecamatan AS c', 'c.kecamatan_id', '=', 'b.kecamatan_id')
            ->leftJoin('00_kabupaten_kota AS d', 'd.kabupaten_kota_id', '=', 'c.kabupaten_kota_id')
            ->leftJoin('00_provinsi AS e', 'e.provinsi_id', '=', 'd.provinsi_id')
        	->select('a.address_id', 'a.address_info', 'a.address_name', 'a.address_detail', 'a.contact_person', 'a.phone_number', 'a.gps_point', 'b.kelurahan_desa_name', 'c.kecamatan_name', 'd.kabupaten_kota_name', 'e.provinsi_name', 'a.isMain')
            ->where('a.customer_id', $id)
            ->where('a.active_flag', 1)
        	->get();

        }else{

            $q = DB::table('00_address AS a')
            ->leftJoin('00_kelurahan_desa AS b', 'b.kelurahan_desa_id', '=', 'a.kelurahan_desa_id')
            ->leftJoin('00_kecamatan AS c', 'c.kecamatan_id', '=', 'b.kecamatan_id')
            ->leftJoin('00_kabupaten_kota AS d', 'd.kabupaten_kota_id', '=', 'c.kabupaten_kota_id')
            ->leftJoin('00_provinsi AS e', 'e.provinsi_id', '=', 'd.provinsi_id')
            ->select('a.address_id', 'a.address_info', 'a.address_name', 'a.address_detail', 'a.contact_person', 'a.phone_number', 'a.gps_point', 'b.kelurahan_desa_name', 'c.kecamatan_name', 'd.kabupaten_kota_name', 'e.provinsi_name', 'a.isMain')
            ->where('a.user_id', $id)
            ->where('a.active_flag', 1)
            ->get();

        }
        
    	if(!$q){
    		return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    	}else{
    		return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
    	}
    }

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function setMainAddress(Request $request)
    {
            $check = DB::table('00_address')
            ->select('address_id')
            ->where([
                ['customer_id', $request->user_id],
                ['isMain', 1]
            ])
            ->orWhere([
                ['user_id', $request->user_id],
                ['isMain', 1]     
            ])
            ->first();

            if(!$check){
                $update = DB::table('00_address')
                ->where('address_id', $request->address_id)
                ->update(['isMain' => 1]);

                if(!$update){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
                }else{
                    return response()->json(['isSuccess' => true, 'message' => 'Main Address successfully set'], 200);
                }
            }else{
                if($check->address_id == $request->address_id){
                    return response()->json(['isSuccess' => false, 'message' => 'Address already set as Main'], 200);
                }else{
                    $resetMain = DB::table('00_address')
                    ->where('address_id', $check->address_id)
                    ->update(['isMain' => 0]);

                    $update = DB::table('00_address')
                    ->where('address_id', $request->address_id)
                    ->update(['isMain' => 1]);

                    if(!$resetMain || !$update){
                        return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
                    }else{
                        return response()->json(['isSuccess' => true, 'message' => 'Main Address successfully set'], 200);
                    }
                }
            }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function setMainAddress(Request $request)
    // {
        //     $check = DB::table('00_address')
        //     ->select('address_id')
        //     ->where([
        //         ['customer_id', $request->user_id],
        //         ['isMain', 1]
        //     ])
        //     ->orWhere([
        //         ['user_id', $request->user_id],
        //         ['isMain', 1]     
        //     ])
        //     ->first();

        //     if(!$check){
        //         $update = DB::table('00_address')
        //         ->where('address_id', $request->address_id)
        //         ->update(['isMain' => 1]);

        //         if(!$update){
        //             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        //         }else{
        //             return response()->json(['isSuccess' => true, 'message' => 'Main Address successfully set'], 200);
        //         }
        //     }else{
        //         if($check->address_id == $request->address_id){
        //             return response()->json(['isSuccess' => false, 'message' => 'Address already set as Main'], 200);
        //         }else{
        //             $resetMain = DB::table('00_address')
        //             ->where('address_id', $check->address_id)
        //             ->update(['isMain' => 0]);

        //             $update = DB::table('00_address')
        //             ->where('address_id', $request->address_id)
        //             ->update(['isMain' => 1]);

        //             if(!$resetMain || !$update){
        //                 return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        //             }else{
        //                 return response()->json(['isSuccess' => true, 'message' => 'Main Address successfully set'], 200);
        //             }
        //         }
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT  START ---------------------------------------------------------------------
    public function getDetailAddress($id)
    {
            $q = DB::table('00_address')
            ->select('address_id', 'address_name', 'address_detail', 'contact_person', 'phone_number', 'gps_point', '00_provinsi.provinsi_id', '00_provinsi.provinsi_name', '00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name', '00_kecamatan.kecamatan_id', '00_kecamatan.kecamatan_name', '00_kelurahan_desa.kelurahan_desa_id', '00_kelurahan_desa.kelurahan_desa_name', 'address_info', 'kode_pos')
            ->join('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
            ->join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
            ->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
            ->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
            ->where('address_id', $id)
            ->first();

            if(!$q){
                return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
            }
    }
    // USING MIDDLEWARE ENDPOINT  END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function getDetailAddress($id)
    // {
        // 	$q = DB::table('00_address')
        //     ->select('address_id', 'address_name', 'address_detail', 'contact_person', 'phone_number', 'gps_point', '00_provinsi.provinsi_id', '00_provinsi.provinsi_name', '00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name', '00_kecamatan.kecamatan_id', '00_kecamatan.kecamatan_name', '00_kelurahan_desa.kelurahan_desa_id', '00_kelurahan_desa.kelurahan_desa_name', 'address_info', 'kode_pos')
        //     ->join('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
        //     ->join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
        //     ->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
        //     ->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
        // 	->where('address_id', $id)
        // 	->first();

        // 	if(!$q){
        // 		return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        // 	}else{
        // 		return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------
    public function addAddress(Request $request)
    {
        if($request->get('is_agent') == 0){
            $add = DB::table('00_address')
            ->insert([
                'customer_id' => $request->user_id,
                'address_name' => $request->address_name,
                'address_detail' => $request->address_detail,
                'contact_person' => $request->contact_person,
                'phone_number' => $request->phone_number,
                'gps_point' => $request->gps_point,
                'address_info' => $request->address_info,
                'kelurahan_desa_id' => $request->kelurahan_desa_id,
                'kecamatan_id' => $request->kecamatan_id,
                'kabupaten_kota_id' => $request->kabupaten_kota_id,
                'provinsi_id' => $request->provinsi_id,
                'created_by' => $request->user_id,
                'updated_by' => $request->user_id,
                'active_flag' => 1,
                'isMain' => 0
            ]);
        }else{
            $add = DB::table('00_address')
            ->insert([
                'user_id' => $request->user_id,
                'address_name' => $request->address_name,
                'address_detail' => $request->address_detail,
                'contact_person' => $request->contact_person,
                'phone_number' => $request->phone_number,
                'gps_point' => $request->gps_point,
                'address_info' => $request->address_info,
                'kelurahan_desa_id' => $request->kelurahan_desa_id,
                'kecamatan_id' => $request->kecamatan_id,
                'kabupaten_kota_id' => $request->kabupaten_kota_id,
                'provinsi_id' => $request->provinsi_id,
                'created_by' => $request->user_id,
                'updated_by' => $request->user_id,
                'active_flag' => 1,
                'isMain' => 0
            ]);
        }

        if(!$add){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'Address successfully added'], 200);
        }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------
    // public function addAddress(Request $request)
    // {
        //     if($request->get('is_agent') == 0){
        // 	$add = DB::table('00_address')
        // 	->insert([
        // 		'customer_id' => $request->user_id,
        // 		'address_name' => $request->address_name,
        // 		'address_detail' => $request->address_detail,
        // 		'contact_person' => $request->contact_person,
        // 		'phone_number' => $request->phone_number,
        // 		'gps_point' => $request->gps_point,
        //         'address_info' => $request->address_info,
        // 		'kelurahan_desa_id' => $request->kelurahan_desa_id,
        // 		'kecamatan_id' => $request->kecamatan_id,
        // 		'kabupaten_kota_id' => $request->kabupaten_kota_id,
        // 		'provinsi_id' => $request->provinsi_id,
        // 		'created_by' => $request->user_id,
        // 		'updated_by' => $request->user_id,
        //         'active_flag' => 1
        // 	]);
        // }else{
        //     $add = DB::table('00_address')
        //     ->insert([
        //         'user_id' => $request->user_id,
        //         'address_name' => $request->address_name,
        //         'address_detail' => $request->address_detail,
        //         'contact_person' => $request->contact_person,
        //         'phone_number' => $request->phone_number,
        //         'gps_point' => $request->gps_point,
        //         'address_info' => $request->address_info,
        //         'kelurahan_desa_id' => $request->kelurahan_desa_id,
        //         'kecamatan_id' => $request->kecamatan_id,
        //         'kabupaten_kota_id' => $request->kabupaten_kota_id,
        //         'provinsi_id' => $request->provinsi_id,
        //         'created_by' => $request->user_id,
        //         'updated_by' => $request->user_id,
        //         'active_flag' => 1
        //     ]);
        // }

        // 	if(!$add){
        // 		return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        // 	}else{
        // 		return response()->json(['isSuccess' => true, 'message' => 'Address successfully added'], 200);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------

     // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------
    public function updateAddress(Request $request)
    {
            if($request->get('is_agent') == 0){
            $update = DB::table('00_address')
            ->where('address_id', $request->address_id)
            ->update([
                'customer_id' => $request->user_id,
                'address_name' => $request->address_name,
                'address_detail' => $request->address_detail,
                'contact_person' => $request->contact_person,
                'phone_number' => $request->phone_number,
                'gps_point' => $request->gps_point,
                'address_info' => $request->address_info,
                'kelurahan_desa_id' => $request->kelurahan_desa_id,
                'kecamatan_id' => $request->kecamatan_id,
                'kabupaten_kota_id' => $request->kabupaten_kota_id,
                'provinsi_id' => $request->provinsi_id,
                'updated_by' => $request->user_id
            ]);
        }else{
            $update = DB::table('00_address')
            ->where('address_id', $request->address_id)
            ->update([
                'user_id' => $request->user_id,
                'address_name' => $request->address_name,
                'address_detail' => $request->address_detail,
                'contact_person' => $request->contact_person,
                'phone_number' => $request->phone_number,
                'gps_point' => $request->gps_point,
                'address_info' => $request->address_info,
                'kelurahan_desa_id' => $request->kelurahan_desa_id,
                'kecamatan_id' => $request->kecamatan_id,
                'kabupaten_kota_id' => $request->kabupaten_kota_id,
                'provinsi_id' => $request->provinsi_id,
                'updated_by' => $request->user_id
            ]);

        }

            if(!$update){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'Address successfully updated'], 200);
            }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

     // ORIGINAL START ---------------------------------------------------------------
    // public function updateAddress(Request $request)
    // {
        //     if($request->get('is_agent') == 0){
        // 	$update = DB::table('00_address')
        // 	->where('address_id', $request->address_id)
        // 	->update([
        //         'customer_id' => $request->user_id,
        // 		'address_name' => $request->address_name,
        // 		'address_detail' => $request->address_detail,
        // 		'contact_person' => $request->contact_person,
        // 		'phone_number' => $request->phone_number,
        // 		'gps_point' => $request->gps_point,
        //         'address_info' => $request->address_info,
        // 		'kelurahan_desa_id' => $request->kelurahan_desa_id,
        // 		'kecamatan_id' => $request->kecamatan_id,
        // 		'kabupaten_kota_id' => $request->kabupaten_kota_id,
        // 		'provinsi_id' => $request->provinsi_id,
        // 		'updated_by' => $request->user_id
        // 	]);
        // }else{
        //     $update = DB::table('00_address')
        //     ->where('address_id', $request->address_id)
        //     ->update([
        //         'user_id' => $request->user_id,
        //         'address_name' => $request->address_name,
        //         'address_detail' => $request->address_detail,
        //         'contact_person' => $request->contact_person,
        //         'phone_number' => $request->phone_number,
        //         'gps_point' => $request->gps_point,
        //         'address_info' => $request->address_info,
        //         'kelurahan_desa_id' => $request->kelurahan_desa_id,
        //         'kecamatan_id' => $request->kecamatan_id,
        //         'kabupaten_kota_id' => $request->kabupaten_kota_id,
        //         'provinsi_id' => $request->provinsi_id,
        //         'updated_by' => $request->user_id
        //     ]);

        // }

        // 	if(!$update){
        // 		return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        // 	}else{
        // 		return response()->json(['isSuccess' => true, 'message' => 'Address successfully updated'], 200);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------
    public function deleteAddress(Request $request)
    {
            $delete = DB::table('00_address')
            ->where('address_id', $request->address_id)
            ->update(['active_flag' => 0, 'updated_by' => $request->user_id]);

            if(!$delete){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'Address successfully deleted'], 200);
            }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------
    // public function deleteAddress(Request $request)
    // {
        // 	$delete = DB::table('00_address')
        // 	->where('address_id', $request->address_id)
        // 	->update(['active_flag' => 0, 'updated_by' => $request->user_id]);

        // 	if(!$delete){
        // 		return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        // 	}else{
        // 		return response()->json(['isSuccess' => true, 'message' => 'Address successfully deleted'], 200);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ------------------------------------------------------
    public function getProfile(Request $request)
    {               
        if($request->get('is_agent') == 0){
            $profile = DB::table('00_customer')
            ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_password AS user_password', 'customer_image AS user_image', 'saldo AS saldo')
            ->where('customer_id', $request->user_id)
            ->first();
        }else{
            $profile = DB::table('98_user')
            ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number', 'user_password AS user_password', 'user_image AS user_image', 'saldo AS saldo')
            ->where('user_id', $request->user_id)
            ->first();
        }
        
        //new script---------------------------------------------------------------------------------------
        $cs = DB::table('99_global_parameter')
            ->select('global_parameter_value as customer_service')
            ->where('global_parameter_name','=', 'whatsapp_customer_service')
            ->first();

        $q = array(
            "user_id"=> $profile->user_id,
            "user_name"=> $profile->user_name,
            "user_email"=> $profile->user_email,
            "user_phone_number"=> $profile->user_phone_number,
            "user_password"=> $profile->user_password,
            "user_image"=> $profile->user_image == null ? null : asset($profile->user_image),
            "saldo"=>$profile->saldo,
            "total_balance"=> $profile->saldo == null ? "0" : (string)$profile->saldo,
            "customer_service" => $cs->customer_service
        );
        //new script end---------------------------------------------------------------------------------------

        // $agent = DB::table('98_user as a')
        //     ->select('a.user_id as agent_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
        //         'b.organization_type_id','b.organization_name', 'b.organization_desc','a.active_flag')
        //     ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
        //     ->where('b.organization_type_id', 2)
        //     ->where([
        //         ['a.user_id', $request->user_id],
                
        //     ])
        //     ->first();
        // dd($agent);
        
        // $profile = array(
        //     'user_id' => $q->user_id,
        //     'user_name' => $q->user_name,
        //     'user_email' => $q->user_email,
        //     'user_phone_number' => $q->user_phone_number,
        //     'user_password_status' => $q->user_password == null || $q->user_password  == "" ? 0 : 1,
        //     'user_image' => $q->user_image == null ? null : asset($q->user_image)
        // );

        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Fetch data failed'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        }
    }
     // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------
    // public function getProfile(Request $request)
    // {   
        //     if($request->get('is_agent') == 0){
        //         $q = DB::table('00_customer')
        //         ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_password AS user_password', 'customer_image AS user_image')
        //         ->where('customer_id', $request->user_id)
        //         ->first();
        //     }else{
        //         $q = DB::table('98_user')
        //         ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number', 'user_password AS user_password', 'user_image AS user_image')
        //         ->where('user_id', $request->user_id)
        //         ->first();
        //     }
        	

        //     // $agent = DB::table('98_user as a')
        //     //     ->select('a.user_id as agent_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
        //     //         'b.organization_type_id','b.organization_name', 'b.organization_desc','a.active_flag')
        //     //     ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
        //     //     ->where('b.organization_type_id', 2)
        //     //     ->where([
        //     //         ['a.user_id', $request->user_id],
                    
        //     //     ])
        //     //     ->first();
        //     // dd($agent);
            
        //     // $profile = array(
        //     //     'user_id' => $q->user_id,
        //     //     'user_name' => $q->user_name,
        //     //     'user_email' => $q->user_email,
        //     //     'user_phone_number' => $q->user_phone_number,
        //     //     'user_password_status' => $q->user_password == null || $q->user_password  == "" ? 0 : 1,
        //     //     'user_image' => $q->user_image == null ? null : asset($q->user_image)
        //     // );

        // 	if(!$q){
        // 		return response()->json(['isSuccess' => false, 'message' => 'Fetch data failed'], 500);
        // 	}else{
        // 		return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------
    public function updateProfile(Request $request)
    {
         // $q = DB::table('00_customer')
         // ->select('customer_name', 'customer_email', 'customer_phone_number', 'customer_image')
         // ->where('customer_id', $request->user_id)
         // ->first();

            if($request->get('is_agent') == 0){
                //new script start---------------------------------------
                $check = DB::table('00_customer')
                    ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number')
                    ->where('customer_id', $request->user_id)
                    ->first();
                //new script end---------------------------------------
                $update = DB::table('00_customer')
                    ->where('customer_id', $request->user_id)
                    ->update([
                        'customer_name' => is_null($request->name) ? $check->user_name : $request->name,
                        'customer_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
                ]);

            }else{
                //new script start---------------------------------------
                $check = DB::table('98_user')
                    ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number')
                    ->where('user_id', $request->user_id)
                    ->first();
                //new script end---------------------------------------
                $update = DB::table('98_user')
                ->where('user_id', $request->user_id)
                ->update([
                    'user_name' => is_null($request->name) ? $check->user_name : $request->name,
                    'user_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
            ]);

            }

         if($update){
             return response()->json(['isSuccess' => true, 'message' => 'Profile successfully updated'], 200);
         }else{
             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong, Data not changed'], 500);
         }
    }

    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

    public function getBalanceDetail(Request $request){
        if($request->get('is_agent') == 0){
            $q = DB::table('10_cashback')
            ->select('10_cashback.cashback_id','10_cashback.return_amount','10_cashback.transaction_flag','10_cashback.issue_id','00_issue.ticketing_num','10_order.order_id','10_order.order_code','10_cashback.created_date')
            ->leftJoin('00_issue','00_issue.issue_id','=','10_cashback.issue_id')
            ->leftJoin('10_order','10_order.order_id','=','00_issue.order_id')
            ->where('10_cashback.customer_id', $request->input('user_id'));

            if($request->input('sort_by') == 1){
                $q->orderBy("10_cashback.transaction_flag","ASC");
            }elseif($request->input('sort_by') == 2){
                $q->orderBy("10_cashback.transaction_flag","DESC");
            }elseif($request->input('sort_by') == 3){
                $q->orderBy("10_cashback.created_date","ASC");
            }elseif($request->input('sort_by') == 4 || $request->input('sort_by') == ""){
                $q->orderBy("10_cashback.created_date","DESC");
            }            
        }else{
            $q = DB::table('10_cashback')
            ->select('10_cashback.cashback_id','10_cashback.return_amount','10_cashback.transaction_flag','10_cashback.issue_id','00_issue.ticketing_num','10_order.order_id','10_order.order_code','10_cashback.created_date')
            ->leftJoin('00_issue','00_issue.issue_id','=','10_cashback.issue_id')
            ->leftJoin('10_order','10_order.order_id','=','00_issue.order_id')
            ->where('10_cashback.agent_id', $request->input('user_id'));
        }

        if($request->input('dateFrom') != ""){
            $start_full_date = $request->input('dateFrom');
            $start_date = explode('-',$request->input('dateFrom'))[0];
            $start_month = explode('-',$request->input('dateFrom'))[1];
            $start_year = explode('-',$request->input('dateFrom'))[2];
            $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
            
            $q->where(DB::raw('CAST(10_cashback.created_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
        }
        if($request->input('dateTo') != ""){
            $end_full_date = $request->input('dateTo');
            $end_date = explode('-',$request->input('dateTo'))[0];
            $end_month = explode('-',$request->input('dateTo'))[1];
            $end_year = explode('-',$request->input('dateTo'))[2];
            $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

            $q->where(DB::raw('CAST(10_cashback.created_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
        }

        $q->whereIn('transaction_flag',[0,1]);
        $q = $q->get();

        $currency = new Currency();

        $balance_transaction_data = array();
    	for ($b=0; $b < count($q); $b++) { 
            $obj = array(
                "balance_id" => $q[$b]->cashback_id,
                "amount" => $currency->convertToCurrency($q[$b]->return_amount),
                "transaction_flag" => $q[$b]->transaction_flag,
                "description" => ($q[$b]->transaction_flag == 0 ? ('Pengurangan saldo karena dipakai untuk pembayaran dengan kode pesanan: '.$q[$b]->order_code) : ('Solusi pengaduan dengan kode pesanan: '.$q[$b]->order_code)),
                "transaction_date" => date("Y-m-d H:i:s", strtotime($q[$b]->created_date))
            );
            array_push($balance_transaction_data, $obj);
        }

        // $agent = DB::table('98_user as a')
        //     ->select('a.user_id as agent_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
        //         'b.organization_type_id','b.organization_name', 'b.organization_desc','a.active_flag')
        //     ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
        //     ->where('b.organization_type_id', 2)
        //     ->where([
        //         ['a.user_id', $request->user_id],
                
        //     ])
        //     ->first();
        // dd($agent);
        
        // $profile = array(
        //     'user_id' => $q->user_id,
        //     'user_name' => $q->user_name,
        //     'user_email' => $q->user_email,
        //     'user_phone_number' => $q->user_phone_number,
        //     'user_password_status' => $q->user_password == null || $q->user_password  == "" ? 0 : 1,
        //     'user_image' => $q->user_image == null ? null : asset($q->user_image)
        // );

    	if(!$q){
    		return response()->json(['isSuccess' => false, 'message' => 'Fetch data failed'], 500);
    	}else{
    		return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $balance_transaction_data], 200);
    	}
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------
    // public function updateProfile(Request $request)
    // {
        // 	// $q = DB::table('00_customer')
        // 	// ->select('customer_name', 'customer_email', 'customer_phone_number', 'customer_image')
        // 	// ->where('customer_id', $request->user_id)
        // 	// ->first();
        //     if($request->get('is_agent') == 0){
        //     	$update = DB::table('00_customer')
        //     	->where('customer_id', $request->user_id)
        //     	->update([
        //     		'customer_name' => is_null($request->name) ? $check->user_name : $request->name,
        //     		'customer_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
        //     	]);

        //     }else{
        //         $update = DB::table('98_user')
        //         ->where('user_id', $request->user_id)
        //         ->update([
        //             'user_name' => is_null($request->name) ? $check->user_name : $request->name,
        //             'user_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
        //     ]);

        //     }

        // 	if($update){
        // 		return response()->json(['isSuccess' => true, 'message' => 'Profile successfully updated'], 200);
        // 	}else{
        // 		return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong, Data not changed'], 500);
        // 	}
    // }
    // ORIGINAL END ---------------------------------------------------------------

     public function updatePhoneNumber(Request $request)
    {

    if($request->get('is_agent') == 0){
       $q = DB::table('00_customer')
        ->select('customer_name','customer_phone_number')
        ->where('customer_id', $request->user_id)
        ->first();

        $update = DB::table('00_customer')
        ->where('customer_id', $request->user_id)
        ->update([
            'customer_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
        ]);
    }else{
         $q = DB::table('98_user')
        ->select('user_name','user_phone_number')
        ->where('user_id', $request->user_id)
        ->first();

        $update = DB::table('98_user')
        ->where('user_id', $request->user_id)
        ->update([
            'user_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number
        ]);
    }

        if($update){
            return response()->json(['isSuccess' => true, 'message' => 'Phone successfully updated'], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong, Data not changed'], 500);
        }
    }

    public function updateProfileNew(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'profile_picture' => ['required','mimes:jpg,jpeg,png,pneg,svg']
        ]);
        
        if($validator->passes()){
            $folder_name = 'img/uploads/profile_picture/';
            $file = $request->file('profile_picture');
            $ext = $file == null ? '' : $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;

            if (!file_exists($folder_name)) {
                mkdir($folder_name, 777, true);
            }
            ini_set('memory_limit', '256M');
            Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
            
        if($request->get('is_agent') == 0){
            $customer = DB::table('00_customer')
            ->where('customer_id', $request->input('user_id'))
            ->first();
            if($customer != null && $customer->customer_image != null){
                File::delete($customer->customer_image);
            }
            $update = DB::table('00_customer')
            ->where('customer_id', $request->input('user_id'))
            ->update([
                'customer_image' => $folder_name.$name,
                'customer_name' => is_null($request->name) ? $check->user_name : $request->name,
                'customer_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number

            ]);

        }else{

            $agent = DB::table('98_user')
            ->where('user_id', $request->input('user_id'))
            ->first();

            if($agent != null && $agent->user_image != null){
                File::delete($agent->user_image);
            }
            $update = DB::table('98_user')
            ->where('user_id', $request->input('user_id'))
            ->update([
                'user_image' => $folder_name.$name,
                'user_name' => is_null($request->name) ? $check->user_name : $request->name,
                'user_phone_number' => is_null($request->phone_number) ? $check->user_phone_number : $request->phone_number

            ]);
        }
            return response()->json(['isSuccess' => true, 'message' => 'Profile Picture successfully changed'], 200);
        
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
        
    }



}