<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Currency;
use App\Helpers\Transaction;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Config;
use App\Order;
use App\Payment;
use App\OrderDetail;
use App\OrderHistory;
use App\Cart;
use App\Notification;
use Mail;
use Exception;
use App\TransactionHeader;
use App\TransactionDetail;

class OrderController extends Controller
{
    
    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        //$this->middleware('check-token');
    }

    public function addNewOrder(Request $request)
    {
        /*======================== EXAMPLE IN POSTMAN =======================*/
        /*{
            "user_id": 10,
            "payment_method_id": 3,
            "data": [
                {
                    "warehouse_id": 1,
                    "shipment_method_id": 2
                },
                {
                    "warehouse_id": 2,
                    "shipment_method_id": 1
                }
            ],
            "address_id": 1,
            "voucher_id": 1
        }*/
        /*======================== EXAMPLE IN POSTMAN =======================*/

        /*======================== START CHECK VOUCHER ========================*/
        // $req_test = array(
        //     "user_id"=> $request->user_id,
        //     "is_agent" => $request->is_agent,
        //     "payment_method_id" => $request->payment_method_id,
        //     "data"=> $request->get('data'),
        //     "address_id" => $request->address_id,
        //     "cashback" => $request->cashback,
        //     "voucher_id" => $request->voucher_id
        // );
        // return response()->json(['isSuccess' => false, 'data' => $req_test], 500);
        $voucher = DB::table('00_vouchers')
                    ->select('00_vouchers.*',
                        DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
                    ->where('voucher_id', $request->voucher_id)
                    ->first();

        $voucher_id = null;
        $voucher_amount = null;
        $voucher_fixed_value = null;
        $voucher_min_price_requirement = null;
        $voucher_max_provided_price = null;
        $voucher_max_used_users = 0;

        if($request->voucher_id != "" && ($voucher != null && $voucher->expired == 0 && ($voucher->max_uses_user <= $voucher->max_uses))){
            $voucher_amount = $voucher->amount;
            $voucher_fixed_value = $voucher->is_fixed;
            $voucher_min_price_requirement = $voucher->min_price_requirement;
            $voucher_max_provided_price = $voucher->max_value_price;
            $voucher_max_used_vouchers = $voucher->max_uses_user;
            DB::table('00_vouchers')
            ->where('voucher_id', $request->voucher_id)
            ->update(['max_uses_user' => ($voucher_max_used_vouchers + 1)]);
            $voucher_id = $request->voucher_id;
        }
        
        /*======================== END CHECK VOUCHER ========================*/

        /*======================== START CHECK ADMIN FEE BY PAYMENT METHOD ========================*/
        $request->payment_method_id = $request->payment_method_id == 'null' ? '13' : $request->payment_method_id;
        $payment = DB::table('00_payment_method')
                ->where('payment_method_id', $request->payment_method_id)
                ->first();
        $admin_fee = null;
        $admin_fee_percentage = null;
        $payment_name = "";
        if($payment != null){
            if($payment->admin_fee != null){
                $admin_fee = $payment->admin_fee;
            }

            if($payment->admin_fee_percentage != null){
                $admin_fee_percentage = $payment->admin_fee_percentage;
            }

            $payment_name = $payment->payment_method_name;
        }
        /*======================== END CHECK ADMIN FEE BY PAYMENT METHOD ========================*/
        
        /*======================== START CHECK TAX ========================*/
        $global_parameter = DB::table('99_global_parameter')
        ->whereIn('global_parameter_name',['pricing_include_tax','tax_value'])
        ->get();
        $pricing_include_tax = 1;
        $tax_value = 0;
        if(count($global_parameter) > 0){
            $pricing_include_tax = $global_parameter[0]->global_parameter_value;
            $tax_value = ($global_parameter[1]->global_parameter_value == "" || $global_parameter[1]
                ->global_parameter_value == null) ? 0 : $global_parameter[1]->global_parameter_value;
            $tax_value = number_format($tax_value,2);
        }
        /*======================== END CHECK TAX ========================*/

        $order_no = "YMOD".date("ymd");
        $bill_no = "YMBL".date("ymd");

        /*======================== START INSERT TABLE PAYMENT ========================*/
        $billing = Payment::insertGetId([
            'payment_name' => $payment_name, 
            'customer_id' => $request->user_id,  
            'payment_method_id' => $request->payment_method_id, 
            'invoice_status_id' => 3, 
            'voucher_id' => $voucher_id,
            'active_flag' => 1, 
            'updated_by' => $request->user_id,
            'is_agent' => $request->is_agent,
            'pricing_include_tax' => $pricing_include_tax,
            'national_income_tax' => $tax_value,
            'payment_date' => null,
            'destination_address' => $request->address_id
        ]);
        /*======================== END INSERT TABLE PAYMENT ========================*/
        $billing_code = "";
        if(!$billing){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            $transaction = new Transaction();
            $billing_code = $transaction->generateCode($billing, "bill");
            $updatePayment = Payment::where('payment_id', $billing)
            ->update([
                'invoice_no' => $billing_code
            ]);

            if(!$updatePayment){
                return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when update order code'], 500);
            }
        }


        $data = $request->get('data');

        $total_all = 0;
        $subtotal_voucher = 0;
        $order_status_id = 5;
        for($i=0;$i<count($data);$i++){
            /*======================== START CHECK SHIPMENT FEE BY INDEX ARRAY SHIPMENT METHOD ========================*/
            $shipment = DB::table('00_shipment_method')
                    ->where('shipment_method_id', '=', $data[$i]['shipment_method_id'])
                    ->first();
            $shipment_fee = $data[$i]['shipment_price'];

            if ($data[$i]['shipment_method_id'] == 4) {
                $order_status_id = 6;
            }
            // dd($order_status_id);
            /*======================== END CHECK SHIPMENT FEE BY INDEX ARRAY SHIPMENT METHOD ========================*/

            /*======================== START INSERT TABLE ORDER ========================*/
            $add = Order::insertGetId([
                'order_code' => $order_no,
                'payment_id' => $billing,
                'buyer_user_id' => $request->user_id,
                'payment_method_id' => $request->payment_method_id,
                'shipment_price' => $shipment_fee,
                //'admin_fee' => $admin_fee,
                //'admin_fee_percentage' => $admin_fee_percentage,
                'shipment_method_id' => $data[$i]['shipment_method_id'],
                'destination_address' => $request->address_id,
                'voucher_id' => $voucher_id,
                'warehouse_id' => $data[$i]['warehouse_id'],
                'grand_total' => 0,//$request->grand_total,
                'min_price' => $voucher_min_price_requirement,
                'max_price' => $voucher_max_provided_price,
                'is_fixed' => $voucher_fixed_value,
                // 'voucher_amount' => $voucher_amount,
                'order_status_id' => $order_status_id,
                'pricing_include_tax' => $pricing_include_tax,
                'national_income_tax' => $tax_value,
                'active_flag' => 1,
                'created_by' => $request->user_id,
                'is_agent' => $request->is_agent,
                'invoice_status_id' => 3
            ]);
            /*======================== END INSERT TABLE ORDER ========================*/

            if(!$add){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
            }else{
                $transaction = new Transaction();
                $order_code = $transaction->generateCode($add);
                $updateOrder = Order::where('order_id', $add)
                ->update([
                    'order_code' => $order_code
                ]);
    
                if(!$updateOrder){
                    return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when update order code'], 500);
                }

                /*======================== START MOVE DATA IN TABLE CART TO ORDER DETAIL ========================*/
                $cart = DB::table('10_cart AS ca')
                ->select('ca.buyer_user_id','ca.prod_id AS product_id',
                    'ca.amount as product_price',
                    // (DB::raw('(CASE WHEN ca.amount <> pr.prod_price THEN pr.prod_price ELSE ca.amount END) AS product_price')),
                    DB::raw('SUM(ca.quantity) AS product_qty'),
                    'pr.is_taxable_product','pr.tax_value','pr.uom_id','pr.uom_value','pr.product_sku_id', 'ca.warehouse_id', 
                    'pr.bruto','pr.dim_length','pr.dim_height','pr.dim_width'
                )
                ->where([
                    ['ca.buyer_user_id', $request->user_id], 
                    ['ca.is_agent', $request->is_agent], 
                    ['ca.warehouse_id', $data[$i]['warehouse_id']]
                ])
                ->join('00_product AS pr','pr.prod_id','=','ca.prod_id')
                ->where('ca.active_flag',1)
                ->groupBy('ca.buyer_user_id')
                ->groupBy('ca.prod_id')
                ->get();

                $cart_array = array();
                $data_prod = array();
                $min_qty_buy_prod = 0;
                $subtotal = 0;
                $subtotal_all = 0;
                $weight_all = 0;
                $total_shipment_price = 0;
                for ($b=0; $b < count($cart); $b++) { 
                    $obj = array(
                        "order_id" => $add,
                        "prod_id" => $cart[$b]->product_id,
                        "quantity" => $cart[$b]->product_qty,
                        "price" => $cart[$b]->product_price,
                        "active_flag" => 1,
                        "uom_id" => $cart[$b]->uom_id,
                        "uom_value" => $cart[$b]->uom_value,
                        "bruto" => $cart[$b]->bruto,
                        "dim_length" => $cart[$b]->dim_length,
                        "dim_width" => $cart[$b]->dim_width,
                        "dim_height" => $cart[$b]->dim_height,
                        "sku_status" => $cart[$b]->product_sku_id == null ? 0 : 1,
                        "is_taxable_product" => $cart[$b]->is_taxable_product,
                        "tax_value" => $cart[$b]->tax_value,
                        "warehouse_id" => $cart[$b]->warehouse_id, 
                        "created_by" => $request->user_id
                    );

                    array_push($cart_array, $obj);
                    array_push($data_prod, $cart[$b]->product_id);
                    if ($min_qty_buy_prod == 0 or $min_qty_buy_prod > $cart[$b]->product_qty) {
                       $min_qty_buy_prod = $cart[$b]->product_qty;
                    }
                    /*======================== START CALCULATE GRANDTOTAL ========================*/
                    $subtotal = $cart[$b]->product_price * $cart[$b]->product_qty;
                    $subtotal_all += $subtotal;
                    $weight_all += $cart[$b]->bruto;
                    /*======================== END CALCULATE GRANDTOTAL ========================*/
                }

                $shipment_price = $shipment_fee;

                $total_include_tax = $subtotal_all + ($subtotal_all*$tax_value);
                $total = $total_include_tax + $shipment_price;
                $subtotal_voucher += $subtotal_all;
                //dd("subtotal : ".$subtotal.", subtotal all : ".$subtotal_all.", tax : ".$total_include_tax.", total: ".$total);
                $total_all  += $total;
                //$total = ($subtotal + ($subtotal*$tax_value)) - $voucher_amount + $admin_fee + $total_shipment_price;

                // COVERAGE AREA
                    $string_prod = 'AND `d`.`prod_id` IN ('.implode(",",$data_prod).')';
                    $wh_id = $data[$i]['warehouse_id'];
                    $getAddress = DB::table('00_address as a')
                                ->select('a.address_id','b.kelurahan_desa_id','b.kode_pos')
                                ->join('00_kelurahan_desa as b','a.kelurahan_desa_id','=','b.kelurahan_desa_id')
                                ->where('a.address_id', $request->address_id)
                                ->first(); 


                    $getYukmarket = DB::table('00_organization')
                                ->select('organization_id')
                                ->where('organization_type_id', 1)
                                ->first();
                    $agent_id = $getYukmarket->organization_id;

                    $getAgent = DB::table('00_mapping_coverage as a')
                            ->selectRaw('a.organization_id')
                            ->Join('00_kelurahan_desa as b','a.kelurahan_desa_id','=','b.kelurahan_desa_id')
                            ->Join('00_organization as c','a.organization_id','=','c.organization_id')
                            ->Join('00_inventory as d','a.organization_id','=','d.organization_id')
                            ->whereRaw("(
                                        `b`.`kode_pos` = '".$getAddress->kode_pos."'
                                        AND `d`.`warehouse_id` = '".$wh_id."'
                                        AND (d.stock - d.booking_qty) > '".$min_qty_buy_prod."'
                                    )")
                            ->groupBy('a.organization_id')
                            ->orderBy('c.rating')
                            ->first();
                    if ($getAgent) {
                        $agent_id = $getAgent->organization_id;
                    }
                // COVERAGE AREA


                $updateOrder = Order::where('order_id', $add)
                ->update([
                    'agent_user_id' => $agent_id,
                    'grand_total' => $total
                    // 'weight' => $weight_all
                    // 'shipment_price' => $shipment_price
                ]);
                // dd($cart_array);

                if(sizeof($cart)>0){
                    $deleteAllDataInCartByUserId = DB::table('10_cart')
                        ->where([
                            ['buyer_user_id', $request->user_id],
                            ['is_agent', $request->is_agent],
                            ['warehouse_id', $data[$i]['warehouse_id']]
                        ])
                        ->delete();
                    if(!$deleteAllDataInCartByUserId){
                        return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when delete cart data'], 500);
                    }
                }
                
                $insertCart = OrderDetail::insert($cart_array);
                if(!$insertCart){
                    return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when inserting cart data'], 200);
                }
            }
        }
        // exit();
        /*======================== START CALCULATE GRANDTOTAL OF THE SAME BILL NUMBER ========================*/
        if ($voucher_fixed_value == 'N') {
            $voucher_amount = ($subtotal_voucher * $voucher_amount / 100);
            if($voucher_amount > $voucher_max_provided_price){
                $voucher_amount = $voucher_max_provided_price;
            }
        }

        $grandtotal_without_saldo = $total_all - $voucher_amount + $admin_fee;
        $saldo = 0;
        if ($request->cashback == true) {
            if ($request->is_agent > 0) {
                $getSaldo = DB::table('98_user')
                            ->select('saldo')
                            ->where('user_id', $request->user_id)
                            ->first();
            } else {
                $getSaldo = DB::table('00_customer')
                            ->select('saldo')
                            ->where('customer_id', $request->user_id)
                            ->first();
            }
            $check_cashback = $grandtotal_without_saldo - $getSaldo->saldo;
            if ($check_cashback <= 0) {
                $saldo = $grandtotal_without_saldo;
            } else {
                $saldo = $getSaldo->saldo;
            }
        }

        $grandtotal = $grandtotal_without_saldo - $saldo;
        $data_send = (object)array(
                'sub_total_product' => $subtotal_voucher,
                'sub_total_include_tax' => $total_all,
                'voucer_price' => $voucher_amount,
                'admin_fee' => $admin_fee,
                'grandtotal_without_saldo' => $grandtotal_without_saldo,
                'saldo_used' => $saldo,
                'grandtotal' => $grandtotal
            );
        // dd($data_send);
        /*======================== END CALCULATE GRANDTOTAL OF THE SAME BILL NUMBER ========================*/

        /*======================== START INTEGRATED MIDTRANS ========================*/
        if ($grandtotal > 0) {
            if ($order_status_id == 6) {
                $updateStock = $this->updateStock($billing_code);
                return response()->json(['isSuccess' => true, 'message' => 'Successfully added Order', 'billing_code' =>  $billing_code], 200);
            } else {
                $midtrans = $this->doPayment($billing_code, $grandtotal, $request->payment_method_id, $add, $billing, $data_send);
                $order_history = DB::table('10_order_history')->insertGetId([
                    'order_id' => $add,
                    'order_status_id' => $order_status_id,
                    'active_flag' => 1
                ]);
                $decode = $midtrans->getData();
                if($decode->doPaymentStatus == 'success'){
                    return response()->json(['isSuccess' => true, 'message' => 'Successfully added Order', 'billing_code' =>  $billing_code], 200);
                }else{
                    return response()->json(['isSuccess' => false, 'message' => 'Failed added order, please try again later', 'billing_code' => $billing_code], 200);
                }
            }
        } else {
            $updateStatusOrder = $this->useSaldo($billing_code, $grandtotal, $data_send, $billing);
            $decode = $updateStatusOrder->getData();
            // return response()->json($updateStatusOrder);
            if($decode->updateStatusOrder == 'success'){
                $updateStock = $this->updateStock($billing_code);
                return response()->json(['isSuccess' => true, 'message' => 'Successfully added Order', 'billing_code' =>  $billing_code], 200);
            }else{
                return response()->json(['isSuccess' => false, 'message' => 'Failed added order, please try again later', 'billing_code' => $billing_code], 200);
            }

        }
        /*======================== END INTEGRATED MIDTRANS ========================*/
    }
    
    //Ketika user melakukan transaksi setelah memilih pembayaran
    /*public function addNewOrderBackup(Request $request)
    {
        $order_no = "YMOD".date("ymd");
        $voucher = DB::table('00_vouchers')
                    ->select('00_vouchers.*',
                        DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
                    ->where('voucher_id', $request->voucher_id)
                    ->first();
        
        $voucher_id = null;
        $voucher_amount = null;
        $voucher_fixed_value = null;
        $voucher_min_price_requirement = null;
        $voucher_max_provided_price = null;
        $voucher_max_used_users = 0;

        if($request->voucher_id != "" && ($voucher != null && $voucher->expired == 0 && ($voucher->max_uses_user < $voucher->max_uses))){
            $voucher_amount = $voucher->amount;
            $voucher_fixed_value = $voucher->is_fixed;
            $voucher_min_price_requirement = $voucher->min_price_requirement;
            $voucher_max_provided_price = $voucher->max_value_price;
            $voucher_max_used_vouchers = $voucher->max_uses_user;
            DB::table('00_vouchers')
            ->where('voucher_id', $request->voucher_id)
            ->update(['max_uses_user' => ($voucher_max_used_vouchers + 1)]);
            $voucher_id = $request->voucher_id;
        }

        $shipment = DB::table('00_shipment_method')
                    ->where('shipment_method_id', $request->shipment_method_id)
                    ->first();
        $shipment_fee = null;
        if($shipment != null){
            $shipment_fee = $shipment->price;
        }

        $payment = DB::table('00_payment_method')
                    ->where('payment_method_id', $request->payment_method_id)
                    ->first();
        $admin_fee = null;
        $admin_fee_percentage = null;
        if($payment != null){
            if($payment->admin_fee != null){
                $admin_fee = $payment->admin_fee;
            }

            if($payment->admin_fee_percentage != null){
                $admin_fee_percentage = $payment->admin_fee_percentage;
            }
        }
        
        $global_parameter = DB::table('99_global_parameter')
        ->whereIn('global_parameter_name',['pricing_include_tax','tax_value'])
        ->get();
        $pricing_include_tax = 1;
        $tax_value = 0;
        if(count($global_parameter) > 0){
            $pricing_include_tax = $global_parameter[0]->global_parameter_value;
            $tax_value = ($global_parameter[1]->global_parameter_value == "" || $global_parameter[1]
                ->global_parameter_value == null) ? 0 : $global_parameter[1]->global_parameter_value;
            $tax_value = number_format($tax_value,2);
        }

        $add = Order::insertGetId([
            'order_code' => $order_no,
            'buyer_user_id' => $request->user_id,
            'payment_method_id' => $request->payment_method_id,
            'shipment_price' => $shipment_fee,
            'admin_fee' => $admin_fee,
            'admin_fee_percentage' => $admin_fee_percentage,
            'shipment_method_id' => $request->shipment_method_id,
            'destination_address' => $request->address_id,
            'voucher_id' => $voucher_id,
            'grand_total' => $request->grand_total,
            'min_price' => $voucher_min_price_requirement,
            'max_price' => $voucher_max_provided_price,
            'is_fixed' => $voucher_fixed_value,
            'voucher_amount' => $voucher_amount,
            'order_status_id' => 5,
            'pricing_include_tax' => $pricing_include_tax,
            'national_income_tax' => $tax_value,
            'active_flag' => 1,
            'created_by' => $request->user_id,
            'invoice_status_id' => 3
        ]);
        // echo $add;
        // exit;

        if(!$add){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            $transaction = new Transaction();
            $order_code = $transaction->generateCode($add);
            $updateOrder = Order::where('order_id', $order_id)
            ->update([
                'order_code' => $order_code
            ]);

            if(!$updateOrder){
                return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when update order code'], 200);
            }

            //pindahin dulu table cart ke table order_detail berdasarkan user_id, setelah itu table cart delete berdasarkan user_id
            $cart = DB::table('10_cart AS ca')
            ->select('ca.buyer_user_id','ca.prod_id AS product_id',
                (DB::raw('(CASE WHEN ca.amount <> pr.prod_price THEN pr.prod_price ELSE ca.amount END) AS product_price')),
                DB::raw('SUM(ca.quantity) AS product_qty'),
                'pr.is_taxable_product','pr.tax_value','pr.uom_id','pr.uom_value','pr.product_sku_id', 'ca.warehouse_id')
            ->where('ca.buyer_user_id', $request->user_id)
            ->join('00_product AS pr','pr.prod_id','=','ca.prod_id')
            ->where('ca.active_flag',1)
            ->groupBy('ca.buyer_user_id')
            ->groupBy('ca.prod_id')
            ->get();

            $cart_array = array();
            for ($b=0; $b < count($cart); $b++) { 
                $obj = array(
                    "order_id" => $add,
                    "prod_id" => $cart[$b]->product_id,
                    "quantity" => $cart[$b]->product_qty,
                    "price" => $cart[$b]->product_price,
                    "active_flag" => 1,
                    "uom_id" => $cart[$b]->uom_id,
                    "uom_value" => $cart[$b]->uom_value,
                    "sku_status" => $cart[$b]->product_sku_id == null ? 0 : 1,
                    "is_taxable_product" => $cart[$b]->is_taxable_product,
                    "tax_value" => $cart[$b]->tax_value,
                    "warehouse_id" => $cart[$b]->warehouse_id, 
                    "created_by" => $request->user_id
                );

                array_push($cart_array, $obj);
            }
            if(sizeof($cart)>0){
                //$updateFlag = DB::table('10_cart')->where('buyer_user_id', $request->user_id)->update(['active_flag' => 0]);
                //if(!$updateFlag){
                //    return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when update cart data'], 200);
                //}
                // $deleteAllDataInCartByUserId = DB::table('10_cart')
                //  ->where([
                //      ['buyer_user_id', $request->user_id]
                //  ])
                //     ->update(['active_flag' => 0]);
                $deleteAllDataInCartByUserId = DB::table('10_cart')
                    ->where([
                        ['buyer_user_id', $request->user_id]
                    ])
                    ->delete();
                if(!$deleteAllDataInCartByUserId){
                    return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when delete cart data'], 500);
                }
            }
            
            $insertCart = OrderDetail::insert($cart_array);
            if(!$insertCart){
                return response()->json(['isSuccess' => false, 'message' => 'Something went wrong when inserting cart data'], 200);
            }
            // print_r($cart);
            // exit;
            $midtrans = $this->doPayment($billing_code, $grandtotal, $request->payment_method_id, $add, $billing);

            $order_history = DB::table('10_order_history')->insertGetId([
                'order_id' => $add,
                'order_status_id' => 5,
                'active_flag' => 1
            ]);
            // return response()->json($midtrans);
            $decode = $midtrans->getData();
            if($decode->doPaymentStatus == 'success'){
                return response()->json(['isSuccess' => true, 'message' => 'Successfully added Order', 'order_code' =>  $order_code], 200);
            }else{
                //jika masuk sini perlu di hapus data sebelumnya, karena dapat mengakibatkan data masuk midtrans error (order no sama)
                return response()->json(['isSuccess' => false, 'message' => 'Failed added order, please try again later', 'order_code' => null], 200);
            }
            // return response()->json(['isSuccess' => true, 'message' => 'Successfully added Order', 'data' => $midtrans], 200);
        }
    }*/
    
    //untuk menambahkan order ke midtrans dan mendapatkan response transaksi id dari midtrans dan no account yang nantinya disimpan di db
    public function doPayment($billing_code, $amount, $payment_method_id, $order_id, $paymentId, $data_send){
        $url = "https://api.sandbox.midtrans.com/v2/charge";

        if($payment_method_id == 2){ //gopay
            $body = array(
                "payment_type" => "gopay",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                )/*,
                "gopay" => array(
                    "enable_callback" => true,
                    "callback_url" => "someapps://callback"
                )*/
            );
        }else if($payment_method_id == 3){ //virtual account bca
            $body = array(
                "payment_type" => "bank_transfer",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "bank_transfer" => array(
                    "bank" => "bca"
                )
            );
        }else if($payment_method_id == 4){ //virtual account bni
            $body = array(
                "payment_type" => "bank_transfer",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "bank_transfer" => array(
                    "bank" => "bni"
                )
            );
        }else if($payment_method_id == 5){ //virtual account permata bank
            $body = array(
                "payment_type" => "permata",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                )
            );
        }else if($payment_method_id == 6){ //virtual account mandiri
            $body = array(
                "payment_type" => "echannel",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "echannel" => array(
                    "bill_info1" => "Payment For yukmarket",
                    "bill_info2" => "shopping"
                )
            );
        }else if($payment_method_id == 7){ //bca clickpay
            $body = array(
                "payment_type" => "bca_klikpay",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "bca_klikpay" => array(
                  "description" => "shopping"
                )
            );
        }else if($payment_method_id == 8){ //cimb clicks
            $body = array(
                "payment_type" => "cimb_clicks",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "cimb_clicks" => array(
                  "description" => "shopping"
                )
            );
        }else if($payment_method_id == 15){ //cimb clicks
            $body = array(
                "payment_type" => "cstore",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "cstore" => array(
                    "store" => "alfamart",
                    "message" => "Payment For yukmarket ",
                    "alfamart_free_text_1" => "Shopping",
                    "alfamart_free_text_2" => "Shopping",
                    "alfamart_free_text_3" => "Shopping",
                )
            );
        }else if($payment_method_id == 16){ //cimb clicks
            $body = array(
                "payment_type" => "cstore",
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                ),
                "cstore" => array(
                    "store" => "indomaret",
                    "message" => "Payment For yukmarket "
                )
            );
        }else if($payment_method_id == 9 || $payment_method_id == 11){ //danamon online dan akulaku
            $payment_type = $payment_method_id == 9 ? "danamon_online" : "akulaku";
            $body = array(
                "payment_type" => $payment_type,
                "transaction_details" => array(
                    "order_id" => $billing_code,
                    "gross_amount" => $amount
                )
            );
        }

        $request = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => "Basic ".base64_encode('SB-Mid-server-PvdDDTyn7QU9vyxoOJtTBvG6')
        ])->post($url, $body);
        $response = json_decode($request->getBody()->getContents(), true);
        //dd($response['status_code']);

        $data = array();
        if($response['status_code'] == 201){
            if($payment_method_id == 2){ //response dari gojek
                $action_array = $response['actions'];
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "actions" => $action_array,
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $action_array[0]['url'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 3){ //virtual account bca
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "va_detail" => [
                        "bank" => $response['va_numbers'][0]['bank'],
                        "va_number" => $response['va_numbers'][0]['va_number']
                    ],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['va_numbers'][0]['va_number'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 4){ //virtual account bni
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "va_detail" => [
                        "bank" => $response['va_numbers'][0]['bank'],
                        "va_number" => $response['va_numbers'][0]['va_number']
                    ],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['va_numbers'][0]['va_number'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 5){ //response dari virtual account permata bank
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "permata_va_number" => $response['permata_va_number'],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['permata_va_number'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 6){ //response dari virtual account mandiri
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "bill_key" => $response['bill_key'],
                    "biller_code" => $response['biller_code'],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['bill_key']."-".$response['biller_code'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 7){ //response dari bca clickpay
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "redirect_data" => [
                        "url" => $response['redirect_data']['url'],
                        "method" => $response['redirect_data']['method'],
                        "params" => array(
                            "klikPayCode" => $response['redirect_data']['params']['klikPayCode'],
                            "transactionNo" => $response['redirect_data']['params']['transactionNo'],
                            "totalAmount" => $response['redirect_data']['params']['totalAmount'],
                            "currency" => $response['redirect_data']['params']['currency'],
                            "payType" => $response['redirect_data']['params']['payType'],
                            "callback" => $response['redirect_data']['params']['callback'],
                            "transactionDate" => $response['redirect_data']['params']['transactionDate'],
                            "description" => $response['redirect_data']['params']['descp'],
                            "miscFee" => $response['redirect_data']['params']['miscFee'],
                            "signature" => $response['redirect_data']['params']['signature']
                        )
                    ],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = "";
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 8){ //response dari cimb click
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "redirect_url" => $response['redirect_url'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = "";
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 15){ //response dari alfamart
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    // "redirect_url" => $response['redirect_url'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "payment_code" => $response['payment_code'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['payment_code'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 16){ //response dari indomart
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    // "redirect_url" => $response['redirect_url'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "payment_code" => $response['payment_code'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = $response['payment_code'];
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else if($payment_method_id == 9 || $payment_method_id == 11){ //response dari danamon dan akulaku
                $data = array(
                    "status_code" => $response['status_code'],
                    "status_message" => $response['status_message'],
                    "redirect_url" => $response['redirect_url'],
                    "transaction_id" => $response['transaction_id'],
                    "order_id" => $response['order_id'],
                    "merchant_id" => $response['merchant_id'],
                    "gross_amount" => $response['gross_amount'],
                    "currency" => $response['currency'],
                    "payment_type" => $response['payment_type'],
                    "transaction_time" => $response['transaction_time'],
                    "transaction_status" => $response['transaction_status'],
                    "fraud_status" => $response['fraud_status']
                );
                $transactionId = $response['transaction_id'];
                $accountNumber = "";
                $transactionStatus = $response['transaction_status'];                
                $transactionDate = $response['transaction_time'];
                //dd($transactionId, $accountNumber, $transactionStatus);
            }else{
                $transactionId = "";
                $accountNumber = "";
                $transactionStatus = "";                
                $transactionDate = "";
                //dd($transactionId, $accountNumber, $transactionStatus);
            }
            
            //update status order dan transaction id
            $updateStatusOrder = $this->updateStatusOrder($billing_code, $transactionId, $accountNumber, $transactionStatus, $order_id, $transactionDate, $paymentId, $amount, $data_send);
            if ($data_send->saldo_used > 0) {
                $updateSaldo = $this->updateSaldo($billing_code, $data_send);
            }
            $decode = $updateStatusOrder->getData();
            // return response()->json($updateStatusOrder);
            if($decode->updateStatusOrder == 'success'){
                $updateStock = $this->updateStock($billing_code);
                return response()->json(['doPaymentStatus' => 'success'], 200);
            }else{
                return response()->json(['doPaymentStatus' => 'false'], 200);
            }
            // return response()->json(['isSuccess' => true, 'message' => 'Successfully payment to midtrans', 'data' => $updateStatusOrder], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
    }

    public function updateStock($billing_code)
    {

        $billing = DB::table('10_payment as a')
        ->leftJoin('10_order as b','a.payment_id','=','b.payment_id')
        ->leftJoin('10_order_detail as c', 'b.order_id', 'c.order_id')
        ->select(
            'a.payment_id',
            'a.invoice_no',
            'a.customer_id',
            'b.buyer_user_id',
            'b.agent_user_id',
            'b.is_agent',
            'c.warehouse_id',
            'c.prod_id',
            'c.quantity'
        )
        ->where('a.invoice_no','=',$billing_code)
        ->get();
        // dd($billing);
        foreach ($billing as $key => $value) :

            $check_stockable = DB::table('00_product')
                        ->where('prod_id', '=', $value->prod_id)
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        // ->where([
                        //     ['warehouse_id', '=', $value->warehouse_id],
                        //     ['prod_id', '=', $check->prod_id],
                        //     ['active_flag', '=', 1],
                        // ])
                        ->first();
            // dd($check_stockable);
            $product_name = '';
            if (isset($check_stockable)) {
                $prod_id = $check_stockable->stockable_flag == 0 ?  $check_stockable->prod_id : $value->prod_id;
                $product_name = $check_stockable->prod_name.' ('.$check_stockable->uom_value.' '.$check_stockable->uom_name.')'; 
            } else {
                $prod_id = $value->prod_id;
            }

            $check = DB::table('00_inventory')
                ->where([
                    ['warehouse_id', '=', $value->warehouse_id],
                    ['prod_id', '=', $prod_id],
                    ['active_flag', '=', 1],
                ])->first();

            if ($check) :

                $old_stock = $check->booking_qty;
                $new_stock = $old_stock + $value->quantity;
                $stock_difference = $check->stock - $new_stock;
                
                $inventory_id = $check->inventory_id;
                DB::table('00_inventory')
                    ->where('inventory_id', $inventory_id)->update([
                    'booking_qty' => $new_stock
                ]);
                if ($stock_difference < 0) {
                    $data_notif = array(
                        'customer_id' => $value->is_agent > 0 ? null : $value->buyer_user_id,
                        'user_id' => $value->is_agent > 0 ? $value->buyer_user_id : null ,
                        'warehouse_id' => $value->warehouse_id,
                        'title' => 'Stock Produk Gudang Penjualan Tidak Mencukupi.',
                        'message' => 'Kirimkan Stock Untuk Produk '.$product_name.' Ke Gudang Penjualan.',
                        'type' => 'min_stock'
                    );    
                    DB::table('00_notification')->insert($data_notif); 
                }
                app('App\Http\Controllers\InventoryController')->pushStockBookingMobile($inventory_id);
            endif;
        endforeach;
        // exit();

    }
    
    //untuk mengupdate ketika mendapatkan balikan response dari midtrans untuk status order nya
    public function updateStatusOrder($billing_code, $transactionId, $accountNumber, $transactionStatus, $order_id, $transactionDate, $paymentId, $amount, $data_send){
        $orderStatusId = "";
        if($transactionStatus == "pending"){
            $orderStatusId = 5;
        }else if($transactionStatus == "settlement"){
            $orderStatusId = 6;
        }else if($transactionStatus == "deny"){
            $orderStatusId = 11;
        }else if($transactionStatus == "expire"){
            $orderStatusId = 12;
        }else{
            $orderStatusId = null;
        }
        
        $updatePayment = DB::table('10_payment')
        ->where('invoice_no', $billing_code)
        ->update([
            'invoice_status_id' => $orderStatusId,
            'payment_date' => $transactionDate,
            'midtrans_transaction_id' => $transactionId,
            'admin_fee' => $data_send->admin_fee,
            'voucher_amount' => $data_send->voucer_price,
            'account_number' => $accountNumber,
            'grandtotal_payment' => $amount
        ]);
        

        $update = DB::table('10_order')
        ->where('payment_id', $paymentId)
        ->update([
            'order_status_id' => $orderStatusId,
            'account_number' => $accountNumber,
            'midtrans_transaction_id' => $transactionId
        ]);

        if(!$update){
            // return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
            return response()->json(['updateStatusOrder' => 'false'], 200);
        }else{
            $this->reminderEmail($paymentId);
            // return response()->json(['isSuccess' => true, 'message' => 'Transaksi berhasil diproses, silahkan periksa riwayat transaksi anda'], 200);
            return response()->json(['updateStatusOrder' => 'success'], 200);
        }
    }

    public function useSaldo($billing_code, $amount, $data_send, $paymentId){
        $updatePayment = DB::table('10_payment')
        ->where('invoice_no', $billing_code)
        ->update([
            'invoice_status_id' => 2,
            'payment_date' => date("Y-m-d H:m:i"),
            // 'midtrans_transaction_id' => $transactionId,
            'admin_fee' => $data_send->admin_fee,
            'voucher_amount' => $data_send->voucer_price,
            // 'account_number' => $accountNumber,
            'grandtotal_payment' => $amount
        ]);
        

        $update = DB::table('10_order')
        ->where('payment_id', $paymentId)
        ->update([
            'order_status_id' => 6,
            // 'account_number' => $accountNumber,
            // 'midtrans_transaction_id' => $transactionId
        ]);
        $updateSaldo = $this->updateSaldo($billing_code, $data_send);
        if(!$update){
            // return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
            return response()->json(['updateStatusOrder' => 'false'], 200);
        }else{
            $this->reminderEmail($paymentId);
            // return response()->json(['isSuccess' => true, 'message' => 'Transaksi berhasil diproses, silahkan periksa riwayat transaksi anda'], 200);
            return response()->json(['updateStatusOrder' => 'success'], 200);
        }
    }

    public function updateSaldo($billing_code, $data_send){
        $check = DB::table('10_payment')
                ->where('invoice_no', $billing_code)
                ->first();
        if ($check->is_agent > 0) {
            $getSaldo = DB::table('98_user')
                            ->select('saldo')
                            ->where('user_id', $check->customer_id)
                            ->first();
            if ($getSaldo) {
               DB::table('98_user')
                    ->where('user_id', $check->customer_id)->update([
                    'saldo' => $getSaldo->saldo - $data_send->saldo_used
                ]);
            }
            if ($data_send->saldo_used > 0 ) {
                $cashback = DB::table('10_cashback')->insertGetId([
                        'payment_id' => $check->payment_id,
                        'agent_id' => $check->customer_id,
                        'claimed_amount' => $data_send->saldo_used,
                        'return_amount' => $data_send->saldo_used,
                        'transaction_flag' => 0,
                        'created_by' => $check->customer_id,
                        'created_date' => date('Y-m-d H:m:i'),
                        'updated_by' => $check->customer_id,
                        'updated_date' => date('Y-m-d H:m:i'),
                        'active_flag' => 1
                    ]);
            }
        } else {
            $getSaldo = DB::table('00_customer')
                            ->select('saldo')
                            ->where('customer_id', $check->customer_id)
                            ->first();
            if ($getSaldo) {
               DB::table('00_customer')
                    ->where('customer_id', $check->customer_id)->update([
                    'saldo' => $getSaldo->saldo - $data_send->saldo_used
                ]);
            }
            if ($data_send->saldo_used > 0 ) {
                $cashback = DB::table('10_cashback')->insertGetId([
                        'payment_id' => $check->payment_id,
                        'customer_id' => $check->customer_id,
                        'claimed_amount' => $data_send->saldo_used,
                        'return_amount' => $data_send->saldo_used,
                        'transaction_flag' => 0,
                        'created_by' => $check->customer_id,
                        'created_date' => date('Y-m-d H:m:i'),
                        'updated_by' => $check->customer_id,
                        'updated_date' => date('Y-m-d H:m:i'),
                        'active_flag' => 1
                    ]);
            }
        }
    }
    
   public function updateInventory($request,$id){
        $inventory = DB::table('00_inventory')
        ->where('inventory_id','=',$id)
        ->update($request);
    }
    public function addStockCard($request){
        $addStockCard = DB::table('00_stock_card')
        ->insert($request);        
    }
    public function addInventory($request){        
        $inventory = DB::table('00_inventory')
        ->insert($request);     
    }
    public function getDetailOrder($order_id){
        $detail = DB::table('10_order_detail')
        ->select('prod_id','price','quantity','uom_id')
        ->where('order_id','=',$order_id)
        ->get();
        return $detail;
    }
    public function getOrganizationId($user_id){
        $organization_id = DB::table('98_user')
        ->select('organization_id','warehouse_id')
        ->where('user_id','=',$user_id)
        ->first();
        return $organization_id;
    }
    public function getProductFromTransactionDetail($transactionHeaderId){
        $prod_id = DB::table('10_transaction_detail as a')
        ->join('00_product as b','a.prod_id','=','b.prod_id')
        ->select('a.prod_id','b.prod_price','b.stock')
        ->where('transaction_header_id','=',$transactionHeaderId)
        ->get();
        return $prod_id;
    }   
    public function checkInventoryConfirmStatusOrder($organization_id,$warehouse_id,$prod_id){        
        $checkInventory = DB::table('00_inventory')
        ->select('inventory_id', 'inventory_price','stock','prod_id')
        ->where([['organization_id','=',$organization_id],
            ['warehouse_id','=',$warehouse_id],
            ['active_flag','=',1]])
        ->whereIn('prod_id',$prod_id)
        ->get();
        // ->whereIn('prod_id',$request['prod_id'][0]);
        return $checkInventory;
    }    
    public function getTransactionHeader($id){
        $trHeader = DB::table('10_transaction_header')
        ->select('transaction_status', 'organization_id', 'warehouse_id','transaction_header_code','transaction_desc')
        ->where('transaction_header_id','=',$id)
        ->first();
        return $trHeader;
    }

     //untuk menyelesaikan transaksi ketika user sudah menerima barang
    public function confirmOrderStatus(Request $request){        
        if($request->get('is_agent') == 0){//jika user customer
            //potong stok
            $org=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
            //potong stok dan push
            app('App\Http\Controllers\InventoryController')->orderMobilePotongStok($request->order_id,$org->parent_organization_id,$org->warehouse_id);

            $update = DB::table('10_order')
            ->where([
                ['buyer_user_id', $request->user_id],
                ['order_id', $request->order_id]
            ])
            ->update([
                'order_status_id' => 10
            ]);

            $order_history = DB::table('10_order_history')->insertGetId([
                'order_id' => $request->order_id,
                'order_status_id' => 10,
                'active_flag' => 1
            ]);

            if(!$update){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
            }else{
                    $this->arrivalConfirmationEmail($request->order_id, $request->get('is_agent'));
                    return response()->json(['isSuccess' => true, 'message' => 'Sukses konfirmasi data order'], 200);
            }
        }else{//jika user agent           
            
            $update = DB::table('10_order')
            ->where([
                ['buyer_user_id', $request->user_id],
                ['order_id', $request->order_id],
                ['is_agent', 1]
            ])
            ->update([
                'order_status_id' => 10
            ]);                            
            
            $order_history = DB::table('10_order_history')->insertGetId([
                'order_id' => $request->order_id,
                'order_status_id' => 10,
                'active_flag' => 1
            ]);            

            //getOrganizationId
            $orgID = $this->getOrganizationId($request->user_id);  
            //potong stok dan push         
            // app('App\Http\Controllers\InventoryController')->orderMobilePotongStok($request->order_id,$orgID->organization_id,$orgID->warehouse_id);

            $transaction_header = DB::table('10_transaction_header')->insertGetId([
                'transaction_header_code' => DB::table('10_order')->select('order_code')->where('order_id', $request->order_id)->first()->order_code,
                'organization_id' => $orgID->organization_id,
                'warehouse_id' => DB::table('10_order')->select('warehouse_id')->where('order_id', $request->order_id)->first()->warehouse_id,
                'transaction_date' => date(now()),
                'transaction_desc' => "Aplikasi",
                'transaction_type_id' => 2,
                'transaction_status' => 1
            ]);

            $transaction_header_id = DB::getPdo()->lastInsertId();            

            $orderDetail = $this->getDetailOrder($request->order_id);            
            for($i=0;$i<count($orderDetail);$i++){//distribute transaction detail                
                $transaction_detail = DB::table('10_transaction_detail')->insertGetId([
                    'transaction_header_id' => $transaction_header_id,
                    'prod_id' =>  $orderDetail[$i]->prod_id,
                    'price' => $orderDetail[$i]->price,
                    'stock' => $orderDetail[$i]->quantity,
                    'uom_id' => $orderDetail[$i]->uom_id,
                    'active_flag' => 1,
                    'created_by' => $request->user_id,
                    'created_date' => date(now())
                ]);
            }            

            $trHeader = $this->getTransactionHeader($transaction_header_id);    

            $listProdId = $this->getProductFromTransactionDetail($transaction_header_id);
            $paramProductId = array();
            for($i=0;$i<count($listProdId);$i++){
                $paramProductId[$i]=$listProdId[$i]->prod_id;            
            }
            // return $paramProductId;                        
            
            if($trHeader->transaction_status>0){                
                $checkInventory = $this->checkInventoryConfirmStatusOrder($trHeader->organization_id,$trHeader->warehouse_id,$paramProductId);     

                

                if(count($checkInventory) >0){//jika tidak ada data product di inventory
                    //list product yg ada di inventory                
                    for($i=0;$i<count($checkInventory);$i++){
                        $inventoryProduct[$i]=$checkInventory[$i]->prod_id;               
                    }
                    //list yang harus diinsert ke inventory
                    $addToInventory= array_diff($paramProductId, $inventoryProduct);
                    if(count($addToInventory)>0){//jika ada data yang harus diinsert
                        for($i=0;$i<count($addToInventory);$i++){
                            $addInventorySchema = array(
                                'organization_id' => $orgID->organization_id,
                                'warehouse_id' => $trHeader->warehouse_id,
                                'prod_id' => $addToInventory[$i],
                                'inventory_date' => date(now()),
                                'inventory_price' => $orderDetail[$i]->price,
                                'stock' => $orderDetail[$i]->quantity,
                                'created_by' => $request->user_id,
                                'created_date' => date(now()),
                                'updated_date' => date(now())
                            );
                            $this->addInventory($addInventorySchema);
                        }
                    }                    
                    
                    //update inventory
                    for($i=0;$i<count($inventoryProduct);$i++){
                        $old_stock = $checkInventory[$i]->stock;
                        // dd($old_stock);
                        $new_stock = $orderDetail[$i]->quantity + $old_stock;
                        // dd($new_stock);
                        $old_price = $checkInventory[$i]->inventory_price;
                        $old_price_total = $old_price * $old_stock;
                        // dd($old_price_total);
                        $new_price_total = $orderDetail[$i]->price * $orderDetail[$i]->quantity;   
                        // dd($new_price_total);
                        $avg_price = ($new_price_total + $old_price_total)/$new_stock;
                        // dd($avg_price);
                        $updateInventorySchema = array(
                            'organization_id' => $orgID->organization_id,
                            'warehouse_id' => $trHeader->warehouse_id,
                            'prod_id' => $orderDetail[$i]->prod_id,
                            'inventory_date' => date(now()),
                            'inventory_price' => $avg_price,
                            'stock' => $new_stock,
                            'created_by' => $request->user_id,
                            'created_date' => date(now()),
                            'updated_date' => date(now())
                        );
                        $this->updateInventory($updateInventorySchema,$checkInventory[$i]->inventory_id);
                        //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                        //app('App\Http\Controllers\IntegrationECommerceController')->validateUpdateStockTokped($trHeader->warehouse_id,$orderDetail[$i]->prod_id,$new_stock);
                    }                                        
                }else{//jika data product di detail transaksi tidak ada satupun di tabel inventory
                    for($i=0;$i<count($listProdId);$i++){
                        $addInventorySchema = array(
                            'organization_id' => $orgID->organization_id,
                            'warehouse_id' => $trHeader->warehouse_id,
                            'prod_id' => $orderDetail[$i]->prod_id,
                            'inventory_date' => date(now()),
                            'inventory_price' => $orderDetail[$i]->price,
                            'stock' => $orderDetail[$i]->quantity,
                            'created_by' => $request->user_id,
                            'created_date' => date(now()),
                            'updated_date' => date(now())
                    );
                        $this->addInventory($addInventorySchema);
                    }                                        
                }

                for($i=0;$i<count($listProdId);$i++){//looping total product detail order  
                    $new_price_total = $orderDetail[$i]->price * $orderDetail[$i]->quantity;                     
                    $addStockCardSchema = array(//add stockcard
                        'stock_card_code' => $trHeader->transaction_header_code,
                        'organization_id' => $orgID->organization_id,
                        'warehouse_id' => $trHeader->warehouse_id,
                        'prod_id' => $orderDetail[$i]->prod_id,
                        'stock' => $orderDetail[$i]->quantity,
                        'transaction_type_id' => 2,
                        'price' => $orderDetail[$i]->price,
                        'total_harga' => $new_price_total,
                        'transaction_desc' => $trHeader->transaction_desc,
                        'uom_id' => $orderDetail[$i]->uom_id,
                        'created_by' => $request->user_id,
                        'order_id' => $request->order_id,
                        'created_date' => date(now()),
                    );
                    $this->addStockCard($addStockCardSchema); 
                }
            }
            $org=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
            //potong stok dan push
            app('App\Http\Controllers\InventoryController')->orderMobilePotongStok($request->order_id,$org->parent_organization_id,$org->warehouse_id);                                      
        }

        if(count($listProdId)<1){//jika product di detail transaksi kosong
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
        }else{
            $this->arrivalConfirmationEmail($request->order_id, $request->get('is_agent'));
            return response()->json(['isSuccess' => true, 'message' => 'Sukses konfirmasi data order'], 200);
        }
    }
    //untuk mendapatkan nomor account ketika transaksi belum dibayar
    public function getOrder($billing_number)
    {
        $q = DB::table('10_order AS a')
        ->leftJoin('00_payment_method AS b', 'b.payment_method_id', '=', 'a.payment_method_id')
        ->leftJoin('10_payment AS c', 'a.payment_id', '=', 'c.payment_id')
        ->select('a.order_code', 'b.payment_method_name AS payment_type', 
            DB::raw('DATE_ADD(a.order_date, INTERVAL 3 HOUR) AS expired_date'), 
            'a.account_number', 'c.grandtotal_payment as grand_total')
        ->where('c.invoice_no', $billing_number)
        ->first();

        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        }
    }
    
    //cek pembayaran ke midtrans
    public function getPaymentStatus($billing_code, $order_id, $order_code, $fcm_token, $is_agent, $customer_id, $payment_id){
        // echo $order_id;
        // exit;
        $url = "https://api.sandbox.midtrans.com/v2/".$billing_code."/status";
        
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => "Basic ".base64_encode('SB-Mid-server-PvdDDTyn7QU9vyxoOJtTBvG6')
        ])->get($url);


        if(isset($response['transaction_status'])){
            $orderStatusId = null;
            $invoiceStatusId = null;
            $transactionStatus = $response['transaction_status'];
            $transactionTime = $response['transaction_time'];
            $title = "";
            $message = "";
            $type = "";
            if($transactionStatus != "pending"){
                if($transactionStatus == "settlement"){
                    // echo $transactionStatus;
                    // exit;
                    $orderStatusId = 6;
                    $invoiceStatusId = 2;
                    $title = "Pesanan Anda telah terverifikasi (".$order_code.")";
                    $message = "Terima kasih telah melakukan transaksi di YukMarket! Pesanan anda dengan kode pesanan (".$order_code.") telah terverifikasi. Mohon menunggu!";
                    $type = "new_order";
                    $this->confirmationEmail($payment_id);
                    // $order_history = DB::table('10_order_history')->insertGetId([
                    //     'order_id' => $order_id,
                    //     'order_status_id' => 6,
                    //     'active_flag' => 1
                    // ]);
                    
                }else if($transactionStatus == "deny"){
                    $orderStatusId = 11;
                    $invoiceStatusId = 4;
                    $title = "Pesanan Anda dibatalkan (".$order_code.")";
                    $message = "Mohon maaf pesanan anda dengan kode pesanan (".$order_code.") telah dibatalkan. Yuk Beli lagi!";
                    $type = "transaction";
                    $this->cancellationEmail($payment_id);
                    // $order_history = DB::table('10_order_history')->insertGetId([
                    //     'order_id' => $order_id,
                    //     'order_status_id' => 11,
                    //     'active_flag' => 1
                    // ]);
                }else if($transactionStatus == "expire"){
                    $orderStatusId = 12;
                    $invoiceStatusId = 5;
                    $title = "Pesanan Anda dibatalkan (".$order_code.")";
                    $message = "Mohon maaf pesanan anda dengan kode pesanan (".$order_code.") telah dibatalkan. Yuk Beli lagi!";
                    $type = "transaction";
                    $this->cancellationEmail($payment_id);
                    // $order_history = DB::table('10_order_history')->insertGetId([
                    //     'order_id' => $order_id,
                    //     'order_status_id' => 12,
                    //     'active_flag' => 1
                    // ]);
                }else{
                    $orderStatusId = null;
                }
                
                //update ke db transaksi yang masih pending
                if($orderStatusId != null && $invoiceStatusId != null){
                    $update = DB::table('10_order')
                    ->where('payment_id', $payment_id)
                    ->update([
                        'invoice_status_id' => $invoiceStatusId,
                        'order_status_id' => $orderStatusId,
                        'transfer_date' => $transactionTime
                    ]);

                    $updatePayment = DB::table('10_payment')
                    ->where('payment_id', $payment_id)
                    ->update([
                        'invoice_status_id' => $invoiceStatusId,
                        'payment_date' => $transactionTime
                    ]);
                    
                    $order_history = DB::table('10_order_history')->insertGetId([
                        'order_id' => $order_id,
                        'order_status_id' => $orderStatusId,
                        'active_flag' => 1
                    ]);
    
                }
                if($orderStatusId == 6){
                    $preparation = DB::table('10_preparation')->insertGetId([
                        'preparation_status_id' => 1,
                        'order_id' => $order_id,
                        'active_flag' => 1
                    ]);
                }

                $url = Config::get('fcm.url');
                $server_key = Config::get('fcm.token');
                $headers = array(
                    'Content-Type'=>'application/json',
                    'Authorization'=>'key='.$server_key
                );
                if($fcm_token != null){
                    $parameter = array(
                        "to" => $fcm_token,
                        "notification" => array(
                            "title" => $title,
                            "body" => $message,
                            "message" => "message",
                            "sound" => "default",
                            "badge" => "1",
                            "image" => "null",
                            "icon" => "@mipmap/ic_stat_ic_notification"
                        ),
                        "data" => array(
                            "target" => "MainActivity",
                            "notifId" => "1",
                            "dataId" => "1"
                        )
                    );
                    $response = Http::withHeaders($headers)->post($url, $parameter);
                }
                Notification::insert([
                    "customer_id" => $is_agent == 0 ? $customer_id : null,
                    "user_id" => $is_agent == 1 ? $customer_id : null,
                    "order_id" => $order_id,
                    "message" => $message,
                    "title" => $title,
                    "type" => $type
                ]);
                // print_r($response['success']);
                // exit;
            }
        }
    }

    public function reminderEmail($id){
        // $id = 39;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_cashback.return_amount',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();

        $payment['status'] = "Belum Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['payment'] = $payment;
        $data['currency'] = $this->currency;
        $data['product_array'] = $product_array;
        Mail::send('payment_reminder_email',$data, function ($message) use ($payment)
            {
                $message->from('support@yukmarket.com', 'YukMarket');
                $message->subject('Menunggu Pembayaran ('.$payment->invoice_no.')');
                $message->to($payment->customer_email);
            }
        );
    }

    public function confirmationEmail($id){
        // $id = 39;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_cashback.return_amount',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();

        $payment['status'] = "Sudah Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['payment'] = $payment;
        $data['product_array'] = $product_array;
        Mail::send('payment_confirmation_email',$data, function ($message) use ($payment)
            {
                $message->subject('Konfirmasi Pembayaran ('.$payment->invoice_no.')');
                $message->to($payment->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        return view("payment_confirmation_email", $data);
    }

    public function cancellationEmail($id){
        // $id = 39;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_cashback.return_amount',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();
        
        $payment['status'] = "Tidak Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['payment'] = $payment;
        $data['product_array'] = $product_array;
        Mail::send('cancellation_payment',$data, function ($message) use ($payment)
            {
                $message->subject('Pembatalan Pembayaran ('.$payment->invoice_no.')');
                $message->to($payment->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        return view("cancellation_payment", $data);
    }

    public function arrivalConfirmationEmail($id, $is_agent){
        // $id = 96;
        $fcm_token = null;
        $customer_id = null;
        if($is_agent == 0){
            $transaction = Order::leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('10_order.order_id', $id)->first();
            $email = $transaction->customer_email;
            $fcm_token = $transaction->fcm_token;
            $customer_id = $transaction->buyer_user_id;
        }else{
            $transaction = Order::leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
            ->leftJoin('98_user','98_user.user_id','=','10_order.buyer_user_id')
            ->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('10_order.order_id', $id)
            ->first();
            $email = $transaction->user_email;
            $fcm_token = $transaction->fcm_token;
            $customer_id = $transaction->buyer_user_id;
        }
        // dd($transaction->customer_email, $transaction);
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        $data['logo'] = Config::get('logo.email');
        Mail::send('customer_arrival_confirmation_email',$data, function ($message) use ($transaction, $email)
            {
                $message->subject('Konfirmasi Barang Sampai (Dari Pelanggan Yang Bersangkutan: '.$transaction->order_code.')');
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->to($transaction->customer_email);
                $message->to($email);
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );

        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Pesanan Anda telah dikonfirmasi (".$transaction->order_code.")";
        $message = "Terima kasih telah melakukan transaksi di YukMarket! Pesanan anda dengan kode pesanan (".$transaction->order_code.") sedang diproses. Mohon menunggu!";
        $type = "transaction";
        if($fcm_token != null){
            $parameter = array(
                "to" => $fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $is_agent == 0 ? $customer_id : null,
            "user_id" => $is_agent == 1 ? $customer_id : null,
            "order_id" => $id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
    }
    
    //mendapatkan riwayat transaksi jika status nya pending maka akan di cek status nya ke midtrans
    public function getTransaction(Request $request){
        if($request->get('is_agent') == 0){
            $column_array = ['10_order.order_id',
            '10_order.order_code',
            '00_customer.fcm_token',
            '10_order.order_date',
            '10_order.buyer_user_id',
            '00_customer.customer_name AS buyer_user_name',
            '00_customer.customer_phone_number AS buyer_user_phone_number',
            '00_customer.customer_email AS buyer_user_email',
            '10_order.payment_method_id',
            '00_payment_method.payment_method_name',
            '10_order.destination_address AS destination_address_id',
            '00_address.address_name AS destination_address_name',
            '10_order.shipment_method_id',
            '00_shipment_method.shipment_method_name',
            DB::raw('(CASE WHEN 00_order_status.parent_order_status_id IS NULL THEN 00_order_status.order_status_name ELSE (SELECT os.order_status_name FROM 00_order_status AS os WHERE os.order_status_id = 00_order_status.parent_order_status_id) END) AS order_status_name'),
            DB::raw('(CASE WHEN 00_order_status.parent_order_status_id IS NULL THEN 00_order_status.order_status_id ELSE (SELECT os.order_status_id FROM 00_order_status AS os WHERE os.order_status_id = 00_order_status.parent_order_status_id) END) AS order_status_id'),
            '10_order.invoice_status_id',
            '00_invoice_status.invoice_status_name',
            // '10_order.admin_fee',
            // '10_order.admin_fee_percentage',
            '10_order.voucher_id',
            // '10_order.voucher_amount',
            '10_order.is_fixed',
            '00_vouchers.voucher_code',
            '00_vouchers.max_value_price',
            '10_order.max_price',
            '10_order.active_flag AS order_active_status',
            '00_address.address_id',
            '00_address.contact_person',
            '00_address.phone_number',

            '00_address.address_detail',
            '00_address.kabupaten_kota_id',
            '00_kabupaten_kota.kabupaten_kota_name',
            '00_address.kecamatan_id',
            '00_kecamatan.kecamatan_name',
            '00_address.kelurahan_desa_id',
            '00_provinsi.provinsi_name',
            '00_kelurahan_desa.kelurahan_desa_name',
            '00_kelurahan_desa.kode_pos', 
            '10_order.shipment_price', 
            DB::raw('(SELECT SUM(10_order_detail.quantity * 10_order_detail.price) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS `total_order`'), 
            '10_order.pricing_include_tax','10_order.national_income_tax',
            '10_payment.invoice_no as billing_code',
            '10_payment.payment_id',
            '10_payment.grandtotal_payment',
            '10_payment.admin_fee',
            '10_payment.voucher_amount',
            '10_cashback.return_amount',
            '10_order.is_agent'
            ];

            $transaction = DB::table("10_order")
            ->select($column_array)
            ->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')
            ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
            ->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')
            ->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')
            ->distinct()
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
            ->where('10_order.buyer_user_id', $request->input('user_id'))
            ->where('10_order.online_flag', 1)
            ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
            ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
            ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
            ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
            ->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
            ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')
            ->leftJoin('10_payment', '10_payment.payment_id', '=', '10_order.payment_id')
            ->leftJoin('10_cashback', '10_cashback.order_id', '=', '10_order.order_id');
            $start_full_date = "";
            $start_date = "";
            $start_month = "";
            $start_year = "";
            $end_full_date = "";
            $end_date = "";
            $end_month = "";
            $end_year = "";
            if($request->input('dateFrom') != ""){
                $start_full_date = $request->input('dateFrom');
                $start_date = explode('-',$request->input('dateFrom'))[0];
                $start_month = explode('-',$request->input('dateFrom'))[1];
                $start_year = explode('-',$request->input('dateFrom'))[2];
                $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
                
                $transaction->where(DB::raw('CAST(10_order.created_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
            }
            if($request->input('dateTo') != ""){
                $end_full_date = $request->input('dateTo');
                $end_date = explode('-',$request->input('dateTo'))[0];
                $end_month = explode('-',$request->input('dateTo'))[1];
                $end_year = explode('-',$request->input('dateTo'))[2];
                $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

                $transaction->where(DB::raw('CAST(10_order.created_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
            }
            // echo $request->input('dateFrom').'<br/>';
            // echo $request->input('dateTo');
            // exit;
            if($request->input('order_id') != ""){
                $transaction->where('10_order.order_id', $request->input('order_id'));
            }
            if($request->input('sort_by') == 2){
                $transaction->orderBy('10_order.order_date','DESC');
            }else{
                $transaction->orderBy('10_order.order_date','ASC');
            }
            $transaction = $transaction->get();
            $data = array (
                "data"=>$transaction
            );
            if(count($data['data'])<1){
                return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 200);
            }
            
            //agent
        }else{
             $column_array = ['10_order.order_id',
            '10_order.order_code',
            '98_user.fcm_token',
            '10_order.order_date',
            // '10_order.agent_user_id',
            '10_order.buyer_user_id',
            '98_user.user_name AS buyer_user_name',
            '98_user.user_phone_number AS buyer_user_phone_number',
            '98_user.user_email AS buyer_user_email',
            '10_order.payment_method_id',
            '00_payment_method.payment_method_name',
            '10_order.destination_address AS destination_address_id',
            '00_address.address_name AS destination_address_name',
            '10_order.shipment_method_id',
            '00_shipment_method.shipment_method_name',
            DB::raw('(CASE WHEN 00_order_status.parent_order_status_id IS NULL THEN 00_order_status.order_status_name ELSE (SELECT os.order_status_name FROM 00_order_status AS os WHERE os.order_status_id = 00_order_status.parent_order_status_id) END) AS order_status_name'),
            DB::raw('(CASE WHEN 00_order_status.parent_order_status_id IS NULL THEN 00_order_status.order_status_id ELSE (SELECT os.order_status_id FROM 00_order_status AS os WHERE os.order_status_id = 00_order_status.parent_order_status_id) END) AS order_status_id'),
            '10_order.invoice_status_id',
            '00_invoice_status.invoice_status_name',
            // '10_order.admin_fee',
            // '10_order.admin_fee_percentage',
            '10_order.voucher_id',
            // '10_order.voucher_amount',
            '10_order.is_fixed',
            '00_vouchers.voucher_code',
             '00_vouchers.max_value_price',
            '10_order.max_price',
            '10_order.active_flag AS order_active_status',
             '00_address.address_id',
            '00_address.contact_person',
            '00_address.phone_number',
            '00_address.address_detail',
            '00_address.kabupaten_kota_id',
            '00_kabupaten_kota.kabupaten_kota_name',
            '00_address.kecamatan_id',
            '00_kecamatan.kecamatan_name',
            '00_address.kelurahan_desa_id',
            '00_provinsi.provinsi_name',
            '00_kelurahan_desa.kelurahan_desa_name',
            '00_kelurahan_desa.kode_pos', 
            '10_order.shipment_price', 
            DB::raw('(SELECT SUM(10_order_detail.quantity * 10_order_detail.price) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS `total_order`'), 
            '10_order.pricing_include_tax','10_order.national_income_tax',
            '10_payment.invoice_no as billing_code',
            '10_payment.payment_id',
            '10_payment.grandtotal_payment',
            '10_payment.admin_fee',
            '10_payment.voucher_amount',
            '10_cashback.return_amount',
            '10_order.is_agent'
        ];

            $transaction = DB::table("10_order")
            ->select($column_array)
            ->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')
            ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
            ->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')
            // ->leftJoin('98_user','98_user.user_id','=','10_order.agent_user_id')
            ->leftJoin('98_user','98_user.user_id','=','10_order.buyer_user_id')
            ->leftJoin('00_organization', '98_user.organization_id', '=', '00_organization.organization_id')
            ->distinct()
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
            ->where('00_organization.organization_type_id', 2)
            ->where('10_order.buyer_user_id', $request->input('user_id'))
            ->where('10_order.is_agent', 1)
            ->where('10_order.online_flag', 1)
            ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
            ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
            ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
            ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
            ->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
            ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')
            ->leftJoin('10_payment', '10_payment.payment_id', '=', '10_order.payment_id')
            ->leftJoin('10_cashback', '10_cashback.order_id', '=', '10_order.order_id');
            $start_full_date = "";
            $start_date = "";
            $start_month = "";
            $start_year = "";
            $end_full_date = "";
            $end_date = "";
            $end_month = "";
            $end_year = "";
            if($request->input('dateFrom') != ""){
                $start_full_date = $request->input('dateFrom');
                $start_date = explode('-',$request->input('dateFrom'))[0];
                $start_month = explode('-',$request->input('dateFrom'))[1];
                $start_year = explode('-',$request->input('dateFrom'))[2];
                $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
                
                $transaction->where(DB::raw('CAST(10_order.created_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
            }
            if($request->input('dateTo') != ""){
                $end_full_date = $request->input('dateTo');
                $end_date = explode('-',$request->input('dateTo'))[0];
                $end_month = explode('-',$request->input('dateTo'))[1];
                $end_year = explode('-',$request->input('dateTo'))[2];
                $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

                $transaction->where(DB::raw('CAST(10_order.created_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
            }
            // echo $request->input('dateFrom').'<br/>';
            // echo $request->input('dateTo');
            // exit;
            if($request->input('order_id') != ""){
                $transaction->where('10_order.order_id', $request->input('order_id'));
            }
            if($request->input('sort_by') == 2){
                $transaction->orderBy('10_order.order_date','DESC');
            }else{
                $transaction->orderBy('10_order.order_date','ASC');
            }
            $transaction = $transaction->get();
            
            $data = array (
                "data"=>$transaction
            );
            if(count($data['data'])<1){
                return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 200);
            }
        }

        $transaction_array = array();
        $q = DB::table("10_order")->select('shipment_price', 'order_id', 'order_code', 'grand_total')
                ->where('payment_id', $transaction[0]->payment_id)
                ->get();
        $sum_sipmen = 0;
        $sum_subtotal = 0;
        $warehouse_payment = array();
        for ($b=0; $b < count($q); $b++) {
            $sum_sipmen += $q[$b]->shipment_price;
            $sum_subtotal += $q[$b]->grand_total;
            $obj = array(
                "order_id" => $q[$b]->order_id,
                "order_code" => $q[$b]->order_code,
                "subtotal" => $this->currency->convertToCurrency($q[$b]->grand_total) == "" ? "0" : $this->currency->convertToCurrency($q[$b]->grand_total)
            );

            array_push($warehouse_payment, $obj);
        }
        for ($b=0; $b < count($transaction); $b++) {
            $total_order = $transaction[$b]->total_order == null ? 0 : $transaction[$b]->total_order;
            $delivery_fee = $transaction[$b]->shipment_price;
            // $sum_sipmen += $delivery_fee;
            $max_provided_price = 0;
            $voucher_amount = 0;
            $admin_fee = 0;
            $admin_fee_percentage = 0;
            $grand_total = $transaction[$b]->total_order;
            $discount = 0;

            $tax_basis = ROUND($grand_total / 1.1);
            $national_income_tax = $grand_total - $tax_basis;
            if($transaction[$b]->pricing_include_tax == 0){
                $tax_basis = 0;
                $national_income_tax = ROUND($grand_total * $transaction[$b]->national_income_tax);
                $grand_total = $grand_total + $national_income_tax;
            }

            if($transaction[$b]->voucher_id != null){
                $max_provided_price = $transaction[$b]->max_price;
                $voucher_amount = $transaction[$b]->voucher_amount;
                if($transaction[$b]->is_fixed == 'Y'){
                    $discount = $transaction[$b]->voucher_amount;
                }else{
                    $discount = $transaction[$b]->total_order * ($transaction[$b]->voucher_amount / 100);
                    if($discount > $max_provided_price){
                        $discount = $max_provided_price;
                    }
                }
                $grand_total = $grand_total - $discount;
            }

            $grand_total = $grand_total + $delivery_fee;

            $all_admin_fee = 0;

            if($transaction[$b]->admin_fee != null){
                $admin_fee = $transaction[$b]->admin_fee;
                $all_admin_fee = $transaction[$b]->admin_fee;
            }
            
            // if($transaction[$b]->admin_fee_percentage != null){
            //     $admin_fee_percentage = $transaction[$b]->admin_fee_percentage / 100;
            //     $all_admin_fee = ($grand_total + $admin_fee_percentage);
            // }

            // $grand_total = $grand_total + $all_admin_fee;
            
            
            //cek ke midtrans untuk transaksi yang masih pending
            if($transaction[$b]->order_status_id == 5){
                $this->getPaymentStatus($transaction[$b]->billing_code, $transaction[$b]->order_id, $transaction[$b]->order_code, $transaction[$b]->fcm_token, $transaction[$b]->is_agent, $transaction[$b]->buyer_user_id, $transaction[$b]->payment_id);
            }

            $cek_complaint = DB::table('00_issue')
            ->select('00_issue.issue_id', 
                '00_issue.ticketing_num', 
                '00_issue.customer_id',
                '00_issue.order_id',
                '00_issue.issue_notes'
            )
            ->leftJoin('10_order', '00_issue.order_id', '=', '10_order.order_id')
            ->where('00_issue.order_id', $transaction[$b]->order_id)
            ->get();
            // dd($cek_complaint);
                 //set header is_varian, price, stok, dan diskon         
           if(isset($cek_complaint) && count($cek_complaint) > 0) { 
                $is_complaint = true;
           } else { 
                $is_complaint = false;
           }

           $cek_cashback = DB::table('10_cashback')
            ->select('10_cashback.cashback_id', 
                '10_cashback.order_id', 
                '10_cashback.return_amount'
            )
            ->leftJoin('10_order', '10_cashback.order_id', '=', '10_order.order_id')
            ->where('10_cashback.payment_id', $transaction[$b]->payment_id)
            ->where('10_cashback.transaction_flag', 0)
            ->first();
            if(isset($cek_cashback)) { 
                $is_cashback = true;
           } else { 
                $is_cashback = false;
           }
           
            $obj = array(
                "order_id" => $transaction[$b]->order_id,
                "order_code" => $transaction[$b]->order_code,
                "billing_code" => $transaction[$b]->billing_code,
                "order_date" => date("Y-m-d H:i:s",strtotime($transaction[$b]->order_date)),
                "address_id" => $transaction[$b]->address_id,
                "buyer_user_id" => $transaction[$b]->buyer_user_id,
                "buyer_user_name" => $transaction[$b]->contact_person,
                "buyer_user_email" => $transaction[$b]->buyer_user_email,
                "buyer_user_phone_number" => $transaction[$b]->phone_number,
                "payment_method_id" => $transaction[$b]->payment_method_id,
                "payment_method_name" => $transaction[$b]->payment_method_name,
                'payment_status_id' => $transaction[$b]->invoice_status_id,
                'payment_status' => $transaction[$b]->invoice_status_name,
                "destination_address" => array(
                    "status" => $transaction[$b]->destination_address_name == null ? "" : $transaction[$b]->destination_address_name,
                    "detail" => $transaction[$b]->address_detail == null ? "" : $transaction[$b]->address_detail,
                    "district" => $transaction[$b]->kabupaten_kota_name == null ? "" : $transaction[$b]->kabupaten_kota_name,
                    "regency" => $transaction[$b]->kecamatan_name == null ? "" : $transaction[$b]->kecamatan_name,
                    "village" => $transaction[$b]->kelurahan_desa_name == null ? "" : $transaction[$b]->kelurahan_desa_name,
                    "province" => $transaction[$b]->provinsi_name == null ? "" : $transaction[$b]->provinsi_name,
                    "postal_code" => $transaction[$b]->kode_pos == null ? 0 : $transaction[$b]->kode_pos
                ),
                "shipment_method_id" => $transaction[$b]->shipment_method_id,
                "shipment_method_name" => $transaction[$b]->shipment_method_name,
                "order_status_id" => $transaction[$b]->order_status_id,
                "order_status_name" => $transaction[$b]->order_status_name,
                "order_active_status" => $transaction[$b]->order_active_status,
                "total_price" => $this->currency->convertToCurrency($transaction[$b]->total_order),
                "voucher" => array(
                    "code" => ($transaction[$b]->voucher_id != null ? $transaction[$b]->voucher_code : ""),
                    "amount" => $transaction[$b]->voucher_id != null ? ($transaction[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($voucher_amount) : (string)($voucher_amount/100)) : "",
                    "max_provided_price" => $transaction[$b]->voucher_id != null ? $this->currency->convertToCurrency($max_provided_price) : "",
                    "is_fixed" => $transaction[$b]->voucher_id != null ? $transaction[$b]->is_fixed : ""
                ),
                "pricing_include_tax" => $transaction[$b]->pricing_include_tax == 0 ? false : true,
                "tax_basis" => $this->currency->convertToCurrency($tax_basis),
                "national_income_tax_percentage" => number_format($transaction[$b]->national_income_tax,2), 
                "national_income_tax" => $this->currency->convertToCurrency($national_income_tax),
                "discount" => $this->currency->convertToCurrency($discount) == "" ? "0" : $this->currency->convertToCurrency($discount),
                "delivery_fee" => $this->currency->convertToCurrency($delivery_fee),
                "admin_fee" => $this->currency->convertToCurrency($admin_fee) == "" ? "0" : $this->currency->convertToCurrency($admin_fee),
                "admin_fee_percentage" => ($admin_fee_percentage * 100),
                "total_order" => ($this->currency->convertToCurrency($grand_total) == "" ? "0" : $this->currency->convertToCurrency($grand_total)),
                "is_complaint" => $is_complaint,
                "grandtotal_payment" => $this->currency->convertToCurrency($transaction[$b]->grandtotal_payment) == "" ? "0" : $this->currency->convertToCurrency($transaction[$b]->grandtotal_payment),
                "detail_payment" => array(
                    "is_cashback" => $is_cashback,
                    "cashback_amount" => $is_cashback == false ? "0" : $this->currency->convertToCurrency($cek_cashback->return_amount),
                    "voucher_amount" => $this->currency->convertToCurrency($transaction[$b]->voucher_amount) == "" ? "0" : $this->currency->convertToCurrency($transaction[$b]->voucher_amount),
                    "total_shipment" => $this->currency->convertToCurrency($sum_sipmen),
                    "payment_id" => $transaction[$b]->payment_id,
                    "payment_amount" => $this->currency->convertToCurrency($admin_fee) == "" ? "0" : $this->currency->convertToCurrency($admin_fee),
                    "subtotal_bayar" => $this->currency->convertToCurrency($sum_subtotal) == "" ? "0" : $this->currency->convertToCurrency($sum_subtotal),
                    "total_bayar" => $this->currency->convertToCurrency($transaction[$b]->grandtotal_payment) == "" ? "0" : $this->currency->convertToCurrency($transaction[$b]->grandtotal_payment),
                    "warehouse_payment" => $warehouse_payment
                )
            );
            array_push($transaction_array, $obj);
        }


            if($request->input('order_id') != ""){
                $transaction_array = $transaction_array[0];
            }

            if(!$transaction){
                return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $transaction_array], 200);
            }
    }

    public function getParentOrderStatus(Request $request){
        $order_status = DB::table('00_order_status')
        ->whereIn('order_status_id',[1,2,3,4])
        ->get();

        $order_history = DB::table('10_order')
        ->select('10_order.order_id','10_order.order_status_id','00_order_status.parent_order_status_id')
        ->join('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
        ->where('order_id', $request->input('order_id'))
        ->first();
        
        $order_status_flag = 0;
        if($order_history != null){
            $order_status_flag = $order_history->parent_order_status_id;
        }
        $order_status_array = array();

        for ($b=0; $b < count($order_status); $b++) { 
            $obj = array(
                "order_status_id" => $order_status[$b]->order_status_id,
                "order_status_name" => $order_status[$b]->order_status_name,
                "order_status_logo" => $order_status[$b]->order_status_id <= $order_status_flag ? asset($order_status[$b]->order_status_active_logo) : asset($order_status[$b]->order_status_inactive_logo),
                "order_status_description" => $order_status[$b]->order_status_desc
            );

            array_push($order_status_array, $obj);
        }
        
        if(!$order_status){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $order_status_array], 200);
        }
    }

    public function getTrackingHistory(Request $request){
        $order = DB::table('10_order_history')
        ->select('10_order.order_id','00_order_status.order_status_id','00_order_status.order_status_name','00_order_status.order_status_desc','10_order_history.updated_date')
        ->join('10_order','10_order.order_id','=','10_order_history.order_id')
        ->join('00_order_status','00_order_status.order_status_id','=','10_order_history.order_status_id')
        ->where('10_order_history.order_id', $request->input('order_id'))
        ->get();
        $tracking_history = array();

        for ($b=0; $b < count($order); $b++) { 
            $obj = array(
                "order_id" => $order[$b]->order_id,
                "order_status_id" => $order[$b]->order_status_id,
                "order_status_name" => $order[$b]->order_status_name,
                "order_status_description" => $order[$b]->order_status_desc,
                "created_date" => date("Y-m-d H:i:s",strtotime($order[$b]->updated_date))
            );

            array_push($tracking_history, $obj);
        }

        if(!$order){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $tracking_history], 200);
        }
    }
    public function getProductByOrderId($id){
        return OrderDetail::select('10_order_detail.prod_id','10_order_detail.quantity')
        ->where('10_order_detail.order_id',$id)
        ->get();
    }
    public function getOrderCode($orderId){
        $order=DB::table('10_order')
        ->where('order_id',$orderId)
        ->select('order_code')
        ->first();
        return $order;
    }
    public function cancelOrder(Request $req){
        $payment=DB::table('10_order as o')
        ->join('10_payment as p','p.payment_id','o.payment_id')
        ->where('o.order_id',$req->order_id)
        ->select('p.payment_id','p.midtrans_transaction_id','p.invoice_no')
        ->first();
        $url = "https://api.sandbox.midtrans.com/v2/".$payment->midtrans_transaction_id."/cancel";
        $token="Basic ".base64_encode('SB-Mid-server-PvdDDTyn7QU9vyxoOJtTBvG6');
        $response=Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $token,
        ])->timeout(10)->retry(2, 2000)->post($url)->json();
        // dd($response);
        if($response['status_code']!=200){
            return response()->json(['isSuccess' => false, 'message' => 'Permintaan gagal diproses, coba beberapa saat lagi!'], 500);
        }else{
            DB::table('10_order')
            ->where('payment_id',$payment->payment_id)
            ->update([
                'order_status_id'=>11
            ]);
            //update status DB cancel order
            $order=DB::table('10_order')
            ->where('payment_id',$payment->payment_id)
            ->select('order_id','warehouse_id')
            ->get();
            for($i=0;$i<count($order);$i++){
                DB::table('10_order_history')
                ->insert([
                    'order_id'=>$order[$i]->order_id,
                    'order_status_id'=>11,
                    'active_flag'=>1,
                ]);
                $prods=$this->getProductByOrderId($order[$i]->order_id,$order[$i]->warehouse_id);
                for($a=0;$a<count($prods);$a++){
                    $prod=app('App\Http\Controllers\ProductController')->checkProduct($prods[$a]->prod_id);
                    if($prod->stockable_flag==1){//---------------jika stockable
                        $inventoryProductId=$prod->prod_id;
                        $bookingQty=$prods[$a]->quantity;
                    }else{//-----------------------------------------------jika non stockable ambil dari parent
                        $inventoryProductId=$prod->parent_prod_id;
                        //------------------------------------------------convert stock
                        $prodParent=app('App\Http\Controllers\ProductController')->checkProduct($checkProduct->parent_prod_id);
                        $bookingQty=app('App\Http\Controllers\ProductController')->convertBookingStock($prodParent->uom_value,$prodParent->uom_id,$checkProduct->uom_value,$checkProduct->uom_id,$prods[$a]->quantity);
                    }
                    $checkInventory=DB::table('00_inventory')
                    ->where([
                        ['organization_id',1],
                        ['warehouse_id',$order[$i]->warehouse_id],
                        ['prod_id',$inventoryProductId]
                    ])
                    ->select('booking_qty')
                    ->first();
                    $newBookingStock=$checkInventory->booking_qty-$bookingQty;
                    DB::table('00_inventory')
                    ->where([
                        ['organization_id',1],
                        ['warehouse_id',$order[$i]->warehouse_id],
                        ['prod_id',$inventoryProductId]
                    ])
                    ->update([
                        'booking_qty'=>$newBookingStock
                    ]);
                }
            }
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => []], 200);
        }
    }
}
