<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\Helpers\Currency;
use App\Helpers\Transaction;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function getNotification(Request $request)
    {   
    	if($request->get('is_agent') == 0){      
	        $q = DB::table('00_notification')
		      ->select('notification_id',
		      	'customer_id',
		      	'title', 
		      	'message', 
		      	'type', 
		      	'is_read',
		      	'created_date',
		      	'order_id'
		      )
		      ->where('customer_id', $request->user_id);
		    if($request->sort_by != null || $request->sort_by != ''){
		            $q->where('type', $request->sort_by);
		      }
		    $start_full_date = "";
            $start_date = "";
            $start_month = "";
            $start_year = "";
            $end_full_date = "";
            $end_date = "";
            $end_month = "";
            $end_year = "";
	        if($request->input('date_from') != ""){
                $start_full_date = $request->input('date_from');
                $start_date = explode('-',$request->input('date_from'))[0];
                $start_month = explode('-',$request->input('date_from'))[1];
                $start_year = explode('-',$request->input('date_from'))[2];
                $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
                
                $q->where(DB::raw('CAST(00_notification.created_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
	            }
	        if($request->input('date_to') != ""){
                $end_full_date = $request->input('date_to');
                $end_date = explode('-',$request->input('date_to'))[0];
                $end_month = explode('-',$request->input('date_to'))[1];
                $end_year = explode('-',$request->input('date_to'))[2];
                $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

                $q->where(DB::raw('CAST(00_notification.created_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
	        }
		    
		    $q->orderBy('created_date', 'DESC');

	     $result = $q->get();

    	}else{
			$q = DB::table('00_notification')
		      ->select('notification_id',
		      	'user_id',
		      	'title', 
		      	'message', 
		      	'type', 
		      	'is_read',
		      	'created_date',
		      	'order_id'
			  	)
			    ->where('user_id', $request->user_id);
			    if($request->sort_by != null || $request->sort_by != ''){
			            $q->where('type', $request->sort_by);
			    }
			    $start_full_date = "";
	            $start_date = "";
	            $start_month = "";
	            $start_year = "";
	            $end_full_date = "";
	            $end_date = "";
	            $end_month = "";
	            $end_year = "";
		        if($request->input('date_from') != ""){
	                $start_full_date = $request->input('date_from');
	                $start_date = explode('-',$request->input('date_from'))[0];
	                $start_month = explode('-',$request->input('date_from'))[1];
	                $start_year = explode('-',$request->input('date_from'))[2];
	                $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
	                
	                $q->where(DB::raw('CAST(00_notification.created_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
		            }
		        if($request->input('date_to') != ""){
	                $end_full_date = $request->input('date_to');
	                $end_date = explode('-',$request->input('date_to'))[0];
	                $end_month = explode('-',$request->input('date_to'))[1];
	                $end_year = explode('-',$request->input('date_to'))[2];
	                $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

	                $q->where(DB::raw('CAST(00_notification.created_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
		        }
		        $q->orderBy('created_date', 'DESC');
		     $result = $q->get();
    	}

    	if(!$q){
    		return response()->json(['isSuccess' => false, 'message' => 'Fetch data failed'], 500);
    	}else{
    		return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $result], 200);
    	}
    }

    public function updateNotification(Request $request)
    {
    	if($request->get('is_agent') == 0){
    		$update = DB::table('00_notification')
        	->where([
                ['customer_id', $request->user_id],
                ['notification_id', $request->notif_id]
            ])
        	->update([
    			'is_read' => 1
            ]);
            
        }else{
            $update = DB::table('00_notification')
            ->where([
                ['user_id', $request->user_id],
                ['notification_id', $request->notif_id]
            ])
            ->update([
                'is_read' => 1
            ]);
        }

    	if(!$update){
    		return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    	}else{
    		return response()->json(['isSuccess' => true, 'message' => 'Berhasil di Update'], 200);
    	}
	}

}
