<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Product;
use App\Console\Commands\IntegrationECommerceCommand;

class ToolsGenerateMasterProductController extends Controller
{
    public function generate($type){
    	$shopId=$this->getShop();
    	//---------------------------get data product ecommerce
    	switch ($type) {
    		case 'parent':
	    		$prodEcommerce=DB::table('00_product_ecommerce')
		    	->select('prod_ecom_id','prod_ecom_code','ecommerce_id','organization_id','prod_ecom_name','description','price','uom_value','uom_id','stock','active_flag','category_id','prod_image')
		    	->where('organization_id',$shopId->organization_id)
		    	->whereNull('variant_id')
		    	->whereNull('prod_id')
		    	->get();	    			
    			break;
    		case 'variant':
				$prodEcommerce=DB::table('00_product_ecommerce')				
		    	->select('prod_ecom_id','prod_ecom_code','ecommerce_id','organization_id','prod_ecom_name','description','price','uom_value','uom_id','stock','active_flag','category_id','variant_id','prod_image')
		    	->where('organization_id',$shopId->organization_id)
		    	->whereNotNull('variant_id')
		    	->whereNull('prod_id')
		    	->get();	   
    			break;
    		default:
    			break;
    	}
    	if(count($prodEcommerce)>0){
	    	for($i=0;$i<count($prodEcommerce);$i++){    		
	    		//---------------------------------add SKU
	    		$skuId=DB::table('00_product_sku')
		    	->insertGetId([
		    		'product_sku_desc'=>$prodEcommerce[$i]->prod_ecom_name,
		    		'active_flag'=>$prodEcommerce[$i]->active_flag,
		    		'product_sku_name'=>$prodEcommerce[$i]->ecommerce_id.'-'.$prodEcommerce[$i]->organization_id.'-'.$prodEcommerce[$i]->prod_ecom_code
		    	]);
		    	echo "\nSKU :".$prodEcommerce[$i]->prod_ecom_name.' ditambahkan';
		    	//------------------get categoryId
		    	$categoryId=app('App\Http\Controllers\CategoryPerEcommerceController')->getCategoryIdEcommerce($prodEcommerce[$i]->category_id);        
		    	// dd($prodEcommerce[$i]->prod_ecom_name.'|'.$categoryId);
		    	//---------------------add product
		    	switch ($type) {
		    		case 'parent':	 
			    		$addProduct = array(							
							'prod_name'=>$prodEcommerce[$i]->prod_ecom_name,
							'prod_code'=>$prodEcommerce[$i]->ecommerce_id.'-'.$prodEcommerce[$i]->organization_id.'-'.$prodEcommerce[$i]->prod_ecom_code,
							'prod_desc'=>$prodEcommerce[$i]->description,
							'prod_price'=>$prodEcommerce[$i]->price,
							'stock'=>$prodEcommerce[$i]->stock == null ? 0 :$prodEcommerce[$i]->stock,
							'product_sku_id'=>$skuId,
			                'uom_value'=>$prodEcommerce[$i]->uom_value,
			                'uom_id'=>$prodEcommerce[$i]->uom_id,
							'active_flag'=>$prodEcommerce[$i]->active_flag,
			                'category_id'=>$categoryId,
			                'prod_image'=>$prodEcommerce[$i]->prod_image,
						);   			
		    			break;
		    		case 'variant':
		    		$parentProduct=app('App\Http\Controllers\ProductECommerceController')->getParentProduct($prodEcommerce[$i]->variant_id,IntegrationECommerceCommand::$ecommerceId);
		    			$addProduct = array(							
							'prod_name'=>$prodEcommerce[$i]->prod_ecom_name,
							'prod_code'=>$prodEcommerce[$i]->ecommerce_id.'-'.$prodEcommerce[$i]->organization_id.'-'.$prodEcommerce[$i]->prod_ecom_code,
							'prod_desc'=>$prodEcommerce[$i]->description,
							'prod_price'=>$prodEcommerce[$i]->price,
							'stock'=>$prodEcommerce[$i]->stock == null ? 0 :$prodEcommerce[$i]->stock,
							'product_sku_id'=>$skuId,
			                'uom_value'=>$prodEcommerce[$i]->uom_value,
			                'uom_id'=>$prodEcommerce[$i]->uom_id,
							'active_flag'=>$prodEcommerce[$i]->active_flag,
			                'category_id'=>$categoryId,
			                'variant_id'=>$parentProduct->prod_id,
			                'prod_image'=>$prodEcommerce[$i]->prod_image,
						);
		    			break;
		    		default:
		    			# code...
		    			break;
		    	}		    	
		    	$getIdProduct=app('App\Http\Controllers\ProductController')->addProduct($addProduct);
		    	echo "\nProduct :".$prodEcommerce[$i]->prod_ecom_name.' ditambahkan';

		    	// DB::table('00_product_sku')
		    	// ->where('product_sku_id',$skuId)
		    	// ->update([
		    	// 	'product_sku_name'=>'SKU-'.$getIdProduct
		    	// ]);
		    	// echo "\nSKU :".$prodEcommerce[$i]->prod_ecom_name.' diupdate';
				DB::table('00_product_ecommerce')
				->where('prod_ecom_code',$prodEcommerce[$i]->prod_ecom_code)
				->update([
					'prod_id'=>$getIdProduct
				]);			
				echo "\nProduct :".$prodEcommerce[$i]->prod_ecom_name.' connected with product ecommerce';
				//----------------add inventory
				// $addInventory = array(
				// 	'prod_id'=>$getIdProduct,				
				// 	'stock'=>$prodEcommerce[$i]->stock == null ? 0 :$prodEcommerce[$i]->stock,
				// 	'inventory_price'=>$prodEcommerce[$i]->price,
				// 	'organization_id'=>$prodEcommerce[$i]->ecommerce_id,
				// 	'warehouse_id'=>0
				// );									
				// app('App\Http\Controllers\InventoryController')->addInventoryFromTokoped($addInventory);

				// echo "\nInventory :".$prodEcommerce[$i]->prod_ecom_name.' ditambahkan';

				//--------------------------product warehouse
				//-------------------------get warehouse id
				$warehouseId=app('App\Http\Controllers\OrganizationController')->getWarehouse($prodEcommerce[$i]->ecommerce_id);
				app('App\Http\Controllers\ProductWarehouseController')->addProductWarehouse($getIdProduct,$warehouseId->warehouse_id,$prodEcommerce[$i]->active_flag,);
				echo "\nWarehouse :".$prodEcommerce[$i]->prod_ecom_name.' ditambahkan';
	    	}    	
	    }
    }
    public function getShop(){
    	$shop=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
    	return $shop;
    }

}
