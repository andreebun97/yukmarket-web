<?php

namespace App\Http\Controllers;

use App\AuthComp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Helpers\Currency;
use App\Helpers\UomConvert;

class CategoryProductController extends Controller
{
    protected $currency;

    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
    }

    //  public function getCategory()
    // {
    //     // $q = DB::table('00_tag')
    //     // ->select('tag_id', 'tag_name', 'tag_image')
    //     // ->orderBy('tag_name', 'ASC')
    //     // ->get();

    //     // $data[] = [
    //     //     'category_id' => 0,
    //     //     'category_name' => 'Semua',
    //     //     'category_image' => asset('img/uploads/category/5f43f4dde57a0.png')
    //     // ];
    //     $url = "http://35.198.248.188:8280/api/mobile/tag/tag";
    //     // $client = new GuzzleHttp\Client();
    //     // $res = $client->get($url);
    //     $json = json_decode(file_get_contents($url), true);
    //     // $request = Http::withHeaders([
    //     //     'Accept' => 'application/json',
    //     //     'Content-Type' => 'application/json'
    //     // ])->get($url);
    //     // $response = json_decode($request->getBody()->getContents(), true);
    //     // dd($json);
    //     // foreach ($response as $key) {
    //     //     $datas = [
    //     //         'category_id' => $key->category_id,
    //     //         'category_name' => $key->category_name,
    //     //         'category_image' => $key->category_image == null ? null : asset($key->category_image)
    //     //     ];
    //     //     array_push($response, $datas);
    //     // }

    //     if(!$json){
    //         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
    //     }else{
    //         return response()->json($json, 200);
    //     }
    // }

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------    
    public function getCategory(){
        $data[] = [
            'category_id' => 0,
            'category_name' => 'Semua',
            'category_image' => asset('img/uploads/category/5ee303602db40.png')
        ];

        //$q = DB::table('00_product AS p')
            //->join('00_product_tag AS pt', 'pt.prod_id', '=', 'p.prod_id')
            //->join('00_tag AS t', 't.tag_id', '=', 'pt.tag_id')
            //->select('p.category_id', DB::raw('t.tag_name AS category_name'), DB::raw('t.tag_image AS category_image'))
            // ->groupBy('p.category_id')
            //->orderBy('p.category_id','ASC')
            //->get();
            $q = DB::table('00_tag as t')
             ->select('t.tag_id', DB::raw('t.tag_name AS category_name'), DB::raw('t.tag_image AS category_image'))
                ->where('active_flag', 1)
                ->get();
            // dd($q);
        for($i=0;$i<count($q);$i++) {
            $datas = [
                'category_id' => $q[$i]->tag_id,
                'category_name' => $q[$i]->category_name,
                'category_image' =>asset($q[$i]->category_image)
            ];
            array_push($data, $datas);
        }

        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found', 'data'=>[]], 404);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $data], 200);
        }

    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------


    // public function getProductByCategory(Request $request)
    // {
    //     $q = DB::table('00_product_category AS z')
    //     ->leftJoin('00_product AS a', 'a.prod_id', '=', 'z.prod_id')
    //     ->leftJoin('00_supplier AS b', 'b.supl_id', '=', 'a.supl_id')
    //     ->leftJoin('00_brand AS c', 'c.brand_id', '=', 'a.brand_id')
    //     ->leftJoin('00_uom AS d', 'd.uom_id', '=', 'a.uom_id')
    //     ->select('a.prod_id', 'a.prod_code', 'a.prod_name', 'b.supl_name', 'c.brand_name', 'a.prod_desc', 'a.prod_weight', 'a.prod_dim_length', 'a.prod_dim_width', 'a.prod_dim_height', 'a.preorder_flag', 'a.price', 'd.uom_name AS satuan')
    //     ->where([
    //         ['z.category_id', $request->route('id')],
    //         ['a.active_flag', 'Y']
    //     ])
    //     ->orderBy('a.created_date', 'DESC')
    //     ->get();

    //     if(!$q){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
    //     }
    // }

    // public function getAllProduct()
    // {
    //     $q = DB::table('00_product AS a')
    //     ->leftJoin('00_supplier AS b', 'b.supl_id', '=', 'a.supl_id')
    //     ->leftJoin('00_brand AS c', 'c.brand_id', '=', 'a.brand_id')
    //     ->leftJoin('00_uom AS d', 'd.uom_id', '=', 'a.uom_id')
    //     ->select('a.prod_id', 'a.prod_code', 'a.prod_name', 'b.supl_name', 'c.brand_name', 'a.prod_desc', 'a.prod_weight', 'a.prod_dim_length', 'a.prod_dim_width', 'a.prod_dim_height', 'a.preorder_flag', 'a.price', 'd.uom_name AS satuan')
    //     ->where('a.active_flag', 'Y')
    //     ->orderBy('a.created_date', 'DESC')
    //     ->get();

    //     if(!$q){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
    //     }
    // }
    
    // public function convertToCurrency($number)
    // {
    //     $converted_string = "";
    //     $string = (string)$number;
    //     $total = strlen($string);
    //     for ($b=0; $b < strlen($string); $b++) { 
    //         $total -= 1;
    //         $converted_string .= $string[$b];
    //         if($total > 0 && $total % 3 == 0){
    //             $converted_string .= ".";
    //         }
    //     }
    //     return $converted_string;
    // }

    public function getProduct($id = null, Request $request)
    {
        $flag = "product";
        $product = DB::table('00_product')
        ->select(
			'00_product.prod_id AS product_id',
			'00_product.prod_name AS product_name',
			'00_product.prod_desc AS product_description',
			'00_product.prod_code AS product_code',
			'00_product.preorder_flag',
			'00_product.prod_price AS product_price',
			'00_product.prod_image AS product_image',
			'00_product.brand_id',
			'00_brand.brand_code',
			'00_brand.brand_name',
			'00_brand.brand_desc AS brand_description',
			'00_brand.brand_image',
			'00_brand.active_flag AS brand_active_status',
			'00_product.uom_id',
			'00_product.uom_value',
			'00_uom.uom_name',
			'00_uom.uom_desc AS uom_description',
			'00_uom.is_decimal',
			'00_product.category_id',
			'00_category.category_name',
			'00_category.category_desc AS category_description',
			'00_category.category_image',
			'00_category.active_flag AS category_active_flag',
            '00_product.stock',
            '00_product.is_taxable_product',
            '00_product.tax_value',
			'00_product.active_flag AS product_active_status',
			'00_product.created_by',
            '00_product.created_date AS product_created_date',
            '00_product.position_date',
            '98_user.user_name AS admin_name',
            
			DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
            DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
            // '00_product_sku.product_sku_name',
            // '00_product_sku.product_sku_id',
            // DB::raw("CONCAT(MIN(CASE WHEN 00_product.prod_id  THEN 00_product.prod_price END),
            // '-',
            // MAX(CASE WHEN 00_product.prod_id  THEN 00_product.prod_price END)
            // ) as price_range"),
            '00_vouchers.voucher_id',
            '00_vouchers.amount',
			'00_vouchers.is_fixed',
			'00_vouchers.max_value_price',
            '00_vouchers.min_price_requirement',
            DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
			->where('00_product.active_flag',1)
			->where('00_category.active_flag',1)
            // ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
			->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
			->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
			->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
			->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
			->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
			->leftJoin('98_user','98_user.user_id','=','00_product.created_by');
            // ->groupBy('00_product_sku.product_sku_id');

        if($id != "0"){
            $product->where('00_category.category_id', $id);
        }
        if($request->filterByMin != ''){
            $product->where('00_product.prod_price', '>=', $request->filterByMin);
        }

        if($request->filterByMax != ''){
            $product->where('00_product.prod_price', '<=', $request->filterByMax);
        }
        // if($request->isPromoMenu == 1){
        //     $flag = "promo";
        //     $product->whereNotNull('00_product_voucher.product_voucher_id')->whereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->where('00_vouchers.active_flag',1);
        // }else{
        //     $product->orWhereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->orWhere('00_vouchers.active_flag',1);
        // }
        // $product->orWhereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->orWhere('00_vouchers.active_flag',1);
        if($request->isPromoMenu == 1){
            // $product_array = array();
            $flag = "promo";
            $product->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }else{
            $product->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }

        if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
            $product->orderBy('00_product.position_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $product->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $product->orderBy('00_product.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $product->orderBy('00_product.prod_price', 'DESC');
        }


        $product = $product->get();
        // dd($product);
        $product_array = array();
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                    // if($total_promo_value > $product[$b]->max_value_price){
                    //     $total_promo_value = $product[$b]->max_value_price;
                    // }
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            
            
            // $varian = null;
            // if (isset($product[$b]->product_sku_id)) {
                // $varian = DB::table('00_product')
                //         ->select('00_product.prod_id as id_varian', 
                //             '00_product.prod_name as varian_name', 
                //             '00_product.prod_price as varian_price')
                //         ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                //         ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                //         ->whereNotNull('00_product.product_sku_id')
                //         ->get();
                // $varian = array([
                //     'title' => $product[$b]->product_sku_name,
                //     'price' => $product[$b]->product_price
                // ]);
            // }
            $obj = array(
                'product_id' => $product[$b]->product_id,
                'product_code' => $product[$b]->product_code,
                'product_name' => $product[$b]->product_name,
                'product_image' => asset($product[$b]->product_image),
                'category_id' => $product[$b]->category_id,
                'category_name' => $product[$b]->category_name,
                'category_image' => asset($product[$b]->category_image),
                'category_description' => $product[$b]->category_description,
                'brand_id' => $product[$b]->brand_id,
                'brand_name' => $product[$b]->brand_name,
                'admin_id' => $product[$b]->created_by,
                'admin_name' => 'Indocyber', //$product[$b]->admin_name
                'product_stock' => $product[$b]->stock,
                'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                'tax_value' => number_format($product[$b]->tax_value,2),
                // 'product_description' => $product[$b]->product_description,
                'product_price_before_discount' => $this->currency->convertToCurrency($product[$b]->product_price),
                'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                'product_weight_uom' => $product[$b]->uom_name,
                'product_discount' => $product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount),
                'product_price_after_discount' => $this->currency->convertToCurrency($product_price_after_discount),
                'product_created_date' => $product[$b]->product_created_date,
                'position_date' => $product[$b]->position_date,
                'sold_qty' => $product[$b]->sold_qty,
				'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                // 'varian' =>  $varian
            );
            array_push($product_array,$obj);
        }
        // dd($product);
        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        }
        
        // if($request->isPromoMenu == 1){
        //     $q = DB::table('00_promo AS z')
        //     ->leftJoin('00_product_supplier AS a', 'a.prod_supl_id', '=', 'z.prod_supl_id')
        //     ->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
        //     ->leftJoin('00_brand AS c', 'c.brand_id', '=', 'b.brand_id')
        //     ->leftJoin('98_user AS d', 'd.user_id', '=', 'a.supplier_user_id')
        //     ->leftJoin('00_uom AS e', 'e.uom_id', '=', 'a.uom_id')
        //     // ->leftJoin('00_product_category AS f', 'f.prod_id', '=', 'a.prod_id')
        //     ->select('a.prod_supl_id AS product_id', 'a.prod_supl_code AS product_code', 'a.prod_supl_name AS product_name', DB::raw('CONCAT("http://34.87.29.119:8080/", b.prod_image) AS product_image'), 'c.brand_name', 'd.user_name AS supplier_name', 'a.description AS product_description', 'a.price AS product_price', DB::raw('ROUND(a.uom_value, 0) AS price_per_weight'), 'e.uom_name AS product_weight',
        //         DB::raw('(CASE 
        //         WHEN z.promo_value IS NOT NULL THEN z.promo_value*100
        //         ELSE "0" 
        //         END) AS product_discount'),
        //         DB::raw('(CASE 
        //         WHEN z.promo_value IS NOT NULL THEN FLOOR(a.price - (a.price*z.promo_value))
        //         ELSE "0" 
        //         END) AS product_price_after_discount'));
        // }else{
        //     $q = DB::table('00_product_supplier AS a')
        //     ->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
        //     ->leftJoin('00_brand AS c', 'c.brand_id', '=', 'b.brand_id')
        //     ->leftJoin('98_user AS d', 'd.user_id', '=', 'a.supplier_user_id')
        //     ->leftJoin('00_uom AS e', 'e.uom_id', '=', 'a.uom_id')
        //     // ->leftJoin('00_product_category AS f', 'f.prod_id', '=', 'a.prod_id')
        //     ->leftJoin('00_promo AS g', 'g.prod_id', '=', 'b.prod_id')
        //     ->select('a.prod_supl_id AS product_id', 'a.prod_supl_code AS product_code', 'a.prod_supl_name AS product_name', DB::raw('CONCAT("http://34.87.29.119:8080/", b.prod_image) AS product_image'), 'c.brand_name', 'd.user_name AS supplier_name', 'a.description AS product_description', 'a.price AS product_price', DB::raw('ROUND(a.uom_value, 0) AS price_per_weight'), 'e.uom_name AS product_weight',
        //         DB::raw('( CASE 
        //         WHEN g.promo_value IS NOT NULL AND (SELECT substr(ROUND(g.promo_value*100, 2), locate(".", ROUND(g.promo_value*100, 2))+1)) != 00 THEN TRUNCATE((g.promo_value * 100), 1)
        //         WHEN g.promo_value IS NOT NULL AND (SELECT substr(ROUND(g.promo_value*100, 2), locate(".", ROUND(g.promo_value*100, 2))+1)) = 00 THEN ROUND((g.promo_value * 100), 0)
        //         ELSE "0" 
        //         END ) AS product_discount'),
        //         DB::raw('(CASE 
        //         WHEN g.promo_value IS NOT NULL THEN FLOOR(a.price - (a.price*g.promo_value))
        //         ELSE "0" 
        //         END) AS product_price_after_discount'));
        // }

        // if($request->route('id') != '0'){
        //     $q->where('b.category_id', $request->route('id'));
        // }

        // if($request->filterByMin != ''){
        //     $q->where('a.price', '>=', $request->filterByMin);
        // }

        // if($request->filterByMax != ''){
        //     $q->where('a.price', '<=', $request->filterByMax);
        // }

        // if($request->filterByLocation != ''){

        // }

        // if($request->isProductMenu == 1){
        //     $q->orderBy('a.prod_supl_name', 'ASC');
        // }

        // if($request->sortBy == 1){ //terbaru
        //     $q->orderBy('a.created_date', 'DESC');
        // }else if($request->sortBy == 2){ //terlaris
            
        // }else if($request->sortBy == 3){//termurah
        //     $q->orderBy('a.price', 'ASC');
        // }else if($request->sortBy == 4){//termahal
        //     $q->orderBy('a.price', 'DESC');
        // }

        // if(!$q->get()){
        //     return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
        // }else{
        //     return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q->get()], 200);
        // }
    }

    public function searchProduct(Request $request){
        $user_id = $request->user_id;
        $inserted_obj =  array(
            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
            'keyword' => $request->input('keyword'),
            'active_flag' => 1
        );
        $products = DB::table('00_product AS pr')
        ->select('pr.prod_id',
            'pr.prod_code',
            'pr.prod_name',
            'pr.prod_image',
            'pr.prod_desc',
            'pr.stock',
            'pr.created_date AS product_created_date',
            'pr.prod_price AS product_price',
            'pr.uom_id',
            'pr.uom_value',
            'uom_table.is_decimal',
            'uom_table.uom_name',
            'ct.category_id',
            'ct.category_name',
            'ct.category_image',
            'ct.category_desc',
            'br.brand_id',
            'br.brand_name',
            'adm.user_id',
            'adm.user_name',
            DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = pr.prod_id) AS sold_qty'),
            ($user_id != "" || $user_id != null ? 
            DB::raw('(select quantity from 10_cart where prod_id = pr.prod_id and 10_cart.buyer_user_id = '.$user_id.') AS is_cart') : 
            DB::raw('0 AS is_cart')),'pr.is_taxable_product','pr.tax_value' )
        ->distinct()
        ->leftJoin('00_category AS ct','ct.category_id','=','pr.category_id')
        ->leftJoin('00_brand as br','br.brand_id','=','pr.brand_id')
        ->join('98_user as adm','adm.user_id','=','pr.created_by')
        ->leftJoin('00_uom as uom_table','uom_table.uom_id','=','pr.uom_id')
        ->leftJoin('00_product_tag AS pta','pta.prod_id','=','pr.prod_id')
        ->leftJoin('00_tag as tg','tg.tag_id','=','pta.tag_id')
        ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','pr.prod_id')
        ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
        ->where('pr.active_flag',1);
        if($request->input('keyword') != null && $request->input('keyword') != ""){
            $products->where(function($query) use($request){
                $query->where('pr.prod_name', 'like', '%'.$request->input('keyword').'%')->orWhere('pr.prod_desc','like', '%'.$request->input('keyword').'%')->orWhere('ct.category_name','like', '%'.$request->input('keyword').'%')->orWhere('br.brand_name','like', '%'.$request->input('keyword').'%')->orWhere('tg.tag_name','like', '%'.$request->input('keyword').'%');
            });
        }

        if($request->input('category_id') != "" && $request->input('category_id') != null && $request->input('category_id') != "0"){
            $products->where('ct.category_id', $request->input('category_id'));
        }

        // if($request->filterByMin != '' && $request->filterByMin != null){
        //     $products->where('pr.prod_price', '>=', $request->filterByMin);
        // }

        // if($request->filterByMax != '' && $request->filterByMax != null){
        //     $products->where('pr.prod_price', '<=', $request->filterByMax);
        // }

        // if(($request->filterByMin != '' && $request->filterByMin != null) && ($request->filterByMax != '' && $request->filterByMax != null)){
        //     $products->whereBetween('pr.prod_price', [$request->filterByMin, $request->filterByMax]);
        // }else{
        //     if($request->filterByMin != '' && $request->filterByMin != null){
        //         $products->where('pr.prod_price', '>=', $request->filterByMin);
        //     }else{
        //         $products->where('pr.prod_price', '<=', $request->filterByMax);
        //     }
        // }
        if($request->filterByMin != '' && $request->filterByMin != null){
            $products->where('pr.prod_price', '>=', $request->filterByMin);
        }
        
        if($request->filterByMax != '' && $request->filterByMax != null){
            $products->where('pr.prod_price', '<=', $request->filterByMax);
        }

        if($request->isPromoMenu == 1){
            // $product_array = array();
            $products->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }else{
            $products->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }


        // if($request->isPromoMenu == 1){
        //     $flag = "promo";
        //     $product->whereNotNull('00_product_voucher.product_voucher_id')->whereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->where('00_vouchers.active_flag',1);
        // }else{
        //     $product->orWhereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->orWhere('00_vouchers.active_flag',1);
        // }
        // $product->orWhereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->orWhere('00_vouchers.active_flag',1);

        if($request->sortBy == 1 || $request->sortBy == "" || $request->sortBy == -1){ //terbaru
            $products->orderBy('pr.created_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $products->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $products->orderBy('pr.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $products->orderBy('pr.prod_price', 'DESC');
        }
        $products = $products->get();
        $product_array = array();
        
        $insert = 0;
        if($request->input('keyword') != "" && $request->input('keyword') != null){
            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
        }
        for ($b=0; $b < count($products); $b++) { 
            $total_promo_value = 0;

            // if($products[$b]->is_fixed == 'Y'){
            //     $product_price_after_discount =  $products[$b]->product_price - $total_promo_value;
            // }else{
            //     $product_price_after_discount =  $products[$b]->product_price - ($products[$b]->product_price * $total_promo_value/100);
            // }

            // if($product_price_after_discount < 0){
            //     $product_price_after_discount = 0;
            // }
            $product_price_after_discount =  $products[$b]->product_price - $total_promo_value;

            $obj = array(
                'product_id' => $products[$b]->prod_id,
                'product_code' => $products[$b]->prod_code,
                'product_name' => $products[$b]->prod_name,
                'product_image' => asset($products[$b]->prod_image),
                'product_description' => $products[$b]->prod_desc,
                'category_id' => $products[$b]->category_id,
                'category_name' => $products[$b]->category_name,
                'category_image' => asset($products[$b]->category_image),
                'category_description' => $products[$b]->category_desc,
                'brand_id' => $products[$b]->brand_id,
                'brand_name' => $products[$b]->brand_name,
                'admin_id' => $products[$b]->user_id,
                'admin_name' => 'Indocyber', //$product[$b]->admin_name
                'product_price_before_discount' => 0,
                'product_weight_value' => 0,
                'product_price_before_discount' => $this->currency->convertToCurrency($products[$b]->product_price),
                'product_weight_value' => $products[$b]->is_decimal == 0 ? ROUND($products[$b]->uom_value,0) : $products[$b]->uom_value,
                'product_weight_uom' => $products[$b]->uom_name,
                'product_discount' => $total_promo_value,
                'product_price_after_discount' => $this->currency->convertToCurrency($product_price_after_discount),
                'product_created_date' => $products[$b]->product_created_date,
                'sold_qty' => $products[$b]->sold_qty,
                'is_cart' => $products[$b]->is_cart == null ? 0 : $products[$b]->is_cart,
                'product_stock' => $products[$b]->stock,
                'is_taxable_product' => $products[$b]->is_taxable_product == 'N' ? false: true,
                'tax_value' => number_format($products[$b]->tax_value,2)
            );
            array_push($product_array,$obj);
        }
        // dd($products);
        if(!$products){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $product_array], 200);
        }
    }

    public function getProductKeyword(Request $request){
        $product_keyword = DB::table('00_search_history AS sh')->select('sh.customer_id','sh.keyword')->where('sh.customer_id',$request->input('user_id'))->distinct()->where('sh.active_flag',1)->get();
        $product_keyword_array = array();
        
        for ($b=0; $b < count($product_keyword); $b++) { 
            $obj = array(
                'customer_id' => $product_keyword[$b]->customer_id,
                'keyword' => $product_keyword[$b]->keyword
            );
            array_push($product_keyword_array,$obj);
        }

        if(!$product_keyword){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $product_keyword_array], 200);
        }
    }

    public function deleteProductKeyword(Request $request){
        $delete = DB::table('00_search_history AS sh')->where('sh.keyword', $request->input('keyword'))->where('sh.customer_id', $request->input('user_id'))->update(['active_flag' => 0]);

        if(!$delete){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'Keyword successfully deleted'], 200);
        }
    }

    public function getVarianProduct(Request $request)
    {
        $q = DB::table('00_product_sku as sku')
        ->selectRaw("sku.product_sku_name as sku_product, 
            CONCAT(MIN(CASE WHEN p.prod_id  THEN p.prod_price END),
            '-',
            MAX(CASE WHEN p.prod_id  THEN p.prod_price END)
            ) as price_range")
        ->join('00_product as p', 'p.product_sku_id', '=', 'sku.product_sku_id')
        ->where('sku.product_sku_id', $request->sku_id)
        ->groupBy('sku.product_sku_id')
        ->get();
        // dd($q);

        // $varian = array(
        //     'sku_product' => $q->sku_product,
        //     'prod_name' => $q->prod_name,
        //     'prod_price' => $q->prod_price
        // );

        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Fetch data failed'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $q], 200);
        }
    }

    public function getProduct2($id = null, Request $request)
    {
        $flag = "product";
        $product_non_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
                ->where('00_product.active_flag',1)
                ->where('00_category.active_flag',1)
                ->whereNull('00_product.product_sku_id')
                ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                ->leftJoin('00_inventory', '00_product.prod_id', '=', '00_inventory.prod_id')
                ->leftJoin('98_user','98_user.user_id','=','00_product.created_by');

            if($id != "0"){
                $product_non_sku->where('00_category.category_id', $id);
            }
            if($request->filterByMin != ''){
                $product_non_sku->where('00_product.prod_price', '>=', $request->filterByMin);
            }

            if($request->filterByMax != ''){
                $product_non_sku->where('00_product.prod_price', '<=', $request->filterByMax);
            }
           
            if($request->isPromoMenu == 1){
                $flag = "promo";
                $product_non_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
                ->where('00_vouchers.active_flag',1);
            }else{
                $product_non_sku->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
                ->where('00_vouchers.active_flag',1);
            }

            if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
                $product_non_sku->orderBy('00_product.position_date', 'DESC');
            }else if($request->sortBy == 2){ //terlaris
                $product_non_sku->orderBy('sold_qty','DESC');
            }else if($request->sortBy == 3){//termurah
                $product_non_sku->orderBy('00_product.prod_price', 'ASC');
            }else if($request->sortBy == 4){//termahal
                $product_non_sku->orderBy('00_product.prod_price', 'DESC');
            }

        $product_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
            )
                ->where('00_product.active_flag',1)
                ->where('00_category.active_flag',1)
                ->whereNotNull('00_product.product_sku_id')
                ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                ->leftJoin('98_user','98_user.user_id','=','00_product.created_by')
                ->groupBy('00_product_sku.product_sku_id');

            if($id != "0"){
                $product_sku->where('00_category.category_id', $id);
            }
            if($request->filterByMin != ''){
                $product_sku->where('00_product.prod_price', '>=', $request->filterByMin);
            }

            if($request->filterByMax != ''){
                $product_sku->where('00_product.prod_price', '<=', $request->filterByMax);
            }
            
            if($request->isPromoMenu == 1){
                $flag = "promo";
                $product_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
                ->where('00_vouchers.active_flag',1);
            }else{
                $product_sku->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
                ->where('00_vouchers.active_flag',1);
            }

            if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
                $product_sku->orderBy('00_product.position_date', 'DESC');
            }else if($request->sortBy == 2){ //terlaris
                $product_sku->orderBy('sold_qty','DESC');
            }else if($request->sortBy == 3){//termurah
                $product_sku->orderBy('00_product.prod_price', 'ASC');
            }else if($request->sortBy == 4){//termahal
                $product_sku->orderBy('00_product.prod_price', 'DESC');
            }


        $product = $product_non_sku->union($product_sku)->orderBy('position_date', 'DESC')->get();

        $product_array = array();
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            $product_header_id = $product[$b]->product_id;
            $location_id = $request->filterByLocation;
            $warehouse = DB::table('00_product_warehouse')
                        ->select('00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_address','00_warehouse.address_id','=','00_address.address_id')
                        ->when($location_id, function ($query, $location_id) {
                            return $query->where('00_address.kabupaten_kota_id',$location_id);
                        })
                        ->where('00_product_warehouse.prod_id',$product_header_id)
                        ->get();
            if (count($warehouse) > 0) :
                // GET VARIAN 
                    $varian_array=array();
                    $arr_warehouse=array();
                    foreach ($warehouse as $wh) {
                        $varian = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.prod_price as varian_price',
                                '00_inventory.stock as varian_stock',
                                '00_product.uom_value as variant_weight_value',
                                '00_uom.uom_name as varian_weight_uom',
                                '00_vouchers.voucher_id as voucher_id',
                                '00_vouchers.amount as varian_voucher_amount',
                                '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            ->whereNotNull('00_product.product_sku_id')
                            ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_voucher.voucher_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            })
                            ->get();
                        $wh_varian = array();
                        for($i=0; $i < count($varian); $i++){
                            $total_promo_varian = 0;
                            if($varian[$i]->varian_voucher_expired == 0 && $varian[$i]->voucher_id != null){
                                if($varian[$i]->varian_voucher_is_fixed == 'Y'){
                                    $total_promo_varian = $varian[$i]->varian_voucher_amount;
                                }else{
                                    $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->varian_voucher_amount/100);
                                }
                            }
                            
                            $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                            $varian_discount = $varian[$i]->voucher_id == null ? "0" : ($varian[$i]->varian_voucher_is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_varian) : $varian[$i]->varian_voucher_amount);
                            $obj_varian = array(
                                'product_id' => $varian[$i]->product_id,
                                'varian_name' => $varian[$i]->varian_name,
                                'varian_price_before_discount' => $varian[$i]->varian_price,
                                'varian_price_after_discount' => $varian_price_after_discount,
                                'varian_stock' => !isset($varian[$i]->varian_stock) ? 0 : $varian[$i]->varian_stock,
                                'varian_weight' => $varian[$i]->variant_weight_value,
                                'varian_uom' => $varian[$i]->varian_weight_uom,
                                'varian_discount' => $varian_discount,
                                'expired' => $varian[$i]->varian_voucher_expired
                            );
                            array_push($varian_array, $obj_varian);
                            array_push($wh_varian, $obj_varian);
                        }
                        $data_warehouse = array(
                            'warehouse_id' => $wh->warehouse_id,
                            'warehouse_name' => $wh->warehouse_name,
                            'varian' => $wh_varian
                        );
                        array_push($arr_warehouse, $data_warehouse);
                    }
                    
                    //mencari harga terendah yang sebelum didiskon di varian
                    $lowestKeyBeforeDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value < $lowestKeyBeforeDiscount )||( $lowestKeyBeforeDiscount== '')){
                                $lowestKeyBeforeDiscount = $value;
                                }
                            }
                        }
                    }
                    $min_value_before_discount = $lowestKeyBeforeDiscount;
                   
                    //mencari harga tertinggi yang sebelum didiskon di varian
                    $highestKeyBeforeDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value > $highestKeyBeforeDiscount )||( $highestKeyBeforeDiscount== '')){
                                $highestKeyBeforeDiscount = $value;
                                }
                            }
                        }
                    }
                    $max_value_before_discount = $highestKeyBeforeDiscount;
                                
                    
                    //mencari harga terendah yang sudah didiskon di varian
                    $lowestKeyAfterDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value < $lowestKeyAfterDiscount )||( $lowestKeyAfterDiscount== '')){
                                $lowestKeyAfterDiscount = $value;
                                }
                            }
                        }
                    }
                    $min_value_after_discount = $lowestKeyAfterDiscount;
                   
                    //mencari harga tertinggi yang sudah didiskon di varian
                    $highestKeyAfterDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value > $highestKeyAfterDiscount )||( $highestKeyAfterDiscount== '')){
                                $highestKeyAfterDiscount = $value;
                                }
                            }
                        }
                    }
                    $max_value_after_discount = $highestKeyAfterDiscount;
                   
                   
                    //mencari diskon terendah
                    $lowestDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_discount'){
                                if(($value < $lowestDiscount )||( $lowestDiscount== '')){
                                $lowestDiscount = $value;
                                }
                            }
                        }
                    }
                    $min_discount = $lowestDiscount;
                   
                    //mencari diskon tertinggi
                    $highestDiscount = '';
                    foreach($varian_array as $arra){
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_discount'){
                                if(($value > $highestDiscount )||( $highestDiscount== '')){
                                $highestDiscount = $value;
                                }
                            }
                        }
                    }
                    $max_discount = $highestDiscount;
                   
                    //menjumlahkan stok untuk header
                    $stockInVariant = 0;
                    foreach($varian_array as $array){
                        foreach ($array as $key=>$value){
                            if ($key == 'varian_stock'){
                                $stockInVariant =+ $stockInVariant + $value;
                            }
                        }
                    }
                    $varianStock = $stockInVariant;
                    
                    //set header is_varian, price, stok, dan diskon         
                    if(isset($varian) && count($varian) > 0) { 
                        $is_varian = true;
                        $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                        $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                        $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                        $stock = $varianStock;
                    } else { 
                        $is_varian = false;
                        $price_before_discount = $this->currency->convertToCurrency($product[$b]->product_price);
                        $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                        $discount = strval($product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount));
                        $stock = $product[$b]->stock;
                    }
                // GET VARIAN 

                // RESUTL GET PRODUCT
                    $product_name = isset($product[$b]->product_sku_id) ? $product[$b]->product_sku_name : $product[$b]->product_name;
                    $obj = array(
                        'product_id' => $product[$b]->product_id,
                        'product_name' => $product_name,
                        'product_image' => asset($product[$b]->product_image),
                        'admin_id' => $product[$b]->created_by,
                        'admin_name' => "Indocyber", //$product[$b]->admin_name
                        // 'warehouse_id' => $product[$b]->warehouse_id,
                        // 'warehouse_name' => $product[$b]->warehouse_name, //$product[$b]->admin_name
                        'product_stock' => $stock,
                        // 'inventory_stock' => $product[$b]->inventory_stock,
                        'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                        'tax_value' => number_format($product[$b]->tax_value,2),
                        'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                        'product_weight_uom' => $product[$b]->uom_name,
                        'product_discount' => $discount,
                        'product_price_before_discount' => $price_before_discount,
                        'product_price_after_discount' => $price_after_discount,
                        'product_created_date' => $product[$b]->product_created_date,
                        'position_date' => $product[$b]->position_date,
                        'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                        'is_varian' => $is_varian,
                        'warehouse' => $arr_warehouse
                        // 'varian' =>  $varian_array
                    );
                    array_push($product_array,$obj);
                // RESUTL GET PRODUCT
            endif;
        }
        // dd($product);
        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        } 
    }

    public function searchProduct2(Request $request){
        $user_id = $request->user_id;
        $inserted_obj =  array(
            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
            'keyword' => $request->input('keyword'),
            'active_flag' => 1
        );

        $products_non_sku = DB::table('00_product AS pr')
        ->select('pr.prod_id',
            'pr.prod_code',
            'pr.prod_name AS product_name',
            'pr.prod_image',
            'pr.prod_desc',
            'pr.stock',
            'pr.created_date AS product_created_date',
            'pr.prod_price AS product_price',
            'pr.uom_id',
            'pr.uom_value',
            'uom_table.is_decimal',
            'uom_table.uom_name',
            'ct.category_id',
            'ct.category_name',
            'ct.category_image',
            'ct.category_desc',
            'br.brand_id',
            'br.brand_name',
            'adm.user_id',
            'adm.user_name',
            'pr.position_date',
            '00_product_sku.product_sku_name',
            '00_product_sku.product_sku_id',
            '00_vouchers.voucher_id',
            '00_vouchers.amount',
            '00_vouchers.is_fixed',
            '00_vouchers.max_value_price',
            '00_vouchers.min_price_requirement',
            DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = pr.prod_id) AS sold_qty'),
            ($user_id != "" || $user_id != null ? 
            DB::raw('(select quantity from 10_cart where prod_id = pr.prod_id and 10_cart.buyer_user_id = '.$user_id.') AS is_cart') : DB::raw('0 AS is_cart')),
            'pr.is_taxable_product','pr.tax_value',
            DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
        )
        ->distinct()
        ->leftJoin('00_product_sku', 'pr.product_sku_id', '=', '00_product_sku.product_sku_id')
        ->leftJoin('00_category AS ct','ct.category_id','=','pr.category_id')
        ->leftJoin('00_brand as br','br.brand_id','=','pr.brand_id')
        ->join('98_user as adm','adm.user_id','=','pr.created_by')
        ->leftJoin('00_uom as uom_table','uom_table.uom_id','=','pr.uom_id')
        ->leftJoin('00_product_tag AS pta','pta.prod_id','=','pr.prod_id')
        ->leftJoin('00_tag as tg','tg.tag_id','=','pta.tag_id')
        ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','pr.prod_id')
        ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
        ->whereNull('pr.product_sku_id')
        ->where('pr.active_flag',1);
        if($request->input('keyword') != null && $request->input('keyword') != ""){
            $products_non_sku->where(function($query) use($request){
                $query->where('pr.prod_name', 'like', '%'.$request->input('keyword').'%');
            });
        }

        if($request->input('category_id') != "" && $request->input('category_id') != null && $request->input('category_id') != "0"){
            $products_non_sku->where('ct.category_id', $request->input('category_id'));
        }

        if($request->filterByMin != '' && $request->filterByMin != null){
            $products_non_sku->where('pr.prod_price', '>=', $request->filterByMin);
        }
        
        if($request->filterByMax != '' && $request->filterByMax != null){
            $products_non_sku->where('pr.prod_price', '<=', $request->filterByMax);
        }

        if($request->isPromoMenu == 1){
            // $product_array = array();
            $products_non_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }else{
            $products_non_sku->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }

        if($request->sortBy == 1 || $request->sortBy == "" || $request->sortBy == -1){ //terbaru
            $products_non_sku->orderBy('pr.created_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $products_non_sku->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $products_non_sku->orderBy('pr.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $products_non_sku->orderBy('pr.prod_price', 'DESC');
        }

        $products_sku = DB::table('00_product AS pr')
        ->select('pr.prod_id',
            'pr.prod_code',
            'pr.prod_name AS product_name',
            'pr.prod_image',
            'pr.prod_desc',
            'pr.stock',
            'pr.created_date AS product_created_date',
            'pr.prod_price AS product_price',
            'pr.uom_id',
            'pr.uom_value',
            'uom_table.is_decimal',
            'uom_table.uom_name',
            'ct.category_id',
            'ct.category_name',
            'ct.category_image',
            'ct.category_desc',
            'br.brand_id',
            'br.brand_name',
            'adm.user_id',
            'adm.user_name',
            'pr.position_date',
            '00_product_sku.product_sku_name',
            '00_product_sku.product_sku_id',
            '00_vouchers.voucher_id',
            '00_vouchers.amount',
            '00_vouchers.is_fixed',
            '00_vouchers.max_value_price',
            '00_vouchers.min_price_requirement',
            DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = pr.prod_id) AS sold_qty'),
            ($user_id != "" || $user_id != null ? 
            DB::raw('(select quantity from 10_cart where prod_id = pr.prod_id and 10_cart.buyer_user_id = '.$user_id.') AS is_cart') : DB::raw('0 AS is_cart')),
            'pr.is_taxable_product','pr.tax_value',
            DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
        )
        ->distinct()
        ->leftJoin('00_product_sku', 'pr.product_sku_id', '=', '00_product_sku.product_sku_id')
        ->leftJoin('00_category AS ct','ct.category_id','=','pr.category_id')
        ->leftJoin('00_brand as br','br.brand_id','=','pr.brand_id')
        ->join('98_user as adm','adm.user_id','=','pr.created_by')
        ->leftJoin('00_uom as uom_table','uom_table.uom_id','=','pr.uom_id')
        ->leftJoin('00_product_tag AS pta','pta.prod_id','=','pr.prod_id')
        ->leftJoin('00_tag as tg','tg.tag_id','=','pta.tag_id')
        ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','pr.prod_id')
        ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
        ->where('pr.active_flag',1)
        ->groupBy('00_product_sku.product_sku_id');

        if($request->input('keyword') != null && $request->input('keyword') != ""){
            $products_sku->where(function($query) use($request){
                $query->where('pr.prod_name', 'like', '%'.$request->input('keyword').'%');
            });
        }

        if($request->input('category_id') != "" && $request->input('category_id') != null && $request->input('category_id') != "0"){
            $products_sku->where('ct.category_id', $request->input('category_id'));
        }

        if($request->filterByMin != '' && $request->filterByMin != null){
            $products_sku->where('pr.prod_price', '>=', $request->filterByMin);
        }
        
        if($request->filterByMax != '' && $request->filterByMax != null){
            $products_sku->where('pr.prod_price', '<=', $request->filterByMax);
        }

        if($request->isPromoMenu == 1){
            // $product_array = array();
            $products_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }else{
            $products_sku->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }

        if($request->sortBy == 1 || $request->sortBy == "" || $request->sortBy == -1){ //terbaru
            $products_sku->orderBy('pr.created_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $products_sku->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $products_sku->orderBy('pr.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $products_sku->orderBy('pr.prod_price', 'DESC');
        }


        // $products = $products->get();
        $products = $products_non_sku->union($products_sku)->orderBy('position_date', 'DESC')->get();
        // dd($products);
        $product_array = array();
        
        $insert = 0;
        if($request->input('keyword') != "" && $request->input('keyword') != null){
            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
        }


        for ($b=0; $b < count($products); $b++) {
            $total_promo_value = 0;
            if($products[$b]->expired == 0 && $products[$b]->voucher_id != null){
                if($products[$b]->is_fixed == 'Y'){
                    $total_promo_value = $products[$b]->amount;
                }else{
                    $total_promo_value = ($products[$b]->product_price * $products[$b]->amount/100);
                    // if($total_promo_value > $product[$b]->max_value_price){
                    //     $total_promo_value = $product[$b]->max_value_price;
                    // }
                }

            }
            $product_price_after_discount = $products[$b]->product_price - $total_promo_value;
            

             $varian = DB::table('00_product')
                ->select('00_product.prod_id as product_id', 
                    '00_product.prod_name as varian_name', 
                    '00_product.prod_price as varian_price',
                    '00_product.stock as varian_stock',
                    '00_product.uom_value as variant_weight_value',
                    '00_uom.uom_name as varian_weight_uom',
                    '00_vouchers.voucher_id as voucher_id',
                    '00_vouchers.amount as varian_voucher_amount',
                    '00_vouchers.is_fixed as varian_voucher_is_fixed',
                    DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                )
                ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                ->where('00_product.active_flag',1)
                    ->whereNotNull('00_product.product_sku_id')
                    ->where('00_product.product_sku_id', $products[$b]->product_sku_id)
                    ->where(function($query) use($request){
                        $query->whereNull('00_product_voucher.voucher_id')
                        ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                    })
                ->get();
                
            $varian_array=array();
            for($i=0; $i < count($varian); $i++){
                $total_promo_varian = 0;
                if($varian[$i]->varian_voucher_expired == 0 && $varian[$i]->voucher_id != null){
                    if($varian[$i]->varian_voucher_is_fixed == 'Y'){
                        $total_promo_varian = $varian[$i]->varian_voucher_amount;
                    }else{
                        $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->varian_voucher_amount/100);
                    }
                }
                
                $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                $varian_discount = $varian[$i]->voucher_id == null ? "0" : ($varian[$i]->varian_voucher_is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_varian) : $varian[$i]->varian_voucher_amount);
                $obj_varian = array(
                    'product_id' => $varian[$i]->product_id,
                    'varian_name' => $varian[$i]->varian_name,
                    'varian_price_before_discount' => $varian[$i]->varian_price,
                    'varian_price_after_discount' => $varian_price_after_discount,
                    'varian_stock' => $varian[$i]->varian_stock,
                    'varian_weight' => $varian[$i]->variant_weight_value,
                    'varian_uom' => $varian[$i]->varian_weight_uom,
                    'varian_discount' => $varian_discount,
                    'expired' => $varian[$i]->varian_voucher_expired
                );
                array_push($varian_array, $obj_varian);
            }
            
            
            //mencari harga terendah yang sebelum didiskon di varian
            $lowestKeyBeforeDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_price_before_discount'){
                        if(($value < $lowestKeyBeforeDiscount )||( $lowestKeyBeforeDiscount== '')){
                        $lowestKeyBeforeDiscount = $value;
                        }
                    }
                }
            }
           $min_value_before_discount = $lowestKeyBeforeDiscount;
           
           //mencari harga tertinggi yang sebelum didiskon di varian
            $highestKeyBeforeDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_price_before_discount'){
                        if(($value > $highestKeyBeforeDiscount )||( $highestKeyBeforeDiscount== '')){
                        $highestKeyBeforeDiscount = $value;
                        }
                    }
                }
            }
           $max_value_before_discount = $highestKeyBeforeDiscount;
                        
            
            //mencari harga terendah yang sudah didiskon di varian
            $lowestKeyAfterDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_price_after_discount'){
                        if(($value < $lowestKeyAfterDiscount )||( $lowestKeyAfterDiscount== '')){
                        $lowestKeyAfterDiscount = $value;
                        }
                    }
                }
            }
           $min_value_after_discount = $lowestKeyAfterDiscount;
           
           //mencari harga tertinggi yang sudah didiskon di varian
            $highestKeyAfterDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_price_after_discount'){
                        if(($value > $highestKeyAfterDiscount )||( $highestKeyAfterDiscount== '')){
                        $highestKeyAfterDiscount = $value;
                        }
                    }
                }
            }
           $max_value_after_discount = $highestKeyAfterDiscount;
           
           
           //mencari diskon terendah
            $lowestDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_discount'){
                        if(($value < $lowestDiscount )||( $lowestDiscount== '')){
                        $lowestDiscount = $value;
                        }
                    }
                }
            }
           $min_discount = $lowestDiscount;
           
           //mencari diskon tertinggi
            $highestDiscount = '';
            foreach($varian_array as $arra){
                foreach ($arra as $key=>$value){
                    if ($key == 'varian_discount'){
                        if(($value > $highestDiscount )||( $highestDiscount== '')){
                        $highestDiscount = $value;
                        }
                    }
                }
            }
           $max_discount = $highestDiscount;
           
           //menjumlahkan stok untuk header
           $stockInVariant = 0;
           foreach($varian_array as $array){
                foreach ($array as $key=>$value){
                    if ($key == 'varian_stock'){
                        $stockInVariant =+ $stockInVariant + $value;
                    }
                }
            }
            $varianStock = $stockInVariant;
           
            
            //set header is_varian, price, stok, dan diskon         
           if(isset($varian) && count($varian) > 0) { 
                $is_varian = true;
                $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                $stock = $varianStock;
           } else { 
                $is_varian = false;
                $price_before_discount = $this->currency->convertToCurrency($products[$b]->product_price);
                $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                $discount = strval($products[$b]->voucher_id == null ? 0 : ($products[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $products[$b]->amount));
                $stock = $products[$b]->stock;
           }

            $product_name = isset($products[$b]->product_sku_id) ? $products[$b]->product_sku_name : $products[$b]->product_name;

            $obj = array(
                'product_id' => $products[$b]->prod_id,
                'product_code' => $products[$b]->prod_code,
                'product_name' => $product_name,
                'product_image' => asset($products[$b]->prod_image),
                // 'product_description' => $products[$b]->prod_desc,
                'category_id' => $products[$b]->category_id,
                'category_name' => $products[$b]->category_name,
                'category_image' => asset($products[$b]->category_image),
                'category_description' => $products[$b]->category_desc,
                // 'brand_id' => $products[$b]->brand_id,
                // 'brand_name' => $products[$b]->brand_name,
                'admin_id' => $products[$b]->user_id,
                'admin_name' => 'Indocyber', //$product[$b]->admin_name
                'product_price_before_discount' => 0,
                'product_price_before_discount' => $price_before_discount,
                'product_price_after_discount' => $price_after_discount,
                'product_stock' => $stock,
                'product_weight_value' => 0,
                'product_weight_value' => $products[$b]->is_decimal == 0 ? ROUND($products[$b]->uom_value,0) : $products[$b]->uom_value,
                'product_weight_uom' => $products[$b]->uom_name,
                'product_discount' => $total_promo_value,
                // 'product_price_after_discount' => $this->currency->convertToCurrency($product_price_after_discount),
                'product_created_date' => $products[$b]->product_created_date,
                'sold_qty' => $products[$b]->sold_qty,
                'is_cart' => $products[$b]->is_cart == null ? 0 : $products[$b]->is_cart,
                // 'is_taxable_product' => $products[$b]->is_taxable_product == 'N' ? false: true,
                // 'tax_value' => number_format($products[$b]->tax_value,2),
                'is_varian' => $is_varian,
                'varian' => $varian_array
            );
            array_push($product_array,$obj);
        }
        // dd($products);
        if(!$products){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $product_array], 200);
        }
    }

    public function getProductWh($id = null, Request $request)
    {
        $flag = "product";
        // $product_non_sku = DB::table('00_product')
        //     ->select(
        //         '00_product.prod_id AS product_id',
        //         '00_product.prod_name AS product_name',
        //         '00_product.prod_desc AS product_description',
        //         '00_product.prod_code AS product_code',
        //         '00_product.preorder_flag',
        //         '00_product.prod_price AS product_price',
        //         '00_product.prod_image AS product_image',
        //         '00_product.brand_id',
        //         '00_brand.brand_code',
        //         '00_brand.brand_name',
        //         '00_brand.brand_desc AS brand_description',
        //         '00_brand.brand_image',
        //         '00_brand.active_flag AS brand_active_status',
        //         '00_product.uom_id',
        //         '00_product.uom_value',
        //         '00_uom.uom_name',
        //         '00_uom.uom_desc AS uom_description',
        //         '00_uom.is_decimal',
        //         '00_product.category_id',
        //         '00_category.category_name',
        //         '00_category.category_desc AS category_description',
        //         '00_category.category_image',
        //         '00_category.active_flag AS category_active_flag',
        //         '00_product.stock',
        //         '00_product.is_taxable_product',
        //         '00_product.tax_value',
        //         '00_product.active_flag AS product_active_status',
        //         '00_product.created_by',
        //         '00_product.created_date AS product_created_date',
        //         '00_product.position_date',
        //         '98_user.user_name AS admin_name',
        //         DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
        //         DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
        //         '00_product_sku.product_sku_name',
        //         '00_product_sku.product_sku_id',
        //         '00_vouchers.voucher_id',
        //         '00_vouchers.amount',
        //         '00_vouchers.is_fixed',
        //         '00_vouchers.max_value_price',
        //         '00_vouchers.min_price_requirement',
        //         DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))
        //         ->where('00_product.active_flag',1)
        //         ->where('00_category.active_flag',1)
        //         ->whereNull('00_product.parent_product_id')
        //         ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
        //         ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
        //         ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
        //         ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
        //         ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
        //         ->leftJoin('00_inventory', '00_product.prod_id', '=', '00_inventory.prod_id')
        //         ->leftJoin('98_user','98_user.user_id','=','00_product.created_by');

        //     if($id != "0"){
        //         $product_non_sku->where('00_category.category_id', $id);
        //     }
        //     if($request->filterByMin != ''){
        //         $product_non_sku->where('00_product.prod_price', '>=', $request->filterByMin);
        //     }

        //     if($request->filterByMax != ''){
        //         $product_non_sku->where('00_product.prod_price', '<=', $request->filterByMax);
        //     }
           
        //     if($request->isPromoMenu == 1){
        //         $flag = "promo";
        //         $product_non_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
        //         ->where('00_vouchers.active_flag',1);
        //     }else{
        //         $product_non_sku->whereNull('00_product_voucher.voucher_id')->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
        //         ->where('00_vouchers.active_flag',1);
        //     }

        //     if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
        //         $product_non_sku->orderBy('00_product.position_date', 'DESC');
        //     }else if($request->sortBy == 2){ //terlaris
        //         $product_non_sku->orderBy('sold_qty','DESC');
        //     }else if($request->sortBy == 3){//termurah
        //         $product_non_sku->orderBy('00_product.prod_price', 'ASC');
        //     }else if($request->sortBy == 4){//termahal
        //         $product_non_sku->orderBy('00_product.prod_price', 'DESC');
        //     }
        $keyword = $request->input('keyword');
        $product_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                // DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                // '00_inventory.warehouse_id',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
            )
            ->when($keyword, function ($query, $keyword) {
                return $query->where('00_product.prod_name','like','%'.$keyword.'%');
            })
            ->where('00_product.active_flag',1)
            ->where('00_category.active_flag',1)
            ->whereNull('00_product.variant_id')
            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
            ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
            ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
            ->leftJoin('98_user','98_user.user_id','=','00_product.created_by')
            ->leftjoin('00_product_warehouse','00_product_warehouse.prod_id','00_product.prod_id')
            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
            ->groupBy('00_product_sku.product_sku_id');

        if($id != "0"){
            $product_sku->where('00_category.category_id', $id);
        }
        if($request->filterByMin != ''){
            $product_sku->where('00_product_warehouse.prod_price', '>=', $request->filterByMin);
        }

        if($request->filterByMax != ''){
            $product_sku->where('00_product_warehouse.prod_price', '<=', $request->filterByMax);
        }
        
        if($request->isPromoMenu == 1){
            $flag = "promo";
            $product_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }

        if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
            $product_sku->orderBy('00_product.position_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $product_sku->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $product_sku->orderBy('00_product.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $product_sku->orderBy('00_product.prod_price', 'DESC');
        }
        $page_size=10;
        $prodTotalPage=$product_sku;
        $totalProduct=count($prodTotalPage->get());
         if($totalProduct==0){
            $totalPageFloor=0;
        }else{
            $totalPageFloor=floor(count($prodTotalPage->get())/$page_size);
            $totalPageMod=fmod($totalProduct,$page_size);
            if($totalPageMod>0){
                $totalPageFloor+=1;
            }
        }       
        $page=1-1;
        // if ($page > -1 and isset($page_size)) {
        //     $product_sku->limit($page_size);
        // }
        // if ($page > -1 and isset($page_size)) {
        //     $product_sku->offset($page*$page_size);
        // }
        // $product = $product_non_sku->union($product_sku)->orderBy('position_date', 'DESC')->get();
        $product = $product_sku->orderBy('position_date', 'DESC')->get();
        // dd($product);
        // dd(count($prod2->get()));

        $insert = 0;
        $inserted_obj =  array(
            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
            'keyword' => $keyword,
            'is_agent' => $request->input('is_agent'),
            'active_flag' => 1
        );
        if($request->input('keyword') != "" && $request->input('keyword') != null){
            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
        }
        
        $check_price_type = null;
        if ($request->input('is_agent') > 0) :
            $check_price_type = DB::table('98_user as user')
                            ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
                            ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
                            ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
                            ->where('user.user_id','=',$request->input('user_id'))
                            ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
                            ->first();
        endif;

        $product_array = array();
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            $product_header_id = $product[$b]->product_id;
            $location_id = $request->filterByLocation;
            $warehouse = DB::table('00_product_warehouse')
                        ->select('00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name','00_provinsi.provinsi_id', '00_provinsi.provinsi_name')
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_address','00_warehouse.address_id','=','00_address.address_id')
                        ->leftJoin('00_kabupaten_kota','00_address.kabupaten_kota_id','=','00_kabupaten_kota.kabupaten_kota_id')
                        ->leftJoin('00_provinsi','00_address.provinsi_id','=','00_provinsi.provinsi_id')
                        ->where('00_warehouse.warehouse_type_id', 2)
                        ->whereNotNull('00_warehouse.warehouse_id')
                        ->whereNotNull('00_product_warehouse.warehouse_id')
                        ->when($location_id, function ($query, $location_id) {
                            return $query->where('00_address.kabupaten_kota_id',$location_id);
                        })
                        ->where('00_product_warehouse.prod_id',$product_header_id)
                        ->get();
            // dd($warehouse);
            if (count($warehouse) > 0) :
                // GET VARIAN 
                    $varian_array=array();
                    $arr_warehouse=array();
                    foreach ($warehouse as $wh) {
                        // $checkVarian=DB::table('00_product')
                        // ->leftJoin('00_product_warehouse','00_product.prod_id','=','00_product_warehouse.prod_id')
                        // ->where('00_product.active_flag',1)
                        // // ->whereNotNull('00_product.product_sku_id')
                        // ->where('00_product.variant_id', $product_header_id)
                        // ->where('00_product_warehouse.warehouse_id', $wh->warehouse_id)
                        // ->select('00_product.prod_id')
                        // ->first();
                        // if($checkVarian){
                        //     $a="00_product.variant_id";    
                        // }else{
                        //     $a="00_product.prod_id";   
                        // }
                        
                        // dd($checkVarian);
                        $prod_parent = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.parent_prod_id as prod_stok_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.stockable_flag', 
                                '00_product.prod_price as varian_price_product',
                                '00_product_warehouse.prod_price as varian_price_warehouse',
                                '00_inventory.stock as varian_stock',
                                '00_inventory.organization_id as org_id',
                                '00_product.uom_value as variant_weight_value',
                                '00_product.uom_id as varian_weight_uom_id', 
                                '00_uom.uom_name as varian_weight_uom',
                                '00_product.bruto as variant_pack_weight',
                                'uom_pack.uom_name as variant_pack_uom',
                                '00_vouchers.voucher_id as voucher_id',
                                '00_vouchers.amount as varian_voucher_amount',
                                '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                '00_product_warehouse.warehouse_id',
                                '00_product_price_type.price_type_id',
                                '00_product_price_type.discount_type',
                                '00_product_price_type.rate',
                                // '00_organization.organization_type_id',
                                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_uom as uom_pack', 'uom_pack.uom_id', '=', '00_product.bruto_uom_id')
                            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            // ->leftJoin('00_organization', '00_inventory.organization_id', '=', '00_organization.organization_id')
                            ->leftJoin('00_product_warehouse','00_product.prod_id','=','00_product_warehouse.prod_id')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            // ->whereNotNull('00_product.product_sku_id')
                            // ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            // ->where($a, $product_header_id)
                            ->where('00_product.prod_id', $product_header_id)

                            ->where('00_product_warehouse.warehouse_id', $wh->warehouse_id)
                            // ->where(function($query) {
                            //     $query->where('00_organization.organization_id', 1)
                            //           ->orWhere('00_organization.organization_id', null);
                            // })
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_voucher.voucher_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            });
                            // ->get();

                        $prod_variant = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.parent_prod_id as prod_stok_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.stockable_flag', 
                                '00_product.prod_price as varian_price_product',
                                '00_product_warehouse.prod_price as varian_price_warehouse',
                                '00_inventory.stock as varian_stock',
                                '00_inventory.organization_id as org_id',
                                '00_product.uom_value as variant_weight_value',
                                '00_product.uom_id as varian_weight_uom_id', 
                                '00_uom.uom_name as varian_weight_uom',
                                '00_product.bruto as variant_pack_weight',
                                'uom_pack.uom_name as variant_pack_uom',
                                '00_vouchers.voucher_id as voucher_id',
                                '00_vouchers.amount as varian_voucher_amount',
                                '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                '00_product_warehouse.warehouse_id',
                                '00_product_price_type.price_type_id',
                                '00_product_price_type.discount_type',
                                '00_product_price_type.rate',
                                // '00_organization.organization_type_id',
                                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_uom as uom_pack', 'uom_pack.uom_id', '=', '00_product.bruto_uom_id')
                            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            // ->leftJoin('00_organization', '00_inventory.organization_id', '=', '00_organization.organization_id')
                            ->leftJoin('00_product_warehouse','00_product.prod_id','=','00_product_warehouse.prod_id')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            // ->whereNotNull('00_product.product_sku_id')
                            // ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            // ->where($a, $product_header_id)
                            ->where('00_product.variant_id', $product_header_id)

                            ->where('00_product_warehouse.warehouse_id', $wh->warehouse_id)
                            // ->where(function($query) {
                            //     $query->where('00_organization.organization_id', 1)
                            //           ->orWhere('00_organization.organization_id', null);
                            // })
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_voucher.voucher_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            });
                            // ->get();

                        $varian = $prod_parent->union($prod_variant)->get();
                        // dd($varian);
                        $wh_varian = array();
                        $voucer_isFix = "Y";
                        for($i=0; $i < count($varian); $i++){
                            //OLD 
                            /*
                            $total_promo_varian = 0;
                            if($varian[$i]->varian_voucher_expired == 0 && $varian[$i]->voucher_id != null){
                                if($varian[$i]->varian_voucher_is_fixed == 'Y'){
                                    $total_promo_varian = $varian[$i]->varian_voucher_amount;
                                }else{
                                    $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->varian_voucher_amount/100);
                                }
                            }
                            
                            $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                            if ($varian[$i]->voucher_id == null) {
                                $varian_discount = "0";
                                $varian_discount_percentage = "0";
                            } else {
                                if ($varian[$i]->varian_voucher_is_fixed == 'Y') {
                                    $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                    $varian_discount_percentage = round(($total_promo_varian/$varian[$i]->varian_price)*100);
                                } else {
                                    $varian_discount = $varian[$i]->varian_voucher_amount.'%';
                                    $varian_discount_percentage = $varian[$i]->varian_voucher_amount;
                                    $voucer_isFix = "N";
                                }
                            }
                            */
                            //OLD
                            //cari product dari wh or master
                            $varian_price = 0;
                            if($varian[$i]->varian_price_warehouse == null){
                                $varian_price = $varian[$i]->varian_price_product ;
                            }else{
                                 $varian_price = $varian[$i]->varian_price_warehouse;
                            }
                            // dd($varian_price);
                            //PRICE TYPE
                                $total_promo_varian = 0;
                                if($varian[$i]->price_type_id != null){
                                    if($varian[$i]->discount_type == 2){
                                        $total_promo_varian = $varian[$i]->rate ;
                                    }else{
                                        $total_promo_varian = ($varian_price * $varian[$i]->rate/100);
                                    }
                                } else {
                                    if ($check_price_type) {
                                        if($check_price_type->discount_type == 2){
                                            $total_promo_varian = $check_price_type->rate ;
                                        }else{
                                            $total_promo_varian = ($varian_price * $check_price_type->rate/100);
                                        }
                                    }
                                }
                                
                                $varian_price_after_discount = $varian_price - $total_promo_varian;
                                if ($varian[$i]->price_type_id == null) {
                                    if ($check_price_type) {
                                        if ($check_price_type->discount_type == 2) {
                                            $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                            $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                        } else {
                                            $varian_discount = $check_price_type->rate.'%';
                                            $varian_discount_percentage = $check_price_type->rate;
                                            $voucer_isFix = "N";
                                        }
                                    } else {
                                        $varian_discount = "0";
                                        $varian_discount_percentage = "0";   
                                    }
                                } else {
                                    if ($varian[$i]->discount_type == 2) {
                                        $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                        $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                    } else {
                                        $varian_discount = $varian[$i]->rate.'%';
                                        $varian_discount_percentage = $varian[$i]->rate;
                                        $voucer_isFix = "N";
                                    }
                                }
                            // PRICE TYPE


                            $getYukmarket = DB::table('00_organization')
                                            ->select('organization_type_id')
                                            ->where('organization_id', $varian[$i]->org_id)
                                            ->where('organization_type_id',1)
                                            ->first();
                            $varian_stock = 0;
                            if ($varian[$i]->stockable_flag > 0) {
                                $varian_stock = !isset($varian[$i]->varian_stock) ? 0 : $varian[$i]->varian_stock;
                                $wh_stock = DB::table('00_warehouse')
                                    ->select('*')
                                    ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                                    ->where('00_warehouse.parent_warehouse_id', '=', $wh->warehouse_id)
                                    ->where('00_inventory.prod_id', '=', $varian[$i]->product_id)
                                    ->get();
                                // dd($wh_stock);
                                foreach ($wh_stock as $whs) {
                                    $booking_qty = isset($whs->booking_qty) ?  floor($whs->booking_qty) : 0;
                                    $stock = isset($whs->stock) ?  floor($whs->stock) : 0;
                                    $varian_stock += $stock - $booking_qty;
                                }
                            } else {
                                $getStock_parent = DB::table('00_inventory')
                                                    ->join('00_organization', '00_organization.organization_id', '=', '00_inventory.organization_id')
                                                    // ->select('organization_type_id')
                                                    // ->where('organization_id', DB::raw("'1'"))
                                                    ->where('00_inventory.prod_id',$varian[$i]->prod_stok_id)
                                                    ->where('00_inventory.warehouse_id',$wh->warehouse_id)
                                                    ->first();

                                if (isset($getStock_parent)) {
                                    $stock_parent = $getStock_parent->stock - $getStock_parent->booking_qty;
                                    $convert = new UomConvert;
                                    $parent_prod = DB::table('00_product')->select('uom_id','uom_value')->where('prod_id','=',$product_header_id)->first();
                                    // $parent_finalStock = $convert->convertToStock($parent_prod->uom_id, $parent_prod->uom_value, $parent_prod->uom_id, 1);
                                    // echo "<Pre>";
                                    // print_r($product_header_id);
                                    // echo "</Pre>";
                                    // exit();
                                    $parent_uom_value = $parent_prod->uom_value * $stock_parent;
                                    $finalStock = $convert->convertToStock( $parent_prod->uom_id, $parent_uom_value, $varian[$i]->varian_weight_uom_id, $varian[$i]->variant_weight_value);
                                    $varian_stock = floor($finalStock);

                                    $wh_stock = DB::table('00_warehouse')
                                        ->select('*')
                                        ->leftJoin('00_inventory', function($join){
                                                         $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                         $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                     })
                                        ->where('00_warehouse.parent_warehouse_id', '=', $wh->warehouse_id)
                                        ->where('00_inventory.prod_id', '=', $product_header_id)
                                        ->get();
                                    foreach ($wh_stock as $whs) {
                                        $booking_qty = isset($whs->booking_qty) ?  floor($whs->booking_qty) : 0;
                                        $stock = isset($whs->stock) ?  floor($whs->stock) : 0;
                                        $varian_stock += $stock - $booking_qty;
                                    }
                                }
                                    
                            }

                            $checkCart = DB::table('10_cart')
                                            ->where([
                                                ['buyer_user_id','=',$request->input('user_id')],
                                                ['warehouse_id','=',$wh->warehouse_id],
                                                ['prod_id','=',$varian[$i]->product_id],
                                                ['is_agent','=',$request->input('is_agent')],
                                            ])
                                            ->first();
                            if (isset($checkCart)) {
                                $varian_stock = $varian_stock - $checkCart->quantity;
                            }
                            $checkBooking = DB::table('00_warehouse')
                                            ->select('*')
                                            ->leftJoin('00_inventory', function($join){
                                                             $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                             $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                         })
                                            ->where('00_warehouse.warehouse_id', '=', $wh->warehouse_id)
                                            ->where('00_inventory.prod_id', '=', $varian[$i]->product_id)
                                            ->first();
                            if (isset($checkBooking)) {
                                $varian_stock = $varian_stock - $checkBooking->booking_qty;
                            }
                            if ($getYukmarket or $varian[$i]->org_id == null) :
                                //parsing nama varian
                                if (strpos($varian[$i]->varian_name, '-') !== false) {
                                    $pars=explode("-",$varian[$i]->varian_name);
                                    $varianName=$pars[1];
                                }else{
                                    $varianName=$varian[$i]->varian_name;
                                }
                                $obj_varian = array(
                                    'org_id' => $varian[$i]->org_id,
                                    'warehouse_id' => $varian[$i]->warehouse_id,
                                    'product_id' => $varian[$i]->product_id,
                                    'varian_name' => $varianName,
                                    'varian_stockable' => $varian[$i]->stockable_flag,
                                    'varian_price_before_discount' => $varian_price,
                                    'varian_price_after_discount' => round($varian_price_after_discount),
                                    'varian_stock' => round($varian_stock),
                                    'varian_weight' => (string)($varian[$i]->variant_weight_value+0),
                                    'varian_uom' => $varian[$i]->varian_weight_uom,
                                    'varian_pack_weight' => (string)($varian[$i]->variant_pack_weight+0),
                                    'varian_pack_uom' => "Kg",
                                    'varian_discount' => (string)$varian_discount,
                                    'varian_discount_percentage' => (string)$varian_discount_percentage
                                    // 'expired' => $varian[$i]->varian_voucher_expired
                                );
                                array_push($varian_array, $obj_varian);
                                array_push($wh_varian, $obj_varian);
                            endif;
                        }
                        $data_warehouse = array(
                            'warehouse_id' => $wh->warehouse_id,
                            'warehouse_name' => $wh->warehouse_name,
                            'warehouse_provinsi_id' => $wh->provinsi_id,
                            'warehouse_provinsi_name' => $wh->provinsi_name,
                            'warehouse_kota_id' => $wh->kabupaten_kota_id,
                            'warehouse_kota_name' => $wh->kabupaten_kota_name,
                            'varian' => $wh_varian
                        );
                        array_push($arr_warehouse, $data_warehouse);
                    }
                    $lowestKeyBeforeDiscount = '';
                    $highestKeyBeforeDiscount = '';
                    $lowestKeyAfterDiscount = '';
                    $highestKeyAfterDiscount = '';
                    $lowestDiscount = '';
                    $highestDiscount = '';
                    $stockInVariant = 0;
                    foreach($varian_array as $arra){
                        //mencari harga terendah yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value < $lowestKeyBeforeDiscount ) ||( $lowestKeyBeforeDiscount== '')){
                                $lowestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value > $highestKeyBeforeDiscount ) ||( $highestKeyBeforeDiscount== '')){
                                $highestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga terendah yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value < $lowestKeyAfterDiscount ) ||( $lowestKeyAfterDiscount== '')){
                                $lowestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value > $highestKeyAfterDiscount ) ||( $highestKeyAfterDiscount== '')){
                                $highestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari diskon terendah
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //mencari diskon tertinggi
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //menjumlahkan stok untuk header
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_stock'){
                                $stockInVariant =+ $stockInVariant + $value;
                            }
                        }
                    }
                    $min_value_before_discount = $lowestKeyBeforeDiscount;
                    $max_value_before_discount = $highestKeyBeforeDiscount;
                    $min_value_after_discount = $lowestKeyAfterDiscount;
                    $max_value_after_discount = $highestKeyAfterDiscount;
                    $min_discount = $lowestDiscount;
                    $max_discount = $highestDiscount;
                    $varianStock = $stockInVariant;

                    //set header is_varian, price, stok, dan diskon         
                    if(count($varian_array) > 0) { 
                        $is_varian = true;
                        if ($min_value_before_discount != $max_value_before_discount) {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                        } else {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount);
                        }

                        if ($min_value_after_discount != $max_value_after_discount) {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                        } else {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount);
                        }
                        $discount = "0";
                        if ($min_discount != 0 and $max_discount != 0) {
                            if ($voucer_isFix != "Y") {
                                $discount = $min_discount."% ~ ".$max_discount."%";
                            } else {
                                $discount = $min_discount." ~ ".$max_discount;
                            }
                        } else {
                            if ($min_discount != 0) {
                                $discount = $min_discount;
                            } elseif ($max_discount != 0) {
                                $discount = $max_discount;
                            }
                        }
                        // $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                        $stock = $varianStock;
                    } else { 
                        $is_varian = false;
                        $price_before_discount = $this->currency->convertToCurrency($product[$b]->product_price);
                        $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                        $discount = strval($product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount));
                        $stock = $product[$b]->stock;
                    }
                // GET VARIAN 

                // RESUTL GET PRODUCT
                    // $product_name = isset($product[$b]->product_sku_id) ? $product[$b]->product_sku_name : $product[$b]->product_name;
                    $product_name=$product[$b]->product_name;
                    $obj = array(
                        'product_id' => $product[$b]->product_id,
                        'product_name' => $product_name,
                        'product_image' => asset($product[$b]->product_image),
                        'product_description' => $product[$b]->product_description,
                        // 'admin_id' => $product[$b]->created_by,
                        // 'admin_name' => "Indocyber", //$product[$b]->admin_name
                        // 'warehouse_id' => $product[$b]->warehouse_id,
                        // 'warehouse_name' => $product[$b]->warehouse_name, //$product[$b]->admin_name
                        'product_stock' => round($stock),
                        // 'inventory_stock' => $product[$b]->inventory_stock,
                        'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                        'tax_value' => number_format($product[$b]->tax_value,2),
                        // 'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                        'product_weight_value' => ROUND($product[$b]->uom_value,0),
                        'product_weight_uom' => $product[$b]->uom_name,
                        'product_discount' => $discount,
                        'product_price_before_discount' => $price_before_discount,
                        'product_price_after_discount' => $price_after_discount,
                        // 'product_created_date' => $product[$b]->product_created_date,
                        // 'position_date' => $product[$b]->position_date,
                        // 'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                        'is_varian' => $is_varian,
                        'warehouse' => $arr_warehouse
                        // 'varian' =>  $varian_array
                    );
                    array_push($product_array,$obj);
                // RESUTL GET PRODUCT
            endif;
        }

        usort($product_array, function($a, $b) {
            return $b['product_stock'] <=> $a['product_stock'];
        }); 

        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        } 
    }

    public function getLocation()
    {
        $query = DB::table('00_provinsi')
                    ->selectRaw('00_provinsi.provinsi_id, 00_provinsi.provinsi_name, 00_kabupaten_kota.kabupaten_kota_id, 00_kabupaten_kota.kabupaten_kota_name')
                    ->leftJoin('00_kabupaten_kota', '00_kabupaten_kota.provinsi_id', '=', '00_provinsi.provinsi_id')
                    ->where('00_provinsi.provinsi_id','!=',1)
                    ->get();

        if(!$query){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $query], 200);
        }
    }

    public function getProductPriceType($id = null, Request $request)
    {
        $flag = "product";
        $keyword = $request->input('keyword');
        $product_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
            )
            ->when($keyword, function ($query, $keyword) {
                return $query->where('00_product.prod_name','like','%'.$keyword.'%');
            })
            ->where('00_product.active_flag',1)
            ->where('00_category.active_flag',1)
            ->whereNull('00_product.parent_prod_id')
            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
            ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
            ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
            ->leftJoin('98_user','98_user.user_id','=','00_product.created_by')
            ->groupBy('00_product_sku.product_sku_id');

        if($id != "0"){
            $product_sku->where('00_category.category_id', $id);
        }
        if($request->filterByMin != ''){
            $product_sku->where('00_product.prod_price', '>=', $request->filterByMin);
        }

        if($request->filterByMax != ''){
            $product_sku->where('00_product.prod_price', '<=', $request->filterByMax);
        }
        
        if($request->isPromoMenu == 1){
            $flag = "promo";
            $product_sku->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])
            ->where('00_vouchers.active_flag',1);
        }

        if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
            $product_sku->orderBy('00_product.position_date', 'DESC');
        }else if($request->sortBy == 2){ //terlaris
            $product_sku->orderBy('sold_qty','DESC');
        }else if($request->sortBy == 3){//termurah
            $product_sku->orderBy('00_product.prod_price', 'ASC');
        }else if($request->sortBy == 4){//termahal
            $product_sku->orderBy('00_product.prod_price', 'DESC');
        }

        // $product = $product_non_sku->union($product_sku)->orderBy('position_date', 'DESC')->get();
        $product = $product_sku->orderBy('position_date', 'DESC')->get();

        $insert = 0;
        $inserted_obj =  array(
            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
            'keyword' => $keyword,
            'active_flag' => 1
        );
        if($request->input('keyword') != "" && $request->input('keyword') != null){
            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
        }

        $check_price_type = DB::table('98_user as user')
                        ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
                        ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
                        ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
                        ->where('user.user_id','=',$request->input('user_id'))
                        ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
                        ->first();
        $product_array = array();
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            $product_header_id = $product[$b]->product_id;
            $location_id = $request->filterByLocation;
            $warehouse = DB::table('00_product_warehouse')
                        ->select('00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name','00_provinsi.provinsi_id', '00_provinsi.provinsi_name')
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_address','00_warehouse.address_id','=','00_address.address_id')
                        ->leftJoin('00_kabupaten_kota','00_address.kabupaten_kota_id','=','00_kabupaten_kota.kabupaten_kota_id')
                        ->leftJoin('00_provinsi','00_address.provinsi_id','=','00_provinsi.provinsi_id')
                        ->whereNotNull('00_product_warehouse.warehouse_id')
                        ->when($location_id, function ($query, $location_id) {
                            return $query->where('00_address.kabupaten_kota_id',$location_id);
                        })
                        ->where('00_product_warehouse.prod_id',$product_header_id)
                        ->get();
            if (count($warehouse) > 0) :
                // GET VARIAN 
                    $varian_array=array();
                    $arr_warehouse=array();
                    foreach ($warehouse as $wh) {
                        $varian = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.prod_price as varian_price',
                                '00_inventory.stock as varian_stock',
                                '00_product.uom_value as variant_weight_value',
                                '00_uom.uom_name as varian_weight_uom',
                                '00_product.bruto as variant_pack_weight',
                                'uom_pack.uom_name as variant_pack_uom',
                                '00_product_price_type.price_type_id',
                                '00_product_price_type.discount_type',
                                '00_product_price_type.rate'
                                // '00_vouchers.voucher_id as voucher_id',
                                // '00_vouchers.amount as varian_voucher_amount',
                                // '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                // DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_product_price_type.starts_at AND 00_product_price_type.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_product_price_type.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_uom as uom_pack', 'uom_pack.uom_id', '=', '00_product.bruto_uom_id')
                            // ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            // ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            // ->whereNotNull('00_product_price_type.price_type_id')
                            // ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            // ->where(function($query) use($request){
                            //     $query->whereNull('00_product_voucher.voucher_id')
                            //     ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            // })
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_price_type.price_type_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_product_price_type.starts_at'),DB::raw('00_product_price_type.expires_at')]);
                            })
                            ->get();

                        $wh_varian = array();
                        $voucer_isFix = "Y";
                        for($i=0; $i < count($varian); $i++){

                            $total_promo_varian = 0;
                            if($varian[$i]->price_type_id != null){
                                if($varian[$i]->discount_type == 2){
                                    $total_promo_varian = $varian[$i]->rate ;
                                }else{
                                    $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->rate/100);
                                }
                            } else {
                                if ($check_price_type) {
                                    if($check_price_type->discount_type == 2){
                                        $total_promo_varian = $check_price_type->rate ;
                                    }else{
                                        $total_promo_varian = ($varian[$i]->varian_price * $check_price_type->rate/100);
                                    }
                                }
                            }
                            
                            $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                            if ($varian[$i]->price_type_id == null) {
                                if ($check_price_type) {
                                    if ($check_price_type->discount_type == 2) {
                                        $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                        $varian_discount_percentage = round(($total_promo_varian/$varian[$i]->varian_price)*100);
                                    } else {
                                        $varian_discount = $check_price_type->rate.'%';
                                        $varian_discount_percentage = $check_price_type->rate;
                                        $voucer_isFix = "N";
                                    }
                                } else {
                                    $varian_discount = "0";
                                    $varian_discount_percentage = "0";   
                                }
                            } else {
                                if ($varian[$i]->discount_type == 2) {
                                    $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                    $varian_discount_percentage = round(($total_promo_varian/$varian[$i]->varian_price)*100);
                                } else {
                                    $varian_discount = $varian[$i]->rate.'%';
                                    $varian_discount_percentage = $varian[$i]->rate;
                                    $voucer_isFix = "N";
                                }
                            }
                            $obj_varian = array(
                                'product_id' => $varian[$i]->product_id,
                                'varian_name' => $varian[$i]->varian_name,
                                'varian_price_before_discount' => $varian[$i]->varian_price,
                                'varian_price_after_discount' => $varian_price_after_discount,
                                'varian_stock' => !isset($varian[$i]->varian_stock) ? 0 : $varian[$i]->varian_stock,
                                'varian_weight' => $varian[$i]->variant_weight_value,
                                'varian_uom' => $varian[$i]->varian_weight_uom,
                                'variant_pack_weight' => $varian[$i]->variant_pack_weight,
                                'variant_pack_uom' => $varian[$i]->variant_pack_uom,
                                'varian_discount' => (string)$varian_discount,
                                'varian_discount_percentage' => (string)$varian_discount_percentage
                                // 'expired' => $varian[$i]->varian_voucher_expired
                            );
                            array_push($varian_array, $obj_varian);
                            array_push($wh_varian, $obj_varian);
                        }
                        $data_warehouse = array(
                            'warehouse_id' => $wh->warehouse_id,
                            'warehouse_name' => $wh->warehouse_name,
                            'warehouse_provinsi_id' => $wh->provinsi_id,
                            'warehouse_provinsi_name' => $wh->provinsi_name,
                            'warehouse_kota_id' => $wh->kabupaten_kota_id,
                            'warehouse_kota_name' => $wh->kabupaten_kota_name,
                            'varian' => $wh_varian
                        );
                        array_push($arr_warehouse, $data_warehouse);
                    }
                    
                    $lowestKeyBeforeDiscount = '';
                    $highestKeyBeforeDiscount = '';
                    $lowestKeyAfterDiscount = '';
                    $highestKeyAfterDiscount = '';
                    $lowestDiscount = '';
                    $highestDiscount = '';
                    $stockInVariant = 0;
                    foreach($varian_array as $arra){
                        //mencari harga terendah yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value < $lowestKeyBeforeDiscount ) ||( $lowestKeyBeforeDiscount== '')){
                                $lowestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value > $highestKeyBeforeDiscount ) ||( $highestKeyBeforeDiscount== '')){
                                $highestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga terendah yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value < $lowestKeyAfterDiscount ) ||( $lowestKeyAfterDiscount== '')){
                                $lowestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value > $highestKeyAfterDiscount ) ||( $highestKeyAfterDiscount== '')){
                                $highestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari diskon terendah
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //mencari diskon tertinggi
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //menjumlahkan stok untuk header
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_stock'){
                                $stockInVariant =+ $stockInVariant + $value;
                            }
                        }
                    }
                    $min_value_before_discount = $lowestKeyBeforeDiscount;
                    $max_value_before_discount = $highestKeyBeforeDiscount;
                    $min_value_after_discount = $lowestKeyAfterDiscount;
                    $max_value_after_discount = $highestKeyAfterDiscount;
                    $min_discount = $lowestDiscount;
                    $max_discount = $highestDiscount;
                    $varianStock = $stockInVariant;

                    //set header is_varian, price, stok, dan diskon         
                    if(count($varian_array) > 0) { 
                        $is_varian = true;
                        if ($min_value_before_discount != $max_value_before_discount) {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                        } else {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount);
                        }

                        if ($min_value_after_discount != $max_value_after_discount) {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                        } else {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount);
                        }
                        $discount = "0";
                        if ($min_discount != 0 and $max_discount != 0) {
                            if ($voucer_isFix != "Y") {
                                $discount = $min_discount."% ~ ".$max_discount."%";
                            } else {
                                $discount = $min_discount." ~ ".$max_discount;
                            }
                        } else {
                            if ($min_discount != 0) {
                                $discount = $min_discount;
                            } elseif ($max_discount != 0) {
                                $discount = $max_discount;
                            }
                        }
                        // $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                        $stock = $varianStock;
                    } else { 
                        $is_varian = false;
                        $price_before_discount = $this->currency->convertToCurrency($product[$b]->product_price);
                        $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                        $discount = strval($product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount));
                        $stock = $product[$b]->stock;
                    }
                // GET VARIAN 

                // RESUTL GET PRODUCT
                    $product_name = isset($product[$b]->product_sku_id) ? $product[$b]->product_sku_name : $product[$b]->product_name;
                    $obj = array(
                        'product_id' => $product[$b]->product_id,
                        'product_name' => $product_name,
                        'product_image' => asset($product[$b]->product_image),
                        // 'admin_id' => $product[$b]->created_by,
                        // 'admin_name' => "Indocyber", //$product[$b]->admin_name
                        // 'warehouse_id' => $product[$b]->warehouse_id,
                        // 'warehouse_name' => $product[$b]->warehouse_name, //$product[$b]->admin_name
                        'product_stock' => $stock,
                        // 'inventory_stock' => $product[$b]->inventory_stock,
                        'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                        'tax_value' => number_format($product[$b]->tax_value,2),
                        'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                        'product_weight_uom' => $product[$b]->uom_name,
                        'product_discount' => $discount,
                        'product_price_before_discount' => $price_before_discount,
                        'product_price_after_discount' => $price_after_discount,
                        // 'product_created_date' => $product[$b]->product_created_date,
                        // 'position_date' => $product[$b]->position_date,
                        // 'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                        'is_varian' => $is_varian,
                        'warehouse' => $arr_warehouse
                        // 'varian' =>  $varian_array
                    );
                    array_push($product_array,$obj);
                // RESUTL GET PRODUCT
            endif;
        }
        // dd($product);
        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        } 
    }

    public function getProductById($id = null, Request $request)
   {
        $flag = "product";
        $keyword = $request->input('keyword');
        $product_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                // DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                // '00_inventory.warehouse_id',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
            )
            ->when($keyword, function ($query, $keyword) {
                return $query->where('00_product.prod_name','like','%'.$keyword.'%');
            })
            ->where('00_product.active_flag',1)
            ->where('00_category.active_flag',1)
            ->whereNull('00_product.parent_prod_id')
            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
            ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
            ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
            ->leftJoin('98_user','98_user.user_id','=','00_product.created_by')
            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
            ->groupBy('00_product_sku.product_sku_id');

        if($id != "0"){
            $product_sku->where('00_product.prod_id', $id);
        }
        if($request->product_id != ''){
            $product_sku->where('00_product.prod_id', $request->product_id);
        }

        // $product = $product_non_sku->union($product_sku)->orderBy('position_date', 'DESC')->get();
        $product = $product_sku->orderBy('position_date', 'DESC')->get();
        // print_r($product);
        // exit;
        $insert = 0;
        $inserted_obj =  array(
            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
            'keyword' => $keyword,
            'is_agent' => $request->input('is_agent'),
            'active_flag' => 1
        );
        if($request->input('keyword') != "" && $request->input('keyword') != null){
            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
        }
        
        $check_price_type = null;
        if ($request->input('is_agent') > 0) :
            $check_price_type = DB::table('98_user as user')
                            ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
                            ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
                            ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
                            ->where('user.user_id','=',$request->input('user_id'))
                            ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
                            ->first();
        endif;

        $product_array = array();
        // print_r($product);
        // exit;
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            $product_header_id = $product[$b]->product_id;
            $location_id = $request->filterByLocation;
            $warehouse = DB::table('00_product_warehouse')
                        ->select('00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name','00_provinsi.provinsi_id', '00_provinsi.provinsi_name')
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_address','00_warehouse.address_id','=','00_address.address_id')
                        ->leftJoin('00_kabupaten_kota','00_address.kabupaten_kota_id','=','00_kabupaten_kota.kabupaten_kota_id')
                        ->leftJoin('00_provinsi','00_address.provinsi_id','=','00_provinsi.provinsi_id')
                        ->where('00_warehouse.warehouse_type_id', 2)
                        // ->whereNotNull('00_warehouse.parent_warehouse_id')
                        ->whereNotNull('00_product_warehouse.warehouse_id')
                        ->when($location_id, function ($query, $location_id) {
                            return $query->where('00_address.kabupaten_kota_id',$location_id);
                        })
                        ->where('00_product_warehouse.prod_id',$product_header_id)
                        ->get();
                // dd($warehouse);
            if (count($warehouse) > 0) :
                // GET VARIAN 
                    $varian_array=array();
                    $arr_warehouse=array();
                    foreach ($warehouse as $wh) {
                        $varian = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.stockable_flag', 
                                '00_product.prod_price as varian_price_product',
                                '00_product_warehouse.prod_price as varian_price_warehouse',
                                '00_inventory.stock as varian_stock',
                                '00_inventory.organization_id as org_id',
                                '00_product.uom_value as variant_weight_value',
                                '00_product.uom_id as varian_weight_uom_id', 
                                '00_uom.uom_name as varian_weight_uom',
                                '00_product.bruto as variant_pack_weight',
                                'uom_pack.uom_name as variant_pack_uom',
                                '00_vouchers.voucher_id as voucher_id',
                                '00_vouchers.amount as varian_voucher_amount',
                                '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                '00_product_warehouse.warehouse_id',
                                '00_product_price_type.price_type_id',
                                '00_product_price_type.discount_type',
                                '00_product_price_type.rate',
                                // '00_organization.organization_type_id',
                                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_uom as uom_pack', 'uom_pack.uom_id', '=', '00_product.bruto_uom_id')
                            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            // ->leftJoin('00_organization', '00_inventory.organization_id', '=', '00_organization.organization_id')
                            ->leftJoin('00_product_warehouse','00_product.prod_id','=','00_product_warehouse.prod_id')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            ->whereNotNull('00_product.product_sku_id')
                            // ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            ->where('00_product.parent_prod_id', $product_header_id)
                            ->where('00_product_warehouse.warehouse_id', $wh->warehouse_id)
                            // ->where(function($query) {
                            //     $query->where('00_organization.organization_id', 1)
                            //           ->orWhere('00_organization.organization_id', null);
                            // })
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_voucher.voucher_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            })
                            ->get();
                        // dd($varian);
                        $wh_varian = array();
                        $voucer_isFix = "Y";
                        for($i=0; $i < count($varian); $i++){
                            //OLD 
                            /*
                            $total_promo_varian = 0;
                            if($varian[$i]->varian_voucher_expired == 0 && $varian[$i]->voucher_id != null){
                                if($varian[$i]->varian_voucher_is_fixed == 'Y'){
                                    $total_promo_varian = $varian[$i]->varian_voucher_amount;
                                }else{
                                    $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->varian_voucher_amount/100);
                                }
                            }
                            
                            $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                            if ($varian[$i]->voucher_id == null) {
                                $varian_discount = "0";
                                $varian_discount_percentage = "0";
                            } else {
                                if ($varian[$i]->varian_voucher_is_fixed == 'Y') {
                                    $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                    $varian_discount_percentage = round(($total_promo_varian/$varian[$i]->varian_price)*100);
                                } else {
                                    $varian_discount = $varian[$i]->varian_voucher_amount.'%';
                                    $varian_discount_percentage = $varian[$i]->varian_voucher_amount;
                                    $voucer_isFix = "N";
                                }
                            }
                            */
                            //OLD
                            //cari product dari wh or master
                            $varian_price = 0;
                            if($varian[$i]->varian_price_warehouse == null){
                                $varian_price = $varian[$i]->varian_price_product ;
                            }else{
                                 $varian_price = $varian[$i]->varian_price_warehouse;
                            }
                            // dd($varian_price);
                            //PRICE TYPE
                                $total_promo_varian = 0;
                                if($varian[$i]->price_type_id != null){
                                    if($varian[$i]->discount_type == 2){
                                        $total_promo_varian = $varian[$i]->rate ;
                                    }else{
                                        $total_promo_varian = ($varian_price * $varian[$i]->rate/100);
                                    }
                                } else {
                                    if ($check_price_type) {
                                        if($check_price_type->discount_type == 2){
                                            $total_promo_varian = $check_price_type->rate ;
                                        }else{
                                            $total_promo_varian = ($varian_price * $check_price_type->rate/100);
                                        }
                                    }
                                }
                                
                                $varian_price_after_discount = $varian_price - $total_promo_varian;
                                if ($varian[$i]->price_type_id == null) {
                                    if ($check_price_type) {
                                        if ($check_price_type->discount_type == 2) {
                                            $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                            $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                        } else {
                                            $varian_discount = $check_price_type->rate.'%';
                                            $varian_discount_percentage = $check_price_type->rate;
                                            $voucer_isFix = "N";
                                        }
                                    } else {
                                        $varian_discount = "0";
                                        $varian_discount_percentage = "0";   
                                    }
                                } else {
                                    if ($varian[$i]->discount_type == 2) {
                                        $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                        $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                    } else {
                                        $varian_discount = $varian[$i]->rate.'%';
                                        $varian_discount_percentage = $varian[$i]->rate;
                                        $voucer_isFix = "N";
                                    }
                                }
                            // PRICE TYPE


                            $getYukmarket = DB::table('00_organization')
                                            ->select('organization_type_id')
                                            ->where('organization_id', $varian[$i]->org_id)
                                            ->where('organization_type_id',1)
                                            ->first();
                            $varian_stock = 0;
                            if ($varian[$i]->stockable_flag > 0) {
                                $varian_stock = !isset($varian[$i]->varian_stock) ? 0 : $varian[$i]->varian_stock;
                                $wh_stock = DB::table('00_warehouse')
                                    ->select('*')
                                    ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                                    ->where('00_warehouse.parent_warehouse_id', '=', $wh->warehouse_id)
                                    ->where('00_inventory.prod_id', '=', $varian[$i]->product_id)
                                    ->get();
                                foreach ($wh_stock as $whs) {
                                    $varian_stock += isset($whs->stock) ?  floor($whs->stock) : 0;
                                }
                            } else {
                                $getStock_parent = DB::table('00_inventory')
                                                    ->join('00_organization', '00_organization.organization_id', '=', '00_inventory.organization_id')
                                                    // ->select('organization_type_id')
                                                    // ->where('organization_id', DB::raw("'1'"))
                                                    ->where('00_inventory.prod_id',$product_header_id)
                                                    ->where('00_inventory.warehouse_id',$wh->warehouse_id)
                                                    ->first();

                                if (isset($getStock_parent)) {
                                    $stock_parent = $getStock_parent->stock - $getStock_parent->booking_qty;
                                    $convert = new UomConvert;
                                    $parent_prod = DB::table('00_product')->select('uom_id','uom_value')->where('prod_id','=',$product_header_id)->first();
                                    // $parent_finalStock = $convert->convertToStock($parent_prod->uom_id, $parent_prod->uom_value, $parent_prod->uom_id, 1);
                                    // echo "<Pre>";
                                    // print_r($product_header_id);
                                    // echo "</Pre>";
                                    // exit();
                                    $parent_uom_value = $parent_prod->uom_value * $stock_parent;
                                    $finalStock = $convert->convertToStock( $parent_prod->uom_id, $parent_uom_value, $varian[$i]->varian_weight_uom_id, $varian[$i]->variant_weight_value);
                                    $varian_stock = floor($finalStock);

                                    $wh_stock = DB::table('00_warehouse')
                                        ->select('*')
                                        ->leftJoin('00_inventory', function($join){
                                                         $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                         $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                     })
                                        ->where('00_warehouse.parent_warehouse_id', '=', $wh->warehouse_id)
                                        ->where('00_inventory.prod_id', '=', $product_header_id)
                                        ->get();
                                    foreach ($wh_stock as $whs) {
                                        $varian_stock += isset($whs->stock) ?  floor($whs->stock) : 0;
                                    }
                                }
                                    
                            }

                            $checkCart = DB::table('10_cart')
                                            ->where([
                                                ['buyer_user_id','=',$request->input('user_id')],
                                                ['warehouse_id','=',$wh->warehouse_id],
                                                ['prod_id','=',$varian[$i]->product_id],
                                                ['is_agent','=',$request->input('is_agent')],
                                            ])
                                            ->first();
                            if (isset($checkCart)) {
                                $varian_stock = $varian_stock - $checkCart->quantity;
                            }
                            if ($getYukmarket or $varian[$i]->org_id == null) :
                                $obj_varian = array(
                                    'org_id' => $getYukmarket ? $varian[$i]->org_id : 1,
                                    'warehouse_id' => $varian[$i]->warehouse_id,
                                    'product_id' => $varian[$i]->product_id,
                                    'varian_name' => $varian[$i]->varian_name,
                                    'varian_stockable' => $varian[$i]->stockable_flag,
                                    'varian_price_before_discount' => $varian_price,
                                    'varian_price_after_discount' => round($varian_price_after_discount),
                                    'varian_stock' => $varian_stock,
                                    'varian_weight' => (string)($varian[$i]->variant_weight_value+0),
                                    'varian_uom' => $varian[$i]->varian_weight_uom,
                                    'varian_pack_weight' => (string)($varian[$i]->variant_pack_weight+0),
                                    'varian_pack_uom' => "Kg",
                                    'varian_discount' => (string)$varian_discount,
                                    'varian_discount_percentage' => (string)$varian_discount_percentage
                                    // 'expired' => $varian[$i]->varian_voucher_expired
                                );
                                array_push($varian_array, $obj_varian);
                                array_push($wh_varian, $obj_varian);
                            endif;
                        }
                        $data_warehouse = array(
                            'warehouse_id' => $wh->warehouse_id,
                            'warehouse_name' => $wh->warehouse_name,
                            'warehouse_provinsi_id' => $wh->provinsi_id,
                            'warehouse_provinsi_name' => $wh->provinsi_name,
                            'warehouse_kota_id' => $wh->kabupaten_kota_id,
                            'warehouse_kota_name' => $wh->kabupaten_kota_name,
                            'varian' => $wh_varian
                        );
                        array_push($arr_warehouse, $data_warehouse);
                    }
                    $lowestKeyBeforeDiscount = '';
                    $highestKeyBeforeDiscount = '';
                    $lowestKeyAfterDiscount = '';
                    $highestKeyAfterDiscount = '';
                    $lowestDiscount = '';
                    $highestDiscount = '';
                    $stockInVariant = 0;
                    foreach($varian_array as $arra){
                        //mencari harga terendah yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value < $lowestKeyBeforeDiscount ) ||( $lowestKeyBeforeDiscount== '')){
                                $lowestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value > $highestKeyBeforeDiscount ) ||( $highestKeyBeforeDiscount== '')){
                                $highestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga terendah yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value < $lowestKeyAfterDiscount ) ||( $lowestKeyAfterDiscount== '')){
                                $lowestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value > $highestKeyAfterDiscount ) ||( $highestKeyAfterDiscount== '')){
                                $highestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari diskon terendah
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //mencari diskon tertinggi
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //menjumlahkan stok untuk header
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_stock'){
                                $stockInVariant =+ $stockInVariant + $value;
                            }
                        }
                    }
                    $min_value_before_discount = $lowestKeyBeforeDiscount;
                    $max_value_before_discount = $highestKeyBeforeDiscount;
                    $min_value_after_discount = $lowestKeyAfterDiscount;
                    $max_value_after_discount = $highestKeyAfterDiscount;
                    $min_discount = $lowestDiscount;
                    $max_discount = $highestDiscount;
                    $varianStock = $stockInVariant;

                    //set header is_varian, price, stok, dan diskon         
                    if(count($varian_array) > 0) { 
                        $is_varian = true;
                        if ($min_value_before_discount != $max_value_before_discount) {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                        } else {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount);
                        }

                        if ($min_value_after_discount != $max_value_after_discount) {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                        } else {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount);
                        }
                        $discount = "0";
                        if ($min_discount != 0 and $max_discount != 0) {
                            if ($voucer_isFix != "Y") {
                                $discount = $min_discount."% ~ ".$max_discount."%";
                            } else {
                                $discount = $min_discount." ~ ".$max_discount;
                            }
                        } else {
                            if ($min_discount != 0) {
                                $discount = $min_discount;
                            } elseif ($max_discount != 0) {
                                $discount = $max_discount;
                            }
                        }
                        // $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                        $stock = $varianStock;
                    } else { 
                        $is_varian = false;
                        $price_before_discount = $this->currency->convertToCurrency($product[$b]->product_price);
                        $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                        $discount = strval($product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount));
                        $stock = $product[$b]->stock;
                    }
                // GET VARIAN 

                // RESUTL GET PRODUCT
                    $product_name = isset($product[$b]->product_sku_id) ? $product[$b]->product_sku_name : $product[$b]->product_name;
                    $obj = array(
                        'product_id' => $product[$b]->product_id,
                        'product_name' => $product_name,
                        'product_image' => asset($product[$b]->product_image),
                        'product_description' => $product[$b]->product_description,
                        // 'admin_id' => $product[$b]->created_by,
                        // 'admin_name' => "Indocyber", //$product[$b]->admin_name
                        // 'warehouse_id' => $product[$b]->warehouse_id,
                        // 'warehouse_name' => $product[$b]->warehouse_name, //$product[$b]->admin_name
                        'product_stock' => $stock,
                        // 'inventory_stock' => $product[$b]->inventory_stock,
                        'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                        'tax_value' => number_format($product[$b]->tax_value,2),
                        'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                        'product_weight_uom' => $product[$b]->uom_name,
                        'product_discount' => $discount,
                        'product_price_before_discount' => $price_before_discount,
                        'product_price_after_discount' => $price_after_discount,
                        // 'product_created_date' => $product[$b]->product_created_date,
                        // 'position_date' => $product[$b]->position_date,
                        // 'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                        'is_varian' => $is_varian,
                        'warehouse' => $arr_warehouse
                        // 'varian' =>  $varian_array
                    );
                    array_push($product_array,$obj);
                // RESUTL GET PRODUCT
            endif;
        }
        // print_r($product_array);
        // exit;
        usort($product_array, function($a, $b) {
            return $b['product_stock'] <=> $a['product_stock'];
        }); 
        
        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        } 
    }
    
    public function getProductbyName(Request $request)
    {
        $flag = "product";
        $keyword = $request->input('product_name');
        $product_sku = DB::table('00_product')
            ->select(
                '00_product.prod_id AS product_id',
                '00_product.prod_name AS product_name',
                '00_product.prod_desc AS product_description',
                '00_product.prod_code AS product_code',
                '00_product.preorder_flag',
                '00_product.prod_price AS product_price',
                '00_product.prod_image AS product_image',
                '00_product.brand_id',
                '00_brand.brand_code',
                '00_brand.brand_name',
                '00_brand.brand_desc AS brand_description',
                '00_brand.brand_image',
                '00_brand.active_flag AS brand_active_status',
                '00_product.uom_id',
                '00_product.uom_value',
                '00_uom.uom_name',
                '00_uom.uom_desc AS uom_description',
                '00_uom.is_decimal',
                '00_product.category_id',
                '00_category.category_name',
                '00_category.category_desc AS category_description',
                '00_category.category_image',
                '00_category.active_flag AS category_active_flag',
                '00_product.stock',
                '00_product.is_taxable_product',
                '00_product.tax_value',
                '00_product.active_flag AS product_active_status',
                '00_product.created_by',
                '00_product.created_date AS product_created_date',
                '00_product.position_date',
                '98_user.user_name AS admin_name',
                DB::raw('(SELECT (CASE WHEN COUNT(10_order_detail.prod_id) IS NULL THEN 0 ELSE COUNT(10_order_detail.prod_id) END) FROM 10_order_detail WHERE 10_order_detail.prod_id = 00_product.prod_id) AS sold_qty'), 
                // DB::raw('(select quantity from 10_cart where prod_id = 00_product.prod_id and buyer_user_id = '.$request->user_id.') as is_cart'),
                '00_product_sku.product_sku_name',
                '00_product_sku.product_sku_id',
                '00_vouchers.voucher_id',
                '00_vouchers.amount',
                '00_vouchers.is_fixed',
                '00_vouchers.max_value_price',
                '00_vouchers.min_price_requirement',
                // '00_inventory.warehouse_id',
                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired')
            )
            ->when($keyword, function ($query, $keyword) {
                return $query->where('00_product.prod_name','like','%'.$keyword.'%');
            })
            ->where('00_product.active_flag',1)
            ->where('00_category.active_flag',1)
            ->whereNull('00_product.parent_prod_id')
            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
            ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
            ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
            ->leftJoin('98_user','98_user.user_id','=','00_product.created_by')
            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
            ->groupBy('00_product_sku.product_sku_id');
            
//            $product_sku->where('00_product.prod_price', '>=', $request->filterByMin);
        
//            $product_sku->where('00_product.prod_price', '<=', $request->filterByMax);

//        if($request->sortBy == 1 || $request->sortBy == ""){ //terbaru
//            $product_sku->orderBy('00_product.position_date', 'DESC');
//        }else if($request->sortBy == 2){ //terlaris
//            $product_sku->orderBy('sold_qty','DESC');
//        }else if($request->sortBy == 3){//termurah
            $product_sku->orderBy('00_product.prod_price', 'ASC');
//        }else if($request->sortBy == 4){//termahal
//            $product_sku->orderBy('00_product.prod_price', 'DESC');
//        }

        // $product = $product_non_sku->union($product_sku)->orderBy('position_date', 'DESC')->get();
        $product = $product_sku->orderBy('position_date', 'DESC')->get();

//        $insert = 0;
//        $inserted_obj =  array(
//            'customer_id' => ($request->input('user_id') == null || $request->input('user_id') == "" ? null : $request->input('user_id')),
//            'keyword' => $keyword,
//            'is_agent' => $request->input('is_agent'),
//            'active_flag' => 1
//        );
//        if($request->input('keyword') != "" && $request->input('keyword') != null){
//            $insert = DB::table('00_search_history')->insertGetId($inserted_obj);
//        }
        
        $check_price_type = null;
//        if ($request->input('is_agent') > 0) :
//            $check_price_type = DB::table('98_user as user')
//                            ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
//                            ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
//                            ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
//                            ->where('user.user_id','=',$request->input('user_id'))
//                            ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
//                            ->first();
//        endif;

        $product_array = array();
        for ($b=0; $b < count($product); $b++) {
            $total_promo_value = 0;
            if($product[$b]->expired == 0 && $product[$b]->voucher_id != null){
                if($product[$b]->is_fixed == 'Y'){
                    $total_promo_value = $product[$b]->amount;
                }else{
                    $total_promo_value = ($product[$b]->product_price * $product[$b]->amount/100);
                }

            }
            $product_price_after_discount = $product[$b]->product_price - $total_promo_value;
            $product_header_id = $product[$b]->product_id;
//            $location_id = $request->filterByLocation;
            $warehouse = DB::table('00_product_warehouse')
                        ->select('00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_kabupaten_kota.kabupaten_kota_id', '00_kabupaten_kota.kabupaten_kota_name','00_provinsi.provinsi_id', '00_provinsi.provinsi_name')
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_address','00_warehouse.address_id','=','00_address.address_id')
                        ->leftJoin('00_kabupaten_kota','00_address.kabupaten_kota_id','=','00_kabupaten_kota.kabupaten_kota_id')
                        ->leftJoin('00_provinsi','00_address.provinsi_id','=','00_provinsi.provinsi_id')
                        ->whereNotNull('00_product_warehouse.warehouse_id')
                        ->where('00_product_warehouse.prod_id',$product_header_id)
                        ->get();
            if (count($warehouse) > 0) :
                // GET VARIAN 
                    $varian_array=array();
                    $arr_warehouse=array();
                    foreach ($warehouse as $wh) {
                        $varian = DB::table('00_product')
                            ->select('00_product.prod_id as product_id', 
                                '00_product.prod_name as varian_name', 
                                '00_product.stockable_flag', 
                                '00_product.prod_price as varian_price_product',
                                '00_product_warehouse.prod_price as varian_price_warehouse',
                                '00_inventory.stock as varian_stock',
                                '00_inventory.organization_id as org_id',
                                '00_product.uom_value as variant_weight_value',
                                '00_product.uom_id as varian_weight_uom_id', 
                                '00_uom.uom_name as varian_weight_uom',
                                '00_product.bruto as variant_pack_weight',
                                'uom_pack.uom_name as variant_pack_uom',
                                '00_vouchers.voucher_id as voucher_id',
                                '00_vouchers.amount as varian_voucher_amount',
                                '00_vouchers.is_fixed as varian_voucher_is_fixed',
                                '00_product_warehouse.warehouse_id',
                                '00_product_price_type.price_type_id',
                                '00_product_price_type.discount_type',
                                '00_product_price_type.rate',
                                // '00_organization.organization_type_id',
                                DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                            )
                            ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                            ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                            ->leftJoin('00_uom as uom_pack', 'uom_pack.uom_id', '=', '00_product.bruto_uom_id')
                            ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                            // ->leftJoin('00_organization', '00_inventory.organization_id', '=', '00_organization.organization_id')
                            ->leftJoin('00_product_warehouse','00_product.prod_id','=','00_product_warehouse.prod_id')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->leftJoin('00_inventory', function($join){
                                                     $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                     $join->on('00_inventory.warehouse_id', '=', '00_warehouse.warehouse_id');
                                                     $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
                                                 })
                            // ->leftJoin('00_inventory', '00_inventory.prod_id', '=', '00_product.prod_id')
                            ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', '00_product.prod_id')
                            ->where('00_product.active_flag',1)
                            ->whereNotNull('00_product.product_sku_id')
                            ->where('00_product.product_sku_id', $product[$b]->product_sku_id)
                            ->where('00_product_warehouse.warehouse_id', $wh->warehouse_id)
                            // ->where(function($query) {
                            //     $query->where('00_organization.organization_id', 1)
                            //           ->orWhere('00_organization.organization_id', null);
                            // })
                            ->where(function($query) use($request){
                                $query->whereNull('00_product_voucher.voucher_id')
                                ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                            })
                            ->get();
                        $wh_varian = array();
                        $voucer_isFix = "Y";
                        for($i=0; $i < count($varian); $i++){
                            //OLD 
                            /*
                            $total_promo_varian = 0;
                            if($varian[$i]->varian_voucher_expired == 0 && $varian[$i]->voucher_id != null){
                                if($varian[$i]->varian_voucher_is_fixed == 'Y'){
                                    $total_promo_varian = $varian[$i]->varian_voucher_amount;
                                }else{
                                    $total_promo_varian = ($varian[$i]->varian_price * $varian[$i]->varian_voucher_amount/100);
                                }
                            }
                            
                            $varian_price_after_discount = $varian[$i]->varian_price - $total_promo_varian;
                            if ($varian[$i]->voucher_id == null) {
                                $varian_discount = "0";
                                $varian_discount_percentage = "0";
                            } else {
                                if ($varian[$i]->varian_voucher_is_fixed == 'Y') {
                                    $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                    $varian_discount_percentage = round(($total_promo_varian/$varian[$i]->varian_price)*100);
                                } else {
                                    $varian_discount = $varian[$i]->varian_voucher_amount.'%';
                                    $varian_discount_percentage = $varian[$i]->varian_voucher_amount;
                                    $voucer_isFix = "N";
                                }
                            }
                            */
                            //OLD
                            //cari product dari wh or master
                            $varian_price = 0;
                            if($varian[$i]->varian_price_warehouse == null){
                                $varian_price = $varian[$i]->varian_price_product ;
                            }else{
                                 $varian_price = $varian[$i]->varian_price_warehouse;
                            }
                            // dd($varian_price);
                            //PRICE TYPE
                                $total_promo_varian = 0;
                                if($varian[$i]->price_type_id != null){
                                    if($varian[$i]->discount_type == 2){
                                        $total_promo_varian = $varian[$i]->rate ;
                                    }else{
                                        $total_promo_varian = ($varian_price * $varian[$i]->rate/100);
                                    }
                                } else {
                                    if ($check_price_type) {
                                        if($check_price_type->discount_type == 2){
                                            $total_promo_varian = $check_price_type->rate ;
                                        }else{
                                            $total_promo_varian = ($varian_price * $check_price_type->rate/100);
                                        }
                                    }
                                }
                                
                                $varian_price_after_discount = $varian_price - $total_promo_varian;
                                if ($varian[$i]->price_type_id == null) {
                                    if ($check_price_type) {
                                        if ($check_price_type->discount_type == 2) {
                                            $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                            $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                        } else {
                                            $varian_discount = $check_price_type->rate.'%';
                                            $varian_discount_percentage = $check_price_type->rate;
                                            $voucer_isFix = "N";
                                        }
                                    } else {
                                        $varian_discount = "0";
                                        $varian_discount_percentage = "0";   
                                    }
                                } else {
                                    if ($varian[$i]->discount_type == 2) {
                                        $varian_discount = $this->currency->thousandsCurrencyFormat($total_promo_varian);
                                        $varian_discount_percentage = round(($total_promo_varian/$varian_price)*100);
                                    } else {
                                        $varian_discount = $varian[$i]->rate.'%';
                                        $varian_discount_percentage = $varian[$i]->rate;
                                        $voucer_isFix = "N";
                                    }
                                }
                            // PRICE TYPE


                            $getYukmarket = DB::table('00_organization')
                                            ->select('organization_type_id')
                                            ->where('organization_id', $varian[$i]->org_id)
                                            ->where('organization_type_id',1)
                                            ->first();
                            $varian_stock = 0;
                            if ($varian[$i]->stockable_flag > 0) {
                                $varian_stock = !isset($varian[$i]->varian_stock) ? 0 : $varian[$i]->varian_stock;
                            } else {
                                $getStock_parent = DB::table('00_inventory')
                                                    ->join('00_organization', '00_organization.organization_id', '=', '00_inventory.organization_id')
                                                    // ->select('organization_type_id')
                                                    // ->where('organization_id', DB::raw("'1'"))
                                                    ->where('00_inventory.prod_id',$product_header_id)
                                                    ->where('00_inventory.warehouse_id',$wh->warehouse_id)
                                                    ->first();
                                if (isset($getStock_parent)) {
                                    $stock_parent = $getStock_parent->stock - $getStock_parent->booking_qty;
                                    $convert = new UomConvert;
                                    $parent_prod = DB::table('00_product')->select('uom_id','uom_value')->where('prod_id','=',$product_header_id)->first();
                                    // $parent_finalStock = $convert->convertToStock($parent_prod->uom_id, $parent_prod->uom_value, $parent_prod->uom_id, 1);
                                    // echo "<Pre>";
                                    // print_r($product_header_id);
                                    // echo "</Pre>";
                                    // exit();
                                    $parent_uom_value = $parent_prod->uom_value * $stock_parent;
                                    $finalStock = $convert->convertToStock( $parent_prod->uom_id, $parent_uom_value, $varian[$i]->varian_weight_uom_id, $varian[$i]->variant_weight_value);
                                    $varian_stock = floor($finalStock);
                                }
                                    
                            }

                            $checkCart = DB::table('10_cart')
                                            ->where([
                                                ['buyer_user_id','=',$request->input('user_id')],
                                                ['warehouse_id','=',$wh->warehouse_id],
                                                ['prod_id','=',$varian[$i]->product_id],
                                                ['is_agent','=',$request->input('is_agent')],
                                            ])
                                            ->first();
                            if (isset($checkCart)) {
                                $varian_stock = $varian_stock - $checkCart->quantity;
                            }
                            if ($getYukmarket or $varian[$i]->org_id == null) :
                                $obj_varian = array(
                                    'org_id' => $varian[$i]->org_id,
                                    'warehouse_id' => $varian[$i]->warehouse_id,
                                    'product_id' => $varian[$i]->product_id,
                                    'varian_name' => $varian[$i]->varian_name,
                                    'varian_stockable' => $varian[$i]->stockable_flag,
                                    'varian_price_before_discount' => $varian_price,
                                    'varian_price_after_discount' => round($varian_price_after_discount),
                                    'varian_stock' => $varian_stock,
                                    'varian_weight' => (string)($varian[$i]->variant_weight_value+0),
                                    'varian_uom' => $varian[$i]->varian_weight_uom,
                                    'varian_pack_weight' => (string)($varian[$i]->variant_pack_weight+0),
                                    'varian_pack_uom' => "Kg",
                                    'varian_discount' => (string)$varian_discount,
                                    'varian_discount_percentage' => (string)$varian_discount_percentage
                                    // 'expired' => $varian[$i]->varian_voucher_expired
                                );
                                array_push($varian_array, $obj_varian);
                                array_push($wh_varian, $obj_varian);
                            endif;
                        }
                        $data_warehouse = array(
                            'warehouse_id' => $wh->warehouse_id,
                            'warehouse_name' => $wh->warehouse_name,
                            'warehouse_provinsi_id' => $wh->provinsi_id,
                            'warehouse_provinsi_name' => $wh->provinsi_name,
                            'warehouse_kota_id' => $wh->kabupaten_kota_id,
                            'warehouse_kota_name' => $wh->kabupaten_kota_name,
                            'varian' => $wh_varian
                        );
                        array_push($arr_warehouse, $data_warehouse);
                    }
                    
                    $lowestKeyBeforeDiscount = '';
                    $highestKeyBeforeDiscount = '';
                    $lowestKeyAfterDiscount = '';
                    $highestKeyAfterDiscount = '';
                    $lowestDiscount = '';
                    $highestDiscount = '';
                    $stockInVariant = 0;
                    foreach($varian_array as $arra){
                        //mencari harga terendah yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value < $lowestKeyBeforeDiscount ) ||( $lowestKeyBeforeDiscount== '')){
                                $lowestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sebelum didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_before_discount'){
                                if(($value > $highestKeyBeforeDiscount ) ||( $highestKeyBeforeDiscount== '')){
                                $highestKeyBeforeDiscount = $value;
                                }
                            }
                        }

                        //mencari harga terendah yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value < $lowestKeyAfterDiscount ) ||( $lowestKeyAfterDiscount== '')){
                                $lowestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari harga tertinggi yang sudah didiskon di varian
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_price_after_discount'){
                                if(($value > $highestKeyAfterDiscount ) ||( $highestKeyAfterDiscount== '')){
                                $highestKeyAfterDiscount = $value;
                                }
                            }
                        }

                        //mencari diskon terendah
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value < $lowestDiscount && $value != "0") ||( $lowestDiscount== '')){
                                        $lowestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //mencari diskon tertinggi
                        foreach ($arra as $key=>$value){
                            if ($voucer_isFix != "Y") {
                                if ($key == 'varian_discount_percentage'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            } else {
                                if ($key == 'varian_discount'){
                                    if(($value > $highestDiscount )||( $highestDiscount == '')){
                                        $highestDiscount = $value;
                                    }
                                }
                            }
                        }

                        //menjumlahkan stok untuk header
                        foreach ($arra as $key=>$value){
                            if ($key == 'varian_stock'){
                                $stockInVariant =+ $stockInVariant + $value;
                            }
                        }
                    }
                    $min_value_before_discount = $lowestKeyBeforeDiscount;
                    $max_value_before_discount = $highestKeyBeforeDiscount;
                    $min_value_after_discount = $lowestKeyAfterDiscount;
                    $max_value_after_discount = $highestKeyAfterDiscount;
                    $min_discount = $lowestDiscount;
                    $max_discount = $highestDiscount;
                    $varianStock = $stockInVariant;

                    //set header is_varian, price, stok, dan diskon         
                    if(count($varian_array) > 0) { 
                        $is_varian = true;
                        if ($min_value_before_discount != $max_value_before_discount) {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_before_discount);
                        } else {
                            $price_before_discount = "Rp. ".$this->currency->convertToCurrency($min_value_before_discount);
                        }

                        if ($min_value_after_discount != $max_value_after_discount) {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount)." ~ Rp. ".$this->currency->convertToCurrency($max_value_after_discount);
                        } else {
                            $price_after_discount = "Rp. ".$this->currency->convertToCurrency($min_value_after_discount);
                        }
                        $discount = "0";
                        if ($min_discount != 0 and $max_discount != 0) {
                            if ($voucer_isFix != "Y") {
                                $discount = $min_discount."% ~ ".$max_discount."%";
                            } else {
                                $discount = $min_discount." ~ ".$max_discount;
                            }
                        } else {
                            if ($min_discount != 0) {
                                $discount = $min_discount;
                            } elseif ($max_discount != 0) {
                                $discount = $max_discount;
                            }
                        }
                        // $discount = ($min_discount != 0 ? $min_discount."% ~ " : "").($max_discount != 0 ? $max_discount."%" : "0");
                        $stock = $varianStock;
                    } else { 
                        $is_varian = false;
                        $price_before_discount = $this->currency->convertToCurrency($product[$b]->product_price);
                        $price_after_discount = $this->currency->convertToCurrency($product_price_after_discount); 
                        $discount = strval($product[$b]->voucher_id == null ? 0 : ($product[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($total_promo_value) : $product[$b]->amount));
                        $stock = $product[$b]->stock;
                    }
                // GET VARIAN 

                // RESUTL GET PRODUCT
                    $product_name = isset($product[$b]->product_sku_id) ? $product[$b]->product_sku_name : $product[$b]->product_name;
                    $obj = array(
                        'product_id' => $product[$b]->product_id,
                        'product_name' => $product_name,
                        'product_image' => asset($product[$b]->product_image),
                        'product_description' => $product[$b]->product_description,
                        // 'admin_id' => $product[$b]->created_by,
                        // 'admin_name' => "Indocyber", //$product[$b]->admin_name
                        // 'warehouse_id' => $product[$b]->warehouse_id,
                        // 'warehouse_name' => $product[$b]->warehouse_name, //$product[$b]->admin_name
                        'product_stock' => $stock,
                        // 'inventory_stock' => $product[$b]->inventory_stock,
                        'is_taxable_product' => $product[$b]->is_taxable_product == 'N' ? false : true,
                        'tax_value' => number_format($product[$b]->tax_value,2),
                        'product_weight_value' => $product[$b]->is_decimal == 0 ? ROUND($product[$b]->uom_value,0) : $product[$b]->uom_value,
                        'product_weight_uom' => $product[$b]->uom_name,
                        'product_discount' => $discount,
                        'product_price_before_discount' => $price_before_discount,
                        'product_price_after_discount' => $price_after_discount,
                        // 'product_created_date' => $product[$b]->product_created_date,
                        // 'position_date' => $product[$b]->position_date,
                        // 'is_cart' => $product[$b]->is_cart == null ? 0 : $product[$b]->is_cart,
                        'is_varian' => $is_varian,
                        'warehouse' => $arr_warehouse
                        // 'varian' =>  $varian_array
                    );
                    array_push($product_array,$obj);
                // RESUTL GET PRODUCT
            endif;
        }

        usort($product_array, function($a, $b) {
            return $b['product_stock'] <=> $a['product_stock'];
        }); 

        if(!$product){
            return response()->json(['isSuccess' => false, 'flag' => $flag, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            
            return response()->json(['isSuccess' => true, 'flag' => $flag, 'message' => 'OK', 'data' => $product_array], 200);
        } 
    
    }

}
