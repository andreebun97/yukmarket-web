<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Http;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use File;
use App\Complaint;
use App\ComplaintDetail;
use App\ComplaintAttachment;
use App\ComplaintSolution;
use App\ComplaintCategory;
use App\Helpers\Transaction;
use Mail;
use Config;
use App\Notification;
use App\Helpers\Currency;

class ComplaintController extends Controller
{
    // public function __construct(){
    //     $this->middleware('check-token');
    // }
    
    public function form(Request $request){
        $image_array = array();
        $inserted_data_array = array();
        $folder_name = 'img/uploads/complain/';
        if (!file_exists($folder_name)) {
            mkdir($folder_name, 777, true);
        }
        $complain_pict = $request->input('complain_pict');
        $issue_id = Complaint::insertGetId([
            'issue_category_id' => $request->input('issue_category'),
            'customer_id' => $request->input('user_id'),
            'order_id' => $request->input('order_id'),
            'issue_solution_id' => $request->input('issue_solution'),
            'issue_notes' => $request->input('notes'),
            'issue_status_id' => 1
        ]);
        for ($b=0; $b < count($complain_pict); $b++) {
            $img = $complain_pict[$b];
            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = uniqid() . '.' . $image_type;
            $file = $folder_name . $filename;
    
            if (($image_type == 'jpg' || $image_type == 'jpeg') || $image_type == 'png') {
                Image::make($image_base64)->fit(300,300)->save(($file),80)->stream($image_type,100);
                // file_put_contents($file, $image_base64);
                array_push($image_array, $file);
                if($issue_id > 0){
                    $obj = array(
                        'issue_id' => $issue_id,
                        'attachment' => $file,
                        'created_by' => $request->input('user_id')
                    );
                    array_push($inserted_data_array, $obj);
                }
            }
        }

        if($issue_id > 0){
            $transaction = new Transaction();
            $ticketing_num = $transaction->generateCode($issue_id, 1);
            // for ($i = 12; $i > 0; $i--) {
            //     if($i > 1){
            //         $ticketing_num .= "0";
            //     }else{
            //         $ticketing_num .= $issue_id;
            //     }
            // }
            // $ticketing_num = "YMIS".$ticketing_num;
            Complaint::where('order_id', $request->input('order_id'))->update([
                'ticketing_num' => $ticketing_num
            ]);
            ComplaintAttachment::insert($inserted_data_array);
            $products = $request->input('products');
            $product_array = array();
            for ($b=0; $b < count($products); $b++) { 
                $obj = array(
                    'issue_id' => $issue_id,
                    'prod_id' => $products[$b]["product_id"],
                    'quantity' => $products[$b]["product_qty"],
                    'issue_category_id' => $products[$b]["issue_category"],
                    'issue_detail_notes' => $products[$b]["notes"]
                );
                array_push($product_array, $obj);
            }
            ComplaintDetail::insert($product_array);
        }

        if($issue_id == 0){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
             $this->email($issue_id);
            return response()->json(['isSuccess' => true, 'message' => 'Complain successfully submitted!'], 200);
        }
    }

    // public function solution(){
    //     $complaint_solution = ComplaintSolution::where('active_flag',1)
    //             ->get();

    //     // dd($paymentMethod);

    //     $complaint_solution_array = array();

    //     foreach($complaint_solution as $data){
    //         $obj = array(
    //             'issue_solution_id' => $data->issue_solution_id,
    //             'issue_solution_name' => $data->issue_solution_name,
    //             'description' => $data->description
    //         );
    //         array_push($complaint_solution_array, $obj);
    //     }

    //     if(!$complaint_solution){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $complaint_solution_array], 200);
    //     }
    // }

    // public function category(){
    //     $complaint_category = ComplaintCategory::where('active_flag',1)
    //             ->get();

    //     // dd($paymentMethod);

    //     $complaint_category_array = array();

    //     foreach($complaint_category as $data){
    //         $obj = array(
    //             'issue_category_id' => $data->issue_category_id,
    //             'issue_category_name' => $data->issue_category_name,
    //             'description' => $data->description
    //         );
    //         array_push($complaint_category_array, $obj);
    //     }

    //     if(!$complaint_category){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $complaint_category_array], 200);
    //     }
    // }

    // public function complaintHistory(Request $request)
    // {
    //     $q = DB::table('00_issue as a')
    //         ->select('a.issue_id as complaint_id',
    //             'a.order_id',
    //             'a.ticketing_num as order_code',
    //             'b.customer_name as complaint_name',
    //             'a.issue_date as complaint_date',
    //             'd.issue_solution_id as complaint_solution_id',
    //             'd.issue_solution_name as complaint_solution_name',
    //             'e.issue_status_id as complaint_status_id',
    //             'e.issue_status_name as complaint_status_name'
    //     )
    //         ->leftJoin('00_customer as b', 'b.customer_id', '=', 'a.customer_id')
    //         ->leftJoin('00_issue_detail as c', 'c.issue_id', '=', 'a.issue_id')
    //         ->leftJoin('00_issue_solution as d', 'd.issue_solution_id', '=', 'a.issue_solution_id')
    //         ->leftJoin('00_issue_status as e', 'e.issue_status_id', '=', 'a.issue_status_id')
    //         ->leftJoin('00_issue_category as f', 'f.issue_category_id', '=', 'c.issue_category_id')
    //         ->leftJoin('00_issue_attachment as g', 'g.issue_id', '=', 'a.issue_id')
    //         ->where('a.customer_id', $request->user_id)
    //         ->where('a.order_id', $request->order_id)
    //     ->first();
    //     // dd($q);
    // }

  // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function complaintHistory(Request $request){
        //$complaint = app('App\Http\Controllers\MiddlewareEndpointController')->getComplaintHistory($request);                   
        
        // getComplainHIstory----------------------------------------------------
        if($request->is_agent == 0){
            $complaint = DB::table('00_issue AS a') 
                ->join('00_customer AS b', 'a.customer_id', '=', 'b.customer_id')
                ->join('00_issue_solution AS c', 'a.issue_solution_id', '=', 'c.issue_solution_id')
                ->join('00_issue_status AS d', 'a.issue_status_id', '=', 'd.issue_status_id')
                ->where('a.order_id', $request->order_id)
                ->where('a.customer_id', $request->user_id)
                ->select('a.issue_id', 'a.order_id', 'a.ticketing_num', 'b.customer_name', 'a.issue_date', 'a.issue_solution_id', 'c.issue_solution_name', 'a.issue_status_id', 'd.issue_status_name')
                ->first();
        }else{
             $complaint = DB::table('00_issue AS a') 
                ->join('98_user AS b', 'a.customer_id', '=', 'b.user_id')
                ->join('00_issue_solution AS c', 'a.issue_solution_id', '=', 'c.issue_solution_id')
                ->join('00_issue_status AS d', 'a.issue_status_id', '=', 'd.issue_status_id')
                ->where('a.order_id', $request->order_id)
                ->where('a.customer_id', $request->user_id)
                ->select('a.issue_id', 'a.order_id', 'a.ticketing_num', 'b.customer_name', 'a.issue_date', 'a.issue_solution_id', 'c.issue_solution_name', 'a.issue_status_id', 'd.issue_status_name')
                ->first();  
        }               
        // getComplainHIstory----------------------------------------------------

        if($complaint == null){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found','data'=>[]], 500);
        }else{
            
        $products = array();
        //DISTRIBUTE PRODUCT            
        //$product=app('App\Http\Controllers\MiddlewareEndpointController')->getProductComplaint($complaint['data']['issue_id']);  

        //getProductComplaint----------------------------------------------------------
        $product = DB::table('00_product AS a')
            ->join('00_issue_detail AS b', 'a.prod_id', '=', 'b.prod_id')
            ->join('00_issue AS c', 'b.issue_id', '=', 'c.issue_id')
            ->join('00_issue_category AS d', 'b.issue_category_id', '=', 'd.issue_category_id')
            ->where('c.issue_id', $complaint->issue_id)
            ->select('a.prod_id', 'a.prod_name', 'a.prod_image', 'a.prod_price', 'b.issue_category_id', 'd.issue_category_name', 'b.issue_detail_notes', 'b.quantity')
            ->get();

        // $totalProduct = DB::table('00_product AS a')
        //     ->join('00_issue_detail', 'a.prod_id', '=', 'b.prod_id')
        //     ->join('00_issue AS c', 'b.issue_id', '=', 'c.issue_id')
        //     ->join('00_issue_category AS d', 'b.issue_category_id', '=', 'd.issue_category_id')
        //     ->where('c.issue_id', $complaint->issue_id)
        //     ->selectRaw('count(a.prod_id) AS total')
        //     ->get();

        // $product = array(
        //     "data"=>$product1,
        //     "total"=>$totalProduct->total
        // );     

        //getProductComplaint----------------------------------------------------------

        foreach ($product as $productData) {
            $obj = array(
                "product_id" => $productData->prod_id,
                "product_name"=> $productData->prod_name,
                "product_image"=> asset($productData->prod_image),
                "product_price"=> $productData->prod_price == null ? 0 : $productData->prod_price,
                "complaint_category_id"=> $productData->issue_category_id,
                "complaint_category_name"=> $productData->issue_category_name,
                "complaint_detail"=> $productData->issue_detail_notes == null ? "" : $productData->issue_detail_notes,
                "complaint_quantity"=> $productData->quantity                    
            );                
            array_push($products, $obj);    
        }
                                                                          
            //DISTRIBUTE IMAGE            
            //$attachmentData = app('App\Http\Controllers\MiddlewareEndpointController')->getAttachment($complaint['data']['issue_id']);              
            $attachmentData = DB::table('00_issue_attachment')
                ->whereRaw('issue_id in ('.$complaint->issue_id.')')
                ->select('attachment AS image')
                ->get();

            $image = array();
            foreach ($attachmentData as $attachmentDataList) {
                $obj = array(
                    "image"=>asset($attachmentDataList->image)
                );       
                array_push($image, $obj);
            }

            //komplain chat
            $q = DB::table('00_issue_replies as a')
              ->select('a.issue_replies_id','a.issue_id','a.customer_id','a.user_id','a.response','a.created_date'
              )
            ->leftJoin('00_issue', '00_issue.issue_id', '=', 'a.issue_id')
            ->where('00_issue.order_id', $request->order_id)
            ->get();

            $discuss = array();
            for ($b=0; $b < count($q); $b++) {

                if($q[$b]->customer_id !== null) { 
                    $is_user = true;
                } else { 
                    $is_user = false;
                }
                // dd($is_user);

                //tampilin image
                // $req_midd = (object)array(
                //                 "is_agent" => $request->is_agent,
                //                 "user_id" => $request->user_id
                //             );
                //$profile2 = app('App\Http\Controllers\MiddlewareEndpointController')->getProfile($req_midd);
                if($request->is_agent == 0){
                    $profile2 = DB::table('00_customer')
                    ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_password AS user_password', 'customer_image AS user_image', 'saldo AS saldo')
                    ->where('customer_id', $request->user_id)
                    ->first();
                }else{
                    $profile2 = DB::table('98_user')
                    ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number', 'user_password AS user_password', 'user_image AS user_image', 'saldo AS saldo')
                    ->where('user_id', $request->user_id)
                    ->first();
                }

                if($is_user == true) { 
                    $is_image = $profile2->user_image == null ? null : asset($profile2->user_image);
                } else { 
                    $is_image = null;
                }

                $obj = array(
                    "id_discuss" => $q[$b]->issue_replies_id,
                    "issue_id" => $q[$b]->issue_id,
                    "message" => $q[$b]->response,
                    "is_user" => $is_user,
                    "create_date" => $q[$b]->created_date,
                    "image"=> $is_image
                );
                array_push($discuss, $obj);
            }
           
            //DISTRIBUTE COMPLAINT HEADER
            $complaintHeader = array(
                "complaint_id"=>$complaint->issue_id,
                "order_id"=>$complaint->order_id,
                "order_code"=>$complaint->ticketing_num,
                "complaint_name"=>$complaint->customer_name,
                "complaint_date"=>$complaint->issue_date,
                "complaint_solution_id"=>$complaint->issue_solution_id,
                "complaint_solution_name"=>$complaint->issue_solution_name,
                "complaint_status_id"=>$complaint->issue_status_id,
                "complaint_status_name"=>$complaint->issue_status_name,
                "complaint_product"=>$products,
                "complaint_image"=>$image,
                "discuss" => $discuss
            );         
            return response()->json(['isSuccess' => true, 'message' => 'OK','data' => $complaintHeader], 200);                      
        }       
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function solution(){
        $solution = DB::table('00_issue_solution')
            ->where('active_flag', '1')
            ->select('issue_solution_id', 'issue_solution_name', 'description')
            ->get();             
        return app('App\Http\Controllers\ResponseController')->getResponseData($solution);                
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------


    // ORIGINAL START ---------------------------------------------------------------------
    // public function solution(){
        //     $complaint_solution = ComplaintSolution::where('active_flag',1)
        //             ->get();

        //     // dd($paymentMethod);

        //     $complaint_solution_array = array();

        //     foreach($complaint_solution as $data){
        //         $obj = array(
        //             'issue_solution_id' => $data->issue_solution_id,
        //             'issue_solution_name' => $data->issue_solution_name,
        //             'description' => $data->description
        //         );
        //         array_push($complaint_solution_array, $obj);
        //     }

        //     if(!$complaint_solution){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        //     }else{
        //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $complaint_solution_array], 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

     // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function category(){
        $category = DB::table('00_issue_category')
            ->where('active_flag', '1')
            ->select('issue_category_id', 'issue_category_name', 'description')
            ->get();
        //$category = app('App\Http\Controllers\MiddlewareEndpointController')->complaintCategory();                
        return app('App\Http\Controllers\ResponseController')->getResponseData($category);                
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function category(){
        //     $complaint_category = ComplaintCategory::where('active_flag',1)
        //             ->get();

            // dd($paymentMethod);

        //     $complaint_category_array = array();

        //     foreach($complaint_category as $data){
        //         $obj = array(
        //             'issue_category_id' => $data->issue_category_id,
        //             'issue_category_name' => $data->issue_category_name,
        //             'description' => $data->description
        //         );
        //         array_push($complaint_category_array, $obj);
        //     }

        //     if(!$complaint_category){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        //     }else{
        //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $complaint_category_array], 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------

    public function email($id = null){
        $currency = new Currency();
        $complaint = Complaint::select('00_issue.issue_id',
            '00_issue.issue_status_id',
            '00_issue.ticketing_num',
            '00_issue.customer_id',
            '10_order.order_date',
            DB::raw('0 AS is_agent'),
            '00_issue.order_id',
            '00_issue_solution.issue_solution_id',
            '00_issue_solution.issue_solution_name',
            '00_issue.issue_notes','00_issue.issue_date',
            '00_payment_method.payment_method_name',
            '00_warehouse.warehouse_id',
            '00_warehouse.warehouse_name',
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            '00_address.address_name','00_address.address_detail',
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),
            '00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name',
            '00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',
            '10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response',
            '10_order.destination_address','10_cashback.return_amount',
            '10_order.buyer_user_id')
        ->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')
        ->join('10_order','10_order.order_id','=','00_issue.order_id')
        ->where('00_issue.issue_id', $id)
        ->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')
        ->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')
        ->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')
        ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
        ->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
        ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
        ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
        ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
        ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')
        ->first();
        $complained_products = Complaint::select('00_product.prod_id', 
            '00_product.prod_name', 
            '00_product.prod_image', 
            '00_uom.uom_name', 
            '10_order_detail.price', 
            '10_order_detail.quantity AS purchased_quantity', 
            '00_issue_detail.quantity AS complained_quantity', 
            '10_order_detail.uom_id', 
            '10_order_detail.uom_value', 
            '10_order_detail.sku_status', 
            '10_order_detail.promo_value', 
            '10_order_detail.dim_length', 
            '10_order_detail.dim_width', 
            '10_order_detail.dim_height', 
            '10_order_detail.warehouse_id', 
            '10_order_detail.is_taxable_product', 
            '10_order_detail.tax_value',
            '00_issue_detail.issue_detail_notes',
            '10_order_detail.promo_value',
            '00_issue_detail.issue_category_id',
            '00_issue_category.issue_category_name')
        ->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')
        ->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')
        ->join('10_order','10_order.order_id','=','00_issue.order_id')
        ->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')
        ->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')
        ->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')
        ->where('00_issue.issue_id',$id)
        ->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')
        ->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        Mail::send('complaint_email',$data, function ($message) use ($complaint)
            {
                $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
                $message->to($complaint->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        // return view('complaint_email', $data);

        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Keluhan Anda sedang diproses (".$complaint->order_code.")";
        $message = "Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak dengan kode pesanan ".$complaint->order_code.", kami ingin menginformasikan bahwa keluhan Anda akan segera diproses oleh pihak YukMarket! Mohon menunggu hingga kabar berikutnya.";
        $type = "complaint";
        if($complaint->fcm_token != null){
            $parameter = array(
                "to" => $complaint->fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $complaint->is_agent == 0 ? $complaint->buyer_user_id : null,
            "user_id" => $complaint->is_agent == 1 ? $complaint->buyer_user_id : null,
            "issue_id" => $id,
            "order_id" => $complaint->order_id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
    }

    public function addDiscuss(Request $request)
    {
        $add = DB::table('00_issue_replies')
        ->insert([
            'issue_id' => $request->issue_id,
            'customer_id' => $request->user_id,
            'response' => $request->message,
            'is_read' => 0,
            'active_flag' => 1,
            'created_by' => $request->user_id,
            'created_date' => date('Y-m-d H:i:s')
        ]);

        if(!$add){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'Discuss successfully added'], 200);
        }


    }
}
