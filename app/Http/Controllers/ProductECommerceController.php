<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProductECommerceController extends Controller
{
    public function checkProduct($id,$ecomId){
    	$check =DB::table('00_product_ecommerce')
    	->select('00_product.prod_id','00_product.prod_name','00_product.prod_desc','00_product.prod_price','00_product.stock','00_product_ecommerce.prod_ecom_id','00_product.uom_id','00_product.uom_value','00_product.product_sku_id','00_product.stockable_flag','00_product.parent_prod_id')
        ->join('00_product','00_product.prod_id','=','00_product_ecommerce.prod_id')
    	->where([
    		['00_product_ecommerce.prod_ecom_code',$id],
    		['00_product_ecommerce.ecommerce_id',$ecomId]
    	])->first();    	
    	return $check;
    }
    public function addProduct($request){
    	DB::table('00_product_ecommerce')
    	->insert($request);
    }
    public function checkProductEcommerce($id,$ecomId){
        return DB::table('00_product_ecommerce')
        ->select('prod_ecom_id','prod_ecom_name')
        ->where([
            ['prod_ecom_code',$id],
            ['ecommerce_id',$ecomId]
        ])->first();        
    }
    public function getParentProduct($prodEcomId){        
        $parent=DB::table('00_product_ecommerce')
        ->select('prod_id')
        ->where('prod_ecom_code',$prodEcomId)
        ->first();
        return $parent;
    }
    public function deleteProductTokped($id){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $prodEcomCode=DB::table('00_product_ecommerce')
        ->select('prod_ecom_code')
        ->where('prod_id',$id)
        ->first();
        $shop=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokped_shop');
        $org=app('App\Http\Controllers\OrganizationController')->organizationByName($shop->global_parameter_value);
        if(!$prodEcomCode){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found', 'data'=>[]], 500);
        }else{
            $url = config('global.tokopedia_integrator_url').'/deleteProduct?productId='.$prodEcomCode->prod_ecom_code.'&shopId='.$org->organization_code;
            // dd($url);
            $delete=Http::withToken($token)->timeout(3)->retry(2, 100)->post($url);
            // dd($delete['data']);
            $this->deleteProductEcommerce($id);
            $parentProdEcomCode=$this->checkProductEcommerceVarian($id);
            if($parentProdEcomCode){
                $this->deleteVarianProductEcommerce($parentProdEcomCode->prod_ecom_code);    
            }
            return response()->json(['isSuccess' => true, 'message' => 'submited', 'data'=>$delete['data']], 200);
        }
    }
    public function deleteProductEcommerce($prodId){
        DB::table('00_product_ecommerce')
        ->where('prod_id',$prodId)
        ->delete();
    }
    public function checkProductEcommerceVarian($prodId){
        $varian=DB::table('00_product_ecommerce')
        ->where('prod_id',$prodId)
        ->select('prod_ecom_code')
        ->first();
        return $varian;
    }
    public function deleteVarianProductEcommerce($prodEcomCodeParent){
        DB::table('00_product_ecommerce')
        ->where('parent_product_id',$prodEcomCodeParent)
        ->delete();
    }
    public function getProductEcom($prodId){
        $prod=DB::table('00_product_ecommerce')
        ->where('prod_id',$prodId)
        ->orderBy('prod_ecom_id','desc')
        ->select('prod_ecom_code')->first();
        return $prod->prod_ecom_code;
    }
    public function inactive($id){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $shop=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
        $prods=DB::table('00_product')
        ->where('prod_id',$id)
        ->orWhere('variant_id',$id)
        ->select('prod_id','prod_name')
        ->get();
        $status=array();
        for($i=0;$i<count($prods);$i++){
            $prodEcom=DB::table('00_product_ecommerce')
            ->where('prod_id',$prods[$i]->prod_id)
            ->select('prod_ecom_code')
            ->orderBy('prod_ecom_id','desc')
            ->first();
            $url=config('global.tokopedia_integrator_url').'/setInactiveProduct?productId='.$prodEcom->prod_ecom_code.'&shopId='.$shop->organization_code;
            try {
                $req=Http::withToken($token)->timeout(10)->retry(2, 2000)->post($url)->json();
                if($req['data']['failed_rows']>0){
                    $msg=[
                        'status'=>'prod '.$prods[$i]->prod_name.' inactive (failed)'
                    ];
                }else{
                    $msg=[
                        'status'=>'prod '.$prods[$i]->prod_name.' inactive (successful)'
                    ];
                }
            } catch (ConnectionException $e) {
                echo "\nurl: ".$url." TIMEOUT";
                $msg=[
                    'status'=>'prod '.$prods[$i]->prod_name.' inactive (failed)'
                ];
                return null;
            }
            array_push($status, $msg);
            sleep(2);
        }
        return response()->json(['isSuccess' => 'OK', 'message' => '', 'data'=>$status], 200);
    }
    public function active($id){
        $token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
        $shop=app('App\Http\Controllers\IntegrationECommerceController')->getShopIdTokped();
        $prods=DB::table('00_product')
        ->where('prod_id',$id)
        ->orWhere('variant_id',$id)
        ->select('prod_id','prod_name')
        ->get();
        $status=array();
        for($i=0;$i<count($prods);$i++){
            $prodEcom=DB::table('00_product_ecommerce')
            ->where('prod_id',$prods[$i]->prod_id)
            ->select('prod_ecom_code')
            ->orderBy('prod_ecom_id','desc')
            ->first();
            $url=config('global.tokopedia_integrator_url').'/setActiveProduct?productId='.$prodEcom->prod_ecom_code.'&shopId='.$shop->organization_code;
            try {
                $req=Http::withToken($token)->timeout(10)->retry(2, 2000)->post($url)->json();
                if($req['data']['failed_rows']>0){
                    $msg=[
                        'status'=>'prod '.$prods[$i]->prod_name.' active (failed)'
                    ];
                }else{
                    $msg=[
                        'status'=>'prod '.$prods[$i]->prod_name.' active (successful)'
                    ];
                }
            } catch (ConnectionException $e) {
                echo "\nurl: ".$url." TIMEOUT";
                $msg=[
                    'status'=>'prod '.$prods[$i]->prod_name.' active (failed)'
                ];
                return null;
            }
            array_push($status, $msg);
            sleep(2);
        }
        return response()->json(['isSuccess' => 'OK', 'message' => '', 'data'=>$status], 200);
    }
}
