<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HistoryOrderController extends Controller
{
    public function addHistoryOrder($data){
        DB::table('10_order_history')
        ->insert($data);
        echo "\n".'| tambah data history order |';
    }
    public function generatePreviousHistory($data){
    	$history6=DB::table('10_order_history')
    	->select('order_history_id')
    	->where([
    		['order_id',$data['order_id']],
    		['order_status_id',6]
    	])
    	->first();
    	if(!$history6){//--------------------------jika history status 6 belum ada
    		$historyOrder=array(
	            'order_id'=>$data['order_id'],
	            'order_status_id'=>6
	        );
	        $this->addHistoryOrder($historyOrder);
    	}
    }
}
