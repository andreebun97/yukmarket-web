<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutUsController extends Controller
{
    public function getAboutUs(){
    	$aboutUs=DB::table('98_aboutus')
    	->select('title','body','image')
    	->first();

    	if (!$aboutUs) {
            return response()->json(['isSuccess' => true, 'message' => 'Data not found', 'data'=>[]], 200);
        } else {
        	$pathImg='/img/uploads/aboutus/'.$aboutUs->image;
        	$response=array(
	    		'image'=>$aboutUs->image == null ? null : asset($pathImg),
	    		'about'=>$aboutUs->body,
	    		'name'=>$aboutUs->title
	    	);
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $response], 200);
        }
    }
}
