<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeaderController extends Controller
{
    public function getHeader(){
    	$header=DB::table('98_color_and_logo')
    	->select('color', 'secondary_color', 'logo', 'logo_page')
    	->first();

    	if (!$header) {
            return response()->json(['isSuccess' => true, 'message' => 'Data not found', 'data'=>[]], 200);
        } else {
        	$response=array(
	    		'logo'=>$header->logo == null ? null : asset('img/uploads/logo/'.$header->logo),
	    		'logo_page'=>$header->logo_page == null ? null : asset('img/uploads/logo/'.$header->logo_page),
	    		'primary_color'=>$header->color,
	    		'secondary_color'=>$header->secondary_color
	    	);
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $response], 200);
        }
    }
}
