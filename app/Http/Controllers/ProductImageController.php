<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductImageController extends Controller
{
    public function addProductImageEcommerce($data){
    	DB::table('00_product_image_ecommerce')
    	->insert([$data]);
    }
}
