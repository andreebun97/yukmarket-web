<?php

namespace App\Http\Controllers;

use App\AuthComp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Currency;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Config;
use Exception;
use App\Cart;

class CartController extends Controller
{
    protected $currency;

    /*public function getCart(Request $request)
    {
        $q = DB::table('10_cart AS a')
        ->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
        ->leftJoin('98_user AS c', 'c.user_id', '=', 'b.created_by')
        ->leftJoin('00_uom as d', 'd.uom_id', '=', 'b.uom_id')
        ->select('a.cart_id', 'b.is_taxable_product', 
            'b.tax_value', 'a.buyer_user_id as user_id', 
            'b.prod_id AS product_id', 'b.stock', 
            'b.prod_name AS product_name', 
            'b.prod_price AS product_price', 
            'b.uom_value', 'd.uom_name',
            'b.product_sku_id',
            'c.user_id as supplier_id', 
            'c.user_name AS supplier_name', 
            'b.prod_image AS product_image', 
            'a.quantity AS qty', 
            DB::raw('(b.prod_price * a.quantity) AS subtotal'))
        ->where([
			['a.buyer_user_id', $request->user_id],
			['a.active_flag', 1]
		])
        ->get();

        $cart_array = array();
        for ($b=0; $b < count($q); $b++) { 

             $varian = DB::table('00_product')
                ->select('00_product.prod_id as product_id', 
                    '00_product.prod_name as varian_name', 
                    '00_product.prod_price as varian_price',
                    '00_product.stock as varian_stock',
                    '00_product.uom_value as variant_weight_value',
                    '00_uom.uom_name as varian_weight_uom',
                    '00_vouchers.voucher_id as voucher_id',
                    '00_vouchers.amount as varian_voucher_amount',
                    '00_vouchers.is_fixed as varian_voucher_is_fixed',
                    DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                )
                ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                ->where('00_product.active_flag',1)
                    ->whereNotNull('00_product.product_sku_id')
                    ->where('00_product.product_sku_id', $q[$b]->product_sku_id)
                    ->where(function($query) use($request){
                        $query->whereNull('00_product_voucher.voucher_id')
                        ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                    })
                ->get();

              //set header is_varian, price, stok, dan diskon         
           if(isset($varian) && count($varian) > 0) { 
                $is_varian = true;
           } else { 
                $is_varian = false;
           }

            $obj = array(
                "cart_id" => $q[$b]->cart_id,
                'product_stock' => $q[$b]->stock,
                'is_taxable_product' => $q[$b]->is_taxable_product,
                'tax_value' => number_format($q[$b]->tax_value,2),
                'user_id' => $q[$b]->user_id,
                'product_id' => $q[$b]->product_id,
                'product_name' => $is_varian == true ? $q[$b]->product_name." ".$q[$b]->uom_value." ".$q[$b]->uom_name : $q[$b]->product_name,
                'product_price' => $q[$b]->product_price,
                'product_image' => asset($q[$b]->product_image),
                'supplier_id' => $q[$b]->supplier_id,
                'supplier_name' => "Indocyber",//$q[$b]->supplier_name,
                'qty' => $q[$b]->qty,
                'subtotal' => $q[$b]->subtotal,

            );
            array_push($cart_array, $obj);
        }

        if(count($q) <= 0){
            return response()->json(['isSuccess' => false, 'message' => 'Cart is Empty', 'data' => $q], 200);
        }else if(count($q) > 0){
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $cart_array], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
    }*/

    public function getCart(Request $request)
    {
        $query = DB::table('10_cart AS a')
            ->Join('00_warehouse AS b', 'b.warehouse_id', '=', 'a.warehouse_id')
            ->select(
                'a.buyer_user_id',
                'a.warehouse_id',
                'b.warehouse_name'
            ) ->where([
                ['a.buyer_user_id', $request->user_id],
                ['a.active_flag', 1]
            ])
            ->groupBy('a.warehouse_id')->get();
        $check_price_type = null;
        if ($request->input('is_agent') > 0) :
            $check_price_type = DB::table('98_user as user')
                            ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
                            ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
                            ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
                            ->where('user.user_id','=',$request->input('user_id'))
                            ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
                            ->first();
        endif;
        $warehouse_array = array();
        for ($i=0; $i < count($query); $i++) {
            $q = DB::table('10_cart AS a')
                ->Join('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
                ->leftJoin('98_user AS c', 'c.user_id', '=', 'b.created_by')
                ->leftJoin('00_uom as d', 'd.uom_id', '=', 'b.uom_id')
                ->leftJoin('00_warehouse as e', 'e.warehouse_id', '=', 'a.warehouse_id')
                ->leftJoin('00_inventory as f', function($join){
                                         $join->on('b.prod_id', '=', 'f.prod_id');
                                         $join->on('f.warehouse_id', '=', 'e.warehouse_id');
                                         $join->on('f.organization_id', '=', DB::raw("'1'"));
                                     })
                ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', 'b.prod_id')
                // ->leftJoin('00_inventory as f', 'b.prod_id', '=', 'f.prod_id')
                ->leftJoin('00_product_voucher as g','g.prod_id','=','b.prod_id')
                ->leftJoin('00_vouchers as h','h.voucher_id','=','g.voucher_id')
                ->select('a.cart_id', 'b.is_taxable_product', 
                    'b.tax_value', 'a.buyer_user_id as user_id', 
                    'b.prod_id AS product_id', 'f.stock', 
                    'b.prod_name AS product_name', 
                    'b.prod_price AS product_price', 
                    'b.uom_value', 'd.uom_name',
                    'b.bruto',
                    'c.user_id as supplier_id', 
                    'c.user_name AS supplier_name', 
                    'b.prod_image AS product_image', 
                    'a.quantity AS qty', 
                    DB::raw('(b.prod_price * a.quantity) AS subtotal'),
                    'h.voucher_id',
                    'h.amount as voucher_amount',
                    'h.is_fixed as voucher_is_fixed',
                    'h.max_value_price as voucher_max_value_price',
                    'h.min_price_requirement as voucher_min_price',
                    '00_product_price_type.price_type_id',
                    '00_product_price_type.discount_type',
                    '00_product_price_type.rate',
                    DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN h.starts_at AND h.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < h.starts_at THEN 2 ELSE 1 END) AS expired')
                )
                ->where([
                    ['a.buyer_user_id', $request->user_id],
                    ['a.warehouse_id', $query[$i]->warehouse_id],
                    ['a.active_flag', 1]
                ])
                ->whereNotNull('a.warehouse_id')
                ->get();

            $cart_array = array();
            $warehouse_subtotal = 0;
            $warehouse_weight = 0;
            for ($b=0; $b < count($q); $b++) { 
                //OLD 
                /*
                $total_promo_value = 0;
                if($q[$b]->expired == 0 && $q[$b]->voucher_id != null){
                    if($q[$b]->voucher_is_fixed == 'Y'){
                        $total_promo_value = $q[$b]->voucher_amount;
                    }else{
                        $total_promo_value = ($q[$b]->product_price * $q[$b]->voucher_amount/100);
                    }

                }
                $product_price_after_discount = $q[$b]->product_price - $total_promo_value;
                if ($q[$b]->voucher_id == null) {
                    $product_discount = "0";
                    $product_discount_percentage = "0";
                } else {
                    if ($q[$b]->voucher_is_fixed == 'Y') {
                        $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                        $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                    } else {
                        $product_discount = $q[$b]->voucher_amount.'%';
                        $product_discount_percentage = $q[$b]->voucher_amount;
                        $voucer_isFix = "N";
                    }
                }
                */
                //OLD
                $check_price_wh = DB::table('00_product_warehouse')
                            ->where('prod_id','=',$q[$b]->product_id)
                            ->where('warehouse_id','=',$query[$i]->warehouse_id)
                            ->first();

                $q[$b]->product_price = ($check_price_wh->prod_price == null) ? $q[$b]->product_price : $check_price_wh->prod_price;
                $total_promo_value = 0;
                if($q[$b]->price_type_id != null){
                    if($q[$b]->discount_type == 2){
                        $total_promo_value = $q[$b]->rate ;
                    }else{
                        $total_promo_value = ($q[$b]->product_price * $q[$b]->rate/100);
                    }
                } else {
                    if ($check_price_type) {
                        if($check_price_type->discount_type == 2){
                            $total_promo_value = $check_price_type->rate ;
                        }else{
                            $total_promo_value = ($q[$b]->product_price * $check_price_type->rate/100);
                        }
                    }
                }

                $product_price_after_discount = $q[$b]->product_price - $total_promo_value;
                if ($q[$b]->price_type_id == null) {
                    if ($check_price_type) {
                        if ($check_price_type->discount_type == 2) {
                            $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                            $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                        } else {
                            $product_discount = $check_price_type->rate.'%';
                            $product_discount_percentage = $check_price_type->rate;
                            $voucer_isFix = "N";
                        }
                    } else {
                        $product_discount = "0";
                        $product_discount_percentage = "0";   
                    }
                } else {
                    if ($q[$b]->discount_type == 2) {
                        $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                        $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                    } else {
                        $product_discount = $q[$b]->rate.'%';
                        $product_discount_percentage = $q[$b]->rate;
                        $voucer_isFix = "N";
                    }
                }

                // $product_price_after_discount = $q[$b]->product_price - $total_promo_value;
                // if ($q[$b]->voucher_id == null) {
                //     $product_discount = "0";
                //     $product_discount_percentage = "0";
                // } else {
                //     if ($q[$b]->voucher_is_fixed == 'Y') {
                //         $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                //         $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                //     } else {
                //         $product_discount = $q[$b]->voucher_amount.'%';
                //         $product_discount_percentage = $q[$b]->voucher_amount;
                //         $voucer_isFix = "N";
                //     }
                // }

                $subtotal = $product_price_after_discount * $q[$b]->qty;
                $weight = $q[$b]->bruto * $q[$b]->qty;
                $warehouse_subtotal += $subtotal;
                $warehouse_weight += $weight;
                $obj = array(
                    "cart_id" => $q[$b]->cart_id,
                    "product_id" => $q[$b]->product_id,
                    "product_name" => $q[$b]->product_name." ".($q[$b]->uom_value+0)." ".$q[$b]->uom_name,
                    "product_discount" => (string)$product_discount,
                    "product_discount_percentage" => (string)$product_discount_percentage,
                    "product_price_before_discount" => $q[$b]->product_price,
                    "product_price_after_discount" => $product_price_after_discount,
                    "product_stock" => round($q[$b]->stock),
                    "product_uom_value" => (string)($q[$b]->uom_value+0),
                    "product_uom_name" => $q[$b]->uom_name,
                    "product_pack_uom_value" => (string)($q[$b]->bruto+0),
                    "product_pack_uom_name" => "Kg",
                    "product_image" => asset($q[$b]->product_image),
                    "qty" => $q[$b]->qty,
                    "subtotal" => $subtotal,
                );
                array_push($cart_array, $obj);
            }

            $obj_warehouse = array(
                'warehouse_id' => $query[$i]->warehouse_id,
                'warehouse_name' => $query[$i]->warehouse_name,
                'warehouse_subtotal' => $warehouse_subtotal,
                'warehouse_weight' => number_format($warehouse_weight, 2, '.', ''),
                'cart' => $cart_array,
            );
            array_push($warehouse_array, $obj_warehouse);
        }

        

        if(count($warehouse_array) <= 0){
            return response()->json(['isSuccess' => true, 'message' => 'Cart is Empty', 'data' => $warehouse_array], 200);
        }else if(count($warehouse_array) > 0){
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $warehouse_array], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
    }

    public function getQtyProduct(Request $request)
    {
        $q = DB::table('10_cart')
        ->select('prod_id', 'quantity')
        ->where([
            ['buyer_user_id', $request->user_id],
            ['prod_id', $request->product_id]
        ])
        ->get();
        
        if(count($q) <= 0){
            return response()->json(['isSuccess' => false, 'message' => 'Product not Found', 'data' => []], 200);
        }else if(count($q) > 0){
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'prod_qty' => $q[0]->quantity], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
    }

    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        // $this->middleware('check-token');
    }

    /*public function getTransaction(Request $request){
        $column_array = ['10_order.order_id','10_order.order_code','10_order.order_date','10_order.buyer_user_id','00_customer.customer_name AS buyer_user_name','00_customer.customer_phone_number AS buyer_user_phone_number','00_customer.customer_email AS buyer_user_email','10_order.payment_method_id','00_payment_method.payment_method_name','10_order.destination_address AS destination_address_id','00_address.address_name AS destination_address_name','10_order.shipment_method_id','00_shipment_method.shipment_method_name','00_order_status.order_status_name','10_order.order_status_id','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.admin_fee','10_order.admin_fee_percentage','10_order.voucher_id','10_order.voucher_amount','10_order.is_fixed','00_vouchers.voucher_code','10_order.max_price','10_order.active_flag AS order_active_status','00_address.address_detail','00_address.kabupaten_kota_id','00_kabupaten_kota.kabupaten_kota_name','00_address.kecamatan_id','00_kecamatan.kecamatan_name','00_address.kelurahan_desa_id','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos', '10_order.shipment_price', DB::raw('(SELECT SUM(10_order_detail.quantity * 10_order_detail.price) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS `total_order`')];
        $transaction = DB::table("10_order")->select($column_array)->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->distinct()->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->where('10_order.buyer_user_id', $request->input('user_id'))->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id');
        $start_full_date = "";
        $start_date = "";
        $start_month = "";
        $start_year = "";
        $end_full_date = "";
        $end_date = "";
        $end_month = "";
        $end_year = "";
        if($request->input('dateFrom') != ""){
            $start_full_date = $request->input('dateFrom');
            $start_date = explode('-',$request->input('dateFrom'))[0];
            $start_month = explode('-',$request->input('dateFrom'))[1];
            $start_year = explode('-',$request->input('dateFrom'))[2];
            $start_full_date = $start_year . '-' . $start_month . '-' . $start_date;
            
            $transaction->where(DB::raw('CAST(10_order.order_date AS DATE)'),'>=',DB::raw('CAST("'.$start_full_date.'" AS DATE)'));
        }
        if($request->input('dateTo') != ""){
            $end_full_date = $request->input('dateTo');
            $end_date = explode('-',$request->input('dateTo'))[0];
            $end_month = explode('-',$request->input('dateTo'))[1];
            $end_year = explode('-',$request->input('dateTo'))[2];
            $end_full_date = $end_year . '-' . $end_month . '-' . $end_date;

            $transaction->where(DB::raw('CAST(10_order.order_date AS DATE)'),'<=',DB::raw('CAST("'.$end_full_date.'" AS DATE)'));
        }
        // echo $request->input('dateFrom').'<br/>';
        // echo $request->input('dateTo');
        // exit;
        if($request->input('order_id') != ""){
            $transaction->where('10_order.order_id', $request->input('order_id'));
        }
        if($request->input('sort_by') == 2){
            $transaction->orderBy('10_order.order_date','DESC');
        }else{
            $transaction->orderBy('10_order.order_date','ASC');
        }
        $transaction = $transaction->get();
        $transaction_array = array();
        for ($b=0; $b < count($transaction); $b++) {
            $total_order = $transaction[$b]->total_order == null ? 0 : $transaction[$b]->total_order;
            $delivery_fee = $transaction[$b]->shipment_price;
            $max_provided_price = 0;
            $voucher_amount = 0;
            $admin_fee = 0;
            $admin_fee_percentage = 0;
            $grand_total = $transaction[$b]->total_order;
            $discount = 0;

            if($transaction[$b]->voucher_id != null){
                $max_provided_price = $transaction[$b]->max_price;
                $voucher_amount = $transaction[$b]->voucher_amount;
                if($transaction[$b]->is_fixed == 'Y'){
                    $discount = $transaction[$b]->voucher_amount;
                }else{
                    $discount = $transaction[$b]->total_order * ($transaction[$b]->voucher_amount / 100);
                    if($discount > $max_provided_price){
                        $discount = $max_provided_price;
                    }
                }
                $grand_total = $transaction[$b]->total_order - $discount;
            }

            $grand_total = $grand_total + $delivery_fee;

            $all_admin_fee = 0;

            if($transaction[$b]->admin_fee != null){
                $admin_fee = $transaction[$b]->admin_fee;
                $all_admin_fee = $transaction[$b]->admin_fee;
            }
            
            if($transaction[$b]->admin_fee_percentage != null){
                $admin_fee_percentage = $transaction[$b]->admin_fee_percentage / 100;
                $all_admin_fee = ($grand_total + $admin_fee_percentage);
            }

            $grand_total = $grand_total + $all_admin_fee;
            
            $obj = array(
                "order_id" => $transaction[$b]->order_id,
                "order_code" => $transaction[$b]->order_code,
                "order_date" => $transaction[$b]->order_date,
                "buyer_user_id" => $transaction[$b]->buyer_user_id,
                "buyer_user_name" => $transaction[$b]->buyer_user_name,
                "buyer_user_email" => $transaction[$b]->buyer_user_email,
                "buyer_user_phone_number" => $transaction[$b]->buyer_user_phone_number,
                "payment_method_id" => $transaction[$b]->payment_method_id,
                "payment_method_name" => $transaction[$b]->payment_method_name,
                'payment_status_id' => $transaction[$b]->invoice_status_id,
                'payment_status' => $transaction[$b]->invoice_status_name,
                "destination_address" => array(
                    "status" => $transaction[$b]->destination_address_name == null ? "" : $transaction[$b]->destination_address_name,
                    "detail" => $transaction[$b]->address_detail == null ? "" : $transaction[$b]->address_detail,
                    "district" => $transaction[$b]->kabupaten_kota_name == null ? "" : $transaction[$b]->kabupaten_kota_name,
                    "regency" => $transaction[$b]->kecamatan_name == null ? "" : $transaction[$b]->kecamatan_name,
                    "village" => $transaction[$b]->kelurahan_desa_name == null ? "" : $transaction[$b]->kelurahan_desa_name,
                    "postal_code" => $transaction[$b]->kode_pos == null ? 0 : $transaction[$b]->kode_pos
                ),
                "shipment_method_id" => $transaction[$b]->shipment_method_id,
                "shipment_method_name" => $transaction[$b]->shipment_method_name,
                "order_status_id" => $transaction[$b]->order_status_id,
                "order_status_name" => $transaction[$b]->order_status_name,
                "order_active_status" => $transaction[$b]->order_active_status,
                "total_price" => $this->currency->convertToCurrency($transaction[$b]->total_order),
                "voucher" => array(
                    "code" => ($transaction[$b]->voucher_id != null ? $transaction[$b]->voucher_code : ""),
                    "amount" => $transaction[$b]->voucher_id != null ? ($transaction[$b]->is_fixed == 'Y' ? $this->currency->convertToCurrency($voucher_amount) : (string)($voucher_amount/100)) : "",
                    "max_provided_price" => $transaction[$b]->voucher_id != null ? $this->currency->convertToCurrency($max_provided_price) : "",
                    "is_fixed" => $transaction[$b]->voucher_id != null ? $transaction[$b]->is_fixed : ""
                ),
                "delivery_fee" => $this->currency->convertToCurrency($delivery_fee),
                "discount" => $this->currency->convertToCurrency($discount) == "" ? "0" : $this->currency->convertToCurrency($discount),
                "admin_fee" => $this->currency->convertToCurrency($admin_fee) == "" ? "0" : $this->currency->convertToCurrency($admin_fee),
                "admin_fee_percentage" => $admin_fee_percentage,
                "total_order" => ($this->currency->convertToCurrency($grand_total) == "" ? "0" : $this->currency->convertToCurrency($grand_total))
            );
            array_push($transaction_array, $obj);
        }
        if($request->input('order_id') != ""){
            $transaction_array = $transaction_array[0];
        }

        if(!$transaction){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $transaction_array], 200);
        }
    }*/

    public function getPurchasedProduct(Request $request){
        //kalau dia non agent
        if($request->get('is_agent') == 0){
            $column_array = ['10_order.order_id',
            '10_order.order_code',
            '10_order.order_date',
            '10_order.buyer_user_id',
            '00_customer.customer_name AS buyer_user_name',
            '00_customer.customer_email AS buyer_user_email',
            '10_order.payment_method_id',
            '00_payment_method.payment_method_name',
            '10_order.destination_address AS destination_address_id',
            '00_address.address_name AS destination_address_name',
            '10_order.shipment_method_id',
            '00_shipment_method.shipment_method_name',
            '00_order_status.order_status_name',
            '10_order.order_status_id',
            '10_order.active_flag AS order_active_status'
            ,DB::raw('(SELECT SUM(10_order_detail.quantity * 10_order_detail.price) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS `total_order`'),
            '10_order_detail.prod_id AS product_id','00_product.prod_name AS product_name',
            '00_product.prod_desc AS product_description',
            '00_product.prod_image AS product_image',
            '00_product.product_sku_id',
            '10_order_detail.quantity AS purchased_quantity',
            '10_order_detail.price AS purchased_price',
            '00_uom.uom_id','00_uom.is_decimal',
            '00_uom.uom_name',
            '00_product.uom_value'];
            
            $products = DB::table("10_order")
            ->select($column_array)
            ->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')
            ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
            ->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')
            ->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
            ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
            ->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->where('10_order.buyer_user_id', $request->input('user_id'));
            if($request->input('order_id') != ""){
                $products->where('10_order.order_id', $request->input('order_id'));
            }
            $products = $products->get();
            $product_array = array();
            for ($b=0; $b < count($products); $b++) {

                 $varian = DB::table('00_product')
                    ->select('00_product.prod_id as product_id', 
                        '00_product.prod_name as varian_name', 
                        '00_product.prod_price as varian_price',
                        '00_product.stock as varian_stock',
                        '00_product.uom_value as variant_weight_value',
                        '00_uom.uom_name as varian_weight_uom',
                        '00_vouchers.voucher_id as voucher_id',
                        '00_vouchers.amount as varian_voucher_amount',
                        '00_vouchers.is_fixed as varian_voucher_is_fixed',
                        DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                    )
                    ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                    ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                    ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                    ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                    ->where('00_product.active_flag',1)
                        ->whereNotNull('00_product.product_sku_id')
                        ->where('00_product.product_sku_id', $products[$b]->product_sku_id)
                        ->where(function($query) use($request){
                            $query->whereNull('00_product_voucher.voucher_id')
                            ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                        })
                    ->get();

                  //set header is_varian, price, stok, dan diskon         
               if(isset($varian) && count($varian) > 0) { 
                    $is_varian = true;
               } else { 
                    $is_varian = false;
               }


                $total_order = $products[$b]->total_order == null ? 0 : $products[$b]->total_order;
                $delivery_fee = 10000;
                $obj = array(
                    "product_id" => $products[$b]->product_id,
                    "product_name" => $is_varian == true ? $products[$b]->product_name." ".$products[$b]->uom_value." ".$products[$b]->uom_name : $products[$b]->product_name,
                    "product_image" => $products[$b]->product_image == null ? null : asset($products[$b]->product_image),
                    "purchased_quantity" => $products[$b]->purchased_quantity,
                    "purchased_price" => $this->currency->convertToCurrency($products[$b]->purchased_price),
                    "total_purchased_per_item" => $this->currency->convertToCurrency(($products[$b]->purchased_quantity * $products[$b]->purchased_price)),
                    "product_uom_id" => $products[$b]->uom_id,
                    "product_weight_ons" => $products[$b]->uom_name,
                    "product_weight" => ($products[$b]->is_decimal == 0 ? (string)ROUND($products[$b]->uom_value,0) : (Str::endsWith($products[$b]->uom_value,'.00') == true ? (string)ROUND($products[$b]->uom_value,0) : (string)$products[$b]->uom_value)),
                    "total_order" => $this->currency->convertToCurrency(($products[$b]->purchased_quantity * $products[$b]->purchased_price))
                );
                array_push($product_array, $obj);
            }
            //kalau dia agant
        }else{
            $column_array = ['10_order.order_id',
            '10_order.order_code',
            '10_order.order_date',
            '10_order.buyer_user_id',
            '98_user.user_name AS buyer_user_name',
            '98_user.user_phone_number AS buyer_user_phone_number',
            '98_user.user_email AS buyer_user_email',
            '10_order.payment_method_id',
            '00_payment_method.payment_method_name',
            '10_order.destination_address AS destination_address_id',
            '00_address.address_name AS destination_address_name',
            '10_order.shipment_method_id',
            '00_shipment_method.shipment_method_name',
            '00_order_status.order_status_name',
            '10_order.order_status_id',
            '10_order.active_flag AS order_active_status'
            ,DB::raw('(SELECT SUM(10_order_detail.quantity * 10_order_detail.price) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS `total_order`'),
            '10_order_detail.prod_id AS product_id','00_product.prod_name AS product_name',
            '00_product.prod_desc AS product_description',
            '00_product.prod_image AS product_image',
            '00_product.product_sku_id',
            '10_order_detail.quantity AS purchased_quantity',
            '10_order_detail.price AS purchased_price',
            '00_uom.uom_id','00_uom.is_decimal',
            '00_uom.uom_name',
            '00_product.uom_value'];
            
            $products = DB::table("10_order")
            ->select($column_array)
            ->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')
            ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
            ->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')
            ->leftJoin('98_user','98_user.user_id','=','10_order.buyer_user_id')
            ->leftJoin('00_organization', '98_user.organization_id', '=', '00_organization.organization_id')
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
            ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
            ->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->where('00_organization.organization_type_id', 2)
            ->where('10_order.is_agent', 1)
            ->where('10_order.buyer_user_id', $request->input('user_id'));
            if($request->input('order_id') != ""){
                $products->where('10_order.order_id', $request->input('order_id'));
            }
            $products = $products->get();
            // dd($products);
            $product_array = array();
            for ($b=0; $b < count($products); $b++) {

                 $varian = DB::table('00_product')
                    ->select('00_product.prod_id as product_id', 
                        '00_product.prod_name as varian_name', 
                        '00_product.prod_price as varian_price',
                        '00_product.stock as varian_stock',
                        '00_product.uom_value as variant_weight_value',
                        '00_uom.uom_name as varian_weight_uom',
                        '00_vouchers.voucher_id as voucher_id',
                        '00_vouchers.amount as varian_voucher_amount',
                        '00_vouchers.is_fixed as varian_voucher_is_fixed',
                        DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS varian_voucher_expired')
                    )
                    ->leftJoin('00_product_sku', '00_product.product_sku_id', '=', '00_product_sku.product_sku_id')
                    ->leftJoin('00_uom', '00_uom.uom_id', '=', '00_product.uom_id')
                    ->leftJoin('00_product_voucher','00_product_voucher.prod_id','=','00_product.prod_id')
                    ->leftJoin('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')
                    ->where('00_product.active_flag',1)
                        ->whereNotNull('00_product.product_sku_id')
                        ->where('00_product.product_sku_id', $products[$b]->product_sku_id)
                        ->where(function($query) use($request){
                            $query->whereNull('00_product_voucher.voucher_id')
                            ->orWhereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')]);
                        })
                    ->get();

                  //set header is_varian, price, stok, dan diskon         
               if(isset($varian) && count($varian) > 0) { 
                    $is_varian = true;
               } else { 
                    $is_varian = false;
               }


                $total_order = $products[$b]->total_order == null ? 0 : $products[$b]->total_order;
                $delivery_fee = 10000;
                $obj = array(
                    "product_id" => $products[$b]->product_id,
                    "product_name" => $is_varian == true ? $products[$b]->product_name." ".$products[$b]->uom_value." ".$products[$b]->uom_name : $products[$b]->product_name,
                    "product_image" => $products[$b]->product_image == null ? null : asset($products[$b]->product_image),
                    "purchased_quantity" => $products[$b]->purchased_quantity,
                    "purchased_price" => $this->currency->convertToCurrency($products[$b]->purchased_price),
                    "total_purchased_per_item" => $this->currency->convertToCurrency(($products[$b]->purchased_quantity * $products[$b]->purchased_price)),
                    "product_uom_id" => $products[$b]->uom_id,
                    "product_weight_ons" => $products[$b]->uom_name,
                    "product_weight" => ($products[$b]->is_decimal == 0 ? (string)ROUND($products[$b]->uom_value,0) : (Str::endsWith($products[$b]->uom_value,'.00') == true ? (string)ROUND($products[$b]->uom_value,0) : (string)$products[$b]->uom_value)),
                    "total_order" => $this->currency->convertToCurrency(($products[$b]->purchased_quantity * $products[$b]->purchased_price))
                );
                array_push($product_array, $obj);
            }
        }

        if(!$products){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $product_array], 200);
        }
    }

    public function addOrUpdateCart(Request $request)
    {
		if($request->status == 0){ //increase cart
			$check = DB::table('10_cart')
			->select('*')
			->where([
                ['buyer_user_id', $request->user_id],
                ['warehouse_id', $request->warehouse_id],
				['prod_id', $request->product_id]
			])
			->first();

			if(!$check){
				$q = DB::table('10_cart')
				->insert([
                    'buyer_user_id' => $request->user_id,
                    'warehouse_id' => $request->warehouse_id,
					'prod_id' => $request->product_id,
					'quantity' => 1,
					'amount' => $request->amount,
					//'total_amount' => $request->amount,
					'active_flag' => 1
				]);
			}else{
				$query = DB::table('10_cart')
					->select('*')
					->where([
                        ['buyer_user_id', $request->user_id],
                        ['warehouse_id', $request->warehouse_id],
						['prod_id', $request->product_id]
					])
					->first();
					
				$qty = $query->quantity + 1;
				/*$subtotal = $cart->total_amount + $request->amount;*/
				
				
				$q = DB::table('10_cart')
				->where([
                    ['buyer_user_id', $request->user_id],
                    ['warehouse_id', $request->warehouse_id],
					['prod_id', $request->product_id]
				])
				->update([
					'quantity' => $qty/*,
					'total_amount' => $subtotal*/
				]);
				
				
			}

			if(!$q){
				return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong insert cart'], 500);
			}else{
				/*$result = DB::table('10_cart AS a')
				->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
				->leftJoin('98_user AS c', 'c.user_id', '=', 'b.created_by')
				->select('a.cart_id', 'b.prod_id AS product_id', 'b.prod_name AS product_name', DB::raw('format(b.prod_price, 0) AS product_price'), 'c.user_name AS supplier_name', DB::raw('CONCAT("http://34.87.29.119:8080/", b.prod_image) AS product_image'), 'a.quantity AS product_qty', DB::raw('format((b.prod_price * a.quantity), 0) AS product_subtotal'))
				->where('a.buyer_user_id', $request->user_id)
				->get();*/

				return response()->json(['isSuccess' => true, 'message' => 'OK'/*, 'data' => $result]*/], 200);                   
			}
		}else{ //decrease cart
			$check = DB::table('10_cart')
			->select('*')
			->where([
				['buyer_user_id', $request->user_id],
                ['warehouse_id', $request->warehouse_id],
				['prod_id', $request->product_id]
			])
			->first();

			if(!$check){
				return response()->json(['isSuccess' => true, 'message' => 'Nothing product id in cart'], 200);
			}else{
				$query = DB::table('10_cart')
					->select('*')
					->where([
						['buyer_user_id', $request->user_id],
                        ['warehouse_id', $request->warehouse_id],
						['prod_id', $request->product_id]
					])
					->first();
					
				$qty = $check->quantity - 1;
				/*$subtotal = $cart->total_amount - $request->amount;*/
				
				if($qty <= 0){ //kalo 0 atau dibawah 0 hapus
					$q = DB::table('10_cart')
					->where([
						['buyer_user_id', $request->user_id],
                        ['warehouse_id', $request->warehouse_id],
						['prod_id', $request->product_id]
					])
					->delete();
				}else{
					$q = DB::table('10_cart')
					->where([
						['buyer_user_id', $request->user_id],
                        ['warehouse_id', $request->warehouse_id],
						['prod_id', $request->product_id]
					])
					->update([
						'quantity' => $qty/*,
						'total_amount' => $subtotal*/
					]);
				}
				
				if(!$q){
					return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong decrease cart'], 500);
				}else{
					return response()->json(['isSuccess' => true, 'message' => 'OK'/*, 'data' => $result]*/], 200);                   
				}
			}
		}
    }

    public function addToCart(Request $request)
    {
        $check = DB::table('10_cart')
            ->select('*')
            ->where([
                ['buyer_user_id', $request->user_id],
                ['warehouse_id', $request->warehouse_id],
                ['is_agent', $request->is_agent],
                ['prod_id', $request->product_id]
            ])
            ->first();

            if(!$check){
                $q = DB::table('10_cart')
                ->insert([
                    'buyer_user_id' => $request->user_id,
                    'warehouse_id' => $request->warehouse_id,
                    'prod_id' => $request->product_id,
                    'quantity' => $request->qty,
                    'amount' => $request->amount,
                    'warehouse_id' => $request->warehouse_id,
                    'is_agent' => $request->is_agent,
                    'total_amount' => $request->amount*$request->qty,
                    'active_flag' => 1
                ]);
            }else{
                $query = DB::table('10_cart')
                    ->select('*')
                    ->where([
                        ['buyer_user_id', $request->user_id],
                        ['warehouse_id', $request->warehouse_id],
                        ['is_agent', $request->is_agent],
                        ['prod_id', $request->product_id]
                    ])
                    ->first();
                    
                $qty = $query->quantity + $request->qty;
                $subtotal = $check->total_amount + $request->amount;
                
                
                $q = DB::table('10_cart')
                ->where([
                    ['buyer_user_id', $request->user_id],
                    ['warehouse_id', $request->warehouse_id],
                    ['is_agent', $request->is_agent],
                    ['prod_id', $request->product_id]
                ])
                ->update([
                    'quantity' => $qty,
                    'total_amount' => $subtotal
                ]);
                
                
            }

            if(!$q){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong insert cart'], 500);
            }else{
                /*$result = DB::table('10_cart AS a')
                ->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
                ->leftJoin('98_user AS c', 'c.user_id', '=', 'b.created_by')
                ->select('a.cart_id', 'b.prod_id AS product_id', 'b.prod_name AS product_name', DB::raw('format(b.prod_price, 0) AS product_price'), 'c.user_name AS supplier_name', DB::raw('CONCAT("http://34.87.29.119:8080/", b.prod_image) AS product_image'), 'a.quantity AS product_qty', DB::raw('format((b.prod_price * a.quantity), 0) AS product_subtotal'))
                ->where('a.buyer_user_id', $request->user_id)
                ->get();*/

                return response()->json(['isSuccess' => true, 'message' => 'OK'/*, 'data' => $result]*/], 200);                   
            }
    }
	
	
	public function deleteItemFromCart(Request $request)
    {
		$q = DB::table('10_cart')
		->where([
			['buyer_user_id', $request->user_id],
			['prod_id', $request->product_id]
		])
		->delete();
				
		if(!$q){
			return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong decrease cart'], 500);
		}else{
			return response()->json(['isSuccess' => true, 'message' => 'OK'/*, 'data' => $result]*/], 200);                   
		}
    }
	
    public function getPaymentStatus(Request $request){
        $url = "https://api.sandbox.midtrans.com/v2/".$request->input("order_id")."/status";
        
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => "Basic ".base64_encode(Config::get('midtrans.server_key'))
        ])->get($url);
        
        $data = array();
        $message = array();
        $code = 0;

        if($response['status_code'] == 201){
            if($request->input('payment_type') == 'echannel'){
                $data = array(
                    'payment_type' => $response['payment_type'],
                    'bill_key' => $response['bill_key'],
                    'biller_code' => $response['biller_code'],
                    'transaction_time' => $response['transaction_time'],
                    'gross_amount' => $response['gross_amount'],
                    'currency' => $response['currency'],
                    'order_id' => $response['order_id'],
                    'signature_key' => $response['signature_key'],
                    'status_code' => $response['status_code'],
                    'transaction_id' => $response['transaction_id'],
                    'transaction_status' => $response['transaction_status'],
                    'fraud_status' => $response['fraud_status'],
                    'merchant_id' => $response['merchant_id']
                );
            }else{
                $data = array(
                    'payment_type' => $response['payment_type'],
                    'va_detail' => array(
                        'bank' => $response['va_numbers'][0]['bank'],
                        'number' => $response['va_numbers'][0]['va_number']
                    ),
                    'transaction_time' => $response['transaction_time'],
                    'gross_amount' => $response['gross_amount'],
                    'currency' => $response['currency'],
                    'order_id' => $response['order_id'],
                    'signature_key' => $response['signature_key'],
                    'status_code' => $response['status_code'],
                    'transaction_id' => $response['transaction_id'],
                    'transaction_status' => $response['transaction_status'],
                    'fraud_status' => $response['fraud_status'],
                    'merchant_id' => $response['merchant_id']
                );
            }
            $message = array(
                'isSuccess' => true,
                'status_code' => $response['status_code'],
                'message' => $response['status_message'],
                'data' => $data
            );
            $code = 200;
        }else{
            $data = array();
            $message = array(
                'isSuccess' => false,
                'status_code' => $response['status_code'],
                'message' => $response['status_message'],
                'data' => $data
            );
            $code = 404;
        }
        
        return response()->json($message, $code);
    }

    public function addCart(Request $request)
    {
        $tes = $request->get('data');
        // dd($tes);
        foreach ($tes as $key) {
            $q = DB::table('10_cart')
            ->insert([
                "buyer_user_id"=>$key['user_id'],
                "prod_id" => $key["product_id"],
                "quantity" => $key["qty"],
                "amount" => $key["product_price"],
                "total_amount" => $key["subtotal"],
                "warehouse_id" => $key["warehouse_id"]
                // "active_flag" => $key["active_flag"],
                // "created_by" => $key["created_by"],
                // "updated_by" => $key["updated_by"]
            ]);
            if(!$q){
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when add to cart'], 500);
            }
        }
       
        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'Berhasil menambahkan ke cart'], 200);
        }
    }

    // public function checkout(Request $request){
    //     $shipment_method = DB::table('00_shipment_method')->where('shipment_method_id', $request->input('shipment_method_id'))->first();
    //     $payment_method = DB::table('00_payment_method')->where('payment_method_id', $request->input('payment_method_id'))->first();
    //     $voucher_amount = 0;

    //     if($request->input('voucher_code') != ""){
    //         $voucher = DB::table('00_vouchers')->select('00_vouchers.*',DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 2 ELSE 1 END) AS expired'))->where('voucher_code', strtoupper($request->input('voucher_code')))->first();
    //         $voucher_amount = ($voucher->amount == null || ($voucher->expired == 1 || $voucher->expired == 2)) ? 0 : $voucher->amount;
    //     }
    //     // echo $voucher_amount;
    //     // exit;
    //     $shipment_fee = $shipment_method->price == null ? 0 : $shipment_method->price;
    //     $admin_fee = $payment_method->admin_fee == null ? 0 : $payment_method->admin_fee;

    //     $order_id = DB::table('10_order')->insertGetId([
    //         'order_date' => date('Y-m-d H:i:s'),
    //         'buyer_user_id' => $request->input('user_id'),
    //         'payment_method_id' => $request->input('payment_method_id'),
    //         'shipment_method_id' => $request->input('shipment_method_id'),
    //         'destination_address' => $request->input('destination_address'),
    //         'shipment_price' => $shipment_fee,
    //         'admin_fee' => $admin_fee,
    //         'grand_total' => 0,
    //         'order_status_id' => 1,
    //         'active_flag' => 1
    //     ]);

    //     $order_code = "";
    //     $total = 0;

    //     if($order_id > 0){
    //         $order_id = (string)$order_id;
    //         $order_code = "";
    //         for ($i = 9; $i > 0; $i--) {
    //             if($i > 1){
    //                 $order_code .= "0";
    //             }else{
    //                 $order_code .= $order_id;
    //             }
    //         }
    //         $order_code = "YMOD".date('ymd').$order_code;
    //         DB::table('10_order')->where('order_id', $order_id)->update([
    //             'order_code' => $order_code
    //         ]);

    //         $products = $request->input('products');
    //         $product_id = array_map(function($value){
    //             return $value['id'];
    //         }, $products);
    //         $quantity = array_map(function($value){
    //             return $value['quantity'];
    //         }, $products);

    //         $data = DB::table('00_product')->whereIn('prod_id', $product_id)->get();
    //         $order_detail = array();
    //         for ($b=0; $b < count($data); $b++) { 
    //             $obj = array(
    //                 'order_id' => $order_id,
    //                 'prod_id' => $data[$b]->prod_id,
    //                 'quantity' => $quantity[$b],
    //                 'price' => $data[$b]->prod_price,
    //                 'active_flag' => 1
    //             );

    //             $total = $total + ($data[$b]->prod_price * $quantity[$b]);

    //             array_push($order_detail, $obj);
    //         }
    //         DB::table('10_order_detail')->insert($order_detail);
    //         $total = $total + $shipment_fee + $admin_fee + $voucher_amount;
    //     }

    //     if(!$order_id){
    //         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => array('order_id' => $order_code, 'order_amount' => $total, 'payment_type_id' => $request->input('payment_method_id'))], 200);
    //     }

    //     // print_r($request->all());
    // }
    
    // public function getVoucher(Request $request){
    //     if($request->input('voucher_id') != ""){
    //         $vouchers = DB::table('00_vouchers')->where('active_flag',1)->where('voucher_id', $request->input('voucher_id'))->whereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->first();
    
    //         $calculated_total = 0;
    //         $total_discount = 0;
            
    //         if(!$vouchers){
    //             return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
    //         }else{
    //             $voucher_amount = $vouchers->amount;
    //             $voucher_max_provided = $vouchers->max_value_price;
    //             $voucher_min_requirement_price = $vouchers->min_price_requirement;
    //             $is_fixed = $vouchers->is_fixed;
    //             $message = 'OK! Voucher has been applied';
    //             $data = array();
    
    //             if($request->input('total_price') < $voucher_min_requirement_price){
    //                 $message = 'Sorry, you cannot apply this voucher because the amount is below the minimum requirement';
    //             }else{
    //                 $total_discount = $is_fixed == 'Y' ? $voucher_amount : $request->input('total_price') * ($voucher_amount/100);
    //                 $calculated_total = $request->input('total_price') - $total_discount;
    //                 if($calculated_total > $voucher_max_provided){
    //                     $calculated_total = $request->input('total_price') - $voucher_max_provided;
    //                 }
    //                 if($calculated_total < 0){
    //                     $calculated_total = 0;
    //                 }
    //                 $data = array(
    //                     'price_before_discount' => $this->convertToCurrency($request->input('total_price')),
    //                     'voucher_amount' => $is_fixed == 'Y' ? $this->convertToCurrency($voucher_amount) : ($voucher_amount.'%'),
    //                     'max_provided' => $this->convertToCurrency($voucher_max_provided),
    //                     'price_after_discount' => $this->convertToCurrency($calculated_total),
    //                     'voucher_id' => $request->input('voucher_id')
    //                 );
    //             }
    
    //             return response()->json(array('isSuccess' => true, 'message' => $message, 'data' => $data));
    //         }
    //     }else{
    //         $vouchers = DB::table('00_vouchers')->where('active_flag',1)->whereRaw('CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at')->get();
    //         $voucher_array = array();

    //         for ($b=0; $b < count($vouchers); $b++) { 
    //             $obj = array(
    //                 'voucher_id' => $vouchers[$b]->voucher_id,
    //                 'voucher_name' => $vouchers[$b]->voucher_name,
    //                 'voucher_code' => $vouchers[$b]->voucher_code,
    //                 'max_uses' => $vouchers[$b]->max_uses,
    //                 'voucher_type_id' => $vouchers[$b]->voucher_type_id,
    //                 'voucher_amount' => $vouchers[$b]->amount,
    //                 'max_value_price' => $vouchers[$b]->max_value_price,
    //                 'min_price_requirement' => $vouchers[$b]->min_price_requirement,
    //                 'starts_at' => $vouchers[$b]->starts_at,
    //                 'expires_at' => $vouchers[$b]->expires_at,
    //                 'is_fixed' => $vouchers[$b]->is_fixed,
    //                 'voucher_description' => $vouchers[$b]->voucher_desc
    //             );
    //             array_push($voucher_array,$obj);
    //         }

    //         if(!$vouchers){
    //             return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 404);
    //         }else{
    //             return response()->json(array('isSuccess' => true, 'message' => 'OK', 'data' => $voucher_array));
    //         }
    //     }

    // }

   function distance($lat1, $lon1, $lat2, $lon2) {
      if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
      }
      else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515 * 1.609344;        
      }
      return ROUND($miles);
    }    

    public function shoppingDetail(Request $request)
    {                  

        if($request->get('is_agent') == 0){
        $profile = DB::table('00_customer')
            ->selectRaw('customer_id AS user_id, customer_name AS user_name, CASE WHEN saldo IS NULL THEN 0 ELSE convert(saldo, char(10)) END as user_saldo, customer_phone_number AS user_phone_number')
            ->leftJoin('10_cart', '00_customer.customer_id', '=', '10_cart.buyer_user_id')
            ->where([
                ['10_cart.buyer_user_id', $request->user_id],
                ['10_cart.active_flag', 1]
            ])
            ->first();
        }else{
            $profile = DB::table('98_user as a')
            ->selectRaw('a.user_id, a.user_name, CASE WHEN a.saldo IS NULL THEN 0 ELSE convert(a.saldo, char(10)) END as user_saldo, a.user_phone_number AS user_phone_number')
            ->leftJoin('10_cart', 'a.user_id', '=', '10_cart.buyer_user_id')
            ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
            ->where('b.organization_type_id', 2)
            ->where([
                ['10_cart.buyer_user_id', $request->user_id],
                ['10_cart.active_flag', 1]
            ])
            ->first();
        }
        // dd($profile);
        $address = DB::table('00_address AS a')
        ->leftJoin('00_kelurahan_desa AS b', 'b.kelurahan_desa_id', '=', 'a.kelurahan_desa_id')
        ->leftJoin('00_kecamatan AS c', 'c.kecamatan_id', '=', 'b.kecamatan_id')
        ->leftJoin('00_kabupaten_kota AS d', 'd.kabupaten_kota_id', '=', 'c.kabupaten_kota_id')
        ->leftJoin('00_provinsi AS e', 'e.provinsi_id', '=', 'd.provinsi_id')
        ->select('a.address_id',
            DB::raw('CONCAT(a.address_detail," ",b.kelurahan_desa_name," ", c.kecamatan_name," ",d.kabupaten_kota_name," ", e.provinsi_name," ",b.kode_pos ) AS address_detail'),
            'a.contact_person','a.phone_number')
        ->where([
            ['a.address_id', $request->address_id],
            ['a.active_flag', 1]
        ])
        ->orWhere([
            ['a.address_id', $request->address_id],
            ['a.active_flag', 1]     
        ])
        ->first();
        // dd($address);

        $data = DB::table('99_global_parameter')
                    ->whereIn('global_parameter_name',
                    ['pricing_include_tax','tax_value'])
                    ->get();      

        $pricing_include_tax = true;
        $tax_value = 0;
        if(count($data) > 0){
            $pricing_include_tax = $data[0]->global_parameter_value == 0 ? false : true;
            $tax_value = ($data[1]->global_parameter_value == "" || $data[1]->global_parameter_value == null) ? 0 : $data[1]->global_parameter_value;
        }
        $tax_value = number_format($tax_value,2);
        $response = array(
            "pricing_include_tax" => $pricing_include_tax,
            "national_income_tax" => $tax_value
        );

        $query = DB::table('10_cart AS a')
            ->leftJoin('00_warehouse AS b', 'b.warehouse_id', '=', 'a.warehouse_id')
            ->select(
                'a.buyer_user_id',
                'a.warehouse_id',
                'b.warehouse_name'
            ) 
            // ->where([
            //     ['a.buyer_user_id', $request->user_id],
            //     ['a.active_flag', 1]
            // ])
            ->groupBy('a.warehouse_id')->get();
        $shipment = DB::table('00_shipment_method')
        // ->where([['active_flag',1],['shipment_method_id',51]])
        ->where('active_flag',1)
        ->get();

        // PRICE TYPE
        $check_price_type = null;
        if ($request->input('is_agent') > 0) :
            $check_price_type = DB::table('98_user as user')
                            ->select('p.discount_type', 'p.starts_at', 'p.expires_at', 'p.rate')
                            ->Join('00_organization as org', 'user.organization_id', '=', 'org.organization_id')
                            ->Join('00_price_type as p', 'org.price_type_id', '=', 'p.price_type_id')
                            ->where('user.user_id','=',$request->input('user_id'))
                            ->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('p.starts_at'),DB::raw('p.expires_at')])
                            ->first();
        endif;
        // PRICE TYPE
        
        $warehouse_array = array();
        for ($i=0; $i < count($query); $i++) {
        $shipment_array = array();

        if($query[$i]->warehouse_id == null){
            continue;
        }
        // Middleware Endpoint Start-------------------------    
        // $urlgetDistance = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/cart/getDistanceData'); 
        // $bodyDistance = array(                    
        //         "warehouse_id" => $query[$i]->warehouse_id
        //     );
        // $responseDistance = Http::post($urlgetDistance, $bodyDistance);   
        
        // $jarak = $this ->distance($responseDistance['data']['latitudeFrom'],$responseDistance['data']['longitudeFrom'],$responseDistance['data']['latitudeTo'],$responseDistance['data']['longitudeTo']);      
        // Middleware Endpoint End-------------------------

        //Original DB Start--------------------------------------
        $responseDistance = DB::table('00_address as a')
            ->join('00_warehouse as b', 'a.address_id', '=', 'b.address_id')
            ->whereRaw('b.warehouse_id IN ('.$query[$i]->warehouse_id.')')
            ->selectRaw('a.latitude AS latitudeFrom, a.longitude AS longitudeFrom, 
                     (SELECT latitude FROM 00_address WHERE address_id = 5) AS latitudeTo,
                     (SELECT longitude FROM 00_address WHERE address_id = 5) AS longitudeTo')
            ->first();

        $jarak = $this ->distance($responseDistance->latitudeFrom, $responseDistance->longitudeFrom, $responseDistance->latitudeTo, $responseDistance->longitudeTo);
        //Original DB END--------------------------------------

            $q = DB::table('10_cart AS a')
                ->leftJoin('00_product AS b', 'b.prod_id', '=', 'a.prod_id')
                ->leftJoin('98_user AS c', 'c.user_id', '=', 'b.created_by')
                ->leftJoin('00_uom as d', 'd.uom_id', '=', 'b.uom_id')
                ->leftJoin('00_warehouse as e', 'e.warehouse_id', '=', 'a.warehouse_id')
                ->leftJoin('00_inventory as f', function($join){
                                         $join->on('b.prod_id', '=', 'f.prod_id');
                                         $join->on('f.warehouse_id', '=', 'e.warehouse_id');
                                         $join->on('f.organization_id', '=', DB::raw("'1'"));
                                     })
                ->leftJoin('00_product_price_type', '00_product_price_type.prod_id', '=', 'b.prod_id')
                // ->leftJoin('00_inventory as f', 'b.prod_id', '=', 'f.prod_id')
                ->leftJoin('00_product_voucher as g','g.prod_id','=','b.prod_id')
                ->leftJoin('00_vouchers as h','h.voucher_id','=','g.voucher_id')
                ->select('a.cart_id', 'b.is_taxable_product', 
                    'b.tax_value', 'a.buyer_user_id as user_id', 
                    'b.prod_id AS product_id', 'f.stock', 
                    'b.prod_name AS product_name', 
                    'b.prod_price AS product_price', 
                    'b.uom_value', 'd.uom_name',
                    'b.bruto',
                    'c.user_id as supplier_id', 
                    'c.user_name AS supplier_name', 
                    'b.prod_image AS product_image', 
                    'a.quantity AS qty', 
                    DB::raw('(b.prod_price * a.quantity) AS subtotal'),
                    'h.voucher_id',
                    'h.amount as voucher_amount',
                    'h.is_fixed as voucher_is_fixed',
                    'h.max_value_price as voucher_max_value_price',
                    'h.min_price_requirement as voucher_min_price',
                    '00_product_price_type.price_type_id',
                    '00_product_price_type.discount_type',
                    '00_product_price_type.rate',
                    DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN h.starts_at AND h.expires_at THEN 0 WHEN CURRENT_TIMESTAMP < h.starts_at THEN 2 ELSE 1 END) AS expired')
                )
                ->where([
                    ['a.buyer_user_id', $request->user_id],
                    ['a.warehouse_id', $query[$i]->warehouse_id],
                    ['a.active_flag', 1]
                ])
                ->get();

            $cart_array = array();
            $warehouse_subtotal = 0;
            $warehouse_weight = 0;
            for ($b=0; $b < count($q); $b++) { 
                // OLD
                /*
                $total_promo_value = 0;
                if($q[$b]->expired == 0 && $q[$b]->voucher_id != null){
                    if($q[$b]->voucher_is_fixed == 'Y'){
                        $total_promo_value = $q[$b]->voucher_amount;
                    }else{
                        $total_promo_value = ($q[$b]->product_price * $q[$b]->voucher_amount/100);
                    }

                }
                $product_price_after_discount = $q[$b]->product_price - $total_promo_value;
                if ($q[$b]->voucher_id == null) {
                    $product_discount = "0";
                    $product_discount_percentage = "0";
                } else {
                    if ($q[$b]->voucher_is_fixed == 'Y') {
                        $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                        $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                    } else {
                        $product_discount = $q[$b]->voucher_amount.'%';
                        $product_discount_percentage = $q[$b]->voucher_amount;
                        $voucer_isFix = "N";
                    }
                }
                */
                // OLD

                // NEW

                $check_price_wh = DB::table('00_product_warehouse')
                            ->where('prod_id','=',$q[$b]->product_id)
                            ->where('warehouse_id','=',$query[$i]->warehouse_id)
                            ->first();
                            
                $q[$b]->product_price = ($check_price_wh->prod_price == null) ? $q[$b]->product_price : $check_price_wh->prod_price;
                $total_promo_value = 0;
                if($q[$b]->price_type_id != null){
                    if($q[$b]->discount_type == 2){
                        $total_promo_value = $q[$b]->rate ;
                    }else{
                        $total_promo_value = ($q[$b]->product_price * $q[$b]->rate/100);
                    }
                } else {
                    if ($check_price_type) {
                        if($check_price_type->discount_type == 2){
                            $total_promo_value = $check_price_type->rate ;
                        }else{
                            $total_promo_value = ($q[$b]->product_price * $check_price_type->rate/100);
                        }
                    }
                }

                $product_price_after_discount = $q[$b]->product_price - $total_promo_value;
                if ($q[$b]->price_type_id == null) {
                    if ($check_price_type) {
                        if ($check_price_type->discount_type == 2) {
                            $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                            $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                        } else {
                            $product_discount = $check_price_type->rate.'%';
                            $product_discount_percentage = $check_price_type->rate;
                            $voucer_isFix = "N";
                        }
                    } else {
                        $product_discount = "0";
                        $product_discount_percentage = "0";   
                    }
                } else {
                    if ($q[$b]->discount_type == 2) {
                        $product_discount = $this->currency->thousandsCurrencyFormat($total_promo_value);
                        $product_discount_percentage = round(($total_promo_value/$q[$b]->product_price)*100);
                    } else {
                        $product_discount = $q[$b]->rate.'%';
                        $product_discount_percentage = $q[$b]->rate;
                        $voucer_isFix = "N";
                    }
                }
                // NEW

                $subtotal = $product_price_after_discount * $q[$b]->qty;
                $weight = $q[$b]->bruto * $q[$b]->qty;
                $warehouse_subtotal += $subtotal;
                $warehouse_weight += $weight;                
                $obj = array(
                    "cart_id" => $q[$b]->cart_id,
                    "product_id" => $q[$b]->product_id,
                    "product_name" => $q[$b]->product_name." ".($q[$b]->uom_value+0)." ".$q[$b]->uom_name,
                    "product_discount" => (string)$product_discount,
                    "product_discount_percentage" => (string)$product_discount_percentage,
                    "product_price_before_discount" => $q[$b]->product_price,
                    "product_price_after_discount" => $product_price_after_discount,
                    "product_stock" => round($q[$b]->stock),
                    "product_uom_value" => (string)($q[$b]->uom_value+0),
                    "product_uom_name" => $q[$b]->uom_name,
                    "product_pack_uom_value" => (string)($q[$b]->bruto+0),
                    "product_pack_uom_name" => "Kg",
                    "product_image" => asset($q[$b]->product_image),
                    "qty" => $q[$b]->qty,
                    "subtotal" => $subtotal,
                );                

                array_push($cart_array, $obj);

                
            }    //end query q                    
            foreach($shipment as $data){                                  
                $priceKG = 0;
                $priceKM = 0;
                $price = 0;
                
                //HITUNG BERAT & JARAK
                if ($data->is_fixed == 0) {
                    $price = $data->price;
                }else{

                    //HITUNG BERAT
                    if($weight <= $data->max_kg){
                        $priceKG = $data->max_price_kg;
                    }else{ //JIKA WEIGHT MELEBIHI SHIPMENT MAX KG

                        $priceKG = $data->max_price_kg + ($data->price_kg * ($weight-$data->max_kg));
                    }

                    //HITUNG JARAK
                    if($jarak <= $data->max_km){
                        $priceKM = $data->max_price_km;
                    }else{
                        $priceKM = $data->max_price_km + ($data->price_km * ($jarak - $data->max_km));
                    }
                    $price = $priceKG + $priceKM;                                                
                }
                $obj = array(
                    'shipment_method_id' => $data->shipment_method_id,
                    'shipment_method_name' => $data->shipment_method_name,
                    'price' => round($price)
                );
                array_push($shipment_array, $obj);
            }
            if (count($cart_array) > 0) :
                $data_cart = array(
                    'warehouse_id' => $query[$i]->warehouse_id,
                    'warehouse_name' => $query[$i]->warehouse_name,
                    'warehouse_subtotal' => $warehouse_subtotal,
                    'warehouse_weight' => number_format($warehouse_weight, 2, '.', ''),
                    'shipment' => $shipment_array,
                    'cart' => $cart_array
                );
                
                array_push($warehouse_array, $data_cart);
            endif;
        }

        $obj_warehouse = array(
            'profile' => $profile,
            'address' => $address,
            'pricing_include_tax' => $pricing_include_tax,
            'national_income_tax' => $tax_value,
            'data_cart' => $warehouse_array,
        );

        if(count($obj_warehouse) <= 0){
            return response()->json(['isSuccess' => true, 'message' => 'Cart is Empty', 'data' => $obj_warehouse], 200);
        }else if(count($obj_warehouse) > 0){
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $obj_warehouse], 200);
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 500);
        }
    }
}
