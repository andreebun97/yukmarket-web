<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryPerEcommerceController extends Controller
{
    public function checkCategory($id,$orgId){
    	$check = DB::table('00_category_per_ecommerce')
    	->select('00_category_per_ecommerce.category_id')
        ->join('00_category','00_category.category_id','=','00_category_per_ecommerce.category_id')
    	->where([
    		['00_category_per_ecommerce.category_id_ecommerce',$id],
            ['00_category_per_ecommerce.organization_id',$orgId]    		
    	])
    	->first();
    	return $check;
    }
    public function addCategory($request){
    	DB::table('00_category_per_ecommerce')
		->insertGetId($request);
    }
    public function getCategoryIdEcommerce($id){
        $category=DB::table('00_category_per_ecommerce')
        ->select('category_id')        
        ->where('category_id_ecommerce',$id)->first();        
        return $category->category_id;
    }    
}
