<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class CustomerController extends Controller
{
	public function missingCustomer(){
		$cust=DB::table('00_customer')
    	->whereRaw('LENGTH(customer_name)<1')
    	->select('customer_id')
    	->get();
    	return $cust;
	}
    public function orderMissingBuyerTokped($buyerId){
    	$order=DB::table('10_order as o')
    	->join('10_order_ecommerce as oc','o.order_id','oc.order_id')
    	->where('o.buyer_user_id',$buyerId)
    	->select('oc.order_ecom_code')
    	->first();
    	return $order;
    }
    public function generateMissingBuyer(){
    	$missing=$this->missingCustomer();
    	$token=app('App\Http\Controllers\GenerateTokenTokpedController')->generateTokenTokped();
    	$shopName=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokped_shop');
    	$shopId=app('App\Http\Controllers\OrganizationController')->organizationByName($shopName->global_parameter_value);
    	for($i=0;$i<count($missing);$i++){
    		$order=$this->orderMissingBuyerTokped($missing[$i]->customer_id);
            if($order){
                $url=config('global.tokopedia_integrator_url').'/getSingleOrderEncrypted?orderId='.$order->order_ecom_code;
                $singleOrder=Http::withToken($token)->timeout(3)->retry(2, 100)->get($url)->json();
                $cust=array(
                    'customer_name'=>$singleOrder['data']['buyer_info']['buyer_fullname'],
                    'customer_email'=>$singleOrder['data']['buyer_info']['buyer_email'],
                    'customer_phone_number'=>$singleOrder['data']['buyer_info']['buyer_phone'],
                );
                $this->updateCustomer($missing[$i]->customer_id,$cust);
            }
    	}
    }
    public function updateCustomer($id,$data){
    	DB::table('00_customer')
    	->where('customer_id',$id)
    	->update(
    		$data
    	);
    }
}
