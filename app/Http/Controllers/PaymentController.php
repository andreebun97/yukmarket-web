<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    
    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    public function getPaymentMethod(){
        
        $payment_method = DB::table('00_payment_method')
                ->select('payment_method_id', 'payment_method_name', 'payment_method_desc',  'payment_logo', 'admin_fee', 'admin_fee_percentage')
                ->where('active_flag',1)
                ->get();

        // dd($paymentMethod);

        $payment_method_array = array();

        foreach($payment_method as $data){
            $obj = array(
                'payment_method_id' => $data->payment_method_id,
                'payment_method_name' => $data->payment_method_name,
                'payment_method_desc' => $data->payment_method_desc,
                //'payment_logo' => $data->payment_logo == null ? null : asset($data->payment_logo),
                'payment_logo' => asset($data->payment_logo) == null ? null : asset($data->payment_logo),
                'admin_fee' => $data->admin_fee,
                'admin_fee_percentage' => $data->admin_fee_percentage,
            );
            array_push($payment_method_array, $obj);
        }

        if(!$payment_method){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found', 'data'=>[]], 404);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $payment_method_array], 200);
        }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    // public function getPaymentMethod(){
        
        //     $payment_method = DB::table('00_payment_method')
        //             ->where('active_flag',1)
        //             ->get();

        //     // dd($paymentMethod);

        //     $payment_method_array = array();

        //     foreach($payment_method as $data){
        //         $obj = array(
        //             'payment_method_id' => $data->payment_method_id,
        //             'payment_method_name' => $data->payment_method_name,
        //             'payment_method_desc' => $data->payment_method_desc,
        //             'payment_logo' => $data->payment_logo == null ? null : asset($data->payment_logo),
        //             'admin_fee' => $data->admin_fee,
        //             'admin_fee_percentage' => $data->admin_fee_percentage,
        //         );
        //         array_push($payment_method_array, $obj);
        //     }

        //     if(!$payment_method){
        //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        //     }else{
        //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $payment_method_array], 200);
        //     }
    // }
    // ORIGINAL END ---------------------------------------------------------------------
}
