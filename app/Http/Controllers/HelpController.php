<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelpController extends Controller {

    public function getHelp() {
        $help = DB::table('98_help')
                ->select('question', 'answer')
                ->where('row_status', 1)
                ->get();

        if (count($help) < 1) {
            return response()->json(['isSuccess' => true, 'message' => 'Data not found', 'data' => []], 200);
        } else {
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $help], 200);
        }
    }

}
