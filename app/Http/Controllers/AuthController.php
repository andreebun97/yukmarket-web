<?php

namespace App\Http\Controllers;

use App\AuthComp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Mail;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    // use SendsPasswordResetEmails;

    protected function generateToken()
    {
        $token = base64_encode(sha1(rand(1, 10000) . uniqid() . time()));
        // return response()->json(['access_token' => $token]);
        return $token;
    }

    public function index(){
        return AuthComp::all();
    }

    // public function register(Request $request)
    // {
    //     // $check = DB::table('00_customer')
    //     // ->select('customer_email', 'customer_phone_number')
    //     // ->where('customer_email', $request->email)
    //     // ->orWhere('customer_phone_number', $request->phone_number)
    //     // ->first();

    //     $url1 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckcustomer');
    //     $body1 = array(
    //             "customer_email" => $request->email,
    //             "customer_phone_number" => $request->phone_number
    //             );
    //     $request1 = Http::post($url1, $body1);
    //     // dd($request1['data']);
    //     if($request1['data'] == null){

    //     $url2 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/addCustomer');
    //     $body2 = array(
    //             'customer_name' => $request->name,
    //             'customer_phone_number' => $request->phone_number,
    //             'customer_email' => $request->email,
    //             'customer_password' => Hash::make($request->confirm_password),
    //             'last_login' => date('Y-m-d H:i:s')
    //             );
    //          $request2 = Http::post($url2, $body2);
    //          // dd($request2->status());
    //         // $q = DB::table('00_customer')
    //         // ->insert([
    //         //     'customer_name' => $request->name,
    //         //     'customer_phone_number' => $request->phone_number,
    //         //     'customer_email' => $request->email,
    //         //     'customer_password' => Hash::make($request->confirm_password),
    //         //     'last_login' => date('Y-m-d H:i:s')
    //         //     // 'remember_token' => $this->generateToken()

    //         // ]);
    //         $id = DB::getPdo()->lastInsertId();

    //         if($request2->status() == 202){
    //             return response()->json(['isSuccess' => true, 'message' => 'Register Success'], 200);
    //         }else{
    //             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);                 
    //         }
    //     }else{
    //         if($request1['data']['customer_email'] == $request->email){
    //             return response()->json(['isSuccess' => false, 'message' => 'Email already registered'], 200);
    //         }else if($request1['data']['customer_phone_number'] != '' && $request1['data']['customer_phone_number'] != null && 
    //             $request1['data']['customer_phone_number'] == $request->phone_number){
    //             return response()->json(['isSuccess' => false, 'message' => 'Phone number already registered'], 200);
    //         }else{
    //             return response()->json(['isSuccess' => false, 'message' => 'Lah Error, Ohiya data nya ga diisi'], 500);
    //         }
    //     }
    // }

    public function register(Request $request)
    {
        $check = $this->loginCheckCustomer($request->email, $request->phone_number);
        $check2 = $this->loginCheckAgent($request->email, $request->phone_number);

        if(!$check && !$check2){
            //belum ada data
            $addCustomer = DB::table('00_customer')
                ->insert([
                    'customer_name' => $request->name,
                    'customer_phone_number' => $request->phone_number,
                    'customer_email' => $request->email,
                    'customer_password' => Hash::make($request->confirm_password),
                    'last_login' => date('Y-m-d H:i:s')
                ]);

            if($addCustomer){
                return response()->json(['isSuccess' => true, 'message' => 'Register Success'], 200);
            }else{
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
            }
        } else {
            //email atau phone registered
            $checkEmail = ($check!=null)? $check->customer_email : $check2->user_email;
            $checkPhone = ($check!=null)? $check->customer_phone_number : $check2->user_phone_number;

            if(($checkEmail!='' && $checkEmail!=null && $checkEmail==$request->email)){
                return response()->json(['isSuccess' => false, 'message' => 'Email already registered'], 200);
            }else if(($checkPhone!='' && $checkPhone!=null && $checkPhone==$request->phone_number)){
                return response()->json(['isSuccess' => false, 'message' => 'Phone number already registered'], 200);
            }else{
                return response()->json(['isSuccess' => false, 'message' => 'Lah Error, Ohiya data nya ga diisi lengkap'], 403);
            }
        }
    }


    public function verifyUsername(Request $request)
    {
        
        if($request->username == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Username tidak boleh kosong'], 400);
        }else{
            $check = DB::table('00_customer AS a')
            // ->leftJoin('00_customer_role AS b', 'b.customer_id', '=', 'a.customer_id')
            ->select('a.customer_email', 'a.customer_phone_number')
            ->where([
                ['a.customer_email', $request->username],
                // ['b.role_id', 4]
            ])
            ->orWhere([
                ['a.customer_phone_number', $request->username],
                // ['b.role_id', 4]
            ])->get();

            $check_agent = DB::table('98_user as a')
            ->select('a.user_email', 'a.user_phone_number')
            ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
            ->where('b.organization_type_id', 2)
            ->where([
                ['a.user_email', $request->username],
                
            ])
            ->orWhere([
                ['a.user_phone_number', $request->username],
            ])
            ->get();

            if(!$check || !$check_agent){
                return response()->json(['isSuccess' => false, 'message' => 'Internal Server Error'], 500);
            }else if(count($check) > 0 || count($check_agent) > 0){
                return response()->json(['isSuccess' => true, 'message' => 'OK'], 200);  
            }else{  
                return response()->json(['isSuccess' => false, 'message' => 'Email atau nomor telepon tidak terdaftar'], 200);
            }
        }


            // $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckcustomer');
            // $body = array(
            //         "customer_email" => $request->username,
            //         "customer_phone_number" => $request->username
            //     );
            // $request1 = Http::post($url, $body);


            // $url2 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckagent');
            // $body2= array(
            //         "user_email" => $request->username,
            //         "user_phone_number" => $request->username
            //     );

            // $request2 = Http::post($url2, $body2);
            // // dd($request2['data']);
            // if(!$request1 || !$request2){
            //     return response()->json(['isSuccess' => false, 'message' => 'Internal Server Error'], 500);
            // }else if($request1['data'] != null || $request2['data'] != null ){
            //     return response()->json(['isSuccess' => true, 'message' => 'OK'], 200);  
            // }else{  
            //     return response()->json(['isSuccess' => false, 'message' => 'Email atau nomor telepon tidak terdaftar'], 200);
            // }
        
    }

     // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
//    public function login(Request $request)
//    {
//        $login = app('App\Http\Controllers\MiddlewareEndpointController')->login($request);
//        if($login){            
//            $nohp = $request->username;
//            $pass = password_verify($request->password, $login['password']);
//            if ($nohp == $request->username) {
//                if ($pass) {
//                    if(!password_verify($request->password, $login['password']) || $login['password'] == null || $login['password'] == ''){
//                    return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
//
//                    } elseif ($login['active_flag'] != 1) {
//                        return response()->json(['isSuccess' => false, 'message' => 'User Inactivated'], 200);
//                    } else {
//                        if($request->username == $login['email']){
//                            $login_type = 'Email';
//
//                            if($login['name'] != ''){
//                                $username = $login['name'];
//                            }else if($login['email'] != ''){
//                                $username = $login['email'];
//                            }else if($login['phone_number'] != ''){
//                                $username = $login['phone_number'];
//                            }
//
//                            $otp = mt_rand(100000, 999999);
//                            try{
//                                Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $login)
//                                {
//                                    $message->subject('Login YukMarket');
//                                    $message->from('support@yukmarket.com', 'YukMarket');
//                                    $message->to($login['email']);
//                                });
//                            }
//                            catch (Exception $e){
//                                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 403);
//                            }
//                            //INSERT OTP
//                            $addOtp = app('App\Http\Controllers\MiddlewareEndpointController')->addOTP($login['id'],$otp,$login['is_agent']);                            
//                        }else{
//                            $login_type = 'Phone';
//                        }
//                        
//                        return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => [
//                            'user_id' => $login['id'],
//                            'login_type' => $login_type,
//                            'username' => $request->username,
//                            'is_agent' => $login['is_agent']
//                        ]], 200);
//                    }
//                } else {
//                    return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
//                }            
//            }else{
//                return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
//            }        
//        } else {
//            return response()->json(['isSuccess' => false, 'message' => 'User not found or Invalid Email'], 200);
//            
//        }                    
//    }
    
    //login pake php anjing
    public function login(Request $request)
    {
        if($request->username == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Username can`t be empty'], 400);
        }else if($request->password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Password can`t be empty'], 400);
        }else{
            $check = DB::table('00_customer AS a')
            // ->leftJoin('00_customer_role AS b', 'b.customer_id', '=', 'a.customer_id')
            ->select('a.customer_id', 'a.customer_name', 'a.customer_email', 'a.customer_phone_number', 'a.customer_password','fcm_token','a.active_flag')
            ->where([
                ['a.customer_email', $request->username],
                // ['b.role_id', 4]
            ])
            ->orWhere([
                ['a.customer_phone_number', $request->username],
                // ['b.role_id', 4]
            ])
            ->first();
            // dd($check);

            $check_agent = DB::table('98_user as a')
            ->select('a.user_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
                'b.organization_type_id','b.organization_name', 'b.organization_desc','a.active_flag'
        )
            ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
            ->where('b.organization_type_id', 2)
            ->where([
                ['a.user_email', $request->username],
                
            ])
            ->orWhere([
                ['a.user_phone_number', $request->username],
            ])
            ->first();
            // dd($check_agent);

        if ($check) {
            $nohp = $request->username;
            $pass = password_verify($request->password, $check->customer_password);
            if ($nohp == $request->username) {
                if ($pass) {
                    if(!password_verify($request->password, $check->customer_password) || $check->customer_password == null || $check->customer_password == ''){
                    return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);

                    } elseif ($check->active_flag != 1) {
                        return response()->json(['isSuccess' => false, 'message' => 'User Inactivated'], 200);
                    } else {
                        if($request->username == $check->customer_email){
                            $login_type = 'Email';

                            if($check->customer_name != ''){
                                $username = $check->customer_name;
                            }else if($check->customer_email != ''){
                                $username = $check->customer_email;
                            }else if($check->customer_phone_number != ''){
                                $username = $check->customer_phone_number;
                            }

                            $otp = mt_rand(100000, 999999);
                            try{
                                Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $check)
                                {
                                    $message->subject('Login YukMarket');
                                    $message->from('support@yukmarket.com', 'YukMarket');
                                    $message->to($check->customer_email);
                                });
                            }
                            catch (Exception $e){
                                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 403);
                            }
                            DB::table('99_otp')
                            ->where([
                                ['customer_id', $check->customer_id]
                            ])
                            ->delete();

                            DB::table('99_otp')
                            ->insert([
                                'customer_id' => $check->customer_id,
                                'otp_code' => $otp,
                                'created_date' => date('Y-m-d H:i:s'),
                                'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

                            ]);
                        }else{
                            $login_type = 'Phone';
                        }

                        return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => [
                            'user_id' => $check->customer_id,
                            'login_type' => $login_type,
                            'username' => $request->username,
                            'is_agent' => 0
                        ]], 200);
                    }
                } else {
                    return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
                }
            }
        } elseif ($check_agent) {
            $nohp = $request->username;
            $pass = password_verify($request->password, $check_agent->user_password);
            if ($nohp == $request->username) {
                if ($pass) {
                    if(!password_verify($request->password, $check_agent->user_password) || $check_agent->user_password == null || $check_agent->user_password == ''){
                    return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
                    } elseif ($check_agent->active_flag != 1) {
                        return response()->json(['isSuccess' => false, 'message' => 'User Inactivated'], 200);
                    } else {
                        if($request->username == $check_agent->user_email){
                            $login_type = 'Email';

                            if($check_agent->user_name != ''){
                                $username = $check_agent->user_name;
                            }else if($check_agent->user_email != ''){
                                $username = $check_agent->user_email;
                            }else if($check_agent->user_phone_number != ''){
                                $username = $check_agent->user_phone_number;
                            }

                            $otp = mt_rand(100000, 999999);
                            try{
                                Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $check_agent)
                                {
                                    $message->subject('Login YukMarket');
                                    $message->from('support@yukmarket.com', 'YukMarket');
                                    $message->to($check_agent->user_email);
                                });
                            }
                            catch (Exception $e){
                                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 403);
                            }

                            DB::table('99_otp')
                            ->where([
                                ['customer_id', $check_agent->user_id]
                            ])
                            ->delete();

                            DB::table('99_otp')
                            ->insert([
                                'user_id' => $check_agent->user_id,
                                'otp_code' => $otp,
                                'created_date' => date('Y-m-d H:i:s'),
                                'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

                            ]);
                        }else{
                            $login_type = 'Phone';
                        }

                        return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => [
                            'user_id' => $check_agent->user_id,
                            'login_type' => $login_type,
                            'username' => $request->username,
                            'is_agent' => 1
                        ]], 200);
                    }
                } else {
                     return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
                }
            }
        } else {
            return response()->json(['isSuccess' => false, 'message' => 'User not found or Invalid Email'], 200);
            
        }
        // return response()->json($jsn);

            
        }
    }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    public function loginSSO(Request $request)
    {               
        $checkCustomer = DB::table('00_customer')
                ->select('customer_id', 'customer_email', 'active_flag')
                ->where('customer_email', '=', $request->email)
                ->first();

        if($checkCustomer == null){
            $request_add = DB::table('00_customer')
                    ->insertGetId([
                        'customer_email' => $request->email,
                        'last_login' => date('Y-m-d H:i:s'),
                        'customer_name' => $request->name,
                        'fcm_token' => $request->fcm_token
                    ], 'customer_id');

            if($request_add){
                $update = DB::table('00_customer')
                    ->where('customer_id', '=', $request_add)
                    ->update([
                        'fcm_token' => $request->fcm_token,
                        'remember_token' => $this->generateToken()
                    ]);

                if(!$update){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
                }

                $lastCustomer = DB::table('00_customer')
                    ->selectRaw('customer_id AS user_id, customer_name AS user_name, customer_email AS user_email, customer_phone_number AS user_phone_number, customer_image AS user_photo_profile, remember_token, fcm_token')
                    ->where('customer_email', '=', $request->email)
                    ->first();

                return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $lastCustomer], 200);
            } else {
                return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
            }
            
        }else{
            if($checkCustomer->active_flag != 1){
                return response()->json(['isSuccess' => false, 'message' => 'User Inactivated', 'data' => []], 403);
            }else{
                $update = DB::table('00_customer')
                    ->where('customer_id', '=', $checkCustomer->customer_id)
                    ->update([
                        'fcm_token' => $request->fcm_token,
                        'remember_token' => $this->generateToken()
                    ]);

                if(!$update){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
                }

                $lastCustomer = DB::table('00_customer')
                    ->selectRaw('customer_id AS user_id, customer_name AS user_name, customer_email AS user_email, customer_phone_number AS user_phone_number, customer_image AS user_photo_profile, remember_token, fcm_token')
                    ->where('customer_email', '=', $request->email)
                    ->first();

                return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $lastCustomer], 200);
            }
        }
    }

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    // public function loginSSO(Request $request)
    // {               
    //     $checkCustomer =app('App\Http\Controllers\MiddlewareEndpointController')->checkLoginSSO($request->email);

    //     if($checkCustomer['data'] == null){
    //         $request_add =app('App\Http\Controllers\MiddlewareEndpointController')->addCustomerSSO($request);                         
    //         $lastCustomer =app('App\Http\Controllers\MiddlewareEndpointController')->getCustomerByEmail($request->email);
            
    //         if($request_add->status() != 202 ){
    //             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //         }else{
                 
    //             //$update =app('App\Http\Controllers\MiddlewareEndpointController')->updateCustomerSSO($checkCustomer['data']['customer_id'],$request->fcm_token,$this->generateToken());
    //             $update =app('App\Http\Controllers\MiddlewareEndpointController')->updateCustomerSSO($lastCustomer['data']['user_id'],$request->fcm_token,$this->generateToken());

    //             //catatan: harus select lagi data baru setelah update, untuk dilampirkan di response

    //             return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $lastCustomer['data']], 200);
    //         }//TO BE CONTINUE
    //     }else{
    //         if($checkCustomer['data']['active_flag'] != 1){
    //             return response()->json(['isSuccess' => false, 'message' => 'User Inactivated'], 403);
    //         }else{
                
    //             $update =app('App\Http\Controllers\MiddlewareEndpointController')->updateCustomerSSO($checkCustomer['data']['customer_id'],$request->fcm_token,$this->generateToken());
                
    //             $lastCustomer =app('App\Http\Controllers\MiddlewareEndpointController')->getCustomerByEmail($request->email);

    //             return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $lastCustomer['data']], 200);
    //         }
    //     }
    // }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    public function logout(Request $request)
    {
        $q = DB::table('00_customer')
        ->where('customer_id', $request->user_id)
        ->update(['remember_token' => null]);

        if(!$q){
            return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 404);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'Logout Success'], 200);
        }
    }


    public function changePassword(Request $request)
    { 
        if($request->old_password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Old password can`t be empty'], 200);
        }else if($request->new_password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'New password can`t be empty'], 200);
        }else if($request->confirm_password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Confirm password can`t be empty'], 200);
        }else if($request->new_password != $request->confirm_password){
            return response()->json(['isSuccess' => false, 'message' => 'New password and confirm password must be same'], 200);
        }else{
            //cek agen atau user
            if($request->get('is_agent') == 0){
                $check = DB::table('00_customer')
                        ->select('customer_password')
                        ->where('customer_id', '=', $request->user_id)
                        ->first();

                if($check->customer_password == null || $check->customer_password ==''){
                    $setPassword = DB::table('00_customer')
                            ->where('customer_id', '=', $request->user_id)
                            ->update([
                                'customer_password' => Hash::make($request->confirm_password)
                            ]);
                } else {
                    $match = password_verify($request->old_password, $check->customer_password);
                    if(!$match){
                        return response()->json(['isSuccess' => false, 'message' => 'Old password isn`t correct'], 200);
                    } else {
                        $setPassword = DB::table('00_customer')
                            ->where('customer_id', '=', $request->user_id)
                            ->update([
                                'customer_password' => Hash::make($request->confirm_password)
                            ]);
                    }
                }
                
                if(!$setPassword){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
                }else{
                    return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
                }

            } else {
                //masuk ke agen
                $check = DB::table('98_user')
                        ->select('user_password')
                        ->where('user_id', '=', $request->user_id)
                        ->first();

                if($check->user_password == null || $check->user_password == ''){
                    $setPassword = DB::table('98_user')
                            ->where('user_id', '=', $request->user_id)
                            ->update([
                                'user_password' => Hash::make($request->confirm_password)
                            ]);
                } else {
                    $match = password_verify($request->old_password, $check->user_password);
                    if(!$match){
                        return response()->json(['isSuccess' => false, 'message' => 'Old password isn`t correct'], 200);
                    } else {
                        $setPassword = DB::table('98_user')
                            ->where('user_id', '=', $request->user_id)
                            ->update([
                                'user_password' => Hash::make($request->confirm_password)
                            ]);
                    }
                }

                if(!$setPassword){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
                }else{
                    return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
                }
            }
        }
    }

    // public function changePassword(Request $request)
    // { 
    //     if($request->new_password == ''){
    //         return response()->json(['isSuccess' => false, 'message' => 'New password can`t be empty'], 200);
    //     }else if($request->confirm_password == ''){
    //         return response()->json(['isSuccess' => false, 'message' => 'Confirm password can`t be empty'], 200);
    //     }else if($request->new_password != $request->confirm_password){
    //         return response()->json(['isSuccess' => false, 'message' => 'New password and confirm password must be same'], 200);
    //     }else{
    //         //cek agen atau user
    //         if($request->get('is_agent') == 0){
    //             $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/changePassCustomer');
    //             $body = array(
    //                 "customer_id" => $request->user_id,
    //                 );
    //             $request1 = Http::post($url, $body);

    //             if($request1['data']['customer_password'] == null || $request1['data']['customer_password'] == ''){

    //                 $url_update = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/updateChangePassCustomer');
    //                 $body_update = array(
    //                     "customer_id" => $request->user_id,
    //                     'customer_password' => Hash::make($request->confirm_password)
    //                     );
    //                 $request_update = Http::post($url_update, $body_update);
                    
    //                 if($request_update->status() != 202){
    //                     return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //                 }else{
    //                     return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
    //                 }
    //             }else{
    //                 $match = password_verify($request->old_password, $request1['data']['customer_password']);
    //                 // dd($old_password)
    //                 if(!$match){
    //                     return response()->json(['isSuccess' => false, 'message' => 'Old password isn`t correct'], 200);
    //                 }else{
                       
    //                     $url_update = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/updateChangePassCustomer');
    //                     $body_update = array(
    //                     "customer_id" => $request->user_id,
    //                     'customer_password' => Hash::make($request->confirm_password)
    //                     );
    //                     $request_update = Http::post($url_update, $body_update);

    //                     if($request_update->status() != 202){
    //                         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //                     }else{
    //                         return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
    //                     }
    //                 }
    //             }
    //         }else {
               
    //             //masuk ke agen
    //             $url2 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/changePassAgent');
    //             $body2 = array(
    //                 "user_id" => $request->user_id,
    //                 );
    //             $request2 = Http::post($url2, $body2);

    //             if($request2['data']['user_password'] == null || $request2['data']['user_password'] == ''){
                    
    //                 $url_update = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/updateChangePassAgent');
    //                 $body_update = array(
    //                     "user_id" => $request->user_id,
    //                     'user_password' => Hash::make($request->confirm_password)
    //                 );
    //                 $request_update = Http::post($url_update, $body_update);

    //                 if($request_update->status() != 202){
    //                     return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //                 }else{
    //                     return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
    //                 }
    //             }else{
    //                 $match = password_verify($request->old_password, $request2['data']['user_password']);

    //                 if(!$match){
    //                     return response()->json(['isSuccess' => false, 'message' => 'Old password isn`t correct'], 200);
    //                 }else{
                        
    //                     $url_update = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/updateChangePassAgent');
    //                     $body_update = array(
    //                     "user_id" => $request->user_id,
    //                     'user_password' => Hash::make($request->confirm_password)
    //                      );
    //                     $request_update = Http::post($url_update, $body_update);

    //                     if($request_update->status() != 202){
    //                         return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //                     }else{
    //                         return response()->json(['isSuccess' => true, 'message' => 'Password successfully changed'], 200);
    //                     }
    //                 }
    //             }
    //         }
                
    //     }
    // }

    public function forgotPassword(Request $request)
    {
        if($request->new_password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Password baru tidak boleh kosong'], 400);
        }else if($request->confirm_password == ''){
            return response()->json(['isSuccess' => false, 'message' => 'Konfirmasi password tidak boleh kosong'], 400);
        }else if($request->new_password != $request->confirm_password){
            return response()->json(['isSuccess' => false, 'message' => 'Password baru dan konfirmasi harus sama'], 400);
        }else{
            //cek agen atau user
            if($request->get('is_agent') == 0){
                $update = DB::table('00_customer')
                ->where('customer_id', $request->user_id)
                ->update(['customer_password' => Hash::make($request->confirm_password)]);
            }else{
                 $update = DB::table('98_user')
                ->where('user_id', $request->user_id)
                ->update(['user_password' => Hash::make($request->confirm_password)]);
            }

            if(!$update){
                return response()->json(['isSuccess' => false, 'message' => 'Internal Server Eror'], 500);
            }else{
                return response()->json(['isSuccess' => true, 'message' => 'Password berhasil diganti'], 200);
            }
        }
    }


    public function sendOTPResetPassword(Request $request)
    {
        if($request->username=='' || $request->username==null){
            return response()->json(['isSuccess' => false, 'message' => 'Please enter registered email or phone number'], 200);
        }

        $isCustomer = $this->loginCheckCustomer($request->username, $request->username);
        $isAgent = $this->loginCheckAgent($request->username, $request->username);

        if ($isCustomer != null) {
            if($request->username ==  $isCustomer->customer_email){
                $login_type = 'Email';
                if($isCustomer->customer_name != ''){
                    $username = $isCustomer->customer_name;
                }else if($isCustomer->customer_email != ''){
                    $username = $isCustomer->customer_email;
                }else if($isCustomer->customer_phone_number != ''){
                    $username = $isCustomer->customer_phone_number;
                }

                $otp = mt_rand(100000, 999999);
                try{
                    Mail::send('emailreset', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $isCustomer)
                    {
                        $message->subject('Reset Password YukMarket');
                        $message->from('support@yukmarket.com', 'YukMarket');
                        $message->to($isCustomer->customer_email);
                    });
                }
                catch (Exception $e){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 500);
                }
                DB::table('99_otp')
                ->where([
                    ['customer_id', $isCustomer->customer_id]
                ])
                ->delete();

                DB::table('99_otp')
                ->insert([
                    'customer_id' => $isCustomer->customer_id,
                    'otp_code' => $otp,
                    'created_date' => date('Y-m-d H:i:s'),
                    'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))
                ]);

            }else if($request->username ==  $isCustomer->customer_phone_number){
                $login_type = 'Phone';
            }

            return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent', 'data' =>[
                'user_id' => $isCustomer->customer_id,
                'login_type' => $login_type,
                'username' => $request->username,
                'is_agent' => 0
            ]], 200);
            
        } else if ($isAgent != null) {
            if($request->username == $isAgent->user_email){
                $login_type = 'Email';
                if($isAgent->user_name != ''){
                    $username = $isAgent->user_name;
                }else if($isAgent->user_email != ''){
                    $username = $isAgent->user_email;
                }else if($isAgent->user_phone_number != ''){
                    $username = $isAgent->user_phone_number;
                }

                $otp = mt_rand(100000, 999999);
                try{
                    Mail::send('emailreset', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $isAgent)
                    {
                        $message->subject('Reset Password YukMarket');
                        $message->from('support@yukmarket.com', 'YukMarket');
                        $message->to($isAgent->user_email);
                    });
                }
                catch (Exception $e){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 500);
                }
                DB::table('99_otp')
                ->where([
                    ['user_id', $isAgent->user_id]
                ])
                ->delete();

                DB::table('99_otp')
                ->insert([
                    'user_id' => $isAgent->user_id,
                    'otp_code' => $otp,
                    'created_date' => date('Y-m-d H:i:s'),
                    'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))
                ]);
            }else{
                $login_type = 'Phone';
            }

            return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent', 'data' =>[
                'user_id' => $isAgent->user_id,
                'login_type' => $login_type,
                'username' => $request->username,
                'is_agent' => 1
            ]], 200);

        } else {
            return response()->json(['isSuccess' => false, 'message' => 'User not found with email or phone number you entered'], 200);
        }
    }

    // public function sendOTPResetPassword(Request $request)
    // {

    //     // $check = DB::table('00_customer AS a')
    //     // // ->leftJoin('00_customer_role AS b', 'b.customer_id', '=', 'a.customer_id')
    //     // ->select('a.customer_id', 'a.customer_name', 'a.customer_email', 'a.customer_phone_number', 'a.customer_password', 'a.active_flag')
    //     // ->where([
    //     //     ['a.customer_email', $request->username],
    //     //     // ['b.role_id', 4]
    //     // ])
    //     // ->orWhere([
    //     //     ['a.customer_phone_number', $request->username],
    //     //     // ['b.role_id', 4]
    //     // ])
    //     // ->first();
    //     $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckcustomer');
    //     $body = array(
    //                 "customer_email" => $request->username,
    //                 "customer_phone_number" => $request->username
    //         );
    //     $request1 = Http::post($url, $body);

            

    //      // $check_agent = DB::table('98_user as a')
    //      //    ->select('a.user_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
    //      //        'b.organization_type_id','a.active_flag')
    //      //    ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
    //      //    ->where('b.organization_type_id', 2)
    //      //    ->where([
    //      //        ['a.user_email', $request->username],
                
    //      //    ])
    //      //    ->orWhere([
    //      //        ['a.user_phone_number', $request->username],
    //      //    ])
    //      //    ->first();
    //         // dd($check_agent);
    //     $url2 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckagent');
    //     $body2= array(
    //             "user_email" => $request->username,
    //             "user_phone_number" => $request->username
    //         );
    //     $request2 = Http::post($url2, $body2);

    //      if ($request1['data'] != null) {
    //         $nohp = $request->username;
    //         if ($nohp == $request->username) {
    //                 if($request->username ==  $request1['data']['customer_email']){
    //                         $login_type = 'Email';
    //                         if($request1['data']['customer_name'] != ''){
    //                             $username = $request1['data']['customer_name'];
    //                         }else if($request1['data']['customer_email'] != ''){
    //                             $username = $request1['data']['customer_email'];
    //                         }else if($request1['data']['customer_phone_number'] != ''){
    //                             $username = $request1['data']['customer_phone_number'];
    //                         }

    //                         $otp = mt_rand(100000, 999999);
    //                         try{
    //                             Mail::send('emailreset', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $request1)
    //                             {
    //                                 $message->subject('Reset Password YukMarket');
    //                                 $message->from('support@yukmarket.com', 'YukMarket');
    //                                 $message->to($request1['data']['customer_email']);
    //                             });
    //                         }
    //                         catch (Exception $e){
    //                             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 500);
    //                         }
    //                         DB::table('99_otp')
    //                         ->where([
    //                             ['customer_id', $request1['data']['customer_id']]
    //                         ])
    //                         ->delete();

    //                         DB::table('99_otp')
    //                         ->insert([
    //                             'customer_id' => $request1['data']['customer_id'],
    //                             'otp_code' => $otp,
    //                             'created_date' => date('Y-m-d H:i:s'),
    //                             'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

    //                         ]);

    //                     }else{
    //                         $login_type = 'Phone';
    //                     }

    //                     return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent', 'data' =>[
    //                         'user_id' => $request1['data']['customer_id'],
    //                         'login_type' => $login_type,
    //                         'username' => $request->username,
    //                         'is_agent' => 0
    //                     ]], 200);
                    
    //             } else {
    //                  return response()->json(['isSuccess' => false, 'message' => 'User not found with email or phone number you entered'], 200);
    //             }
            
    //     } elseif ($request2['data'] != null) {
    //         $nohp = $request->username;
    //         if ($nohp == $request->username) {
    //                 if($request->username == $request2['data']['user_email']){
    //                         $login_type = 'Email';
    //                         if($request2['data']['user_name'] != ''){
    //                             $username = $request2['data']['user_name'];
    //                         }else if($request2['data']['user_email'] != ''){
    //                             $username = $request2['data']['user_email'];
    //                         }else if($request2['data']['user_phone_number'] != ''){
    //                             $username = $request2['data']['user_phone_number'];
    //                         }

    //                         $otp = mt_rand(100000, 999999);
    //                         try{
    //                             Mail::send('emailreset', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $request2)
    //                             {
    //                                 $message->subject('Reset Password YukMarket');
    //                                 $message->from('support@yukmarket.com', 'YukMarket');
    //                                 $message->to($request2['data']['user_email']);
    //                             });
    //                         }
    //                         catch (Exception $e){
    //                             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 500);
    //                         }
    //                         DB::table('99_otp')
    //                         ->where([
    //                             ['user_id', $request2['data']['user_id']]
    //                         ])
    //                         ->delete();

    //                         DB::table('99_otp')
    //                         ->insert([
    //                             'user_id' => $request2['data']['user_id'],
    //                             'otp_code' => $otp,
    //                             'created_date' => date('Y-m-d H:i:s'),
    //                             'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

    //                         ]);

    //                     }else{
    //                         $login_type = 'Phone';
    //                     }

    //                     return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent', 'data' =>[
    //                         'user_id' => $request2['data']['user_id'],
    //                         'login_type' => $login_type,
    //                         'username' => $request->username,
    //                         'is_agent' => 1
    //                     ]], 200);
                    
    //             } else {
    //                  return response()->json(['isSuccess' => false, 'message' => 'User not found or Invalid Email'], 200);
    //             }

    //     } else {
    //         return response()->json(['isSuccess' => false, 'message' => 'User not found with email or phone number you entered'], 200);
            
    //     }
    // }


    public function sendRequestOTP(Request $request)
    {
        //cek agen atau user
        if($request->get('is_agent') == 0){
            $check = $this->loginCheckCustomer($request->username, $request->username);

            if($request->username == $check->customer_email){
                $login_type = 'Email';

                if($check->customer_name != ''){
                    $username = $check->customer_name;
                }else if($check->customer_email != ''){
                    $username = $check->customer_email;
                }else if($check->customer_phone_number != ''){
                    $username = $check->customer_phone_number;
                }

                $otp = mt_rand(100000, 999999);
                try{
                    Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $check)
                    {
                        $message->subject('Login YukMarket');
                        $message->from('support@yukmarket.com', 'YukMarket');
                        $message->to($check->customer_email);
                    });
                }
                catch (Exception $e){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email']);
                }

                DB::table('99_otp')
                ->where([
                    ['customer_id', $check->customer_id]
                ])
                ->delete();

                DB::table('99_otp')
                ->insert([
                    'customer_id' => $check->customer_id,
                    'otp_code' => $otp,
                    'created_date' => date('Y-m-d H:i:s'),
                    'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

                ]);

                return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent'], 200);
            }else{
                $login_type = 'Phone';
            }
        }else{
            $check = $this->loginCheckAgent($request->username, $request->username);

            if($request->username == $check->user_email){
                $login_type = 'Email';

                if($check->user_name != ''){
                    $username = $check->user_name;
                }else if($check->user_email != ''){
                    $username = $check->user_email;
                }else if($check->user_phone_number != ''){
                    $username = $check->user_phone_number;
                }

                $otp = mt_rand(100000, 999999);
                try{
                    Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $check)
                    {
                        $message->subject('Login YukMarket');
                        $message->from('support@yukmarket.com', 'YukMarket');
                        $message->to($check->user_email);
                    });
                }
                catch (Exception $e){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email']);
                }

                DB::table('99_otp')
                ->where([
                    ['user_id',  $check->user_id]
                ])
                ->delete();

                DB::table('99_otp')
                ->insert([
                    'user_id' => $check->user_id,
                    'otp_code' => $otp,
                    'created_date' => date('Y-m-d H:i:s'),
                    'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

                ]);

                return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent'], 200);
            }else{
                $login_type = 'Phone';
            }
        }
    }


    // public function sendRequestOTP(Request $request)
    // {
    //     //cek agen atau user
    //     if($request->get('is_agent') == 0){
    //         // $check = DB::table('00_customer AS a')
    //         // // ->leftJoin('00_customer_role AS b', 'b.customer_id', '=', 'a.customer_id')
    //         // ->select('a.customer_id', 'a.customer_name', 'a.customer_email', 'a.customer_phone_number', 'a.customer_password', 'a.active_flag')
    //         // ->where([
    //         //     ['a.customer_email', $request->username],
    //         //     // ['b.role_id', 4]
    //         // ])
    //         // ->orWhere([
    //         //     ['a.customer_phone_number', $request->username],
    //         //     // ['b.role_id', 4]
    //         // ])
    //         // ->first();

    //         $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckcustomer');
    //         $body = array(
    //                     "customer_email" => $request->username,
    //                     "customer_phone_number" => $request->username
    //             );
    //         $request1 = Http::post($url, $body);

    //         if($request->username == $request1['data']['customer_email']){
    //             $login_type = 'Email';

    //             if($request1['data']['customer_name'] != ''){
    //                 $username = $request1['data']['customer_name'];
    //             }else if($request1['data']['customer_email'] != ''){
    //                 $username = $request1['data']['customer_email'];
    //             }else if($request1['data']['customer_phone_number'] != ''){
    //                 $username = $request1['data']['customer_phone_number'];
    //             }

    //             $otp = mt_rand(100000, 999999);
    //             try{
    //                 Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $request1)
    //                 {
    //                     $message->subject('Login YukMarket');
    //                     $message->from('support@yukmarket.com', 'YukMarket');
    //                     $message->to($request1['data']['customer_email']);
    //                 });
    //             }
    //             catch (Exception $e){
    //                 return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email']);
    //             }

    //             DB::table('99_otp')
    //             ->where([
    //                 ['customer_id', $request1['data']['customer_id']]
    //             ])
    //             ->delete();

    //             DB::table('99_otp')
    //             ->insert([
    //                 'customer_id' => $request1['data']['customer_id'],
    //                 'otp_code' => $otp,
    //                 'created_date' => date('Y-m-d H:i:s'),
    //                 'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

    //             ]);

    //             return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent'], 200);
    //         }else{
    //             $login_type = 'Phone';
    //         }
    //     }else{

    //         // $check_agent = DB::table('98_user as a')
    //         // ->select('a.user_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
    //         //     'b.organization_type_id','a.active_flag')
    //         // ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
    //         // ->where('b.organization_type_id', 2)
    //         // ->where([
    //         //     ['a.user_email', $request->username],
                
    //         // ])
    //         // ->orWhere([
    //         //     ['a.user_phone_number', $request->username],
    //         // ])
    //         // ->first();
    //         // dd($check_agent);

    //         $url2 = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/logincheckagent');
    //         $body2= array(
    //             "user_email" => $request->username,
    //             "user_phone_number" => $request->username
    //         );
    //          $request2 = Http::post($url2, $body2);


    //         if($request->username == $request2['data']['user_email']){
    //             $login_type = 'Email';

    //             if($request2['data']['user_name'] != ''){
    //                 $username = $request2['data']['user_name'];
    //             }else if($request2['data']['user_email'] != ''){
    //                 $username = $request2['data']['user_email'];
    //             }else if($request2['data']['user_phone_number'] != ''){
    //                 $username = $check_agent->user_phone_number;
    //             }

    //             $otp = mt_rand(100000, 999999);
    //             try{
    //                 Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $request2)
    //                 {
    //                     $message->subject('Login YukMarket');
    //                     $message->from('support@yukmarket.com', 'YukMarket');
    //                     $message->to($request2['data']['user_email']);
    //                 });
    //             }
    //             catch (Exception $e){
    //                 return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email']);
    //             }

    //             DB::table('99_otp')
    //             ->where([
    //                 ['user_id',  $request2['data']['user_id']]
    //             ])
    //             ->delete();

    //             DB::table('99_otp')
    //             ->insert([
    //                 'user_id' => $request2['data']['user_id'],
    //                 'otp_code' => $otp,
    //                 'created_date' => date('Y-m-d H:i:s'),
    //                 'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

    //             ]);

    //             return response()->json(['isSuccess' => true, 'message' => 'OTP request successfully sent'], 200);
    //         }else{
    //             $login_type = 'Phone';
    //         }
    //     }
    // }

    public function verifyOTP(Request $request)
    {
        $check = DB::table('99_otp')
        ->select('customer_id','user_id','otp_code', 'expired_date')
        ->where([
            ['customer_id', $request->user_id],
            ['otp_code', $request->otp_code]
        ])
        ->orWhere([
            ['user_id', $request->user_id],
            ['otp_code', $request->otp_code]
        ])
        ->first();

        if($check){
            $expired = strtotime(date('Y-m-d H:i:s', strtotime($check->expired_date)));
            $now = strtotime(date('Y-m-d H:i:s'));
            $diff = ($expired - $now) / 60;
            $minleft = substr($diff, 0, 1);

            if($check->otp_code != $request->otp_code){
                return response()->json(['isSuccess' => false, 'message' => 'Kode OTP yang anda masukan salah', 'data' => []], 200);
            }else if($minleft == '-'){
                /*DB::table('99_otp')
                ->where([
                    ['customer_id', $request->user_id],
                    ['otp_code', $request->otp_code]
                ])
                ->delete();*/
                return response()->json(['isSuccess' => false, 'message' => 'Kode OTP sudah kadaluarsa, silahkan kirim ulang OTP', 'data' => []], 200);
            }else{
                if($request->get('is_agent') == 0){
                    $check_fcm = DB::table('00_customer')
                    ->select('customer_id','fcm_token')
                    ->where('customer_id', $request->user_id)
                    ->first();
                        if(!$check_fcm){
                            $add = DB::table('00_customer')
                            ->insert([
                                'fcm_token' => $request->fcm_token
                            ]);

                        }else{
                            DB::table('00_customer')
                            ->where('customer_id', $request->user_id)
                            ->update([
                                'fcm_token' => $request->fcm_token
                            ]);
                        }
                    DB::table('00_customer')
                    ->where('customer_id', $request->user_id)
                    ->update(['remember_token' => $this->generateToken()]);
                }else{
                    $check_fcm = DB::table('98_user')
                    ->select('user_id','fcm_token')
                    ->where('user_id', $request->user_id)
                    ->first();
                        if(!$check_fcm){
                            $add = DB::table('98_user')
                            ->insert([
                                'fcm_token' => $request->fcm_token
                            ]);

                        }else{
                            DB::table('98_user')
                            ->where('user_id', $request->user_id)
                            ->update([
                                'fcm_token' => $request->fcm_token
                            ]);
                        }
                     DB::table('98_user as a')
                    ->where('a.user_id', $request->user_id)
                    ->update(['a.remember_token' => $this->generateToken()]);
                }


                // DB::table('00_customer')
                // ->where('customer_id', $request->user_id)
                // ->update(['remember_token' => $this->generateToken()]);

                // DB::table('99_otp')
                // ->where([
                //     ['customer_id', $request->user_id],
                //     ['otp_code', $request->otp_code]
                // ])
                // ->delete();

                if($request->get('is_agent') == 0){
                    $q = DB::table('00_customer')
                    ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_image AS user_photo_profile', 'remember_token', 'fcm_token')
                    ->where('customer_id', $request->user_id)
                    ->first();
                }else{
                    $q = DB::table('98_user')
                    ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number', 'user_image AS user_photo_profile', 'remember_token', 'fcm_token')
                    ->where('user_id', $request->user_id)
                    ->first();
                }

                return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $q], 200);
            }
        }else{
            return response()->json(['isSuccess' => false, 'message' => 'Kode OTP yang anda masukan salah', 'data' => []], 200);
        }   
    }


    // -----------------------------------------------------------------------Star ENDPOINT

    // public function verifyOTP(Request $request)
    // {
    //     // $check = DB::table('99_otp')
    //     // ->select('customer_id','user_id','otp_code', 'expired_date')
    //     // ->where([
    //     //     ['customer_id', $request->user_id],
    //     //     ['otp_code', $request->otp_code]
    //     // ])
    //     // ->orWhere([
    //     //     ['user_id', $request->user_id],
    //     //     ['otp_code', $request->otp_code]
    //     // ])
    //     // ->first();
    //     $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/getVerifyOTP');
    //     $body = array(
    //                 "otp_code" => $request->otp_code,
    //                 "customer_id" => $request->user_id,
    //                 "user_id" => $request->user_id,
    //         );
    //     $request1 = Http::post($url, $body);

    //     if($request1['data'] != null){
    //         $expired = strtotime(date('Y-m-d H:i:s', strtotime($request1['data']['expired_date'])));
    //         $now = strtotime(date('Y-m-d H:i:s'));
    //         $diff = ($expired - $now) / 60;
    //         $minleft = substr($diff, 0, 1);

    //         if($request1['data']['otp_code'] != $request->otp_code){
    //             return response()->json(['isSuccess' => false, 'message' => 'Kode OTP yang anda masukan salah', 'data' => []], 200);
    //         }else if($minleft == '-'){
    //             /*DB::table('99_otp')
    //             ->where([
    //                 ['customer_id', $request->user_id],
    //                 ['otp_code', $request->otp_code]
    //             ])
    //             ->delete();*/
    //             return response()->json(['isSuccess' => false, 'message' => 'Kode OTP sudah kadaluarsa, silahkan kirim ulang OTP', 'data' => []], 200);
    //         }else{
    //             if($request->get('is_agent') == 0){
    //                 // DB::table('00_customer')
    //                 // ->where('customer_id', $request->user_id)
    //                 // ->update(['remember_token' => $this->generateToken()]);
    //                 $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/verifyUpdateTokenCustomer');
    //                 $body = array(
    //                             "customer_id" => $request->user_id,
    //                             "remember_token" => $this->generateToken()
    //                     );
    //                 $request1 = Http::post($url, $body);
    //             }else{
    //                 //  DB::table('98_user as a')
    //                 // ->where('a.user_id', $request->user_id)
    //                 // ->update(['a.remember_token' => $this->generateToken()]);
    //                 $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/verifyUpdateTokenAgent');
    //                 $body = array(
    //                             "user_id" => $request->user_id,
    //                             "remember_token" => $this->generateToken()
    //                     );
    //                 $request1 = Http::post($url, $body);
    //             }

    //             // DB::table('00_customer')
    //             // ->where('customer_id', $request->user_id)
    //             // ->update(['remember_token' => $this->generateToken()]);

    //             // DB::table('99_otp')
    //             // ->where([
    //             //     ['customer_id', $request->user_id],
    //             //     ['otp_code', $request->otp_code]
    //             // ])
    //             // ->delete();

    //             // $q = DB::table('00_customer')
    //             // // ->leftJoin('00_kelurahan_desa AS b', 'b.kelurahan_desa_id', '=', 'a.kelurahan_desa_id')
    //             // // ->leftJoin('00_kecamatan AS c', 'c.kecamatan_id', '=', 'b.kecamatan_id')
    //             // // ->leftJoin('00_kabupaten_kota AS d', 'd.kabupaten_kota_id', '=', 'c.kabupaten_kota_id')
    //             // // ->leftJoin('00_provinsi AS e', 'e.provinsi_id', '=', 'd.provinsi_id')
    //             // ->select('customer_id AS user_id', 'customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_image AS user_photo_profile', 'remember_token')
    //             // ->where('customer_id', $request->user_id)
    //             // ->first();

    //             if($request->get('is_agent') == 0){
    //                 // $q = DB::table('00_customer')
    //                 // ->select('customer_id AS user_id','customer_name AS user_name', 'customer_email AS user_email', 'customer_phone_number AS user_phone_number', 'customer_password AS user_password', 'customer_image AS user_photo_profile', 'remember_token')
    //                 // ->where('customer_id', $request->user_id)
    //                 // ->first();
    //                 $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/verifyGetCustomerById');
    //                 $body = array(
    //                             "customer_id" => $request->user_id
    //                     );
    //                 $request1 = Http::post($url, $body);

    //             }else{
    //                 // $q = DB::table('98_user')
    //                 // ->select('user_id AS user_id','user_name AS user_name', 'user_email AS user_email', 'user_phone_number AS user_phone_number', 'user_password AS user_password', 'user_image AS user_photo_profile', 'remember_token')
    //                 // ->where('user_id', $request->user_id)
    //                 // ->first();
    //                 $url = app('App\Http\Controllers\MiddlewareEndpointController')->getUrl('/auth/verifyGetAgentById');
    //                 $body = array(
    //                             "user_id" => $request->user_id
    //                     );
    //                 $request1 = Http::post($url, $body);
    //             }

    //             return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => $request1['data']], 200);
    //         }
    //     }else{
    //         return response()->json(['isSuccess' => false, 'message' => 'Kode OTP yang anda masukan salah', 'data' => []], 200);
    //     }   
    // }
    
    // --------------------------------------------------END ENDPOINT

    // USING MIDDLEWARE ENDPOINT START ---------------------------------------------------------------------
    // public function autoRegister(Request $request)
    // {
    //     $check = app('App\Http\Controllers\MiddlewareEndpointController')->checkCustomer($request->username,$request->phone_number);

    //     if($check['data']==null){
    //         $token = $this->generateToken();
    //         $req= array(
    //             "username"=>$request->name,
    //             "phone"=>$request->phone_number,
    //             "email"=>$request->email,
    //             "password"=>"",
    //             "last_login"=>date('Y-m-d H:i:s')
    //         );

    //         $adduser = app('App\Http\Controllers\MiddlewareEndpointController')->addCustomer($req);
    //         $lastCustomer = app('App\Http\Controllers\MiddlewareEndpointController')->getCustomerByEmail($request->email);
            
    //         $updateToken = app('App\Http\Controllers\MiddlewareEndpointController')->updateCustomerSSO($lastCustomer['data']['user_id'],"",$token);            

    //         //TO BE CONTINUE
    //         $add = array(
    //             "customer_id" => $lastCustomer['data']['user_id'],
    //             "address_name" =>  'Alamat Utama',
    //             "address_detail" => $request->address_detail,
    //             "contact_person" => $request->name,
    //             "phone_number" => $request->phone_number,
    //             "gps_point" => "",
    //             "address_info" => $request->address_info,
    //             "kelurahan_desa_id" => $request->kelurahan_desa_id,
    //             "kecamatan_id" => $request->kecamatan_id,
    //             "kabupaten_kota_id" => $request->kabupaten_kota_id,
    //             "provinsi_id" => $request->provinsi_id,
    //             "created_by" => $lastCustomer['data']['user_id'],
    //             "updated_by" => $lastCustomer['data']['user_id'],
    //             "isMain"=>1
    //         );

    //         $addAddressCustomer = app('App\Http\Controllers\MiddlewareEndpointController')->addAddressCustomer($add);            
    //         $lastIdAddress = app('App\Http\Controllers\MiddlewareEndpointController')->getLastAddressId($lastCustomer['data']['user_id']);

    //         if(!$adduser || !$add){
    //             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
    //         }else{
    //             return response()->json(['isSuccess' => true, 'message' => 'Registered Successfully', 'data' => ['user_id' => $lastCustomer['data']['user_id'], 'address_id' => $lastIdAddress['data']['address_id'], 'token' => $token]], 200);
    //         }
    //     }else{
    //         if($check['data']['customer_phone_number'] != '' && $check['data']['customer_phone_number'] != null && $check['data']['customer_phone_number'] == $request->phone_number){
    //             return response()->json(['isSuccess' => false, 'message' => 'Phone number already registered'], 200);
    //         }else if($check['data']['customer_email'] != '' && $check['data']['customer_email'] != null && $check['data']['customer_email'] == $request->email){
    //             return response()->json(['isSuccess' => false, 'message' => 'Email already registered'], 200);
    //         }else{
    //             return response()->json(['isSuccess' => false, 'message' => 'Data tidak Boleh Kosong'], 500);
    //         }
    //     }
    // }
    // USING MIDDLEWARE ENDPOINT END ---------------------------------------------------------------------

    // ORIGINAL START ---------------------------------------------------------------------
    public function autoRegister(Request $request)
    {
            $check = DB::table('00_customer')
            ->select('*')
            ->where('customer_email', $request->email)
            ->orWhere('customer_phone_number', $request->phone_number)
            ->first();

            if(!$check){
                $token = $this->generateToken();
                $adduser = DB::table('00_customer')
                ->insert([
                    'customer_name' => $request->name,
                    'customer_phone_number' => $request->phone_number,
                    'customer_email' => $request->email,
                    'last_login' => date('Y-m-d H:i:s'),
                    'remember_token' => $token,
                    // 'fcm_token' => $request->fcm_token
                ]);

                $id = DB::getPdo()->lastInsertId();

                $add = DB::table('00_address')
                ->insert([
                    'customer_id' => $id,
                    'address_name' => 'Alamat Utama',
                    'address_detail' => $request->address_detail,
                    'contact_person' => $request->name,
                    'phone_number' => $request->phone_number,
                    // 'gps_point' => $request->gps_point,
                    'address_info' => $request->address_info,
                    'kelurahan_desa_id' => $request->kelurahan_desa_id,
                    'kecamatan_id' => $request->kecamatan_id,
                    'kabupaten_kota_id' => $request->kabupaten_kota_id,
                    'provinsi_id' => $request->provinsi_id,
                    'created_by' => $request->user_id,
                    'updated_by' => $request->user_id,
                    'isMain' => 1,
                    'active_flag' => 1
                ]);
                $address_id = DB::getPdo()->lastInsertId();

                if(!$adduser || !$add){
                    return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong'], 403);
                }else{
                    return response()->json(['isSuccess' => true, 'message' => 'Registered Successfully', 'data' => ['user_id' => $id, 'address_id' => $address_id, 'token' => $token]], 200);
                }
            }else{
                if($check->customer_phone_number != '' && $check->customer_phone_number != null && $check->customer_phone_number == $request->phone_number){
                    return response()->json(['isSuccess' => false, 'message' => 'Phone number already registered'], 200);
                }else if($check->customer_email != '' && $check->customer_email != null && $check->customer_email == $request->email){
                    return response()->json(['isSuccess' => false, 'message' => 'Email already registered'], 200);
                }else{
                    return response()->json(['isSuccess' => false, 'message' => 'Lah Error, Ohiya data nya ga diisi'], 500);
                }
            }
    }


    public function loginCheckCustomer($email, $phone){
        $check = DB::table('00_customer')
            ->select('customer_id', 'customer_name', 'customer_email', 'customer_phone_number', 'customer_password', 'fcm_token', 'active_flag')
            ->where('customer_email', $email)
            ->orWhere('customer_phone_number', $phone)
            ->first();
        return $check;
    }


    public function loginCheckAgent($email, $phone){
        $check = DB::table('98_user AS a')
            ->leftJoin('00_organization AS b', 'b.organization_id', '=', 'a.organization_id')
            ->select('a.user_id', 'a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password', 'b.organization_type_id', 'b.organization_name', 'b.organization_desc', 'a.active_flag')
            ->where('b.organization_type_id', '=', '2')
            ->whereRaw('a.user_email = "'.$email.'" OR a.user_phone_number = "'.$phone.'"')
            ->first();
        return $check;
    }

    // ORIGINAL START ---------------------------------------------------------------------

    // public function login_lama(Request $request)
    // {
    //     if($request->username == ''){
    //         return response()->json(['isSuccess' => false, 'message' => 'Username can`t be empty'], 400);
    //     }else if($request->password == ''){
    //         return response()->json(['isSuccess' => false, 'message' => 'Password can`t be empty'], 400);
    //     }else{
    //         $check = DB::table('00_customer AS a')
    //         // ->leftJoin('00_customer_role AS b', 'b.customer_id', '=', 'a.customer_id')
    //         ->select('a.customer_id', 'a.customer_name', 'a.customer_email', 'a.customer_phone_number', 'a.customer_password','fcm_token','a.active_flag')
    //         ->where([
    //             ['a.customer_email', $request->username],
    //             // ['b.role_id', 4]
    //         ])
    //         ->orWhere([
    //             ['a.customer_phone_number', $request->username],
    //             // ['b.role_id', 4]
    //         ])
    //         ->first();
    //         // dd($check);

    //         $check_agent = DB::table('98_user as a')
    //         ->select('a.user_id','a.user_name', 'a.user_email', 'a.user_phone_number', 'a.user_password',
    //             'b.organization_type_id','b.organization_name', 'b.organization_desc'
    //     )
    //         ->leftJoin('00_organization as b', 'a.organization_id', '=', 'b.organization_id')
    //         ->where('b.organization_type_id', 2)
    //         ->where([
    //             ['a.user_email', $request->username],
                
    //         ])
    //         ->orWhere([
    //             ['a.user_phone_number', $request->username],
    //         ])
    //         ->first();
    //         // dd($check_agent);

    //         if(!$check){
    //             return response()->json(['isSuccess' => false, 'message' => 'User not found or Invalid Email'], 200);
    //         } else {
    //             // $verifypass = password_verify($request->password, $check->customer_password);
    //             if(!password_verify($request->password, $check->customer_password) || $check->customer_password == null || $check->customer_password == ''){
    //                 return response()->json(['isSuccess' => false, 'message' => 'Password don`t match'], 200);
    //             }else{
    //                 if($check->active_flag != 1){
    //                     return response()->json(['isSuccess' => false, 'message' => 'User Inactivated'], 200);
    //                 }else{
    //                     if($request->username == $check->customer_email){
    //                         $login_type = 'Email';

    //                         if($check->customer_name != ''){
    //                             $username = $check->customer_name;
    //                         }else if($check->customer_email != ''){
    //                             $username = $check->customer_email;
    //                         }else if($check->customer_phone_number != ''){
    //                             $username = $check->customer_phone_number;
    //                         }

    //                         $otp = mt_rand(100000, 999999);
    //                         try{
    //                             Mail::send('emaillogin', ['nama' => $username, 'otp' => $otp], function ($message) use ($request, $check)
    //                             {
    //                                 $message->subject('Login YukMarket');
    //                                 $message->from('support@yukmarket.com', 'YukMarket');
    //                                 $message->to($check->customer_email);
    //                             });
    //                         }
    //                         catch (Exception $e){
    //                             return response()->json(['isSuccess' => false, 'message' => 'Whoops, Something went wrong when sending otp to email'], 403);
    //                         }
    //                         DB::table('99_otp')
    //                         ->insert([
    //                             'customer_id' => $check->customer_id,
    //                             'otp_code' => $otp,
    //                             'created_date' => date('Y-m-d H:i:s'),
    //                             'expired_date' => date('Y-m-d H:i:s', strtotime('+5 minutes'))

    //                         ]);
    //                     }else{
    //                         $login_type = 'Phone';
    //                     }

    //                     return response()->json(['isSuccess' => true, 'message' => 'Login Success', 'data' => [
    //                         'user_id' => $check->customer_id,
    //                         'login_type' => $login_type,
    //                         'username' => $request->username
    //                     ]], 200);
    //                 }
    //             }
    //         }
    //     }
    // }
}
