<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductWarehouseController extends Controller
{
	//-----INTEGRATION TOKOPEDIA START
    public function checkProductWarehouse($prodId,$warehouseId){
    	$check = DB::table('00_product_warehouse')
    	->select('product_warehouse_id','prod_id','warehouse_id')
    	->where([
    		['prod_id',$prodId],
    		['warehouse_id',$warehouseId]
    	])
    	->first();
    	return $check;
    }
    public function addProductWarehouse($prodId,$warehouseId,$activeFlag){
    	DB::table('00_product_warehouse')
    	->insert([
    		'prod_id'=>$prodId,
    		'warehouse_id'=>$warehouseId,
            'active_flag'=>$activeFlag
    	]);
    }
    //-----INTEGRATION TOKOPEDIA END
}
