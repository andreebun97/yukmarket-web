<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PreparationController extends Controller
{
    public function addOrUpdatePreparation($orderId,$ecomStatus,$condition){
    	$this->validateOrderStatusEcommerce($orderId,$ecomStatus,$condition);
    }
    public function checkPreparation($orderId){
    	return DB::table('10_preparation')
    	->select('preparation_status_id')
    	->where('order_id',$orderId)
    	->orderBy('preparation_status_id','DESC')
    	->distinct()
    	->limit(1)
    	->first();
    }
    public function validateOrderStatusEcommerce($orderId,$ecomStatus,$condition){
		if($ecomStatus==220){
			$preparationStatus=1;	
		}elseif($ecomStatus>220&&$ecomStatus<600&&$ecomStatus!=550){
			$preparationStatus=4;	
		}elseif($ecomStatus>=600){
			$preparationStatus=5;	
		}else{
			echo "order reject";
			return null;
		}
		$schema=array(
			'order_id'=>$orderId,
			'preparation_status_id'=>$preparationStatus	
		);
		if($condition=='update'){
			$currentStatusPreparation=$this->checkPreparation($orderId);
			if($preparationStatus!=$currentStatusPreparation->preparation_status_id){
				if($preparationStatus==4){
					for($i=2;$i<=$preparationStatus;$i++){
						$schema['preparation_status_id']=$i;
						$this->addPreparation($schema);	
					}
				}else{
					$this->addPreparation($schema);	
				}
			}
		}elseif($condition=='insert'&&$preparationStatus==4){
			for($i=1;$i<=$preparationStatus;$i++){
				$schema['preparation_status_id']=$i;
				$this->addPreparation($schema);	
			}
		}else{
			$this->addPreparation($schema);
		}
    }
    public function addPreparation($data){
    	DB::table('10_preparation')
    	->insert($data);
    }
}
