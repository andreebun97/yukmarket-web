<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintStatus extends Model
{
    protected $table = '00_issue_status';
    
    protected $primaryKey = 'issue_status_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_status_name', 'active_flag', 'created_by', 'updated_by'
    ];
}
