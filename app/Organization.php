<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = '00_organization';
    
    protected $primaryKey = 'organization_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_name', 'organization_type_id', 'price_type_id', 'organization_desc', 'organization_code', 'rating', 'total_order', 'organization_logo', 'active_flag', 'created_by', 'updated_by'
    ];
}
