<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintSolution extends Model
{
    protected $table = '00_issue_solution';
    
    protected $primaryKey = 'issue_solution_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'issue_solution_name', 'description', 'active_flag', 'created_by', 'updated_by'
    ];
}
