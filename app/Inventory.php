<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = '00_inventory';
    
    protected $primaryKey = 'inventory_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id', 'stock', 'inventory_desc','active_flag','created_by','updated_by'
    ];
}
