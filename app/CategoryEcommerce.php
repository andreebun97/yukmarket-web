<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryEcommerce extends Model
{
    //
    protected $table = '00_category_per_ecommerce';

    protected $primaryKey = 'category_ecommerce_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'organization_id', 'organization_name', 'category_name', 'category_id', 'category_name_ecommerce', 'created_by', 'updated_by'
    ];
}
