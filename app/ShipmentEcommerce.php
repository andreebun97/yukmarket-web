<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentEcommerce extends Model
{
    protected $table = '00_shipment_method_ecommerce';
    
    protected $primaryKey = 'shipment_method_ecom_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'shipment_method_name', 'organization_id', 'shipment_logo', 'shipment_method_desc', 'active_flag', 'created_by', 'updated_by', 'weight', 'dim_length', 'dim_width', 'dim_height'
    // ];
}
