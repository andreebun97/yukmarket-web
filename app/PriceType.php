<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceType extends Model
{
    // use Notifiable;

    protected $table = '00_price_type';
    
    protected $primaryKey = 'price_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price_type_name', 'price_type_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
