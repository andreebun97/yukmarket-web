<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = '00_payment_method';
    
    protected $primaryKey = 'payment_method_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_method_name', 'payment_logo', 'payment_method_desc', 'admin_fee', 'admin_fee_percentage', 'active_flag', 'created_by', 'updated_by'
    ];
}
