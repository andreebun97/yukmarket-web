<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = '00_kecamatan';
    
    protected $primaryKey = 'kecamatan_id';
}
