<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyTerm extends Model
{
    protected $table = '98_privacy_term';
    
    protected $primaryKey = 'id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'key', 'value', 'created_by', 'created_date', 'updated_by', 'updated_date'
    ];
}