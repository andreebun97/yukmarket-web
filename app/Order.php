<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = '10_order';
    
    protected $primaryKey = 'order_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_code', 'cart_id', 'order_date', 'buyer_user_id', 'seller_user_id', 'payment_method_id', 'shipment_method_id', 'destination_address', 'order_status_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
