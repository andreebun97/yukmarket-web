<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappingCoverage extends Model
{
    protected $table = '00_mapping_coverage';
    
    protected $primaryKey = 'mapping_coverage_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id', 'provinsi_id', 'kabupaten_kota_id', 'kecamatan_id', 'kelurahan_desa_id', 'active_flag','created_by','updated_by'
    ];
}
