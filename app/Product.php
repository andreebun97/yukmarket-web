<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // use Notifiable;

    protected $table = '00_product';
    
    protected $primaryKey = 'prod_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_code', 'prod_name', 'brand_id', 'prod_desc', 'preorder_flag', 'prod_price', 'prod_image', 'uom_id', 'stock', 'active_flag', 'created_by', 'updated_by'
    ];
}
