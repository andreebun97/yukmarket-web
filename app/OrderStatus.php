<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = '00_order_status';
    
    protected $primaryKey = 'order_status_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_status_name', 'order_status_active_logo', 'order_status_inactive_logo', 'order_status_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
