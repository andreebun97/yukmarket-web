<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingDurability extends Model
{
    protected $table = '00_shipping_durability';
    
    protected $primaryKey = 'shipping_durability_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipping_durability_name', 'active_flag', 'created_by', 'updated_by'
    ];
}
