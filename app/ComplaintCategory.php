<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintCategory extends Model
{
    protected $table = '00_issue_category';
    
    protected $primaryKey = 'issue_category_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_category_name', 'description', 'active_flag', 'created_by', 'updated_by'
    ];
}
