<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = '00_kelurahan_desa';
    
    protected $primaryKey = 'kelurahan_desa_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kelurahan_desa_name', 'kecamatan_id', 'kode_pos', 'active_flag', 'updated_by', 'updated_date'
    ];
}
