<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Helpers\Currency;
use App\Helpers\UomConvert;
use App\Organization;
use App\StockKeepingUnit;
use App\Product;
use App\Category;
use App\CategoryEcommerce;
use App\ProductEcommerce;
use App\Brand;
use App\UOM;
use App\Tag;
use App\ProductTag;
use App\ProductCategory;
use App\Warehouse;
use App\ProductWarehouse;
use Session;

class PushData{
    public function pushEcommerce($arr){

        $result = new \stdClass();
        try {
            $tokopedia_store = Organization::where('organization_type_id',6)->where('parent_organization_id',28)->where('active_flag',1)->first();
            $arr_tokopedia = array();
            $arr_ecommerce = array();
            foreach ($arr as $key => $value) {

                $tokopedia_etalase = isset($value->tokopedia_etalase) ? (int)$value->tokopedia_etalase : null ;
                $etalase =(Object)array('id' => $tokopedia_etalase);
                $tokopedia_category_id = $value->tokopedia_category_id;
                $product_weight = $this->getBeratProductTokped($tokopedia_category_id);
                $product_weight = array_filter($product_weight, function($value){
                    return $value['variant_id'] == 1;
                });
                $product_weight = array_values($product_weight);
                $product_weight_unit = $product_weight[0]['units'][0]['values'];
                $tokopedia_unit_value = $product_weight_unit;
                

                $prod_parent = DB::table("00_product AS a")
                            ->where('a.prod_id', $key);
                $data_parent = $prod_parent->first();
                $prod_name_implode = explode("-",$data_parent->prod_name);
                $prodName = count($prod_name_implode) > 1 ? $prod_name_implode[0] : $data_parent->prod_name ;
                $description = $data_parent->prod_desc;
                $preorder=(Object)['duration'=>0,'is_active'=>false,'time_unit'=>'DAY'];
                $price = $value->product_price;
                $sku = $data_parent->prod_code;
                $bruto = $data_parent->bruto * 1000;

                $data_stock = DB::table("00_inventory AS a")
                            ->selectRaw(" SUM((a.stock - (CASE WHEN a.booking_qty IS NULL THEN 0 ELSE a.booking_qty END) - (CASE WHEN a.goods_in_transit IS NULL THEN 0 ELSE a.goods_in_transit END) - (CASE WHEN a.goods_arrived IS NULL THEN 0 ELSE a.goods_arrived END))) AS stock ")
                            ->where('a.prod_id', $key)
                            ->where('a.organization_id', 1)
                            ->first();
                if (isset($data_stock)) {
                    $all_stock = isset($data_stock->stock) ? $data_stock->stock : 0;
                    $stock = $all_stock > 0 ? $all_stock : 1;
                } else {
                    $stock = 1;
                }

                $prod_varian = DB::table("00_product AS a")
                            ->where('a.variant_id', $key);

                $data_variant = $prod_parent->union($prod_varian)->get();

                
                $product_variants = array();
                $product_combinations = array();
                $product_pictures = array();
                $variant_prod_ecom = array();
                $picture = isset($data_variant[0]->prod_image) ? url('/'.$data_variant[0]->prod_image) : 'http://yukmarket.id/img/noresult-image-bw.png';
                array_push($product_pictures, (Object)array(
                    "file_path" => $picture
                ));
                foreach ($data_variant as $key_variant => $variant) {
                    $is_primary = isset($variant->variant_id) ? true : false;

                    $get_price = DB::table('00_product_warehouse')
                        ->selectRaw(" MAX(00_product_warehouse.prod_price) as prod_price ")
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->where('00_product_warehouse.prod_id', '=', $variant->prod_id)
                        ->where('00_warehouse.warehouse_type_id', '=', 2)
                        ->first();
                    $product_price = isset($get_price) ? $get_price->prod_price : $variant->prod_price;

                    $data_stock_variant = DB::table("00_inventory AS a")
                            ->selectRaw(" SUM((a.stock - (CASE WHEN a.booking_qty IS NULL THEN 0 ELSE a.booking_qty END) - (CASE WHEN a.goods_in_transit IS NULL THEN 0 ELSE a.goods_in_transit END) - (CASE WHEN a.goods_arrived IS NULL THEN 0 ELSE a.goods_arrived END))) AS stock ")
                            ->where('a.prod_id', $key)
                            ->where('a.organization_id', 1)
                            ->first();
                    if (isset($data_stock_variant)) {
                        $all_stock_variant = isset($data_stock_variant->stock) ? $data_stock_variant->stock : 0;
                        $stock_variant = $all_stock_variant > 0 ? $all_stock_variant : 1;
                    } else {
                        $stock_variant = 1;
                    }


                    array_push($product_combinations, (Object)array(
                        "combination" => array($key_variant),
                        "is_primary" => $is_primary,
                        "pictures" => array(
                            ["file_path" => $picture]
                        ),
                        "price" => (int)(isset($product_price) ? $product_price : $price),
                        "sku" => $sku,
                        "status" => "LIMITED",
                        "stock" => (int)$stock_variant
                    ));


                    // $product_weight = $this->getBeratProductTokped($tokopedia_category_id);
                    // $product_weight = array_filter($product_weight, function($value){
                    //     return $value['variant_id'] == 1;
                    // });
                    $varian_name_implode = explode("-",$variant->prod_name);
                    $varian_name = count($varian_name_implode) > 1 ? $varian_name_implode[1] : $variant->prod_name ;

                    array_push($product_variants, (Object)array(
                        "hex_code" => "",
                        "unit_value_id" => $tokopedia_unit_value[$key_variant]['value_id'],
                        "value" => $varian_name
                    ));

                    $data_prod_ecom = (object)array(
                                'ecommerce_id' => 28,
                                'category_id' => (int)$tokopedia_category_id,
                                'organization_id' => $tokopedia_store['organization_id'],
                                'prod_id' => $variant->prod_id,
                                // 'prod_ecom_code' => $prod_ecom_code,
                                'prod_ecom_name' => $varian_name,
                                'description' => $variant->prod_desc,
                                'preorder_flag' => 'N',
                                'price' => $product_price,
                                'bruto' => $bruto,
                                'etalase_id' => $tokopedia_etalase
                            );
                    array_push($variant_prod_ecom, $data_prod_ecom);
                }
                if (count($arr_ecommerce) <= 0) {
                    $arr_ecommerce[$sku] = $variant_prod_ecom;
                } else {
                    if (!isset($arr_ecommerce[$sku])){
                        $arr_ecommerce[$sku] = $variant_prod_ecom;
                    }
                }
                $product_variants = array((Object)array(
                    "id" => 1,
                    "options" => $product_variants,
                    "unit_id" => 0
                ));
                $variant = (Object)array(
                                "products" => $product_combinations,
                                "selection" => $product_variants,
                            );
                $data_tokopedia = (Object)array(
                    // 'id' => null,
                    'category_id'=>(int)$tokopedia_category_id,
                    'condition'=>'NEW', //-------------- string option: NEW, USED
                    'description'=>$description,
                    'etalase'=>$etalase,
                    'is_free_return'=>false, //-----------boolean option: true, false
                    'is_must_insurance'=>false, //------------boolean option: true, false
                    'min_order'=>1,
                    'name'=>$prodName,
                    'pictures'=> $product_pictures,
                    'preorder'=>$preorder,
                    'price'=>$price, //---------------------parent price, jika ada variant maka tidak berlaku
                    'price_currency'=>"IDR",
                    'sku'=> $sku,
                    'status'=>'LIMITED',
                    'stock'=>(int)$stock,
                    'variant'=>$variant,
                    // 'videos'=>$videosArray,
                    'weight'=>(int)$bruto,
                    'weight_unit'=>"GR",
                    // 'wholesale'=>$wholesaleArray
                );
                array_push($arr_tokopedia, $data_tokopedia);
            }
            $credential=$this->getGlobalParameter('user_token_integrator');
            $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
            try {
                $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
                $getAuth = explode(" ",$getToken['data']['token']);
                $token = array_pop($getAuth);
            } catch (ConnectionException $e) {
                $token=null;
            }

            $shopConfig = DB::table('99_global_parameter')
                            ->where('global_parameter_name','tokped_shop')
                            ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
                            ->first();
            $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
            $shopId=$dbShopId->organization_code;
            $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
            $obj=['products'=>$arr_tokopedia];
            // echo "<pre>";
            // echo json_encode($obj, JSON_PRETTY_PRINT);
            // echo "</pre>";

            $pushProduct=Http::withToken($token)->timeout(3)->retry(2, 100)->post($url,$obj);
            $statusPushProduct=json_decode($pushProduct->body());

            $tokopedia_error_list = array();
            $result = (Object)array();
            if (isset($statusPushProduct)) {
                $upload_id = $statusPushProduct->data->upload_id;
                $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$upload_id;
                $status = "loading";
                $product_success = array();
                do{
                    sleep(6);
                    $getUploadedProductStatus = Http::withToken($token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
                    $getUploadedProductResponse = json_decode($getUploadedProductStatus->body());
                    $product_success_rows = 0;
                    if ($getUploadedProductResponse->data->failed_rows > 0) {
                        $status = 'failed';
                        $result->status = 500;
                        $result->error_rows = $getUploadedProductResponse->data->failed_rows;
                        $result->error = $getUploadedProductResponse->data->failed_rows_data;
                        $result->success_rows = $getUploadedProductResponse->data->success_rows;
                        $result->success_rows_data = $getUploadedProductResponse->data->success_rows_data;
                    } else {
                        if ($getUploadedProductResponse->data->success_rows > 0) {
                            $status = 'success';
                            $result->status = 200;
                            $result->error_rows = $getUploadedProductResponse->data->failed_rows;
                            $result->error = $getUploadedProductResponse->data->failed_rows_data;
                            $result->success_rows = $getUploadedProductResponse->data->success_rows;
                            $result->success_rows_data = $getUploadedProductResponse->data->success_rows_data;
                        } elseif($getUploadedProductResponse->data->unprocessed_rows > 0){
                            $status = "loading";
                            $result->status = 500;
                            array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');
                        }
                    }
                }while($status == "loading");
                $result->status_tokopedia = $status;
                if ($status == 'success') { //JIKA STATUS SUCCESS INSERT KE PRODUCT ECOMMERCE
                    try {
                        $product_success = $result->success_rows_data;
                        foreach ($product_success as $prod_ecom_code) {
                            $prod_ecom_id = $prod_ecom_code->product_id;
                            $parent = $this->getSingleProduct($prod_ecom_id,$token);
                            $ecom_parent_sku = $parent['other']['sku'];
                            $ecom_parent_name = $parent['basic']['name'];
                            $ecom_parent_desc = $parent['basic']['shortDesc'];
                            $ecom_variant_id = isset($parent['variant']['childrenID']) ? $parent['variant']['childrenID'] : array();
                            $arr_variant_ecom = $arr_ecommerce[$ecom_parent_sku];

                            $product_ecommerce_parent = ProductEcommerce::where('prod_ecom_code',$prod_ecom_id)->first();
                            $data_parent_insert = array(
                                'ecommerce_id' => 28,
                                'category_id' => (int)$arr_variant_ecom[0]->category_id,
                                'organization_id' => $arr_variant_ecom[0]->organization_id,
                                'prod_id' => $arr_variant_ecom[0]->prod_id,
                                'prod_ecom_code' => $prod_ecom_id,
                                'prod_ecom_name' => $ecom_parent_name,
                                'description' => $ecom_parent_desc,
                                'preorder_flag' => 'N',
                                'price' => $arr_variant_ecom[0]->price,
                                'bruto' => $arr_variant_ecom[0]->bruto,
                                'etalase_id' => $arr_variant_ecom[0]->etalase_id
                            );
                            if (isset($product_ecommerce_parent) or $product_ecommerce_parent == null) {
                                $insert_product_ecom = ProductEcommerce::insertGetId($data_parent_insert);
                            } else {
                                $insert_product_ecom = ProductEcommerce::where('prod_ecom_code', $prod_ecom_id)->update($data_parent_insert);
                            }
                            
                            if (isset($arr_variant_ecom)) {
                                if (count($ecom_variant_id) == count($arr_variant_ecom)) {
                                    foreach ($arr_variant_ecom as $key_ecom => $variant_ecom) {
                                        $data_variant_insert = array(
                                            'ecommerce_id' => 28,
                                            'category_id' => (int)$variant_ecom->category_id,
                                            'organization_id' => $variant_ecom->organization_id,
                                            'prod_id' => $variant_ecom->prod_id,
                                            'prod_ecom_code' => $ecom_variant_id[$key_ecom],
                                            'prod_ecom_name' => $ecom_parent_name.' - '.$variant_ecom->prod_ecom_name,
                                            'description' => $variant_ecom->description,
                                            'preorder_flag' => 'N',
                                            'price' => $variant_ecom->price,
                                            'bruto' => $variant_ecom->bruto,
                                            'etalase_id' => $variant_ecom->etalase_id
                                        );
                                        $product_ecommerce_variant = ProductEcommerce::where('prod_ecom_code',$ecom_variant_id[$key_ecom])->orderBy('created_date','ASC')->first();
                                        if (isset($product_ecommerce_variant)  or $product_ecommerce_variant == null) {
                                            ProductEcommerce::insert($data_variant_insert);
                                        } else {
                                            ProductEcommerce::where('prod_ecom_code', $ecom_variant_id[$key_ecom])->update($data_variant_insert);
                                        }
                                    }
                                } else {
                                    $result->status = 500;
                                    array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
                                }
                            } else {
                                $result->status = 500;
                                array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
                            }
                        }
                    } catch (\Exception $e) {
                        $result->status = 500;
                        $result->error_message = $e->getMessage();
                        array_push($tokopedia_error_list, 'Ups, Something Went Wrong !!!');
                    }
                } else{ //JIKA STATUS FAILED DELETE PRODUCT YANG SUDAH TERINSERT KE TOKOPEDIA
                    $result->status = 500;
                    array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');
                    // $product_success_rows = $getUploadedProductResponse->data->success_rows;
                    // $product_success = $getUploadedProductResponse->data->success_rows_data;
                    // if ($product_success_rows > 0) {
                    //     // DELETE PRODUCT DISINI
                    //     foreach ($product_success as $prod_ecom_code) {
                    //         $prod_ecom_id = $prod_ecom_code->product_id;
                    //         $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/deleteProduct?shopId='.$shopId.'&productId='.$prod_ecom_id;
                    //         $delete_prod_ecom = Http::withToken($token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
                    //     }
                    //     $result->status = 500;
                    //     array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
                    // }
                }
            } else {
                $result->status = 500;
            }
        } catch (\Exception $e) {
            $result->status = 500;
            $result->error_message = $e->getMessage();
        }
        return $result;

    }

    public function updateEcommerce($arr){

        $result = new \stdClass();
        try {
            $tokopedia_store = Organization::where('organization_type_id',6)->where('parent_organization_id',28)->where('active_flag',1)->first();
            $arr_tokopedia = array();
            $arr_ecommerce = array();
            $result->status = 200;
            foreach ($arr as $key => $value) {

                $tokopedia_etalase = isset($value->tokopedia_etalase) ? (int)$value->tokopedia_etalase : null ;
                $etalase =(Object)array('id' => $tokopedia_etalase);
                $tokopedia_category_id = $value->tokopedia_category_id;

                $product_weight = $this->getBeratProductTokped($tokopedia_category_id);
                $product_weight = array_filter($product_weight, function($value){
                    return $value['variant_id'] == 1;
                });
                $product_weight = array_values($product_weight);
                $product_weight_unit = $product_weight[0]['units'][0]['values'];
                $tokopedia_unit_value = $product_weight_unit;
                

                $prod_parent = DB::table("00_product AS a")
                            ->where('a.prod_id', $key);
                $data_parent = $prod_parent->first();
                $prod_name_implode = explode("-",$data_parent->prod_name);
                $prodName = count($prod_name_implode) > 1 ? $prod_name_implode[0] : $data_parent->prod_name ;
                $description = $data_parent->prod_desc;
                $preorder=(Object)['duration'=>0,'is_active'=>false,'time_unit'=>'DAY'];
                $price = $value->product_price;
                $sku = $data_parent->prod_code;
                $bruto = $data_parent->bruto * 1000;

                $data_stock = DB::table("00_inventory AS a")
                            ->selectRaw(" SUM((a.stock - (CASE WHEN a.booking_qty IS NULL THEN 0 ELSE a.booking_qty END) - (CASE WHEN a.goods_in_transit IS NULL THEN 0 ELSE a.goods_in_transit END) - (CASE WHEN a.goods_arrived IS NULL THEN 0 ELSE a.goods_arrived END))) AS stock ")
                            ->where('a.prod_id', $key)
                            ->where('a.organization_id', 1)
                            ->first();
                if (isset($data_stock)) {
                    $all_stock = isset($data_stock->stock) ? $data_stock->stock : 0;
                    $stock = $all_stock > 0 ? $all_stock : 1;
                } else {
                    $stock = 1;
                }

                $prod_varian = DB::table("00_product AS a")
                            ->where('a.variant_id', $key);

                $data_variant = $prod_parent->union($prod_varian)->get();

                
                $product_variants = array();
                $product_combinations = array();
                $product_pictures = array();
                $variant_prod_ecom = array();

                $picture = isset($data_variant[0]->prod_image) ? url('/'.$data_variant[0]->prod_image) : 'http://yukmarket.id/img/noresult-image-bw.png';
                array_push($product_pictures, (Object)array(
                    "file_path" => $picture
                ));
                foreach ($data_variant as $key_variant => $variant) {
                    $is_primary = isset($variant->variant_id) ? true : false;

                    $get_price = DB::table('00_product_warehouse')
                        ->selectRaw(" MAX(00_product_warehouse.prod_price) as prod_price ")
                        ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                        ->where('00_product_warehouse.prod_id', '=', $variant->prod_id)
                        ->where('00_warehouse.warehouse_type_id', '=', 2)
                        ->first();
                    $product_price = isset($get_price) ? $get_price->prod_price : $variant->prod_price;

                    $data_stock_variant = DB::table("00_inventory AS a")
                            ->selectRaw(" SUM((a.stock - (CASE WHEN a.booking_qty IS NULL THEN 0 ELSE a.booking_qty END) - (CASE WHEN a.goods_in_transit IS NULL THEN 0 ELSE a.goods_in_transit END) - (CASE WHEN a.goods_arrived IS NULL THEN 0 ELSE a.goods_arrived END))) AS stock ")
                            ->where('a.prod_id', $key)
                            ->where('a.organization_id', 1)
                            ->first();
                    if (isset($data_stock_variant)) {
                        $all_stock_variant = isset($data_stock_variant->stock) ? $data_stock_variant->stock : 0;
                        $stock_variant = $all_stock_variant > 0 ? $all_stock_variant : 1;
                    } else {
                        $stock_variant = 1;
                    }


                    array_push($product_combinations, (Object)array(
                        "combination" => array($key_variant),
                        "is_primary" => $is_primary,
                        "pictures" => array(
                            ["file_path" => $picture]
                        ),
                        "price" => (int)(isset($product_price) ? $product_price : $price),
                        "sku" => $sku,
                        "status" => "LIMITED",
                        "stock" => (int)$stock_variant
                    ));


                    // $product_weight = $this->getBeratProductTokped($tokopedia_category_id);
                    // $product_weight = array_filter($product_weight, function($value){
                    //     return $value['variant_id'] == 1;
                    // });
                    $varian_name_implode = explode("-",$variant->prod_name);
                    $varian_name = count($varian_name_implode) > 1 ? $varian_name_implode[1] : $variant->prod_name ;

                    array_push($product_variants, (Object)array(
                        "hex_code" => "",
                        "unit_value_id" => $tokopedia_unit_value[$key_variant]['value_id'],
                        "value" => trim($varian_name)
                    ));

                    $data_prod_ecom = (object)array(
                                'ecommerce_id' => 28,
                                'category_id' => (int)$tokopedia_category_id,
                                'organization_id' => $tokopedia_store['organization_id'],
                                'prod_id' => $variant->prod_id,
                                // 'prod_ecom_code' => $prod_ecom_code,
                                'prod_ecom_name' => $varian_name,
                                'description' => $variant->prod_desc,
                                'preorder_flag' => 'N',
                                'price' => $product_price,
                                'bruto' => $bruto,
                                'etalase_id' => $tokopedia_etalase
                            );
                    array_push($variant_prod_ecom, $data_prod_ecom);
                }
                if (count($arr_ecommerce) <= 0) {
                    $arr_ecommerce[$sku] = $variant_prod_ecom;
                } else {
                    if (!isset($arr_ecommerce[$sku])){
                        $arr_ecommerce[$sku] = $variant_prod_ecom;
                    }
                }
                $product_variants = array((Object)array(
                    "id" => 1,
                    "options" => $product_variants,
                    "unit_id" => 0
                ));
                $variant = (Object)array(
                                "products" => $product_combinations,
                                "selection" => $product_variants,
                            );

                $prod_eccom = DB::table("00_product_ecommerce AS a")
                            ->where('a.prod_id', $key)
                            ->first();
                if (isset($prod_eccom)) {
	                $data_tokopedia = (Object)array(
	                    'id' => $prod_eccom->prod_ecom_code,
	                    'category_id'=>(int)$tokopedia_category_id,
	                    'condition'=>'NEW', //-------------- string option: NEW, USED
	                    'description'=>$description,
	                    'etalase'=>$etalase,
	                    'is_free_return'=>false, //-----------boolean option: true, false
	                    'is_must_insurance'=>false, //------------boolean option: true, false
	                    'min_order'=>1,
	                    'name'=>trim($prodName),
	                    'pictures'=> $product_pictures,
	                    'preorder'=>$preorder,
	                    'price'=>$price, //---------------------parent price, jika ada variant maka tidak berlaku
	                    'price_currency'=>"IDR",
	                    'sku'=> $sku,
	                    'status'=>'LIMITED',
	                    'stock'=>(int)$stock,
	                    'variant'=>$variant,
	                    // 'videos'=>$videosArray,
	                    'weight'=>(int)$bruto,
	                    'weight_unit'=>"GR",
	                    // 'wholesale'=>$wholesaleArray
	                );
	                array_push($arr_tokopedia, $data_tokopedia);
                } else {
	                $result->status = 500;
	                $result->hidden_message = 'Parent Tidak Ada di Product Ecomm';
                }
            }
            if ($result->status != 500) {
	            $credential=$this->getGlobalParameter('user_token_integrator');
	            $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
	            try {
	                $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
	                $getAuth = explode(" ",$getToken['data']['token']);
	                $token = array_pop($getAuth);
	            } catch (ConnectionException $e) {
	                $token=null;
	            }

	            $shopConfig = DB::table('99_global_parameter')
	                            ->where('global_parameter_name','tokped_shop')
	                            ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
	                            ->first();
	            $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
	            $shopId=$dbShopId->organization_code;
	            $url='https://api.ecomm.inergi.id/tokopedia/updateProductWithVariant?shopId='.$shopId;
	            // $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
	            $obj=['products'=>$arr_tokopedia];
                // dd($obj);
	            // echo "<pre>";
	            // echo json_encode($obj, JSON_PRETTY_PRINT);
	            // echo "</pre>";
                // exit();

	            $pushProduct=Http::withToken($token)->timeout(3)->retry(2, 100)->post($url,$obj);
	            $statusPushProduct=json_decode($pushProduct->body());
	            // dd($statusPushProduct);
	            $tokopedia_error_list = array();
	            $result = (Object)array();
	            if (isset($statusPushProduct)) {
	                $upload_id = $statusPushProduct->data->upload_id;
	                $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$upload_id;
	                $status = "loading";
	                $product_success = array();
	                do{
	                    sleep(6);
	                    $getUploadedProductStatus = Http::withToken($token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
	                    $getUploadedProductResponse = json_decode($getUploadedProductStatus->body());
	                    $product_success_rows = 0;
	                    if ($getUploadedProductResponse->data->failed_rows > 0) {
	                        $status = 'failed';
	                        $result->status = 200;
	                        $result->error_rows = $getUploadedProductResponse->data->failed_rows;
	                        $result->error = $getUploadedProductResponse->data->failed_rows_data;
	                        $result->success_rows = $getUploadedProductResponse->data->success_rows;
	                        $result->success_rows_data = $getUploadedProductResponse->data->success_rows_data;
	                    } else {
	                        if ($getUploadedProductResponse->data->success_rows > 0) {
	                            $status = 'success';
	                            $result->status = 200;
	                            $result->error_rows = $getUploadedProductResponse->data->failed_rows;
	                            $result->error = $getUploadedProductResponse->data->failed_rows_data;
	                            $result->success_rows = $getUploadedProductResponse->data->success_rows;
	                            $result->success_rows_data = $getUploadedProductResponse->data->success_rows_data;
	                        } elseif($getUploadedProductResponse->data->unprocessed_rows > 0){
	                            $status = "loading";
	                            $result->status = 500;
	                            array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');
	                        }
	                    }
	                }while($status == "loading");
	                $result->status_tokopedia = $status;
	                if ($status == 'success') { //JIKA STATUS SUCCESS INSERT KE PRODUCT ECOMMERCE
	                    try {
	                        foreach ($product_success as $prod_ecom_code) {
	                            $prod_ecom_id = $prod_ecom_code->product_id;
	                            $parent = $this->getSingleProduct($prod_ecom_id,$token);
	                            $ecom_parent_sku = $parent['other']['sku'];
	                            $ecom_parent_name = $parent['basic']['name'];
	                            $ecom_parent_desc = $parent['basic']['shortDesc'];
	                            $ecom_variant_id = isset($parent['variant']['childrenID']) ? $parent['variant']['childrenID'] : array();
	                            $arr_variant_ecom = $arr_ecommerce[$ecom_parent_sku];

	                            $product_ecommerce_parent = ProductEcommerce::where('prod_ecom_code',$prod_ecom_id)->first();
	                            $data_parent_insert = array(
	                                'ecommerce_id' => 28,
	                                'category_id' => (int)$arr_variant_ecom[0]->category_id,
	                                'organization_id' => $arr_variant_ecom[0]->organization_id,
	                                'prod_id' => $arr_variant_ecom[0]->prod_id,
	                                'prod_ecom_code' => $prod_ecom_id,
	                                'prod_ecom_name' => $ecom_parent_name,
	                                'description' => $ecom_parent_desc,
	                                'preorder_flag' => 'N',
	                                'price' => $arr_variant_ecom[0]->price,
	                                'bruto' => $arr_variant_ecom[0]->bruto,
	                                'etalase_id' => $arr_variant_ecom[0]->etalase_id
	                            );
	                            if (isset($product_ecommerce_parent) or $product_ecommerce_parent == null) {
	                                $insert_product_ecom = ProductEcommerce::insertGetId($data_parent_insert);
	                            } else {
	                                $insert_product_ecom = ProductEcommerce::where('prod_ecom_code', $prod_ecom_id)->update($data_parent_insert);
	                            }
	                            
	                            if (isset($arr_variant_ecom)) {
	                                if (count($ecom_variant_id) == count($arr_variant_ecom)) {
	                                    foreach ($arr_variant_ecom as $key_ecom => $variant_ecom) {
	                                        $data_variant_insert = array(
	                                            'ecommerce_id' => 28,
	                                            'category_id' => (int)$variant_ecom->category_id,
	                                            'organization_id' => $variant_ecom->organization_id,
	                                            'prod_id' => $variant_ecom->prod_id,
	                                            'prod_ecom_code' => $ecom_variant_id[$key_ecom],
	                                            'prod_ecom_name' => $ecom_parent_name.' - '.$variant_ecom->prod_ecom_name,
	                                            'description' => $variant_ecom->description,
	                                            'preorder_flag' => 'N',
	                                            'price' => $variant_ecom->price,
	                                            'bruto' => $variant_ecom->bruto,
	                                            'etalase_id' => $variant_ecom->etalase_id
	                                        );
	                                        $product_ecommerce_variant = ProductEcommerce::where('prod_ecom_code',$ecom_variant_id[$key_ecom])->orderBy('created_date','ASC')->first();
	                                        if (isset($product_ecommerce_variant)  or $product_ecommerce_variant == null) {
	                                            ProductEcommerce::insert($data_variant_insert);
	                                        } else {
	                                            ProductEcommerce::where('prod_ecom_code', $ecom_variant_id[$key_ecom])->update($data_variant_insert);
	                                        }
	                                    }
	                                } else {
	                                    $result->status = 500;
	                                    array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
	                                }
	                            } else {
	                                $result->status = 500;
	                                array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
	                            }
	                        }
	                    } catch (\Exception $e) {
	                        $result->status = 500;
	                        $result->error_message = $e->getMessage();
	                        array_push($tokopedia_error_list, 'Ups, Something Went Wrong !!!');
	                    }
	                } else{ //JIKA STATUS FAILED DELETE PRODUCT YANG SUDAH TERINSERT KE TOKOPEDIA
	                    // $product_success_rows = $getUploadedProductResponse->data->success_rows;
	                    // $product_success = $getUploadedProductResponse->data->success_rows_data;
	                    // if ($product_success_rows > 0) {
	                    //     // DELETE PRODUCT DISINI
	                    //     foreach ($product_success as $prod_ecom_code) {
	                    //         $prod_ecom_id = $prod_ecom_code->product_id;
	                    //         $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/deleteProduct?shopId='.$shopId.'&productId='.$prod_ecom_id;
	                    //         $delete_prod_ecom = Http::withToken($token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
	                    //     }
	                    //     $result->status = 500;
	                    //     array_push($tokopedia_error_list, 'Gagal Menambahkan Produk Ke Tokopedia');     
	                    // }
	                }
	            } else { 
	                $result->status = 500;
	                $result->hidden_message = 'Masuk Sini!';
	            }
            }
        } catch (\Exception $e) {
            $result->status = 500;
            $result->error_message = $e->getMessage();
        }
        return $result;

    }

    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }

    public function getSingleProduct($id,$token){
        $url='https://api.ecomm.inergi.id/tokopedia/getSingleProductByProductId?productId='.$id;
        $product=Http::withToken($token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $product['data'][0];
    }

    public function pushToYm($parent_id,$data){
        try {
            $result = new \stdClass();
            $varian_parent_id = null;
            if (isset($data->varian_parent)) {
                $prod_stock = DB::table('00_product')
                            ->select('prod_id')
                            ->where('prod_code', '=', $data->varian_parent)
                            ->first();
                $varian_parent_id = isset($prod_stock) ? $prod_stock->prod_id : null;
            }
            $inserted_variant_obj = [
                'prod_code' => $data->prod_code,
                'prod_name' => $data->prod_name,
                'prod_desc' => $data->prod_desc,
                'uom_id' => $data->satuan_pengukuran,
                'uom_value' => $data->satuan_jual,
                'bruto' => $data->prod_bruto,
                'bruto_uom_id' => 1,
                'active_flag' => 1,
                'parent_prod_id' => $varian_parent_id,
                'stockable_flag' => $data->stockable_flag,
                'category_id' => $data->prod_category,
                'created_by' => Session::get('users')['id'],
                'variant_id' => $parent_id,
            ];

            $product_id = Product::insertGetId($inserted_variant_obj);
            // insert product_tag
            $product_tag_array = array();
            foreach ($data->prod_tag as $id_tag_item) {
                $obj = array(
                    "prod_id" => $product_id,
                    "tag_id" => $id_tag_item,
                    "active_flag" => 1,
                    "created_by" => Session::get('users')['id']
                );
                array_push($product_tag_array, $obj);
            }
            ProductTag::insert($product_tag_array);

            // insert product warehouse
            $warehouse_array = array();
            $price_warehouse = 0;
            foreach ($data->gudang as $gudang_detail) {
                $obj = array(
                    "prod_id" => $product_id,
                    "warehouse_id" => $gudang_detail['warehouse_id'],
                    "prod_price" => $gudang_detail['harga'],
                    "minimum_stock" => $gudang_detail['min_stok'],
                    "active_flag" => 1
                );
                array_push($warehouse_array, $obj);
                if ($gudang_detail['warehouse_type_id'] == 2) {
                    $price_warehouse = ($gudang_detail['harga'] > $price_warehouse) ? $gudang_detail['harga'] : $price_warehouse;
                }
            }
            ProductWarehouse::insert($warehouse_array);

            $get_price = DB::table('00_product_warehouse')
                    ->selectRaw(" MAX(00_product_warehouse.prod_price) as prod_price ")
                    ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                    ->where('00_product_warehouse.prod_id', '=', $product_id)
                    ->where('00_warehouse.warehouse_type_id', '=', 2)
                    ->first();
            $product_price = isset($get_price) ? $get_price->prod_price : $price_warehouse;

            $kategori_tokopedia = $data->tokped_kategori_id;
            $etalase = $data->tokped_etalase_id;

            $initial_id = (isset($parent_id) or $parent_id != null) ? $parent_id : $product_id;
            $search_variant = ProductEcommerce::where('prod_id',$parent_id)->first();
            $data = (object)array(
                'flag_ecom' => isset($search_variant),
                'parent_id' => $initial_id,
                'prod_name' => $data->prod_name,
                'tokped_kategori_id' => $kategori_tokopedia,
                'tokped_etalase_id' => $etalase,
                'harga_tokped' => $product_price,
                // 'product_weight_unit' => $product_weight_unit,
            );
            $result->status = 200;
            $result->data = $data;
            $result->error_message = '';
        } catch (\Exception $e) {
            $result->status = 500;
            $result->data = array();
            $result->error_message = $e->getMessage();
        }
        return $result;
    }

    public function pushDelete($product_success){
        // DELETE PRODUCT DISINI
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);
        } catch (ConnectionException $e) {
            $token=null;
        }

        $shopConfig = DB::table('99_global_parameter')
                        ->where('global_parameter_name','tokped_shop')
                        ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
                        ->first();
        $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
        $shopId=$dbShopId->organization_code;
        $result = new \stdClass();
        try{
	        foreach ($product_success as $prod_ecom_code) {
	            sleep(3);
	            $prod_ecom_id = $prod_ecom_code;
	            $url='https://api.ecomm.inergi.id/tokopedia/deleteProduct?shopId='.$shopId.'&productId='.$prod_ecom_id;
	            $deleteProduct=Http::withToken($token)->timeout(3)->retry(2, 100)->post($url);
	        }
        } catch (\Exception $e) {
            $result->status = 500;
            $result->error_message = $e->getMessage();
        }
        return $result;
    }
    

    public function getBeratProductTokped($category){
        sleep(3);
        $url='https://api.ecomm.inergi.id/tokopedia/getAllVariantsByCategoryId?categoryId='.$category;
        $this->generateToken();
        $berat=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $berat['data'];
    }

    public function generateToken(){
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);
        } catch (ConnectionException $e) {
            $token=null;
        }
        $this->token=$token;
    }
}

?>