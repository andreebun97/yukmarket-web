<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = '10_payment';
    
    protected $primaryKey = 'payment_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_name', 'invoice_no', 'customer_id', 'payment_type_id', 'payment_method_id', 'payment_date', 'invoice_status_id', 'midtrans_transaction_id', 'account_number', 'active_flag', 'updated_by', 'updated_date'
    ];
}
