<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseMutationDetail extends Model
{
    protected $table = '10_warehouse_mutation_detail';
    
    protected $primaryKey = 'mutation_detail_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mutation_header_id', 'mutation_no', 'prod_id', 'price', 'stock', 'uom_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
