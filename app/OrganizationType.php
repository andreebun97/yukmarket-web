<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    protected $table = '00_organization_type';
    
    protected $primaryKey = 'organization_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization__type_name', 'organization_type_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
