<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockCard extends Model
{
    //
    protected $table = '00_stock_card';
    
    protected $primaryKey = 'stock_card_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stock_card_code', 'organization_id', 'supplier_id', 'warehouse_id', 'prod_id', 'stock', 'stockable_flag', 'transaction_type_id', 'price', 'total_harga', 'transaction_desc'
    ];
}
