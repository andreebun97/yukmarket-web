<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UomConvertion extends Model
{
    //
    protected $table = '00_uom_convertion';
    
    protected $primaryKey = 'uom_convertion_id';

    // const CREATED_AT = 'created_date';
    // const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uom_first_id', 'uom_second_id', 'formula'
    ];
}
