<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Product;
use App\ProductEcommerce;

class ProductVariantImport implements ToCollection
{
    /**
    * @param array $row
    *
    */
    public function collection(Collection $collection)
    {
        // print_r($collection->toArray()[3]);
        $data = $collection->toArray();
        $excel_data = array();
        for ($b=1; $b < count($data); $b++) { 
            # code...
            // echo "<pre>";
            // print_r($data[$b]);
            // exit;
            $id = $data[$b][0];
            $name = $data[$b][1];
            $variant = $data[$b][2] == '\N' || $data[$b][2] == '' ? '' : $data[$b][2];
            $product_detail = ProductEcommerce::join('00_product','00_product.prod_id','=','00_product_ecommerce.prod_id')->where('prod_ecom_code',$id)->first();
            $exist = "NO";
            if($product_detail == null){
                $parent_product_detail = ProductEcommerce::join('00_product','00_product.prod_id','=','00_product_ecommerce.prod_id')->where('prod_ecom_code',$variant)->first();
                $parent_product_id = NULL;
                if($parent_product_detail == null && $variant != ''){
                    $parent_product_id = Product::insertGetId(array(
                        "prod_id" => $variant,
                        "variant_id" => NULL,
                        "active_flag" => 1
                    ));
                    ProductEcommerce::where('prod_ecom_code', $variant)->update([
                        "prod_id" => $parent_product_id,
                        "ecommerce_id" => 28,
                        "organization_id" => 30,
                        "prod_ecom_name" => $name
                    ]);
                }else{
                    $parent_product_id = $parent_product_detail['prod_id'];
                }
                $product_id = Product::insertGetId(array(
                    // "prod_id" => $pid,
                    "prod_name" => $name,
                    "variant_id" => $parent_product_id,
                    "active_flag" => 1
                ));
                ProductEcommerce::where('prod_ecom_code', $id)->update([
                    "prod_id" => $product_id,
                    "ecommerce_id" => 28,
                    "organization_id" => 30,
                    "prod_ecom_name" => $name
                ]);
            }else{
                $exist = "YES";
                $parent_product_detail = ProductEcommerce::join('00_product','00_product.prod_id','=','00_product_ecommerce.prod_id')->where('prod_ecom_code',$variant)->first();
                $parent_product_id = NULL;
                if($parent_product_detail == null && $variant != ''){
                    $parent_product_id = Product::insertGetId(array(
                        "prod_id" => $variant,
                        "variant_id" => NULL,
                        "active_flag" => 1
                    ));
                    ProductEcommerce::where('prod_ecom_code', $variant)->update([
                        "prod_id" => $parent_product_id,
                        "ecommerce_id" => 28,
                        "organization_id" => 30,
                        "prod_ecom_name" => $name
                    ]);
                }else{
                    $parent_product_id = $parent_product_detail['prod_id'];
                }
                $product_id = $product_detail['prod_id'];
                Product::where('prod_id', $product_id)->update(array(
                    // "prod_id" => $id,
                    "prod_name" => $name,
                    "variant_id" => $parent_product_id,
                    "active_flag" => 1,
                    "stockable_flag" => ($parent_product_id == NULL ? 1 : 0)
                ));
                ProductEcommerce::where('prod_ecom_code', $id)->update([
                    "prod_id" => $product_id,
                    "ecommerce_id" => 28,
                    "organization_id" => 30,
                    "prod_ecom_name" => $name
                ]);
            }
            $obj = array(
                "id" => $id,
                "name" => $name,
                "variant" => $variant,
                "exist" => $exist
            );
            array_push($excel_data, $obj);
        }
        // echo "<pre>";
        // array_multisort(array_column($excel_data, 'variant'), SORT_ASC,
        //         array_column($excel_data, 'id'),      SORT_ASC,
        //         $excel_data);
        // print_r($excel_data);
        // exit;
        // echo 'good';
        return $excel_data;
    }
}
