<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class InventoryImport implements ToCollection,WithStartRow,WithCalculatedFormulas
{
    /**
    * @param Collection $collection
    */
    public $data = array();

    public function collection(Collection $collection)
    {
        foreach($collection as $row){
            if($row->filter()->isNotEmpty()){
                //$row->code = 'mantab';
                array_push($this->data, $row);
            }
        }
    }

    public function startRow(): int
    {
        return 2;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
