<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHeader extends Model
{
    //
    protected $table = '10_transaction_header';
    
    protected $primaryKey = 'transaction_header_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_header_code', 'organization_id', 'supplier_id', 'warehouse_id', 'transaction_type_id', 'transaction_desc', 'transaction_type_id', 'transaction_date'
    ];
}
