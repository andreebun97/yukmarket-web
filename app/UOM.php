<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UOM extends Model
{
    //
    protected $table = '00_uom';
    
    protected $primaryKey = 'uom_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uom_name', 'uom_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
