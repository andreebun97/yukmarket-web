<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherType extends Model
{
    // use Notifiable;

    protected $table = '00_voucher_type';
    
    protected $primaryKey = 'voucher_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'voucher_type_name', 'voucher_type_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
