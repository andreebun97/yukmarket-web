<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $table = '00_issue';
    
    protected $primaryKey = 'issue_id';

    const CREATED_AT = 'issue_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_id', 'issue_category_id', 'approved_solution_id', 'ticketing_num', 'customer_id', 'order_id', 'issue_solution_id', 'issue_notes', 'issue_date', 'issue_status_id', 'active_flag','updated_by'
    ];
}
