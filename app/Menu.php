<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = '98_menu';
    
    protected $primaryKey = 'menu_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_name', 'menu_url', 'menu_icon', 'orderno', 'parent_id', 'menu_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
