<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleType extends Model
{
    protected $table = '98_role_type';
    
    protected $primaryKey = 'role_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_type_name', 'role_type_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}