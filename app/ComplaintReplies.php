<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintReplies extends Model
{
    protected $table = '00_issue_replies';
    
    protected $primaryKey = 'issue_replies_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = [
        'issue_id', 'user_id', 'customer_id', 'response', 'is_read', 'notes', 'active_flag', 'created_by', 'updated_by'
    ];
}
