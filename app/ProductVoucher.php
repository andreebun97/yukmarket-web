<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVoucher extends Model
{
    //
    protected $table = '00_product_voucher';
    
    protected $primaryKey = 'product_voucher_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id', 'voucher_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
