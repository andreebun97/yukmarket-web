<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = '00_customer';
    
    protected $primaryKey = 'customer_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_name', 'customer_email', 'customer_phone_number', 'customer_image', 'active_flag', 'created_by', 'updated_by'
    ];
}
