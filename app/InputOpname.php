<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputOpname extends Model
{
    //
    protected $table = '10_input_opname';
    
    protected $primaryKey = 'input_opname_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_number', 'organization_id', 'prod_id', 'price', 'stock_difference', 'stock', 'rack'
    ];
}
