<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
// use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Order;
use DB;
use Session;
use App\Helpers\Currency;


class TransactionExport implements FromView, WithColumnFormatting, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $request = array();
    
    public function __construct($request){
        $this->request = $request;
    }

    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
        ];
    }
    
    public function styles(Worksheet $sheet){
        return [
            // Style the first row as bold text.
            '1'    => ['font' => ['bold' => true]]
        ];
        // $sheet->getStyle('A1')->applyFromArray(array(
        //     'fill' => array(
        //         // 'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        //         'color' => array('rgb' => 'FF0000')
        //     ),
        //     'font' => array(
        //         'bold' => true
        //     )
        // ));

        // $sheet->getStyle('B1')->applyFromArray(array(
        //     'fill' => array(
        //         // 'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        //         'color' => array('rgb' => 'FF0000')
        //     ),
        //     'font' => array(
        //         'bold' => true
        //     )
        // ));
        // $sheet->setFontFamily('Comic Sans MS');
        // $sheet->setStyle(array(
        //     'font' => array(
        //         'name'      =>  'Calibri',
        //         'size'      =>  20,
        //         'bold'      =>  true
        //     )
        // ));
    }

    public function view(): View
    {
        // print_r($this->request);
        // exit;
        // print_r($request->all());
        // exit;
        $transaction = Order::select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.buyer_user_id',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) as buyer_user_name'),'00_organization.organization_id','00_organization.organization_name','10_order.destination_address','10_order.buyer_name','10_order.buyer_address','00_provinsi.provinsi_id','00_provinsi.provinsi_name','00_address.address_detail','00_kecamatan.kecamatan_id','00_kecamatan.kecamatan_name','00_kabupaten_kota.kabupaten_kota_id','00_kabupaten_kota.kabupaten_kota_name','00_kelurahan_desa.kelurahan_desa_id','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','10_order.grand_total','10_order.online_flag',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.shipment_price')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->where(function($query){
            $query->where('10_order.online_flag',1)->orWhereNotNull('10_order.parent_order_id');
        });
        if(Session::get('users')['warehouse_id'] != null){
            $warehouse_id = Session::get('users')['warehouse_id'];
            $transaction->where('warehouse_id', $warehouse_id);
        }
        if(isset($this->request['organization'])){
            if($this->request['organization'] != "all_agent"){
                $transaction->where('10_order.agent_user_id', $this->request['organization']);
            }
        }
        if(isset($this->request['order_status'])){
            $order_status = $this->request['order_status'];
            // if($request->input('arrival') != ""){
            //     $order_status = 9;
            // }
            $transaction->where('10_order.order_status_id', $order_status);
        }
        if(isset($this->request['start_date'])){
            $transaction->where(DB::raw('CAST(10_order.order_date AS DATE)'),">=", DB::raw('CAST("'.date('Y-m-d',strtotime($this->request['start_date'])).'" AS DATE)'));
        }
        if(isset($this->request['end_date'])){
            $transaction->where(DB::raw('CAST(10_order.order_date AS DATE)'),"<=", DB::raw('CAST("'.date('Y-m-d',strtotime($this->request['end_date'])).'" AS DATE)'));
        }
        $transaction = $transaction->get();

        return view('admin::payment.export',[
            'transaction' => $transaction
        ]);
    }
}
