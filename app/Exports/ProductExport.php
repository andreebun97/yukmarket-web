<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Support\Facades\DB;

class ProductExport implements FromView, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Product::all();
    }

    public function styles(Worksheet $sheet){
        return [
            // Style the first row as bold text.
            '1'    => ['font' => ['bold' => true]]
        ];
        // $sheet->getStyle('A1')->applyFromArray(array(
        //     'fill' => array(
        //         // 'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        //         'color' => array('rgb' => 'FF0000')
        //     ),
        //     'font' => array(
        //         'bold' => true
        //     )
        // ));

        // $sheet->getStyle('B1')->applyFromArray(array(
        //     'fill' => array(
        //         // 'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        //         'color' => array('rgb' => 'FF0000')
        //     ),
        //     'font' => array(
        //         'bold' => true
        //     )
        // ));
        // $sheet->setFontFamily('Comic Sans MS');
        // $sheet->setStyle(array(
        //     'font' => array(
        //         'name'      =>  'Calibri',
        //         'size'      =>  20,
        //         'bold'      =>  true
        //     )
        // ));
    }

     public function view(): View

    {
    	// $product = Product::where('active_flag', '=', 1)
        $product = DB::table('00_product as a')
            // ->select('a.prod_name',
            //         'a.prod_code', 
            //         DB::raw('(CASE WHEN a.variant_id IS NOT NULL THEN (SELECT 00_product.prod_code from 00_product WHERE 00_product.prod_id = a.variant_id) ELSE NULL END) AS parent_prod_code'), 
            //         DB::raw('(CASE WHEN a.parent_prod_id IS NOT NULL THEN (SELECT 00_product.prod_code from 00_product WHERE 00_product.prod_id = a.parent_prod_id) ELSE NULL END) AS prod_stok_code'), 'a.uom_value',
            //         DB::raw('GROUP_CONCAT( DISTINCT (SELECT tag_name FROM 00_tag AS `tag` WHERE f.tag_id = f.tag_id) SEPARATOR ', ') AS tag_name'), 
            //         'a.uom_value',
            //         'a.bruto',
            //         'b.uom_name',
            //         'a.shipping_durability_max',
            //         'c.shipping_durability_name',
            //         'a.minimum_order',
            //         'd.brand_name',
            //         'e.category_name',
            //         'a.prod_desc',
            //         'a.prod_id'
            // )
            ->selectRaw("
                        a.prod_name, 
                        a.prod_code,
                        (CASE WHEN a.variant_id IS NOT NULL THEN (SELECT 00_product.prod_code from 00_product WHERE 00_product.prod_id = a.variant_id) ELSE NULL END) AS parent_prod_code,
                        (CASE WHEN a.parent_prod_id IS NOT NULL THEN (SELECT 00_product.prod_code from 00_product WHERE 00_product.prod_id = a.parent_prod_id) ELSE NULL END) AS prod_stok_code,
                        (CASE WHEN tag.tag_name IS NOT NULL THEN GROUP_CONCAT(tag.tag_name) ELSE NULL END) AS tag_name,
                        (CASE WHEN wh.warehouse_id IS NOT NULL THEN GROUP_CONCAT(wh.warehouse_name) ELSE NULL END ) AS wh_name,
                        (CASE WHEN wh.warehouse_id IS NOT NULL THEN GROUP_CONCAT(g.prod_price) ELSE NULL END ) AS price,
                        (CASE WHEN wh.warehouse_id IS NOT NULL THEN GROUP_CONCAT(g.minimum_stock) ELSE NULL END ) AS stock_min,
                        a.uom_value,
                        a.bruto,
                        b.uom_name,
                        a.shipping_durability_max,
                        c.shipping_durability_name,
                        a.minimum_order,
                        d.brand_name,
                        e.category_name,
                        h.category_name as category_tokopedia,
                        a.prod_desc,
                        a.stockable_flag,
                        i.etalase_name,
                        a.prod_id
                    ")
            ->distinct()
            ->leftJoin('00_uom as b', 'a.uom_id', '=', 'b.uom_id')
            ->leftJoin('00_shipping_durability as c', 'a.shipping_durability_id', '=', 'c.shipping_durability_id')
            ->leftJoin('00_brand as d', 'a.brand_id', '=', 'd.brand_id')
            ->leftJoin('00_category as e', 'a.category_id', '=', 'e.category_id')
            ->leftJoin('00_product_tag as f', 'f.prod_id', '=', 'a.prod_id')
            ->leftjoin('00_tag as tag', 'tag.tag_id', '=', 'f.tag_id')
            ->leftJoin('00_product_warehouse as g', 'g.prod_id', '=', 'a.prod_id')
            ->leftJoin('00_warehouse as wh', 'wh.warehouse_id', '=', 'g.warehouse_id')
            ->leftJoin('00_category_per_ecommerce as h', 'a.category_id', '=', 'h.category_id')
            ->leftJoin('00_product_ecommerce as i', 'a.prod_id', '=', 'i.prod_id')
            ->where('a.active_flag', 1)
            ->orderByRaw(DB::raw('(CASE WHEN a.variant_id IS NULL THEN a.prod_id ELSE a.variant_id END) ASC'))
            // ->where('a.active_flag',1)
            ->groupby('a.prod_id')
            // ->whereNotNull('a.parent_prod_id')
            // ->toSql();
        	->get();
        return view('admin::products.export', [
            'product' => $product
        ]);

    }

}