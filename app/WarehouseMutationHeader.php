<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseMutationHeader extends Model
{
    protected $table = '10_warehouse_mutation_header';
    
    protected $primaryKey = 'mutation_header_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'warehouse_id', 'parent_warehouse_id', 'mutation_date', 'organization_id', 'user_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
