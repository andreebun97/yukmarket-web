<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $table = '00_kabupaten_kota';
    
    protected $primaryKey = 'kabupaten_kota_id';
}
