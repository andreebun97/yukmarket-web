<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintAttachment extends Model
{
    protected $table = '00_issue_attachment';
    
    protected $primaryKey = 'attachment_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_id', 'attachment', 'active_flag', 'created_by', 'updated_by'
    ];
}
