<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductEcommerce extends Model
{
    protected $table = '00_product_ecommerce';
    
    protected $primaryKey = 'prod_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_ecommerce_id', 'prod_id', 'ecommerce_id', 'prod_id', 'preorder_flag', 'prod_ecommerce_code', 'prod_ecommerce_name'
    ];
}
