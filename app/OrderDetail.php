<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = '10_order_detail';
    
    protected $primaryKey = 'order_detail_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'prod_id', 'quantity', 'price', 'active_flag', 'created_by', 'updated_by'
    ];
}
