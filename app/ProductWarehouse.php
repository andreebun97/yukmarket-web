<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWarehouse extends Model
{
    protected $table = '00_product_warehouse';
    
    protected $primaryKey = 'product_warehouse_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_time';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id', 'warehouse_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
