<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    //
    protected $table = '00_product_tag';
    
    protected $primaryKey = 'prod_category_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id', 'tag_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
