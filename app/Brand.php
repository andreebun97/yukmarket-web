<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $table = '00_brand';
    
    protected $primaryKey = 'brand_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_name', 'brand_code', 'brand_desc', 'brand_image', 'brand_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
