<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintDetail extends Model
{
    protected $table = '00_issue_detail';
    
    protected $primaryKey = 'issue_detail_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_id', 'prod_id','quantity','issue_detail_notes', 'issue_detail_date', 'active_flag', 'created_by', 'updated_by'
    ];
}
