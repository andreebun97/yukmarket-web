<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    // use Notifiable;

    protected $table = '00_warehouse';
    
    protected $primaryKey = 'warehouse_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'warehouse_name', 'warehouse_desc', 'contact_person', 'phone_number', 'parent_warehouse_id', 'address_id', 'active_flag', 'created_by', 'updated_by'
    ];
}
