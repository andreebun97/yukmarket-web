<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalSettings extends Model
{
    protected $table = '99_global_parameter';
    
    protected $primaryKey = 'global_parameter_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'global_parameter_name', 'global_parameter_value', 'global_parameter_desc', 'active_flag', 'created_by', 'updated_by'
    ];
}
