<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = '10_cart';
    
    protected $primaryKey = 'cart_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'buyer_user_id', 'prod_id', 'quantity', 'amount', 'total_amount', 'active_flag', 'created_by', 'updated_by'
    ];
}
