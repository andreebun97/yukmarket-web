<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = '98_role_menu';
    
    protected $primaryKey = 'role_menu_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','menu_id', 'allow_create', 'allow_update', 'allow_delete', 'allow_print', 'allow_export', 'active_flag', 'created_by', 'updated_by'
    ];
}
