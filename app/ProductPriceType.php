<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPriceType extends Model
{
    // use Notifiable;

    protected $table = '00_product_price_type';
    
    protected $primaryKey = 'product_price_type_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id', 'price_type_id', 'price', 'item_code', 'min_purchased_qty', 'uom_promo', 'discount_type', 'rate', 'applicable_on', 'active_flag', 'created_by', 'updated_by'
    ];
}
