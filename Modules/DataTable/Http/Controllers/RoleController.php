<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use DataTables;
use App\Role;
use App;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function get()
    {
        $role = Role::all();
        $role_array = array();
        for ($b=0; $b < count($role); $b++) { 
            $obj = array(
                'role_id' => $role[$b]['role_id'],
                'role_name' => $role[$b]['role_name'],
                'role_description' => $role[$b]['role_desc'],
                'active_flag' => $role[$b]['active_flag'],
                'action' => '<ul class="ico-block"><li><a href="'.route("admin/role/edit","?id=".$role[$b]['role_id']).'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a> '.($role[$b]['active_flag'] == 0 ? '<a href="'.route("admin/role/activate","?id=".$role[$b]['role_id']).'" data-toggle="tooltip"  title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></a></li></ul>' : '<a href="'.route("admin/role/deactivate","?id=".$role[$b]['role_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></a></li></ul>')
            );
            array_push($role_array,$obj);
        }
        return DataTables::of($role_array)->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
