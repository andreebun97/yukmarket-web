<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\UOM;
use App;
use DataTables;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function get()
    {
        $uom = UOM::whereIn('active_flag',[0,1])->get();
        $uom_array = array();
        for ($b=0; $b < count($uom); $b++) { 
            $obj = array(
                'uom_id' => $uom[$b]['uom_id'],
                'uom_name' => $uom[$b]['uom_name'],
                'uom_description' => $uom[$b]['uom_desc'],
                'action' => '<ul class="ico-block"><li><a href="'.route('admin/uom/edit','?id='.$uom[$b]['uom_id']).'" data-toggle="tooltip" data-container="body" title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>  '.($uom[$b]['active_flag'] == 0 ? '<a href="'.route('admin/uom/activate','?id='.$uom[$b]['uom_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route('admin/uom/deactivate','?id='.$uom[$b]['uom_id']).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li"></ul>')
            );
            array_push($uom_array, $obj);
        }
        return DataTables::of($uom_array)->make();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
