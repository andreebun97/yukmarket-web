<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DataTables;
use App\Inventory;
use App;
use DB;
use Session;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function get()
    {
        $inventory_array = array();
        $inventory = Inventory::select('00_inventory.inventory_id','00_product.prod_code','00_product.prod_id','00_product.prod_name','00_inventory.stock')->join('00_product','00_product.prod_id','=','00_inventory.prod_id')->where('00_inventory.active_flag',1)->get();

        for ($b=0; $b < count($inventory); $b++) { 
            $obj = array(
                'inventory_id' => $inventory[$b]['inventory_id'],
                'product_code' => $inventory[$b]['prod_code'],
                'product_id' => $inventory[$b]['prod_id'],
                'product_name' => $inventory[$b]['prod_name'],
                'stock' => $inventory[$b]['stock'],
                'action' => "<a href='".route('admin/inventory/edit','?id='.$inventory[$b]['inventory_id'])."'><i class='fa fa-eye'></i></a>"
            );
            array_push($inventory_array, $obj);
        }
        return DataTables::of($inventory_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getInventoryReport(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->has('start') ? $request->get('start') : 0;
        $length = $request->has('length') ? $request->get('length') : 5;
        $search = $request->has('search') ? $request->get('search')['value'] : '';

        $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
        $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';
        if ($order_column == 0) {
            $column_name = "prod.prod_name";
            $order_type = "DESC";
        } elseif ($order_column == 1) {
            $column_name = "opening.saldo_awal";
        } elseif ($order_column == 2) {
            $column_name = "ig.barang_masuk";
        } elseif ($order_column == 3) {
            $column_name = "og.barang_keluar";
        } elseif ($order_column == 4) {
            $column_name = "saldo_akhir";
        } else {
            $column_name = $request->get('columns')[$order_column]['data'];
        }

        $array = array();
        $organization_id = Session::get('users')['organization_id'];
        if(isset($request->category)){
            $category_query = "and prod.category_id = cast($request->category as signed)";
        }else{
            $category_query ="";
        }
        $warehouse_id = Session::get('users')['warehouse_id'];
        if(isset($request->inventory_warehouse)){
            $warehouse_query = "and warehouse_id = $request->inventory_warehouse";
        }else{
            if($warehouse_id){
                $warehouse_query = "and warehouse_id = $warehouse_id";
            }else{
                $warehouse_query = "";
            }
        }
        $transaction_start_date = isset($request->transaction_start_date) ? date('Y-m-d', strtotime($request->transaction_start_date)) : date('Y-m-d 00:00:00');
        $transaction_end_date = isset($request->transaction_end_date) ? date('Y-m-d H:m:i', strtotime($request->transaction_end_date . ' 23:59:59')) : date('Y-m-d 23:59:59');
        $total = DB::table('00_product as prod')
                ->selectRaw("prod.prod_id,
                                prod.prod_name,
                                opening.saldo_awal,
                                ig.barang_masuk,
                                og.barang_keluar,
				                (IFNULL(opening.saldo_awal,0) + IFNULL(ig.barang_masuk,0) + IFNULL(og.barang_keluar,0)) as saldo_akhir")
                ->leftJoin(DB::raw("(SELECT SUM(stock) AS'saldo_awal',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 1 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) opening")
                                    ,'prod.prod_id', '=', 'opening.prod_id')
                ->leftJoin(DB::raw("(SELECT SUM(stock) AS'barang_masuk',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 2 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) ig")
                                    ,'prod.prod_id', '=', 'ig.prod_id')
                ->leftJoin(DB::raw("(SELECT SUM(stock) AS'barang_keluar',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 3 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) og")
                                    ,'prod.prod_id', '=', 'og.prod_id')
                ->whereRaw("(saldo_awal IS NOT NULL OR barang_masuk IS NOT NULL OR barang_keluar IS NOT NULL) $category_query")
                ->orderBy("$column_name", "$order_type")
                ->get();

        $query = DB::table('00_product as prod')
                    ->selectRaw("prod.prod_id,
                                    prod.prod_name,
                                    IFNULL(opening.saldo_awal,0) as saldo_awal,
                                    IFNULL(ig.barang_masuk,0) as barang_masuk,
                                    IFNULL(og.barang_keluar,0) as barang_keluar,
                                    (IFNULL(opening.saldo_awal,0) + IFNULL(ig.barang_masuk,0) + IFNULL(og.barang_keluar,0)) as saldo_akhir")
                    ->leftJoin(DB::raw("(SELECT SUM(stock) AS'saldo_awal',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 1 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) opening")
                                        ,'prod.prod_id', '=', 'opening.prod_id')
                    ->leftJoin(DB::raw("(SELECT SUM(stock) AS'barang_masuk',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 2 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) ig")
                                        ,'prod.prod_id', '=', 'ig.prod_id')
                    ->leftJoin(DB::raw("(SELECT SUM(stock) AS'barang_keluar',prod_id FROM 00_stock_card where organization_id = $organization_id and transaction_type_id = 3 and created_date between '$transaction_start_date' and '$transaction_end_date' $warehouse_query GROUP BY prod_id) og")
                                        ,'prod.prod_id', '=', 'og.prod_id')
                    ->whereRaw("(saldo_awal IS NOT NULL OR barang_masuk IS NOT NULL OR barang_keluar IS NOT NULL) $category_query")
                    ->limit($length)
                    ->offset($start)
                    ->orderBy("$column_name", "$order_type")
                    ->get();

        $no = $start + 1;
        foreach ($query as $key => $value):
            $data_object = (object) array(
                        'no' => $no++,
                        'product_name' => $value->prod_name,
                        'balance_opening' => $value->saldo_awal,
                        'balance_in' => $value->barang_masuk,
                        'balance_out' => $value->barang_keluar,
                        'balance_end' => $value->saldo_akhir,
            );
            array_push($array, $data_object);
        endforeach;

        //$total_page = count($array);


        $data = array(
            'draw' => $draw,
            'recordsTotal' => count($total),
            'recordsFiltered' => count($total),
            'data' => $array,
        );

        echo json_encode($data);
    }
}
