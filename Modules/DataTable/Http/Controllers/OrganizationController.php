<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App;
use App\Organization;
use DataTables;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function get()
    {
        $organization_array = array();
        $organization = Organization::select('00_organization.organization_id','00_organization.organization_name','00_organization_type.organization_type_id','00_organization_type.organization_type_name','00_organization.active_flag','00_price_type.price_type_id','00_price_type.price_type_name','00_organization.organization_desc','00_organization.organization_code','00_organization.rating','00_organization.total_order')->leftJoin('00_organization_type','00_organization_type.organization_type_id','=','00_organization.organization_type_id')->leftJoin('00_price_type','00_price_type.price_type_id','=','00_organization.price_type_id')->get();
        for ($b=0; $b < count($organization); $b++) { 
            $obj = array(
                "organization_id" => $organization[$b]['organization_id'],
                "organization_name" => $organization[$b]['organization_name'],
                "organization_type_id" => $organization[$b]['organization_type_id'],
                "organization_type_name" => $organization[$b]['organization_type_name'],
                "price_type_id" => $organization[$b]['price_type_id'],
                "price_type_name" => $organization[$b]['price_type_name'],
                "active_flag" => $organization[$b]['active_flag'],
                "organization_description" => $organization[$b]['organization_desc'],
                "organization_code" => $organization[$b]['organization_code'],
                "rating" => $organization[$b]['rating'].'/10',
                "total_order" => $organization[$b]['total_order'],
                "action" => "<ul class='ico-block'><li><a href='".route("admin/organization/edit","?id=".$organization[$b]["organization_id"])."' data-toggle='tooltip'  data-container='body' title='Ubah' ><i class='fa fa-pencil'></i></a>".($organization[$b]["active_flag"] == 0 ? "<a href='".route("admin/organization/activate","?id=".$organization[$b]["organization_id"])."' data-toggle='tooltip' data-container='body' title='".(__('page.inactive'))."'><i class='fa fa-check-circle color-gray ico-size-td'></i></a></li></ul>" : "<a href='".route("admin/organization/deactivate","?id=".$organization[$b]["organization_id"])."' data-toggle='tooltip'  data-container='body' title='".(__('page.active'))."'><i class='fa fa-check-circle color-green ico-size-td'></i></a></li></ul>")
            );

            array_push($organization_array, $obj);
        }
        return DataTables::of($organization_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
