<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Category;
use Session;
use DataTables;
use App;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    public function get()
    {
        $category = Category::orderBy('category_name','ASC')->whereIn('active_flag',[0,1])->get();
        $category_array = array();
        for ($b=0; $b < count($category); $b++) { 
            # code...
            $obj = array(
                'category_id' => $category[$b]['category_id'],
                'category_name' => $category[$b]['category_name'],
                'category_desc' => $category[$b]['category_desc'],
                'active_flag' => $category[$b]['active_flag'],
                'action' => '<ul class="ico-block"><li><a class="loader-trigger" href="'.route("admin/category/edit","?category=".$category[$b]['category_id']).'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a> '.($category[$b]['active_flag'] == 1 ? '<a href="'.route("admin/category/deactivate","?category=".$category[$b]['category_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li></ul>' : '<a href="'.route("admin/category/activate","?category=".$category[$b]['category_id']).'" data-toggle="tooltip"  title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></li></ul>')
            );
            array_push($category_array,$obj);
        }
        return DataTables::of($category_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        //
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
