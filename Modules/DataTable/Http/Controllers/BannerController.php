<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\Banner;
use App;
use DataTables;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function get()
    {
        $banner = Banner::whereIn('active_flag',[0,1])->get();
        $banner_array = array();
        for ($b=0; $b < count($banner); $b++) { 
            $obj = array(
                'banner_id' => $banner[$b]['banner_id'],
                'banner_name' => $banner[$b]['banner_name'],
                'banner_description' => $banner[$b]['banner_desc'],
                'banner_image' => asset($banner[$b]['banner_image']),
                'action' => '<ul class="ico-block"><li><a href="'.route('admin/banner/edit','?id='.$banner[$b]['banner_id']).'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>  '.($banner[$b]['active_flag'] == 0 ? '<a href="'.route('admin/banner/activate','?id='.$banner[$b]['banner_id']).'" data-toggle="tooltip"  title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route('admin/banner/deactivate','?id='.$banner[$b]['banner_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa  fa-check-circle color-green ico-size-td"></i></a></li></ul>')
            );
            array_push($banner_array, $obj);
        }
        return DataTables::of($banner_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
