<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Complaint;
use Session;
use DataTables;
use App;
use DB;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function get(Request $request)
    {
        $complaint_array = array();
        $issue_status = 1;
        if($request->input('issue_status') != null){
            $issue_status = $request->input('issue_status');
        }
        $complaint = Complaint::select('00_issue.issue_id','00_issue.ticketing_num','00_issue.customer_id','00_issue.order_id','00_issue.issue_date',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.order_code','10_order.order_id',DB::raw('(SELECT 98_user.user_name FROM 98_user where 98_user.user_id = 00_issue.processed_by) AS processed_by'))->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_status_id', $issue_status)->get();
        for ($b=0; $b < count($complaint); $b++) {
            $obj = array(
                'issue_id' => $complaint[$b]['issue_id'],
                'ticketing_num' => $complaint[$b]['ticketing_num'],
                'customer_detail' => $complaint[$b]['customer_name'] . ' ('. $complaint[$b]['customer_phone_number'] . ')',
                'order_id' => $complaint[$b]['order_id'],
                'order_code' => $complaint[$b]['order_code'],
                'issue_status_id' => $complaint[$b]['issue_status_id'],
                'issue_status_name' => $complaint[$b]['issue_status_name'],
                'processed_by' => $complaint[$b]['processed_by'],
                'processed_date' => $complaint[$b]['processed_date'] == null ? null : date('Y-m-d H:i:s', strtotime($complaint[$b]['processed_date'])),
                // 'done_by' => $complaint[$b]['processed_by'],
                // 'handled_date' => $complaint[$b]['handled_date'] == null ? null : date('Y-m-d H:i:s', strtotime($complaint[$b]['handled_date'])),
                'issue_date' => date('Y-m-d H:i:s', strtotime($complaint[$b]['issue_date'])),
                'action' => '<a href="'.route('admin/complaint/detail','?id='.$complaint[$b]['issue_id']).'" " data-toggle="tooltip" data-container="body" title="Detail"><i class="fa fa-eye color-green font-size-18"></i></a>'
            );

            array_push($complaint_array, $obj);
        }
        return DataTables::of($complaint_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
