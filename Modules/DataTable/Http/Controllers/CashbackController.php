<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Cashback;
use App\Customer;
use App\User;
use Session;
use DataTables;
use App;
use App\Helpers\Currency;
use DB;

class CashbackController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;
    
    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        App::setLocale('in');
    }

    public function get(Request $request)
    {
        $cashback_array = array();
        $cashback = Customer::select('00_customer.customer_id','00_customer.customer_name','00_customer.customer_email','00_customer.saldo AS return_amount')->whereIn('00_customer.customer_id',function($query){
            $query->select('10_cashback.customer_id')->from('10_cashback')->where('10_cashback.active_flag',1);
        })->get();
        if($request->input('role') == 'agent'){
            $cashback = User::select('98_user.user_id AS customer_id','98_user.user_name AS customer_name','98_user.user_email AS customer_email','98_user.saldo AS return_amount')->whereIn('98_user.user_id', function($query){
                $query->select('10_cashback.agent_id')->from('10_cashback')->where('10_cashback.active_flag',1);
            })->get();
        }
        for ($b=0; $b < count($cashback); $b++) { 
            $obj = array(
                "customer_id" => $cashback[$b]['customer_id'],
                "customer_name" => $cashback[$b]['customer_name'],
                "customer_email" => $cashback[$b]['customer_email'],
                "total_balance" => $this->currency->convertToCurrency($cashback[$b]['return_amount']),
                "action" => "<a href='".route("admin/cashback/detail","?id=".$cashback[$b]['customer_id'].($request->input('role') == 'agent' ? '&role=agent' : '&role=customer'))."'><i class='fa fa-eye'></i></a>"
            );
            array_push($cashback_array, $obj);
        }
        return DataTables::of($cashback_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
