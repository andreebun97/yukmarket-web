<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Payment;
use App\GlobalSettings;
use App;
use App\Helpers\Currency;
use DataTables;
use DB;
use Session;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct($currency = null){
        App::setLocale('in');
        $currency = new Currency();
        $this->currency = $currency;
    }

    public function payment(Request $request)
    {
        $payment_array = array();
        $payment = Payment::leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_customer','00_customer.customer_id','=','10_payment.customer_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('reckon_flag',0)->where('10_payment.invoice_status_id',2);
        if($request->input('start_date')){
            $payment->where(DB::raw('CAST(10_payment.payment_date AS DATE)'),">=", DB::raw('CAST("'.$request->input('start_date').'" AS DATE)'));
        }
        if($request->input('end_date')){
            $payment->where(DB::raw('CAST(10_payment.payment_date AS DATE)'),"<=", DB::raw('CAST("'.$request->input('end_date').'" AS DATE)'));
        }
        if($request->input('payment_method')){
            $payment->where('10_payment.payment_method_id', $request->input('payment_method'));
        }
        $payment = $payment->get();
        for ($b=0; $b < count($payment); $b++) { 
            $obj = array(
                'payment_id' => $payment[$b]['payment_id'],
                'customer_id' => $payment[$b]['customer_id'],
                'customer_name' => $payment[$b]['customer_name'],
                'invoice_status_name' => $payment[$b]['invoice_status_name'],
                'account_number' => $payment[$b]['account_number'],
                'payment_date' => date('Y-m-d H:i:s',strtotime($payment[$b]['payment_date'])),
                'invoice_num' => $payment[$b]['invoice_no'],
                'grand_total' => $this->currency->convertToCurrency($payment[$b]['grandtotal_payment']),
                'unformatted_grand_total' => $payment[$b]['grandtotal_payment'],
                'payment_method_id' => $payment[$b]['payment_method_id'],
                'payment_method_name' => $payment[$b]['payment_method_name']
            );
            array_push($payment_array, $obj);
        }
        return DataTables::of($payment_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function ecommerce(Request $request)
    {
        $payment_array = array();
        $payment = Payment::select('10_payment.payment_id AS id',DB::raw('CONCAT("Transaksi Penjualan Berhasil - ",CAST(10_payment.invoice_no AS VARCHAR(500))) AS detail'),'10_payment.invoice_no',DB::raw('(10_payment.grandtotal_payment) AS amount'),'10_payment.payment_date','10_payment.payment_id',DB::raw('(SELECT (CASE WHEN SUM(00_voucher_ecommerce_detail.amount) IS NULL THEN 0 ELSE SUM(00_voucher_ecommerce_detail.amount) END) FROM 00_voucher_ecommerce_detail WHERE 00_voucher_ecommerce_detail.voucher_ecom_h_id = 00_voucher_ecommerce_header.voucher_ecom_h_id AND 00_voucher_ecommerce_detail.voucher_type_id = 5) AS insurance_amount'),DB::raw('3 AS `flag`'))->leftJoin('00_voucher_ecommerce_header','00_voucher_ecommerce_header.voucher_ecom_h_id','=','10_payment.voucher_id')->join('10_order','10_order.payment_id','=','10_payment.payment_id')->where('10_order.agent_user_id',28);
        if($request->input('start_date') != null){
            $payment->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $payment->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $voucher = Payment::select('00_voucher_ecommerce_detail.voucher_ecom_d_id AS id',DB::raw('CAST(00_voucher_ecommerce_detail.voucher_name_ecom AS VARCHAR(500)) AS detail'),'10_payment.invoice_no','00_voucher_ecommerce_detail.amount_after_discount AS amount','10_payment.payment_date','10_payment.payment_id',DB::raw('0 AS insurance_amount'),DB::raw('2 AS `flag`'))->join('00_voucher_ecommerce_header','00_voucher_ecommerce_header.voucher_ecom_h_id','=','10_payment.voucher_id')->join('00_voucher_ecommerce_detail','00_voucher_ecommerce_header.voucher_ecom_h_id','=','00_voucher_ecommerce_detail.voucher_ecom_h_id')->whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        });
        if($request->input('start_date') != null){
            $voucher->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $voucher->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $admin_fee = Payment::select('10_payment.payment_id AS id',DB::raw('CONCAT("Pemotongan Biaya Layanan Power Merchant - ",CAST(10_payment.invoice_no AS VARCHAR(500))) AS detail'),'10_payment.invoice_no','10_payment.admin_fee','10_payment.payment_date','10_payment.payment_id',DB::raw('0 AS insurance_amount'),DB::raw('1 AS `flag`'))->join('00_voucher_ecommerce_header','00_voucher_ecommerce_header.voucher_ecom_h_id','=','10_payment.voucher_id')->whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        });
        if($request->input('start_date') != null){
            $admin_fee->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $admin_fee->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $payment->unionAll($voucher)->unionAll($admin_fee)->orderBy('payment_date','ASC')->orderBy('flag','DESC');
        
        $payment = $payment->get()->toArray();
        $tokped_balance = GlobalSettings::where('global_parameter_name','tokopedia_balance')->first();
        // print_r($payment);
        // exit;
        $amount = 0;
        if($tokped_balance != null){
            $amount = $tokped_balance['global_parameter_value'];
        }
        for ($b=0; $b < count($payment); $b++) {
            // if($payment[$b]['flag'] == 1 || $payment[$b]['flag'] == 2){
            //     $amount -= ($payment[$b]['amount'] - $payment[$b]['insurance_amount']);
            // }else{
            //     $amount += ($payment[$b]['amount'] - $payment[$b]['insurance_amount']);
            // }
            $amount += $payment[$b]['amount'];
            $obj = array(
                'id' => $payment[$b]['id'],
                'payment_date' => date('Y-m-d H:i:s', strtotime($payment[$b]['payment_date'])),
                'description' => ($payment[$b]['flag'] == 2) ? ($payment[$b]['detail'] . ' - ' . $payment[$b]['invoice_no']) : $payment[$b]['detail'],
                'invoice_no' => $payment[$b]['invoice_no'],
                'amount' => ($payment[$b]['flag'] == 2 || $payment[$b]['flag'] == 1) ? ("-".($this->currency->convertToCurrency($payment[$b]['amount']))) : ($this->currency->convertToCurrency($payment[$b]['amount'])),
                'flag' => $payment[$b]['flag'],
                'balance' => $this->currency->convertToCurrency($amount)
            );
            array_push($payment_array, $obj);
        }
        return DataTables::of($payment_array)->make(true);
        // echo "<pre>";
        // print_r($payment);
        // exit;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
