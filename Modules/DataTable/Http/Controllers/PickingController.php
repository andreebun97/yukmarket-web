<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Currency;
use Session;
use DataTables;
use App\Order;
use DB;
use App;

class PickingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;

    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        App::setLocale('in');

    }

    public function get(Request $request)
    {
        $order = Order::join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->select('10_order.order_id','10_order.order_code','10_order.voucher_id','10_order_detail.promo_value','10_order.voucher_amount','10_order.pricing_include_tax','10_order.national_income_tax','10_order.is_fixed AS voucher_fix_type','10_order.is_printed_invoice','10_order.is_printed_label','10_order.max_price AS max_provided_voucher_value','10_order.admin_fee','10_order.admin_fee_percentage','10_order.shipment_price',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN NULL ELSE (SELECT 00_organization.organization_name FROM 00_organization JOIN 98_user ON 98_user.organization_id = 00_organization.organization_id WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS organization_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'), '10_order.is_printed_label', '10_order.payment_method_id','10_order.is_agent',DB::raw('(SELECT 10_preparation.updated_date FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1) as preparation_date'),'00_payment_method.payment_method_name', DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.destination_address','00_address.provinsi_id','00_address.kabupaten_kota_id','00_address.kecamatan_id','00_address.kelurahan_desa_id','10_order.order_status_id','00_order_status.order_status_name','10_order.order_date AS purchased_date','00_address.address_name','00_address.contact_person','00_address.phone_number','00_address.address_detail','00_address.address_info','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity * 10_order_detail.price) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity * 10_order_detail.price) + 10_order_detail.promo_value) END) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS total_price'),'10_order.buyer_name','10_order.buyer_address','10_order_ecommerce.order_status_ecom_id',DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1) as picker_name'),DB::raw('(SELECT 10_preparation.preparation_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1) as preparation_id'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->distinct()->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_id DESC LIMIT 1)'),[1])->where('10_order.order_status_id',6)->leftJoin('10_order_ecommerce','10_order_ecommerce.order_id','=','10_order.order_id')->orderBy('10_preparation.preparation_id','ASC');
        if($request->input('warehouse_id') != null && $request->input('warehouse_id') != "all_warehouse"){
            $warehouse_id = $request->input('warehouse_id');
            $order->where('10_order.warehouse_id', $warehouse_id);
        }
        if($request->input('organization')){
            if($request->input('organization') != "all_agent"){
                $order->where('10_order.agent_user_id', $request->input('organization'))->orWhere('10_order.agent_user_id',$request->input('parent_organization'));
            }
        }
        if($request->input('picker') != ""){
            if($request->input('picker') == '0'){
                $order->whereIn('10_order.is_printed_label',[0,1]);
                if($request->input('activity') == 'set_picker'){
                    $order->whereNull(DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'));
                }elseif($request->input('activity') == 'print_shipping_label'){
                    $order->whereNotNull(DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'));
                }
            }elseif($request->input('picker') == '1'){
                $order->whereIn('10_order.is_printed_label',[2]);
            }
        }
        $order = $order->get();
        $transaction_array = array();
        for ($b=0; $b < count($order); $b++) { 
            $total_price = $order[$b]['total_price'];
            $national_income_tax = 0;
            if($order[$b]['pricing_include_tax'] == 0){
                $national_income_tax = ROUND($total_price * $order[$b]['national_income_tax']);
                $total_price = $total_price + $national_income_tax;
            }
            $voucher_amount = 0;
            // if($order[$b]['voucher_id'] != null){
            //     if($order[$b]['voucher_fix_type'] == 'Y'){
            //         $voucher_amount = $order[$b]['voucher_amount'];
            //     }else{
            //         $voucher_amount = $total_price * $order[$b]['voucher_amount']/100;
            //         if($voucher_amount > $order[$b]['max_provided_voucher_value']){
            //             $voucher_amount = $order[$b]['max_provided_voucher_value'];
            //         }
            //     }
            // }
            $shipment_fee = 0;
            if($order[$b]['shipment_price'] != null){
                $shipment_fee = $order[$b]['shipment_price'];
            }
            $all_admin_fee = 0;
            // if($order[$b]['admin_fee'] != null){
            //     $all_admin_fee = $order[$b]['admin_fee'];
            // }
            // if($order[$b]['admin_fee_percentage'] != null){
            //     $all_admin_fee = $total * ($order[$b]['admin_fee_percentage']/100);
            // }
            $total_price = ($total_price - $voucher_amount) + $shipment_fee + $all_admin_fee;
            $obj = array(
                'order_id' => $order[$b]['order_id'],
                'order_code' => $order[$b]['order_code'],
                'customer_name' => ($order[$b]['customer_name'] == null ? $order[$b]['buyer_name'] : ($order[$b]['customer_name'] . ($order[$b]['is_agent'] == 0 ? '' : ' ('.__('role.agent').')'))),
                'customer_email' => $order[$b]['customer_email'],
                'customer_phone_number' => $order[$b]['customer_phone_number'],
                'total_price' => 'Rp. '.$this->currency->convertToCurrency($total_price),
                'order_status_name' => $order[$b]['order_status_name'],
                'purchased_date' => date("Y-m-d H:i:s",strtotime($order[$b]['purchased_date'])),
                'grand_total' => $order[$b]['grand_total'],
                'address_name' => $order[$b]['address_name'],
                'contact_person' => $order[$b]['contact_person'],
                'phone_number' => $order[$b]['phone_number'],
                'address_detail' => $order[$b]['address_detail'],
                'address_info' => $order[$b]['address_info'],
                'province_name' => $order[$b]['provinsi_name'],
                'regency_name' => $order[$b]['kabupaten_kota_name'],
                'district_name' => $order[$b]['kecamatan_name'],
                'village_name' => $order[$b]['kelurahan_desa_name'],
                'postal_code' => $order[$b]['kode_pos'],
                'preparation_date' => date("Y-m-d H:i:s",strtotime($order[$b]['preparation_date'])),
                'picker_name' => $order[$b]['picker_name'] == null ? '-' : $order[$b]['picker_name'],
                'payment_method_name' => $order[$b]['payment_method_name'],
                'action' => '<ul class="ico-block"><li><a class="viewTransactionDetail" href="'.route("admin/picking/form","?id=".$order[$b]['order_id']).'" ><i class="fa fa-eye color-green font-size-18"></i></a>'.($order[$b]['order_status_ecom_id'] != null && $order[$b]['order_status_ecom_id'] == 220 ? '<a href="'.route("admin/picking/acceptOrder","?id=".$order[$b]['order_id']).'" data-toggle="tooltip" title="'.(__('page.accept_order')).'"><i class="fa fa-check-circle"></i></a>' : '').($request->input('picker') == 0 && $request->input('picker') != null && $request->input('activity') != null ? '<input type="checkbox" name="checked_order[]" value="'.($request->input('activity') == 'set_picker' ? $order[$b]['preparation_id'] : $order[$b]['order_id']).'"></li>' : ($request->input('picker') == 1 && $request->input('picker') != null && $request->input('activity') != null ? '<input type="checkbox" name="checked_order[]" value="'.$order[$b]['order_id'].'"></li>' : '</li>')).'</ul>',
            );
            array_push($transaction_array,$obj);
        }
        return DataTables::of($transaction_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
