<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App;
use App\Payment;
use Lang;
use App\Voucher;
use DataTables;
use DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function __construct(){
        App::setLocale('in');
    }

    public function get(Request $request)
    {
        $voucher = Voucher::select('00_vouchers.voucher_id','00_vouchers.voucher_name','00_vouchers.voucher_code','00_vouchers.max_uses','00_vouchers.voucher_desc','00_vouchers.voucher_type_id','00_voucher_type.voucher_type_name','00_vouchers.amount','00_vouchers.max_value_price','00_vouchers.min_price_requirement','00_vouchers.starts_at','00_vouchers.expires_at','00_vouchers.is_fixed','00_vouchers.active_flag',DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN 00_vouchers.starts_at AND 00_vouchers.expires_at OR CURRENT_TIMESTAMP < 00_vouchers.starts_at THEN 0 ELSE 1 END) AS expired'))->join('00_voucher_type','00_voucher_type.voucher_type_id','=','00_vouchers.voucher_type_id');
        if($request->input('type') == 'transaction' || $request->input('type') == null){
            $voucher->whereNotIn('00_vouchers.voucher_id',function($query){
                $query->select('00_product_voucher.voucher_id')->from('00_product_voucher');
            });
        }else{
            $voucher->whereIn('00_vouchers.voucher_id',function($query){
                $query->select('00_product_voucher.voucher_id')->from('00_product_voucher');
            });   
        }
        $voucher = $voucher->get();
        $voucher_array = array();
        for ($b=0; $b < count($voucher); $b++) { 
            $obj = array(
                "voucher_id" => $voucher[$b]['voucher_id'],
                "voucher_code" => $voucher[$b]['voucher_code'],
                "voucher_name" => $voucher[$b]['voucher_name'],
                "max_uses" => $voucher[$b]['max_uses'],
                "voucher_type_id" => $voucher[$b]['voucher_type_id'],
                "voucher_type_name" => $voucher[$b]['voucher_type_name'],
                "amount" => $voucher[$b]['amount'],
                "voucher_active_status" => ($voucher[$b]['active_flag'] == 0 || $voucher[$b]['expired'] == 1 ? __('page.inactive') : __('page.active') ),
                "max_value_price" => $voucher[$b]['max_value_price'],
                "min_price_requirement" => $voucher[$b]['min_price_requirement'],
                "period" => $voucher[$b]['starts_at'] . " - " . $voucher[$b]['expires_at'],
                "is_fixed" => $voucher[$b]['is_fixed'],
                "voucher_description" => $voucher[$b]['voucher_desc'],
                "action" => ($voucher[$b]['expired'] == 1 ? '' : '<ul class="ico-block"><li><a href="'.($request->input('type') == 'transaction' || $request->input('type') == null ? route('admin/voucher/edit','?id='.$voucher[$b]['voucher_id']) : route('admin/product/promo/edit','?id='.$voucher[$b]['voucher_id'])).'" data-toggle="tooltip" title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>  '.($voucher[$b]['active_flag'] == 0 ? '<a href="'.(route('admin/voucher/activate','?id='.$voucher[$b]['voucher_id'])).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route('admin/voucher/deactivate','?id='.$voucher[$b]['voucher_id']).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li"></ul>'))
            );

            array_push($voucher_array,$obj);
        }
        return DataTables::of($voucher_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    var $_columns = array(
        // null,
        // null,
        'voucher_code',
        'voucher_name',
        'voucher_desc'
    );

    private function _root($parameter) {
        // print_r($parameter['column']);
        // exit;
        $column = $parameter['column'] < 2 ? 0 : ($parameter['column'] - 2);
        // print_r($parameter['order']);
        // echo $parameter['order'].'<br>';
        // echo $this->_columns[$column];
        // exit;
        try {
            $columns = $this->_columns;
            $customer = Voucher::select('00_vouchers.voucher_id','00_vouchers.voucher_code','00_vouchers.voucher_name','00_vouchers.voucher_desc')
                ->join('10_payment','10_payment.voucher_id','=','00_vouchers.voucher_id')
                ->join('00_customer', '00_customer.customer_id','=','10_payment.customer_id')
                ->where('10_payment.is_agent','=',0)
                ->groupBy('00_vouchers.voucher_code')
                ->where(function($query) use ($columns, $parameter){
                    for ($k=0; $k < count($columns); $k++) {
                        if($k > 0){
                            $query->orWhere($columns[$k],'like','%'.$parameter['search'].'%');
                        }else{
                            $query->where($columns[$k],'like','%'.$parameter['search'].'%');
                        }
                    }
                });

            $user = Voucher::select('00_vouchers.voucher_id','00_vouchers.voucher_code','00_vouchers.voucher_name','00_vouchers.voucher_desc')
                ->join('10_payment','10_payment.voucher_id','=','00_vouchers.voucher_id')
                ->join('98_user', '98_user.user_id','=','10_payment.customer_id')
                ->where('10_payment.is_agent','=',1)
                ->groupBy('00_vouchers.voucher_code')
                ->where(function($query) use ($columns, $parameter){
                    for ($k=0; $k < count($columns); $k++) {
                        if($k > 0){
                            $query->orWhere($columns[$k],'like','%'.$parameter['search'].'%');
                        }else{
                            $query->where($columns[$k],'like','%'.$parameter['search'].'%');
                        }
                    }
                });

            $sql = $customer->union($user)->orderBy($this->_columns[$column], $parameter['order']);
            return $sql;
        } catch (Exception $ex) {
            echo $ex->getMessage(); exit();
        }
    }

    public function getData($parameter) {
        // print_r($parameter['search']);
        // exit;
        $sql = $this->_root($parameter);
        $sql->limit($parameter['limit'])
            ->offset($parameter['offset']);
        return $sql->get();
    }

    public function getTotal($parameter) {
        $sql = $this->_root($parameter);
        return $sql->count();
    }

    public function shows(Request $request) {
        $parameter = array(
            'offset' => $request->has('start') ? $request->get('start') : 0,
            'limit' => $request->has('length') ? $request->get('length') : 10,
            'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
            'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc',
            'search' => $request->has('search') ? $request->get('search')['value'] : ''
        );
        
        $data = array(
			'draw' => $request->get('draw'),
			'recordsTotal' => $this->getTotal($parameter),
			'recordsFiltered' => $this->getTotal($parameter),
			'data' => $this->getData($parameter)
		);
        return response()->json($data, 200);
    }

    // var $_columnsDetailCustomer = array(
    //     '00_customer.customer_name AS cust_name',
    //     '00_customer.customer_email AS cust_email',
    //     '00_customer.customer_phone_number AS cust_phone',
    //     '10_payment.voucher_amount AS v_amount',
    //     '10_payment.payment_date AS paid_date'
    // );

    var $_columnsDetailUser = array(
        'cust_name',
        'cust_email',
        'cust_phone',
        'v_amount',
        'paid_date'
    );
    private function _rootDetails($parameter) {
        try {

            $column = $parameter['column'] < 1 ? 0 : ($parameter['column'] - 1);

            $customer = Payment::select(
                    '00_customer.customer_name AS cust_name',
                    '00_customer.customer_email AS cust_email',
                    '00_customer.customer_phone_number AS cust_phone',
                    '10_payment.voucher_amount AS v_amount',
                    // '10_payment.voucher_value_type AS v_value_type',
                    '10_payment.payment_date AS paid_date'
                )
                ->join('00_customer', '00_customer.customer_id','=','10_payment.customer_id')
                ->where('10_payment.voucher_id','=',$parameter['voucherId'])
                ->where('10_payment.is_agent','=',0);

            $user = Payment::select(
                    '98_user.user_name AS cust_name',
                    '98_user.user_email AS cust_email',
                    '98_user.user_phone_number AS cust_phone',
                    '10_payment.voucher_amount AS v_amount',
                    // '10_payment.voucher_value_type AS v_value_type',
                    '10_payment.payment_date AS paid_date'
                )
                ->join('98_user', '98_user.user_id','=','10_payment.customer_id')
                ->where('10_payment.voucher_id','=',$parameter['voucherId'])
                ->where('10_payment.is_agent','=',1);

            $sql = $customer->union($user)->orderBy($this->_columnsDetailUser[$column], $parameter['order']);
            return $sql;
        } catch (Exception $ex) {
            echo $ex->getMessage(); exit();
        }
    }

    public function getDataDetail($parameter) {
        // echo '<pre>';
        // dd($parameter['search']);
        // exit;
        $sql = $this->_rootDetails($parameter);
        $sql->limit($parameter['limit'])
            ->offset($parameter['offset']);
        return $sql->get();
    }

    public function getTotalDetail($parameter) {
        $sql = $this->_rootDetails($parameter);
        return $sql->count();
    }

    public function details(Request $request) {
        $parameter = array(
            'offset' => $request->has('start') ? $request->get('start') : 0,
            'limit' => $request->has('length') ? $request->get('length') : 10,
            'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
            'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc',
            'search' => $request->has('search') ? $request->get('search')['value'] : '',
            'voucherId' => $request->input('voucherId')
        );
        
        $data = array(
			'draw' => $request->get('draw'),
			'recordsTotal' => $this->getTotalDetail($parameter),
			'recordsFiltered' => $this->getTotalDetail($parameter),
			'data' => $this->getDataDetail($parameter)
		);
        return response()->json($data, 200);
    }
}
