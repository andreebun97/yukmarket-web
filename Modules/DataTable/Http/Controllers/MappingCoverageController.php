<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DataTables;
use App\MappingCoverage;
use App;
use DB;

class MappingCoverageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function get(Request $request)
    {
        $mapping_coverage_array = array();
        $mapping_coverage = MappingCoverage::select('00_mapping_coverage.mapping_coverage_id','00_mapping_coverage.organization_id','00_organization.organization_name','00_mapping_coverage.provinsi_id','00_provinsi.provinsi_name','00_mapping_coverage.kabupaten_kota_id','00_kabupaten_kota.kabupaten_kota_name','00_mapping_coverage.kecamatan_id','00_kecamatan.kecamatan_name','00_mapping_coverage.kelurahan_desa_id','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_mapping_coverage.active_flag')->leftJoin('00_organization','00_organization.organization_id','=','00_mapping_coverage.organization_id')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_mapping_coverage.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_mapping_coverage.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_mapping_coverage.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_mapping_coverage.kelurahan_desa_id')->whereIn('00_mapping_coverage.active_flag',[1,0]);
        if($request->input('organization_id')){
            $mapping_coverage->where('00_mapping_coverage.organization_id',$request->input('organization_id'));
        }
        $mapping_coverage = $mapping_coverage->get();
        if($request->input('organization_id') == null){
            $mapping_coverage = [];
        }
        for ($b=0; $b < count($mapping_coverage); $b++) { 
            $obj = array(
                'mapping_coverage_id' => $mapping_coverage[$b]['mapping_coverage_id'],
                'organization_id' => $mapping_coverage[$b]['organization_id'],
                'organization_name' => $mapping_coverage[$b]['organization_name'],
                'province_coverage_id' => $mapping_coverage[$b]['provinsi_id'],
                'province_coverage_name' => $mapping_coverage[$b]['provinsi_name'],
                'regency_coverage_id' => $mapping_coverage[$b]['kabupaten_kota_id'],
                'regency_coverage_name' => $mapping_coverage[$b]['kabupaten_kota_name'],
                'district_coverage_id' => $mapping_coverage[$b]['kecamatan_id'],
                'district_coverage_name' => $mapping_coverage[$b]['kecamatan_name'],
                'village_coverage_id' => $mapping_coverage[$b]['kelurahan_desa_id'],
                'village_coverage_name' => $mapping_coverage[$b]['kelurahan_desa_name'],
                'postal_code' => $mapping_coverage[$b]['kode_pos'],
                'action' => '<ul class="ico-block"><li><a href="'.(route("admin/mapping_coverage/delete","?id=".$mapping_coverage[$b]['mapping_coverage_id'])).'" data-toggle="tooltip" title="'.(__('page.delete')).'"><i class="fa fa-trash"></i></a>'.($mapping_coverage[$b]['active_flag'] == 0 ? '<a href="'.route('admin/mapping_coverage/activate','?id='.$mapping_coverage[$b]['mapping_coverage_id']).'" data-toggle="tooltip"  title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route('admin/mapping_coverage/deactivate','?id='.$mapping_coverage[$b]['mapping_coverage_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a>').'</li></ul>'
            );

            array_push($mapping_coverage_array, $obj);
        }
        return DataTables::of($mapping_coverage_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
