<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use DataTables;
use App\Brand;
use App;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function get()
    {
        $brand = Brand::whereIn('active_flag',[0,1])->get();
        $brand_array = array();
        for ($b=0; $b < count($brand); $b++) { 
            $obj = array(
                'brand_id' => $brand[$b]['brand_id'],
                'brand_name' => ($brand[$b]['brand_name'] == '' ? '-' : $brand[$b]['brand_name']),
                'brand_code' => $brand[$b]['brand_code'],
                'brand_image' => asset($brand[$b]['brand_image']),
                'brand_description' => ($brand[$b]['brand_desc'] == '' ? '-' : $brand[$b]['brand_desc']),
                'active_flag' => $brand[$b]['active_flag'],
                'action' => '<ul class="ico-block"><li><a href="'.route("admin/brand/edit","?id=".$brand[$b]['brand_id']).'" data-toggle="tooltip" title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>  '.($brand[$b]['active_flag'] == 1 ? '<a href="'.route("admin/brand/deactivate","?id=".$brand[$b]['brand_id']).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li></ul>' : '<a href="'.route("admin/brand/activate","?id=".$brand[$b]['brand_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></li></ul>')
            );
            array_push($brand_array,$obj);
        }
        return DataTables::of($brand_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
