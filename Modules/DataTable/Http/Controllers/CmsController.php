<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Help;
use Session;
use DataTables;
use App;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    public function getHelp()
    {
        $help = Help::where('row_status', 1)->orderBy('question','ASC')->get();
        $help_array = array();
        for ($b=0; $b < count($help); $b++) { 
            # code...
            $obj = array(
                'id' => $help[$b]['id'],
                'question' => $help[$b]['question'],
                'answer' => $help[$b]['answer'],
                'row_status' => $help[$b]['row_status'],
                'action' => '<ul class="ico-block">
                                <li>
                                    <a class="loader-trigger" href="'.route("admin/cms/help/edit","?help=".$help[$b]['id']).'" data-toggle="tooltip" title="'.(__('page.edit')).'">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="'.route("admin/cms/help/deactivate","?help=".$help[$b]['id']).'" data-toggle="tooltip" title="Hapus">
                                        <i class="fa fa-trash color-green ico-size-td"></i>
                                    </a>
                                 </li>
                             </ul>');
            array_push($help_array,$obj);
        }
        return DataTables::of($help_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        //
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
