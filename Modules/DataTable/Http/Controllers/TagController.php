<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use DataTables;
use App\Tag;
use App;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    public function get()
    {
        $tag = Tag::whereIn('active_flag',[0,1])->get();
        $tag_array = array();
        for ($b=0; $b < count($tag); $b++) { 
            $obj = array(
                'tag_id' => $tag[$b]['tag_id'],
                'tag_name' => $tag[$b]['tag_name'],
                'tag_description' => $tag[$b]['tag_desc'],
                'active_flag' => $tag[$b]['active_flag'],
                'action' => '<ul class="ico-block"><li><a href="'.route("admin/tag/edit","?id=".$tag[$b]['tag_id']).'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a> '.($tag[$b]['active_flag'] == 0 ? '<a href="'.route("admin/tag/activate","?id=".$tag[$b]['tag_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></a>' : '<a href="'.route("admin/tag/deactivate","?id=".$tag[$b]['tag_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></a></li></ul>')
            );
            array_push($tag_array,$obj);
        }
        
        return DataTables::of($tag_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
