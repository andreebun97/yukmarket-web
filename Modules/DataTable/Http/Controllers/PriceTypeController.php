<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App;
use App\PriceType;
use DataTables;

class PriceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function get()
    {
        $price_type_array = array();
        $price_type = PriceType::whereNull('parent_id')->orderBy('active_flag','DESC')->get();
        for ($b=0; $b < count($price_type); $b++) { 
            $obj = array(
                "price_type_id" => $price_type[$b]['price_type_id'],
                'active_flag' => $price_type[$b]['active_flag'],
                "price_type_name" => $price_type[$b]['price_type_name'],
                "price_type_description" => $price_type[$b]['price_type_desc'],
                "action" => "<ul class='ico-block'><li><a href='".route("admin/price_type/edit","?id=".$price_type[$b]["price_type_id"])."' data-toggle='tooltip'  data-container='body' title='Ubah' ><i class='fa fa-pencil'></i></a>".($price_type[$b]["active_flag"] == 0 ? "<a href='".route("admin/price_type/activate","?id=".$price_type[$b]["price_type_id"])."' data-toggle='tooltip' data-container='body' title='".(__('page.inactive'))."'><i class='fa fa-check-circle color-gray ico-size-td'></i></a></li></ul>" : "<a href='".route("admin/price_type/deactivate","?id=".$price_type[$b]["price_type_id"])."' data-toggle='tooltip'  data-container='body' title='".(__('page.active'))."'><i class='fa fa-check-circle color-green ico-size-td'></i></a></li></ul>")
            );

            array_push($price_type_array,$obj);
        }
        return DataTables::of($price_type_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
