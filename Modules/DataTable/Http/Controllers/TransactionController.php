<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App;
use Illuminate\Support\Facades\Http;
use App\Helpers\Currency;
use Session;
use DataTables;
use App\Order;
use App\OrderDetail;
use Mail;
use App\OrderHistory;
use App\Preparation;
use App\TransactionHeader;
use DB;
use Config;
use Exception;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;

    public function __construct($currency = null){
        App::setLocale('in');
        $currency = new Currency();
        $this->currency = $currency;
    }

    var $_columns = array(
        '10_order.order_code',
        '10_order.created_date',
        '10_order.created_date',
        '00_warehouse.warehouse_id',
        '00_customer.customer_id'
    );

    private function _root($parameter) {
        try {
            $sql = Order::select(
                '10_order.order_id',
                '10_order.order_code',
                '10_order.buyer_user_id',
                '10_order.created_date',
                '00_warehouse.warehouse_name',
                '00_customer.customer_name')
                ->join('00_warehouse', '00_warehouse.warehouse_id','=','10_order.warehouse_id')
                ->join('00_customer', '00_customer.customer_id','=','10_order.buyer_user_id')
                ->where('10_order.online_flag','=',0);

            foreach($parameter as $key => $value) :
                if ($key == 0) {
                    $sql->when($parameter[$key], function($query) use ($key, $value) {
                        return $query->where($this->_columns[$key], 'like', '%'.$value.'%');
                    });
                } else if ($key == 1) {
                    $sql->when($parameter[$key], function($query) use ($key, $value) {
                        return $query->where($this->_columns[$key], '>', $value);
                    });
                } else if ($key == 2) {
                    $sql->when($parameter[$key], function($query) use ($key, $value) {
                        return $query->where($this->_columns[$key], '<', $value);
                    });
                } else if ($key == 3 or $key == 4) {
                    $sql->when($parameter[$key], function($query) use ($key, $value) {
                        return $query->where($this->_columns[$key], '=', $value);
                    });
                }
            endforeach;
            return $sql;
        } catch (Exception $ex) {
            echo $ex->getMessage(); exit();
        }
    }

    public function getData($required, $parameter) {
        $sql = $this->_root($parameter);
        $sql->orderBy($this->_columns[$required['column']], $required['order'])
            ->limit($required['limit'])
            ->offset($required['offset']);
        return $sql->get();
    }

    public function getTotal($parameter) {
        $sql = $this->_root($parameter);
        return $sql->count();
    }

    public function showTransactionOffline(Request $request) {
        $s = (!empty($request->startDate)) ? date('Y-m-d 00:00:00',strtotime($request->startDate)) : null;
        $e = (!empty($request->endDate)) ? date('Y-m-d 23:59:59',strtotime($request->endDate)) : null;

        $parameter = array(
            $request->transactionCode,
            $s,
            $e,
            $request->gudang,
            $request->konsumen
        );

        $required = array(
            'offset' => $request->has('start') ? $request->get('start') : 0,
            'limit' => $request->has('length') ? $request->get('length') : 10,
            'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
            'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc'
        );

        $temp = $this->getData($required, $parameter);
        $url = url('admin/transaction/offline_detail');
        $data = array();
        foreach ($temp as $key) :
            $path = $url.'/'.$key->order_id;
            $action = '<div row>';
            $action .= '<div class="col-sm-6"><a class="loader-trigger" href="'.$path.'" data-toggle="tooltip"  title="Detail"><i class="fa fa-eye"></i></a></div>';
            $action .= '</div>';

            $junk = (object) array(
                'trx_code' => $key->order_code,
                'warehouse_name' => $key->warehouse_name,
                'created_date' => date('d/m/Y H:i:s', strtotime($key->created_date)),
                'customer_name' => $key->customer_name,
                'action' => $action
            );
            array_push($data, $junk);
        endforeach;

        $data = array(
			'draw' => $request->get('draw'),
			'recordsTotal' => $this->getTotal($parameter),
			'recordsFiltered' => $this->getTotal($parameter),
			'data' => $data
		);
        return response()->json($data, 200);
    }

    public function get(Request $request)
    {
        //
        $order = Order::join(
            '10_order_detail',
            '10_order_detail.order_id','=','10_order.order_id')
            ->select('10_order.order_id',
            '10_order.order_code',
            '10_order.order_date',
            '10_order_detail.promo_value',
            '10_order.voucher_amount',
            '10_order.destination_address',
            '00_address.provinsi_id',
            '00_address.kabupaten_kota_id',
            '00_address.kecamatan_id',
            '00_address.kelurahan_desa_id',
            '10_order.is_fixed AS voucher_fix_type',
            '10_order.max_price AS max_provided_voucher_value',
            '10_order.admin_fee',
            '10_order.admin_fee_percentage',
            '10_order.pricing_include_tax',
            '10_order.national_income_tax',
            '10_order.shipment_price',
            DB::raw('(SELECT 10_order_history.created_date FROM 10_order_history WHERE 10_order_history.order_status_id = 8 AND 10_order_history.order_id = 10_order.order_id ORDER BY 10_order_history.created_date DESC LIMIT 1) AS shipping_date'),'00_organization.organization_name',
            DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),
            '10_order.warehouse_id',
            '10_order.buyer_name',
            '10_order.buyer_address',
            '10_order.payment_method_id',
            '00_payment_method.payment_method_name', 
            DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),
            '10_order.order_status_id',
            '10_order.voucher_id',
            '00_order_status.order_status_name',
            '10_order.is_agent',
            '10_order.order_date AS purchased_date',
            DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity * 10_order_detail.price) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity * 10_order_detail.price) + 10_order_detail.promo_value) END) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS total_price'))
            ->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
            ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
            ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
            ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
            ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
            ->distinct()->orderBy('10_order.created_date','ASC');
            if($request->input('warehouse_id') != null && $request->input('warehouse_id') != "all_warehouse"){
                $warehouse_id = $request->input('warehouse_id');
                $order->where('10_order.warehouse_id', $warehouse_id);
            }
        // if($request->input('agent_user_id') != null){
        //     $order->where('10_order.agent_user_id', $request->input('agent_user_id'));
        // }
        // echo date('Y-m-d',strtotime($request->input('start_date')));
        // exit;
        if($request->input('offline') == null){
            if($request->input('report') != null){
                $order->where(function($query){
                    $query->where('10_order.online_flag',1);
                });
            }else{
                $order->where(function($query){
                    $query->where('10_order.online_flag',1)->orWhereNotNull('10_order.parent_order_id');
                });
            }
        }else{
            $order->where('10_order.online_flag',0);
        }
        if($request->input('customer_id')){
            $order->where('10_order.buyer_user_id', $request->input('customer_id'));
        }
        // if($request->input('agent')){
        //     $order->where('is_agent',1);
        // }
        if($request->input('organization')){
            if($request->input('organization') != "all_agent"){
                $order->where('10_order.agent_user_id', $request->input('organization'))->orWhere('10_order.agent_user_id',$request->input('parent_organization'));
            }
        }

        if($request->input('order_status') != ""){
            $order_status = $request->input('order_status');
            // if($request->input('arrival') != ""){
            //     $order_status = 9;
            // }
            $order->where('10_order.order_status_id', $order_status);
        }
        if($request->input('payment_method') != ""){
            $order->where('10_order.payment_method_id', $request->input('payment_method'));
        }
        if($request->input('based_on') != "shipping_date"){
            if($request->input('start_date')){
                $order->where(DB::raw('CAST(10_order.order_date AS DATE)'),">=", DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
            }
            if($request->input('end_date')){
                $order->where(DB::raw('CAST(10_order.order_date AS DATE)'),"<=", DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
            }
        }else{
            // $order->where('10_order.order_status_id',8);
            if($request->input('start_date')){
                $order->where(DB::raw('CAST((SELECT 10_order_history.created_date FROM 10_order_history WHERE 10_order_history.order_status_id = 8 AND 10_order_history.order_id = 10_order.order_id ORDER BY 10_order_history.created_date DESC LIMIT 1) AS DATE)'),">=", DB::raw('CAST("'.$request->input('start_date').'" AS DATE)'));
            }
            if($request->input('end_date')){
                $order->where(DB::raw('CAST((SELECT 10_order_history.created_date FROM 10_order_history WHERE 10_order_history.order_status_id = 8 AND 10_order_history.order_id = 10_order.order_id ORDER BY 10_order_history.created_date DESC LIMIT 1) AS DATE)'),"<=", DB::raw('CAST("'.$request->input('end_date').'" AS DATE)'));
            }
        }
        $order = $order->get();
        $transaction_array = array();
        for ($b=0; $b < count($order); $b++) {
            // $this->getMidtransTransaction($order[$b]['order_id']);
            $total_price = $order[$b]['total_price'];
            $national_income_tax = 0;
            if($order[$b]['pricing_include_tax'] == 0){
                $national_income_tax = ROUND($total_price * $order[$b]['national_income_tax']);
                $total_price = $total_price + $national_income_tax;
            }
            $voucher_amount = 0;
            // if($order[$b]['voucher_id'] != null){
            //     if($order[$b]['voucher_fix_type'] == 'Y'){
            //         $voucher_amount = $order[$b]['voucher_amount'];
            //     }else{
            //         $voucher_amount = $total_price * $order[$b]['voucher_amount']/100;
            //         if($voucher_amount > $order[$b]['max_provided_voucher_value']){
            //             $voucher_amount = $order[$b]['max_provided_voucher_value'];
            //         }
            //     }
            // }
            $shipment_fee = 0;
            if($order[$b]['shipment_price'] != null){
                $shipment_fee = $order[$b]['shipment_price'];
            }
            $all_admin_fee = 0;
            // if($order[$b]['admin_fee'] != null){
            //     $all_admin_fee = $order[$b]['admin_fee'];
            // }
            // if($order[$b]['admin_fee_percentage'] != null){
            //     $all_admin_fee = $total_price * ($order[$b]['admin_fee_percentage']/100);
            // }
            $total_price = ($total_price - $voucher_amount) + $shipment_fee + $all_admin_fee;
            $obj = array(
                'order_id' => $order[$b]['order_id'],
                'order_code' => $order[$b]['order_code'],
                'customer_name' => ($order[$b]['customer_name'] == null ? $order[$b]['buyer_name'] : ($order[$b]['customer_name'] . ($order[$b]['is_agent'] == 0 ? '' : ' ('.__('role.agent').')'))),
                'customer_email' => $order[$b]['customer_email'],
                'customer_phone_number' => $order[$b]['customer_phone_number'],
                'total_price' => 'Rp. '.$this->currency->convertToCurrency($total_price),
                'unformatted_total_price' => $total_price,
                'order_status_name' => $order[$b]['order_status_name'],
                'user_id' => $order[$b]['user_id'],
                'shipping_date' => $order[$b]['shipping_date'] == null ? null : date("Y-m-d H:i:s",strtotime($order[$b]['shipping_date'])),
                'user_name' => $order[$b]['user_name'],
                'purchased_date' => date("Y-m-d H:i:s",strtotime($order[$b]['purchased_date'])),
                'payment_method_name' => $order[$b]['payment_method_name'],
                'organization_id' => $order[$b]['organization_id'],
                'organization_name' => $order[$b]['organization_name'],
                'action' => $request->input('method') == 'user_detail' ? '<a href="'.$request->input('url').'&order_id='.$order[$b]['order_id'].'" " data-toggle="tooltip" data-container="body" title="Detail"><i class="fa fa-eye color-green font-size-18"></i></a>' : ($request->input('order_status') == 8 ? '<a href="'.route("admin/arrival/form","?id=".$order[$b]['order_id']).'"  data-toggle="tooltip" data-container="body" title="Detail"><i class="fa fa-eye font-size-18"></i></a>' : '<a href="'.route("admin/transaction/detail","?id=".$order[$b]['order_id']).'"  data-toggle="tooltip" data-container="body" title="Detail"><i class="fa fa-eye font-size-18"></i></a><a href="#" class="trackingButton" data-toggle="modal" data-target="#trackingModal" data-order-id="'.$order[$b]['order_id'].'"><i class="fa fa-location-arrow color-green font-size-18 margin-l5" data-toggle="tooltip" data-container="body" title="Tracking"></i></a>'),
            );
            array_push($transaction_array,$obj);
        }
        if($request->input('minimum_total') != null && $request->input('minimum_total') != ""){
            $transaction_array = array_filter($transaction_array, function($value) use ($request){
                return ($value['unformatted_total_price'] >= $request->input('minimum_total'));
            });
        }
        if($request->input('maximum_total') != null && $request->input('maximum_total') != ""){
            $transaction_array = array_filter($transaction_array, function($value) use ($request){
                return ($value['unformatted_total_price'] <= $request->input('maximum_total'));
            });
        }
        return DataTables::of($transaction_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getMidtransTransaction($transaction_id)
    {
        // $order = Order::where('order_id', $transaction_id)->first();

        // $response = array();
        // if($order->invoice_status_id == 3){

        //     $url = "https://api.sandbox.midtrans.com/v2/".$order->order_code."/status";

        //     $response = Http::withHeaders([
        //         'Accept' => 'application/json',
        //         'Content-Type' => 'application/json',
        //         'Authorization' => "Basic ".base64_encode(Config::get('midtrans.server_key'))
        //     ])->get($url);

        //     if($response['status_code'] == 201 || $response['status_code'] == 200 || $response['status_code'] == 407){
        //         if($response['transaction_status'] == "settlement"){
        //             Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 2, 'order_status_id' => 6, "transfer_date" => $response['settlement_time']]);
        //             OrderHistory::insert([
        //                 'order_id' => $transaction_id,
        //                 'order_status_id' => 6,
        //                 'active_flag' => 1,
        //                 'updated_by' => Session::get('users')['id']
        //             ]);
        //             Preparation::insert([
        //                 'preparation_status_id' => 1,
        //                 'order_id' => $transaction_id,
        //                 'updated_by' => Session::get('users')['id']
        //             ]);
        //             $this->confirmationEmail($transaction_id);
        //         }else if($response['transaction_status'] == "expire"){
        //             Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 5, 'order_status_id' => 12]);
        //             OrderHistory::insert([
        //                 'order_id' => $transaction_id,
        //                 'order_status_id' => 12,
        //                 'active_flag' => 1,
        //                 'updated_by' => Session::get('users')['id']
        //             ]);
        //             $this->cancellationEmail($transaction_id);
        //         }else if($response['transaction_status'] == "deny"){
        //             Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 4, 'order_status_id' => 11]);
        //             OrderHistory::insert([
        //                 'order_id' => $transaction_id,
        //                 'order_status_id' => 11,
        //                 'active_flag' => 1,
        //                 'updated_by' => Session::get('users')['id']
        //             ]);
        //             $this->cancellationEmail($transaction_id);
        //         }
        //     }
        // }

        // return $response;
    }

    public function reminderEmail($id){
        // $id = 39;
        $transaction = Order::leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->where('order_id', $id)->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->first();
        $transaction['status'] = "Belum Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        Mail::send('payment_reminder_email',$data, function ($message) use ($transaction)
            {
                $message->from('support@yukmarket.com', 'YukMarket');
                $message->subject('Menunggu Pembayaran ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
            }
        );
    }

    public function confirmationEmail($id){
        // $id = 39;
        $transaction = Order::leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->where('order_id', $id)->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->first();
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        Mail::send('payment_confirmation_email',$data, function ($message) use ($transaction)
            {
                $message->subject('Konfirmasi Pembayaran ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        return view("payment_confirmation_email", $data);
    }

    public function cancellationEmail($id){
        // $id = 39;
        $transaction = Order::leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->where('order_id', $id)->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->first();
        $transaction['status'] = "Tidak Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        Mail::send('cancellation_payment',$data, function ($message) use ($transaction)
            {
                $message->subject('Pembatalan Pembayaran ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                $message->to("vivi.maudiwati@indocyber.co.id");
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        return view("cancellation_payment", $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
