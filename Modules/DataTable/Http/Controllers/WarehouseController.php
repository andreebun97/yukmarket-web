<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App;
use Lang;
use App\Warehouse;
use App\AddressDetail;
use DataTables;
use DB;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function get()
    {
        $store = AddressDetail::select('00_address.address_id','00_address.user_id','00_address.address_name','00_address.provinsi_id','00_address.kabupaten_kota_id','00_address.kecamatan_id','00_address.kelurahan_desa_id','00_address.address_detail','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_address.gps_point','00_address.active_flag','00_address.isMain')->join('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_warehouse','00_warehouse.address_id','=','00_address.address_id')->whereNull('00_address.customer_id')->whereNotNull('00_address.user_id')->get();
        
        $store_array = array();
        for ($b=0; $b < count($store); $b++) { 
            $obj = array(
                "address_id" => $store[$b]['address_id'],
                "user_id" => $store[$b]['user_id'],
                "warehouse_label" => $store[$b]['address_name'],
                "province_id" => $store[$b]['provinsi_id'],
                "province_name" => $store[$b]['provinsi_name'],
                "regency_id" => $store[$b]['kabupaten_kota_id'],
                "regency_name" => $store[$b]['kabupaten_kota_name'],
                "district_id" => $store[$b]['kecamatan_id'],
                "district_name" => $store[$b]['kecamatan_name'],
                "village_id" => $store[$b]['kelurahan_desa_id'],
                "village_name" => $store[$b]['kelurahan_desa_name'],
                "postal_code" => $store[$b]['kode_pos'],
                "destination_address" => $store[$b]['address_detail'] . ", " .$store[$b]['provinsi_name'] . ", " . $store[$b]['kabupaten_kota_name'] . ", " . $store[$b]['kecamatan_name'] . ", " . $store[$b]['kelurahan_desa_name'] . ", " . $store[$b]['kode_pos'],
                "address_detail" => $store[$b]['address_detail'],
                "gps_point" => $store[$b]['gps_point'],
                "store_active_status" => $store[$b]['active_flag'],
                'main_status' => $store[$b]['isMain'] == 1 ? __('notification.yes') : __('notification.no'),
                "action" => "<ul class='ico-block'><li><a href='".route("admin/warehouse/edit","?id=".$store[$b]['address_id'])."'><i class='fa fa-pencil'></i></a>".($store[$b]['active_flag'] == 0 ? '<a href="'.route("admin/warehouse/activate","?id=".$store[$b]['address_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></a>' : '<a href="'.route("admin/warehouse/deactivate","?id=".$store[$b]['address_id']).'" data-toggle="tooltip"  title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></a></li></ul>')
            );

            array_push($store_array, $obj);
        }
        return DataTables::of($store_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
