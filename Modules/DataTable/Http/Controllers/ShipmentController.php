<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Shipment;
use App\ShipmentEcommerce;
use App\Order;
use App;
use App\Helpers\Currency;
use DB;
use DataTables;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    protected $currency;

    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        App::setLocale('in');
    }

    public function index()
    {
        return view('datatable::index');
    }

    public function get(Request $request){
        $shipment = array();
        if($request->input('flag') == 'yukmarket' || $request->input('flag') == null){
            $shipment = Shipment::select('shipment_method_id','shipment_method_name','price',DB::raw('null AS organization_id'),DB::raw('null AS organization_name'),'shipment_logo','shipment_method_desc','active_flag')->get();
        }else{
            $shipment = ShipmentEcommerce::select('shipment_method_ecom_id AS shipment_method_id','shipment_method_name','price','00_organization.organization_id','00_organization.organization_name','shipment_logo','shipment_method_desc','00_shipment_method_ecommerce.active_flag')->join('00_organization','00_organization.organization_id','=','00_shipment_method_ecommerce.organization_id')->get();
        }
        $shipment_array = array();
        for ($b=0; $b < count($shipment); $b++) {
            $shipment_price = $this->currency->convertToCurrency($shipment[$b]["price"]);
            $obj = array(
                "shipment_method_id" => $shipment[$b]["shipment_method_id"],
                "shipment_method_name" => $shipment[$b]["shipment_method_name"].($shipment[$b]['organization_id'] != null ? (' - '.$shipment[$b]['organization_name']) : ''),
                "shipment_price" => "Rp. ".($shipment_price == 0 ? "-" : $shipment_price),
                "shipment_logo" => asset($shipment[$b]["shipment_logo"]),
                "shipment_description" => ($shipment[$b]["shipment_method_desc"] == null ? '-' : $shipment[$b]["shipment_method_desc"]),
                "shipment_active_status" => $shipment[$b]["active_flag"],
                "action" => ($request->input('flag') == 'ecommerce' ? '' : "<ul class='ico-block'><li><a href='".route("admin/shipment/edit","?id=".$shipment[$b]["shipment_method_id"])."' data-toggle='tooltip'  data-container='body' title='Ubah' ><i class='fa fa-pencil'></i></a>".($shipment[$b]["active_flag"] == 0 ? "<a href='".route("admin/shipment/activate","?id=".$shipment[$b]["shipment_method_id"])."' data-toggle='tooltip' data-container='body' title='".(__('page.inactive'))."'><i class='fa fa-check-circle color-gray ico-size-td'></i></a></li></ul>" : "<a href='".route("admin/shipment/deactivate","?id=".$shipment[$b]["shipment_method_id"])."' data-toggle='tooltip'  data-container='body' title='".(__('page.active'))."'><i class='fa fa-check-circle color-green ico-size-td'></i></a></li></ul>"))
            );
            array_push($shipment_array,$obj);
        }
        return DataTables::of($shipment_array)->make(true);
    }

    public function history(Request $request){
        // $order = Order::select('10_order.order_id','10_order.order_code',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),'10_order.is_agent','10_order.shipment_price','00_address.address_detail','00_address.address_info','00_address.address_name','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_shipment_method.shipment_method_name')->distinct()->join('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->where('order_status_id',7)->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->orderBy('10_preparation.preparation_status_id','DESC');
        $order = Order::select('10_order.order_id as order_id','10_order.order_code as order_code',
            DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            '10_order.is_agent as is_agent','10_order.shipment_price as shipment_price','00_address.address_detail as address_detail','00_address.address_info as address_info','00_address.address_name as address_name','00_provinsi.provinsi_name as provinsi_name','10_order.order_date',
            '00_kabupaten_kota.kabupaten_kota_name as kabupaten_kota_name',
            '00_kecamatan.kecamatan_name as kecamatan_name',
            '00_kelurahan_desa.kelurahan_desa_name as kelurahan_desa_name',
            '00_kelurahan_desa.kode_pos as kode_pos',
            '00_shipment_method.shipment_method_name as shipment_method_name')
        ->join('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')
        ->where('order_status_id',7)
        ->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])
        ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
        ->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
        ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
        ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
        ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
        ->join('10_preparation','10_preparation.order_id','=','10_order.order_id')
        ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
        ->leftjoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
        ->leftjoin('00_organization_type','00_organization_type.organization_type_id','=','00_organization.organization_type_id')
        ->orderBy('10_preparation.preparation_status_id','DESC')
        ->where('00_organization_type.organization_type_id','!=',4)
        ->groupBy('10_order.order_id');
        $orderEcom=Order::select('10_order.order_id as order_id','10_order.order_code as order_code',
            DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            '10_order.is_agent as is_agent','10_order.shipment_price as shipment_price','00_address.address_detail as address_detail','00_address.address_info as address_info','00_address.address_name as address_name','00_provinsi.provinsi_name as provinsi_name','10_order.order_date',
            '00_kabupaten_kota.kabupaten_kota_name as kabupaten_kota_name',
            '00_kecamatan.kecamatan_name as kecamatan_name',
            '00_kelurahan_desa.kelurahan_desa_name as kelurahan_desa_name',
            '00_kelurahan_desa.kode_pos as kode_pos',
            '00_shipment_method_ecommerce.shipment_method_name as shipment_method_name')
        ->join('00_shipment_method_ecommerce','00_shipment_method_ecommerce.shipment_method_ecom_id','=','10_order.shipment_method_id')
        ->where('order_status_id',7)
        ->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])
        ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
        ->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')
        ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
        ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
        ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
        ->join('10_preparation','10_preparation.order_id','=','10_order.order_id')
        ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
        ->leftjoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
        ->leftjoin('00_organization_type','00_organization_type.organization_type_id','=','00_organization.organization_type_id')
        ->orderBy('10_preparation.preparation_status_id','DESC')
        ->where('00_organization_type.organization_type_id',4)
        ->groupBy('10_order.order_id');
        if($request->input('warehouse_id') != null){
            $warehouse_id = $request->input('warehouse_id');
            $order->where('10_order.warehouse_id', $warehouse_id);
        }
        if($request->input('organization')){
            if($request->input('organization') != "all_agent"){
                $order->where('10_order.agent_user_id', $request->input('organization'))->orWhere('10_order.agent_user_id',$request->input('parent_organization'));
            }
        }
        if($request->input('shipment_method_id') != ""){
            $order->where('10_order.shipment_method_id', $request->input('shipment_method_id'));
        }
        $order->unionAll($orderEcom)->orderBy('order_date','desc');
        $order = $order->get();
        $shipped_order = array();
        for ($b=0; $b < count($order); $b++) {
            $obj = array(
                "order_id" => $order[$b]['order_id'],
                "order_code" => $order[$b]['order_code'],
                "customer_name" => ($order[$b]['customer_name'] == null ? $order[$b]['buyer_name'] : ($order[$b]['customer_name'] . ($order[$b]['is_agent'] == 0 ? '' : ' ('.__('role.agent').')'))),
                "shipment_price" => "Rp. ".$this->currency->convertToCurrency($order[$b]['shipment_price']),
                "purchased_date" => date('Y-m-d H:i:s',strtotime($order[$b]['order_date'])),
                "destination_address" => $order[$b]['destination_address'],
                "address_detail" => $order[$b]['address_detail'] . ', ' . $order[$b]['provinsi_name'] . ', ' . $order[$b]['kabupaten_kota_name'] . ', ' . $order[$b]['kecamatan_name'] . ', ' . $order[$b]['kelurahan_desa_name'] . ', ' . $order[$b]['kode_pos'],
                "shipment_method_name" => $order[$b]['shipment_method_name'],
                "action" => "<a href='".route("admin/shipment/history/form","?id=".$order[$b]['order_id'])."'  data-toggle='tooltip' data-container='body' title='Detail'><i class='fa fa-eye color-green font-size-18'></i></a>"
            );
            array_push($shipped_order, $obj);
        }

        return DataTables::of($shipped_order)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
