<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Currency;
use DataTables;
use App\Product;
use App\ProductWarehouse;
use App\StockKeepingUnit;
use App;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    protected $currency;
    
    public function __construct($currency = null){
        $currency = new Currency();
        $this->currency = $currency;
        App::setLocale('in');
    }

    public function index()
    {
        return view('datatable::index');
    }

    public function get(Request $request){
        // print_r($request->all());
        // echo $request->input('search')['value'];
        // exit;
        if($request->input('warehouse_id') != null){
            $warehouse_price = '(SELECT (CASE WHEN prw.prod_price IS NULL THEN 0 ELSE prw.prod_price END) FROM 00_product_warehouse AS prw WHERE prw.prod_id = 00_product.prod_id AND prw.warehouse_id = "'.$request->input('warehouse_id').'" ORDER BY prw.prod_id DESC LIMIT 1) AS warehouse_price';
            $warehouse_minimum_stock = '(SELECT (CASE WHEN prw.minimum_stock IS NULL THEN 0 ELSE prw.minimum_stock END) FROM 00_product_warehouse AS prw WHERE prw.prod_id = 00_product.prod_id AND prw.warehouse_id = "'.$request->input('warehouse_id').'" ORDER BY prw.prod_id DESC LIMIT 1) AS warehouse_minimum_stock';
            $warehouse_highest_price = "(SELECT (CASE WHEN MAX(prw.prod_price) IS NULL THEN 0 ELSE MAX(prw.prod_price) END) FROM 00_product_warehouse AS prw JOIN 00_warehouse AS wrh ON wrh.warehouse_id = prw.warehouse_id WHERE prw.prod_id = 00_product.prod_id AND prw.prod_price > 0 ORDER BY prw.prod_id DESC LIMIT 1) AS warehouse_highest_price";
        }else{
            $warehouse_price = '0 AS warehouse_price';
            $warehouse_minimum_stock ='0 AS warehouse_minimum_stock';
            $warehouse_highest_price = "(SELECT (CASE WHEN MAX(prw.prod_price) IS NULL THEN 0 ELSE MAX(prw.prod_price) END) FROM 00_product_warehouse AS prw JOIN 00_warehouse AS wrh ON wrh.warehouse_id = prw.warehouse_id WHERE prw.prod_id = 00_product.prod_id AND prw.prod_price > 0) AS warehouse_highest_price";
        }
        $products = Product::select('00_product.prod_id','00_product.position_date','00_product.prod_code','00_product.prod_name','00_product.minimum_order','00_product.brand_id','00_product.prod_desc','00_product.prod_price','00_product.prod_image','00_product.prod_id','00_product.uom_id','00_product.uom_value','00_product.variant_id','00_product.stock','00_product.stockable_flag','00_product.dim_length','00_product.product_sku_id','00_product.dim_width','00_product.dim_height','00_product.active_flag AS product_active_status','00_category.category_id','00_category.category_name','00_category.category_desc','00_product.bruto',DB::raw('(SELECT uo.uom_name FROM 00_uom AS uo WHERE uo.uom_id = 00_product.bruto_uom_id) AS bruto_uom'),'00_category.category_image','00_category.active_flag AS category_active_status','00_brand.brand_id','00_brand.brand_name','00_brand.brand_image','00_brand.brand_desc','00_brand.active_flag AS brand_active_status','00_uom.uom_name','00_uom.uom_desc','00_uom.active_flag AS uom_active_status','00_product.shipping_durability_min','00_product.shipping_durability_max','00_product.shipping_durability_id','00_shipping_durability.shipping_durability_name',DB::raw($warehouse_price),DB::raw($warehouse_minimum_stock), DB::raw($warehouse_highest_price))
            ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
            ->leftJoin('00_shipping_durability','00_shipping_durability.shipping_durability_id','=','00_product.shipping_durability_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
            ->whereIn('00_product.active_flag',[0,1])
            ->where(function($query) use ($request){
                // $query->from('00_product AS op')->select('op.prod_id')->whereIn ('op.prod_id')->where('op.prod_name','like','%'.($request->input('prod_name')).'%');
                $query->whereIn('prod_id',function($query2) use ($request){
                     $query2->from('00_product AS op')->select('op.prod_id')->where('op.prod_name','like','%'.($request->input('additional_search')).'%');
                })->orwhereIn('prod_id',function($query2) use ($request){
                    $query2->from('00_product AS op')->select('op.variant_id')->where('op.prod_name','like','%'.($request->input('additional_search')).'%');
                })->orwhereIn('prod_id',function($query2) use ($request){
                    $query2->from('00_product AS op')->select('op.variant_id')->where('op.prod_code','like','%'.($request->input('additional_search')).'%');
                })->orWhere('00_product.prod_code', 'LIKE', '%'.$request->input('additional_search').'%')->orWhere('00_product.prod_name', 'LIKE', '%'.$request->input('additional_search').'%')->orWhere('00_category.category_name', 'LIKE', '%'.$request->input('additional_search').'%')->orWhere('00_brand.brand_name', 'LIKE', '%'.$request->input('additional_search').'%');
            });
            // ->leftJoin('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
            // ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id');
        if($request->input('parent_product') == null){
            $products->orderBy('00_product.position_date','ASC');
            $products->whereNull('variant_id');
        }else{
            $products->orderBy('00_product.created_date','ASC');
            $products->whereNotNull('variant_id')->where('variant_id',$request->input('parent_product'));
        }
        
        $products = $products->get();
        $product_array = array();
        for ($b=0; $b < count($products); $b++) { 
            # code...
            $product_warehouse_array = array();
            if($request->input('parent_product') != null){
                $product_warehouse = ProductWarehouse::select('00_product_warehouse.prod_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_product_warehouse.prod_price','00_product_warehouse.minimum_stock','00_warehouse.warehouse_type_id')->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')->where('00_product_warehouse.prod_id', $products[$b]['prod_id'])->where('00_product_warehouse.active_flag',1)->get();
                for ($j=0; $j < count($product_warehouse); $j++) { 
                    $obj = array(
                        "prod_id" => $product_warehouse[$j]['prod_id'],
                        "prod_price" => $this->currency->convertToCurrency($product_warehouse[$j]['prod_price']),
                        "warehouse_id" => $product_warehouse[$j]['warehouse_id'],
                        "minimum_stock" => $product_warehouse[$j]['minimum_stock'],
                        "warehouse_name" => $product_warehouse[$j]['warehouse_name'],
                        "index" => $b,
                        "warehouse_type_id" => $product_warehouse[$j]['warehouse_type_id']
                    );
                    array_push($product_warehouse_array,$obj);
                }
            }
            $product_price = $this->currency->convertToCurrency($products[$b]['prod_price']);
            $obj = array(
                'prod_id' => $products[$b]['prod_id'],
                'prod_code' => $products[$b]['prod_code'],
                'product_active_status' => $products[$b]['product_active_status'],
                'position_date' => date('Y-m-d H:i:s',strtotime($products[$b]['position_date'])), 
                'prod_name' => $products[$b]['prod_name'],
                'prod_image' => $products[$b]['prod_image'],
                'prod_desc' => $products[$b]['prod_desc'],
                'category_id' => $products[$b]['category_id'],
                'category_name' => $products[$b]['category_name'],
                'uom_id' => $products[$b]['uom_id'],
                'uom_value' => ($products[$b]['uom_value']+0),
                'uom_name' => $products[$b]['uom_name'],
                'stock' => $products[$b]['stock'],
                'index' => $b,
                'stockable_flag' => $products[$b]['stockable_flag'],
                'weight' => ($products[$b]['uom_value']+0).' '.$products[$b]['uom_name'],
                'bruto' => ($products[$b]['bruto']+0).' '.$products[$b]['bruto_uom'],
                'bruto_value' => ($products[$b]['bruto']+0),
                'prod_price' => "Rp. ".$product_price,
                'brand_id' => $products[$b]['brand_id'],
                'dim_length' => $products[$b]['dim_length'],
                'flag' => 1,
                'warehouse_price' => $products[$b]['warehouse_price'],
                'warehouse_minimum_stock' => $products[$b]['warehouse_minimum_stock'],
                'dim_width' => $products[$b]['dim_width'],
                'dim_height' => $products[$b]['dim_height'],
                'minimum_order' => $products[$b]['minimum_order'],
                'parent_prod_id' => $products[$b]['parent_prod_id'],
                'product_sku_id' => $products[$b]['product_sku_id'],
                'brand_name' => $products[$b]['brand_name'],
                'shipping_durability_min' => $products[$b]['shipping_durability_min'],
                'shipping_durability_max' => $products[$b]['shipping_durability_max'],
                'shipping_durability_unit_id' => $products[$b]['shipping_durability_id'],
                'shipping_durability_unit_name' => $products[$b]['shipping_durability_name'],
                'warehouse_highest_price' => $products[$b]['warehouse_highest_price'],
                'warehouse_detail' => $product_warehouse_array,
                'action' => '<ul class="ico-block">
                <li><a '.($request->input('parent_product') != null ? 'data-toggle="modal" class="editProductVariantButton '.($request->input('parent_product') != null ? 'd-none' : '').'" data-target="#productVariantModal"' : 'href="'.route('admin/product/edit','?product='.$products[$b]['prod_id']).'"').' data-toggle="tooltip" title="'.(__('page.edit')).'">
                <i class="fa fa-pencil"></i></a> '.($request->input('parent_product') != null ? ('<div class="checkbox"><label><input type="checkbox" '.($products[$b]['product_active_status'] == 1 ? 'checked' : '').' name="product_active_checkbox[]" id="toggle-datatable" data-toggle="toggle" class="my_switch"/></label></div>'.'<input type="hidden" name="product_active_status[]" value="'.$products[$b]['product_active_status'].'">'.'<a data-toggle="modal" data-target="#deletionPopupModal" class="productDeletionButton" data-url="'.route('admin/product/delete','?id='.$products[$b]['prod_id']).'"><i class="fa fa-trash"></i></a>') : ($products[$b]['product_active_status'] == 0 ? ('<a href="'.route('admin/product/activate','?id='.$products[$b]['prod_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a><a data-toggle="modal" data-target="#deletionPopupModal" class="productDeletionButton" data-url="'.route('admin/product/delete','?id='.$products[$b]['prod_id']).'"><i class="fa fa-trash"></i></a></li></ul>') : ('<a href="'.route('admin/product/deactivate','?id='.$products[$b]['prod_id'])).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a><a data-toggle="modal" class="productDeletionButton" data-target="#deletionPopupModal" data-url="'.route('admin/product/delete','?id='.$products[$b]['prod_id']).'"><i class="fa fa-trash"></i></a></li></ul>'))
            );
            array_push($product_array, $obj);
        }
        return DataTables::of($product_array)
        ->filter(function ($instance) use ($request) {
            if ($request->has('additional_search')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return $row['flag'] == 1;
                });
            }
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function stockKeepingUnit()
    {
        $products = StockKeepingUnit::select('00_product_sku.product_sku_id','00_product_sku.product_sku_name','00_product_sku.product_sku_desc','00_product_sku.active_flag')->get();
        $product_array = array();
        for ($b=0; $b < count($products); $b++) {
            $obj = array(
                'product_sku_id' => $products[$b]['product_sku_id'],
                'product_sku_name' => $products[$b]['product_sku_name'],
                'product_sku_description' => $products[$b]['product_sku_desc'],
                'action' => '<ul class="ico-block"><li><a href="'.route('admin/sku/form','id='.$products[$b]['product_sku_id']).'" data-toggle="tooltip" title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a> '.($products[$b]['active_flag'] == 0 ? ('<a href="'.route('admin/sku/activate','?id='.$products[$b]['product_sku_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a></li></ul>') : ('<a href="'.route('admin/sku/deactivate','?id='.$products[$b]['product_sku_id']).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li></ul>'))
            );
            array_push($product_array, $obj);
        }
        return DataTables::of($product_array)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
