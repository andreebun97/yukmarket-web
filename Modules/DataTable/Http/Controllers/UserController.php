<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Customer;
use Session;
use DataTables;
use DB;
use App;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function get(Request $request){
        $users = array();
        $user_array = array();
        $role_name = $request->input('role');
        if($request->input('role') == null){
            $role_name = "admin";
        }
        // if($request->input('ytm') != null && $request->input('role') == null){
        //     $role_id = 2;
        // }
        $users = User::select('*')->whereIn('user_id', function($query){
            $query->select('user_id')->from('98_user_role');
        })->orderByRaw('IF(user_id='.(Session::get('users')['id']).',0,1)')->orderBy('active_flag','DESC')->orderBy('user_name','ASC')->get();
        if($role_name == 'customer'){
            $users = Customer::select('customer_id AS user_id','customer_name AS user_name','customer_email AS user_email','customer_phone_number AS user_phone_number','active_flag','created_date')->orderBy('created_date','DESC')->get();
        }

        for ($i=0; $i < count($users); $i++) {
            # code...
            $obj = array(
                'user_name' => $users[$i]['user_name'],
                'user_email' => $users[$i]['user_email'],
                'user_phone_number' => ($users[$i]['user_phone_number'] == '' || $users[$i]['user_phone_number'] == null ? '-' : $users[$i]['user_phone_number']),
                'created_date' => date("d-m-Y",strtotime($users[$i]->created_date)),
                'action' => ($request->input('ytm') != null ? ($role_name == 'admin' ? '<a href="'.route("admin/yukmarket/user/admin","?id=".$users[$i]['user_id']).'" data-toggle="tooltip" data-container="body"><i class="fa fa-eye font-size-18"></i></a>' : '<a href="'.route("admin/yukmarket/user/customer","?id=".$users[$i]['user_id']).'" data-toggle="tooltip" data-container="body"  title="Detail" ><i class="fa fa-eye font-size-18"></i></a>') : ($request->input('role') != 4 ? '<ul class="ico-block"><li><a href="'.route("admin/edit","?user=".$users[$i]['user_id']."").'"  data-toggle="tooltip" data-container="body"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a> ' : '').($users[$i]['user_id'] == Session::get('users')['id'] ? '' : ($users[$i]['active_flag'] == 0 ? '<a href="'.route("admin/activate","?user=".$users[$i]['user_id']."").'" data-toggle="tooltip" data-container="body" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route("admin/deactivate","?user=".$users[$i]['user_id']."").'" data-toggle="tooltip" data-container="body" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a></li></ul>')))
            );
            array_push($user_array,$obj);
        }
        return DataTables::of($user_array)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
