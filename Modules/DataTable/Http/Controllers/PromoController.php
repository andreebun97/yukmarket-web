<?php

namespace Modules\DataTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\ProductPromo;
use App;
use DataTables;
use DB;
use Session;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
    }

    public function get(Request $request)
    {
        $promo = ProductPromo::select('00_promo.*',DB::raw('(CASE WHEN CURRENT_TIMESTAMP BETWEEN start_date AND end_date THEN 0 ELSE 1 END) AS `expired`'))->where(['prod_id' => $request->input('product_id')])->get();
        $promo_array = array();
        for ($k=0; $k < count($promo); $k++) { 
            $obj = array(
                'promo_id' => $promo[$k]['promo_id'],
                'promo_code' => $promo[$k]['promo_code'],
                'product_id' => $promo[$k]['prod_id'],
                'promo_value' => $promo[$k]['promo_value'] * 100,
                'converted_promo_value' => ($promo[$k]['promo_value'] * 100).'%',
                'start_date' => date("d-m-Y", strtotime($promo[$k]['start_date'])),
                'end_date' => date("d-m-Y", strtotime($promo[$k]['end_date'])),
                'promo_image' => asset($promo[$k]['promo_image']),
                'promo_description' => $promo[$k]['promo_desc'],
                'active_flag' => $promo[$k]['active_flag'],
                'action' => $promo[$k]['expired'] == 1 ? '' : ('<a class="editPromoModal" data-toggle="modal" data-target="#promoModalForm"><i class="fa fa-pencil"></i></a>'.($promo[$k]['active_flag'] == 0 ? '<a href="'.route("admin/promo/activate","?id=".$promo[$k]['promo_id']).'" data-toggle="tooltip" title="'.(__('page.inactive')).'"><i class="fa fa-check-circle color-gray ico-size-td"></i></a>' : '<a href="'.route("admin/promo/deactivate","?id=".$promo[$k]['promo_id']).'" data-toggle="tooltip" title="'.(__('page.active')).'"><i class="fa fa-check-circle color-green ico-size-td"></i></a>'))
            );
            array_push($promo_array,$obj);
        }
        return DataTables::of($promo_array)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('datatable::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('datatable::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('datatable::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
