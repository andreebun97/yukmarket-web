<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('datatable')->group(function() {
    Route::post('/get_products','ProductController@get')->name('datatable/get_products');
    Route::post('/get_users','UserController@get')->name('datatable/get_users');
    Route::get('/get_shipments','ShipmentController@get')->name('datatable/get_shipments');
    Route::get('/get_category','CategoryController@get')->name('datatable/get_category');
    Route::get('/get_banner','BannerController@get')->name('datatable/get_banner');
    Route::get('/get_uom','UomController@get')->name('datatable/get_uom');
    Route::get('/get_role','RoleController@get')->name('datatable/get_role');
    Route::get('/get_brand','BrandController@get')->name('datatable/get_brand');
    Route::post('/get_product_promo','PromoController@get')->name('datatable/get_product_promo');
    Route::get('/get_tag','TagController@get')->name('datatable/get_tag');
    Route::post('/get_shipment','ShipmentController@get')->name('datatable/get_shipment');
    Route::post('/get_voucher','VoucherController@get')->name('datatable/get_voucher');
    Route::get('/get_warehouse','WarehouseController@get')->name('datatable/get_warehouse');
    Route::post('/get_payment','TransactionController@get')->name('datatable/get_payment');
    Route::post('/get_payment_offline','TransactionController@showTransactionOffline')->name('datatable/get_payment_offline');
    Route::post('/get_shipped_order','ShipmentController@history')->name('datatable/get_shipped_order');
    Route::post('/get_verified_order','PickingController@get')->name('datatable/get_verified_order');
    Route::post('/get_ready_order','PackingController@get')->name('datatable/get_ready_order');
    Route::post('/get_complaint_list','ComplaintController@get')->name('datatable/get_complaint_list');
    Route::get('/get_inventory','InventoryController@get')->name('datatable/get_inventory');
    Route::get('/get_stock_keeping_unit','ProductController@stockKeepingUnit')->name('datatable/get_stock_keeping_unit');
    Route::get('/get_organization','OrganizationController@get')->name('datatable/get_organization');
    Route::get('/get_price_type','PriceTypeController@get')->name('datatable/get_price_type');
    Route::post('/get_cashback','CashbackController@get')->name('datatable/get_cashback');
    Route::post('/get_payment_report','ReportController@payment')->name('datatable/get_payment_report');
    Route::get('/get_inventory_report','InventoryController@getInventoryReport')->name('datatable/get_inventory_report');
    Route::post('/get_mapping_coverage','MappingCoverageController@get')->name('datatable/get_mapping_coverage');
    Route::get('/shows', 'VoucherController@shows')->name('datatable/shows');
    Route::post('/details', 'VoucherController@details')->name('datatable/details');
    Route::get('/get_help','CmsController@getHelp')->name('datatable/get_help');
    Route::post('/get_ecommerce_report','ReportController@ecommerce')->name('datatable/get_ecommerce_report');
});
