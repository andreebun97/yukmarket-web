<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('customer')->group(function() {
    Route::get('/', 'CustomerController@index')->name('customer');
    Route::get('profile', 'ProfileController@index')->name('customer/profile');
    Route::get('address', 'ProfileController@address')->name('customer/profile/address');
    Route::prefix('transaction')->group(function(){
        Route::get('/', 'TransactionController@index')->name('customer/transaction');
        Route::get('detail', 'TransactionController@detail')->name('customer/transaction/detail');
    });
    Route::get('tracking','TransactionController@tracking')->name('customer/tracking');
    Route::get('password','ProfileController@password')->name('customer/password');
    Route::prefix('auth')->group(function(){
        Route::post('logout', 'AuthController@logout')->name('customer/auth/logout');
        Route::get('login', 'AuthController@login')->name('customer/auth/login');
        Route::post('login', 'AuthController@authenticate')->name('customer/auth/login');
        Route::get('register', 'AuthController@register')->name('customer/auth/register');
    });
});
