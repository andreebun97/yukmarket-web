<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hash;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Session;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function login()
    {
        return view('customer::auth/login');
    }

    public function register()
    {
        return view('customer::auth/register');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('customer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $body = array(
            "username" => $request->input("username"),
            "password" => $request->input("password")
        );
        
        $response = app()->call('App\Http\Controllers\AuthController@login',[$body]);
        $data = $response->getData();
        
        if($data->isSuccess == false){
            return redirect()->back();
        }else{
            $customer = array(
                'id' => $data->data->user_id,
                'login_type' => $data->data->login_type,
                'username' => $data->data->username,
                'role_status' => 'customer'
            );
            Session::put('users', $customer);
            return redirect('customer');
        }
    }

    public function logout(Request $request){
        $body = array(
            "user_id" => $request->input('user_id')
        );
        
        $response = app()->call('App\Http\Controllers\AuthController@logout',[$body]);
        $data = $response->getData();
        if($data->isSuccess == false){
            return redirect()->back();
        }else{
            Session::flush();
            return redirect('customer');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('customer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('customer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
