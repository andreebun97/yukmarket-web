@extends('customer::layouts.master')

@section('title',__('menubar.login'))

@section('content')

@include('menubar.customer')
  <section>
    <div class="container">
      <div class="row user-profile mt-4 mb-5">
        <div class="col-md-3">
          @include('sidebar.customer',['menubar' => 'shipment_address'])
        </div>
        <div class="col-md-9">
          <div class="card">
            <ul class="alamat-kirim">
              <li>
                <div class="dropdown">
                  <a class="btn btn-secondary btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ic-dot"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#alamatBaru"><i class="ic-edit"></i> Edit Alamat</a>
                    <a href="#" class="dropdown-item"><i class="ic-delete"></i> Hapus Alamat</a>
                  </div>
                </div>
                <p class="order-id">Rumah</p>
                <p>Jl. Bahagia 1 no. 123 RT 02 RW 01 Harapan Mulya Kemayoran Jakarta Pusat DKI Jakarta </p>
              </li>
              <li>
                <div class="dropdown">
                  <a class="btn btn-secondary btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ic-dot"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#alamatBaru"><i class="ic-edit"></i> Edit Alamat</a>
                    <a href="#" class="dropdown-item"><i class="ic-delete"></i> Hapus Alamat</a>
                  </div>
                </div>
                <p class="order-id">Kantor</p>
                <p>Jalan Haji R. Rasuna Said Kav. C 22, RT.2/RW.5, Karet Kuningan, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12940</p>
              </li>
              <li>
                <div class="dropdown">
                  <a class="btn btn-secondary btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ic-dot"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#alamatBaru"><i class="ic-edit"></i> Edit Alamat</a>
                    <a href="#" class="dropdown-item"><i class="ic-delete"></i> Hapus Alamat</a>
                  </div>
                </div>
                <p class="order-id">Kost Bahagia</p>
                <p>Jl. H. R. Rasuna Said No.Kav. C-22 A, RT.2/RW.5, Karet Kuningan, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12940</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('web_footer',['auth' => 'auth'])

  <!-- Modal -->
  <div class="modal fade" id="alamatBaru" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Alamat Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="labelAlamat">Label Alamat</label>
            <input type="text" class="form-control" id="labelAlamat" Value="Rumah">
          </div>
          <div class="form-group">
            <label for="alamatLengkap">Alamat Lengkap</label>
            <input type="text" class="form-control" id="alamatLengkap" Value="Jl. Bahagia 1 no. 123 RT 02 RW 01">
          </div>
          <div class="form-group">
            <label for="provinsi">Provinsi</label>
            <select class="form-control" id="provinsi">
              <option>DKI Jakarta</option>
            </select>
          </div>
          <div class="form-group">
            <label for="kotaKabupaten">Kota/Kabupaten</label>
            <select class="form-control" id="kotaKabupaten">
              <option>Jakarta Pusat</option>
            </select>
          </div>
          <div class="form-group">
            <label for="kecamatan">Kecamatan</label>
            <select class="form-control" id="kecamatan">
              <option>Kemayoran</option>
            </select>
          </div>
          <div class="form-group">
            <label for="desaKelurahan">Desa/Kelurahan</label>
            <select class="form-control" id="desaKelurahan">
              <option>Harapan Mulya</option>
            </select>
          </div>
          <div class="form-group">
            <label for="kodePos">Kode POS</label>
            <select class="form-control" id="kodePos">
              <option>10640</option>
            </select>
          </div>
          <div class="form-group">
            <label for="detailAlamat">Detail Alamat</label>
            <input type="text" class="form-control" id="detailAlamat" Value="Seberang Masjid">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-success">Simpan</button>
        </div>
      </div>
    </div>
  </div>
  @include('assets_link.js_list',['role' => 'customer'])
@endsection