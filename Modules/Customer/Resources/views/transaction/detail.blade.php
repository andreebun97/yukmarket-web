@extends('customer::layouts.master')

@section('title',__('menubar.transaction'))

@section('content')

    <!-- navbar -->
    <nav class="navbar navbar-light nav-detail">
        <a href="{{ route('customer/transaction') }}"><i class="ic-left-arrow"></i></a>
        <h1 class="text-center">Order #1588531350</h1>
    </nav>
        
    <!-- promo -->
    <section>
        <div class="container">
            <div class="row order-detail mt-4 mb-5">
                <div class="col-md-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <label class="order-left">ID Pesanan</label>
                                        <label class="order-right">1588531350</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Tanggal</label>
                                        <label class="order-right">12-03-2020 15:16 WIB</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Pembayaran</label>
                                        <label class="order-right">Kartu Kredit</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Nama Penerima</label>
                                        <label class="order-right">Budi Setiawan</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Nomor HP</label>
                                        <label class="order-right">087766665555</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Email</label>
                                        <label class="order-right">budi@gmail.com</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Alamat</label>
                                        <label class="order-right">Jl. Bahagia 1 no. 123 RT 02 RW 01 Harapan Mulya Kemayoran Jakarta Pusat DKI Jakarta</label>
                                    </li>
                                    <li class="list-group-item">
                                        <label class="order-left">Status</label>
                                        <label class="order-right order-proses">Sedang Diproses</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group order-produk">
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-2 col-md-2 d-flex align-items-center">
                                                <p class="order-jumlah">1x</p>
                                            </div>
                                            <div class="col-md-3">
                                                <img class="img-circle img-responsive" alt="" src="assets/img/img-alpukat.jpg">
                                            </div>
                                            <div class="col-md-7">
                                                <p class="order-title">Alpukat Mentega</p>
                                                <p class="order-price">Rp 40.000</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-2 col-md-2 d-flex align-items-center">
                                                <p class="order-jumlah">1x</p>
                                            </div>
                                            <div class="col-md-3">
                                                <img class="img-circle img-responsive" alt="" src="assets/img/img-alpukat.jpg">
                                            </div>
                                            <div class="col-md-7">
                                                <p class="order-title">Alpukat Mentega</p>
                                                <p class="order-price">Rp 40.000</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-2 col-md-2 d-flex align-items-center">
                                                <p class="order-jumlah">1x</p>
                                            </div>
                                            <div class="col-md-3">
                                                <img class="img-circle img-responsive" alt="" src="assets/img/img-alpukat.jpg">
                                            </div>
                                            <div class="col-md-7">
                                                <p class="order-title">Alpukat Mentega</p>
                                                <p class="order-price">Rp 40.000</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <div class="form-group">
                                    <div class="total">
                                        <label class="total-text">Total Belanja</label>
                                        <label class="total-price">Rp 189.000</label>
                                    </div>
                                    <a href="{{ route('customer/tracking') }}" class="btn btn-success btn-block">Lacak Pesanan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('assets_link.js_list',['role' => 'customer'])
@endsection