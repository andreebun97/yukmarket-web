@extends('customer::layouts.master')

@section('title',__('menubar.transaction'))

@section('content')

@include('menubar.customer')
    <!-- promo -->
    <section>
        <div class="container">
            <div class="row user-profile mt-4 mb-5">
                <div class="col-md-3">
                    @include('sidebar.customer',['menubar' => 'transaction'])
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Semua</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Diproses</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Selesai</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <ul>
                                    <li>
                                        <a href="{{ route('customer/transaction/detail') }}">
                                            <p class="order-id">ORDER #1588531350</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Tanggal</p>
                                                    <label>12-03-2020 15:16</label>
                                                    <p>Metode Pembayaran</p>
                                                    <label>Kartu Kredit</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Jumlah</p>
                                                    <label>Rp 63.000</label>
                                                    <p>Status</p>
                                                    <label class="order-proses">Diproses</label>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('customer/transaction/detail') }}">
                                            <p class="order-id">ORDER #1777212109</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Tanggal</p>
                                                    <label>10-01-2020 12:00</label>
                                                    <p>Metode Pembayaran</p>
                                                    <label>Kartu Kredit</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Jumlah</p>
                                                    <label>Rp 121.000</label>
                                                    <p>Status</p>
                                                    <label class="order-selesai">Selesai</label>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <ul>
                                    <li>
                                        <a href="{{ route('customer/transaction/detail') }}">
                                            <p class="order-id">ORDER #1588531350</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Tanggal</p>
                                                    <label>12-03-2020 15:16</label>
                                                    <p>Metode Pembayaran</p>
                                                    <label>Kartu Kredit</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Jumlah</p>
                                                    <label>Rp 63.000</label>
                                                    <p>Status</p>
                                                    <label class="order-proses">Diproses</label>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <ul>
                                    <li>
                                        <a href="{{ route('customer/transaction/detail') }}">
                                        <p class="order-id">ORDER #1777212109</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>Tanggal</p>
                                                <label>10-01-2020 12:00</label>
                                                <p>Metode Pembayaran</p>
                                                <label>Kartu Kredit</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Jumlah</p>
                                                <label>Rp 121.000</label>
                                                <p>Status</p>
                                                <label class="order-selesai">Selesai</label>
                                            </div>
                                        </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

  @include('web_footer',['auth' => 'auth'])

  @include('assets_link.js_list',['role' => 'customer'])
@endsection