@extends('customer::layouts.master')

@section('title',__('page.profile'))

@section('content')

    <!-- navbar -->
    <nav class="navbar navbar-light nav-password">
        <a href="{{ route('customer/auth/login') }}"><i class="ic-left-arrow"></i></a>
        <h1 class="text-center">{{ __('page.profile') }}</h1>
    </nav>
    
    <!-- login -->
    <section>
        <div class="container">
            <div class="row align-items-center login-row">
                <div class="card mx-auto">
                    <div class="card-body">
                        <div class="text-center mb-5">
                            <div class="login-profile">
                                <img id="imageResult" src="{{ asset('img/img-avatar.svg') }}" alt="" class="rounded-circle img-responsive mb-2">
                                <p>Budi Setiawan</p>
                                <input id="upload" type="file" onchange="readURL(this);" class="form-control border-0">
                                <label for="upload" class="btn btn-success"><i class="ic-camera"></i></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nomorHP">Nomor HP</label>
                            <input type="text" class="form-control" id="nomorHP" aria-describedby="emailHelp" value="087766665555">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="budi@gmail.com">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success btn-block" onclick="window.location.href='transaksi.html';">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

  @include('web_footer')
  @include('assets_link.js_list',['role' => 'customer'])
  <script>
      /*  ==========================================
    SHOW UPLOADED IMAGE
    * ========================================== */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageResult')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $('#upload').on('change', function () {
            readURL(input);
        });
    });

    /*  ==========================================
        SHOW UPLOADED IMAGE NAME
    * ========================================== */
    var input = document.getElementById( 'upload' );
    var infoArea = document.getElementById( 'upload-label' );

    input.addEventListener( 'change', showFileName );
    function showFileName( event ) {
      var input = event.srcElement;
      var fileName = input.files[0].name;
      infoArea.textContent = 'File name: ' + fileName;
    }
  </script>
@endsection