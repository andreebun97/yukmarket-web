@extends('customer::layouts.master')

@section('title',__('menubar.register'))

@section('content')

<!-- register -->
<section>
    <div class="container">
      <div class="row align-items-center login-row">
        <div class="col-md-6 text-center">
          <img class="img-responsive" src="{{ asset('img/ic-logo-big.svg') }}" alt="">
        </div>
        <div class="col-md-6">
          <div class="card mx-auto">
            <div class="card-body">
              <h5 class="card-title">Daftar Baru</h5>
              <h4 class="card-title">Data Pribadi</h4>
              <div class="form-group">
                <label for="namaLengkap">Nama Lengkap</label>
                <input type="text" class="form-control" id="namaLengkap" aria-describedby="emailHelp" placeholder="Cth: Permata Putri">
              </div>
              <div class="form-group">
                <label for="nomorHp">Nomor HP <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="NomorHp" aria-describedby="emailHelp" placeholder="Cth: 081200000000">
              </div>
              <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                  <input type="password" class="form-control pwd"  value="iamapassword">
                  <div class="input-group-append">
                    <a class="btn btn-outline-secondary show-password" type="button">
                      <i class="ic-eye-slash"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="alert alert-danger text-center" role="alert">
                Email / Nomor HP atau Kata Sandi salah atau tidak terdaftar!
              </div>
            </div>
            <div class="card-footer">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customControlInline">
                <label class="custom-control-label" for="customControlInline">I Agree <b>Terms</b> and <b>Condition</b></label>
              </div>
              <button class="btn btn-success btn-block" onclick="window.location.href='lupa-otp.html';">Selanjutnya</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('web_footer')
  @include('assets_link.js_list',['role' => 'customer'])
@endsection