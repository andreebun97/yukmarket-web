@extends('customer::layouts.master')

@section('title',__('menubar.login'))

@section('content')
<!-- login -->
<section>
    <div class="container">
        <div class="row align-items-center login-row">
            <div class="col-md-6 text-center">
                <img class="img-responsive" src="{{ asset('img/ic-logo-big.svg') }}" alt="">
            </div>
            <div class="col-md-6">
                <form action="{{ route('customer/auth/login') }}" method="POST">
                    @csrf
                    <div class="card mx-auto">
                        <div class="card-body">
                            <h5 class="card-title">Log In</h5>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email / Nomor HP</label>
                                <input type="text" class="form-control" name="username" id="username" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control pwd" name="password" id="password">
                                    <div class="input-group-append">
                                        <a class="btn btn-outline-secondary show-password" type="button">
                                            <i class="ic-eye-slash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-danger text-center" role="alert">
                                Email / Nomor HP atau Kata Sandi salah atau tidak terdaftar!
                            </div>
                        </div>
                        <div class="card-footer">
                            <label>Belum punya akun?<a href="{{ route('customer/auth/register') }}"> Daftar sekarang</a></label>
                            <button class="btn btn-success btn-block">Log In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </section>

  @include('web_footer')
  @include('assets_link.js_list',['role' => 'customer'])
@endsection