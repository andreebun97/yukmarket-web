@extends('customer::layouts.master')

@section('content')

@include('menubar.customer')
    
  <!-- promo -->
  <section>
      <div class="container">
          <div id="carouselPromo" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="row">

                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-o">
                          <h5 class="card-title">Disc up to 50%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Tetap di Rumah</h6>
                            <p class="text-muted">Tetap Hemat...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">promo50</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-promo-1.png" alt="Card image cap">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-g">
                          <h5 class="card-title">Sabtu Ceria 10%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Aneka Buah Diskon</h6>
                            <p class="text-muted">Akhir Pekan, buruan...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">sabtucer10</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-bawang-bombay.jpg" alt="Card image cap">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-b">
                          <h5 class="card-title">Semangat Senin 10%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Aneka Sayur Diskon</h6>
                            <p class="text-muted">di Awal Pekan, buruan...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">semangatsen10</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-bawang-bombay.jpg" alt="Card image cap">
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="carousel-item">
                  <div class="row">

                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-o">
                          <h5 class="card-title">Disc up to 50%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Tetap di Rumah</h6>
                            <p class="text-muted">Tetap Hemat...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">promo50</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-bawang-bombay.jpg" alt="Card image cap">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-b">
                          <h5 class="card-title">Semangat Senin 10%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Aneka Sayur Diskon</h6>
                            <p class="text-muted">di Awal Pekan, buruan...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">semangatsen10</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-bawang-bombay.jpg" alt="Card image cap">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body promo promo-g">
                          <h5 class="card-title">Sabtu Ceria 10%</h5>
                          <div class="col-md-10 row">
                            <h6 class="card-subtitle text-muted">Aneka Buah Diskon</h6>
                            <p class="text-muted">Akhir Pekan, buruan...</p>
                            <p class="card-text">Gunakan kode kupon:</p>
                            <a href="#" class="btn btn-warning">sabtucer10</a>
                          </div>
                          <img class="img-responsive" src="assets/img/img-bawang-bombay.jpg" alt="Card image cap">
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <ol class="carousel-indicators">
                <li data-target="#carouselPromo" data-slide-to="0" class="active"></li>
                <li data-target="#carouselPromo" data-slide-to="1"></li>
              </ol>
          </div>
      </div>
  </section>
    
  <!-- kategori -->
  <section>
    <div class="container">
      <div class="row">
        <a href="produk.html" class="col category-product active">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Semua</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-category-buah.png" alt="Card image cap">
          <label>Buah</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-category-sayur.png" alt="Card image cap">
          <label>Sayur</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-category-beras.png" alt="Card image cap">
          <label>Beras</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-category-daging.png" alt="Card image cap">
          <label>Daging</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Ikan</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Bumbu</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Susu</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Telor</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Minyak</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Minuman</label>
        </a>
        <a href="produk.html" class="col category-product">
          <img class="img-responsive" src="assets/img/img-all-category.png" alt="Card image cap">
          <label>Roti</label>
        </a>
      </div>
    </div>
  </section>

  <!-- produk -->
  <section>
    <div class="container">
      <div class="form-inline section-text">
        <strong class="">Promo Hari Ini </strong>
        <a href="produk.html" class="mr-md-auto">Lihat Semua Promo... </a>
      </div>

      <div class="row">
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">20%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-beras.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Beras Super</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 120.000</strike><span class="h6 text-muted ml-1">/5 kg</span></h1>
              <h1>Rp 96.000</h1>
            </div>
        </div>
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">5%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-jahe.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Jahe</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 3.700</strike><span class="h6 text-muted ml-1">/0.2 kg</span></h1>
              <h1>Rp 3.515</h1>
            </div>
        </div>
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">20%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-daging.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Daging Segar</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 96.000</strike><span class="h6 text-muted ml-1">/1 paket</span></h1>
              <h1>Rp 86.800</h1>
            </div>
        </div>
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">10%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-mangga.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Mangga Mana Lagi</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 25.000</strike><span class="h6 text-muted ml-1">/1 pcs</span></h1>
              <h1>Rp 22.500</h1>
            </div>
        </div>
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">20%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-kubis.png" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Kubis Putih</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 25.000</strike><span class="h6 text-muted ml-1">/1 paket</span></h1>
              <h1>Rp 20.000</h1>
            </div>
        </div>
        <div class="col-md-2 promo-list">
          <a href="detail-produk.html">
            <span class="badge">30%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-telur.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Telur Premium</a>
              <h1 class="font-weight-normal mb-0"><strike>Rp 30.000</strike><span class="h6 text-muted ml-1">/1 kg</span></h1>
              <h1>Rp 24.000</h1>
            </div>
        </div>

      </div>
    </div>
  </section>

  <!-- produk -->
  <section>
    <div class="container">
      <div class="form-inline section-text">
        <strong class="">Produk </strong>
        <a href="produk.html" class="mr-auto">Lihat Semua Promo... </a>
        <a href="#"><i class="ic-sort"></i></a>
        <a href="#"><i class="ic-filter"></i></a>
      </div>

      <div class="row">

        <div class=" product-list">
          <a href="detail-produk.html">
            <span class="badge">20%</span>
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-mangga.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Mangga Harum Manis</a>
              <h1 class="font-weight-normal mb-0">Rp 20.000<span class="h6 text-muted ml-1">/500 gram</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-wortel.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Wortel Brastagi</a>
              <h1 class="font-weight-normal mb-0">Rp 15.500<span class="h6 text-muted ml-1">/200 gram</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-alpukat.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Alpukat Mentega</a>
              <h1 class="font-weight-normal mb-0">Rp 10.000<span class="h6 text-muted ml-1">/1 pcs</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-tomat.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Tomat Merah</a>
              <h1 class="font-weight-normal mb-0">Rp 15.000<span class="h6 text-muted ml-1">/1 paket</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-bayam.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Daun Bayam Hijau</a>
              <h1 class="font-weight-normal mb-0">Rp 25.000<span class="h6 text-muted ml-1">/1 paket</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-papaya.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Pepaya Kalifornia</a>
              <h1 class="font-weight-normal mb-0">Rp 20.000<span class="h6 text-muted ml-1">/200 gram</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-lemon.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Lemon Local</a>
              <h1 class="font-weight-normal mb-0">Rp 12.000<span class="h6 text-muted ml-1">/100 gram</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-paha.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Paha Ayam</a>
              <h1 class="font-weight-normal mb-0">Rp 96.000<span class="h6 text-muted ml-1">/1 paket</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-buncis.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Buncis</a>
              <h1 class="font-weight-normal mb-0">Rp 20.000<span class="h6 text-muted ml-1">/1 paket</span></h1>
            </div>
        </div>
        <div class=" product-list">
          <a href="detail-produk.html">
            <img class="img-thumbnail card-img-top smallimg" src="assets/img/img-bawang.jpg" alt="">
          </a>
            <div class="card-block">
              <a href="detail-produk.html" class="card-subtitle text-muted">Penjual Indocyber</a>
              <a href="detail-produk.html" class="card-title">Bawang Putih</a>
              <h1 class="font-weight-normal mb-0">Rp 35.000<span class="h6 text-muted ml-1">/1 paket</span></h1>
            </div>
        </div>

      </div>
    </div>
  </section>

  <!-- Button trigger modal -->
  <button type="button" class="btn btn-cart" data-toggle="modal" data-target="#shoppingCart" data-backdrop="static" data-keyboard="false" data-badge="34">
    <i class="ic-shop-cart"></i>
  </button>

  @include('web_footer',['auth' => 'auth'])

  <!-- SHOPPING CART -->
  <div class="modal fade" id="shoppingCart" tabindex="-1" role="dialog" aria-labelledby="shoppingCartLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="shoppingCartLabel">Keranjang Belanja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="border-bottom">
            <label class="card-subtitle">Penjual Indocyber</label>
            <a href="index.html" class="add-product float-right"><i class="ic-add-cart"></i> Tambah lagi</a>
          </div>

          <ul class="list-group">
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-3">
                  <img class="img-circle img-responsive" alt="" src="assets/img/img-alpukat.jpg">
                </div>
                <div class="col-md-9 item-amount">
                  <label class="card-subtitle">Alpukat Mentega</label>
                  <label class="float-right">Rp 40.000</label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default btn-number btn-left" disabled="disabled" data-type="minus" data-field="quant[1]">
                        <i class="ic-cart-minus"></i>
                      </button>
                    </span>
                    <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default btn-number btn-right" data-type="plus" data-field="quant[1]">
                        <i class="ic-cart-plus"></i>
                      </button>
                    </span>
                  </div>
                </div>
              </div>
            </li>
          </ul>

        </div>
        <div class="modal-footer">
          <div class="total">
            <label class="total-text mr-md-auto">Total Belanja</label>
            <label class="total-price float-right">Rp 189.000</label>
          </div>
          <a href="pengiriman.html" class="btn btn-success btn-block">Beli</a>
        </div>
      </div>
    </div>
  </div>
  @include('assets_link.js_list',['role' => 'customer'])
@endsection
