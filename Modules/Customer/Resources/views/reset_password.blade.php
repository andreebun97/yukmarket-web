@extends('customer::layouts.master')

@section('title',__('menubar.change_password'))

@section('content')

  <!-- navbar -->
  <nav class="navbar navbar-light nav-password">
    <a href="login.html"><i class="ic-left-arrow"></i></a>
    <h1 class="text-center">{{ __('menubar.change_password') }}</h1>
  </nav>
    
  <!-- login -->
  <section>
    <div class="container">
      <div class="row align-items-center login-row">
        <div class="card mx-auto">
          <div class="card-body">       
            <div class="form-group">
              <label>Kata Sandi Lama</label>
              <div class="input-group">
                <input type="password" class="form-control pwd"  value="iamapassword">
                <div class="input-group-append">
                  <a class="btn btn-outline-secondary show-password" type="button">
                    <i class="ic-eye-slash"></i>
                  </a>
                </div>
              </div>
            </div>       
            <div class="form-group">
              <label>Kata Sandi Baru</label>
              <div class="input-group">
                <input type="password" class="form-control pwdn"  value="iamapassword">
                <div class="input-group-append">
                  <a class="btn btn-outline-secondary show-password-new" type="button">
                    <i class="ic-eye-slash"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="alert alert-danger text-center" role="alert">
              Email / Nomor HP atau Kata Sandi salah atau tidak terdaftar!
            </div>
          </div>
          <div class="card-footer">
            <button class="btn btn-success btn-block" onclick="window.location.href='transaksi.html';">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('web_footer')

  @include('assets_link.js_list',['role' => 'customer'])
@endsection