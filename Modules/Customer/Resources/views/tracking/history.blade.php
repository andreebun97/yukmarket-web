@extends('customer::layouts.master')

@section('title',__('menubar.transaction'))

@section('content')
    <!-- navbar -->
    <nav class="navbar navbar-light nav-detail">
        <a href="{{ route('customer/transaction/detail') }}"><i class="ic-left-arrow"></i></a>
        <h1 class="text-center">Order #1588531350</h1>
    </nav>
        
    <!-- promo -->
    <section>
        <div class="container">
            <div class="row order-detail mt-4 mb-5">
                <div class="col-md-12">
                    <div class="card align-items-center">                        
                        <ul class="timeline-icon">
                            <li class="active">
                                <div class="icon-circle">
                                    <i class="ic-diproses-a"></i>
                                </div>
                                <p>Diproses</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-dikirim"></i>
                                </div>
                                <p>Dikirim</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-sampai"></i>
                                </div>
                                <p>Sampai</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-selesai"></i>
                                </div>
                                <p>Selesai</p>
                            </li>
                        </ul>

                        <ul class="timeline">
                            <li class="active">
                                <p>Diproses Penjual - Kamis 12-03-2020</p>
                                <p>Pesanan sedang dikemas oleh penjual</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                        </ul>

                        <a href="#" class="btn btn-success ">Pesan Lagi</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
  @include('assets_link.js_list',['role' => 'customer'])
@endsection