<?php

namespace Modules\Home\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $category = array(
            array(
                'id' => 1,
                'img' => 'menu-category/semua.png',
                'name' => 'Semua'
            ),
            array(
                'id' => 2,
                'img' => 'menu-category/buah.png',
                'name' => 'Buah'
            ),
            array(
                'id' => 3,
                'img' => 'menu-category/sayur.png',
                'name' => 'Sayur'
            ),
            array(
                'id' => 4,
                'img' => 'menu-category/beras.png',
                'name' => 'Beras'
            ),
            array(
                'id' => 5,
                'img' => 'menu-category/daging.png',
                'name' => 'Daging'
            ),
            array(
                'id' => 6,
                'img' => 'menu-category/ikan.png',
                'name' => 'Ikan'
            ),
            array(
                'id' => 7,
                'img' => 'menu-category/bumbu.png',
                'name' => 'Bumbu'
            ),
            array(
                'id' => 8,
                'img' => 'menu-category/susu.png',
                'name' => 'Susu'
            ),
            array(
                'id' => 9,
                'img' => 'menu-category/telur.png',
                'name' => 'Telur'
            ),
            array(
                'id' => 10,
                'img' => 'menu-category/ikan.png',
                'name' => 'Ikan'
            ),
            array(
                'id' => 11,
                'img' => 'menu-category/minuman.png',
                'name' => 'Minuman'
            ),
            array(
                'id' => 12,
                'img' => 'menu-category/roti.png',
                'name' => 'Roti'
            )
        );

        $promo_list = array(
            array(
                'id' => 1,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Beras Super',
                'real_price' => 120000,
                'weight' => 5,
                'promo_price' => 96000
            ),
            array(
                'id' => 2,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Jahe',
                'real_price' => 3700,
                'weight' => 0.2,
                'promo_price' => 3515
            ),
            array(
                'id' => 3,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Paha Ayam',
                'real_price' => 96000,
                'weight' => 1,
                'promo_price' => 86800
            ),
            array(
                'id' => 4,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Buah Naga',
                'real_price' => 25000,
                'weight' => 1,
                'promo_price' => 20000
            ),
            array(
                'id' => 5,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Pisang Sunrise',
                'real_price' => 25000,
                'weight' => 1,
                'promo_price' => 22500
            ),
            array(
                'id' => 6,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Cabe Keriting',
                'real_price' => 30000,
                'weight' => 1,
                'promo_price' => 24000
            )
        );

        $recent_product_list = array(
            array(
                'id' => 1,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Beras Super',
                'real_price' => 120000,
                'weight' => 5,
                'promo_price' => 96000
            ),
            array(
                'id' => 2,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Jahe',
                'real_price' => 3700,
                'weight' => 0.2,
                'promo_price' => 3515
            ),
            array(
                'id' => 3,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Paha Ayam',
                'real_price' => 96000,
                'weight' => 1,
                'promo_price' => 86800
            ),
            array(
                'id' => 4,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Buah Naga',
                'real_price' => 25000,
                'weight' => 1,
                'promo_price' => 20000
            ),
            array(
                'id' => 5,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Pisang Sunrise',
                'real_price' => 25000,
                'weight' => 1,
                'promo_price' => 22500
            ),
            array(
                'id' => 6,
                'img' => 'img-beras.jpg',
                'seller_name' => 'Penjual Indocyber',
                'product_name' => 'Cabe Keriting',
                'real_price' => 30000,
                'weight' => 1,
                'promo_price' => 24000
            )
        );
        $data['category'] = $category;
        $data['promo_list'] = $promo_list;
        $data['recent_product_list'] = $recent_product_list;
        return view('home::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('home::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('home::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('home::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
