@extends('home::layouts.master')

@section('title','Beranda')

@section('footer','height-auto')

@section('content')
    <!-- menu -->
     @include('menubar.customer')
    <div class="container">
        <div class="col-md-12">
            <div class="banner-carousel owl-theme owl-carousel">
                <div class="item">
                    <img src="{{ asset('img/dummy/banner1.jpg') }}" alt="">
                </div>
                <div class="item">
                    <img src="{{ asset('img/dummy/banner1.jpg') }}" alt="">
                </div>
                <div class="item">
                    <img src="{{ asset('img/dummy/banner1.jpg') }}" alt="">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="category-scroll-container">
                <div class="category-wrap">
                    @for($b = 0; $b < count($category); $b++)
                    <div class="card borderless card-category">
                            <a href="{{ route('home','?category='.$category[$b]['id']) }}" class="card-item category-name f-14{{ $category[$b]['id'] == Request::get('category') ? ' active' : ($b == 0 && Request::get('category') == null ? ' active' : '') }}">
                                <img src="{{ asset('img/'.$category[$b]['img']) }}">
                                <p>{{ $category[$b]['name'] }}</p>
                                
                            </a>
                    </div>
                    @endfor
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12">
            <div class="ym-promo-container">
                <h4 class="medium-bold color-green">Promo Hari Ini </h4> 
                <a href="" class="text-gray ml-2 f-14">Lihat Semua...</a>
                <div class="clearfix"></div>

                @for($b = 0; $b < count($promo_list); $b++)
                    <div class="col-md-2 col-sm-3 col-xs-4">
                        <div class="ym-promo-item">
                            <div class="ym-promo-img">
                                <div class="promo"><p>20%</p></div>
                                <img src="{{ asset('img/img-beras.jpg') }}" class="w-100" alt="">
                                <div class="ym-popover">
                                    <a href="#!" class="btn-buy"> <h4> Beli</h4></a>
                                </div>
                                <div class="ym-popover">
                                    <div class="btn-add">
                                    <div class="product-image-transaction d-flex mb-3">
                                        <button class="btn remove_button" data-product-id="{{ $recent_product_list[$b]['id'] }}">
                                            <span>-</span>
                                        </button>
                                        <input type="text" class="form-control" readonly value="0 buah">
                                        <button class="btn add_button" data-product-id="{{ $recent_product_list[$b]['id'] }}">
                                            <span>+</span>
                                        </button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ym-promo-desc">
                                <span class="text-gray">{{ $promo_list[$b]['seller_name'] }}</span>
                                <div class="clearfix"></div>
                                <span><h4>{{ $promo_list[$b]['product_name'] }}</h4></span>
                                <div class="price-display">
                                    <span class="normal-price"><s>Rp. {{ $promo_list[$b]['real_price'] }}</s> / 5kg</span>
                                    <div class="clearfix"></div>
                                    <span class="promo-price">Rp. {{ $promo_list[$b]['promo_price'] }}</span>
                                </div>
                                <div class="price-display-block">
                                    <span class="float-left">Total</span>
                                    <span class="float-right">Rp. {{ $promo_list[$b]['promo_price'] }}</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endfor
                <div class="clearfix"></div>
            </div>
        </div>
    
        <div class="col-md-12">
            <div class="ym-product-container">
                <h4 class="medium-bold color-green">Produk Terbaru </h4> 
                <a href="" class="text-gray ml-2 f-14">Lihat Semua...</a>
                <div class="clearfix"></div>
            
                @for($b = 0; $b < count($recent_product_list); $b++)
                    <div class="col-md-5ths col-xs-6">
                        <div class="ym-product-item">
                            <div class="ym-promo-img">
                                <img src="{{ asset('img/img-beras.jpg') }}" class="w-100" alt="">
                                <div class="ym-popover">
                                    <a href="#!" class="btn-buy"> <h4> Beli</h4></a>
                                </div>
                                <div class="ym-popover">
                                    <div class="btn-add">
                                        <div class="product-image-transaction d-flex mb-3">
                                            <button class="btn remove_button" data-product-id="{{ $recent_product_list[$b]['id'] }}">
                                                <span>-</span>
                                            </button>
                                            <input type="text" class="form-control" readonly value="0 buah">
                                            <button class="btn add_button" data-product-id="{{ $recent_product_list[$b]['id'] }}">
                                                <span>+</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ym-promo-desc">
                                <span class="text-gray d-block">{{ $recent_product_list[$b]['seller_name'] }}</span>
                                <div class="clearfix"></div>
                                <span><h4>{{ $recent_product_list[$b]['product_name'] }}</h4></span>
                                <div class="clearfix"></div>
                                <div class="price-display">
                                    <span class="normal-price"> <span class="muli-bold">Rp. {{ $recent_product_list[$b]['real_price'] }}</span> / 5 kg</span>
                                </div>
                                <div class="price-display-block">
                                    <span class="float-left">Total</span>
                                    <span class="float-right">Rp. {{ $recent_product_list[$b]['real_price'] }}</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="separator"></div>
    </div>
    <div class="overlay-cart">
        <a href="#!" data-toggle="modal" data-target="#cart-modal">
            <p>20</p>
        </a>
        <i class="ic-shop-cart"></i>
    </div>
    <!-- Modal -->
	<div class="modal right fade cart-modal" id="cart-modal" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                <div class="cart-container">
                    <div class="modal-header">
                        
                    </div>

                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>

			</div>
		</div>
	</div>
	<!-- end modal -->
	
</div><!-- container -->
    @include('web_footer')
    @include('assets_link.js_list')
@endsection
