@extends('admin::layouts.master')

@section('title',__('page.forgot_password'))

@section('content')
    <div class="login-container">

        <!-- Page container -->
        <div class="login-page forgot-page">

            <!-- Page content -->
            <!-- <div class="page-content"> -->

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content no-padding">

                        <!-- Advanced login -->
                        <form method="POST" id="forgotPasswordForm" action="{{ route('admin/auth/password/forgot') }}">
                            @csrf
                            <div class="panel-body login-form">
                                <span class="logo-yukmarket-potrait"></span>
                                <div class="text-center">
                                    <h4 class="content-group-lg font-med color-1">{{ __('page.forgot_password') }}
                                        <!--   <small class="display-block">With Your Social Media Network</small> -->

                                    </h4>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="user_email" class="form-control @error('user_email') is-invalid @enderror" placeholder="Email" value="{{ old('user_email') }}" autocomplete="off">
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-envelope fa-login text-muted "></i>
                                    </div>
                                    @error('user_email')
                                        <span class="invalid-feedback"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>{{ $errors->first('user_email') }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-6 padding-r-10">
                                        <button type="button" class="btn btn-gray btn-block" onclick="window.location.href = '{{ route("admin/auth/login") }}'">{{ __('page.back') }}</button>
                                        
                                    </div>
                                    <div class="col-xs-6 no-padding ">
                                    <button type="submit" class="btn btn-green btn-block no-margin">{{ __('page.submit') }}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- <div class="content-divider text-muted form-group"><span>or sign in with</span></div>
                                <ul class="list-inline form-group list-inline-condensed text-center">
                                    <li><a href="#" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#" class="btn border-pink-300 text-pink-300 btn-flat btn-icon btn-rounded"><i class="icon-dribbble3"></i></a></li>
                                    <li><a href="#" class="btn border-slate-600 text-slate-600 btn-flat btn-icon btn-rounded"><i class="icon-github"></i></a></li>
                                    <li><a href="#" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>
                                </ul>

                                <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
                                <a href="login_registration.html" class="btn btn-default btn-block content-group">Sign up</a>
                                <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span> -->
                            </div>
                        </form>
                        <div class="login100">                            
                            <div class="login-image-container">
                                <img src="{{ asset('img/img-forgot.png') }}">
                            </div>
                        </div>
                        <!-- /advanced login -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var validationRules = {
            user_email: {
                required: true
            }
        };
        var validationMessages = {
            user_email: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_email')]) }}"
            }
        };

        $('form#forgotPasswordForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            errorPlacement: function(error, element) {
                console.log(element.index(this));
                error.insertAfter(element.siblings('.btn-white-2'));
                error.append('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
            },
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });
    </script>
@endsection