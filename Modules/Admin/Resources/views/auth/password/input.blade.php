@extends('admin::layouts.master')

@section('title',__('page.input_password'))

@section('content')
    <div class="login-container">

        <!-- Page container -->
        <div class="login-page forgot-page">

            <!-- Page content -->
            <!-- <div class="page-content"> -->

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content no-padding">

                        <!-- Advanced login -->
                        <form method="POST" action="{{ route('admin/auth/password/input') }}" id="passwordForm">
                            @csrf
                            <div class="panel-body login-form">
                                <span class="logo-yukmarket-potrait"></span>
                                <div class="text-center">
                                    <h2 class="content-group-lg font-med color-1">{{ __('page.input_password') }}
                                    </h2>
                                </div>
                                <input type="hidden" name="user_email" value="{{ Session::get('username') }}">
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" name="user_new_password" class="form-control @error('user_new_password') is-invalid @enderror" placeholder="Password" autocomplete="off" id="new-password-field">
                                    <span toggle="#new-password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                               
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-lock fa-login"></i>
                                    </div>
                                    @error('user_new_password')
                                        <span class="invalid-feedback"><i class="fa fa-exclamation-circle right35" aria-hidden="true"></i>{{ $errors->first('user_new_password') }}</span>
                                    @enderror
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" name="user_confirm_password" class="form-control @error('user_confirm_password') is-invalid @enderror" placeholder="{{ __('field.confirm_new_password') }}" autocomplete="off" id="confirm-password-field">
                                    <span toggle="#confirm-password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                               
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-lock fa-login"></i>
                                    </div>
                                    @error('user_confirm_password')
                                        <span class="invalid-feedback"><i class="fa fa-exclamation-circle right35" aria-hidden="true"></i>{{ $errors->first('user_confirm_password') }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-6 padding-r-10">
                                        <button type="button" class="btn btn-gray bg-gray btn-block" onclick="window.location.href = '{{ route("admin/auth/password/otp") }}'">{{ __('page.back') }}</button>
                                    </div>
                                    <div class="col-xs-6 no-padding">
                                        <button type="submit" class="btn bg-green btn-block no-margin ">{{ __('page.submit') }}</button>
                                    </div>


                                </div>
                            </div>
                        </form>
                        <div class="login100">
                            
                            <div class="login-image-container">
                                <img src="{{ asset('img/img-forgot.png') }}">
                            </div>
                        </div>
                        <!-- /advanced login -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $.validator.addMethod("alpha_num", function(value, element) {
            if (this.optional(element)) {
                return true;
            } else if (!/[A-Z]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            } else if (!/[0-9]/.test(value)) {
                return false;
            }

            return true;
        }, "Letters & Numeric only please");

        $.validator.addMethod("isEmail", function(value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        }, "Email format please");

        $('input[name="user_old_password"], input[name="user_new_password"], input[name="user_confirm_password"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            user_new_password: {
                required: true,
                minlength: 8,
                alpha_num: true
            },
            user_confirm_password: {
                required: true,
                equalTo: "input[name='user_new_password']"
            }
        };
        var validationMessages = {
            user_new_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.new_password')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.new_password'), 'min' => 8]) }}",
                alpha_num: "{{ __('validation.custom.new_password.alpha_num',['attribute' => __('validation.attributes.new_password')]) }}"
            },
            user_confirm_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.confirm_new_password')]) }}",
                equalTo: "{{ __('validation.same',['attribute' => __('validation.attributes.confirm_new_password'), 'other' => __('validation.attributes.new_password')]) }}"
            }
        };

        $('form#passwordForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            errorPlacement: function(error, element) {
                console.log(element.index(this));
                error.insertAfter(element.siblings('.btn-white-2'));
                error.append('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
            },
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });
    </script>
@endsection