@extends('admin::layouts.master')

@section('title',__('page.otp'))

@section('content')
    <div class="login-container">

        <!-- Page container -->
        <div class="login-page forgot-page">

            <!-- Page content -->
            <!-- <div class="page-content"> -->

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content no-padding">

                        <!-- Advanced login -->
                        <form method="POST" action="{{ route('admin/auth/password/otp') }}" id="otpForm">
                            @csrf
                            <div class="panel-body login-form">
                                <span class="logo-yukmarket-potrait"></span>
                                <div class="text-center">
                                    <h4 class="content-group-lg font-med color-1">{{ __('page.otp') }}
                                    </h4>
                                </div>
                                <input type="hidden" name="timer" value="{{ $timer }}">
                                <input type="hidden" name="user_email" value="{{ Session::get('username') }}">
                                <div class="otp_duration">
                                    <h2>{{ $timer }}</h2>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-lock fa-login"></i>
                                    </div>
                                
                                    <input type="text" name="otp_code" class="form-control @error('otp_code') is-invalid @enderror" placeholder="OTP" autocomplete="off" id="password-field">
                               
                                    @error('otp_code')
                                        <span class="invalid-feedback"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i>{{ $errors->first('otp_code') }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <button type="submit" id="submitOtpButton" class="btn bg-green btn-block">{{ __('page.submit') }}</button>
                                    <span class="none" id="resendCodeOrder">{{ __('page.resend_code') }}</span>
                                </div>

                                <!-- <div class="content-divider text-muted form-group"><span>or sign in with</span></div>
                                <ul class="list-inline form-group list-inline-condensed text-center">
                                    <li><a href="#" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#" class="btn border-pink-300 text-pink-300 btn-flat btn-icon btn-rounded"><i class="icon-dribbble3"></i></a></li>
                                    <li><a href="#" class="btn border-slate-600 text-slate-600 btn-flat btn-icon btn-rounded"><i class="icon-github"></i></a></li>
                                    <li><a href="#" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>
                                </ul>

                                <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
                                <a href="login_registration.html" class="btn btn-default btn-block content-group">Sign up</a>
                                <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span> -->
                            </div>
                        </form>
                        <div class="login100">
                            <div class="login-image-container">
                                <img src="{{ asset('img/img-otp.png') }}">
                            </div>
                        </div>
                        <!-- /advanced login -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </div>
    <form method="POST" id="resendCodeForm" action="{{ route('admin/auth/password/forgot') }}">
        @csrf
        <input type="hidden" name="user_email" value="{{ Session::get('username') }}">
    </form>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var validationRules = {
            otp_code: {
                required: true
            }
        };
        var validationMessages = {
            otp_code: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.otp_code')]) }}"
            }
        };

        $('form#otpForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            // errorPlacement: function(error, element) {
            //     console.log(element.index(this));
            //     // error.insertAfter(element.siblings('.btn-white-2'));
            //     // error.append('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
            // },
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        var timer = $('.otp_duration h2').html();
        var hour = parseInt(timer.split(":")[0]);
        var second = parseInt(timer.split(":")[1]);
        var count;
        var timer_flag = 0;
        var timer;

        function timerCount(){
            second -= 1;
            if(second == -1 && hour > 0){
                second = 59;
                hour -= 1;
            }
            if(second == 0 && hour == 0){
                stopCount();
                $('#submitOtpButton').attr('disabled',true);
                $('#resendCodeOrder').removeClass('none');
            }
            $('.otp_duration h2').html((hour < 10 ? ("0"+hour) : hour)+":"+(second < 10 ? ("0"+second) : second));
            $('input[name="timer"]').val((hour < 10 ? ("0"+hour) : hour)+":"+(second < 10 ? ("0"+second) : second));
        }

        count = setInterval(timerCount,1000);

        function startCount(){
            if(!timer_flag){
                timer_flag = 1;
                timerCount();
            }
        }

        function stopCount(){
            clearInterval(count);
            timer_flag = 0;
        }

        $('#resendCodeOrder').click(function(){
            $('form#resendCodeForm').submit();
        })
    </script>
@endsection