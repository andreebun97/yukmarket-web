@extends('admin::layouts.master')

@section('title',__('menubar.login'))

@section('content')
    <div class="login-container">

        <!-- Page container -->
        <div class="login-page">

            <!-- Page content -->
            <!-- <div class="page-content"> -->

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content no-padding">

                        <!-- Advanced login -->
                        <form action="{{ route('admin/login') }}" id="loginForm" method="POST">
                            @csrf
                            <div class="panel-body login-form">
                                <span class="logo-yukmarket-potrait" style="background-image: url('{{ asset($logo) }}')"></span>
                                <div class="text-center">
                                    <h4 class="content-group-lg font-med color-1">{{ __('menubar.login') }}
                                        <!--   <small class="display-block">With Your Social Media Network</small> -->
                                    </h4>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" name="user_email" class="form-control @error('user_email') is-invalid @enderror" placeholder="Email" value="{{ old('user_email') }}" autocomplete="off">
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-user fa-login text-muted "></i>
                                    </div>
                                    @error('user_email')
                                        <span class="invalid-feedback"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>{{ $errors->first('user_email') }}</span>
                                    @enderror
                                </div>

                                <div class="form-group has-feedback has-feedback-left  password-invalid">
                                    <input type="password" name="user_password" class="form-control @error('user_password') is-invalid @enderror" placeholder="{{ __('field.password') }}" autocomplete="off" id="password-field">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                               
                                    <div class="form-control-feedback btn-white-2">
                                        <i class="fa fa-lock fa-login"></i>
                                    </div>
                                    @error('user_password')
                                        <span class="invalid-feedback"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>{{ $errors->first('user_password') }}</span>
                                    @enderror
                                </div>

                                <div class="form-group login-options forgot">
                                    <a href="{{ route('admin/auth/password/forgot') }}">{{ __('page.forgot_password') }}?</a>
                                            
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <button type="submit" class="btn bg-green btn-block">{{ __('menubar.login') }}</button>
                                </div>
                            </div>
                        </form>
                        <div class="login100">
                            
                            <div class="login-image-container">
                                <img src="{{ asset('img/img-login-image.png') }}">
                                <h1>Yuk Beli Sekarang!</h1>
                            </div>
                        </div>
                        <!-- /advanced login -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var validationRules = {
            user_email: {
                required: true,
                emregex: true
            },
            user_password: {
                required: true
            }
        };
        var validationMessages = {
            user_email: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_email')]) }}"
            },
            user_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_password')]) }}"
            }
        };

        $.validator.addMethod("emregex", function(value, element) {
		    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
	    }, "Format email salah.");

        $('form#loginForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            errorPlacement: function(error, element) {
                console.log(element.index(this));
                error.insertAfter(element.siblings('.btn-white-2'));
                error.append('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>');
            },
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });
    </script>
@endsection