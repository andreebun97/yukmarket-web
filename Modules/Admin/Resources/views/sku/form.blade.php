@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_stock_keeping_unit') : __('page.edit_stock_keeping_unit')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-cubes"></i></li>
                                <li>{{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_stock_keeping_unit') : __('page.edit_stock_keeping_unit') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_stock_keeping_unit') : __('page.edit_stock_keeping_unit') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form method="POST" id="stockKeepingUnitForm" class="form-style" action="{{ route('admin/sku/form') }}">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('id') != null)
                                                            <input type="hidden" name="product_sku_id" value="{{ Request::get('id') }}">
                                                        @endif
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                            <div class="clearfix"></div>
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="sku_name">{{ __('field.sku_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" name="sku_name" id="sku_name" @isset($sku) value="{{ $sku['product_sku_name'] }}" @endif class="form-control">
                                                                    @error('sku_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('sku_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="sku_description">{{ __('field.sku_description') }} <span class="required-label">*</span></label>
                                                                    <textarea name="sku_description" id="sku_description" class="form-control" style="resize:none" cols="30" rows="10">{{ isset($sku) ? $sku['product_sku_desc'] : '' }}</textarea>
                                                                    @error('sku_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('sku_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </form>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        @if(Request::get('id') != null)
            // $('select[name="product_name"]').select2('readonly',true);
            $('select[name="product_name"]').siblings('.select2-container').addClass('pointer-none');
        @endif

        var validationRules = {
            sku_name: {
                required: true
            },
            sku_description: {
                required: true
            }
        };
        var validationMessages = {
            sku_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.sku_name')]) }}"
            },
            sku_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.sku_description')]) }}"
            }
        }

        $('form#stockKeepingUnitForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/sku") }}')

        exitPopup('{{ route("admin/sku") }}');
    </script>
    @endif
@endsection
