@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_tag') : __('page.edit_tag')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li >{{ __('menubar.tag') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_tag') : __('page.edit_tag') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_tag') : __('page.edit_tag') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page') 
                                        @else
                                            <form method="POST" id="tagForm" action="{{ route('admin/tag/form') }}" enctype="multipart/form-data">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        <div class="col-md-4 small-img padding-r-10">
                                                            <div id="showing_product_image">
                                                                <img src="{{ Request::get('id') == null ? asset('img/default_product.jpg') : asset($tag_by_id['tag_image']) }}" alt="" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 res-left-form res-no-pad-left">
                                                                @csrf
                                                                @if(Request::get('id') != null)
                                                                    <input type="hidden" name="tag_id" value="{{ Request::get('id') }}">
                                                                @endif
                                                                <div class="info">
                                                                    <i class="fa fa-info-circle"></i>
                                                                    {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                                </div>     
                                                                <div class="form-group">
                                                                    <label for="tag_name">{{ __('field.tag_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('tag_name') is-invalid @enderror" name="tag_name" autocomplete="off" @if(Request::get('id') == null) value="{{ old('tag_name') }}" @else value="{{ $tag_by_id['tag_name'] }}"  @endif>
                                                                    @error('tag_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('tag_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="tag_description">{{ __('field.tag_description') }} <span class="required-label">*</span></label>
                                                                    <textarea class="form-control @error('tag_description') is-invalid @enderror" style="resize:none; height: 192px;" name="tag_description">@if(Request::get('id') == null){{ old('tag_description')}}@else{{ $tag_by_id['tag_desc']}}@endif</textarea>
                                                                    @error('tag_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('tag_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group no-margin">
                                                                    <label style="display: block;" for="tag_image">{{ __('field.tag_image') }} <span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    <div class="input-img-name">
                                                                        <span id="image_name">{{ __('page.no_data') }}</span>
                                                                        <button type="button" class="btn btn-green-upload no-margin" id="tag_image_button">{{ __('page.upload') }}</button> 
                                                                    </div>
                                                                    <input type="file" name="tag_image" class="form-control" style="display: none;">
                                                                    @error('tag_image')
                                                                        <span class="invalid-feedback">{{ $errors->first('tag_image') }}</span>
                                                                    @enderror
                                                                </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 no-padding">
                                                    <div class="card-footer bg-transparent ">
                                                        <div class="col-md-4 no-padding float-right">
                                                            <div class="col-xs-6 padding-r-10">
                                                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                            </div>
                                                            <div class="col-xs-6 padding-l-10">
                                                            <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </form>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        clickCancelButton('{{ route("admin/tag") }}')

        exitPopup('{{ route("admin/tag") }}');

        @if(Request::get('id') == null)
            var validationRules = {
                tag_name: {
                    required: true,
                    // minlength: 5,
                    alpha: true
                },
                tag_description: {
                    required: true
                    // minlength: 10
                },
                tag_image: {
                    required: true,
                    extension: "png|pneg|svg|jpg|jpeg",
                    maxsize: 2000000
                }
            };

            var validationMessages = {
                tag_name: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.tag_name')]) }}",
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.tag_name'), 'min' => 5]) }}",
                    alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.tag_name')]) }}"
                },
                tag_description: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.tag_description')]) }}",
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.tag_description'), 'min' => 10]) }}"
                },
                tag_image: {
                    required: "{{ __('validation.custom.tag_image.required',['attribute' => __('validation.attributes.tag_image')]) }}",
                    extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.tag_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                    maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
                }
            };
        @else
            var validationRules = {
                tag_name: {
                    required: true,
                    // minlength: 5,
                    alpha: true
                },
                tag_description: {
                    required: true
                    // minlength: 10
                },
                tag_image: {
                    required: true,
                    extension: "png|pneg|svg|jpg|jpeg",
                    maxsize: 2000000
                }
            };

            var validationMessages = {
                tag_name: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.tag_name')]) }}",
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.tag_name'), 'min' => 5]) }}",
                    alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.tag_name')]) }}"
                },
                tag_description: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.tag_description')]) }}"
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.tag_description'), 'min' => 10]) }}"
                },
                tag_image: {
                    required: "{{ __('validation.custom.tag_image.required',['attribute' => __('validation.attributes.tag_image')]) }}",
                    extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.tag_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                    maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
                }
            };
        @endif

        $('form#tagForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#tag_image_button').click(function(e){
            e.preventDefault();
            $('input[name="tag_image"]').click();
        });

        $('input[name="tag_image"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });
    </script>
    @endif
@endsection
