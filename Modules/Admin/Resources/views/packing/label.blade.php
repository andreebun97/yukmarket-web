@extends('admin::layouts.master')
@section('title',__('page.packing_label'))
@section('content')

<style type="text/css">
    #hidden_div:first-child{
        display: block;
    }
    #hidden_div{
        display: none;
    }    
    .print-page{
        display: none;
    }
</style>

    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'transaction'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <li>{{ __('menubar.packing') }}</li>
                                <li>{{ __('page.packing_form') . ' ('.$purchased_products[0]['order_code'].')' }}</li>
                                <li class="active text-bold ">{{ __('page.packing_label') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content vh100">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat fit-height">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body fit-height hmax-calc104">
                                            <div id="printableArea">
                                                @foreach($loop_receipt as $receipt1)
                                                    <!-- loop -->
                                                    <div class="struk-wrap size-a6" id="hidden_div">
                                                        <div class="struk">
                                                            <div class="yukmarket-logo">
                                                                <center><img src="{{ asset($logo) }}" alt=""></center>
                                                                <span class="print-page">{{$receipt1+0}} / {{ $total_pack }}</span>
                                                            </div>
                                                            @php
                                                                $main_code = ($purchased_products[0]['organization_type_id'] == 4 ? ($purchased_products[0]['shipping_receipt_num'] == null ? $purchased_products[0]['invoice_no'] : $purchased_products[0]['shipping_receipt_num']) : $purchased_products[0]['order_code'])
                                                            @endphp
                                                            <div class="barcode">
                                                                <center>
                                                                     {!! '<img class="w100 pad-lr-40" src="data:image/png;base64,' . DNS1D::getBarcodePNG($main_code, 'C39',1.8,60) . '" alt="barcode"/>'  !!}
                                                                 </center>
                                                            </div>
                                                            <div class="resi-number">
                                                                <center><span class="d-block padding-20 font-bold color-black">{{ $main_code }}</span></center>
                                                            </div>
                                                            <!-- <div class="col-md-12 col-xs-12 pad-t-10 title-color-green text-bold resi-bold"> -->
                                                                <!-- <div class="col-xs-2 no-padding">
                                                                    <span>{{ __('field.order_code') }}</span>
                                                                </div> -->
                                                                <!-- <div class="col-xs-10 no-padding">
                                                                    
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div> -->
                                                            <!-- <div class="col-xs-12 no-padding margin-b5">
                                                                <div class="col-xs-2 no-padding">
                                                                    <span>{{ __('field.shipping_receipt_num') }}</span>
                                                                </div>
                                                                <div class="col-xs-10 no-padding">
                                                                    <span class="d-block">{{ $purchased_products[0]['shipping_receipt_num'] }}</span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div> -->
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="col-md-12 col-sm-12  col-xs-12  no-border">
                                                                    <div class="col-xs-4 no-padding margin-b5">
                                                                        <h5 style="font-weight:bold">Penerima</h5>
                                                                        <!-- <div class="col-xs-2 no-padding">
                                                                            <span>{{ __('field.user_name') }}</span>
                                                                        </div> -->
                                                                    </div>
                                                                    <div class="col-xs-8 no-padding margin-b5 margin-t12">
                                                                        <div class="col-xs-10 no-padding">
                                                                            <span class="d-block">{{ $purchased_products[0]->contact_person == null ? ($purchased_products[0]->customer_name != null ? $purchased_products[0]->customer_name : $purchased_products[0]->buyer_name) : $purchased_products[0]->contact_person }}</span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <!-- <div class="col-xs-2 no-padding">
                                                                            <span>{{ __('field.destination_address') }}</span>
                                                                        </div> -->
                                                                        <div class="col-xs-10 no-padding">
                                                                            <span class="d-block">{{ $purchased_products[0]['destination_address'] != null ? ($purchased_products[0]['address_detail'] . ', ' . $purchased_products[0]['kabupaten_kota_name'] . ', ' . $purchased_products[0]['kecamatan_name'] . ', ' . $purchased_products[0]['kelurahan_desa_name'] . ', ' . $purchased_products[0]['kode_pos'] . '('.($purchased_products[0]['address_info'] == "" ? "-" : $purchased_products[0]['address_info']).')') : $purchased_products[0]['buyer_address'] }}</span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="col-xs-12 no-padding margin-b5">
                                                                            <!-- <div class="col-xs-2 no-padding">
                                                                                <span>{{ __('field.contact_person') }}</span>
                                                                            </div> -->
                                                                            <div class="col-xs-10 no-padding">
                                                                                <span class="d-block">{{ $purchased_products[0]->phone_number == null ? $phone_number->convert($purchased_products[0]['customer_phone_number']) : ($phone_number->convert($purchased_products[0]['phone_number']) ) }}</span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <!-- <div class="col-xs-2 no-padding">
                                                                                <span>{{ __('field.user_email') }}</span>
                                                                            </div> -->
                                                                            <!-- <div class="col-xs-10 no-padding">
                                                                                <span class="d-block">{{ $purchased_products[0]['customer_email'] }}</span>
                                                                            </div>
                                                                            <div class="clearfix"></div> -->
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12 no-border">
                                                                    <div class="col-xs-4 no-padding margin-b5">
                                                                        <h5 style="font-weight:bold">Pengirim</h5>
                                                                        <!-- <div class="col-xs-1 no-padding">
                                                                            <span class="ico-print"><img src="{{ asset('img/label/ic_home.png') }}" ></span>
                                                                        </div> -->
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <!-- <div class="col-xs-12 no-padding margin-b5">
                                                                        
                                                                        <div class="col-xs-11 no-padding">
                                                                            <span class="d-block">Komplek Aldiron Hero Block C No.9-10, Jl. Daan Mogot Kav.119, Kebon Jeruk, RT.6/RW.5, Duri Kepa, Kec. Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11510</span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div> -->
                                                                    <div class="col-xs-8 no-padding margin-b5 margin-t12">
                                                                        <!-- <div class="col-xs-1 no-padding">
                                                                            <span class="ico-print"><img src="{{ asset('img/label/ic_phone.png') }}" ></span>
                                                                        </div> -->
                                                                        <div class="col-xs-11 no-padding">
                                                                            <span class="d-block">YukMarket!</span>
                                                                        </div>
                                                                        <div class="col-xs-11 no-padding">
                                                                            <span class="d-block">{{ $phone_number->convert(5663705) }}</span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>     
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- <div class="yukmarket-transaction-detail">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No</th>
                                                                            <th>{{ __('field.product_name') }}</th>
                                                                            <th>{{ __('field.product_quantity') }}</th>
                                                                            <th>{{ __('field.product_price') }}</th>
                                                                            <th>{{ __('field.total') }}</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if(count($purchased_products) == 0)
                                                                        <tr>
                                                                            <td colspan="4">{{ __('page.no_data') }}</td>
                                                                        </tr>
                                                                        @else
                                                                            @php $grand_total = 0; $total_quantity = 0; @endphp
                                                                            @for($b = 0; $b < count($purchased_products); $b++)
                                                                                @php $total_per_product = ($purchased_products[$b]['purchased_quantity'] * $purchased_products[$b]['purchased_price']); @endphp
                                                                                @php $grand_total += $total_per_product; $total_quantity += $purchased_products[$b]['purchased_quantity']; @endphp
                                                                                <tr>
                                                                                    <td>{{ $b + 1 . "." }}</td>
                                                                                    <td>{{ $purchased_products[$b]['purchased_product_name'] }}</td>
                                                                                    <td>{{ $purchased_products[$b]['purchased_quantity'] }}</td>
                                                                                    <td style="text-align:right">{{ 'Rp. '.$currency->convertToCurrency($purchased_products[$b]['purchased_price']) }}</td>
                                                                                    <td style="text-align:right">{{ 'Rp. '.$currency->convertToCurrency($total_per_product) }}</td>
                                                                                </tr>
                                                                            @endfor
                                                                            <tr>
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.subtotal') }} @if($purchased_products[0]['pricing_include_tax'] == 1) (+{{ __('field.national_income_tax') }}) @endif</strong></td>
                                                                                <td class="subtotal" style="text-align:right">{{ 'Rp. '.$currency->convertToCurrency($grand_total) }}</td>
                                                                            </tr>
                                                                            @php
                                                                                $tax_basis = ROUND($grand_total / 1.1);
                                                                                $national_income_tax = $grand_total - $tax_basis;
                                                                                if($purchased_products[0]['pricing_include_tax'] == 0){
                                                                                    $tax_basis = 0;
                                                                                    $national_income_tax = $grand_total * $purchased_products[0]['national_income_tax'];
                                                                                    $grand_total += $national_income_tax;
                                                                                }
                                                                            @endphp
                                                                            @if($purchased_products[0]['pricing_include_tax'] == 0)
                                                                            <tr>
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.national_income_tax') }} @if($purchased_products[0]['pricing_include_tax'] == 0) {{ '('.($purchased_products[0]['national_income_tax']*100).'%)' }}@endif</strong></td>
                                                                                <td class="subtotal" style="text-align:right">{{ 'Rp. '.$currency->convertToCurrency($national_income_tax) }}</td>
                                                                            </tr>
                                                                            @endif
                                                                            @php
                                                                                $voucher_amount = 0;
                                                                            @endphp
                                                                            <tr class="no-border">
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.voucher') . ($purchased_products[0]['voucher_id'] == null ? '' : ' ('.$purchased_products[0]['voucher_code'].')') }}</strong></td>
                                                                                <td class="subtotal color-red" style="text-align:right">{{ $voucher_amount == 0 ? "-" : 'Rp. '.('-'.$currency->convertToCurrency($voucher_amount)) }}</td>
                                                                            </tr>
                                                                            @php
                                                                                $grand_total = $grand_total - $voucher_amount;
                                                                                $admin_fee = 0;
                                                                                
                                                                                if($grand_total < 0){
                                                                                    $grand_total = 0;
                                                                                }
                                                                            @endphp
                                                                            @php
                                                                                $shipment_fee = 0;
                                                                                if($purchased_products[0]['shipment_price'] != null){
                                                                                    $shipment_fee = $purchased_products[0]['shipment_price'];
                                                                                }
                                                                            @endphp
                                                                            <tr class="no-border">
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.shipment_fee') }}</strong></td>
                                                                                <td class="subtotal" style="text-align:right">{{ 'Rp. '.$shipment_fee == 0 ? "-" : $currency->convertToCurrency($shipment_fee) }}</td>
                                                                            </tr>
                                                                            <tr class="no-border">
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.admin_fee') }}</strong></td>
                                                                                <td class="subtotal" style="text-align:right">{{ $admin_fee == 0 ? "-" : 'Rp. '.$currency->convertToCurrency($admin_fee) }}</td>
                                                                            </tr>
                                                                            <tr class="no-border">
                                                                                <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.total') }}</strong></td>
                                                                                <td class="subtotal" style="text-align:right"><strong>{{ 'Rp. '.$currency->convertToCurrency($grand_total + $admin_fee + $shipment_fee) }}</strong></td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                                    
                                                                <div class="clearfix"></div>
                                                                <div class="thanks">
                                                                    <p>Terima kasih telah melakukan transaksi di toko kami. Semoga Anda senang dengan pelayanan kami.</p>
                                                                </div>
                                                            </div> -->
                                                            
                                                       
                                                        <div class="clearfix"></div>
                                                        <div class="footer-resi">
                                                            <div class="col-md-12 text-bold margin-b20">
                                                                <div class="col-md-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                        <span class="d-block">{{ __('field.shipment_method') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                        <span class="d-block">: {{ $purchased_products[0]['shipment_method_id'] == null ? "-" : $purchased_products[0]['shipment_method_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-md-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                        <span class="d-block">{{ __('field.shipment_price') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                        <span class="d-block">: {{ 'Rp. '.($currency->convertToCurrency($purchased_products[0]['shipment_price'])) }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <input type="hidden" name="total_pack" value="{{ $purchased_products[0]['total_pack'] == null ? 1 : $purchased_products[0]['total_pack'] }}">

                                                                <div class="clearfix"></div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="clearfix"></div>
                                                    <!-- end loop -->
                                                @endforeach
                                            <div class="btn-wrap-print-a5 margin-b20">
                                                    <div class="col-md-12 no-padding"> 
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <button class="btn btn-gray btn-block d-none"  disabled>{{ __('page.back') }}</button>
                                                        </div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            @if($receipt1 == 1)
                                                             <button class="btn btn-green btn-block" onclick="printDiv('hidden_div')">Print</button>
                                                            @else
                                                            <button class="btn btn-green btn-block" onclick="printDiv('printableArea')">Print</button>
                                                            @endif
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
                window.print();
            document.body.innerHTML = originalContents;
        }

        window.onafterprint = function(){
            window.location.href = '{{ route("admin/packing/form","?id=".$purchased_products[0]["order_id"]) }}';
        }
    </script>
@endsection