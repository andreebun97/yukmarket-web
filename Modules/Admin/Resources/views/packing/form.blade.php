
@extends('admin::layouts.master')

@section('title',__('page.packing_form'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li>{{ __('menubar.packing') }}</li>
                                <li class="active text-bold ">{{ __('page.packing_form') . ' ('.$order_master->order_code.')' }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.packing_form') . ' ('.$order_master->order_code.')' }}</h4>
                                            <div class="clearfix"></div>
                                        </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <div class="row no-padding">
                                                <div class="col-md-12 trs-detail">
                                                    <div class="col-md-6 no-padding">
                                                        <div class="info-detail">
                                                            <h2>{{ __('page.receiver_info') }}</h2>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-6" for="user_name">{{ __('field.user_name') }}</label>
                                                                <span class="col-md-6 user_data_information">
                                                                {{ $order_master->contact_person == null ? ($order_master->customer_name == null ? $order_master->buyer_name : $order_master->customer_name) : $order_master->contact_person }}
                                                                </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-6  " for="destination_address">{{ __('field.destination_address') }}</label>
                                                                <span class="col-md-6 user_data_information">{{ $order_master->destination_address == null ? ($order_master->buyer_address == null ? "-" : $order_master->buyer_address) :  $order_master->address_detail . ', ' . ($order_master->kelurahan_desa_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kabupaten_kota_name . ', ' . $order_master->kode_pos . " (".($order_master->address_info == null ? "-" : $order_master->address_info).", ".($order_master->address_name == null ? "-" : $order_master->address_name).")") }}</span>    
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <!-- <div class="form-group  no-padding">
                                                                <label class="col-md-6" for="contact_person">{{ __('field.contact_person') }}</label>
                                                                <span class="col-md-6 user_data_information">{{ $order_master->contact_person == null ? $order_master->customer_name . ' ('.$phone_number->convert($order_master->customer_phone_number).')' : ($order_master->contact_person . ' (' . $phone_number->convert($order_master->phone_number) . ')') }}</span>    
                                                            </div>
                                                            <div class="clearfix"></div> -->
                                                            <div class="form-group  no-padding">
                                                                <label class="col-md-6"   for="payment_method">{{ __('field.shipment_method') }}</label>
                                                                <span class="col-md-6 user_data_information">{{ $order_master->shipment_method_name }}</span>    
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 no-padding">
                                                        <div id="user_profile " class="info-detail">
                                                            <h2>{{ __('page.sender_info') }}</h2>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-6" for="user_name">{{ __('field.sender_name') }}</label>
                                                                <span class="col-md-6 user_data_information">
                                                                YukMarket!
                                                                </span>
                                                            </div>
                                                            <!-- <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-6" for="user_email">{{ __('field.sender_address') }}</label>
                                                                <span class="col-md-6 user_data_information">Komplek Aldiron Hero Block C No.9-10, Jl. Daan Mogot Kav.119, Kebon Jeruk, RT.6/RW.5, Duri Kepa, Kec. Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11510</span>
                                                            </div> -->
                                                            <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-6" for="sender_phone_number">{{ __('field.sender_phone_number') }}</label>
                                                                <span class="col-md-6 dd user_data_information">(021) 5663705</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="margin-tb-10"></div>
                                                <div class="separator no-margin"></div>
                                                <div class="clearfix"></div>
                                                <div class="margin-tb-10"></div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12">
                                                    <div id="user_transaction" class="info-detail">
                                                        <h2>{{ __('page.transactions') }}</h2>
                                                        <div id="purchase_transaction">
                                                        @if(count($transactions) == 0)  
                                                            <span>{{ __('page.no_data') }}</span>
                                                        @else
                                                            <div class="form-group">
                                                                <div id="purchase_transaction_detail">
                                                                    <div class="col-xs-12 no-padding res-scroll-md">
                                                                        <table class="table display w-100 table-brown total-transaksi" id="product_list_table">
                                                                            <thead class="bg-darkgrey">
                                                                                <tr role="row">
                                                                                    <th>Gambar</th>
                                                                                    <th>Kode</th>
                                                                                    <th style="min-width: 200px;" >Nama</th>
                                                                                    <th class="text-right">Jumlah</th>
                                                                                    <th class="text-right">Harga</th>
                                                                                    <th class="text-right">Total</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @php
                                                                                $total_product = 0;
                                                                                $total_weight = 0;
                                                                                $subtotal = 0;
                                                                                $total_dimension = 0;
                                                                            @endphp
                                                                            @for($b = 0; $b < count($transactions); $b++)
                                                                                @php
                                                                                    $subtotal += $transactions[$b]['quantity'] *  $transactions[$b]['price'];
                                                                                    $total_product += $transactions[$b]['quantity'];
                                                                                    $dim_length = ($transactions[$b]['dim_length']);
                                                                                    $dim_width = ($transactions[$b]['dim_width']);
                                                                                    $dim_height = ($transactions[$b]['dim_height']);
                                                                                    $product_weight = $transactions[$b]['bruto'] * $transactions[$b]['quantity'];
                                                                                    $total_weight += $product_weight;
                                                                                    $total_dimension = ($dim_length * $dim_width * $dim_height * $transactions[$b]['quantity']);
                                                                                @endphp
                                                                                <tr {{ $b < count($transactions) - 1 ? '' : ''}}>
                                                                                    <td><img src="{{ asset($transactions[$b]['prod_image']) }}" class="img-50" alt="" onerror="this.src='{{ asset('img/default_product.jpg') }}'"></td>
                                                                                    <td>{{ $transactions[$b]['prod_code'] }}</td>
                                                                                    <td>{{ $transactions[$b]['prod_name'] . ($transactions[$b]['sku_status'] == 0 ? '' : (' '.($transactions[$b]['uom_value']+0).' '.$transactions[$b]['uom_name'])) }}</td>
                                                                                    <td class="text-right">{{ $transactions[$b]['quantity'] }}</td>
                                                                                    <td class="text-right">{{ $currency->convertToCurrency($transactions[$b]['price']) }}</td>
                                                                                    <td class="text-right">{{ $currency->convertToCurrency($transactions[$b]['quantity'] *  $transactions[$b]['price']) }}</td>
                                                                                </tr>
                                                                                @endfor
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="col-md-6 col-lg-4 col-sm-6 col-xs-7 no-padding float-right">
                                                                        <div class="total-wrap">
                                                                            <div class="col-md-12 no-padding">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span  class="dp-block color-black">{{ __('field.subtotal') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($subtotal) }}</span>
                                                                                </div>
                                                                            </div>
                                                                            @php
                                                                                $national_income_tax = 0;
                                                                                $tax_basis = 0;
                                                                                if($order_master->pricing_include_tax == 0){
                                                                                    $national_income_tax = $subtotal * $order_master->national_income_tax;
                                                                                    $subtotal = $subtotal + $national_income_tax;
                                                                                }else{
                                                                                    $tax_basis = ROUND($subtotal / 1.1);
                                                                                    $national_income_tax = $subtotal - $tax_basis;
                                                                                }
                                                                            @endphp
                                                                            @if($order_master->pricing_include_tax == 1)
                                                                            <div class="col-md-12 no-padding tax-dpp">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span  class="dp-block color-black">{{ __('field.tax_basis') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($tax_basis) }}</span>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                            <div class="col-md-12 no-padding tax-ppn">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span  class="dp-block color-black">{{ __('field.national_income_tax') }} @if($order_master->pricing_include_tax == 0) {{ '('.($order_master->national_income_tax*100).'%)' }} @endif</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($national_income_tax) }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 no-padding d-none">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span class="dp-block color-black">{{ __('field.voucher') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    @php
                                                                                    $voucher_amount = 0;
                                                                                    
                                                                                    @endphp
                                                                                    <span class="float-right dp-block color-red" > {{ $order_master->voucher_id == null ? '-' : ($order_master->is_fixed == 'Y' ? $currency->convertToCurrency($order_master->voucher_amount) : '-'.$currency->convertToCurrency($voucher_amount)) }}</span>
                                                                                    @php $subtotal_and_amount = $subtotal - $voucher_amount; @endphp
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 no-padding">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span class="dp-block color-black">{{ __('field.shipment_fee') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black"> {{ $order_master->shipment_price == null ? '-' : $currency->convertToCurrency($order_master->shipment_price) }}</span>
                                                                                    @php
                                                                                        $all_admin_fee = 0;
                                                                                        $grand_total = $all_admin_fee + ($order_master->shipment_price == null ? 0 : $order_master->shipment_price) + $subtotal_and_amount;
                                                                                        $admin_fee = 0;
                                                                                    @endphp
                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="col-md-12 no-padding">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span class="dp-block color-black">{{ __('field.product_quantity') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black"> {{ $total_product.' Pcs' }}</span>
                                                                                </div>
                                                                            </div> -->
                                                                            <div class="col-md-12 no-padding d-none">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span class="dp-block color-black">{{ __('field.admin_fee') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black">{{ $order_master->admin_fee == null && $order_master->admin_fee_percentage == null ? '-' : $currency->convertToCurrency($admin_fee) }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 no-padding">
                                                                                <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                    <span class=" color-black dp-block font-bold">{{ __('field.total') }}</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                    <span class="float-right dp-block color-black font-bold">{{ $currency->convertToCurrency($grand_total) }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>  
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <form method="POST" id="packingConfirmationForm" action="{{ route('admin/order/packing') }}" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <input type="hidden" name="order_id" value="{{ $order_master->order_id }}">
                                                                        @for($b = 0; $b < count($transactions); $b++)
                                                                            <input type="hidden" name="purchased_products[]" value="{{ $transactions[$b]['prod_id'] }}">
                                                                            <input type="hidden" name="purchased_quantity[]" value="{{ $transactions[$b]['quantity'] }}">
                                                                        @endfor
                                                                    </form>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($order_master->is_printed_invoice == 1)
                                                        <div class="col-md-12 margin-b20 no-padding">
                                                            <form id="submitPackingDetailForm" method="POST" action="{{ route('admin/packing/submit_detail') }}">
                                                                @csrf
                                                                <input type="hidden" name="order_id" value="{{ Request::get('id') }}">
                                                                <div class="col-md-6 d-none">
                                                                    <div class="form-group">
                                                                        <label for="product_weight">{{ __('field.bruto') }}</label>
                                                                        <input type="text" class="form-control" value="{{ $order_master->total_weight != null ? $order_master->total_weight : $total_weight }}" @if($order_master->total_weight != null) readonly @endif name="product_weight"><span class="uom-inbox">kg</span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="total_dimension">{{ __('field.total_dimension') }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="total_dimension" id="total_dimension" class="form-control @error('total_dimension') is-invalid @enderror" value="{{ sprintf('%f', ($order_master->total_dimension != null ? ($order_master->total_dimension) : ($total_dimension / 1000000))+0) }}" readonly><span class="uom-inbox">m<sup>3</sup></span>
                                                                        @error('total_dimension')
                                                                            <span class="invalid-feedback">{{ $errors->first('total_dimension') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 d-none">
                                                                    <div class="form-group">
                                                                        <label for="total_pack">{{ __('field.total_pack') }}</label>
                                                                        <input type="text" class="form-control" name="total_pack" @if($order_master->total_pack != null) value="{{ $order_master->total_pack }}" readonly @else value="1" @endif><span class="uom-inbox">Bungkus</span>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>                      
                                                <div class="clearfix"></div>
                                                @endif
                                            </div>
                                            <div class="float-left no-padding res-multi-btn res-btn">
                                                <div class="no-pad-right res-no-pad-xs display-inline-block">
                                                    <button type="button" class="btn btn-print btn-block" onclick="window.location.href = '{{ route("admin/transaction/label","?id=".Request::get('id')) }}'"><i class="fa fa-print"></i> {{ __('page.print_shipping_label') }}</button>
                                                </div>
                                                <div class="no-pad-right res-no-pad-xs display-inline-block">
                                                    <button type="button" class="btn btn-print btn-block" id="submitPackingDetailButton" {{ $order_master->is_printed_invoice == 1 ? 'disabled' : '' }}><i class="fa fa-print"></i> {{  __('page.print_invoice') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-transparent ">
                                            <div class="col-xs-12 col-sm-9 float-right no-padding res-multi-btn res-btn">
                                                <div class="col-md-3 col-sm-4 col-xs-12 no-pad-right res-no-pad-xs">
                                                    <button type="button" class="btn btn-green btn-block" id="packingConfirmationButton" {{ $order_master->current_preparation_status_id != 4 ? '' : 'disabled' }}>{{ __('page.confirm') }}</button>
                                                </div>
                                                <div class="col-md-3 col-sm-4 col-xs-12 no-padding res-no-pad-xs">
                                                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#failedPickingForm" {{ $order_master->current_preparation_status_id != 4 && $order_master->is_printed_invoice == 0 ? '' : 'disabled' }}>{{ __('page.back_to_picking') }}</button>
                                                </div>
                                                <div class="col-md-3 col-sm-4 col-xs-12 no-pad-left res-no-pad-xs">
                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.back') }}</button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @if($order_master->shipping_receipt_num == null)
    <!-- Modal -->
    <div class="modal fade" id="failedPickingForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="failedPickingFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="failedPickingFormLabel">{{ __('page.packing_rejection') }}</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    
                </div>
                <div class="modal-body">
                    <form id="failedPickingForm" method="POST" action="{{ route('admin/packing/reject') }}">
                        @csrf
                        <input type="hidden" name="preparation_id" value="{{ $order_master->preparation_id }}">
                        <input type="hidden" name="order_id" value="{{ Request::get('id') }}">
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="rejected_reason">{{ __('field.rejected_reason') }} <span class="required-label">*</span></label>
                                    <textarea name="rejected_reason" class="form-control" id="rejected_reason" style="resize:none;height:150px"></textarea>
                                </div>
                            </div>                            
                            <div class="col-sm-12 no-padding btn-footer margin-t10">
                                <div class="col-md-6 padding-r-10 ">
                                    <button type="button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <button type="submit" id="failedPickingButton" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                </div>
                            </div>
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var validationRules = {
            rejected_reason: {
                required: true,
                minlength: 10
            }
        };

        var validationMessages = {
            rejected_reason: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.rejected_reason')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.rejected_reason'), 'min' => 10]) }}"
            }
        };

        $('form#failedPickingForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();                
            }
        });

        $('button#packingConfirmationButton').click(function(){
            $('form#packingConfirmationForm').submit();
        });

        @if($order_master->is_printed_invoice == 1)
        validationRules = {
            product_weight: {
                required: true
            },
            total_dimension: {
                required: true
            },
            total_pack: {
                required: true,
                min: 1
            }
        };

        validationMessages = {
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            total_dimension: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.total_dimension')]) }}"
            },
            total_pack: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.total_pack')]) }}",
                min: "{{ __('validation.min.string',['attribute' => __('validation.attributes.total_pack'), 'min' => 10]) }}"
            }
        };

        $('#submitPackingDetailButton').click(function(){
            $('form#submitPackingDetailForm').submit();
        });

        $('form#submitPackingDetailForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });
        @else
        $('#submitPackingDetailButton').click(function(){
            $('form#submitPackingDetailForm').submit();
            
            window.location.href = '{{ route("admin/transaction/invoice","?id=".$order_master->order_id) }}';
        });
        @endif
        // $('#failedPickingButton').click(function(){
        //     $('#failedPickingForm').submit();
        // });

        clickCancelButton('{{ route("admin/packing") }}')

        exitPopup('{{ route("admin/packing") }}');
    </script>
@endsection
