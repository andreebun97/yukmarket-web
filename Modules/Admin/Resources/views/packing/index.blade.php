
@extends('admin::layouts.master')

@section('title',__('menubar.packing'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>Transaksi</li>
                                <li class="active text-bold ">{{ __('menubar.packing') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flatt" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.packing') }}</h4>
                                            <div class="col-xs-4 no-padding float-right">
                                                <div class="form-group no-margin">
                                                    <label for="activity">{{ __('page.activity') }}</label>
                                                    <select name="packing_activity" id="packing_activity" class="form-control single-select">
                                                        <option value="">{{ __('page.show_all') }}</option>
                                                        <option value="print_invoice" {{ Request::get('activity') == 'print_invoice' ? 'selected' : '' }}>{{ __('page.print_invoice') }}</option>
                                                        <option value="confirm_packing" {{ Request::get('activity') == 'confirm_packing' ? 'selected' : '' }}>{{ __('page.confirm_packing') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                                <table class="table display w-100 table-brown" id="packing_list_table">
                                                    <thead class="bg-darkgrey">
                                                        <tr role="row">
                                                            <th>{{ __('field.action') }}</th>
                                                            <th>No.</th>
                                                            <th>{{ __('field.order_code') }}</th>
                                                            <th>{{ __('field.customer_name') }}</th>
                                                            <th>{{ __('field.purchased_date') }}</th>
                                                            <th>{{ __('field.confirmation_date') }}</th>
                                                            <th>{{ __('field.order_status') }}</th>
                                                            <th>{{ __('field.shipment_method') }}</th>
                                                            <th>{{ __('field.picked_by') }}</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                @php
                                                    $button_name = (Request::get('activity') == 'print_invoice' ? 'printInvoiceButton' : 'confirmPicking');
                                                    $button_type = (Request::get('activity') == 'print_invoice' ? 'button' : 'submit');
                                                @endphp
                                                <form @if(Request::get('activity') == 'confirm_packing') action="{{ route('admin/order/packing') }}" method="POST" @endif>
                                                    @csrf
                                                    <input type="hidden" name="order_id">
                                                    <button type="{{ $button_type }}" class="btn btn-orange d-none" id="{{ $button_name }}">@if(Request::get('activity') == 'print_invoice')<i class="fa fa-print"></i>@endif {{ __('page.'.Request::get('activity')) }}</button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        clickCancelButton('{{ Request::url() }}');

        exitPopup('{{ Request::url() }}');

        $('select[name="packing_activity"]').change(function(){
            if($(this).val() == 'print_invoice'){
                window.location.href = "{{ route('admin/packing') }}"+'?activity='+$(this).val();
            }else if($(this).val() == 'confirm_packing'){
                window.location.href = "{{ route('admin/packing') }}"+'?activity='+$(this).val();
            }else{
                window.location.href = "{{ route('admin/packing') }}";
            }
            showLoader();
        });
        

        var packingTable = $('#packing_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX:true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="packing_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_ready_order") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.url = "{{ Request::fullUrl() }}";
                    d.activity = "{{ Request::get('activity') }}";
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.organization = "{{ $user['organization_type_id'] == 1 ? 'all_agent' : $user['organization_id'] }}";
                    d.parent_organization = "{{ $user['organization_type_id'] == 1 ? 'all_agent' : $user['parent_organization_id'] }}";
                }
            },
            order: [[ 5, "asc" ]],
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {
                    data: 'preparation_date',
                    name: 'preparation_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {data: 'order_status_name', name: 'order_status_name'},
                {data: 'shipment_method_name', name: 'shipment_method_name'},
                {data: 'picker_name', name: 'picker_name'},
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        packingTable.on('click','input[name="checked_order_id[]"]',function(){
            var checked_box = [];
            var checked_box_url = "";
            var url = "{{ route('admin/packing/invoice/multiple') }}";
            $('#packing_list_table tbody tr input[name="checked_order_id[]"]:checked').each(function(i){
                checked_box[i] = $(this).val();
            });

            if(checked_box.length == 0){
                $('#'+'{{ $button_name }}').addClass('d-none');
                $('input[name="order_id"]').val("");
            }else{
                checked_box_url = url+'?id='+checked_box.toString();
                $('#'+'{{ $button_name }}').removeClass('d-none');
                @if(Request::get('activity') == 'print_invoice')
                    $('#'+'{{ $button_name }}').attr('onclick','window.location.href = "'+checked_box_url+'"');
                @else
                    $('input[name="order_id"]').val(checked_box.toString());
                @endif
            }
        })
    </script>
    @endif
@endsection
