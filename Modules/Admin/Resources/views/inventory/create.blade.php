
@extends('admin::layouts.master')

@section('title',__('menubar.incoming_goods'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.incoming_goods') }}</li>

                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.incoming_goods') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>

                                        </div>
                                        <form id="inventory_form" action="">
                                            <div class="panel-body">
                                                <input type="hidden" class="list_prod" value="">
                                                <div class="form_header">
                                                    <div class="col-xs-12 no-padding">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="inventory_org" value="{{$org_id}}">
                                                        <div class="col-md-8 col-sm-12 no-pad-left res-no-pad">
                                                            <div class="col-sm-6 no-pad-left res-no-pad-sm">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$no_trx}}" readonly="readonly" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                        <div class="clearfix"></div>
                                                                        @if(is_null($user_wh_org->warehouse_id))
                                                                            <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('product_category') is-invalid @enderror" required>
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                @foreach($all_warehouse as $warehouse)
                                                                                <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        @else
                                                                            <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('product_category') is-invalid @enderror">
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                <option value="{{$user_wh_org->warehouse_id}}"> {{$user_wh_org->warehouse_name}}</option>
                                                                            </select>
                                                                            <!-- <input type="hidden" name="inventory_warehouse" class="inventory_warehouse" value="{{$user_wh_org->warehouse_id}}">
                                                                            <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user_wh_org->warehouse_name}}" readonly="readonly" /> -->
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 no-pad-left res-no-pad-sm">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Masuk <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <span class="input-group-addon-custom"><i class="icon-calendar22"></i></span>
                                                                        <!-- <input type="date"  class="form-control daterange-single"   name="inventory_date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}" readOnly> -->
                                                                        <!-- <input type="text"  class="form-control daterange-single"   name="inventory_date"  value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"  readOnly> -->
                                                                        <input type="text"  class="form-control"   name="inventory_date"  value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"  readOnly>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                        <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror" required>
                                                                            <option value="">-- Pilih Status --</option>
                                                                            <option value="0">Tertunda</option>
                                                                            <option value="1">Selesai</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4 no-pad-right res-no-pad-left">
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="form-group">
                                                                    <label for="product_stock">Keterangan</label>
                                                                    <textarea type="text" name="inventory_desc" class="form-control inventory_desc" value="" style="resize: none;height: 120px;"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12 no-padding form_body">
                                                    <div class="col-xs-12 no-padding res-scroll">
                                                        <table class="table display w-100 table-brown border-head datatable-basic" id="detailTrx" style="display: none;">
                                                            <thead class="bg-darkgrey w-100">
                                                                <tr>
                                                                    <th></th>
                                                                    <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                    <th class="text-center">ID Produk<span class="required-label">*</span></th>
                                                                    <th class="text-center">Satuan Product</th>
                                                                    <th class="text-center">Harga Beli<span class="required-label">*</span></th>
                                                                    <th class="text-center">Stok<span class="required-label">*</span></th>
                                                                    <th class="text-center">Total<span class="required-label">*</span></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="inventory_item">
                                                                <!-- <tr class="inventory_item_1">
                                                                    <td></td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            @if($wh_product != null)
                                                                                <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                    <option value="">-- Pilih Produk --</option>
                                                                                    @foreach($wh_product as $prd)
                                                                                        <option value="{{$prd->prod_id}}">{{$prd->prod_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <span class="invalid-feedback error_inventory_product"></span>
                                                                            @else
                                                                                <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                    <option value="">-- Pilih Produk --</option>
                                                                                </select>
                                                                                <span class="invalid-feedback error_inventory_product"></span>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="text" id="inventory_produk_code_1" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="text" id="inventory_produk_unit_1" class="form-control inventory_produk_unit text-right" value="" disabled="disabled" />
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="text" id="inventory_price_1" name="inventory_price[]" class="form-control inventory_price text-right" onkeyup="inventoryPrice(this)" value="" />
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <input type="text" id="inventory_stock_1" name="inventory_stock[]" class="form-control inventory_stock text-right" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" />
                                                                                    <input type="hidden" id="final_stock_1" name="final_stock[]" class="form-control final_stock" />
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <select type="text" id="satuan_produk_1" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)">
                                                                                        @if(!empty($satuan))
                                                                                        <option value="">-- Pilih Produk --</option>
                                                                                        @foreach($satuan as $st)
                                                                                            <option value="{{$st->uom_id}}">{{$st->uom_name}}</option>
                                                                                        @endforeach
                                                                                        @else
                                                                                            <option value="">-- Pilih Produk --</option>
                                                                                        @endif
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="text" id="inventory_price_total_1" name="inventory_price_total[]" class="form-control inventory_price_total text-right" value="0" disabled="disabled" />
                                                                        </div>
                                                                    </td>
                                                                </tr> -->
                                                            </tbody>
                                                            <tfoot>
                                                                <td colspan="3">
                                                                    <button class="btn btn-orange" id="addInvenProduct" data-id="1"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                    <button class="btn btn-orange" id="importproduk" data-toggle="modal" data-target="#importIncoming"><i class="fa fa-upload"></i> Import Produk</button>
                                                                    <a class="btn btn-orange" href="{{ asset('files/template_upload_barang_masuk.xlsx') }}" style="color: #fff">
                                                                        <i class="fa fa-file-text"></i> Template
                                                                    </a>
                                                                    <div class="total-data-import" style="padding: 15px;">Total Data : <span class="total_data">0</span></div>
                                                                </td>
                                                                <td class="text-right stock_total" colspan="2">Grand Total</td>
                                                                <td class="text-right price_total" colspan="2">0</td>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form_footer card-footer bg-transparent text-right">
                                                <div class="col-md-6 res-btn col-xs-12 no-padding float-right">
                                                    <div class="col-xs-6 col-sm-4 no-padding">
                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan">
                                                    </div>
                                                    <div class="col-xs-6 col-sm-4 no-pad-left">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->

    </div>

    <!-- Modal -->
    <div class="modal fade" id="importIncoming" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importIncomingLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Import Barang Masuk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <form method="POST" enctype="multipart/form-data" id="formImport">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="wh_id" id="wh_id" value="">
                        <div class="form-group">
                            <label for="product_picture" class="dp-block">File Excel  <span class="required-label">*</span></label>
                            <div class="clearfix"></div>
                            <div class="input-img-name">
                                <span id="image_name">Tidak Ada Data</span>
                                <input type="file" name="file" class="form-control" value="" style="display:none;">
                                <button type="button" class="btn btn-green-upload no-margin" id="product_picture_button">Cari</button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Footer -->
                    <div class="modal-footer border-top-grey" >
                        <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                            <div class="col-xs-6 padding-r-10 res-no-pad-sm margin-b10">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <button type="submit" id="submitFilterButton" value="1" name="activity_button" class="btn btn-orange btn-block">Unggah</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importfail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importfailLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Produk Gagal Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <table class="table display w-100 table-brown border-head datatable-basic" id="table-fail">
                        <thead class="bg-darkgrey w-100">
                            <tr>
                                <th class="text-center">Kode Produk<span class="required-label">*</span></th>
                                <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                <th class="text-center">Pesan<span class="required-label">*</span></th>
                            </tr>
                        </thead>
                        <tbody id="dataImportFail">

                        </tbody>
                    </table>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                        <div class="col-xs-12 padding-r-10 res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('#addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            // console.log(id+'-'+i);
            $(this).attr('data-id', i);
            var wh_id = $('.inventory_warehouse').val();

            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
                success: function (r) {
                    // console.log(r);
                    html = '<tr class="inventory_item_'+i+'">';
                    html+= '<td>';
                    html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                    html+= '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<select id="inventory_product_'+i+'" name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)" required>';
                    html += '<option value="">-- Pilih Produk --</option>';
                    $.each(r.product, function (i, item) {
                        html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                    });
                    html += '</select>';
                    html += '<span class="invalid-feedback error_inventory_product"></span>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_produk_code'+i+'" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_produk_unit'+i+'" class="form-control inventory_produk_unit text-right" value="" disabled="disabled" />';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_price'+i+'" name="inventory_price[]" onkeyup="inventoryPrice(this)" class="form-control text-right inventory_price" value="" required/>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="row">';
                    html += '<div class="col-sm-6">';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock text-right" onkeyup="inventoryStock(this)" onchange="convertStock(this)" value="" required/>';
                    html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-sm-6">';
                    html += '<div class="form-group">';
                    html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)">';
                    html += '<option value="">-- Pilih Produk --</option>';
                    $.each(r.uom, function (k, satu) {
                        html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_price_total'+i+'" name="inventory_price_total[]" class="form-control inventory_price_total text-right" value="0" disabled="disabled" />';
                    html += '</div>';
                    html += '</td>';

                    html += '</tr>';
                    $('#inventory_item').append(html);
                    $(".single-select").select2();
                    var total_row = $('.inventory_product').length;
                    $('.total_data').text(total_row);
                    hideLoader();
                }
            });
        });


        $('#importproduk').click(function(e){
            e.preventDefault();
        });

        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
        }

        function inventoryProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            var list_uom = <?php echo $satuan; ?>;
            var flag = false;
            // console.log(row);
            showLoader();
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                success: function (r) {
                    row.find('.satuan_produk option').attr('disabled', false);
                    row.find('.inventory_produk_code').val(r.prod_code);
                    $.each( list_uom, function( key, value ) {
                        if(value.uom_id == r.uom_id){
                            flag = true;
                        }
                    });
                    if (!flag) {
                        row.find('.satuan_produk').empty().append($('<option>', {
                            value: r.uom_id,
                            text: r.uom_name,
                            selected: true
                        }));
                    }else{
                        row.find('.satuan_produk').empty();
                        $.each( list_uom, function( key, value ) {
                            row.find('.satuan_produk').append($('<option>', {
                                value: value.uom_id,
                                text: value.uom_name,
                            }));
                        });
                        row.find('.satuan_produk').val(r.uom_id).trigger('change');
                    }
                    row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');
                    hideLoader();
                }
            });
        }

        function inventoryPrice(i){
            // console.log(i);
            currencyFormat(i);
            var price = $(i).val().split(".").join("");
            var row = $(i).closest('tr');
            var qty = row.find('.inventory_stock').val();
            var total = Math.round(price * qty);
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            }
            //var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            //$('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
            // convertStock(i);
        }

        function inventoryStock(i){
            // console.log(i);
            // currencyFormat(i);
            var qty = $(i).val();
            var row = $(i).closest('tr');
            var price = row.find('.inventory_price').val().split(".").join("");
            var total = Math.round(price * qty);

            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            } else {
                row.find('.inventory_price_total').val("0");
            }
            //var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            //$('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
            convertStock(i);
        }

        function currency(i){
            var total = i.length;
            var currencyTemp  = "";
            for (let k = 0; k < i.length; k++) {
                total -= 1;
                currencyTemp = currencyTemp + i[k];
                if(total > 0 && total % 3 == 0){
                    currencyTemp = currencyTemp + ".";
                }
            }
            return currencyTemp;
        }

        function sum(input){

            if (toString.call(input) !== "[object Array]"){
                return false;
            }

            var total =  0;
            for(var i=0;i<input.length;i++){
                var num = input[i].split(".").join("");
                if(isNaN(num)){
                    continue;
                }
                  total += Number(num);
            }

            return total;
        }

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            var error = 0;

            $("input[name='inventory_stock[]']").each(function() {
                if ($(this).val() < 0) {
                    error = 1;
                }
            });

            if ($('.inventory_product') .length == 0 || error == 1) {
                $('#notificationPopup').modal('show');
                $('#notificationPopup .modal-header').removeClass('success');
                $('#notificationPopup .modal-header').addClass('error');
                $('#notificationPopup .modal-title').html("{{ __('popup.error') }}");
                $('#notificationPopup .modal-body p').html("Error");
            }else{
                showLoader();
                // console.log($(this).serialize());
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/inventory/form") }}',
                    data: $(this).serialize(),
                    success: function (r) {
                        // console.log(r);
                        if (r.code != '200') {
                            $.each(r.messages, function (i, item) {
                                $('.error_'+i).text(item);
                            });
                            hideLoader();
                        } else {
                            hideLoader();
                            $('.modal-custom-text p').text(r.messages);
                            $('#TambahStok').modal('hide');
                            $('#notificationPopup .modal-title').html("{{ __('popup.success') }}");
                            $('#notificationPopup .modal-header').removeClass('error');
                            $('#notificationPopup .modal-header').addClass('success');
                            $('#notificationPopup').modal('show');
                            // $('#inventory_form')[0].reset();
                            window.location.replace("<?= url('admin/inventory') ?>");
                        }
                        // $('.inventory_product_code').val(r.prod_code);
                    }
                });
            }
            // console.log('test');
        });

        function convertStock(i) {
            var row = $(i).closest('tr');
            var productId = row.find('.inventory_product').val();
            var uom_id = row.find('.satuan_produk').val();
            var uom_value = row.find('.inventory_stock').val();
            // console.log(row.find('.inventory_price').val());
            var price = row.find('.inventory_price').val().split(".").join("");
            if (uom_value != '' && uom_id != '' && productId != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/inventory/converterStock") }}',
                    data: {
                        productId: productId,
                        satuan_produk: uom_id,
                        inventory_stock: uom_value
                    },
                    success: function (r) {
                        console.log(r);
                        if (r.flag !== false) {
                            row.find('.final_stock').val(r);
                            var stock = row.find('.inventory_stock').val();
                            var total = price * stock;
                            row.find('.inventory_price_total').val(currency(total.toString()));
                            //var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
                            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
                            //$('.stock_total').text(stock_total);
                            $('.price_total').text(currency(price_total.toString()));
                            $('#addInven').attr('disabled', false);
                        } else {
                            $('.modal-custom-text p').text(r.message);
                            $('#notificationPopup  .modal-header').removeClass('success');
                            $('#notificationPopup  .modal-header').addClass('error');
                            $('#TambahStok').modal('hide');
                            $('#notificationPopup').modal('show');
                            $('#notificationPopupLabel').text('Gagal');
                            $('#addInven').attr('disabled', true);
                        }
                    }
                });
            }
        }

        $(".inventory_warehouse").change(function(e){
            e.preventDefault();
            showLoader();
            var wh_id = $('.inventory_warehouse').val();
            $('#wh_id').val(wh_id);
            if (wh_id != '') {
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
                    success: function (r) {
                        // console.log(r);

                        var strObj = JSON.stringify(r);
                        var obj64 = btoa(strObj);
                        $('.list_prod').val(obj64);

                        $.each(r.product, function() {
                            $('#inventory_product_1').val(null);
                            var newOption = new Option(this.prod_name+'('+this.uom_value+' '+this.uom_name+')', this.prod_id, false, false);
                            $('#inventory_product_1').append(newOption);
                            //$(".inventory_warehouse").select2({ disabled:'readonly' })
                        });

                        $('#detailTrx').fadeIn();
                        hideLoader();
                    }
                });
            } else {
                $('#detailTrx').fadeOut();
                hideLoader();
            }
        });

        $('#formImport').submit(function(e){
            e.preventDefault();
            // console.log('test');
            showLoader();
            // console.log($(this).serialize());
            var list_prod = atob($('.list_prod').val());
            var objProd = JSON.parse(list_prod);
            $('.item_fail').remove();
            $('.total-import').remove();
            // console.log(objProd);
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/importIncoming") }}',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (r) {
                    console.log(r);
                    if (r.isSuccess == true) {
                        $.each(r.data, function (q, im) {
                            var i = q+1;
                            html = '<tr class="inventory_item_'+i+'">';
                            html+= '<td>';
                            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                            html+= '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<select id="inventory_product_'+i+'" name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(objProd.product, function (i, item) {
                                if (im.prod_id == item.prod_id) {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" selected>'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                } else {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                }
                            });
                            html += '</select>';
                            html += '<span class="invalid-feedback error_inventory_product"></span>';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_produk_code'+i+'" name="inventory_produk_code" class="form-control inventory_produk_code" value="'+im.prod_code+'" disabled="disabled" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_produk_unit'+i+'" class="form-control inventory_produk_unit text-right" value="'+im.prod_uom_name+'" disabled="disabled" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_price'+i+'" name="inventory_price[]" onkeyup="inventoryPrice(this)" class="form-control text-right inventory_price" value="'+im.harga_beli+'" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="row">';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock text-right" onkeyup="inventoryStock(this)" onchange="convertStock(this)" value="'+im.stock+'" />';
                            html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" value="'+im.finalStock+'" />';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)">';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(objProd.uom, function (k, satu) {
                                if (im.uom_id == satu.uom_id) {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product" selected>'+satu.uom_name+'</option>';
                                } else {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                                }
                            });
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</td>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_price_total'+i+'" name="inventory_price_total[]" class="form-control inventory_price_total text-right" value="0" disabled="disabled" />';
                            html += '</div>';
                            html += '</td>';

                            html += '</tr>';
                            $('#inventory_item').append(html);
                            $(".single-select").select2();
                            $('.inventory_price').keyup();
                        });
                        $('#importIncoming').modal('hide');
                        if (r.data_fail.length > 0) {
                            var htmlFail = '';
                            $.each(r.data_fail, function (x, fail) {
                                htmlFail += '<tr class="item_fail">';
                                htmlFail += '<th class="text-center">'+fail.prod_code+'</th>';
                                htmlFail += '<th class="text-center">'+fail.prod_name+'</th>';
                                htmlFail += '<th class="text-center">'+fail.message+'</th>';
                                htmlFail += '</tr>';
                            });
                            $('#dataImportFail').append(htmlFail);
                            $('#importfail').modal('show');
                        }
                        var total_row = $('.inventory_product').length;
                        $('.total_data').text(total_row);
                        hideLoader();
                    } else {
                        hideLoader();
                    }

                }
            });
        });

        $('#product_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('form#formImport input[name="file"]').click();
        });

        $('form#formImport input[name="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            // console.log(fileName);
            $('#image_name').text(fileName);
            // readURL(this,'#showing_product_image img');
            // console.log('The file "' + fileName +  '" has been selected.');
        });
        
        $('.discard_changes_button').click(function(e){
            redirectToPage('{{ route("admin/inventory") }}')
        });
    </script>
@endsection
