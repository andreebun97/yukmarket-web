@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_inventory') : __('page.edit_inventory')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'inventory'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li>{{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_inventory') : __('page.edit_inventory') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_inventory') : __('page.edit_inventory') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form method="POST" id="inventoryForm" action="{{ route('admin/inventory/form') }}" class="form-style">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('id') != null)
                                                            <input type="hidden" name="inventory_id" value="{{ $inventory_by_id['inventory_id'] }}">
                                                        @endif
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                            <div class="clearfix"></div>


                                                            <div class="col-xs-12 no-padding margin-b20">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="role_name">Atur Stok</label><br>
                                                                    <span class="user_data_information">
                                                                        <div class="form-check float-left margin-r10" >
                                                                            <input class="form-check-input" type="radio" name="user_active_status" checked="" value="1">
                                                                            <label class="form-check-label" for="super_active_status">
                                                                            Per Hari
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check float-left" >
                                                                            <input class="form-check-input" type="radio" name="user_active_status" value="0">
                                                                            <label class="form-check-label" for="inactive_status">
                                                                            Per Bulan
                                                                            </label>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="product_name">Max QTY/Beli <span class="required-label">*</span></label>
                                                                    <select name="product_name" id="product_name" class="form-control single-select" placeholder="{{ __('page.choose_product') }}">
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                    </select>
                                                                    @error('product_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('product_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="product_name">Pemberitahuan QTY dibawah <span class="required-label">*</span></label>
                                                                    <select name="product_name" id="product_name" class="form-control single-select" placeholder="{{ __('page.choose_product') }}">
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                    </select>
                                                                    @error('product_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('product_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </form>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        @if(Request::get('id') != null)
            // $('select[name="product_name"]').select2('readonly',true);
            $('select[name="product_name"]').siblings('.select2-container').addClass('pointer-none');
        @endif

        var validationRules = {
            product_name: {
                required: true
            },
            product_stock: {
                required: true
            }
        };
        var validationMessages = {
            product_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_name')]) }}"
            },
            product_stock: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_stock')]) }}"
            }
        }

        $('form#inventoryForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/inventory") }}')

        exitPopup('{{ route("admin/inventory") }}');
    </script>
    @endif
@endsection
