
@extends('admin::layouts.master')

@section('title',__('menubar.stock_recording'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_recording') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_recording') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="opname_org" value="{{$trx_header->organization_id}}">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="product_stock">No. Transaksi</label>
                                                                <input type="text" name="opname_trx" class="form-control opname_trx" value="{{$trx_header->trx_code}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <input type="hidden" name="opname_warehouse" class="opname_warehouse" value="{{$trx_header->warehouse_id}}">
                                                                <input type="text" name="opname_warehouse_name" class="form-control opname_warehouse_name" value="{{$trx_header->warehouse_name}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="opname_date">Tanggal Opname <span class="required-label">*</span></label>
                                                                <div class="input-group padding-left-20">
                                                                    <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                    <input type="text" class="form-control daterange-single" name="opname_date" value="{{date('Y-m-d', strtotime($trx_header->trx_date))}}" max="{{date('Y-m-d')}}" >
                                                                    <!-- <input type="date" class="form-control" name="opname_date" value="{{date('Y-m-d', strtotime($trx_header->trx_date))}}" max="{{date('Y-m-d')}}"> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Kategori<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <select name="opname_category[]" class="form-control multi-select opname_category @error('product_category') is-invalid @enderror" multiple>
                                                                    <option value="">-- Pilih Kategori --</option>
                                                                    @foreach($all_category as $category)
                                                                        @foreach($category_opname as $cat_op)
                                                                            <option value="{{$category->category_id}}" {{ $cat_op == $category->category_id ? 'selected' : '' }}> {{$category->category_name}}</option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <input type="hidden" class="list_prod" value="{{$list_productBase64}}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                <select name="opname_status" class="form-control single-select opname_status @error('product_category') is-invalid @enderror">
                                                                    <option value="">-- Pilih Status --</option>
                                                                    <option value="0" {{ $trx_header->trx_status == 0 ? 'selected' : '' }}>Tunda</option>
                                                                    <option value="1" {{ $trx_header->trx_status == 1 ? 'selected' : '' }}>Selesai</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" >
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">ID Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">Satuan Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">No. Rak<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Freeze Stock<span class="required-label">*</span></th> -->
                                                                        <th class="text-center">Stok<span class="required-label">*</span></th>
                                                                        <th class="text-center">Berat Produk<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Selisih Stok<span class="required-label">*</span></th> -->
                                                                        <!-- <th class="text-center">Total<span class="required-label">*</span></th> -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    <tr class="form_hide_after_add" style="display: none;">
                                                                        <td class="text-center" colspan="7">
                                                                            <button class="btn btn-orange addInvenProduct" data-id="0"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                        </td>
                                                                    </tr>
                                                                    @foreach($trx_detail as $key => $detail)
                                                                    <tr class="inventory_item inventory_item_{{$key+1}}">
                                                                        <td>
                                                                            <div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct({{$key+1}})"><i class="fa fa-times-circle color-red ico-size-td"></i></div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <select name="opname_product[]" class="form-control opname_product single-select" onchange="inventoryProduct(this)">
                                                                                <option value="">-- Pilih Produk --</option>
                                                                                    @foreach($list_product as $prd)
                                                                                        <?php $prod_name = $prd->prod_name.' ('.$prd->uom_value.' '.$prd->uom_name.')'; ?>
                                                                                        <option value="{{$prd->prod_id}}" {{ $prd->prod_id == $detail->prod_id ? 'selected' : '' }}>{{$prod_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <span class="invalid-feedback opname_product"></span>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="{{$detail->prod_code}}" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" name="inventory_produk_unit text-right" class="form-control inventory_produk_unit text-right" value="{{$detail->uom_value.' '.$detail->uom_name}}" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" name="opname_rack[]" class="form-control opname_rack" value="{{$detail->rack}}" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" name="opname_stock[]" class="form-control opname_stock" onkeyup="convertStock(this)" onkeypress="return numeric(event)" value="{{$detail->stock}}" />
                                                                                <input type="hidden" name="final_stock[]" class="form-control final_stock" value="{{$detail->final_stock}}"/>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                            <select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>
                                                                            <option value="">-- Pilih Produk --</option>
                                                                            @foreach($list_uom as $uom)
                                                                                <option value="{{$uom->uom_id}}" {{ $uom->uom_id == $detail->uom_id ? 'selected' : '' }}>{{$uom->uom_name}}</option>
                                                                            @endforeach
                                                                            </select>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot class="footer_form">
                                                                    <td colspan="7">
                                                                        <button class="btn btn-orange addInvenProduct" data-id="{{$list_item}}"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                    </td>
                                                                    <!-- <th class="text-right">Grand Total</th>
                                                                    <th class="text-left stock_total" >0</th>
                                                                    <th class="text-left" ></th> -->
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="form_footer text-right">
                                                        <div class="form-group footer_form" style="margin-right: 35px;">
                                                            <input type="submit" name="addInven" id="addInven" class="btn btn-orange" value="Simpan">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('.addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            console.log(id+'-'+i);
            $('.addInvenProduct').attr('data-id', i);
            var wh_id = $('.opname_warehouse').val();
            var list_prod = atob($('.list_prod').val());
            var objProd = JSON.parse(list_prod);
            console.log(objProd);
            html = '<tr class="inventory_item inventory_item_'+i+'">';
            html+= '<td>';
            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
            html+= '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<select name="opname_product[]" class="form-control opname_product single-select" onchange="inventoryProduct(this)">';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(objProd, function (i, item) {
                html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
            });
            html += '</select>';
            html += '<span class="invalid-feedback opname_product"></span>';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_produk_unit text-right" class="form-control inventory_produk_unit text-right" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="opname_rack[]" class="form-control opname_rack" value="" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="opname_stock[]" class="form-control opname_stock" onkeyup="convertStock(this)" onkeypress="return numeric(event)" value="" />';
            html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
            html += '</div>';
            html += '</td>';

            html += '<td>';
            html += '<div class="form-group">';
            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(<?php echo $list_uom; ?>, function (k, satu) {
                html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
            });
            html += '</select>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            $('.form_hide_after_add').fadeOut();
            $('.footer_form').fadeIn();
            $('#inventory_item').append(html);
            $(".single-select").select2();
            hideLoader();
        });
        $('.opname_category').change(function(){
            showLoader();
            $('.inventory_item').remove();
            $('.form_hide_after_add').fadeIn();
            $('.footer_form').fadeOut();
            var cat_id = $(this).val();
            var wh_id = $('.opname_warehouse').val();
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhCatID') }}/"+wh_id+"/"+cat_id,
                success: function (r) {
                    console.log(r);
                    var strObj = JSON.stringify(r);
                    var obj64 = btoa(strObj);
                    $('.list_prod').val(obj64);
                    hideLoader();
                }
            });
        });
        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
            if ($('.inventory_item').length < 1 ) {
                $('.form_hide_after_add').fadeIn();
                $('.footer_form').fadeOut();
            }
        }

        function inventoryProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            showLoader();
            console.log(row);
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                success: function (r) {
                    console.log(r);
                    row.find('.inventory_produk_code').val(r.prod_code);
                    row.find('.satuan_produk').val(r.uom_id).trigger('change');
                    row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');hideLoader();
                    hideLoader();
                }
            });
        }

        function inventoryPrice(i){
            console.log(i);
            currencyFormat(i);
            var price = $(i).val().split(".").join("");
            var row = $(i).closest('tr');
            var qty = row.find('.opname_stock').val();
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            }
            var stock_total = sum($("input[name='opname_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
        }

        function inventoryStock(i){
            // console.log(i);
            // currencyFormat(i);
            // var qty = $(i).val();
            // var row = $(i).closest('tr');
            // var price = row.find('.inventory_price').val().split(".").join("");
            // var total = price * qty;
            // if (qty != '') {
            //     row.find('.inventory_price_total').val(currency(total.toString()));
            // } else {
            //     row.find('.inventory_price_total').val("0");
            // }
            // var stock_total = sum($("input[name='opname_stock[]']").map(function(){return $(this).val();}).get());
            // var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            // $('.stock_total').text(stock_total);
            // $('.price_total').text(currency(price_total.toString()));
        }

        function currency(i){
            var total = i.length;
            var currencyTemp  = "";
            for (let k = 0; k < i.length; k++) {
                total -= 1;
                currencyTemp = currencyTemp + i[k];
                if(total > 0 && total % 3 == 0){
                    currencyTemp = currencyTemp + ".";
                }
            }
            return currencyTemp;
        }

        function sum(input){
             
            if (toString.call(input) !== "[object Array]"){
                return false;
            }
            
            var total =  0;
            for(var i=0;i<input.length;i++){
                var num = input[i].split(".").join("");
                if(isNaN(num)){
                    continue;
                }
                  total += Number(num);
            }

            return total;
        }

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            // console.log('test');
            showLoader();
            console.log($(this).serialize());
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/stock/recording/form") }}',
                data: $(this).serialize(),
                success: function (r) {
                    console.log(r);
                    if (r.code != '200') {
                        $.each(r.messages, function (i, item) {
                            $('.error_'+i).text(item);
                        });
                        hideLoader();
                    } else {
                        hideLoader();
                        $('.modal-custom-text p').text(r.messages);
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        // $('#inventory_form')[0].reset();
                        window.location.replace("<?= url('admin/inventory/stock_recording') ?>");
                    }
                    // $('.opname_product_code').val(r.prod_code);
                }
            });
        });

        function convertStock(i) {
            var row = $(i).closest('tr');
            var productId = row.find('.opname_product ').val();
            var uom_id = row.find('.satuan_produk').val();
            var uom_value = row.find('.opname_stock').val();
            console.log(productId);
            console.log(uom_id);
            console.log(uom_value);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/converterStock") }}',
                data: {
                    productId: productId,
                    satuan_produk: uom_id,
                    inventory_stock: uom_value
                },
                success: function (r) {
                    if (r.flag !== false) {
                        row.find('.final_stock').val(r);
                        $('#addInven').attr('disabled', false);
                    } else {
                        $('.modal-custom-text p').text(r.message);
                        $('#notificationPopup  .modal-header').removeClass('success');
                        $('#notificationPopup  .modal-header').addClass('error');
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        $('#notificationPopupLabel').text('Gagal');
                        $('#addInven').attr('disabled', true);
                    }
                }
            });
        }
    </script>
@endsection
