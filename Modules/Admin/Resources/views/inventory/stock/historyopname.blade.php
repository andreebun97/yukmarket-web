
@extends('admin::layouts.master')

@section('title',__('menubar.stock_recording'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_recording') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_recording') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="product_stock">No. Transaksi</label>
                                                                <input type="text" name="opname_trx" class="form-control opname_trx" value="{{$trx_header->transaction_header_code}}" readonly="readonly" />
                                                                <input type="hidden" name="id_detail" value="{{$id_detail}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                    <input type="hidden" name="opname_warehouse" class="opname_warehouse" value="{{$trx_header->warehouse_id}}">
                                                                    <input type="text" name="opname_warehouse_name" class="form-control opname_warehouse_name" value="{{$trx_header->warehouse_name}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="opname_date">Tanggal Opname <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control" name="opname_date" value="{{date('d-m-Y', strtotime($trx_header->updated_date))}}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Kategori<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <input type="text" class="form-control " value="{{$category_name}}" readonly="readonly" />
                                                            </div>
                                                            <input type="hidden" class="list_prod" value="">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                <input type="text" class="form-control " value="{{$trx_header->transaction_status == 1 ? 'Selesai' :'Tertunda'}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" id="tableOP">
                                                            <table id="table-history" class="table display w-100 table-brown border-head datatable-basic" >
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th class="text-center">Nama Produk</th>
                                                                        <th class="text-center">ID Produk</th>
                                                                        <th class="text-center">Satuan Produk</th>
                                                                        <th class="text-center">No. Rak</th>
                                                                        <th class="text-center">Stok</th>
                                                                        <th class="text-center">Berat Produk</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        var url = "<?= url('admin/inventory/stock_recording/historydetail') ?>";
        var id = $('input[name="id_detail"]').val();
        loadDatatable(url, id);
        function loadDatatable(url, id){
            var categoryTable = $('#table-history').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                filter: false,
                ajax: {
                    url: url+'/'+id
                },
                columns: [
                    {data: "prod_name"},
                    {data: "prod_code"},
                    {data: "prod_unit", className: "text-right"},
                    {data: "rack",className: "text-right"},
                    {data: "stock",className: "text-right"},
                    {data: "opname_uom",className: "text-right"},
                ]
            });
        }
    </script>
@endsection
