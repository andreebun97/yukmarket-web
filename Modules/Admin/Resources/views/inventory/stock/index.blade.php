
@extends('admin::layouts.master')

@section('title',__('menubar.stock'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!--div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-gear"></i> Pengaturan Stok</a>
                                            </div-->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <div class="tabbable">
                                                <div class="tabs-wrap res-tab">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item col-md-6 col-lg-3 col-sm-6 col-xs-6 text-center no-padding active"><a href="#StokTersedia" data-toggle="tab"> Stok tersedia</a></li>
                                                        <li class="nav-item col-md-6 col-lg-3 col-sm-6 col-xs-6 text-center no-padding"><a href="#StokHampirHabis" data-toggle="tab">Stok Hampir Habis</a></li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="StokTersedia">
                                                        <div class="col-md-12 no-padding">
                                                            <div class="panel-heading no-padding-bottom">
                                                                <div class="col-md-12 no-padding">
                                                                    <!-- <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="dim_length">Gudang Penyimpanan<span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            @if(is_null($user['warehouse_id']))                                                                            
                                                                            <select name="inventory_warehouse" id="stockAvWh" class="stockAv form-control single-select @error('product_category') is-invalid @enderror">
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                @foreach($inventory_warehouse as $warehouse)
                                                                                    <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @else
                                                                            <input type="hidden" name="inventory_warehouse" id="stockAvWh" class="inventory_warehouse" value="{{$user['warehouse_id']}}">
                                                                            <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user['warehouse_name']}}" readonly="readonly" />
                                                                            @endif
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="col-sm-6 no-padding">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="inventory_warehouse">Gudang <span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            <select name="inventory_warehouse[]" id="stockIvWh" class="stockSl form-control multi-select @error('product_category') is-invalid @enderror" multiple>
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                @foreach($all_warehouse as $warehouse)
                                                                                    <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 no-padding-right res-no-pad-sm">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="dim_length">Kategori<span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            <select name="product_category" id="stockAvCategory" class="stockAv form-control single-select @error('product_category') is-invalid @enderror">
                                                                                <option value="">-- Pilih Kategori --</option>
                                                                                @foreach($all_category as $category)
                                                                                    <option value="{{$category->category_id}}"> {{$category->category_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <span class="invalid-feedback">{{ $errors->first('product_category') }}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="panel-body">
                                                                
                                                                <div class="table-no-datatable no-padding border-solid-top-gray padding-t20" >
                                                                    <!-- <table class="table display w-100 table-brown border-head " id="productVariantTable"> -->
                                                                    <div class="col-xs-12 no-padding res-scroll-md">
                                                                        <table class="table display w-100 table-brown border-head datatable-basic datatable-scroll-y" id="table-list-stock" >
                                                                            <thead class="bg-darkgrey w-100">
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>ID Produk</th>
                                                                                    <th>Kategori</th>
                                                                                    <th>Nama Produk</th>
                                                                                    <th>Satuan Produk</th>
                                                                                    <th>Penyimpanan</th>
                                                                                    <th>Rata-rata Harga Beli</th>
                                                                                    <th>Stok</th>
                                                                                    <th>Total Harga</th>
                                                                                    <!-- <th>Aksi</th> -->
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <!-- <tr class="odd">
                                                                                    <td>1</td>
                                                                                    <td>123456789</td>
                                                                                    <td>Makanan</td>
                                                                                    <td>Mie</td>
                                                                                    <td>Gudang</td>
                                                                                    <td>Rp 2000,00</td>
                                                                                    <td>1</td>
                                                                                    <td>
                                                                                        <ul class="ico-block">
                                                                                            <li>
                                                                                                <a data-toggle="modal" data-target="#EditStok"title="Ubah">
                                                                                                    <i class="fa fa-eye"></i>
                                                                                                </a> 
                                                                                            </li>
                                                                                        </ul>
                                                                                    </td>
                                                                                </tr> -->
                                                                            </tbody>
                                                                            <tfoot>
                                                                                <tr role="row">
                                                                                    <th colspan="7" class="text-right">Total Keseluruhan</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th class="text-right"></th>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>      
                                                    </div>

                                                    <div class="tab-pane" id="StokHampirHabis">
                                                        <div class="col-md-12 no-padding">
                                                            <div class="panel-heading no-padding-bottom">
                                                                <div class="col-md-12 no-padding">
                                                                    <!-- <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            @if(is_null($user['warehouse_id']))                                                                            
                                                                            <select name="product_category" id="stockOvWh" class="stockAv form-control single-select @error('product_category') is-invalid @enderror">
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                @foreach($all_warehouse as $warehouse)
                                                                                    <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <span class="invalid-feedback">{{ $errors->first('product_category') }}</span>
                                                                            @else
                                                                            <input type="hidden" name="product_category" id="stockOvWh" class="inventory_warehouse" value="{{$user['warehouse_id']}}">
                                                                            <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user['warehouse_name']}}" readonly="readonly" />
                                                                            @endif
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="col-sm-6 no-padding">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="inventory_warehouse">Gudang <span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            <select name="inventory_warehouse_hampir_habis[]" id="stockOvWh" class="stockSl form-control multi-select @error('product_category') is-invalid @enderror" multiple>
                                                                                <option value="">-- Pilih Gudang --</option>
                                                                                @foreach($all_warehouse as $warehouse)
                                                                                    <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 no-padding-right res-no-pad-sm">
                                                                        <div class="form-group margin-b10">
                                                                            <label for="dim_length">Kategori<span class="required-label">*</span></label>
                                                                            <div class="clearfix"></div>
                                                                            <select name="product_category_hampir_habis" id="stockOvCategory" class="stockOv form-control single-select @error('product_category') is-invalid @enderror">
                                                                                <option value="">-- Pilih Kategori --</option>
                                                                                @foreach($all_category as $category)
                                                                                    <option value="{{$category->category_id}}"> {{$category->category_name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <span class="invalid-feedback">{{ $errors->first('product_category') }}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="panel-body">
                                                                
                                                                <div class="table-no-datatable no-padding border-solid-top-gray padding-t20" >
                                                                    <!-- <table class="table display w-100 table-brown border-head " id="productVariantTable"> -->
                                                                    <div class="col-xs-12 no-padding res-scroll-md">
                                                                        <table class="table display w-100 table-brown border-head datatable-basic " id="table-outOfStock">
                                                                            <thead class="bg-darkgrey w-100">
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>ID Produk</th>
                                                                                    <th>Kategori</th>
                                                                                    <th>Nama Produk</th>
                                                                                    <th>Satuan Produk</th>
                                                                                    <th>Penyimpanan</th>
                                                                                    <th>Rata-rata Harga Beli</th>
                                                                                    <th>Stok</th>
                                                                                    <th>Total Harga</th>
                                                                                    <!-- th>Aksi</th -->
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <!--tr class="odd">
                                                                                    <td>1</td>
                                                                                    <td>123456789</td>
                                                                                    <td>Makanan</td>
                                                                                    <td>Mie</td>
                                                                                    <td>Gudang</td>
                                                                                    <td>Rp 2000,00</td>
                                                                                    <td>1</td>
                                                                                    <td>
                                                                                        <ul class="ico-block">
                                                                                            <li>
                                                                                                <a data-toggle="modal" data-target="#EditStok"title="Ubah">
                                                                                                    <i class="fa fa-eye"></i>
                                                                                                </a> 
                                                                                            </li>
                                                                                        </ul>
                                                                                    </td>
                                                                                </tr -->
                                                                            </tbody>
                                                                            <tfoot>
                                                                                <tr role="row">
                                                                                    <th colspan="7" class="text-right">Total Keseluruhan</th>
                                                                                    <th class="text-right"></th>
                                                                                    <th class="text-right"></th>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>
                                            <!-- @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="inventory_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>{{ __('field.product_code') }}</th>
                                                        <th>{{ __('field.product_name') }}</th>
                                                        <th>{{ __('field.product_stock') }}</th>
                                                        <th>{{ __('field.action') }}</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif -->
                                        </div>
                                            <!-- <table class="table display w-100 table-brown" id="inventory_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>{{ __('field.product_code') }}</th>
                                                        <th>{{ __('field.product_name') }}</th>
                                                        <th>{{ __('field.product_stock') }}</th>
                                                        <th>{{ __('field.action') }}</th>
                                                    </tr>
                                                 </thead>
                                            </table> -->
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var inventory_warehouse = null;
        var sales_warehouse = null;
        // $('select[name="inventory_warehouse"]').change(function(){
        //     inventory_warehouse = $(this).val();
        //     $.ajax({
        //         url: "{{ route('admin/inventory/getWarehouseInventory') }}",
        //         type: "POST",
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         data: {
        //             warehouse_id: $(this).val()
        //         },
        //         async: false,
        //         success: function(resultData){
        //             // console.log(resultData);
        //             var warehouse = resultData['data']['warehouse'];
        //             var warehouseList = ["<option value=''></option>"];
        //             if(warehouse.length > 0){
        //                 for (let b = 0; b < warehouse.length; b++) {
        //                     // const element = array[b];
        //                     warehouseList.push('<option value="'+warehouse[b]['warehouse_id']+'">'+warehouse[b]['warehouse_name']+'</option>');
        //                 }
        //             }
        //             $('select[name="sales_warehouse"]').html(warehouseList);
        //         }
        //     });
        // });

        $('select[name="inventory_warehouse[]"]').change(function(){
            inventory_warehouse = $(this).val() == "" ? null : $(this).val().toString();
            console.log(inventory_warehouse);
            category_id = null;
            var url = "<?= url('admin/inventory/getAllStock') ?>";
            loadDatatable(url,inventory_warehouse,category_id);
            // console.log($(this).val());
        });



        $('select[name="product_category"]').change(function(){
            category_id = $(this).val() == "" ? null : $(this).val();
            // category_id = null;
            var url = "<?= url('admin/inventory/getAllStock') ?>";
            loadDatatable(url,inventory_warehouse,category_id);
        })

        $('select[name="inventory_warehouse_hampir_habis[]"]').change(function(){
            inventory_warehouse = $(this).val() == "" ? null : $(this).val().toString();
            category_id = null;
            var url = "<?= url('admin/inventory/getAllOutOfStock') ?>";
            loadDataTableOutOfStock(url,inventory_warehouse,category_id);
            // console.log($(this).val());
        });

        $('.stockOv').change(function(){
            var wh_id = $("#stockOvWh").val() == "" ? null : $('#stockOvWh').val();
            var category_id = $('#stockOvCategory').val() == "" ? null : $('#stockOvCategory').val();
            var urlOutOfStock = "<?= url('admin/inventory/getAllOutOfStock') ?>";
            loadDataTableOutOfStock(urlOutOfStock,wh_id,category_id);
        });
        
        function loadDatatable(url,wh_id,category_id){
            $('#table-list-stock').DataTable().clear().destroy();
            var categoryTable = $('#table-list-stock').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                ajax: {
                    url: url+"/"+wh_id+"/"+category_id
                },
                bSort: false,
                'columnDefs': [
                        {
                            "targets": [4,6,7,8],
                            className: 'dt-body-right'
                        },
                    ],
                columns: [
                    {data: "no"},
                    {data: "prod_code"},
                    {data: "category_name"},
                    {data: "prod_name"},
                    {data: "prod_unit"},
                    {data: "warehouse_name"},
                    {
                        data: "prod_price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')
                    },
                    {data: "stock"},
                    {
                        data: "total_price",
                        className: "text-right",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')
                    }
                ],
                fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                    var api = this.api(), oSettings;
                    
                    var intVal = function ( i ) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };

                    total = api.column(8).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    pageTotal = api.column( 8, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    $( api.column( 8 ).footer() ).html(
                        '( ' + $.fn.dataTable.render.number(',','','0','Rp. ').display(total) + ' )'
                    );

                    return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                }
            });
        }

        function loadDataTableOutOfStock(urlOutOfStock,wh_id,category_id){
            $('#table-outOfStock').DataTable().clear().destroy();
            var categoryTable = $('#table-outOfStock').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                ajax: {
                    url: urlOutOfStock+"/"+wh_id+"/"+category_id
                },
                bSort: false,
                'columnDefs': [
                        {
                            "targets": [4,6,7,8],
                            className: 'dt-body-right'
                        },
                    ],
                columns: [
                    {data: "no"},
                    {data: "prod_code"},
                    {data: "category_name"},
                    {data: "prod_name"},
                    {data: "prod_unit"},
                    {data: "warehouse_name"},
                    {
                        data: "prod_price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')
                    },
                    {data: "stock"},
                    {
                        data: "total_price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')
                    }
                ],
                fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                    var api = this.api(), oSettings;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };

                    total = api.column( 8 ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    pageTotal = api.column( 8, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    $( api.column( 8 ).footer() ).html(
                        '( ' + $.fn.dataTable.render.number(',','','0','Rp. ').display(total) + ' )'
                    );

                    return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                }
            });
        }

        // $('.stockAv').change(function(){
        //     var wh_id = $("#stockAvWh").val() == "" ? null : $('#stockAvWh').val();
        //     var category_id = $('#stockAvCategory').val() == "" ? null : $('#stockAvCategory').val();
        //     var url = "<?= url('admin/inventory/getAllStock') ?>";
        //     loadDatatable(url,wh_id,category_id);
        // });

        
    </script>
@endsection
