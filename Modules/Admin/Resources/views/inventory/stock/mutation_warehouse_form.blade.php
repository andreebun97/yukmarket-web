
@extends('admin::layouts.master')

@section('title',__('page.add_warehouse_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li>{{ __('menubar.stock_transfer') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</h4>
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" method="POST" action="{{ route('admin/inventory/stock_transfer/form') }}" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" class="list_prod" value="">
                                                        <input type="hidden" name="inventory_org" value="{{ Session::get('users')['organization_id'] }}">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$no_trx}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Dari<span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    @if(Request::get('id') == null)
                                                                    <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('inventory_warehouse') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang Dari --</option>
                                                                        @foreach($inv_warehouse as $warehouse)
                                                                            <option value="{{$warehouse->warehouse_id}}"> {{ $warehouse->warehouse_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @else
                                                                    <input type="text" class="form-control" value="{{ $inventory_warehouse['warehouse_name'] }}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Tujuan<span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    @if(Request::get('id') == null)
                                                                    <select name="sales_warehouse" class="form-control single-select sales_warehouse @error('sales_warehouse') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang Tujuan --</option>
                                                                        @foreach($inv_warehouse as $warehouse)
                                                                            <option value="{{$warehouse->warehouse_id}}"> {{ $warehouse->warehouse_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @else
                                                                    <input type="text" class="form-control" value="{{ $sales_warehouse['warehouse_name'] }}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Mutasi <span class="required-label">*</span></label>
                                                                    <input type="date" class="form-control" name="inventory_date" value="{{ Request::get('id') == null ? date('Y-m-d') : date('Y-m-d',strtotime($warehouse_mutation_header['mutation_date'])) }}" min="{{ date('Y-m-d') }}">
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                    <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Status --</option>
                                                                        <option value="0">Tertunda</option>
                                                                        <option value="1">Selesai</option>
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <!-- <div class="col-md-4 pl0-pr20">
                                                            <div class="form-group">
                                                                <label for="product_stock">Keterangan</label>
                                                                <textarea type="text" name="inventory_desc" class="form-control inventory_desc" value="" readonly style="resize:none; height:120px;"></textarea>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" id="inventoryTableDetail" style="width:100%">
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        @if(Request::get('id') == null)
                                                                        <th></th>
                                                                        @endif
                                                                        <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">ID Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">Harga Beli<span class="required-label">*</span></th>
                                                                        <th class="text-center">Stok<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Total<span class="required-label">*</span></th>
                                                                        <th></th> -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    @if(Request::get('id') != null)
                                                                        @for($b = 0; $b < count($warehouse_mutation_detail); $b++)
                                                                        <tr>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['prod_name'] }}</td>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['prod_code'] }}</td>
                                                                            <td>{{ $currency->convertToCurrency($warehouse_mutation_detail[$b]['price']) }}</td>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['stock'] . ' ' . $warehouse_mutation_detail[$b]['uom_name'] }}</td>
                                                                        </tr>
                                                                        @endfor
                                                                    @endif
                                                                    <!-- <tr class="inventory_item_1">
                                                                        <td>
                                                                            <div class="form-group">
                                                                                
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_produk_code_1" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_price_1" name="inventory_price[]" class="form-control inventory_price" onkeyup="currencyFormat(this)" value="" readonly />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-sm-8">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="inventory_stock_1" name="inventory_stock[]" class="form-control inventory_stock" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr> -->
                                                                </tbody>
                                                                <tfoot>
                                                                    <td colspan="2">
                                                                        <button type="button" class="btn btn-orange" id="addInvenProduct" data-id="1"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                    </td>
                                                                    <td class="text-right">Grand Total</td>
                                                                    <td class="text-left">
                                                                        <span class="stock_total" style="float:right">0</span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="price_total" style="float:right">0</span>
                                                                    </td>
                                                                    <!-- <th class="text-left price_total" colspan="1">0</th> -->
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @if(Request::get('id') == null)
                                                    <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('.inventory_warehouse').change(function(){
            showLoader();
            $('.inventory_item').remove();
            var wh_id = $('.inventory_warehouse').val();
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhStock') }}/"+wh_id+"/?variant=1",
                success: function (r) {
                    console.log(r);
                    var strObj = JSON.stringify(r);
                    var obj64 = btoa(strObj);
                    $('.list_prod').val(obj64);
                    hideLoader();
                }
            });
        });

        $('#addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            console.log(id+'-'+i);
            $('#addInvenProduct').attr('data-id', i);
            var wh_id = $('.inventory_warehouse').val();
            var list_prod = atob($('.list_prod').val());
            var objProd = JSON.parse(list_prod);
            console.log(objProd.product);
            html = '<tr class="inventory_item inventory_item_'+i+'">';
            html+= '<td>';
            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
            html+= '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<select name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(objProd.product, function (i, item) {
                    html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
            });
            html += '</select>';
            html += '<span class="invalid-feedback inventory_product"></span>';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_price[]" class="form-control inventory_price" onkeyup="currencyFormat(this)" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            // html += '<input type="text" name="inventory_stock[]" class="form-control inventory_stock text-right" onkeyup="convertStock(this)" onkeypress="return numeric(event)" value="" />';
            html += '<input type="text" name="inventory_stock[]" class="form-control inventory_stock text-right"  onkeypress="return numeric(event)" value="" />';
            html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
            html += '</div>';
            html += '</td>';       
            html += '</tr>';
            // $('.form_hide_after_add').fadeOut();
            // $('.footer_form').fadeIn();
            $('#inventory_item').append(html);
            $(".single-select").select2();
            hideLoader();
        });

        function inventoryProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            showLoader();
            console.log(row);
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                success: function (r) {
                    console.log(r);
                    row.find('.inventory_produk_code').val(r.prod_code);
                    row.find('.inventory_price').val(r.inventory_price).keyup();
                    // row.find('.satuan_produk').val(r.uom_id).trigger('change');
                    // row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');
                    hideLoader();
                }
            });
        }
        // var b = 1;
        // $('select[name="inventory_warehouse"]').change(function(){
        //     // getWarehouseAndInventory($(this).val());
        //     $('table#inventoryTableDetail tbody').html("<tr><td><select name='inventory_product[]' class='form-control'></select></td><td><input name='product_code[]' class='form-control' readonly></td><td><input name='inventory_price[]' class='form-control' readonly></td></td><td><select name='inventory_stock[]' class='form-control'></select></td><td><div style='cursor:pointer' class='deleteInvenProduct' onclick='deleteInvenProduct("+b+")'><i class='fa fa-times-circle color-red ico-size-td'></i></div></td></tr>");
        //     b += 1;
        //     var wh_id = $('.inventory_warehouse').val();
            
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id+"/?variant=1",
        //         success: function (r) {
        //             var data = r['product'];
        //             var product_list = [];
        //             if(data.length > 0){
        //                 for (let b = 0; b < data.length; b++) {
        //                     // const element = array[b];
        //                     product_list.push('<option value="'+data[b]['prod_id']+'">'+data[b]['prod_name']+' ' + (String(data[b]['uom_value']).replace(/\.00$/,'')) + ' '  + data[b]['uom_name']+'</option>');
        //                 }
        //             }
        //             $('select[name="inventory_product[]"]').html(product_list);
        //             $('select[name="inventory_product[]"]').select2();
        //         }
        //     });

        //     // $.ajax();
        // });

        // $('#addInvenProduct').click(function(){
        //     $("<tr><td><select name='inventory_product[]' class='form-control'></select></td><td><input name='product_code[]' class='form-control' readonly></td><td><input name='inventory_price[]' class='form-control' readonly></td></td><td><select name='inventory_stock[]' class='form-control'></select></td><td><div style='cursor:pointer' class='deleteInvenProduct' onclick='deleteInvenProduct("+b+")'><i class='fa fa-times-circle color-red ico-size-td'></i></div></td></tr>").insertAfter('table#inventoryTableDetail tbody tr:last-child');

        //     b += 1;
        //     var wh_id = $('.inventory_warehouse').val();
            
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id+"/?variant=1",
        //         success: function (r) {
        //             var data = r['product'];
        //             var product_list = [];
        //             if(data.length > 0){
        //                 for (let b = 0; b < data.length; b++) {
        //                     // const element = array[b];
        //                     product_list.push('<option value="'+data[b]['prod_id']+'">'+data[b]['prod_name']+' ' + (String(data[b]['uom_value']).replace(/\.00$/,'')) + ' '  + data[b]['uom_name']+'</option>');
        //                 }
        //             }
        //             $('select[name="inventory_product[]"]').html(product_list);
        //             $('select[name="inventory_product[]"]:last-child').select2();
        //         }
        //     });
        // })

        // function getWarehouseAndInventory(warehouse_id){
        //     $.ajax({
        //         type: "POST",
        //         url: "{{ route('admin/inventory/getWarehouseInventory') }}",
        //         data: {
        //             warehouse_id: warehouse_id,
        //             organization_id: "{{ Session::get('users')['organization_id'] }}"
        //         },
        //         headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         success: function(resultData){
        //             var data = resultData['data'];
        //             console.log(data);
        //             var inventory = data['inventory'];
        //             var warehouse = data['warehouse'];
        //             var inventory_dropdown_list = [];
        //             var warehouse_dropdown_list = ["<option value=''>-- Pilih Gudang --</option>"];
        //             $('table#inventoryTableDetail tbody').html(inventory_dropdown_list);
        //             $('select[name="sales_warehouse"]').html(warehouse_dropdown_list);
        //             if(inventory.length > 0){
        //                 for (let b = 0; b < inventory.length; b++) {
        //                     // const element = array[b];
        //                     var stock = inventory[b]['stock'];
        //                     var stock_dropdown = "";
        //                     stock_dropdown += "<select class='form-control single-select' name='inventory_stock[]'>"
        //                     for (let k = stock; k > 0; k--) {
        //                         // const element = array[k];
        //                         stock_dropdown += "<option value="+(k)+">"+(k)+"</option>";
        //                     }
        //                     stock_dropdown += "</select>"
        //                     inventory_dropdown_list.push("<tr class='inventory_item_"+b+"'><td>"+(inventory[b]['prod_name'] + (inventory[b]['product_sku_id'] == null ? '' : (' '+inventory[b]['uom_value'] + ' ' + inventory[b]['uom_name'])))+"<input type='hidden' name='inventory_product[]' value='"+inventory[b]['prod_id']+"'><input type='hidden' name='inventory_uom[]' value='"+inventory[b]['uom_id']+"'></td><td>"+inventory[b]['prod_code']+"</td><td>"+inventory[b]['inventory_price']+"<input type='hidden' name='inventory_price[]' value='"+inventory[b]['inventory_price']+"'></td><td>"+stock_dropdown+"</td><td><div style='cursor:pointer' class='deleteInvenProduct' onclick='deleteInvenProduct("+b+")'><i class='fa fa-times-circle color-red ico-size-td'></i></div></td></tr>");
        //                 }
        //                 $('table#inventoryTableDetail tbody').html(inventory_dropdown_list);
        //             }
        //             if(warehouse.length > 0){
        //                 dropdown_list = [];
        //                 for (let b = 0; b < warehouse.length; b++) {
        //                     // const element = array[b];
        //                     warehouse_dropdown_list.push("<option value='"+warehouse[b]['warehouse_id']+"'>"+warehouse[b]['warehouse_name']+"</option>");
        //                 }
        //                 $('select[name="sales_warehouse"]').html(warehouse_dropdown_list);
        //             }

        //             // if(warehouse.length > 0 && inventory.length > 0){
        //             //     $('#addInven').attr('disabled',false);
        //             // }else{
        //             //     $('#addInven').attr('disabled',true);
        //             // }
        //             // console.log(data);
        //         }
        //     });
        // }

        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
        }

        $('select[name="sales_warehouse"]').change(function(){
            if($(this).val() == ""){
                $('#addInven').attr('disabled',true);
            }else{
                $('#addInven').attr('disabled',false);
            }
        });

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            // console.log('test');
            showLoader();
            console.log($(this).serialize());
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/stock_transfer/form") }}',
                data: $(this).serialize(),
                success: function (r) {
                    console.log(r);
                    hideLoader();
                    if (r.code != '200') {
                        $.each(r.messages, function (i, item) {
                            $('.error_'+i).text(item);
                        });
                        hideLoader();
                    } else {
                        hideLoader();
                        $('.modal-custom-text p').text(r.messages);
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        // $('#inventory_form')[0].reset();
                        window.location.replace("<?= route('admin/inventory/stock_transfer') ?>");
                    }
                    $('.inventory_product_code').val(r.prod_code);
                }
            });
        });
        // $('#addInvenProduct').click(function(e){
        //     e.preventDefault();
        //     showLoader();
        //     var id = parseInt($(this).attr('data-id'));
        //     i = id + 1;
        //     console.log(id+'-'+i);
        //     $(this).attr('data-id', i);
        //     var wh_id = $('.inventory_warehouse').val();
            
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id+"/?variant=1",
        //         success: function (r) {
        //             console.log(r);
        //             html = '<tr class="inventory_item_'+i+'">';
        //             html += '<td>';
        //             html += '<div class="form-group">';
        //             html += '<select id="inventory_product_'+i+'" name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
        //             html += '<option value="">-- Pilih Produk --</option>';
        //             $.each(r.product, function (i, item) {
        //                 html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+' '+item.uom_value+' '+item.uom_name+'</option>';
        //             });
        //             html += '</select>';
        //             html += '<span class="invalid-feedback error_inventory_product"></span>';
        //             html += '</div>';
        //             html += '</td>';
        //             html += '<td>';
        //             html += '<div class="form-group">';
        //             html += '<input type="text" id="inventory_produk_code'+i+'" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
        //             html += '</div>';
        //             html += '</td>';
        //             html += '<td>';
        //             html += '<div class="form-group">';
        //             html += '<input type="text" id="inventory_price'+i+'" name="inventory_price[]" onkeyup="currencyFormat(this)" readonly class="form-control inventory_price" value="" />';
        //             html += '</div>';
        //             html += '</td>';
        //             html += '<td>';
        //             html += '<div class="row">';
        //             html += '<div class="col-sm-8">';
        //             html += '<div class="form-group">';
        //             html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" />';
        //             html += '</div>';
        //             html += '</div>';
        //             // html += '<div class="col-sm-4">';
        //             // html += '<div class="form-group">';
        //             // html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">';
        //             // html += '<option value="">-- Pilih Produk --</option>';
        //             // $.each(r.uom, function (k, satu) {
        //             //     html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
        //             // });
        //             // html += '</select>';
        //             // html += '</div>';
        //             // html += '</div>';
        //             html += '</div>';
        //             html += '</td>';
        //             html += '</td>';
        //             // html += '<td>';
        //             // html += '<div class="form-group">';
        //             // html += '<input type="text" id="inventory_price_total'+i+'" name="inventory_price_total[]" class="form-control inventory_price_total" value="0" disabled="disabled" />';
        //             // html += '</div>';
        //             // html += '</td>';
        //             html+= '<td>';
        //             html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
        //             html+= '</td>';
        //             html += '</tr>';
        //             $('#inventory_item').append(html);
        //             $(".single-select").select2();
        //             hideLoader();
        //         }
        //     });
        // });

        // function deleteInvenProduct(i){
        //     $('.inventory_item_'+i).remove();
        // }

        // function inventoryProduct(i){
        //     // console.log($(i).val());
        //     var row = $(i).closest('tr');
        //     var prod_id = $(i).val();
        //     console.log(row);
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
        //         success: function (r) {
        //             // console.log();
        //             console.log(r);
        //             row.find('.inventory_produk_code').val(r.prod_code);
        //             row.find('.inventory_price').val(r.inventory_price == null ? 0 : convertToCurrency(r.inventory_price));
        //         }
        //     });
        // }

        // function inventoryPrice(i){
        //     console.log(i);
        //     currencyFormat(i);
        //     var price = $(i).val().split(".").join("");
        //     var row = $(i).closest('tr');
        //     var qty = row.find('.inventory_stock').val();
        //     var total = price * qty;
        //     if (qty != '') {
        //         row.find('.inventory_price_total').val(currency(total.toString()));
        //     }
        //     var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
        //     var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
        //     $('.stock_total').text(stock_total);
        //     $('.price_total').text(currency(price_total.toString()));
        // }

        // function inventoryStock(i){
        //     // console.log(i);
        //     // currencyFormat(i);
        //     var qty = $(i).val();
        //     var row = $(i).closest('tr');
        //     var price = row.find('.inventory_price').val().split(".").join("");
        //     var total = price * qty;
        //     if (qty != '') {
        //         row.find('.inventory_price_total').val(currency(total.toString()));
        //     } else {
        //         row.find('.inventory_price_total').val("0");
        //     }
        //     var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
        //     var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
        //     $('.stock_total').text(stock_total);
        //     $('.price_total').text(currency(price_total.toString()));
        // }

        // function currency(i){
        //     var total = i.length;
        //     var currencyTemp  = "";
        //     for (let k = 0; k < i.length; k++) {
        //         total -= 1;
        //         currencyTemp = currencyTemp + i[k];
        //         if(total > 0 && total % 3 == 0){
        //             currencyTemp = currencyTemp + ".";
        //         }
        //     }
        //     return currencyTemp;
        // }

        // function sum(input){
             
        //     if (toString.call(input) !== "[object Array]"){
        //         return false;
        //     }
            
        //     var total =  0;
        //     for(var i=0;i<input.length;i++){
        //         var num = input[i].split(".").join("");
        //         if(isNaN(num)){
        //             continue;
        //         }
        //           total += Number(num);
        //     }

        //     return total;
        // }

        

        clickCancelButton('{{ route("admin/inventory/stock_transfer") }}')
    </script>
@endsection
