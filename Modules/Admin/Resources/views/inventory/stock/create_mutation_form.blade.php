
@extends('admin::layouts.master')

@section('title',__('page.add_stock_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li>{{ __('menubar.stock_mutation') }}</li>
                                <li class="active text-bold">{{ __('page.add_stock_mutation') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('page.add_stock_mutation') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="inventory_org" value="{{$user_wh_org->organization_id}}">
                                                        <div class="col-md-8">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$no_trx}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    @if($user_wh_org->warehouse_id != null)
                                                                        <input type="hidden" name="inventory_warehouse" class="inventory_warehouse" value="{{$user_wh_org->warehouse_id}}">
                                                                        <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user_wh_org->warehouse_name}}" readonly="readonly" />
                                                                    @else
                                                                        <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('product_category') is-invalid @enderror">
                                                                            <option value="">-- Pilih Gudang --</option>
                                                                            @foreach($all_warehouse as $warehouse)
                                                                            <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Mutasi <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <!--  <input type="date" class="form-control" name="inventory_date" value="" min="{{date('Y-m-d')}}"> -->
                                                                        <input type="text" class="form-control daterange-single" name="inventory_date" value="" min="{{date('Y-m-d')}}" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                    <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Status --</option>
                                                                        <option value="0">Tertunda</option>
                                                                        <option value="1">Selesai</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl0-pr20">
                                                            <div class="form-group">
                                                                <label for="product_stock">Keterangan</label>
                                                                <textarea type="text" name="inventory_desc" class="form-control inventory_desc" value="" readonly style="resize:none; height:120px;"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" >
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">ID Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">Harga Beli<span class="required-label">*</span></th>
                                                                        <th class="text-center">Stok<span class="required-label">*</span></th>
                                                                        <th></th>
                                                                        <!-- <th class="text-center">Total<span class="required-label">*</span></th>
                                                                        <th></th> -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    <!-- <tr class="inventory_item_1">
                                                                        <td></td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                @if($wh_product != null)
                                                                                    <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                        <option value="">-- Pilih Produk --</option>
                                                                                        @foreach($wh_product as $prd)
                                                                                            <option value="{{$prd->prod_id}}">{{$prd->prod_name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <span class="invalid-feedback error_inventory_product"></span>
                                                                                @else
                                                                                    <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                        <option value="">-- Pilih Produk --</option>
                                                                                    </select>
                                                                                    <span class="invalid-feedback error_inventory_product"></span>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_produk_code_1" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_produk_unit_1" class="form-control inventory_produk_unit text-right" value="" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_price_1" name="inventory_price[]" class="form-control inventory_price text-right" onkeyup="inventoryPrice(this)" value="" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <input type="text" id="inventory_stock_1" name="inventory_stock[]" class="form-control inventory_stock text-right" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" />
                                                                                        <input type="hidden" id="final_stock_1" name="final_stock[]" class="form-control final_stock" />
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <select type="text" id="satuan_produk_1" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)">
                                                                                            @if(!empty($satuan))
                                                                                            <option value="">-- Pilih Produk --</option>
                                                                                            @foreach($satuan as $st)
                                                                                                <option value="{{$st->uom_id}}">{{$st->uom_name}}</option>
                                                                                            @endforeach
                                                                                            @else
                                                                                                <option value="">-- Pilih Produk --</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group">
                                                                                <input type="text" id="inventory_price_total_1" name="inventory_price_total[]" class="form-control inventory_price_total text-right" value="0" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                    </tr> -->
                                                                </tbody>
                                                                <tfoot>
                                                                    <td>
                                                                        <button class="btn btn-orange" id="addInvenProduct" data-id="1"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                    </td>
                                                                    <th class="text-right">Grand Total</th>
                                                                    <th class="text-left">
                                                                    <span class="stock_total">0</span><span class="price_total" style="float:right">0</span></th>
                                                                    <!-- <th class="text-left price_total" colspan="1">0</th> -->
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('#addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            console.log(id+'-'+i);
            $(this).attr('data-id', i);
            var wh_id = $('.inventory_warehouse').val();
            
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id+"/?variant=1",
                success: function (r) {
                    console.log(r);
                    html = '<tr class="inventory_item_'+i+'">';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<select id="inventory_product_'+i+'" name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
                    html += '<option value="">-- Pilih Produk --</option>';
                    $.each(r.product, function (i, item) {
                        html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+' '+item.uom_value+' '+item.uom_name+'</option>';
                    });
                    html += '</select>';
                    html += '<span class="invalid-feedback error_inventory_product"></span>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_produk_code'+i+'" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_price'+i+'" name="inventory_price[]" onkeyup="currencyFormat(this)" readonly class="form-control inventory_price" value="" />';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="row">';
                    html += '<div class="col-sm-8">';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" />';
                    html += '</div>';
                    html += '</div>';
                    // html += '<div class="col-sm-4">';
                    // html += '<div class="form-group">';
                    // html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">';
                    // html += '<option value="">-- Pilih Produk --</option>';
                    // $.each(r.uom, function (k, satu) {
                    //     html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                    // });
                    // html += '</select>';
                    // html += '</div>';
                    // html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</td>';
                    // html += '<td>';
                    // html += '<div class="form-group">';
                    // html += '<input type="text" id="inventory_price_total'+i+'" name="inventory_price_total[]" class="form-control inventory_price_total" value="0" disabled="disabled" />';
                    // html += '</div>';
                    // html += '</td>';
                    html+= '<td>';
                    html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                    html+= '</td>';
                    html += '</tr>';
                    $('#inventory_item').append(html);
                    $(".single-select").select2();
                    hideLoader();
                }
            });
        });

        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
        }

        function inventoryProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            console.log(row);
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                success: function (r) {
                    // console.log();
                    console.log(r);
                    row.find('.inventory_produk_code').val(r.prod_code);
                    row.find('.inventory_price').val(r.inventory_price == null ? 0 : convertToCurrency(r.inventory_price));
                }
            });
        }

        function inventoryPrice(i){
            console.log(i);
            currencyFormat(i);
            var price = $(i).val().split(".").join("");
            var row = $(i).closest('tr');
            var qty = row.find('.inventory_stock').val();
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            }
            var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
        }

        function inventoryStock(i){
            // console.log(i);
            // currencyFormat(i);
            var qty = $(i).val();
            var row = $(i).closest('tr');
            var price = row.find('.inventory_price').val().split(".").join("");
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            } else {
                row.find('.inventory_price_total').val("0");
            }
            var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
        }

        function currency(i){
            var total = i.length;
            var currencyTemp  = "";
            for (let k = 0; k < i.length; k++) {
                total -= 1;
                currencyTemp = currencyTemp + i[k];
                if(total > 0 && total % 3 == 0){
                    currencyTemp = currencyTemp + ".";
                }
            }
            return currencyTemp;
        }

        function sum(input){
             
            if (toString.call(input) !== "[object Array]"){
                return false;
            }
            
            var total =  0;
            for(var i=0;i<input.length;i++){
                var num = input[i].split(".").join("");
                if(isNaN(num)){
                    continue;
                }
                  total += Number(num);
            }

            return total;
        }

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            // console.log('test');
            if ($('.inventory_product') .length == 0) {
                $('#notificationPopup').modal('show');
                $('#notificationPopup .modal-header').removeClass('success');
                $('#notificationPopup .modal-header').addClass('error');
                $('#notificationPopup .modal-title').html("{{ __('popup.error') }}");
                $('#notificationPopup .modal-body p').html("Error");
            } else {
                showLoader();
                console.log($(this).serialize());
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/inventory/stock/mutationForm") }}',
                    data: $(this).serialize(),
                    success: function (r) {
                        console.log(r);
                        if (r.code != '200') {
                            $.each(r.messages, function (i, item) {
                                $('.error_'+i).text(item);
                            });
                            hideLoader();
                        } else {
                            hideLoader();
                            $('.modal-custom-text p').text(r.messages);
                            $('#TambahStok').modal('hide');
                            $('#notificationPopup').modal('show');
                            // $('#inventory_form')[0].reset();
                            window.location.replace("<?= url('admin/inventory/stock/mutation') ?>");
                        }
                        // $('.inventory_product_code').val(r.prod_code);
                    }
                });
            }

        });

        clickCancelButton('{{ route("admin/inventory/stock/mutation") }}')
    </script>
@endsection
