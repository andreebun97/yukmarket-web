@extends('admin::layouts.master')

@section('title',__('page.add_warehouse_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li>{{ __('menubar.stock_transfer') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</h4>
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" method="POST" action="{{ route('admin/inventory/stock_transfer/form') }}" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" class="list_prod" value="">
                                                        <input type="hidden" name="inventory_org" value="{{ Session::get('users')['organization_id'] }}">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$warehouse_mutation_header->mutation_header_code}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Dari<span class="required-label">*</span></label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$warehouse_mutation_header->wh_from_name}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Tujuan<span class="required-label">*</span></label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$warehouse_mutation_header->wh_from_name}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Mutasi <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <input type="text" class="form-control daterange-single" name="inventory_date" value="{{ date('Y-m-d',strtotime($warehouse_mutation_header->mutation_date)) }}" min="{{ date('Y-m-d') }}" disabled="disabled">
                                                                    </div>

                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" id="inventoryTableDetail" style="width:100%;">
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th class="text-center">Produk Dari<span class="required-label">*</span></th>
                                                                        <th class="text-center">Harga Beli<span class="required-label">*</span></th>
                                                                        <th class="text-center">Produk Tujuan<span class="required-label">*</span></th>
                                                                        <th class="text-center">Stok Dikirim<span class="required-label">*</span></th>
                                                                        <th class="text-center">Stok DIterima<span class="required-label">*</span></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    @foreach($warehouse_mutation_detail as $wh_detail)
                                                                    <tr>
                                                                        <td>
                                                                            {{ $wh_detail->prod_from_name }}
                                                                            <span class="inventory_produk_code">Kode Produk : {{$wh_detail->prod_from_code}}</span>
                                                                        </td>
                                                                        <td>{{ $currency->convertToCurrency($wh_detail->price) }}</td>
                                                                        <td>
                                                                            {{ $wh_detail->prod_to_name }}
                                                                            <span class="inventory_produk_code">Kode Produk : {{$wh_detail->prod_to_code}}</span>
                                                                        </td>
                                                                        <td>{{ $wh_detail->stock }} {{ $wh_detail->uom_from_name }}</td>
                                                                        <td>{{ $wh_detail->destination_stock }} {{ $wh_detail->uom_to_name }}</td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('#inventoryTableDetail').DataTable();
    </script>
@endsection
