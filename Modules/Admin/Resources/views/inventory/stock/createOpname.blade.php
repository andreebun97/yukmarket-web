
@extends('admin::layouts.master')

@section('title',__('menubar.stock_recording'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_recording') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_recording') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="opname_org" value="{{$user_wh_org->organization_id}}">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="product_stock">No. Transaksi</label>
                                                                <input type="text" name="opname_trx" class="form-control opname_trx" value="{{$no_trx}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                @if($user_wh_org->warehouse_id != null)
                                                                    <input type="hidden" name="opname_warehouse" class="opname_warehouse" value="{{$user_wh_org->warehouse_id}}">
                                                                    <input type="text" name="opname_warehouse_name" class="form-control opname_warehouse_name" value="{{$user_wh_org->warehouse_name}}" readonly="readonly" />
                                                                @else
                                                                    <select name="opname_warehouse" class="form-control single-select opname_warehouse @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang --</option>
                                                                        @foreach($all_warehouse as $warehouse)
                                                                        <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="opname_date">Tanggal Opname <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <!-- <input type="date" class="form-control" name="opname_date" value="{{date('Y-m-d')}}" max="{{date('Y-m-d')}}"> -->
                                                                        <!-- <input type="text" class="form-control daterange-single" value="{{date('Y-m-d')}}" max="{{date('Y-m-d')}}" name="opname_date" > -->
                                                                        <input type="text" class="form-control" value="{{date('Y-m-d')}}" max="{{date('Y-m-d')}}" name="opname_date" readonly="readonly">
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Kategori<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <select name="opname_category[]" class="form-control multi-select opname_category @error('product_category') is-invalid @enderror" multiple>
                                                                    <option value="">-- Pilih Kategori --</option>
                                                                    @foreach($all_category as $category)
                                                                        <option value="{{$category->category_id}}"> {{$category->category_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <input type="hidden" class="list_prod" value="">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                <select name="opname_status" class="form-control single-select opname_status @error('product_category') is-invalid @enderror" required="required">
                                                                    <option value="">-- Pilih Status --</option>
                                                                    <option value="0">Tertunda</option>
                                                                    <option value="1">Selesai</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" id="tableOP"  style="display: none;">
                                                            <table class="table display w-100 table-brown border-head datatable-basic" >
                                                                <thead class="bg-darkgrey w-100">
                                                                    <!-- <tr>
                                                                        <td colspan="5">
                                                                            <ul class="list-inline">
                                                                                <li>Nomor Rak</li>
                                                                                <li>:</li>
                                                                                <li>
                                                                                    <div class="form-group" style="margin: 0;">
                                                                                        <input type="text" class="form-control opname_rack" name="opname_rack[]" value="" >
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr> -->
                                                                    <tr>
                                                                        <th></th>
                                                                        <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">ID Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">Satuan Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">No. Rak<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Freeze Stock<span class="required-label">*</span></th> -->
                                                                        <th class="text-center">Stok<span class="required-label">*</span></th>
                                                                        <th class="text-center">Berat Produk<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Selisih Stok<span class="required-label">*</span></th> -->
                                                                        <!-- <th class="text-center">Total<span class="required-label">*</span></th> -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">

                                                                    <tr class="form_hide_after_add">
                                                                        <td class="text-center" colspan="7">
                                                                            <button class="btn btn-orange addInvenProduct" data-id="0"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                            <button type="button" class="btn btn-orange" data-toggle="modal" data-target="#importIncoming"><i class="fa fa-upload"></i> Import Produk</button>
                                                                            <a class="btn btn-orange" href="{{ asset('files/template_upload_stok_opname.xlsx') }}">
                                                                                <i class="fa fa-file-text"></i> Template
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot class="footer_form" style="display: none;">
                                                                    <td colspan="7">
                                                                        <button class="btn btn-orange addInvenProduct" data-id="0"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                        <button type="button" class="btn btn-orange" data-toggle="modal" data-target="#importIncoming"><i class="fa fa-upload"></i> Import Produk</button>
                                                                        <a class="btn btn-orange" href="{{ asset('files/template_upload_stok_opname.xlsx') }}">
                                                                            <i class="fa fa-file-text"></i> Template
                                                                        </a>
                                                                    </td>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="form_footer text-right">
                                                        <div class="form-group footer_form" style="margin-right: 35px; display: none;">
                                                            <input type="submit" name="addInven" id="addInven" class="btn btn-orange" value="Simpan">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>


    <!-- Modal -->
    <div class="modal fade" id="importIncoming" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importIncomingLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Import Transfer Stok</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <form method="POST" enctype="multipart/form-data" id="formImport">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="wh_id" id="wh_id" value="">
                        <div class="form-group">
                            <label for="product_picture" class="dp-block">File Excel  <span class="required-label">*</span></label>
                            <div class="clearfix"></div>
                            <div class="input-img-name">
                                <span id="image_name">Tidak Ada Data</span>
                                <input type="file" name="file" class="form-control" value="" style="display:none;">
                                <button type="button" class="btn btn-green-upload no-margin" id="product_picture_button">Cari</button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Footer -->
                    <div class="modal-footer border-top-grey" >
                        <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                            <div class="col-xs-6 padding-r-10 res-no-pad-sm margin-b10">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <button type="submit" id="submitFilterButton" value="1" name="activity_button" class="btn btn-orange btn-block">Unggah</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importfail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importfailLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Produk Gagal Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <table class="table display w-100 table-brown border-head datatable-basic" id="table-fail">
                        <thead class="bg-darkgrey w-100">
                        <tr>
                            <th class="text-center">Kode Produk<span class="required-label">*</span></th>
                            <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                            <th class="text-center">Pesan<span class="required-label">*</span></th>
                        </tr>
                        </thead>
                        <tbody id="dataImportFail">

                        </tbody>
                    </table>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                        <div class="col-xs-12 padding-r-10 res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        var productList = [];
        $('.addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            // console.log(id+'-'+i);
            $('.addInvenProduct').attr('data-id', i);
            var wh_id = $('.opname_warehouse').val();
            // var list_prod = atob(productList);
            var list_prod = atob($('.list_prod').val());
            var objProd = JSON.parse(list_prod);
            // console.log(objProd);
            html = '<tr class="inventory_item inventory_item_'+i+'">';
            html+= '<td>';
            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
            html+= '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<select name="opname_product[]" class="form-control opname_product single-select" onchange="inventoryProduct(this)">';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(objProd, function (i, item) {
                    // console.log(item);
                    html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
            });
            html += '</select>';
            html += '<span class="invalid-feedback opname_product"></span>';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_produk_unit text-right" class="form-control inventory_produk_unit text-right" value="" disabled="disabled" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="opname_rack[]" class="form-control opname_rack" value="" />';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="opname_stock[]" class="form-control opname_stock text-right" onkeyup="convertStock(this)" onkeypress="return numeric(event)" value="" />';
            html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
            html += '</div>';
            html += '</td>';

            html += '<td>';
            html += '<div class="form-group">';
            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(<?php echo $list_uom; ?>, function (k, satu) {
                html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
            });
            html += '</select>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            $('.form_hide_after_add').fadeOut();
            $('.footer_form').fadeIn();
            $('#inventory_item').append(html);
            $(".single-select").select2();
            hideLoader();
        });
        $('.opname_warehouse').change(function(){
            showLoader();
            $('#tableOP').fadeIn();
            $('.inventory_item').remove();
            $('.footer_form').fadeOut();
            var wh_id = $(this).val();
            var cat_id = $('.opname_category').val();
            var params = {};
            params['wh_id'] = wh_id;
            params['cat_id'] = cat_id;
            var params_data = JSON.stringify(params);
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhCatIDUseParam') }}/" + params_data,
                // async: false,
                success: function (r) {
                    // console.log(r.length);
                    if (r.length > 0){
                        var strObj = JSON.stringify(r);
                        var obj64 = btoa(strObj);
                        productList = obj64;
                        // console.log(productList);
                        $('.list_prod').val(obj64);
                        $('.form_hide_after_add').fadeIn();
                    } else {
                        $('.form_hide_after_add').fadeOut();
                        $('#notificationPopup').modal('show');
                        $('#notificationPopup .modal-header').removeClass('success');
                        $('#notificationPopup .modal-header').addClass('error');
                        $('#notificationPopup .modal-title').html("Produk Tidak Ditemukan");
                        $('#notificationPopup .modal-body p').html("Error");
                    }
                    hideLoader();
                }
            });
        });
        $('.opname_category').change(function(){
            var wh_id = $('.opname_warehouse').val();
            if (wh_id!="" || wh_id.length != 0) {
                showLoader();
                $('#tableOP').fadeIn();
                $('.inventory_item').remove();
                $('.footer_form').fadeOut();
                var cat_id = $(this).val();
                // console.log(cat_id);
                var params = {};
                params['wh_id'] = wh_id;
                params['cat_id'] = cat_id;
                var params_data = JSON.stringify(params);
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/getProductByWhCatIDUseParam') }}/" + params_data,
                    // async: false,
                    success: function (r) {
                        // console.log(r.length);
                        if (r.length > 0){
                            var strObj = JSON.stringify(r);
                            var obj64 = btoa(strObj);
                            productList = obj64;
                            // console.log(productList);
                            $('.list_prod').val(obj64);
                            $('.form_hide_after_add').fadeIn();
                        } else {
                            $('.form_hide_after_add').fadeOut();
                            $('#notificationPopup').modal('show');
                            $('#notificationPopup .modal-header').removeClass('success');
                            $('#notificationPopup .modal-header').addClass('error');
                            $('#notificationPopup .modal-title').html("Produk Tidak Ditemukan");
                            $('#notificationPopup .modal-body p').html("Error");
                        }
                        hideLoader();
                    }
                });
            } else {
                $('.modal-custom-text p').text("{{ __('validation.filled',['attribute' => __('validation.attributes.product_warehouse')]) }}");
                $('#notificationPopupLabel').text('Gagal');
                $('#notificationPopup  .modal-header').removeClass('success');
                $('#notificationPopup  .modal-header').addClass('error');
                $('#notificationPopup').modal('show');
                $(this).val("").trigger('change.select2');
            }
        });
        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
            if ($('.inventory_item').length < 1 ) {
                $('.form_hide_after_add').fadeIn();
                $('.footer_form').fadeOut();
            }
        }

        function inventoryProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            showLoader();
            // console.log(row);
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                success: function (r) {
                    // console.log(r);
                    row.find('.inventory_produk_code').val(r.prod_code);
                    row.find('.satuan_produk').val(r.uom_id).trigger('change');
                    row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');
                    hideLoader();
                }
            });
        }

        function inventoryPrice(i){
            // console.log(i);
            currencyFormat(i);
            var price = $(i).val().split(".").join("");
            var row = $(i).closest('tr');
            var qty = row.find('.opname_stock').val();
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            }
            var stock_total = sum($("input[name='opname_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
        }

        function inventoryStock(i){
            // console.log(i);
            // currencyFormat(i);
            // var qty = $(i).val();
            // var row = $(i).closest('tr');
            // var price = row.find('.inventory_price').val().split(".").join("");
            // var total = price * qty;
            // if (qty != '') {
            //     row.find('.inventory_price_total').val(currency(total.toString()));
            // } else {
            //     row.find('.inventory_price_total').val("0");
            // }
            // var stock_total = sum($("input[name='opname_stock[]']").map(function(){return $(this).val();}).get());
            // var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            // $('.stock_total').text(stock_total);
            // $('.price_total').text(currency(price_total.toString()));
        }

        function currency(i){
            var total = i.length;
            var currencyTemp  = "";
            for (let k = 0; k < i.length; k++) {
                total -= 1;
                currencyTemp = currencyTemp + i[k];
                if(total > 0 && total % 3 == 0){
                    currencyTemp = currencyTemp + ".";
                }
            }
            return currencyTemp;
        }

        function sum(input){

            if (toString.call(input) !== "[object Array]"){
                return false;
            }

            var total =  0;
            for(var i=0;i<input.length;i++){
                var num = input[i].split(".").join("");
                if(isNaN(num)){
                    continue;
                }
                  total += Number(num);
            }

            return total;
        }

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            showLoader();
            console.log($(this).serialize());
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/stock/recording/form") }}',
                data: $(this).serialize(),
                success: function (r) {
                    console.log(r);
                    if (r.code != '200') {
                        if (r.messages.length > 0) {
                            $.each(r.messages, function (i, item) {
                                $('.error_'+i).text(item);
                            });    
                        } else {
                            $('.error_1').text(item);
                        }
                        hideLoader();
                    } else {
                        hideLoader();
                        $('.modal-custom-text p').text(r.messages);
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        // $('#inventory_form')[0].reset();
                        window.location.replace("<?= url('admin/inventory/stock_recording') ?>");
                    }
                    // $('.opname_product_code').val(r.prod_code);
                }
            });
        });

        function convertStock(i) {
            var row = $(i).closest('tr');
            var productId = row.find('.opname_product ').val();
            var uom_id = row.find('.satuan_produk').val();
            var uom_value = row.find('.opname_stock').val();
            // console.log(productId);
            // console.log(uom_id);
            // console.log(uom_value);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/converterStock") }}',
                data: {
                    productId: productId,
                    satuan_produk: uom_id,
                    inventory_stock: uom_value
                },
                success: function (r) {
                    if (r.flag !== false) {
                        row.find('.final_stock').val(r);
                        $('#addInven').attr('disabled', false);
                    } else {
                        $('.modal-custom-text p').text(r.message);
                        $('#notificationPopup  .modal-header').removeClass('success');
                        $('#notificationPopup  .modal-header').addClass('error');
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        $('#notificationPopupLabel').text('Gagal');
                        $('#addInven').attr('disabled', true);
                    }
                }
            });
        }

        $('#formImport').submit(function(e){
            e.preventDefault();

            var wh_id = $('.opname_warehouse').val();
            var cat_id = $('.opname_category').val();
            var formData  = new FormData(this);
            formData.append('wh_id', wh_id);
            formData.append('cat_id', cat_id);

            // console.log('test');
            showLoader();
            // console.log($(this).serialize());
            // var list_prod = atob($('.list_prod').val());
            // var objProd = JSON.parse(list_prod);
            // $('.item_fail').remove();
            // $('.total-import').remove();
            // console.log(objProd);
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/importStokOpname") }}',
                data: formData,
                contentType: false,
                processData: false,
                success: function (r) {
                    // console.log(r);
                    if (r.isSuccess == true) {
                        $.each(r.data, function (q, im) {
                            // increment data_id button
                            var id = parseInt($('.addInvenProduct').attr('data-id'));
                            i = id + 1;
                            // console.log(id+'-'+i);
                            $('.addInvenProduct').attr('data-id', i);

                            // get list prod obj
                            var list_prod = atob($('.list_prod').val());
                            var objProd = JSON.parse(list_prod);


                            // create table
                            html = '<tr class="inventory_item inventory_item_'+i+'">';
                            html+= '<td>';
                            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                            html+= '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<select name="opname_product[]" class="form-control opname_product single-select" onchange="inventoryProduct(this)">';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(objProd, function (i, item) {
                                if (im.prod_id == item.prod_id) {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" selected>'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                } else {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                }
                            });
                            html += '</select>';
                            html += '<span class="invalid-feedback opname_product"></span>';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="'+im.prod_code+'" disabled="disabled" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" name="inventory_produk_unit text-right" class="form-control inventory_produk_unit text-right" value="'+im.prod_uom_value+' '+im.prod_uom_name+'" disabled="disabled" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" name="opname_rack[]" class="form-control opname_rack" value="'+im.opname_rack+'" />';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" name="opname_stock[]" class="form-control opname_stock text-right" onkeyup="convertStock(this)" onkeypress="return numeric(event)" value="'+im.opname_stock+'" />';
                            html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" value="'+im.final_stock+'" />';
                            html += '</div>';
                            html += '</td>';

                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(<?php echo $list_uom; ?>, function (k, satu) {
                                if (im.uom_id == satu.uom_id) {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product" selected>'+satu.uom_name+'</option>';
                                } else {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                                }
                            });
                            html += '</select>';
                            html += '</div>';
                            html += '</td>';
                            html += '</tr>';
                            $('.form_hide_after_add').fadeOut();
                            $('.footer_form').fadeIn();
                            $('#inventory_item').append(html);
                            $(".single-select").select2();
                        });
                        $('#importIncoming').modal('hide');
                        if (r.data_fail.length > 0) {
                            $('#dataImportFail').html("");
                            var htmlFail = '';
                            $.each(r.data_fail, function (x, fail) {
                                htmlFail += '<tr class="item_fail">';
                                htmlFail += '<th class="text-center">'+fail.prod_code+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.prod_name+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.message+'<span class="required-label">*</span></th>';
                                htmlFail += '</tr>';
                            });
                            $('#dataImportFail').append(htmlFail);
                            $('#importfail').modal('show');
                        }
                        var total_row = $('.inventory_product').length;
                        $('.total_data').text(total_row);
                        hideLoader();
                    } else {
                        hideLoader();
                    }

                }
            });
        });

        $('#product_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('form#formImport input[name="file"]').click();
        });

        $('form#formImport input[name="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            // console.log(fileName);
            $('#image_name').text(fileName);
            // readURL(this,'#showing_product_image img');
            // console.log('The file "' + fileName +  '" has been selected.');
        });
    </script>
@endsection
