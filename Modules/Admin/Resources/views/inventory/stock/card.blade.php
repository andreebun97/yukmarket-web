
@extends('admin::layouts.master')

@section('title',__('menubar.stock_card'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_card') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray ">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_card') }}</h4>
                                            @if($accessed_menu == 1)
                                           
                                            @endif
                                            <div class="clearfix"></div>
                                           
                                        </div>
                                        <div class="panel-body padding-lr-20" >
                                            <form id="form_stock_card">
                                                {{ csrf_field() }}
                                                <div class="col-md-12 no-padding">
                                                    <div class="col-sm-3 no-padding-left res-no-pad-sm">
                                                        <div class="form-group margin-b10">
                                                            <label for="dim_length">Lokasi </label>
                                                            <div class="clearfix"></div>
                                                            <div class="clearfix"></div>
                                                            @if(is_null($user->warehouse_id))
                                                                <select name="sc_warehouse" class="form-control single-select sc_warehouse @error('product_category') is-invalid @enderror">
                                                                    <option value="">-- Pilih Gudang --</option>
                                                                    @foreach($all_warehouse as $warehouse)
                                                                    <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <input type="hidden" name="sc_warehouse" class="sc_warehouse" value="{{$user->warehouse_id}}">
                                                                <input type="text" name="sc_warehouse_name" class="form-control sc_warehouse_name" value="{{$user->warehouse_name}}" readonly="readonly" />
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 res-no-pad-sm">
                                                        <div class="form-group margin-b10">
                                                            <label for="dim_length">Produk</label>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                @if($wh_product != null)
                                                                    <select name="sc_product" class="form-control sc_product single-select @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Produk --</option>
                                                                        @foreach($wh_product as $prd)
                                                                            <option value="{{$prd->prod_id}}">{{$prd->prod_name . ' ' . $prd->uom_value . ' ' . $prd->uom_name }}</option>
                                                                            <!-- <option value="{{$prd->prod_id}}">{{$prd->prod_name}}</option> -->
                                                                        @endforeach
                                                                    </select>
                                                                    <span class="invalid-feedback error_sc_product"></span>
                                                                @else
                                                                    <select name="sc_product" class="form-control sc_product single-select @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Produk --</option>
                                                                    </select>
                                                                    <span class="invalid-feedback error_inventory_product"></span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-sm-3 res-no-pad-sm" >
                                                        <div class="form-group">
                                                            <label for="voucher_start_date">Periode <span class="required-label">*</span></label>
                                                            <div class="input-group padding-left-20">
                                                                <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                <!-- <input type="date" class="form-control" name="sc_date_from" value="{{ date('Y-m-d') }}" required> -->
                                                                <input type="text" class="form-control daterange-single"   value="{{ date('Y-m-d') }}" name="sc_date_from"  required> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 no-padding-right res-no-pad-sm" >
                                                        <div class="form-group ">
                                                            <label for="voucher_start_date">Sampai <span class="required-label">*</span></label>
                                                            <div class="input-group padding-left-20">
                                                                <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                <!-- <input type="date" class="form-control " value="{{ date('Y-m-d') }}" name="sc_date_to" required> -->
                                                                <input type="text" class="form-control daterange-single"   value="{{ date('Y-m-d') }}"  name="sc_date_to" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 no-padding">
                                                    <div class="col-md-3 col-xs-6 float-right no-padding margin-b20">
                                                        <button type="submit" class="btn btn-orange btn-block" id="sc_find">
                                                            <i class="fa fa-search"></i> Cari
                                                        </button>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>   
                                                <div class="clearfix"></div>
                                            </form>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <!-- <table class="table display w-100 table-brown" id="inventory_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>{{ __('field.product_code') }}</th>
                                                        <th>{{ __('field.product_name') }}</th>
                                                        <th>{{ __('field.product_stock') }}</th>
                                                        <th>{{ __('field.action') }}</th>
                                                    </tr>
                                                </thead>
                                            </table> -->
                                            <div class="table-no-datatable no-padding border-solid-top-gray padding-t20" >
                                                <div class="col-xs-12 res-scroll-md">
                                                <!-- <table class="table display w-100 table-brown border-head " id="productVariantTable"> -->
                                                    <table class="table display w-100 table-brown border-head datatable-basic datatable-scroll-y" id="table-sc">
                                                        <thead class="bg-darkgrey w-100">
                                                            <tr>
                                                                <th>No.</th>
                                                                <th>Tanggal</th>
                                                                <th>ID Transaksi</th>
                                                                <th>Tipe</th>
                                                                <th>QTY</th>
                                                                <th>Satuan Produk</th>
                                                                <th>Harga Rata-rata</th>
                                                                <th>Harga Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- <tr class="odd">
                                                                <td>1</td>
                                                                <td>10 Agustus 2020</td>
                                                                <td>123456789</td>
                                                                <td>Makanan</td>
                                                                <td>1</td>
                                                                <td>Rp 2000,00</td>
                                                                <td>Rp 2000,00</td>
                                                            </tr> -->
                                                        </tbody>
                                                        <tfoot class="bg-darkgrey">
                                                            <tr role="row">
                                                                <th colspan="3" class="bg-darkgrey text-right">Balance Akhir</th>
                                                                <th colspan="2" class="bg-darkgrey text-right"></th>
                                                                <th colspan="2" class="bg-darkgrey text-right">Total Keseluruhan</th>
                                                                <th colspan="1" class="bg-darkgrey text-right"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $('#form_stock_card').submit(function(e){
            e.preventDefault();
            console.log($(this).serializeArray());
            $('#table-sc').DataTable().clear().destroy();
            var categoryTable = $('#table-sc').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                filter: false,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("admin/inventory/findSC") }}',
                    type: "POST",
                    data: getFormData($(this))
                },
                bSort: false,
                'columnDefs': [
                        {
                            "targets": [4,5,6,7],
                            className: 'dt-body-right'
                        },
                    ],
                columns: [
                    {data: "no"},
                    {data: "sc_date"},
                    {data: "sc_code"},
                    {data: "sc_type"},
                    {data: "sc_qty"},
                    {data: "sc_unit"},
                    {
                        data: "sc_price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')    
                    },
                    {data: "sc_tot_price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ') 
                    }
                ],
                fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                    var api = this.api(), oSettings;

                    var intVal = function ( i ) {
                        var reg = typeof i === 'string' ? i.replace(/\D\-/g,'') * 1 : typeof i === 'number' ? i : 0;
                        return reg
                    };

                    total = api.column(7).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    qty = api.column(4).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    pageTotal = api.column( 7, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    $('tr:eq(0) th:eq(3)', api.table().footer() ).html(
                        $.fn.dataTable.render.number(',','','0','Rp. ').display(total)
                    );
                    $('tr:eq(0) th:eq(1)', api.table().footer() ).html(
                        qty
                    );

                    return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                }
            });
        });

        function getFormData(form, checkbox) {
            var unindexed_array = form.serializeArray();
            console.log(unindexed_array);
            var indexed_array = {};
            $.map(unindexed_array, function (n, i) {
                if (checkbox) {
                    if (n['name'].indexOf("[]") >= 0) {
                        var replace = n['name'].replace("[]", "");
                        indexed_array[replace] = checkbox;
                    } else {
                        indexed_array[n['name']] = n['value'];
                    }
                } else {
                    indexed_array[n['name']] = n['value'];
                }
            });

            return indexed_array;
        }

        $('.sc_warehouse').change(function(){
            showLoader();
            var wh_id = $(".sc_warehouse").val();
            console.log(wh_id);
            if(wh_id != ""){
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
                    success: function (r) {
                        console.log(r);
                        var strObj = JSON.stringify(r);
                        var obj64 = btoa(strObj);
                        //$('.sc_product').val(obj64);
                        var options = $(".sc_product");
                        options.empty();
                        options.prop('selectedIndex',0);
                        options.append($("<option />").val(null).text('--Pilih Produk--'));
                        if(r.product.length >0){
                            $.each(r.product, function(key,item) {
                            var prd_name = item.prod_name+'('+item.uom_value+' '+item.uom_name+')';
                            options.append($("<option />").val(item.prod_id).text(prd_name));
                            });
                            hideLoader();
                        }else{
                            options.append($("<option />").val(null).text('--Pilih Produk--'));
                            hideLoader();
                        }
                    }
                });
            }else{
                hideLoader();
            }
        });
    </script>
@endsection
