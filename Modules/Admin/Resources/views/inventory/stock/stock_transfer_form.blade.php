
@extends('admin::layouts.master')

@section('title',__('page.add_warehouse_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li>{{ __('menubar.stock_transfer') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ Request::get('id') == null ? __('page.add_warehouse_mutation') : __('page.warehouse_mutation_detail') }}</h4>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" method="POST" action="{{ route('admin/inventory/stock_transfer/form') }}" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" class="list_prod" value="">
                                                        <input type="hidden" name="inventory_org" value="{{ Session::get('users')['organization_id'] }}">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$no_trx}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Dari<span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    @if(Request::get('id') == null)
                                                                    <select name="inventory_warehouse" class="form-control single-select inventory_warehouse inven_wh @error('inventory_warehouse') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang Dari --</option>
                                                                        @foreach($inv_warehouse as $warehouse)
                                                                            <option value="{{$warehouse->warehouse_id}}"> {{ $warehouse->warehouse_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @else
                                                                    <input type="text" class="form-control" value="{{ $inventory_warehouse['warehouse_name'] }}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_warehouse">Gudang Tujuan<span class="required-label">*</span></label>
                                                                    <div class="clearfix"></div>
                                                                    @if(Request::get('id') == null)
                                                                    <select name="sales_warehouse" class="form-control single-select sales_warehouse inven_wh @error('sales_warehouse') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang Tujuan --</option>
                                                                        @foreach($inv_warehouse as $warehouse)
                                                                            <option value="{{$warehouse->warehouse_id}}"> {{ $warehouse->warehouse_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @else
                                                                    <input type="text" class="form-control" value="{{ $sales_warehouse['warehouse_name'] }}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Mutasi <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <input type="text" class="form-control daterange-single" name="inventory_date" value="{{ Request::get('id') == null ? date('Y-m-d') : date('Y-m-d',strtotime($warehouse_mutation_header['mutation_date'])) }}" min="{{ date('Y-m-d') }}">
                                                                         <!-- <input type="date" class="form-control" name="inventory_date" value="{{ Request::get('id') == null ? date('Y-m-d') : date('Y-m-d',strtotime($warehouse_mutation_header['mutation_date'])) }}" min="{{ date('Y-m-d') }}"> -->
                                                                    </div>


                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                    <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Status --</option>
                                                                        <option value="0">Tertunda</option>
                                                                        <option value="1">Selesai</option>
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <!-- <div class="col-md-4 pl0-pr20">
                                                            <div class="form-group">
                                                                <label for="product_stock">Keterangan</label>
                                                                <textarea type="text" name="inventory_desc" class="form-control inventory_desc" value="" readonly style="resize:none; height:120px;"></textarea>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" id="inventoryTableDetail" style="width:100%;display: none;">
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        @if(Request::get('id') == null)
                                                                        <th></th>
                                                                        @endif
                                                                        <th class="text-center">Produk Dari<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Kode Produk<span class="required-label">*</span></th> -->
                                                                        <th class="text-center">Harga Beli<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Kode Produk Tujuan<span class="required-label">*</span></th> -->
                                                                        <th class="text-center">Produk Tujuan<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">ID Produk<span class="required-label">*</span></th> -->
                                                                        <th class="text-center">Stok Dikirim<span class="required-label">*</span></th>
                                                                        <th class="text-center">Stok DIterima<span class="required-label">*</span></th>
                                                                        <!-- <th class="text-center">Total<span class="required-label">*</span></th>
                                                                        <th></th> -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    @if(Request::get('id') != null)
                                                                        @for($b = 0; $b < count($warehouse_mutation_detail); $b++)
                                                                        <tr>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['prod_name'] }}</td>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['prod_code'] }}</td>
                                                                            <td>{{ $currency->convertToCurrency($warehouse_mutation_detail[$b]['price']) }}</td>
                                                                            <td>{{ $warehouse_mutation_detail[$b]['stock'] . ' ' . $warehouse_mutation_detail[$b]['uom_name'] }}</td>
                                                                        </tr>
                                                                        @endfor
                                                                    @endif
                                                                </tbody>
                                                                <tfoot>
                                                                    <td colspan="6">
                                                                        <button type="button" class="btn btn-orange" id="addInvenProduct" data-id="1"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                        <button type="button" class="btn btn-orange" data-toggle="modal" data-target="#importIncoming"><i class="fa fa-upload"></i> Import Produk</button>
                                                                        <a class="btn btn-orange" href="{{ asset('files/template_upload_transfer_stok.xlsx') }}">
                                                                            <i class="fa fa-file-text"></i> Template
                                                                        </a>
                                                                    </td>
                                                                    <!-- <td class="text-right">Grand Total</td>
                                                                    <td class="text-left">
                                                                        <span class="stock_total" style="float:right">0</span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="price_total" style="float:right">0</span>
                                                                    </td> -->
                                                                    <!-- <th class="text-left price_total" colspan="1">0</th> -->
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @if(Request::get('id') == null)
                                                    <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importIncoming" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importIncomingLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Import Transfer Stok</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <form method="POST" enctype="multipart/form-data" id="formImport">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="wh_id" id="wh_id" value="">
                        <div class="form-group">
                            <label for="product_picture" class="dp-block">File Excel  <span class="required-label">*</span></label>
                            <div class="clearfix"></div>
                            <div class="input-img-name">
                                <span id="image_name">Tidak Ada Data</span>
                                <input type="file" name="file" class="form-control" value="" style="display:none;">
                                <button type="button" class="btn btn-green-upload no-margin" id="product_picture_button">Cari</button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Footer -->
                    <div class="modal-footer border-top-grey" >
                        <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                            <div class="col-xs-6 padding-r-10 res-no-pad-sm margin-b10">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <button type="submit" id="submitFilterButton" value="1" name="activity_button" class="btn btn-orange btn-block">Unggah</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importfail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importfailLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Produk Gagal Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <table class="table display w-100 table-brown border-head datatable-basic" id="table-fail">
                        <thead class="bg-darkgrey w-100">
                        <tr>
                            <th class="text-center">Kode Produk Dari<span class="required-label">*</span></th>
                            <th class="text-center">Nama Produk Dari<span class="required-label">*</span></th>
                            <th class="text-center">Kode Produk Tujuan<span class="required-label">*</span></th>
                            <th class="text-center">Nama Produk Tujuan<span class="required-label">*</span></th>
                            <th class="text-center">Pesan<span class="required-label">*</span></th>
                        </tr>
                        </thead>
                        <tbody id="dataImportFail">

                        </tbody>
                    </table>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                        <div class="col-xs-12 padding-r-10 res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('.inven_wh').change(function(){
            showLoader();
            $('.inventory_item').remove();
            var wh_id = $('.inventory_warehouse').val();
            var wh_id_to = $('.sales_warehouse').val();
            if (wh_id != '' && wh_id_to != '') {
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/getProductByWhTf') }}/"+wh_id+"/"+wh_id_to,
                    success: function (r) {
                        console.log(r.product_form.length);
                        if (r.product_form.length > 0 && r.product_to.length > 0) {
                            var strObj = JSON.stringify(r);
                            var obj64 = btoa(strObj);
                            // console.log(obj64);
                            $('.list_prod').val(obj64);
                            $('#inventoryTableDetail').fadeIn();
                            hideLoader();
                        } else {
                            $('#inventoryTableDetail').fadeOut();
                            if (r.product_form.length <= 0) {
                                $('.modal-custom-text p').text("Gudang Dari Tidak Memiliki Produk, Silahkan Pilih Gudang Lain.");
                            } else {
                                $('.modal-custom-text p').text("Gudang Tujuan Tidak Memiliki Produk, Silahkan Pilih Gudang Lain.");
                            }
                            $("#notificationPopup .modal-header").removeClass('success');
                            $("#notificationPopup .modal-header").addClass('error');
                            $('#notificationPopup').modal('show');
                            hideLoader();
                        }
                    }
                });
            } else {
                $('#inventoryTableDetail').fadeOut();
                hideLoader();
            }
        });

        $('#addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            console.log(id+'-'+i);
            $('#addInvenProduct').attr('data-id', i);
            var wh_id = $('.inventory_warehouse').val();
            var list_prod = atob($('.list_prod').val());
            var objProd = JSON.parse(list_prod);
            console.log(objProd.product);
            html = '<tr class="inventory_item inventory_item_'+i+'">';
            html+= '<td>';
            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
            html+= '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            // html += '<select name="inventory_product[]" class="form-control inventory_product single-select" onchange="listProduct(this)">';
            html += '<select name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(objProd.product_form, function (i, item) {
                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
            });
            html += '</select>';
            html += '<span class="inventory_produk_code"></span>';
            html += '<span class="invalid-feedback inventory_product"></span>';
            html += '</div>';
            html += '</td>';
            // html += '<td>';
            // html += '<div class="form-group">';
            // html += '<input type="text" name="inventory_produk_code[]" class="form-control inventory_produk_code" value="" readonly="readonly" />';
            // html += '</div>';
            // html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<input type="text" name="inventory_price[]" class="form-control inventory_price" onkeyup="currencyFormat(this)" value="" readonly="readonly" />';
            html += '</div>';
            html += '</td>';
            // html += '<td>';
            // html += '<div class="form-group">';
            // html += '<input type="text" name="inventory_produk_code_to[]" class="form-control inventory_produk_code_to" value="" readonly="readonly" />';
            // html += '</div>';
            // html += '</td>';
            html += '<td>';
            html += '<div class="form-group">';
            html += '<select name="inventory_product_to[]" class="form-control inventory_product_to single-select" onchange="inventoryProductTo(this)">';
            html += '<option value="">-- Pilih Produk --</option>';
            $.each(objProd.product_to, function (i, item) {
                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
            });
            html += '</select>';
            html += '<span class="inventory_produk_code_to"></span>';
            html += '<span class="invalid-feedback inventory_product_to"></span>';
            html += '</div>';
            html += '</td>';
            // html += '<td>';
            // html += '<div class="form-group">';
            // html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
            // html += '</div>';
            // html += '</td>';
            html += '<td>';
            html += '<div class="row">';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock text-right" value="" required/>';
            // html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror">';
            html += '<option value="">-- Pilih Satuan --</option>';
            $.each(objProd.uom, function (k, satu) {
                html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
            });
            html += '</select>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="row">';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<input type="text" id="inventory_stock_to'+i+'" name="inventory_stock_to[]" class="form-control inventory_stock_to text-right" value="" required/>';
            // html += '<input type="hidden" id="final_stock_to'+i+'" name="final_stock_to[]" class="form-control final_stock_to" />';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-6">';
            html += '<div class="form-group">';
            html += '<select type="text" id="satuan_produk_to'+i+'" name="satuan_produk_to[]" class="form-control satuan_produk_to single-select @error('product_category') is-invalid @enderror">';
            html += '<option value="">-- Pilih Satuan --</option>';
            $.each(objProd.uom, function (k, satu) {
                html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
            });
            html += '</select>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            // $('.form_hide_after_add').fadeOut();
            // $('.footer_form').fadeIn();
            $('#inventory_item').append(html);
            $(".single-select").select2();
            hideLoader();
        });

        function inventoryProduct(i){
            showLoader();
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            var prod_code = $(i).select2().find(":selected").data("code");
            var prod_price = $(i).select2().find(":selected").data("price");
            row.find('.inventory_produk_code').text("Kode Produk : "+prod_code);
            row.find('.inventory_price').val(prod_price).keyup();
            hideLoader();
        }

        function inventoryProductTo(i){
            showLoader();
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            var prod_code = $(i).select2().find(":selected").data("code");
            row.find('.inventory_produk_code_to').text("Kode Produk : "+prod_code);
            hideLoader();
        }

        function listProduct(i){
            // console.log($(i).val());
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            row.find('.inventory_list_product_to').remove();
            showLoader();
            console.log(row);
            if (prod_id == '') {
                hideLoader();
            } else {
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/listProductById') }}/"+prod_id,
                    success: function (r) {
                        console.log(r);
                        var html = '';
                        $.each(r, function (i, item) {
                            html += '<option value="'+item.prod_id+'" class="inventory_list_product_to">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                        });
                        row.find('select.inventory_product_to').append(html);
                         $.ajax({
                            type: "GET",
                            url: "{{ url('/admin/inventory/getProductById') }}/"+prod_id,
                            success: function (r) {
                                console.log(r);
                                // row.find('.inventory_produk_code').val(r.prod_code);
                                row.find('.inventory_price').val(r.inventory_price).keyup();
                                // row.find('.satuan_produk').val(r.uom_id).trigger('change');
                                // row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');
                                hideLoader();
                            }
                        });
                        // row.find('.inventory_produk_code').val(r.prod_code);
                        // row.find('.inventory_price').val(r.inventory_price).keyup();
                        // row.find('.satuan_produk').val(r.uom_id).trigger('change');
                        // row.find('.inventory_produk_unit').val(r.uom_value+' '+r.uom_name).trigger('change');
                        // hideLoader();
                    }
                });
            }
        }

        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
        }

        $('select[name="sales_warehouse"]').change(function(){
            if($(this).val() == ""){
                $('#addInven').attr('disabled',true);
            }else{
                $('#addInven').attr('disabled',false);
            }
        });

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            // console.log('test');
            showLoader();
            console.log($(this).serialize());
            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/stock_transfer/form") }}',
                data: $(this).serialize(),
                success: function (r) {
                    console.log(r);
                    hideLoader();
                    if (r.code != '200') {
                        $.each(r.messages, function (i, item) {
                            $('.error_'+i).text(item);
                        });
                        hideLoader();
                    } else {
                        hideLoader();
                        $('.modal-custom-text p').text(r.messages);
                        $('#TambahStok').modal('hide');
                        $("#notificationPopup .modal-header").addClass('success');
                        $("#notificationPopup .modal-header").removeClass('error');
                        $('#notificationPopup').modal('show');
                        // $('#inventory_form')[0].reset();
                        window.location.replace("<?= route('admin/inventory/stock_transfer') ?>");
                    }
                    $('.inventory_product_code').val(r.prod_code);
                }
            });
        });

        clickCancelButton('{{ route("admin/inventory/stock_transfer") }}')


        $('#formImport').submit(function(e){
            e.preventDefault();

            var wh_id = $('select[name="inventory_warehouse"]').val();
            var wh_id_to = $('select[name="sales_warehouse"]').val();

            var formData  = new FormData(this);
            formData.append('wh_id', wh_id);
            formData.append('wh_id_to', wh_id_to);

            showLoader();

            $.ajax({
                type: "POST",
                url: '{{ route("admin/inventory/importTransferStok") }}',
                data: formData,
                contentType: false,
                processData: false,
                success: function (r) {
                    console.log(r);
                    if (r.isSuccess == true) {
                        $("input[name='file']").val('');
                        $('#image_name').text("Tidak Ada Data");
                        $.each(r.data, function (q, im) {
                            // increment data_id button
                            var id = parseInt($('#addInvenProduct').attr('data-id'));
                            i = id + 1;
                            console.log(id+'-'+i);
                            $('#addInvenProduct').attr('data-id', i);

                            // get list prod obj
                            var list_prod = atob($('.list_prod').val());
                            var objProd = JSON.parse(list_prod);

                            var kode_prod_from;
                            var kode_prod_to;

                            // create table
                            html = '<tr class="inventory_item inventory_item_'+i+'">';
                            html+= '<td>';
                            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                            html+= '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<select name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(objProd.product_form, function (i, item) {
                                if (im.prod_id == item.prod_id) {
                                    kode_prod_from = item.prod_code;
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'" selected>'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                } else {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                }
                            });
                            html += '</select>';
                            html += '<span class="inventory_produk_code">Kode Produk : '+kode_prod_from+'</span>';
                            html += '<span class="invalid-feedback inventory_product"></span>';
                            html += '</div>';
                            html += '</td>';
                            // html += '<td>';
                            // html += '<div class="form-group">';
                            // html += '<input type="text" name="inventory_produk_code[]" class="form-control inventory_produk_code" value="" readonly="readonly" />';
                            // html += '</div>';
                            // html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<input type="text" name="inventory_price[]" class="form-control inventory_price" onkeyup="currencyFormat(this)" value="'+im.prod_inventory_price+'" readonly="readonly" />';
                            html += '</div>';
                            html += '</td>';
                            // html += '<td>';
                            // html += '<div class="form-group">';
                            // html += '<input type="text" name="inventory_produk_code_to[]" class="form-control inventory_produk_code_to" value="" readonly="readonly" />';
                            // html += '</div>';
                            // html += '</td>';
                            html += '<td>';
                            html += '<div class="form-group">';
                            html += '<select name="inventory_product_to[]" class="form-control inventory_product_to single-select" onchange="inventoryProductTo(this)">';
                            html += '<option value="">-- Pilih Produk --</option>';
                            $.each(objProd.product_to, function (i, item) {
                                if (im.prod_id_to == item.prod_id){
                                    kode_prod_to = item.prod_code;
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'" selected>'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                } else {
                                    html += '<option value="'+item.prod_id+'" class="inventory_list_product" data-code="'+item.prod_code+'" data-price="'+item.inventory_price+'">'+item.prod_name+'('+item.uom_value+' '+item.uom_name+') </option>';
                                }

                                // html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
                            });
                            html += '</select>';
                            html += '<span class="inventory_produk_code_to">Kode Produk : '+kode_prod_to+'</span>';
                            html += '<span class="invalid-feedback inventory_product_to"></span>';
                            html += '</div>';
                            html += '</td>';
                            // html += '<td>';
                            // html += '<div class="form-group">';
                            // html += '<input type="text" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
                            // html += '</div>';
                            // html += '</td>';
                            html += '<td>';
                            html += '<div class="row">';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock text-right" value="'+im.stock+'" required/>';
                            // html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror">';
                            html += '<option value="">-- Pilih Satuan --</option>';
                            $.each(objProd.uom, function (k, satu) {
                                if (im.uom_id == satu.uom_id) {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product" selected>'+satu.uom_name+'</option>';
                                } else {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                                }

                            });
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<div class="row">';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<input type="text" id="inventory_stock_to'+i+'" name="inventory_stock_to[]" class="form-control inventory_stock_to text-right" value="'+im.stock_to+'" required/>';
                            // html += '<input type="hidden" id="final_stock_to'+i+'" name="final_stock_to[]" class="form-control final_stock_to" />';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="col-sm-6">';
                            html += '<div class="form-group">';
                            html += '<select type="text" id="satuan_produk_to'+i+'" name="satuan_produk_to[]" class="form-control satuan_produk_to single-select @error('product_category') is-invalid @enderror">';
                            html += '<option value="">-- Pilih Satuan --</option>';
                            $.each(objProd.uom, function (k, satu) {
                                if (im.uom_id_to == satu.uom_id) {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product" selected>'+satu.uom_name+'</option>';
                                } else {
                                    html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                                }
                            });
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</td>';
                            html += '</tr>';
                            $('.form_hide_after_add').fadeOut();
                            $('.footer_form').fadeIn();
                            $('#inventory_item').append(html);
                            $(".single-select").select2();
                        });
                        $('#importIncoming').modal('hide');
                        if (r.data_fail.length > 0) {
                            $('#dataImportFail').html("");
                            var htmlFail = '';
                            $.each(r.data_fail, function (x, fail) {
                                htmlFail += '<tr class="item_fail">';
                                htmlFail += '<th class="text-center">'+fail.prod_code+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.prod_name+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.prod_code_to+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.prod_name_to+'<span class="required-label">*</span></th>';
                                htmlFail += '<th class="text-center">'+fail.message+'<span class="required-label">*</span></th>';
                                htmlFail += '</tr>';
                            });
                            $('#dataImportFail').append(htmlFail);
                            $('#importfail').modal('show');
                        }
                        var total_row = $('.inventory_product').length;
                        $('.total_data').text(total_row);
                        hideLoader();
                    } else {
                        hideLoader();
                    }

                }
            });
        });

        $('#product_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('form#formImport input[name="file"]').click();
        });

        $('form#formImport input[name="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            // console.log(fileName);
            $('#image_name').text(fileName);
            // readURL(this,'#showing_product_image img');
            // console.log('The file "' + fileName +  '" has been selected.');
        });
    </script>
@endsection
