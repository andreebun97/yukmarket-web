
@extends('admin::layouts.master')

@section('title',__('menubar.stock_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_mutation') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_mutation') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/stock/mutation/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_stock_mutation') }}</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                                <table class="table display w-100 table-brown datatable-basic datatable-scroll-y" id="table-incoming-goods" >
                                                    <thead class="bg-darkgrey w-100">
                                                        <tr>
                                                            <th>{{ __('field.action') }}</th>
                                                            <th>No.</th>
                                                            <th>No. Transaksi</th>
                                                            <th>Tanggal Masuk</th>
                                                            <th>Gudang</th>
                                                            <th>Status</th>
                                                            <th>Deskripsi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        var url = "<?= url('admin/inventory/getStockMutation') ?>";
        loadDatatable(url);
        function loadDatatable(url){
            $('#table-incoming-goods').DataTable().clear().destroy();
            var categoryTable = $('#table-incoming-goods').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                ajax: {
                    url: url
                },
                columns: [
                    {data: "action"},
                    {data: "no"},
                    {data: "trx_no"},
                    {data: "trx_date"},
                    {data: "warehouse_name"},
                    {data: "trx_status"},
                    {data: "trx_desc"},
                ],
                fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                    return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                }
            });
        }

    </script>
@endsection
