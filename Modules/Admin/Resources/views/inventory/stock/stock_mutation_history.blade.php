
@extends('admin::layouts.master')

@section('title',__('menubar.stock_mutation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'inventory'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-industry"></i> {{ __('menubar.inventory') }}</li>
                                <li class="active text-bold">{{ __('menubar.stock_mutation') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.stock_mutation') }}</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="tabbable">
                                            <div class="tab-content">
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="inventory_org" value="{{$trx_header->organization_id}}">
                                                        <input type="hidden" name="id_detail" value="{{$id_detail}}">
                                                        <div class="col-md-8">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="product_stock">No. Transaksi</label>
                                                                    <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$trx_header->trx_code}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Gudang</label>
                                                                    <div class="clearfix"></div>
                                                                    <input type="hidden" name="inventory_warehouse" class="inventory_warehouse" value="{{$trx_header->warehouse_id}}">
                                                                    <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$trx_header->warehouse_name}}" readonly="readonly" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="inventory_date">Tanggal Mutasi </label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <input type="text" class="form-control daterange-single" name="inventory_date" value="{{date('Y-m-d', strtotime($trx_header->trx_date))}}" min="{{date('Y-m-d')}}"  readonly>
                                                                        <!-- <input type="date" class="form-control" name="inventory_date" value="{{date('Y-m-d', strtotime($trx_header->trx_date))}}" min="{{date('Y-m-d')}}" readonly> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="dim_length">Status</label>
                                                                    <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror" disabled>
                                                                        <option value="">-- Pilih Status --</option>
                                                                        <option value="0" {{ $trx_header->trx_status == 0 ? 'selected' : '' }}>Tunda</option>
                                                                        <option value="1" {{ $trx_header->trx_status == 1 ? 'selected' : '' }}>Selesai</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 pl0-pr20">
                                                            <div class="form-group">
                                                                <label for="product_stock">Keterangan</label>
                                                                <textarea type="text" name="inventory_desc" class="form-control inventory_desc" value="{{$trx_header->trx_desc}}" readonly style="resize:none; height:120px;"> </textarea> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form_body">
                                                        <div class="table-no-datatable" >
                                                            <table class="table display w-100 table-brown border-head datatable-basic" id="table-history">
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th class="text-center">Nama Produk</th>
                                                                        <th class="text-center">ID Produk</th>
                                                                        <th class="text-center">Harga Beli</th>
                                                                        <th class="text-center">Stok</th>
                                                                        <th class="text-center">Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                                <tfoot class="bg-darkgrey">
                                                                    <tr role="row">
                                                                        <td colspan="4" class="text-right">Grand Total</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <a href="<?= url('admin/inventory/stock/mutation'); ?>" class="btn btn-gray btn-block ">{{ __('page.cancel') }}</a>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        var url = "<?= url('admin/inventory/historyDetail') ?>";
        var id = $('input[name="id_detail"]').val();
        loadDatatable(url, id);
        function loadDatatable(url, id){
            $('#table-history').DataTable().clear().destroy();
            var categoryTable = $('#table-history').on('processing.dt', function ( e, settings, processing ) {
                $('.dataTables_processing').remove();
                if(processing){
                    showLoader();
                }else{
                    hideLoader();
                }
            }).DataTable({
                scrollX: true,
                language: {
                    paginate: {
                        previous: '<i class="fa fa-chevron-left"></i>',
                        next: '<i class="fa fa-chevron-right"></i>'
                    },
                    lengthMenu: '{{ __("page.showing") }} <select name="category_list_length">'+
                        '<option value="5">5</option>'+
                        '<option value="10">10</option>'+
                        '<option value="20">20</option>'+
                        '<option value="25">25</option>'+
                        '<option value="50">50</option>'+
                        '</select> data',
                    emptyTable: '{{ __("page.no_data") }}'
                },
                oLanguage: {
                    sSearch: "{{ __('page.search') }}:"
                },
                lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
                processing: true,
                serverSide: true,
                ajax: {
                    url: url+'/'+id
                },
                columns: [
                    {data: "formatted_product_name"},
                    {data: "prod_code"},
                    {
                        data: "price",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')
                    },
                    {data: "stock"},
                    {
                        data: "total",
                        render: $.fn.dataTable.render.number(',','','0','Rp. ')    
                    }
                ]
                ,
                fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                    var api = this.api(), oSettings;
                    
                    var intVal = function ( i ) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };

                    total = api.column(4).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    pageTotal = api.column(4, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    $( api.column(4).footer() ).html(
                        '( ' + $.fn.dataTable.render.number(',','','0','Rp. ').display(total) + ' )'
                    );
                    return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                }
            });
        }
    </script>
@endsection
