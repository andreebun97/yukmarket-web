@extends('admin::layouts.master')

@section('title',__('menubar.user_management'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'user_management'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-group position-left"></i>{{ __('menubar.user_management') }}</li>
                                <li class="active text-bold ">{{ __('menubar.user_list') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card-custom">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.user_list') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div id="add_user_button" class="float-right">
                                                <a href="{{ route('admin/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_user') }}</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="user_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.user_name') }}</th>
                                                        <th>{{ __('field.user_email') }}</th>
                                                        <th>{{ __('field.user_phone_number') }}</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('error_popup')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var userTable = $('#user_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="admin_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_users") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function (d) {
                    d.role = '{{ Request::get("role") }}'
                    console.log(d);
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false
                },
                {
                    data: null,
                    sortable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'user_name', name: 'user_name'},
                {data: 'user_email', name: 'user_email'},
                {data: 'user_phone_number', name: 'user_phone_number'},
            ],
            initComplete: function() {
                // $('.dataTables_filter').html($('.dataTables_filter').html().replace('Search','{{ __("page.search") }}'));
                $('.dataTables_info').html($('.dataTables_info').html().replace('Showing','{{ __("page.showing") }}'));
                $('.dataTables_info').html($('.dataTables_info').html().replace('to','{{ __("page.to") }}'));
                $('.dataTables_info').html($('.dataTables_info').html().replace('of','{{ __("page.of") }}'));
                $('.dataTables_info').html($('.dataTables_info').html().replace('entries','{{ __("page.entries") }}'));
                $('a.paginate_button.previous').html('<i class="fa fa-chevron-left"></i>');
                $('a.paginate_button.next').html('<i class="fa fa-chevron-right"></i>');
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        userTable.on('draw', function(){
            $('.dataTables_info').html($('.dataTables_info').html().replace('Showing','{{ __("page.showing") }}'));
            $('.dataTables_info').html($('.dataTables_info').html().replace('to','{{ __("page.to") }}'));
            $('.dataTables_info').html($('.dataTables_info').html().replace('of','{{ __("page.of") }}'));
            $('.dataTables_info').html($('.dataTables_info').html().replace('entries','{{ __("page.entries") }}'));
            $('a.paginate_button.previous').html('<i class="fa fa-chevron-left"></i>');
            $('a.paginate_button.next').html('<i class="fa fa-chevron-right"></i>');
        });

        
    </script>
    @endif
@endsection
