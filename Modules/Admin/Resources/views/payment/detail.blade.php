
@extends('admin::layouts.master')

@section('title',__('page.transaction_detail'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li>Pembayaran</li>
                                <li class="active text-bold ">{{ __('page.transaction_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.transaction_detail') }}</h4>
                                        <div class="clearfix"></div>
                                    </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                        <div class="row no-padding">
                                            <div class="payments col-md-12 trs-detail">
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div id="user_profile " class="info-detail">
                                                        <h2>{{ __('page.customer_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.user_name') }}</label>
                                                            <span  class="user_data_information col-md-7 col-sm-7"> {{ $order_master->contact_person == null ? ($order_master->customer_name != null ? $order_master->customer_name : $order_master->buyer_name) : ($order_master->contact_person) }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.user_email') }}</label>
                                                            <span  class="user_data_information col-md-7 col-sm-7"> {{ $order_master->customer_email }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.user_phone_number') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7"> {{ $order_master->phone_number == null ? $phone_number->convert($order_master->customer_phone_number) : $phone_number->convert($order_master->phone_number) }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 ">
                                                    <div class="info-detail">
                                                        <h2>{{ __('page.payment_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.payment_status') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7">{{ $order_master->invoice_status_name }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.payment_date') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7"> {{ $order_master->transfer_date == null ? '-' : $order_master->transfer_date }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.payment_method') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7"> {{ $order_master->payment_method_name }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.order_code'). ' (Midtrans)' }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7"> {{ ($order_master->midtrans_transaction_id == null ? '-' : $order_master->midtrans_transaction_id) }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="info-detail">
                                                        <h2>{{ __('page.shipment_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.destination_address') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7">{{ $order_master->destination_address == null ? ($order_master->buyer_address == null ? "-" : $order_master->buyer_address) :  $order_master->address_detail . ', ' . ($order_master->kelurahan_desa_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kabupaten_kota_name . ', ' . $order_master->kode_pos . " (".($order_master->address_info == null ? "-" : $order_master->address_info).", ".($order_master->address_name == null ? "-" : $order_master->address_name).")") }}</span>
                                                        </div>   
                                                        <div class="clearfix"></div>
                                                        <!-- <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.contact_person') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7">{{ $order_master->contact_person == null ? ($order_master->customer_name. ' (' . $phone_number->convert($order_master->customer_phone_number) . ')') : ($order_master->contact_person . ' (' . $phone_number->convert($order_master->phone_number) . ')') }}</span>
                                                        </div>
                                                        <div class="clearfix"></div> -->
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-5 col-sm-5">{{ __('field.shipment_method') }}</label>
                                                            <span class="user_data_information col-md-7 col-sm-7">{{ $order_master->shipment_method_name }}</span>      
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                    <div id="user_transaction" class="info-detail">
                                                        <h2>{{ __('page.transactions') }}</h2>
                                                        <div id="purchase_transaction">
                                                            <div class="form-group">
                                                                <div id="purchase_transaction_detail">
                                                                        @if(count($transactions) == 0)  
                                                                            <span>{{ __('page.no_data') }}</span>
                                                                        @else
                                                                        <div class="col-md-5 float-left no-padding">
                                                                            <div class="info-orange display-block font-14">
                                                                                {{ __('field.order_code') }} <bold>: {{ $order_master->order_code }}</bold>
                                                                                <span style="display: block;">{{ __('field.order_status') }} <bold>: {{ $order_master->order_status_name }}</bold></span>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        
                                                                        <div class="col-xs-12 res-scroll-md no-padding">
                                                                            <table class="table display w-100 table-brown total-transaksi" id="banner_list_table">
                                                                                <thead class="bg-darkgrey">
                                                                                    <tr role="row">
                                                                                        <th>Gambar</th>
                                                                                        <th>Kode</th>
                                                                                        <th style="min-width:200px;" >Nama</th>
                                                                                        <th class="text-right">Jumlah</th>
                                                                                        <th class="text-right">Harga</th>
                                                                                        <th class="text-right">Total</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @php $subtotal = 0; @endphp
                                                                                    @for($b = 0; $b < count($transactions); $b++)
                                                                                    @php $subtotal += ($transactions[$b]['quantity'] *  $transactions[$b]['price']) - $transactions[$b]['promo_value']; @endphp
                                                                                    <tr {{ $b < count($transactions) - 1 ? '' : ''}}>
                                                                                        <td><img src="{{ asset($transactions[$b]['prod_image']) }}" class="img-50" alt="" onerror="this.src='{{ asset('img/default_product.jpg') }}'"></td>
                                                                                        <td>{{ $transactions[$b]['prod_code'] }}</td>
                                                                                        <td>{{ ' ('. $transactions[$b]['prod_name'] . ($transactions[$b]['sku_status'] == 0 ? '' : (' '.$transactions[$b]['uom_value'].' '.$transactions[$b]['uom_name'])) . ')' }}</td>
                                                                                        <td class="text-right">{{ $transactions[$b]['quantity'] }}</td>
                                                                                        <td class="text-right">{{ $currency->convertToCurrency($transactions[$b]['price']) }}</td>
                                                                                        <td class="text-right">{{ $currency->convertToCurrency(($transactions[$b]['quantity'] *  $transactions[$b]['price']) - $transactions[$b]['promo_value']) }}</td>
                                                                                    </tr>
                                                                                        @endfor
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                       
                                                                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-7 no-padding float-right">
                                                                            <div class="total-wrap">
                                                                                <div class="col-md-12 no-padding">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span  class="dp-block color-black">{{ __('field.subtotal') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($subtotal) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                @php
                                                                                    $national_income_tax = 0;
                                                                                    $tax_basis = 0;
                                                                                    $balance = 0;
                                                                                    if($order_master->pricing_include_tax == 0){
                                                                                        $national_income_tax = ROUND($subtotal * $order_master->national_income_tax);
                                                                                        $subtotal = $subtotal + $national_income_tax;
                                                                                    }else{
                                                                                        $tax_basis = ROUND($subtotal / 1.1);
                                                                                        $national_income_tax = $subtotal - $tax_basis;
                                                                                    }
                                                                                @endphp
                                                                                @if($order_master->pricing_include_tax == 1)
                                                                                <div class="col-md-12 no-padding tax-dpp">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span  class="dp-block color-black">{{ __('field.tax_basis') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($tax_basis) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                @endif
                                                                                <div class="col-md-12 no-padding tax-ppn">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span  class="dp-block color-black">{{ __('field.national_income_tax') }} @if($order_master->pricing_include_tax == 0) {{ '('.($order_master->national_income_tax*100).'%)' }} @endif</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black">{{ $currency->convertToCurrency($national_income_tax) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 no-padding d-none">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class="dp-block color-black">{{ __('field.voucher') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        @php
                                                                                        $voucher_amount = 0;
                                                                                        @endphp
                                                                                        <span class="float-right dp-block color-red" > {{ $order_master->voucher_id == null ? '-' : ($order_master->is_fixed == 'Y' ? $currency->convertToCurrency($order_master->voucher_amount) : '-'.$currency->convertToCurrency($voucher_amount)) }}</span>
                                                                                        @php $subtotal_and_amount = $subtotal - $voucher_amount; @endphp
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 no-padding">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class="dp-block color-black">{{ __('field.shipment_fee') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black"> {{ $order_master->shipment_price == null ? '-' : $currency->convertToCurrency($order_master->shipment_price) }}</span>
                                                                                        @php
                                                                                            $all_admin_fee = 0;
                                                                                            $grand_total = $all_admin_fee + ($order_master->shipment_price == null ? 0 : $order_master->shipment_price) + $subtotal_and_amount;
                                                                                            $admin_fee = 0;
                                                                                            if($order_master->admin_fee != null){
                                                                                                $admin_fee = $order_master->admin_fee;
                                                                                                $all_admin_fee = $order_master->admin_fee;
                                                                                            }else if($order_master->admin_fee_percentage != null){
                                                                                                $admin_fee = $order_master->admin_fee_percentage;
                                                                                                $all_admin_fee = $grand_total * ($order_master->admin_fee_percentage / 100);
                                                                                            }
                                                                                            $grand_total  = ($order_master->shipment_price == null ? 0 : $order_master->shipment_price) + $subtotal_and_amount;
                                                                                        @endphp
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 no-padding d-none">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class="dp-block color-black">{{ __('field.admin_fee') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black">{{ $order_master->admin_fee == null && $order_master->admin_fee_percentage == null ? '-' : $currency->convertToCurrency(round($admin_fee)) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 no-padding">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class=" color-black dp-block font-bold">{{ __('field.total') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black font-bold">{{ $currency->convertToCurrency($grand_total) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                @php
                                                                                    $subtotal_payment = 0;
                                                                                    $total_payment = 0;
                                                                                    $admin_fee = 0;
                                                                                    $national_income_tax = 0;
                                                                                    $tax_basis = 0;
                                                                                @endphp
                                                                                @for($b = 0; $b < count($payment); $b++)
                                                                                    @php $subtotal_payment += $payment[$b]['grand_total']; @endphp
                                                                                @endfor
                                                                                @php
                                                                                    $insurance_amount = ($subtotal_payment - $grand_total);
                                                                                    $total_payment += $subtotal_payment;
                                                                                    
                                                                                    if($payment[0]['voucher_id'] != null){
                                                                                        $voucher_amount = ROUND($payment[0]['voucher_amount']);
                                                                                    }

                                                                                    $total_payment -= ROUND($payment[0]['voucher_amount']);

                                                                                    if($payment[0]['admin_fee'] != null){
                                                                                        $admin_fee = $payment[0]['admin_fee'];
                                                                                    }

                                                                                    if($payment[0]['admin_fee_percentage'] != null){
                                                                                        $admin_fee = ($total_payment * $payment[0]['admin_fee_percentage']/100);
                                                                                    }

                                                                                    if($payment[0]['return_amount'] != null){
                                                                                        $balance = $payment[0]['return_amount'];
                                                                                    }

                                                                                    $total_payment += (round($admin_fee) - round($balance));
                                                                                @endphp
                                                                                @if($insurance_amount != 0)
                                                                                <div class="col-md-12 no-padding">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class=" color-black dp-block font-bold">{{ __('field.insurance') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black font-bold">{{ $currency->convertToCurrency($insurance_amount) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                @endif
                                                                                <div class="total-trx col-sm-12 no-padding" data-toggle="modal" data-target="#paymentDetailModal">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span class=" color-black dp-block font-bold">{{ __('field.total_payment') }}<span class="smaller"> ⓘ</span></span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black font-bold">{{ $currency->convertToCurrency($total_payment) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <!-- <div class="clearfix"></div>
                                                                        <div class="separator margin-tb10"></div> -->
                                                                        
                                                                        <!-- <div class="col-md-12 no-padding res-dtl-text float-left">
                                                                            <div class="col-md-3 no-padding">
                                                                                <span class="dp-block">{{ __('field.purchased_date') }}</span>
                                                                            </div>
                                                                            <div class="col-md-9 no-padding">
                                                                                <span class="dp-block">: {{ $order_master->order_date }}</span>
                                                                            </div>
                                                                                <div class="clearfix"></div>
                                                                            <div class="col-md-3 no-padding">
                                                                                <span class="dp-block">{{ __('field.destination_address') }}</span>
                                                                            </div>
                                                                            <div class="col-md-9 no-padding">
                                                                                <span class="dp-block">: {{ $order_master->destination_address == null ? "-" :  $order_master->address_detail . ', ' . ($order_master->kabupaten_kota_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kelurahan_desa_name . ', ' . $order_master->kode_pos . " (".($order_master->address_info == null ? "-" : $order_master->address_info).", ".($order_master->address_name == null ? "-" : $order_master->address_name).")") }}</span>
                                                                            </div>
                                                                            <div class="col-md-3 no-padding">
                                                                                <span class="dp-block">{{ __('field.contact_person') }}</span>
                                                                            </div>
                                                                            <div class="col-md-9 no-padding">
                                                                                <span class="dp-block">: {{ $order_master->contact_person == null ? "-" : ($order_master->contact_person . ' (' . $order_master->phone_number . ')') }}</span>
                                                                            </div>
                                                                                <div class="clearfix"></div>
                                                                            <div class="col-md-3 no-padding">
                                                                                <span class="dp-block">{{ __('field.shipment_method') }}</span>
                                                                            </div>
                                                                            <div class="col-md-9 no-padding">
                                                                                <span class="dp-block">: {{ $order_master->shipment_method_name }}</span>
                                                                            </div>
                                                                                <div class="clearfix"></div>
                                                                        </div> -->
                                                                        <div class="clearfix"></div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="paymentDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="transactionDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transactionDetailModalLabel">{{ __('page.transaction_detail') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body rincian-transaksi" style="max-height:90vh">
                    <div class="info-orange display-block font-17">
                        <i class="fa fa-info-circle"></i>
                        {{ __('field.invoice_num') }}: <bold> {{ $payment[0]['invoice_no'] }}</bold>
                    </div>                                                                               
                    <table class="table display w-100 table-brown total-transaksi" id="payment_item_list_table">
                        <thead class="bg-darkgrey">
                            <tr role="row">
                                <th>Kode Transaksi</th>
                                <th class="text-right">Total Belanja</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($b = 0; $b < count($payment); $b++)
                                <tr>
                                    <td>{{ $payment[$b]['order_code'] }}</td>
                                    <td class="text-right">{{ $currency->convertToCurrency($payment[$b]['grand_total']) }}</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                    <div class="col-md-12 res-dtl-text no-padding">
                        <div class="total-wrap">
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.subtotal') }} </span>                                     
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black">{{ $currency->convertToCurrency($subtotal_payment) }}</span> 
                        </div>
                        @if($payment[0]['pricing_include_tax'] == 1)
                        <div class="col-md-6 no-padding tax-dpp d-none">
                            <span class="dp-block">{{ __('field.tax_basis') }} </span>                                     
                        </div>
                        <div class="col-md-6 no-padding tax-dpp d-none">
                            <span style="display:block" class="float-right color-black">{{ $currency->convertToCurrency($tax_basis) }}</span> 
                        </div>
                        @endif
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.voucher') }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block;" class="float-right color-red"> {{ '-'.($payment[0]['voucher_id'] == null ? '' : $currency->convertToCurrency($voucher_amount)) }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.admin_fee') }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black">{{ $payment[0]['admin_fee'] == null && $payment[0]['admin_fee_percentage'] ? '-' : $currency->convertToCurrency(ROUND($admin_fee)) }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.balance') }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-red">-{{ $payment[0]['return_amount'] == null ? '' : $currency->convertToCurrency(ROUND($balance)) }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="color-black font-bold">{{ __('field.total') }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black font-bold">{{ $currency->convertToCurrency($total_payment) }}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                      
                       
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
@endsection
