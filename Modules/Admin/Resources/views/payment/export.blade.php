<table border="2" width="100%">
    <thead>
        <tr>
            <th style="background-color:yellow">No</th>
            <th align="center" style="background-color:yellow">Kode Transaksi</th>
            <th align="center" style="background-color:yellow">Tanggal Transaksi</th>
            <th align="center" style="background-color:yellow">Nama Pembeli</th>
            <th align="center" style="background-color:yellow">Nama Agen/Ecommerce</th>
            <th align="center" style="background-color:yellow">Alamat Tujuan</th>
            <th align="center" style="background-color:yellow">Jenis Transaksi</th>
            <th align="center" style="background-color:yellow">Metode Pengiriman</th>
            <th align="center" style="background-color:yellow">Harga Pengiriman</th>
            <th align="center" style="background-color:yellow">Total Keseluruhan</th>
        </tr>
    </thead>
    <tbody>
        @for($b = 0; $b < count($transaction); $b++)
        <tr>
            <td>{{ $b+1 }}</td>
            <td>{{ $transaction[$b]['order_code'] }}</td>
            <td>{{ date('Y/m/d H:i:s',strtotime($transaction[$b]['order_date'])) }}</td>
            <td>{{ $transaction[$b]['buyer_user_name'] == null ? $transaction[$b]['buyer_name'] : $transaction[$b]['buyer_user_name'] }}</td>
            <td>{{ $transaction[$b]['organization_name'] }}</td>
            <td>{{ $transaction[$b]['destination_address'] != null ? ($transaction[$b]['address_detail'] . ', ' . $transaction[$b]['kelurahan_desa_name'] . ', ' . $transaction[$b]['kecamatan_name'] . ', ' . $transaction[$b]['kabupaten_kota_name'] . ', ' .  $transaction[$b]['kabupaten_kota_name'] . ', ' .$transaction[$b]['kode_pos']) : $transaction[$b]['buyer_address'] }}</td>
            <td>{{ $transaction[$b]['online_flag'] == 1 ? 'Online' : 'Offline' }}</td>
            <td>{{ $transaction[$b]['shipment_method_name'] }}</td>
            <td>{{ $transaction[$b]['shipment_price'] }}</td>
            <td>{{ $transaction[$b]['grand_total'] == null ? 0 : $transaction[$b]['grand_total'] }}</td>
        </tr>
        @endfor
    </tbody>
</table>