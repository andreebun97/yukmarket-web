@extends('admin::layouts.master')

@section('title',__('page.shipping_label'))

@section('content')
<div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'transaction'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            @if(isset($reprint))
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <li>{{ __('menubar.shipment_history') }}</li>
                                <li>{{ __('page.shipment_history_form') . ' ('.$purchased_products[0]['order_code'].')' }}</li>
                                <li class="active text-bold">{{ __('page.shipping_label') }}</li>
                            </ul>
                            @else
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <li>{{ __('menubar.picking') }}</li>
                                <li>{{ __('page.picking_form') . ' ('.$purchased_products[0]['order_code'].')' }}</li>
                                <li class="active text-bold ">{{ __('page.shipping_label') }}</li>
                            </ul>
                            @endif
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content vh100">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="panel panel-flat fit-height">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('page.shipping_label') }}</h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">    
                                            <div class="struk-wrap size-a5 printableArea"  id="printableArea" >
                                                <div class="struk">
                                                    
                                                    @if($purchased_products[0]['shipping_receipt_label'] != null)
                                                    <div class="struk-toped-wrap" >
                                                    <iframe  id="iframetoped" src="data:text/html, {{ $purchased_products[0]['shipping_receipt_label'] }}"></iframe>
                                                    </div>
                                                    @else
                                                    @if($purchased_products[0]['organization_type_id'] != 4)
                                                    <div class="yukmarket-logo">
                                                        <center><img src="{{ asset($logo) }}" alt="">
                                                        <div class="clearfix"></div></center>
                                                        <!-- <span class="print-page">1/1</span> -->
                                                    </div>
                                                    <div class="barcode">
                                                        <center>
                                                             {!! '<img class="w100 pad-lr-40" src="data:image/png;base64,' . DNS1D::getBarcodePNG($purchased_products[0]['order_code'], 'C39',1.8,60) . '" alt="barcode"/>'  !!}
                                                         </center>
                                                    </div>
                                                    <div class="resi-number">
                                                        <center><span class="d-block padding-20 font-bold color-black">{{ $purchased_products[0]['order_code'] }}</span></center>
                                                    </div>
                                                    @else
                                                    <div class="yukmarket-logo-2">
                                                        <div class="label-brand col-md-6">
                                                            <img class="organization_logo_pict" src="{{ asset($purchased_products[0]['organization_logo']) }}" alt="" onError="this.onerror=null;this.src='{{ asset($logo) }}';">
                                                        </div>
                                                        <div class="payment-type col-md-6">
                                                            <span>Non Tunai</span>
                                                        </div>
                                                        <!-- <span class="print-page">1/1</span> -->
                                                    </div>
                                                    <div class="label-ship col-md-5">
                                                        <div class="market-invoice">
                                                            <p>{{ $purchased_products[0]['invoice_no'] }}</p>
                                                        </div>
                                                        <div class="col-md-12 no-padding margin-b5">
                                                            <div class="col-md-6 no-pad-left">
                                                                <img class="shipment_logo_pict" src="{{ asset($purchased_products[0]['shipment_logo']) }}" alt="" onError="this.onerror=null;this.src='{{ asset($logo) }}';">
                                                            </div>
                                                            <div class="col-md-6 no-pad-right">
                                                                <p class="exp-courier">{{ explode('(',$purchased_products[0]['shipment_method_name'])[0] }}</p>
                                                                <p class="exp-pack">{{ str_replace(')','',explode('(',$purchased_products[0]['shipment_method_name'])[1]) }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="col-md-6 no-pad-left">
                                                                <p class="exp-load">Berat:</p>
                                                                <p class="exp-weight">{{ $total_bruto }} Kg</p>
                                                            </div>
                                                            <div class="col-md-6 no-pad-right">
                                                                <p class="exp-ongkir">Ongkir:</p>
                                                                <p class="exp-fee">Rp {{ $currency->convertToCurrency($purchased_products[0]['shipment_price']) }}</p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="barcode">
                                                        <center>
                                                             {!! '<img class="w100 pad-lr-40" src="data:image/png;base64,' . DNS1D::getBarcodePNG(($purchased_products[0]['shipping_receipt_num'] == null ? $purchased_products[0]['order_code'] : $purchased_products[0]['shipping_receipt_num']), 'C39',1.8,60) . '" alt="barcode"/>'  !!}
                                                         </center>
                                                    </div>
                                                    <div class="resi-number">
                                                        <center><span class="d-block padding-20 font-bold color-black">{{ ($purchased_products[0]['shipping_receipt_num'] == null ? $purchased_products[0]['order_code'] : $purchased_products[0]['shipping_receipt_num']) }}</span></center>
                                                    </div>
                                                    @endif
                                                    <div class="clearfix"></div>
                                                    <div class="yukmarket-destination-address">
                                                        <div class="col-xs-12 no-padding margin-b5">
                                                            <div class="col-xs-1 no-padding">
                                                            <span class="ico-print"><img src="{{ asset('img/label/ic_home.png') }}" ></span>
                                                            </div>
                                                            <div class="col-xs-11 no-padding">
                                                                <span class="d-block">{{ $purchased_products[0]['contact_person'] == null ? ($purchased_products[0]->customer_name != null ? $purchased_products[0]['customer_name'] : $purchased_products[0]['buyer_name']) : $purchased_products[0]['contact_person']  }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-xs-12 no-padding margin-b5">
                                                            <div class="col-xs-1 no-padding">
                                                                <span class="ico-print"><img src="{{ asset('img/label/ic_location.png') }}" ></span>
                                                            </div>
                                                            <div class="col-xs-11 no-padding">
                                                            <span class="d-block">{{ $purchased_products[0]['destination_address'] != null ? ($purchased_products[0]['address_detail'] . ', ' . $purchased_products[0]['kabupaten_kota_name'] . ', ' . $purchased_products[0]['kecamatan_name'] . ', ' . $purchased_products[0]['kelurahan_desa_name'] . ', ' . $purchased_products[0]['kode_pos'] .'('.($purchased_products[0]['address_info'] == "" ? "-" : $purchased_products[0]['address_info']).')') : $purchased_products[0]['buyer_address'] }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-xs-12 no-padding margin-b5">
                                                            <div class="col-xs-1 no-padding">
                                                                <span class="ico-print"><img src="{{ asset('img/label/ic_phone.png') }}" ></span>
                                                            </div>
                                                            <div class="col-xs-11 no-padding">
                                                                <span class="d-block">{{ $purchased_products[0]['phone_number'] == null ? $phone_number->convert($purchased_products[0]['customer_phone_number']) : ($phone_number->convert($purchased_products[0]['phone_number']) ) }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        
                                                        <!-- <div class="col-xs-12 no-padding margin-b5">
                                                            <div class="col-xs-1 no-padding">
                                                            <span class="ico-print"><img src="{{ asset('img/label/ic_email.png') }}" ></span>
                                                            </div>
                                                            <div class="col-xs-11 no-padding">
                                                                <span class="d-block">{{ $purchased_products[0]['customer_email'] }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div> -->
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="yukmarket-transaction-detail">
                                                        @if($purchased_products[0]['organization_type_id'] != 4)
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>{{ __('field.product_name') }}</th>
                                                                    <th>{{ __('field.product_quantity') }}</th>
                                                                    <!-- <th>{{ __('field.product_price') }}</th>
                                                                    <th>{{ __('field.total') }}</th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(count($purchased_products) == 0)
                                                                <tr>
                                                                    <td colspan="4">{{ __('page.no_data') }}</td>
                                                                </tr>
                                                                @else
                                                                    @php $grand_total = 0; $total_quantity = 0; @endphp
                                                                    @for($b = 0; $b < count($purchased_products); $b++)
                                                                        @php $total_per_product = ($purchased_products[$b]['purchased_quantity'] * $purchased_products[$b]['purchased_price']) + $purchased_products[$b]['promo_value']; @endphp
                                                                        @php $grand_total += $total_per_product; $total_quantity += $purchased_products[$b]['purchased_quantity']; @endphp
                                                                        <tr>
                                                                            <td>{{ $b + 1 . "." }}</td>
                                                                            <td>{{ $purchased_products[$b]['purchased_product_name'] . ($purchased_products[$b]['sku_status'] == 0 ? '' : (' '.$purchased_products[$b]['uom_value'].' '.$purchased_products[$b]['uom_name'])) }}</td>
                                                                            <td>{{ $purchased_products[$b]['purchased_quantity'] }}</td>
                                                                            <!-- <td>{{ $currency->convertToCurrency($purchased_products[$b]['purchased_price']) }}</td>
                                                                            <td>{{ $currency->convertToCurrency($total_per_product) }}</td> -->
                                                                        </tr>
                                                                    @endfor
                                                                    <!-- <tr>
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.subtotal') }}</strong></td>
                                                                        <td class="subtotal">{{ $currency->convertToCurrency($grand_total) }}</td>
                                                                    </tr> -->
                                                                    <!-- @php
                                                                        $voucher_amount = 0;
                                                                        if($purchased_products[0]['voucher_id'] != null){
                                                                            if($purchased_products[0]['voucher_fix_status'] == 'Y'){
                                                                                $voucher_amount = $purchased_products[0]['voucher_amount'];
                                                                            }else{
                                                                                $voucher_amount = $grand_total * ($purchased_products[0]['voucher_amount'] / 100);
                                                                                if($voucher_amount > $purchased_products[0]['max_provided_price']){
                                                                                    $voucher_amount = $purchased_products[0]['max_provided_price'];
                                                                                }
                                                                            }
                                                                        }
                                                                    @endphp
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.voucher') . ($purchased_products[0]['voucher_id'] == null ? '' : ' ('.$purchased_products[0]['voucher_code'].')') }}</strong></td>
                                                                        <td class="subtotal">{{ $voucher_amount == 0 ? "-" : ( $purchased_products[0]['voucher_fix_status'] == 'Y' ? ($currency->convertToCurrency($voucher_amount)) : ($purchased_products[0]['voucher_amount'].'%'.' (Maksimal '.$currency->convertToCurrency($voucher_amount).')') ) }}</td>
                                                                    </tr>
                                                                    @php
                                                                        $grand_total = $grand_total - $voucher_amount;
                                                                        $admin_fee = 0;
                                                                        if($purchased_products[0]['admin_fee'] != null){
                                                                            $admin_fee = $purchased_products[0]['admin_fee'];
                                                                        }
                                                                        if($purchased_products[0]['admin_fee_percentage'] != null){
                                                                            $admin_fee = $grand_total * ($purchased_products[0]['admin_fee_percentage'] / 100);
                                                                        }
                                                                        if($grand_total < 0){
                                                                            $grand_total = 0;
                                                                        }
                                                                    @endphp
                                                                    @php
                                                                        $shipment_fee = 0;
                                                                        if($purchased_products[0]['shipment_price'] != null){
                                                                            $shipment_fee = $purchased_products[0]['shipment_price'];
                                                                        }
                                                                    @endphp
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.shipment_fee') }}</strong></td>
                                                                        <td class="subtotal">{{ $shipment_fee == 0 ? "-" : $currency->convertToCurrency($shipment_fee) }}</td>
                                                                    </tr>
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.admin_fee') }}</strong></td>
                                                                        <td class="subtotal">{{ $admin_fee == 0 ? "-" : $currency->convertToCurrency($admin_fee) }}</td>
                                                                    </tr>
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.total') }}</strong></td>
                                                                        <td class="subtotal"><strong>{{ $currency->convertToCurrency($grand_total + $admin_fee + $shipment_fee) }}</strong></td>
                                                                    </tr> -->
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                        @else
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Produk</th>
                                                                    <th>Varian</th>
                                                                    <th>SKU</th>
                                                                    <th>Jumlah</th>
                                                                    <!-- <th>{{ __('field.product_price') }}</th>
                                                                    <th>{{ __('field.total') }}</th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(count($purchased_products) == 0)
                                                                <tr>
                                                                    <td colspan="4">{{ __('page.no_data') }}</td>
                                                                </tr>
                                                                @else
                                                                    @php $grand_total = 0; $total_quantity = 0; @endphp
                                                                    @for($b = 0; $b < count($purchased_products); $b++)
                                                                        @php $total_per_product = ($purchased_products[$b]['purchased_quantity'] * $purchased_products[$b]['purchased_price']) + $purchased_products[$b]['promo_value']; @endphp
                                                                        @php $grand_total += $total_per_product; $total_quantity += $purchased_products[$b]['purchased_quantity']; @endphp
                                                                        <tr>
                                                                            <td>{{ $b + 1 . ". " . ($purchased_products[$b]['purchased_product_name'] . ($purchased_products[$b]['sku_status'] == 0 ? '' : (' '.$purchased_products[$b]['uom_value'].' '.$purchased_products[$b]['uom_name']))) }}</td>
                                                                            <td>{{ $purchased_products[$b]['uom_value'] . ' ' . $purchased_products[$b]['uom_name'] . ', ' . $purchased_products[$b]['purchased_product_name'] }}</td>
                                                                            <td>{{ $purchased_products[$b]['product_sku_name'] }}</td>
                                                                            <td>{{ $purchased_products[$b]['purchased_quantity'] }}</td>
                                                                            <!-- <td>{{ $currency->convertToCurrency($purchased_products[$b]['purchased_price']) }}</td>
                                                                            <td>{{ $currency->convertToCurrency($total_per_product) }}</td> -->
                                                                        </tr>
                                                                    @endfor
                                                                    <!-- <tr>
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.subtotal') }}</strong></td>
                                                                        <td class="subtotal">{{ $currency->convertToCurrency($grand_total) }}</td>
                                                                    </tr> -->
                                                                    <!-- @php
                                                                        $voucher_amount = 0;
                                                                        if($purchased_products[0]['voucher_id'] != null){
                                                                            if($purchased_products[0]['voucher_fix_status'] == 'Y'){
                                                                                $voucher_amount = $purchased_products[0]['voucher_amount'];
                                                                            }else{
                                                                                $voucher_amount = $grand_total * ($purchased_products[0]['voucher_amount'] / 100);
                                                                                if($voucher_amount > $purchased_products[0]['max_provided_price']){
                                                                                    $voucher_amount = $purchased_products[0]['max_provided_price'];
                                                                                }
                                                                            }
                                                                        }
                                                                    @endphp
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.voucher') . ($purchased_products[0]['voucher_id'] == null ? '' : ' ('.$purchased_products[0]['voucher_code'].')') }}</strong></td>
                                                                        <td class="subtotal">{{ $voucher_amount == 0 ? "-" : ( $purchased_products[0]['voucher_fix_status'] == 'Y' ? ($currency->convertToCurrency($voucher_amount)) : ($purchased_products[0]['voucher_amount'].'%'.' (Maksimal '.$currency->convertToCurrency($voucher_amount).')') ) }}</td>
                                                                    </tr>
                                                                    @php
                                                                        $grand_total = $grand_total - $voucher_amount;
                                                                        $admin_fee = 0;
                                                                        if($purchased_products[0]['admin_fee'] != null){
                                                                            $admin_fee = $purchased_products[0]['admin_fee'];
                                                                        }
                                                                        if($purchased_products[0]['admin_fee_percentage'] != null){
                                                                            $admin_fee = $grand_total * ($purchased_products[0]['admin_fee_percentage'] / 100);
                                                                        }
                                                                        if($grand_total < 0){
                                                                            $grand_total = 0;
                                                                        }
                                                                    @endphp
                                                                    @php
                                                                        $shipment_fee = 0;
                                                                        if($purchased_products[0]['shipment_price'] != null){
                                                                            $shipment_fee = $purchased_products[0]['shipment_price'];
                                                                        }
                                                                    @endphp
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.shipment_fee') }}</strong></td>
                                                                        <td class="subtotal">{{ $shipment_fee == 0 ? "-" : $currency->convertToCurrency($shipment_fee) }}</td>
                                                                    </tr>
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.admin_fee') }}</strong></td>
                                                                        <td class="subtotal">{{ $admin_fee == 0 ? "-" : $currency->convertToCurrency($admin_fee) }}</td>
                                                                    </tr>
                                                                    <tr class="no-border">
                                                                        <td colspan="4" class="font-weight-bold text-right"><strong>{{ __('field.total') }}</strong></td>
                                                                        <td class="subtotal"><strong>{{ $currency->convertToCurrency($grand_total + $admin_fee + $shipment_fee) }}</strong></td>
                                                                    </tr> -->
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                        @endif
                                                    </div>
                                                                <!-- <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                        <span class="">{{ __('field.order_status') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                        <span class="">: {{ $purchased_products[0]['order_status_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.payment_status') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $purchased_products[0]['invoice_status_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.payment_date') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $purchased_products[0]['transfer_date'] == null ? "-" : $purchased_products[0]['transfer_date'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.payment_method') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $purchased_products[0]['payment_method_id'] == null ? "-" : $purchased_products[0]['payment_method_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div> -->
                                                            @if($purchased_products[0]['organization_type_id'] != 4)
                                                            <div class="yukmarket-transaction-summary">
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.shipment_method') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $purchased_products[0]['shipment_method_id'] == null ? "-" : $purchased_products[0]['shipment_method_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.product_quantity') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $total_quantity . " Pcs" }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                @if($purchased_products[0]['picker_name'] != null)
                                                                <div class="col-sm-12 no-padding margin-b5">
                                                                    <div class="col-xs-5 no-padding float-left">
                                                                    <span class="d-block">{{ __('field.picked_by') }}</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                    <span class="d-block">: {{ $purchased_products[0]['picker_name'] }}</span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                @endif
                                                                <div class="clearfix"></div>
                                                                
                                                            </div>
                                                            <div class="thanks">
                                                                <p>Terima kasih telah melakukan transaksi di toko kami. Semoga Anda senang dengan pelayanan kami.</p>
                                                            </div>
                                                            @endif
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="btn-print-wrap margin-b20">
                                                    <div class="col-md-12 no-padding ">
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <button class="btn btn-block btn-gray d-none" disabled>{{ __('page.back') }}</button>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <button class="btn btn-block btn-green"  onclick="printLabel('printableArea')">Print</button>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>

                                          
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
                window.print();
            document.body.innerHTML = originalContents;
        }

        function printLabel(divName){
            $.ajax({
                url: "{{ route('admin/packing/printLabel') }}",
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    order_id: "{{ Request::get('id') }}"
                },
                success: function(resultData){
                    console.log(resultData);
                    printDiv(divName)
                }
            });
        }

        window.onafterprint = function(){
            @if(isset($reprint))
            window.location.href = '{{ route("admin/shipment/history/form","?id=".Request::get("id")) }}';
            @else
            window.location.href = '{{ route("admin/picking") }}';
            @endif
        }
       
    </script>

@endsection