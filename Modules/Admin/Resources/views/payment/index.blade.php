
@extends('admin::layouts.master')

@section('title',__('menubar.transaction'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li class="active text-bold ">Pembayaran</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.order_history') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <!-- <a class="btn btn-green float-left mr-10" role="button" id="importTransactionButton"><i class="fa fa-download"></i> {{ __('page.import_transaction') }}</a> -->
                                                <a class="btn btn-green float-left mr-10" role="button" href="{{ route('admin/transaction/export','start_date='.(Request::get('transaction_start_date') == null ? date('Y-m-d') : Request::get('transaction_start_date')).'&end_date='.(Request::get('transaction_end_date') == null ? date('Y-m-d') : Request::get('transaction_end_date')).'&organization='.(Request::get('organization') != null ? (Request::get('organization')) : ($user['organization_type_id'] == 1 ? 'all_agent' : $user['organization_id'])).(Request::get('order_status') != null ? ('&order_status='.Request::get('order_status')) : '')) }}"><i class="fa fa-download"></i> {{ __('page.export_transaction') }}</a>
                                                <a class="btn btn-orange float-left" role="button" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i> Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form enctype="multipart/form-data" id="importTransactionForm" class="d-none" action="{{ route('admin/transaction/import') }}" method="POST">
                                                @csrf
                                                <input type="file" name="excel">
                                            </form>
                                            <table class="table display w-100 table-brown" id="payment_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.order_code') }}</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.total_price') }}</th>
                                                        <th>{{ __('field.purchased_date') }}</th>
                                                        <th>{{ __('field.order_status') }}</th>
                                                        <th>{{ __('field.payment_method') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot >
                                                    <tr role="row">
                                                        <td colspan="6" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <form id="transactionFilterForm">
                        @csrf
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                         <input type="text" class="form-control daterange-single" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif>
                                         <!-- <input type="date" class="form-control" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="order_status">{{ __('field.order_status') }}</label>
                                    <select name="order_status" id="order_status" class="form-control single-select" placeholder="{{ __('page.choose_order_status') }}">
                                        <option value="">{{ __('page.choose_order_status') }}</option>
                                        @for($b = 0; $b < count($order_status); $b++)
                                            <option value="{{ $order_status[$b]['order_status_id'] }}"  @if(Request::get('order_status') != null) @if($order_status[$b]['order_status_id'] == Request::get('order_status')) selected @endif @endif>{{ $order_status[$b]['order_status_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="organization">{{ __('field.agent_name') }}</label>
                                    <select name="organization" id="organization" class="form-control single-select" placeholder="{{ __('page.choose_agent') }}">
                                        <option value="">{{ __('page.choose_agent') }}</option>
                                        @for($b = 0; $b < count($organization); $b++)
                                            <option value="{{ $organization[$b]['organization_id'] }}"  @if(Request::get('organization') != null) @if($organization[$b]['organization_id'] == Request::get('organization')) selected @endif @endif>{{ $organization[$b]['organization_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding d-none">
                                <div class="form-group margin-b10">
                                    <label for="payment_method">{{ __('field.payment_method') }}</label>
                                    <select name="payment_method" id="payment_method" class="form-control single-select" placeholder="{{ __('page.choose_payment_method') }}">
                                        <option value="">{{ __('page.choose_payment_method') }}</option>
                                        @for($b = 0; $b < count($payment_method); $b++)
                                            <option value="{{ $payment_method[$b]['payment_method_id'] }}" @if(Request::get('payment_method') != null) @if($payment_method[$b]['payment_method_id'] == Request::get('payment_method')) selected @endif @endif>{{ $payment_method[$b]['payment_method_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                        </div>

                    </div>
                    <!-- Modal Footer -->
                    <div class="modal-footer border-top-grey" >
                        <div class="col-sm-12 no-padding btn-footer margin-t20">
                            <div class="col-sm-6 col-md-6 no-pad-left res-no-pad-sm margin-b10">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('order_status') == null && Request::get('payment_method') == null && Request::get('organization') == null) disabled @endif>{{ __('page.reset') }}</button>
                            </div>
                            <div class="col-sm-6 col-md-6 no-padding">
                                <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <!-- Modal Tracking -->
    <div class="modal fade" id="trackingModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content res-md-modal">
                <div class="modal-header modal-height-150">
                    <h5 class="modal-title">Tracking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="top: 30px;">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="clearfix"></div>
                    <div class="card align-items-center no-border">
                        <ul class="timeline-icon-tracking">
                            <li class="active">
                                <div class="icon-circle">
                                    <i class="ic-diproses-a"></i>
                                </div>
                                <p>Diproses</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-dikirim"></i>
                                </div>
                                <p>Dikirim</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-sampai"></i>
                                </div>
                                <p>Sampai</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                    <i class="ic-selesai"></i>
                                </div>
                                <p>Selesai</p>
                            </li>
                        </ul>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-body modal-dialog-scrollable">

                    <div class="card align-items-center no-border">
                        <ul class="timeline">
                            <li class="active">
                                <p>Diproses Penjual - Kamis 12-03-2020</p>
                                <p>Pesanan sedang dikemas oleh penjual</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($accessed_menu == 1)
    <script>
        $('#importTransactionButton').click(function(){
            $('input[name="excel"]').click();
        })

        $('input[name="excel"]').change(function(){
            $('form#importTransactionForm').submit();
        });

        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/transaction') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var paymentTable = $('#payment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            order: [[ 5, "desc" ]],
            ajax: {
                url:'{{ route("datatable/get_payment") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.start_date = "{{ Request::get('transaction_start_date') == null ? date('Y-m-d') : Request::get('transaction_start_date') }}";
                    d.end_date = "{{ Request::get('transaction_end_date') == null ? date('Y-m-d') : Request::get('transaction_end_date') }}";
                    d.order_status = "{{ Request::get('order_status') }}";
                    d.payment_method = "{{ Request::get('payment_method') }}";
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.organization = "{{ Request::get('organization') != null ? (Request::get('organization')) : ($user['organization_type_id'] == 1 ? 'all_agent' : $user['organization_id']) }}";
                    d.parent_organization = "{{ Request::get('organization') != null ? (Request::get('organization')) : ($user['organization_type_id'] == 1 ? 'all_agent' : $user['parent_organization_id']) }}";
                    // d.organization_type_id = "{{ $user['organization_type_id'] }}";
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'total_price',
                    name: 'total_price',
                    className: 'dt-body-right'
                },
                {   data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {data: 'order_status_name', name: 'order_status_name'},
                {data: 'payment_method_name', name: 'payment_method_name', visible: false},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        var idx = 0;
        setInterval( function () {
            idx += 1;
            console.log(idx);
            if(idx > 0 && idx % 180 == 0){
                paymentTable.ajax.reload(null, false);
            }
        }, 1000);

        paymentTable.on('click','a.trackingButton',function(){
            // console.log('eaaa');
            $.ajax({
                type: 'POST',
                url: '{{ route("admin/transaction/tracking") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    order_id : $(this).attr('data-order-id')
                },
                success: function(resultData){
                    // console.log(resultData);
                    var data = resultData['data'];
                    console.log(data);

                    var history = [];
                    var order_status_list = [];

                    if(data['history'].length > 0){
                        for (let b = 0; b < data['history'].length; b++) {
                            // const element = array[b];
                            var history_per_index  = "";
                            history_per_index += "<li "+(b == 0 ? 'class="active"' : '')+">"
                            history_per_index += "<p>"+data['history'][b]['order_status_name']+" - "+data['history'][b]['created_date']
                            history_per_index += "<p>"+data['history'][b]['order_status_description']+"</p>"
                            // history_per_index += "<p>15:16 WIB</p>"
                            history_per_index += "</li>";
                            history.push(history_per_index);
                        }
                    }
                    if(data['status'].length > 0){
                        for (let b = 0; b < data['status'].length; b++) {
                            // const element = array[b];
                            var order_status_per_index  = "";
                            order_status_per_index += '<li>'
                            order_status_per_index += '<div class="icon-circle">'
                            order_status_per_index += '<img src="'+data['status'][b]['order_status_logo']+'">'
                            order_status_per_index += '</div>'
                            order_status_per_index += '<p>'+data['status'][b]['order_status_name']+'</p>'
                            order_status_per_index += '</li>';
                            order_status_list.push(order_status_per_index);
                        }
                    }
                    $('ul.timeline').html(history);
                    $('ul.timeline-icon-tracking').html(order_status_list);
                    // if(data['role_type_id'] == 1){
                    //     // if(flag == 1){
                    //     // }
                    //     $('select[name="warehouse_name"]').prepend('<option value="all">{{ __("page.all_warehouse") }}</option>');
                    //     $('select[name="warehouse_name"]').val("all").trigger('change');
                    //     $('#warehouse_name_div').addClass('d-none');
                    // }else{
                    //     $('select[name="warehouse_name"] option[value="all"]').remove();
                    //     $('#warehouse_name_div').removeClass('d-none');
                    // }
                }
            });
        });
    </script>
    @endif
@endsection
