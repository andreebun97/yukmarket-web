@extends('admin::layouts.master')

@section('title',__('menubar.change_password'))

@section('content')
    <div class="password-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin')
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-user position-left"></i>Profile</li>
                                <li class="active text-bold ">{{ __('menubar.change_password') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.change_password') }}</h4>
                                        
                                            <div class="clearfix"></div>
                                        </div>
                                        

                                            <div class="col-md-12 no-padding">
                                                <div class="panel-body">
                                                    <div class="col-md-6 col-sm-12">
                                                        <ul class="nav nav-tabs text-center">
                                                            <li class="nav-item col-md-6 col-sm-6 col-xs-6 no-padding float-left">
                                                                <a class="nav-link text-center" href="{{ route('admin/profile') }}">{{ __('menubar.profile') }}</a>
                                                            </li>
                                                            <li class="nav-item col-md-6 col-sm-6 col-xs-6 no-padding float-left">
                                                                <a class="nav-link text-center active" href="{{ route('admin/password') }}">{{ __('menubar.change_password') }}</a>
                                                            </li>
                                                        </ul>
                                                        <form method="POST" action="{{ route('admin/password/change') }}" id="passwordForm">
                                                            @csrf
                                                            <input type="hidden" name="user_email" value="{{ Session::get('users')['email'] }}">
                                                            <div class="form-group">
                                                                <label for="user_old_password">{{ __('field.old_password') }} <span class="required-label">*</span></label>
                                                                <input type="password" class="form-control @error('user_old_password') is-invalid @enderror" name="user_old_password" autocomplete="off">
                                                                @error('user_old_password')
                                                                    <span class="invalid-feedback">{{ $errors->first('user_old_password') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="user_new_password">{{ __('field.new_password') }} <span class="required-label">*</span></label>
                                                                <input type="password" class="form-control @error('user_new_password') is-invalid @enderror" name="user_new_password" autocomplete="off">
                                                                @error('user_new_password')
                                                                    <span class="invalid-feedback">{{ $errors->first('user_new_password') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="user_confirm_password">{{ __('field.confirm_new_password') }} <span class="required-label">*</span></label>
                                                                <input type="password" class="form-control @error('user_confirm_password') is-invalid @enderror" name="user_confirm_password" autocomplete="off">
                                                                @error('user_confirm_password')
                                                                    <span class="invalid-feedback">{{ $errors->first('user_confirm_password') }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $.validator.addMethod("alpha_num", function(value, element) {
            if (this.optional(element)) {
                return true;
            } else if (!/[A-Z]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            } else if (!/[0-9]/.test(value)) {
                return false;
            }

            return true;
        }, "Letters & Numeric only please");

        $.validator.addMethod("isEmail", function(value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        }, "Email format please");

        $('input[name="user_old_password"], input[name="user_new_password"], input[name="user_confirm_password"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            user_old_password: {
                required: true
            },
            user_new_password: {
                required: true,
                minlength: 8,
                alpha_num: true
            },
            user_confirm_password: {
                required: true,
                equalTo: "input[name='user_new_password']"
            }
        };
        var validationMessages = {
            user_old_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.old_password')]) }}"
            },
            user_new_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.new_password')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.new_password'), 'min' => 8]) }}",
                alpha_num: "{{ __('validation.custom.new_password.alpha_num',['attribute' => __('validation.attributes.new_password')]) }}"
            },
            user_confirm_password: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.confirm_new_password')]) }}",
                equalTo: "{{ __('validation.same',['attribute' => __('validation.attributes.confirm_new_password'), 'other' => __('validation.attributes.new_password')]) }}"
            }
        };

        $('form#passwordForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/password") }}')

        exitPopup('{{ route("admin/password") }}');
    </script>
@endsection
