@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_shipment_method') : __('page.edit_shipment_method')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.shipment_method') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_shipment_method') : __('page.edit_shipment_method') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey ">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_shipment_method') : __('page.edit_shipment_method') }}</h4>
                                        
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page') 
                                            @else
                                            <form method="POST" action="{{ route('admin/shipment/form') }}" enctype="multipart/form-data" id="shipmentMethodForm">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        <div class="col-md-4 med-img padding-r-10">
                                                            <div id="showing_product_image">
                                                                <img src="{{ Request::get('id') == null ? asset('img/default_product.jpg') : ('/'.$shipment_by_id['shipment_logo']) }}" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                            </div>
                                                        </div>
                                                        @csrf
                                                        @if(Request::get('id') !== null)
                                                            <input type="hidden" name="shipment_id" value="{{ Request::get('id') }}">
                                                            <input type="hidden" name="shipment_logo_temp" value="{{ $shipment_by_id['shipment_logo'] }}">
                                                        @endif     
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="shipment_method_name">{{ __('field.shipment_method_name') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control @error('shipment_method_name') is-invalid @enderror" name="shipment_method_name" autocomplete="off" @if(Request::get('id') == null) value="{{ old('shipment_method_name') }}" @else value="{{ $shipment_by_id['shipment_method_name'] }}"  @endif>
                                                                @error('shipment_method_name')
                                                                    <span class="invalid-feedback">{{ $errors->first('shipment_method_name') }}</span>
                                                                @enderror
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label for="shipment_logo dp-block">{{ __('field.shipment_logo') }} @if(Request::get('id') == null)<span class="required-label">*</span>@endif</label>
                                                                <div class="clearfix"></div>
                                                                <div class="input-img-name">
                                                                    <span id="image_name">{{ __('page.no_data') }}</span>
                                                                    <button type="button" class="btn btn-green-upload no-margin" id="shipment_logo_button">{{ __('page.upload') }}</button> 
                                                                </div>
                                                                <input type="file" name="shipment_logo" class="form-control" style="display:none">
                                                                @error('shipment_logo')
                                                                    <span class="invalid-feedback">{{ $errors->first('shipment_logo') }}</span>
                                                                @enderror
                                                                <div class="info-yellow margin-t10">
                                                                    <i class="fa fa-info-circle"></i>
                                                                    {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                                </div>
                                                            </div>
                                                            <div class="form-group has-col">
                                                                <div class="col-xs-4 no-padding">
                                                                    <div class="form-group padding-r-10">
                                                                        <label for="dim_length">{{ __('field.dim_length').' (CM)' }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="dim_length" class="form-control @error('dim_length') is-invalid @enderror" @if(Request::get('id') == null) value="{{ old('dim_length') }}" @else value="{{ $shipment_by_id['dim_length'] }}"  @endif>
                                                                        @error('dim_length')
                                                                            <span class="invalid-feedback">{{ $errors->first('dim_length') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-4 no-padding">
                                                                    <div class="form-group padding-r-10">
                                                                        <label for="dim_width">{{ __('field.dim_width').' (CM)' }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="dim_width" class="form-control @error('dim_width') is-invalid @enderror" @if(Request::get('id') == null) value="{{ old('dim_width') }}" @else value="{{ $shipment_by_id['dim_width'] }}"  @endif>
                                                                        @error('dim_width')
                                                                            <span class="invalid-feedback">{{ $errors->first('dim_width') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-4 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="dim_height">{{ __('field.dim_height').' (CM)' }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="dim_height" class="form-control @error('dim_height') is-invalid @enderror" @if(Request::get('id') == null) value="{{ old('dim_height') }}" @else value="{{ $shipment_by_id['dim_height'] }}"  @endif>
                                                                        @error('dim_height')
                                                                            <span class="invalid-feedback">{{ $errors->first('dim_height') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="shipment_description">{{ __('field.shipment_description') }} <span class="required-label">*</span></label>
                                                                <textarea class="form-control @error('shipment_description') is-invalid @enderror" style="resize:none; height:202px; margin-bottom: 20px;" name="shipment_description">@if(Request::get('id') == null){{ old('shipment_description') }}@else{{ $shipment_by_id['shipment_method_desc'] }} @endif</textarea>
                                                                @error('shipment_description')
                                                                    <span class="invalid-feedback">{{ $errors->first('shipment_description') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="rates_apply">{{ __('field.rates_apply') }} <span class="required-label">*</span></label>
                                                                <select name="rates_apply" class="form-control single-select">
                                                                    <option value="">-- Pilih Tarif Berlaku --</option>
                                                                    <option value="1" {{ Request::get('id') == null ? '' : ($shipment_by_id['is_fixed'] == 1 ? 'selected' : '') }}>{{ __('field.multiple') }}</option>
                                                                    <option value="0" {{ Request::get('id') == null ? '' : ($shipment_by_id['is_fixed'] == 0 ? 'selected' : '') }}>{{ __('field.absolute') }}</option>
                                                                </select>
                                                                @error('rates_apply')
                                                                    <span class="invalid-feedback">{{ $errors->first('rates_apply') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="shipment_price">{{ __('field.shipment_price') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control @error('shipment_price') is-invalid @enderror" name="shipment_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('shipment_price') }}" @else value="{{ $shipment_by_id['price'] }}"  @endif>
                                                                @error('shipment_price')
                                                                    <span class="invalid-feedback">{{ $errors->first('shipment_price') }}</span>
                                                                @enderror
                                                            </div>
                                                            
                                                            <div id="adjustable" hidden="hidden">
                                                                <div class="row">
                                                                    <div class="form-group no-margin">
                                                                        <label class="col-md-12"><h5 class="no-margin color-green border-bottom-gray font-bold padding-bt10">{{ __('field.distance_lable') }}</h5></label>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="distance_maximum">{{ __('field.distance_maximum') }} <span class="required-label">*</span></label>
                                                                            <input type="number" class="form-control @error('distance_maximum') is-invalid @enderror" name="distance_maximum" @if(Request::get('id') == null) value="{{ old('distance_maximum') }}" @else value="{{ $shipment_by_id['max_km'] }}"  @endif>
                                                                            @error('distance_maximum')
                                                                                <span class="invalid-feedback">{{ $errors->first('distance_maximum') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="distance_price">{{ __('field.distance_price') }} <span class="required-label">*</span></label>
                                                                            <input type="text" class="form-control @error('distance_price') is-invalid @enderror" name="distance_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('distance_price') }}" @else value="{{ $shipment_by_id['max_price_km'] }}"  @endif>
                                                                            @error('distance_price')
                                                                                <span class="invalid-feedback">{{ $errors->first('distance_price') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="distance_unit">{{ __('field.distance_unit') }} <span class="required-label">*</span></label>
                                                                            <input type="text" class="form-control @error('distance_unit') is-invalid @enderror" name="distance_unit" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('distance_unit') }}" @else value="{{ $shipment_by_id['price_km'] }}"  @endif>
                                                                            @error('distance_unit')
                                                                                <span class="invalid-feedback">{{ $errors->first('distance_unit') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group no-margin">
                                                                        <label class="col-md-12"><h5 class="no-margin color-green border-bottom-gray font-bold padding-bt10">Berat</h5></label>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="weight_maximum">{{ __('field.weight_maximum') }} <span class="required-label">*</span></label>
                                                                            <input type="number" class="form-control @error('weight_maximum') is-invalid @enderror" name="weight_maximum" @if(Request::get('id') == null) value="{{ old('weight_maximum') }}" @else value="{{ $shipment_by_id['max_kg'] }}"  @endif>
                                                                            @error('weight_maximum')
                                                                                <span class="invalid-feedback">{{ $errors->first('weight_maximum') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="weight_price">{{ __('field.weight_price') }} <span class="required-label">*</span></label>
                                                                            <input type="text" class="form-control @error('weight_price') is-invalid @enderror" name="weight_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('weight_price') }}" @else value="{{ $shipment_by_id['max_price_kg'] }}"  @endif>
                                                                            @error('weight_price')
                                                                                <span class="invalid-feedback">{{ $errors->first('weight_price') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="weight_unit">{{ __('field.weight_unit') }} <span class="required-label">*</span></label>
                                                                            <input type="text" class="form-control @error('weight_unit') is-invalid @enderror" name="weight_unit" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('weight_unit') }}" @else value="{{ $shipment_by_id['price_kg'] }}"  @endif>
                                                                            @error('weight_unit')
                                                                                <span class="invalid-feedback">{{ $errors->first('weight_unit') }}</span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="checkbox" name="request_pickup_flag" value="1" @if(Request::get('id') != null) {{ $shipment_by_id['request_pickup_flag'] == 1 ? 'selected' : '' }}  @endif>
                                                                <span>Request Pickup</span>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="rates_apply">{{ __('field.rates_apply') }} <span class="required-label">*</span></label>
                                                                    <select name="rates_apply" class="form-control single-select">
                                                                        <option value="">-- Pilih Tarif Berlaku --</option>
                                                                        <option value="1" {{ Request::get('id') == null ? '' : ($shipment_by_id['is_fixed'] == 1 ? 'selected' : '') }}>{{ __('field.multiple') }}</option>
                                                                        <option value="0" {{ Request::get('id') == null ? '' : ($shipment_by_id['is_fixed'] == 0 ? 'selected' : '') }}>{{ __('field.absolute') }}</option>
                                                                    </select>
                                                                    @error('rates_apply')
                                                                        <span class="invalid-feedback">{{ $errors->first('rates_apply') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="shipment_price">{{ __('field.shipment_price') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('shipment_price') is-invalid @enderror" name="shipment_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('shipment_price') }}" @else value="{{ $shipment_by_id['price'] }}"  @endif>
                                                                    @error('shipment_price')
                                                                        <span class="invalid-feedback">{{ $errors->first('shipment_price') }}</span>
                                                                    @enderror
                                                                </div>
                                                                
                                                                <div id="adjustable" hidden="hidden">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label class="col-md-12"><h5 class="no-margin color-green border-bottom-gray font-bold padding-bt10">{{ __('field.distance_lable') }}</h5></label>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="distance_maximum">{{ __('field.distance_maximum') }} <span class="required-label">*</span></label>
                                                                                <input type="number" class="form-control @error('distance_maximum') is-invalid @enderror" name="distance_maximum" @if(Request::get('id') == null) value="{{ old('distance_maximum') }}" @else value="{{ $shipment_by_id['max_km'] }}"  @endif>
                                                                                @error('distance_maximum')
                                                                                    <span class="invalid-feedback">{{ $errors->first('distance_maximum') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="distance_price">{{ __('field.distance_price') }} <span class="required-label">*</span></label>
                                                                                <input type="text" class="form-control @error('distance_price') is-invalid @enderror" name="distance_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('distance_price') }}" @else value="{{ $shipment_by_id['max_price_km'] }}"  @endif>
                                                                                @error('distance_price')
                                                                                    <span class="invalid-feedback">{{ $errors->first('distance_price') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="form-group col-md-12">
                                                                                <label for="distance_unit">{{ __('field.distance_unit') }} <span class="required-label">*</span></label>
                                                                                <input type="text" class="form-control @error('distance_unit') is-invalid @enderror" name="distance_unit" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('distance_unit') }}" @else value="{{ $shipment_by_id['price_km'] }}"  @endif>
                                                                                @error('distance_unit')
                                                                                    <span class="invalid-feedback">{{ $errors->first('distance_unit') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label class="col-md-12"><h5 class="no-margin color-green border-bottom-gray font-bold padding-bt10">Berat</h5></label>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="weight_maximum">{{ __('field.weight_maximum') }} <span class="required-label">*</span></label>
                                                                                <input type="number" class="form-control @error('weight_maximum') is-invalid @enderror" name="weight_maximum" @if(Request::get('id') == null) value="{{ old('weight_maximum') }}" @else value="{{ $shipment_by_id['max_kg'] }}"  @endif>
                                                                                @error('weight_maximum')
                                                                                    <span class="invalid-feedback">{{ $errors->first('weight_maximum') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="weight_price">{{ __('field.weight_price') }} <span class="required-label">*</span></label>
                                                                                <input type="text" class="form-control @error('weight_price') is-invalid @enderror" name="weight_price" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('weight_price') }}" @else value="{{ $shipment_by_id['max_price_kg'] }}"  @endif>
                                                                                @error('weight_price')
                                                                                    <span class="invalid-feedback">{{ $errors->first('weight_price') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="form-group col-md-12">
                                                                                <label for="weight_unit">{{ __('field.weight_unit') }} <span class="required-label">*</span></label>
                                                                                <input type="text" class="form-control @error('weight_unit') is-invalid @enderror" name="weight_unit" onkeyup="currencyFormat(this)" @if(Request::get('id') == null) value="{{ old('weight_unit') }}" @else value="{{ $shipment_by_id['price_kg'] }}"  @endif>
                                                                                @error('weight_unit')
                                                                                    <span class="invalid-feedback">{{ $errors->first('weight_unit') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="checkbox" name="request_pickup_flag" value="1" @if(Request::get('id') != null) {{ $shipment_by_id['request_pickup_flag'] == 1 ? 'selected' : '' }}  @endif>
                                                                    <span>Request Pickup</span>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="card-footer ">
                                                        <div class="col-md-4 no-padding float-right">
                                                            <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                            </div>
                                                            <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </form>
                                            @endif
                                    </div>  
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="shipment_method_name"], textarea[name="shipment_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });


        // $.validator.addMethod("greaterThan",
        //     function (value, element, param) {
        //         var $otherElement = $(param);
        //         return parseInt(value, 10) > parseInt($otherElement.val(), 10);
        // });
        
        @if(Request::get('id') == null)
        var validationRules = {
            shipment_method_name: {
                required: true,
                // minlength: 3,
                alpha: true
            },
            shipment_price: {
                required: true
            },
            // product_weight: {
            //     required: true
            // },
            dim_length: {
                required: true
            },
            dim_width: {
                required: true
            },
            dim_height: {
                required: true
            },
            shipment_logo: {
                required: true,
                extension: "png|pneg|svg|jpg|jpeg",
                maxsize: 2000000
            },
            shipment_description: {
                required: true,
                // minlength: 10
            },
            rates_apply: {
                required: true
            },
            // min_distance: {
            //     required: true
            // }
            distance_maximum: {
                required: true
            },
            distance_price: {
                required: true
            },
            distance_unit: {
                required: true
            },
            weight_maximum: {
                required: true
            },
            weight_price: {
                required: true
            },
            weight_unit: {
                required: true
            },
        };
        var validationMessages = {
            shipment_method_name: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_method_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.shipment_method_name'), 'min' => 3]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.shipment_method_name')]) }}"
            },
            shipment_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_price')]) }}"
            },
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            dim_length: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_length')]) }}"
            },
            dim_width: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_width')]) }}"
            },
            dim_height: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_height')]) }}"
            },
            rates_apply: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.rates_apply')]) }}"
            },
            min_distance: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.min_distance')]) }}"
            },
            shipment_logo: {
                required: "{{ __('validation.custom.shipment_logo.required',['attribute' => __('validation.attributes.shipment_logo')]) }}",
                extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.shipment_logo'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
            },
            shipment_description: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.shipment_description'), 'min' => 10]) }}"
            },
            distance_maximum: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.distance')]) }}"
            },
            distance_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            distance_unit: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            weight_maximum: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.weight')]) }}"
            },
            weight_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            weight_unit: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            }
        };
        @else
            @if ($shipment_by_id['is_fixed'] == 1)
                var validationRules = {
                    shipment_method_name: {
                        required: true,
                        // minlength: 3,
                        alpha: true
                    },
                    shipment_price: {
                        required: true
                    },
                    // product_weight: {
                    //     required: true
                    // },
                    dim_length: {
                        required: true
                    },
                    dim_width: {
                        required: true
                    },
                    dim_height: {
                        required: true
                    },
                    shipment_description: {
                        required: true
                        // minlength: 10
                    },
                    rates_apply: {
                        required: true
                    },
                    // min_distance: {
                    //     required: true
                    // }
                    distance_maximum: {
                        required: true
                    },
                    distance_price: {
                        required: true
                    },
                    distance_unit: {
                        required: true
                    },
                    weight_maximum: {
                        required: true
                    },
                    weight_price: {
                        required: true
                    },
                    weight_unit: {
                        required: true
                    },
                };
            @else
                var validationRules = {
                    shipment_method_name: {
                        required: true,
                        // minlength: 3,
                        alpha: true
                    },
                    shipment_price: {
                        required: true
                    },
                    // product_weight: {
                    //     required: true
                    // },
                    dim_length: {
                        required: true
                    },
                    dim_width: {
                        required: true
                    },
                    dim_height: {
                        required: true
                    },
                    shipment_description: {
                        required: true
                        // minlength: 10
                    },
                    rates_apply: {
                        required: true
                    }
                };
            @endif
        var validationMessages = {
            shipment_method_name: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_method_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.shipment_method_name'), 'min' => 3]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.shipment_method_name')]) }}"
            },
            shipment_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_price')]) }}"
            },
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            dim_length: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_length')]) }}"
            },
            dim_width: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_width')]) }}"
            },
            dim_height: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.dim_height')]) }}"
            },
            rates_apply: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.rates_apply')]) }}"
            },
            min_distance: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.min_distance')]) }}"
            },
            shipment_description: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.shipment_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.shipment_description'), 'min' => 10]) }}"
            },
            distance_maximum: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.distance')]) }}"
            },
            distance_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            distance_unit: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            weight_maximum: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.weight')]) }}"
            },
            weight_price: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            },
            weight_unit: {
                required:  "{{ __('validation.required',['attribute' => __('validation.attributes.price')]) }}"
            }
        };
        @endif

        $('select[name="rates_apply"]').change(function(){
            console.log($(this).val());
            if($(this).val() == 1){
                $('input[name="shipment_price"]').val("");
                $('input[name="shipment_price"]').prop('disabled',true);
                $('#adjustable').attr('hidden', false);
                $('#adjustable :input').prop('disabled', false);
                $('#adjustable :input').prop('required', true);
            }else{
                $('input[name="shipment_price"]').prop('disabled',false);
                $('#adjustable').attr('hidden', true);
                $('#adjustable :input').prop('disabled', true);
                $('#adjustable :input').prop('required', false);
            }
        });

        $('form#shipmentMethodForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        // var shipmentPriceCurrency = currencyFormat(document.getElementsByName('shipment_price')[0]);
        // var shipmentMinPriceCurrency = currencyFormat(document.getElementsByName('shipment_min_price')[0]);
        // $('input[name="shipment_price"]').val(shipmentPriceCurrency);
        // $('input[name="shipment_min_price"]').val(shipmentMinPriceCurrency);
        var shipment_price = currencyFormat(document.getElementsByName('shipment_price')[0]);
        $('input[name="shipment_price"]').val(shipment_price);
        var distance_price = currencyFormat(document.getElementsByName('distance_price')[0]);
        $('input[name="distance_price"]').val(distance_price);
        var weight_price = currencyFormat(document.getElementsByName('weight_price')[0]);
        $('input[name="weight_price"]').val(weight_price);
        var distance_unit = currencyFormat(document.getElementsByName('distance_unit')[0]);
        $('input[name="distance_unit"]').val(distance_unit);
        var weight_unit = currencyFormat(document.getElementsByName('weight_unit')[0]);
        $('input[name="weight_unit"]').val(weight_unit);
        @if(Request::get('id') != null && $shipment_by_id['is_fixed'] == 1)
            //$('input[name="shipment_min_price"]').prop('readonly',false);
            $('input[name="shipment_price"]').val("");
            $('input[name="shipment_price"]').prop('disabled',true);
            $('#adjustable').attr('hidden', false);
            $('#adjustable :input').prop('disabled', false);
        @endif
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#shipment_logo_button').click(function(e){
            e.preventDefault();
            $('input[name="shipment_logo"]').click();
        });

        $('input[name="shipment_logo"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });

        clickCancelButton('{{ route("admin/shipment") }}')

        exitPopup('{{ route("admin/shipment") }}');
    </script>
@endsection
