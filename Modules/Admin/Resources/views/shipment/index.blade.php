
@extends('admin::layouts.master')

@section('title',__('menubar.shipment_method'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li class="active text-bold ">{{ __('menubar.shipment_method') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.shipment_method') }}</h4>
                                        @if($accessed_menu == 1)
                                        <div class="float-right">
                                        <a  class="btn btn-orange" href="{{ route('admin/shipment/create') }}"><i class="fa fa-plus"></i> {{ __('page.add_shipment_method') }}</a>
                                        @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-heading no-padding-bottom">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item col-md-6 col-lg-4 col-sm-6 col-xs-6 text-center no-padding">
                                                <a class="nav-link{{ (Request::get('flag') == null || Request::get('flag') == 'yukmarket') ? ' active' : '' }}" href="{{ route('admin/shipment','flag=yukmarket') }}">YukMarket</a>
                                            </li>
                                            <li class="nav-item col-md-6 col-lg-4 col-sm-6 col-xs-6 text-center no-padding{{ (Request::get('flag') == null || Request::get('flag') == 'yukmarket') ? '' : ' active' }}">
                                                <a class="nav-link {{ Request::get('flag') == null || Request::get('flag') == 'yukmarket' ? '' : ' active' }}" href="{{ route('admin/shipment','flag=ecommerce') }}">Ecommerce</a>
                                            </li>
                                        </ul>
                                    </div> 
                                    <div class="panel-body" >
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <table class="table display w-100 table-brown" id="shipment_list_table">
                                            <thead class="bg-darkgrey">
                                                <tr role="row">
                                                    <th>{{ __('field.action') }}</th>
                                                    <th>No.</th>
                                                    <th>{{ __('field.shipment_method_name') }}</th>
                                                    <th>{{ __('field.shipment_price') }}</th>
                                                    <th>{{ __('field.shipment_logo') }}</th>
                                                    <th>{{ __('field.shipment_description') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @if($accessed_menu == 1)
    <!-- Modal -->
    <div class="modal fade" id="shipmentLogoModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="shipmentLogoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="shipmentLogoModalLabel">{{ __('field.shipment_method_image') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var shipmentTable = $('#shipment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="shipment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_shipment") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    d.flag = "{{ Request::get('flag') }}"
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'shipment_method_name', name: 'shipment_method_name'},
                {
                    data: 'shipment_price',
                    name: 'shipment_price',
                    className: 'dt-body-right'
                },
                {
                    data: null,
                    searchable: false,
                    orderable: false,
                    render: function(data, type, meta, row){
                        return '<img data-toggle="modal" data-target="#shipmentLogoModal" class="shipment_logo" src="{{ asset('img/default_product.jpg') }}">';
                    }
                },
                {data: 'shipment_description', name: 'shipment_description'},
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        shipmentTable.on('click','img.shipment_logo',function(){
            console.log('lalala');
            var index = $('img.shipment_logo').index(this);
            var data = shipmentTable.data();
            var shipment_logo = data[index]['shipment_logo'];
            $('#shipmentLogoModal .modal-body').html('<img src="'+shipment_logo+'">');
            $('#shipmentLogoModal .modal-body img').attr('onerror','this.src="{{ asset("img/default_product.jpg") }}"');
            $('#shipmentLogoModal .modal-body img').on('error',function(){
                $(this).css({'width':'20rem','display':'block','margin':'auto'});
            });
        });
    </script>
    @endif
@endsection
