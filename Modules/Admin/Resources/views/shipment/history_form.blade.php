
@extends('admin::layouts.master')

@section('title',__('page.shipment_history_form'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li>{{ __('menubar.shipment_history') }}</li>
                                <li class="active text-bold ">{{ __('page.shipment_history_form') . ' ('.$order_master->order_code.')' }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.shipment_history_form') .' ('.$order_master->order_code.')' }}</h4>
                                        <div class="clearfix"></div>
                                    </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                        <div class="row no-padding">
                                            <div class="col-md-12 trs-detail">
                                                <div class="col-md-6 no-padding">
                                                    <div id="user_profile " class="info-detail">
                                                        <h2>{{ __('page.customer_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_name">{{ __('field.user_name') }}</label>
                                                            <span class="col-md-6 user_data_information">
                                                            {{ $order_master->contact_person == null ? ($order_master->customer_name == null ? $order_master->buyer_name : $order_master->customer_name) : $order_master->contact_person }}
                                                            </span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_email">{{ __('field.user_email') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->customer_email }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_phone_number">{{ __('field.user_phone_number') }}</label>
                                                            <span class="col-md-6 dd user_data_information">{{ $order_master->phone_number == null ? $phone_number->convert($order_master->customer_phone_number) : $phone_number->convert($order_master->phone_number) }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 no-padding">
                                                    <div class="info-detail">
                                                        <h2>{{ __('page.shipment_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6  " for="destination_address">{{ __('field.destination_address') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->destination_address == null ? $order_master->buyer_address :  ($order_master->address_detail . ', ' . ($order_master->kabupaten_kota_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kelurahan_desa_name . ', ' . $order_master->kode_pos . " (".($order_master->address_info == null ? "-" : $order_master->address_info).", ".($order_master->address_name == null ? "-" : $order_master->address_name).")")) }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <!-- <div class="form-group  no-padding">
                                                            <label class="col-md-6" for="contact_person">{{ __('field.contact_person') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->contact_person == null ? "-" : ($order_master->contact_person . ' (' . $phone_number->convert($order_master->phone_number) . ')') }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div> -->
                                                        <div class="form-group  no-padding">
                                                            <label class="col-md-6"   for="shipment_method">{{ __('field.shipment_method') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->shipment_method_name }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group  no-padding">
                                                            <label class="col-md-6"   for="shipment_price">{{ __('field.shipment_price') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ 'Rp. '.$currency->convertToCurrency($order_master->shipment_price) }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="margin-b20"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="clearfix"></div>
                                            <div class="margin-b20"></div>
                                            <div class="clearfix"></div>
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title" data-toggle="collapse" data-target="#collapseOne">Tampilkan Produk</h6>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse">
                                                        <div id="user_transaction" class="info-detail">
                                                            <!-- <h2>{{ __('page.transactions') }}</h2> -->
                                                            @php
                                                                $total_product = 0;
                                                                $total_bruto = 0;
                                                            @endphp
                                                            <div id="purchase_transaction">
                                                                <div id="purchase_transaction_detail">
                                                                    @if(count($transactions) == 0)  
                                                                    <span>{{ __('page.no_data') }}</span>
                                                                    @else
                                                                    <form method="POST" action="{{ route('admin/shipment/send') }}" id="shipmentHistoryForm" enctype="multipart/form-data">
                                                                        @csrf
                                                                        

                                                                        @for($b = 0; $b < count($transactions); $b++)
                                                                            @php
                                                                                $total_product += $transactions[$b]['quantity'];
                                                                                $total_bruto += ($transactions[$b]['bruto'] * $transactions[$b]['quantity']);
                                                                            @endphp
                                                                        @endfor
                                                                        <div class="col-xs-12 no-padding res-scroll-md">
                                                                            <table class="table display w-100 table-brown total-transaksi" id="product_list_table">
                                                                                <thead class="bg-darkgrey">
                                                                                    <tr role="row">
                                                                                        <th>Gambar</th>
                                                                                        <th>Kode</th>
                                                                                        <th style="min-width: 200px;" >Nama</th>
                                                                                        <th class="text-right">Jumlah</th>
                                                                                        <th class="text-right">Harga</th>
                                                                                        <th class="text-right">Total</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @php
                                                                                    $total_product = 0;
                                                                                    $total_weight = 0;
                                                                                    $subtotal = 0;
                                                                                    $total_dimension = 0;
                                                                                @endphp
                                                                                @for($b = 0; $b < count($transactions); $b++)
                                                                                    @php
                                                                                        $subtotal += $transactions[$b]['quantity'] *  $transactions[$b]['price'];
                                                                                        $total_product += $transactions[$b]['quantity'];
                                                                                        $dim_length = ($transactions[$b]['dim_length']);
                                                                                        $dim_width = ($transactions[$b]['dim_width']);
                                                                                        $dim_height = ($transactions[$b]['dim_height']);
                                                                                        $product_weight = $transactions[$b]['bruto'] * $transactions[$b]['quantity'];
                                                                                        $total_weight += $product_weight;
                                                                                        $total_dimension = ($dim_length * $dim_width * $dim_height * $transactions[$b]['quantity']);
                                                                                    @endphp
                                                                                    <tr {{ $b < count($transactions) - 1 ? '' : ''}}>
                                                                                        <td><img src="{{ asset($transactions[$b]['prod_image']) }}" class="img-50" alt="" onerror="this.src='{{ asset('img/default_product.jpg') }}'"></td>
                                                                                        <td>{{ $transactions[$b]['prod_code'] }}</td>
                                                                                        <td>{{ $transactions[$b]['prod_name'] . ($transactions[$b]['sku_status'] == 0 ? '' : (' '.($transactions[$b]['uom_value']+0).' '.$transactions[$b]['uom_name'])) }}</td>
                                                                                        <td class="text-right">{{ $transactions[$b]['quantity'] }}</td>
                                                                                        <td class="text-right">{{ $currency->convertToCurrency($transactions[$b]['price']) }}</td>
                                                                                        <td class="text-right">{{ $currency->convertToCurrency($transactions[$b]['quantity'] *  $transactions[$b]['price']) }}</td>
                                                                                    </tr>
                                                                                    @endfor
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="col-md-6 no-pad-left res-no-pad">
                                                <input type="hidden" name="order_id" value="{{ $order_master->order_id }}">
                                                <div class="form-group">
                                                    <label for="product_quantity">{{ __('field.bruto') }}</label>
                                                    <input class="form-control" value="{{ $total_bruto }}" readonly><span class="uom-inbox">kg</sup>
                                                </div>
                                            </div>
                                            @if($order_master->shipping_receipt_num != "" || $order_master->shipping_receipt_num != null)
                                                <input type="hidden" name="having_inputted_shipping_receipt_num" value="1">
                                            @endif
                                            <div class="col-md-6 no-pad-right res-no-pad">
                                                <div class="form-group">
                                                    <label for="shipping_receipt_num">{{ __('field.shipping_receipt_num') }}</label>
                                                    <input type="text" class="form-control" id="shipping_receipt_num" aria-describedby="shipping_receipt_num" name="shipping_receipt_num" autocomplete="off" @if($order_master->shipping_receipt_num != "" || $order_master->shipping_receipt_num != null) value="{{ $order_master->shipping_receipt_num }}" readonly @endif>
                                                    @error('shipping_receipt_num')
                                                        <span class="invalid-feedback">{{ $errors->first('shipping_receipt_num') }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        @endif
                                        </div>
                                        <div class="card-footer bg-transparent ">
                                            <div class="col-md-8 res-btn col-xs-12 no-padding float-right">
                                                <div class="col-xs-4 col-sm-4 no-padding">
                                                    <button type="submit" class="btn btn-green btn-block">{{ __('page.confirm') }}</button>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 no-padding">
                                                    <button type="button" class="btn btn-print btn-block" onclick="window.location.href = '{{ route("admin/shipment/label","?id=".Request::get('id')) }}'"><i class="fa fa-print"></i> {{ __('page.print_shipping_label') }}</button>
                                                </div>
                                                @if($order_master->request_pickup_flag == 1 && $order_master->agent_user_id == 28)
                                                <div class="col-xs-4 col-sm-4 no-padding">
                                                    <form action="{{ route('admin/picking/requestPickup') }}" id="requestPickupForm" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="order_id" value="{{ Request::get('id') }}">
                                                    </form>
                                                    <button type="button" class="btn btn-print btn-block" onclick="document.getElementById('requestPickupForm').submit();showLoader();"><i class="fa fa-print"></i> Request Pickup</button>
                                                </div>
                                                @endif
                                                <div class="col-xs-4 col-sm-4 no-pad-left">
                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ $order_master->shipping_receipt_num != "" || $order_master->shipping_receipt_num != null ? __('page.back') : __('page.cancel') }}</button>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var validationRules = {
            shipping_receipt_num: {
                required: true
            }
        };

        var validationMessages = {
            shipping_receipt_num: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.shipping_receipt_num')]) }}"
            }
        };
        $('form#shipmentHistoryForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();                
            }
        });

        clickCancelButton('{{ route("admin/shipment/history") }}')

        exitPopup('{{ route("admin/shipment/history") }}');
        $(function () {
            var active = true;
            $('#collapse-init').click(function () {
                if (active) {
                    active = false;
                    $('.panel-collapse').collapse('show');
                    $('.panel-title').attr('data-toggle', '');
                    $(this).text('Enable accordion behavior');
                } else {
                    active = true;
                    $('.panel-collapse').collapse('hide');
                    $('.panel-title').attr('data-toggle', 'collapse');
                    $(this).text('Disable accordion behavior');
                }
            });
            $('#accordion').on('show.bs.collapse', function () {
                if (active) $('#accordion .in').collapse('hide');
            });
        });
    </script>
@endsection
