
@extends('admin::layouts.master')

@section('title',__('page.print_shipping_list'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <li>{{ __('menubar.shipment_history') }}</li>
                                <li>{{ __('page.print_shipping_list') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">   
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('page.print_shipping_list') }}</h4>
                                            <div class="clearfix"></div>
                                        </div> 
                                        <div class="panel-body size-a4 no-padding" id="printableArea">
                                            <div class="struk">
                                                    <!-- <div class="yukmarket-logo">
                                                        <center><img src="{{ asset('img/img-logo-yukmarket-landscape.svg') }}" alt=""></center>
                                                    </div> -->
                                                @if($accessed_menu == 0)
                                                    @include('prohibited_page')
                                                @else
                                                    @for($b = 0; $b < count($shipment_list); $b++)
                                                        <div class="order-wrap pad-20" style="width: 50%; float: left;" >
                                                                <div class="yukmarket-logo">
                                                                    <center><img src="{{ asset('img/img-logo-yukmarket-landscape.svg') }}" alt=""></center>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <h6 class="text-bold">{{ $shipment_list[$b]['order_code'] }}</h6>
                                                                <p>{{ $shipment_list[$b]['customer_name'] }}</p>
                                                                <p>{{ $shipment_list[$b]['shipment_price'] }}</p>
                                                                <p>{{ $shipment_list[$b]['address_detail'] }}</p>
                                                        </div>    
                                                    @endfor
                                                @endif
                                                <div class="clearfix"></div>
                                                <div class="ttd-wrap pad-20" style="width: 50%; float: left;" >
                                                        <p class="text-bold">Pihak Ekspedisi</p>
                                                </div>
                                                <div class="ttd-wrap pad-20" style="width: 50%; float: left;" >
                                                        <p class="text-bold">Pihak YukMarket</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-wrap-print-a5 margin-b20">
                                            <div class="col-md-12 no-padding">  
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <button class="btn btn-gray btn-block" onclick="window.location.href = '{{ route("admin/shipment/history","id=".Request::get('id')) }}' ">{{ __('page.back') }}</button>
                                                    </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <button class="btn btn-green btn-block"  onclick="printDiv('printableArea')">Print</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')  
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
                window.print();
            document.body.innerHTML = originalContents;
        }
    </script>  
@endsection
