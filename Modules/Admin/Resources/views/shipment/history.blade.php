
@extends('admin::layouts.master')

@section('title',__('menubar.shipment_history'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <li class="active text-bold ">{{ __('menubar.shipment_history') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">   
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.shipment_history') }}</h4>
                                            @php
                                                $shipment_method_value = Request::get('shipment_method');
                                                $ecommerce = Request::get('ecommerce') == null ? null : Request::get('ecommerce'); 
                                            @endphp
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <form id="transactionFilterForm">
                                                    @csrf
                                                    <select name="shipment_method_list" id="shipment_method_list" class="form-control single-select" placeholder="{{ __('page.choose_shipment_method') }}">
                                                        <option value="">{{ __('page.choose_shipment_method') }}</option>
                                                        @for($b = 0; $b < count($shipment_method); $b++)
                                                            @php
                                                                $shipment_method_complete_value = $shipment_method[$b]['shipment_method_id'] . ( $shipment_method[$b]['organization_id'] == null ? null : '|' . $shipment_method[$b]['organization_id']);

                                                                $shipment_method_per_index = $shipment_method_complete_value;
                                                                $ecommerce_per_index = null;
                                                                if(Str::contains($shipment_method_complete_value,"|") == true){
                                                                    $shipment_method_per_index = explode("|",$shipment_method_complete_value)[0];
                                                                    $ecommerce_per_index = explode("|",$shipment_method_complete_value)[1];
                                                                }
                                                            @endphp
                                                            <option value="{{ $shipment_method_complete_value }}" @if(Request::get('shipment_method') != null) @if($shipment_method_per_index == $shipment_method_value && $ecommerce_per_index == $ecommerce) selected @endif @endif>{{ $shipment_method[$b]['shipment_method_name'] . ($shipment_method[$b]['organization_id'] == null ? '' : ' - '.$shipment_method[$b]['organization_name']) }}</option>
                                                        @endfor
                                                    </select>
                                                </form>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div> 
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                                <table class="table display w-100 table-brown" id="payment_list_table">
                                                    <thead class="bg-darkgrey">
                                                        <tr role="row">
                                                            <th>{{ __('field.action') }}</th>
                                                            <th>No.</th>
                                                            <th>{{ __('field.order_code') }}</th>
                                                            <th>{{ __('field.customer_name') }}</th>
                                                            <th>{{ __('field.purchased_date') }}</th>
                                                            <th>{{ __('field.shipment_method') }}</th>
                                                            <th>{{ __('field.shipment_price') }}</th>
                                                            <th>{{ __('field.address_detail') }}</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                </div>
                                                @if(Request::get('shipment_method') != null)

                                                    <div class="card-footer ">
                                                        <div class="col-md-3 no-padding float-right">
                                                            <button class="btn btn-orange btn-block" id="printListButton" onclick="window.location.href = '{{ route("admin/shipment/history/list","?id=".Request::get('shipment_method').(Request::get('ecommerce') == null ? "" : "&ecommerce=".Request::get('ecommerce'))) }}'">{{ __('page.print_shipping_list') }} <span id="shipmentMethodName"></span></button>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    
                                                @endif
                                            @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <!-- Modal Tracking -->
    <div class="modal fade" id="trackingModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md ">
            <div class="modal-content">
                <div class="modal-header modal-height-150">
                    <h5 class="modal-title">Tracking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="top: 30px;">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="clearfix"></div>
                    <div class="card align-items-center no-border">
                        <ul class="timeline-icon-tracking">
                            <li class="active">
                                <div class="icon-circle">
                                <i class="ic-diproses-a"></i>
                                </div>
                                <p>Diproses</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                <i class="ic-dikirim"></i>
                                </div>
                                <p>Dikirim</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                <i class="ic-sampai"></i>
                                </div>
                                <p>Sampai</p>
                            </li>
                            <li>
                                <div class="icon-circle">
                                <i class="ic-selesai"></i>
                                </div>
                                <p>Selesai</p>
                            </li>
                        </ul>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-body modal-dialog-scrollable">
          
                    <div class="card align-items-center no-border">
                            <ul class="timeline">
                            <li class="active">
                                <p>Diproses Penjual - Kamis 12-03-2020</p>
                                <p>Pesanan sedang dikemas oleh penjual</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Verifikasi - Kamis 12-03-2020</p>
                                <p>Pembayaran sedang diverifikasi sistem</p>
                                <p>15:16 WIB</p>
                            </li>
                            <li>
                                <p>Menunggu Pembayaran - Kamis 12-03-2020</p>
                                <p>Menunggu pembayaran dari pembeli</p>
                                <p>15:16 WIB</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('select[name="shipment_method_list"]').change(function(){
            // window.location.href = "{{ route('admin/transaction') }}";
            var shipment_method_list = $(this).val();
            
            shipment_method = shipment_method_list.split("|")[0];
            // var ecommerce = $(this).val();
            
            ecommerce = shipment_method_list.split("|")[1] == null ? null : shipment_method_list.split("|")[1];
            // console.log(ecommerce);
            $('<input type="hidden" name="shipment_method" value="'+shipment_method+'">').insertAfter("form#transactionFilterForm input[name='_token']");
            if(ecommerce != null){
                $('<input type="hidden" name="ecommerce" value="'+ecommerce+'">').insertAfter("form#transactionFilterForm input[name='shipment_method']");
            }
            console.log('value: '+$(this).val());
            console.log('shipment: '+shipment_method);
            console.log('ecommerce: '+ ecommerce);
            $('form#transactionFilterForm').submit();
            showLoader();
        });

        $('span#shipmentMethodName').html($("select[name='shipment_method_list'] option:selected").val() == '' ? '' : '('+$("select[name='shipment_method_list'] option:selected").text()+')');

        var shipmentTable = $('#payment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            order: [[ 4, "asc" ]],
            ajax: {
                url:'{{ route("datatable/get_shipped_order") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.shipment_method_id = "{{ Request::get('shipment_method') }}";
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.organization = "{{ $user['organization_type_id'] == 1 ? ($ecommerce == null ? 'all_agent' : $ecommerce) : $user['organization_id'] }}";
                    d.organization = "{{ $user['organization_type_id'] == 1 ? ($ecommerce == null ? 'all_agent' : $ecommerce) : $user['parent_organization_id'] }}";
                    // if(d.length == 0){
                    //     $('#printListButton').attr('disabled',true);
                    // }
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {
                    data: 'shipment_method_name',
                    name: 'shipment_method_name'
                },
                {data: 'shipment_price', name: 'shipment_price'},
                {data: 'address_detail', name: 'address_detail'},
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                if(iTotal == 0){
                    $('#printListButton').attr('disabled',true);
                }
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        var idx = 0;
        setInterval( function () {
            idx += 1;
            console.log(idx);
            if(idx > 0 && idx % 180 == 0){
                shipmentTable.ajax.reload(null, false);
            }
        }, 1000);
    </script>
    @endif
@endsection
