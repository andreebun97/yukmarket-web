@extends('admin::layouts.master')

@section('title',(Request::get('user') == null ? __('page.add_user') : __('page.edit_user')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'user_management'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-group position-left"></i>{{ __('menubar.user_management') }}</li>
                                <li>{{ __('menubar.user_list') }}</li>
                                <li class="active text-bold ">{{ Request::get('user') == null ? __('page.add_user') : __('page.edit_user') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat fit-height">
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold">{{ __('menubar.user_list') }}</h4>
                                    </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        
                                          
                                                <form  method="POST" action="{{ route('admin/form') }}" id="userForm">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="panel-body" >
                                                            <div class="col-md-6 no-padding">
                                                                @csrf
                                                                @if(Request::get('user') !== null)
                                                                <input type="hidden" name="user_id" value="{{ Request::get('user') }}">
                                                                @if($user['organization_type_id'] == null)
                                                                <div class="form-group">
                                                                    <label for="user_name">{{ __('field.agent_id') }}<span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_code') is-invalid @enderror" name="user_code" @if(Request::get('user') !== null) value="{{ $user_by_id['user_code'] }}" readonly @endif autocomplete="off">
                                                                    @error('user_code')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_code') }}</span>
                                                                    @enderror
                                                                </div>
                                                                @endif
                                                                @endif
                                                                <div class="form-group">
                                                                    <label for="user_name">{{ __('field.user_name') }}<span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" @if(Request::get('user') !== null) value="{{ $user_by_id['user_name'] }}" @else value="{{ old('user_name') }}" @endif autocomplete="off">
                                                                    @error('user_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="user_email">{{ __('field.user_email') }}<span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_email') is-invalid @enderror" name="user_email" @if(Request::get('user') !== null) value="{{ $user_by_id['user_email'] }}" @else value="{{ old('user_email') }}" @endif autocomplete="off">
                                                                    @error('user_email')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_email') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="user_phone_number">{{ __('field.user_phone_number') }}<span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_phone_number') is-invalid @enderror" name="user_phone_number" @if(Request::get('user') !== null) value="{{ $user_by_id['user_phone_number'] }}" @else value="{{ old('user_phone_number') }}" @endif autocomplete="off">
                                                                    @error('user_phone_number')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_phone_number') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 res-no-pad">
                                                                <div class="form-group">
                                                                    <label for="role_id">{{ __('field.role_name') }}<span class="required-label">*</span></label>
                                                                    <select name="role_id" class="form-control single-select" placeholder="{{ __('page.choose_role') }}">
                                                                        <option value="">{{ __('page.choose_role') }}</option>
                                                                        @for($b = 0; $b < count($role); $b++)
                                                                            <option value="{{ $role[$b]['role_id'] }}" @if(Request::get('user') !== null && $role[$b]['role_id'] == $user_by_id['role_id']) selected @endif>{{ $role[$b]['role_name'] }}</option>
                                                                        @endfor
                                                                    </select>
                                                                    @error('role_id')
                                                                        <span class="invalid-feedback">{{ $errors->first('role_id') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="organization_name">{{ __('field.organization_name') }}<span class="required-label">*</span></label>
                                                                    <select name="organization_name" class="form-control single-select" placeholder="{{ __('page.choose_organization') }}">
                                                                        <option value="">{{ __('page.choose_organization') }}</option>
                                                                        @for($b = 0; $b < count($organization); $b++)
                                                                            <option value="{{ $organization[$b]['organization_id'] }}" @if(Request::get('user') !== null && $organization[$b]['organization_id'] == $user_by_id['organization_id']) selected @endif>{{ $organization[$b]['organization_name'] }}</option>
                                                                        @endfor
                                                                    </select>
                                                                    @error('organization_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('organization_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group {{ Request::get('user') != null ? ($user_by_id['role_id'] == 1 ? 'd-none' : '') : 'd-none' }}" id="warehouse_name_div">
                                                                    <label for="warehouse_name">{{ __('field.warehouse_name') }}<span class="required-label">*</span></label>
                                                                    <select name="warehouse_name" class="form-control single-select">
                                                                        <option value="all">{{ __('page.all_warehouse') }}</option>
                                                                        @for($b = 0; $b < count($warehouse); $b++)
                                                                            <option value="{{ $warehouse[$b]['warehouse_id'] }}" @if(Request::get('user') !== null && $warehouse[$b]['warehouse_id'] == $user_by_id['warehouse_id']) selected @endif>{{ $warehouse[$b]['warehouse_name'] }}</option>
                                                                        @endfor
                                                                    </select>
                                                                    @error('warehouse_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('warehouse_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $.validator.addMethod("isEmail", function(value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        }, "Email format please");

        $('input[name="user_name"], input[name="user_email"], input[name="user_phone_number"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var flag = 0;
        $('select[name="role_id"]').change(function(){
            // if($(this).val() == 1){
            //     if(flag == 1){
            //         $('select[name="warehouse_name"]').prepend('<option value="all">{{ __("page.all_warehouse") }}</option>');
            //     }
            //     $('select[name="warehouse_name"]').val("all").trigger('change');
            //     $('#warehouse_name_div').addClass('d-none');
            // }else{
            //     flag = 1;
            //     $('select[name="warehouse_name"] option[value="all"]').remove();
            //     $('#warehouse_name_div').removeClass('d-none');
            // }
            $.ajax({
                type: 'POST',
                url: '{{ route("admin/role/get") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    role_id : $(this).val()
                },
                success: function(resultData){
                    // console.log(resultData);
                    var data = resultData['data'];

                    if(data['role_type_id'] == 1){
                        // if(flag == 1){
                        // }
                        $('select[name="warehouse_name"]').prepend('<option value="all">{{ __("page.all_warehouse") }}</option>');
                        $('select[name="warehouse_name"]').val("all").trigger('change');
                        $('#warehouse_name_div').addClass('d-none');
                    }else{
                        $('select[name="warehouse_name"] option[value="all"]').remove();
                        $('#warehouse_name_div').removeClass('d-none');
                    }
                }
            });
        });

        var validationRules = {
            user_name: {
                required: true,
                minlength: 4,
                alpha: true
            },
            user_email: {
                required: true,
                minlength: 10,
                isEmail: true
            },
            user_phone_number: {
                required: true,
                minlength: 10,
                maxlength: 15,
                digits: true
            },
            role_id: {
                required: true
            },
            organization_name: {
                required: true
            },
            warehouse_name: {
                required: true
            }
        };
        var validationMessages = {
            user_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_name')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_name'), 'min' => 4]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.user_name')]) }}"
            },
            user_email: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_email')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_email'), 'min' => 10]) }}",
                isEmail: "{{ __('validation.email',['attribute' => __('validation.attributes.user_email')]) }}"
            },
            user_phone_number: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_phone_number')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_phone_number'), 'min' => 10]) }}",
                maxlength: "{{ __('validation.max.string',['attribute' => __('validation.attributes.user_phone_number'), 'max' => 15]) }}",
                digits: "{{ __('validation.numeric',['attribute' => __('validation.attributes.user_phone_number')]) }}"
            },
            role_id: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.role_id')]) }}"
            },
            organization_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.organization_name')]) }}"
            },
            warehouse_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_name')]) }}"
            }
        };

        $('form#userForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/list") }}')

        exitPopup('{{ route("admin/list") }}');
    </script>
@endsection
