
@extends('admin::layouts.master')

@section('title',__('menubar.global_settings'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'global_settings'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-gears position-left"></i>Pengaturan Global</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.global_settings') }}</h4>
                                        <div class="clearfix"></div>
                                    </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <form method="POST" action="{{ route('admin/settings/form') }}" enctype="multipart/form-data" id="settingsForm">
                                            @csrf      
                                            <div class="col-md-12 no-padding">
                                                <div class="panel-body" >                                  
                                                    <div class="col-md-4 ">
                                                        <div class="form-group">
                                                            <label for="company_name">{{ __('field.company_name') }}</label>
                                                            <input type="text" class="form-control" name="company_name" value="{{ $global_settings['company_name'] }}" autocomplete="off">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company_address">{{ __('field.company_address') }}</label>
                                                            <textarea name="company_address" class="form-control" style="resize: none; height: 115px;">{{ $global_settings['company_address'] }}</textarea>
                                                        </div>
                                                    </div>                                       
                                                    <div class="col-md-4 ">
                                                        <div class="form-group">
                                                            <label for="company_province">{{ __('field.company_province') }}</label>
                                                            <select name="company_province" class="form-control single-select"></select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company_regency">{{ __('field.company_regency') }}</label>
                                                            <select name="company_regency" class="form-control single-select"></select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company_district">{{ __('field.company_district') }}</label>
                                                            <select name="company_district" class="form-control single-select"></select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company_village">{{ __('field.company_village') }}</label>
                                                            <select name="company_village" class="form-control single-select"></select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company_logo">{{ __('field.company_logo') }}</label>
                                                            <div class="input-img-name">
                                                                <span id="image_name">{{ __('page.no_data') }}</span>
                                                                <button class="btn btn-green-upload no-margin" id="company_logo_button">{{ __('page.upload') }}</button>
                                                            </div>
                                                            <input type="file" name="company_logo" style="display:none">
                                                        </div>
                                                    </div>                                       
                                                    <div class="col-md-4">
                                                        <div class="col-md-6 col-sm-6 no-pad-left res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="pricing_include_tax">{{ __('field.pricing_include_tax') }}</label>
                                                                <select name="pricing_include_tax" class="form-control">
                                                                    <option value="">{{ __('page.choose') }}</option>
                                                                    <option value="1" {{ $global_settings['pricing_include_tax'] == 1 ? 'selected' : '' }}>{{ __('notification.yes') }}</option>
                                                                    <option value="0" {{ $global_settings['pricing_include_tax'] == 0 ? 'selected' : '' }}>{{ __('notification.no') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 no-pad-right res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="tax_value">{{ __('field.tax_value') }}</label>
                                                                <input type="number" name="tax_value" class="form-control" value="{{ $global_settings['tax_value'] == null ? '' : '' }}" {{ $global_settings['pricing_include_tax'] == '1' ? 'readonly' : '' }}>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="implemented_price_type">{{ __('field.implemented_price_type') }}</label>
                                                            <select name="implemented_price_type" class="form-control single-select">
                                                                <option value="">Pilih Tipe Harga Regular</option>
                                                                @for($b = 0; $b < count($price_type); $b++)
                                                                    <option value="{{ $price_type[$b]['price_type_id'] }}" {{ $global_settings['implemented_price_type'] == $price_type[$b]['price_type_id'] ? 'selected' : '' }}>{{ $price_type[$b]['price_type_name'] }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tokopedia">{{ __('field.tokopedia') }}</label>
                                                            <select name="tokopedia" class="form-control single-select">
                                                                <option value="on" {{ $global_settings['tokopedia'] == 'on' ? 'selected' : '' }}>On</option>
                                                                <option value="off" {{ $global_settings['tokopedia'] == 'off' ? 'selected' : '' }}>Off</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="whatsapp_customer_service">{{ __('field.whatsapp_customer_service') }}</label>
                                                            <input type="text" class="form-control" name="whatsapp_customer_service" value="{{ $global_settings['whatsapp_customer_service'] == null ? '' : $global_settings['whatsapp_customer_service'] }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="schedule_config">{{ __('field.schedule_config') }}</label>
                                                            <select name="schedule_config" class="form-control single-select">
                                                                <option value="on" {{ $global_settings['schedule_config'] == 'on' ? 'selected' : '' }}>On</option>
                                                                <option value="off" {{ $global_settings['schedule_config'] == 'off' ? 'selected' : '' }}>Off</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="user_token_integrator">{{ __('field.user_token_integrator') }}</label>
                                                            <input type="text" class="form-control" name="user_token_integrator" value="{{ $global_settings['user_token_integrator'] == null ? '' : $global_settings['user_token_integrator'] }}">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 no-padding">
                                                <div class="card-footer bg-transparent ">
                                                    <div class="col-md-4 no-padding float-right">
                                                        <div class="col-xs-6 padding-r-10">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                        </div>
                                                        <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#company_logo_button').click(function(e){
            e.preventDefault();
            $('input[name="company_logo"]').click();
        });

        $('input[name="company_logo"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });

        var province_array = [];
        var regency_array = [];
        var district_array = [];
        var village_array = [];
        getProvince();
        $('select[name="company_province"]').html(province_array);
        @if($global_settings['company_province'] != null)
            $('select[name="company_province"]').val("{{ $global_settings['company_province'] }}");
            getRegency($('select[name="company_province"]').val());
        @endif
        @if($global_settings['company_regency'] != null)
            $('select[name="company_regency"]').val("{{ $global_settings['company_regency'] }}");
            getDistrict($('select[name="company_regency"]').val());
        @endif
        @if($global_settings['company_district'] != null)
            $('select[name="company_district"]').val("{{ $global_settings['company_district'] }}");
            getVillage($('select[name="company_district"]').val());
        @endif
        @if($global_settings['company_village'] != null)
            $('select[name="company_village"]').val("{{ $global_settings['company_village'] }}");
        @endif

        $('select[name="company_province"]').change(function(){
            company_province = $(this).val();
            getRegency(company_province);
            $('select[name="company_district"]').val("").trigger('change');
            $('select[name="company_village"]').val("").trigger('change');
            regency_array = [];
        });
        $('select[name="company_regency"]').change(function(){
            company_regency = $(this).val();
            getDistrict(company_regency);
            $('select[name="company_village"]').val("").trigger('change');
            district_array = [];
        });
        $('select[name="company_district"]').change(function(){
            company_district = $(this).val();
            getVillage(company_district);
            village_array = [];
        });

        function getProvince(){
            $.ajax({
                url: "/api/provinsi",
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var province = resultData['data'];
                    for (let b = 0; b < province.length; b++) {
                        province_array.push("<option value='"+province[b]['provinsi_id']+"'>"+province[b]['provinsi_name']+"</option>");                        
                    }
                }
            });
        }

        function getRegency(province_id, regency_div = null){
            $.ajax({
                url: "/api/kabkot/"+province_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var regency = resultData['data'];
                    console.log(resultData);
                    if(regency.length > 0){
                        for (let b = 0; b < regency.length; b++) {
                            regency_array.push("<option value='"+regency[b]['kabupaten_kota_id']+"'>"+regency[b]['kabupaten_kota_name']+"</option>");                        
                        }
                        $('select[name="company_regency"]').html(regency_array);
                    }
                }
            });
        }

        function getDistrict(regency_id){
            $.ajax({
                url: "/api/kecamatan/"+regency_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(regency_id);
                    // console.log(resultData);
                    var district = resultData['data'];
                    console.log(resultData);
                    if(district.length > 0){
                        for (let b = 0; b < district.length; b++) {
                            district_array.push("<option value='"+district[b]['kecamatan_id']+"'>"+district[b]['kecamatan_name']+"</option>");                        
                        }
                        $('select[name="company_district"]').html(district_array);
                    }
                }
            });
        }

        function getVillage(district_id){
            $.ajax({
                url: "/api/kelurahan/"+district_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(district_id);
                    // console.log(resultData);
                    var village = resultData['data'];
                    console.log(resultData);
                    if(village.length > 0){
                        for (let b = 0; b < village.length; b++) {
                            village_array.push("<option value='"+village[b]['kelurahan_desa_id']+"'>"+village[b]['kelurahan_desa_name']+"</option>");                        
                        }
                        $('select[name="company_village"]').html(village_array);
                    }
                }
            });
        }

        $('select[name="pricing_include_tax"]').change(function(){
            var pricing_include_tax = $(this).val();

            if($(this).val() == 1){
                $('input[name="tax_value"]').val("");
                $('input[name="tax_value"]').attr('readonly',true);
            }else{
                $('input[name="tax_value"]').val(0);
                $('input[name="tax_value"]').attr('readonly',false);
            }
        });

        $('#settingsForm').submit(function(){
            showLoader();
        });
    </script>
@endsection
