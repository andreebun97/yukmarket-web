
@extends('admin::layouts.master')

@section('title',__('menubar.complaint'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'complaint'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-comment"></i></li>
                                <li class="active text-bold ">{{ __('menubar.complaint') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray">
                                            <h4 class="panel-title text-bold title-color-green">{{ __('menubar.complaint') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <form id="complaintFilterForm">
                                                    <select name="issue_status" id="issue_status" class="form-control single-select" placeholder="{{ __('page.choose_issue_status') }}">
                                                        <!-- <option value="">{{ __('page.choose_issue_status') }}</option> -->
                                                        @for($b = 0; $b < count($complaint_status); $b++)
                                                            <option value="{{ $complaint_status[$b]['issue_status_id'] }}" {{ Request::get('issue_status') == $complaint_status[$b]['issue_status_id'] ? 'selected' : '' }}>{{ $complaint_status[$b]['issue_status_name'] }}</option>
                                                        @endfor
                                                    </select>
                                                </form>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        <div class="panel-body padding-lr-20" >
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="complaint_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>{{ __('field.ticketing_num') }}</th>
                                                        <th>{{ __('field.order_code') }}</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.issue_date') }}</th>
                                                        @if(Request::get('issue_status') == 2)
                                                        <th>{{ __('field.processed_by') }}</th>
                                                        
                                                        @endif
                                                        <th>{{ __('field.action') }}</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $('select[name="issue_status"]').change(function(){
            $('form#complaintFilterForm').submit();
        });
        // get_complaint_list
        $('#complaint_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                lengthMenu: '{{ __("page.showing") }} <select name="complaint_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_complaint_list") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.issue_status = "{{ Request::get('issue_status') }}";
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'ticketing_num', name: 'ticketing_num'},
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_detail', name: 'customer_detail'},
                {data: 'issue_date', name: 'issue_date'},
                @if(Request::get('issue_status') == 2)
                    {data: 'processed_by', name: 'processed_by'},                
                @endif
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                }
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
    </script>
@endsection
