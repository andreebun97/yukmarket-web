
@extends('admin::layouts.master')

@section('title',__('menubar.complaint'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'complaint'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-comment"></i></li>
                                <li>{{ __('menubar.complaint') }}</li>
                                <li class="active text-bold ">{{ __('page.complaint_form') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.complaint_form') }}</h4>
                                            <div class="clearfix"></div>                                        
                                        </div>                            
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <div class="row">
                                                <div class="col-md-12 complaint-page">
                                                    @php
                                                        $method = "";
                                                        $action = "";
                                                        if($complaint->issue_status_id == 1){
                                                            $method = "method=POST";
                                                            $action = "action=".route('admin/complaint/process')."";
                                                        }elseif($complaint->issue_status_id >= 2){
                                                            $method = "method=POST";
                                                            $action = "action=".route('admin/complaint/submit')."";
                                                        }
                                                    @endphp
                                                    <form {{ $method }} {{ $action }} id="complaintSubmissionForm">
                                                        @csrf
                                                        <input type="hidden" name="just_reply" value="1">
                                                        <input type="hidden" name="issue_id" value="{{ Request::get('id') }}">
                                                        <input type="hidden" name="customer_id" value="{{ $complaint->customer_id }}">
                                                        <input type="hidden" name="is_agent" value="{{ $complaint->is_agent }}">
                                                        <input type="hidden" name="issue_status_id" value="{{ $complaint->issue_status_id }}">
                                                        <input type="hidden" name="issue_solution_id" value="{{ $complaint->issue_solution_id }}">
                                                        <input type="hidden" name="approved_solution_id" value="{{ $complaint->approved_solution_id == null ? $complaint->issue_solution_id : $complaint->approved_solution_id }}">
                                                        <input type="hidden" name="order_id" value="{{ $complaint->order_id }}">
                                                        <div class="col-md-6 info-detail no-padding">
                                                            <h2 for="complain">Informasi Pelanggan</h2>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-5 col-sm-5" for="complain">{{ __('field.customer_name') }}</label>
                                                                <span class="user_data_information col-md-7 col-sm-7">{{ $complaint->customer_name }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-5 col-sm-5" for="complain">{{ __('field.order_code') }}</label>
                                                                <span class="user_data_information col-md-7 col-sm-7">{{ $complaint->order_code }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-5 col-sm-5" for="complain">{{ __('field.issue_notes') }}</label>
                                                                <span class="user_data_information col-md-7 col-sm-7">{{ $complaint->issue_notes }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group no-padding">
                                                                <label class="col-md-5 col-sm-5" for="complain">{{ __('field.issue_date') }}</label>
                                                                <span class="user_data_information col-md-7 col-sm-7">{{ date('d-m-Y H:i:s',strtotime($complaint->issue_date)) }}</span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-md-6 info-detail no-padding">
                                                            <div class="form-group no-padding">
                                                                <h2 for="complain">{{ __('field.issue_attachment') }}</h2>
                                                                <div class="complain-attachment" style="display:block">
                                                                    @for($b = 0; $b < count($complaint_attachment); $b++)
                                                                        <img style="width:15rem" src="{{ asset($complaint_attachment[$b]['attachment']) }}" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="margin-tb-10"></div>
                                                        <div class="separator no-margin"></div>
                                                        <div class="clearfix"></div>
                                                        <div class="margin-tb-10"></div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-12    info-detail no-padding">

                                                            <div class="table">
                                                                <div class="panel-heading no-padding res-tab">
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="nav-item col-md-3 col-sm-4 col-xs-6 text-center no-padding tab-one active">
                                                                            <a class="nav-link " href="#complained" data-toggle="tab" > <div for="complain">{{ __('field.complained_product') }}</div> </a>
                                                                        </li>
                                                                        <li class="nav-item col-md-3 col-sm-4 col-xs-6 text-center no-padding">
                                                                            <a class="nav-link tab-discuss" href="#discuss" data-toggle="tab"> Diskusi</a>
                                                                        </li>
                                                                     </ul>
                                                                </div>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane min-height-300 active" id="complained">
                                                                        <div class="form-group no-padding">
                                                                            <!-- <h2 for="complain">{{ __('field.complained_product') }}</h2> -->
                                                                            <div class="col-xs-12 no-padding res-scroll">
                                                                                <table class="table display w-100 table-brown text-align-center">
                                                                                    <thead class="bg-darkgrey ">
                                                                                        <tr>
                                                                                            <th>Gambar</th>
                                                                                            <th>Nama</th>
                                                                                            <th>Kategori</th>
                                                                                            <th>Catatan</th>
                                                                                            <th class="text-align-right">Harga</th>
                                                                                            <th class="text-align-right">Jumlah yang ingin dikembalikan</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        @php $total_unpaid_price = 0; @endphp
                                                                                        @php $total_price = 0; @endphp
                                                                                        @for($b = 0; $b < count($complained_products); $b++)
                                                                                            @php
                                                                                                $total_per_row = ($complained_products[$b]['price'] * $complained_products[$b]['quantity']) - $complained_products[$b]['promo_value'];

                                                                                                if($complained_products[$b]['issue_category_id'] == 1 && $complaint['is_delivery'] == 1){
                                                                                                    $total_unpaid_price += $total_per_row;
                                                                                                }
                                                                                                $total_price += $total_per_row;
                                                                                            @endphp
                                                                                            <tr>
                                                                                                <td><img src="{{ asset($complained_products[$b]['prod_image'] ) }}" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';" style="width:100px" alt=""></td>
                                                                                                <td>{{ $complained_products[$b]['prod_name'] . ($complained_products[$b]['sku_status'] == 0 ? '' : (' ' . ($complained_products[$b]['uom_value']+0) . ' ' . $complained_products[$b]['uom_name'])) }}</td>
                                                                                                <td>{{ $complained_products[$b]['issue_category_name'] }}</td>
                                                                                                <td>{{ $complained_products[$b]['issue_detail_notes'] }}</td>
                                                                                                <td class="text-align-right"><span class=" product_price">{{ $currency->convertToCurrency($complained_products[$b]['price']) }}</span></td>
                                                                                                <td class="text-align-right">
                                                                                                    <input type="hidden" name="dim_length[]" value="{{ $complained_products[$b]['dim_length'] }}">
                                                                                                    <input type="hidden" name="is_taxable_product[]" value="{{ $complained_products[$b]['is_taxable_product[]'] }}">
                                                                                                    <input type="hidden" name="tax_value[]" value="{{ $complained_products[$b]['tax_value'] }}">
                                                                                                    <input type="hidden" name="promo_value[]" value="{{ $complained_products[$b]['promo_value'] }}">
                                                                                                    <input type="hidden" name="product_id[]" value="{{ $complained_products[$b]['prod_id'] }}">
                                                                                                    <input type="hidden" name="warehouse_id[]" value="{{ $complained_products[$b]['warehouse_id'] }}">
                                                                                                    <input type="hidden" name="dim_width[]" value="{{ $complained_products[$b]['dim_width'] }}">
                                                                                                    <input type="hidden" name="dim_height[]" value="{{ $complained_products[$b]['dim_height'] }}">
                                                                                                    <input type="hidden" name="uom_id[]" value="{{ $complained_products[$b]['uom_id'] }}">
                                                                                                    <input type="hidden" name="uom_value[]" value="{{ ($complained_products[$b]['uom_value']+0) }}">
                                                                                                    <input type="hidden" name="sku_status[]" value="{{ $complained_products[$b]['sku_status'] }}">
                                                                                                    <input type="hidden" name="price[]" value="{{ $complained_products[$b]['price'] }}">
                                                                                                    <span class="product_quantity">{{ $complained_products[$b]['quantity'] }}</span>
                                                                                                    <select name="quantity[]" class="form-control d-none">
                                                                                                        @for($a = $complained_products[$b]['quantity']; $a > 0 ; $a--)
                                                                                                            <option value="{{ ($a) }}">{{ ($a) }}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </td>
                                                                                            </tr>
                                                                                        @endfor
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-6">
                                                                            
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="form-group no-padding">
                                                                                    <label class="margin-b10" for="applied_solution">{{ __('field.applied_solution') }}</label>
                                                                                    <textarea name="applied_solution" style="resize:none;height:40px" class="form-control"  readonly>{{ $complaint->issue_solution_name }}</textarea>
                                                                                </div>
                                                                                @if($complaint['issue_status_id'] >= 2 && $complaint['approved_solution_id'] != null && $complaint['approved_solution_id'] != $complaint['issue_solution_id'])
                                                                                <div class="form-group no-padding">
                                                                                    <label class="margin-b10" for="approved_solution">{{ __('field.approved_solution') }}</label>
                                                                                    <textarea name="approved_solution" style="resize:none;height:40px" class="form-control"  readonly>{{ $complaint->approved_solution_name }}</textarea>
                                                                                </div>
                                                                                @endif
                                                                                <input type="hidden" name="unpaid_amount" value="{{ $total_unpaid_price }}">
                                                                                <div class="form-group">
                                                                                    <label class="margin-b10" for="claimed_balance_amount">{{ __('field.claimed_balance_amount') }}</label>
                                                                                    <input type="text" class="form-control" name="claimed_balance_amount" value="{{ $complaint['claimed_balance_amount'] == null ? $currency->convertToCurrency($total_price) : $currency->convertToCurrency($complaint['claimed_balance_amount']) }}" readonly>
                                                                                </div>
                                                                                @if($complaint['issue_status_id'] >= 2)
                                                                                <div class="form-group {{ $complaint['approved_solution_id'] == 2 ? 'd-none' : '' }}">
                                                                                    <label class="margin-b10" for="return_amount">{{ __('field.return_amount') }}</label>
                                                                                    <input type="text" class="form-control" name="return_amount" value="{{ $complaint['return_amount'] == null ? $currency->convertToCurrency($total_price) : $currency->convertToCurrency($complaint['return_amount']) }}">
                                                                                </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="tab-pane min-height-300" id="discuss">
                                                                    <div class="col-md-12 container-chat">
                                                                            <!-- Layout Chat yg lama  -->
                                                                            <!-- @for($b = 0; $b < count($complaint_response); $b++)
                                                                            <div class="row complaintResponseDiv" {{ $b > 0 ? 'style=margin-top:10px' : '' }}>
                                                                                <div class="col-md-10">
                                                                                    @if($complaint_response[$b]['customer_id'] == null)
                                                                                    <div class="col-md-2 col-sm-6">
                                                                                        <img src="{{ asset('img/img-avatar.svg') }}" style="width:5rem;display:block;margin:auto" alt="">
                                                                                    </div>
                                                                                    <div class="col-md-10 col-sm-6">
                                                                                        <span style="display:block;font-weight:bold">{{ $complaint_response[$b]['user_name'] }}</span>
                                                                                        <span style="width:100%;display:block">{{ $complaint_response[$b]['response'] }} </span>
                                                                                        <small>{{ date('Y-m-d H:i:s',strtotime($complaint_response[$b]['created_date'])) }}</small>
                                                                                    </div>
                                                                                    @else
                                                                                    <div class="col-md-10 col-sm-6" style="text-align:right">
                                                                                        <span style="display:block;font-weight:bold">{{ $complaint_response[$b]['customer_name'] }}</span>
                                                                                        <span style="width:100%;display:block">{{ $complaint_response[$b]['response'] }}</span>
                                                                                        <small>{{ date('Y-m-d H:i:s',strtotime($complaint_response[$b]['created_date'])) }}</small>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-sm-6">
                                                                                        <img src="{{ asset($complaint_response[$b]['customer_image']) }}" onError="this.onerror=null;this.src='{{ asset('img/img-avatar.svg') }}';" style="width:5rem;display:block;margin:auto" alt="">
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            @endfor -->


                                                                            <div class="chat-list">
                                                                                <div class="row">
                                                                                    <div class="col-md-12 ">
                                                                                        <div class="complaint_response"></div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                                @for($b = 0; $b < count($complaint_response); $b++)
                                                                                <div class="row no-margin complaintResponseDiv" {{ $b > 0 ? 'style=margin-top:10px' : '' }}>
                                                                                        @if($complaint_response[$b]['customer_id'] == null)
                                                                                        <div class="col-md-12 col-sm-12 no-padding margin-b10">
                                                                                            <div class="col-md-1 col-sm-2 text-center margin-auto">
                                                                                                <div class="avatar-chat-wrap">
                                                                                                    <img src="{{ asset('img/img-avatar.svg') }}" alt="">
                                                                                                </div>
                                                                                                <span style="display:block;font-weight:bold">{{ $complaint_response[$b]['user_name'] }}</span>
                                                                                            </div>
                                                                                            <div class="col-md-11 col-sm-10">
                                                                                                <div class="buble-wrap-left">
                                                                                                    <div class="triangle-left">
                                                                                                        <div class="inner-triangle-left"></div>
                                                                                                    </div>
                                                                                                    <!-- <input type="text" class="form-control"> -->
                                                                                                    <span style="width:100%;display:block">{{ $complaint_response[$b]['response'] }} </span>
                                                                                                    <small class="date-chat">{{ date('Y-m-d H:i:s',strtotime($complaint_response[$b]['created_date'])) }}</small>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        @else
                                                                                        <div class="col-md-12 col-sm-12 no-padding margin-b10">
                                                                                            <div class="col-md-11 col-sm-10">
                                                                                                <div class="buble-wrap-right">
                                                                                                    <div class="triangle-right">
                                                                                                        <div class="inner-triangle-right"></div>
                                                                                                    </div>
                                                                                                    <span style="width:100%;display:block"> {{ $complaint_response[$b]['response'] }}</span>
                                                                                                    <small class="date-chat">{{ date('Y-m-d H:i:s',strtotime($complaint_response[$b]['created_date'])) }}</small>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-1 col-sm-2 text-center margin-auto">
                                                                                                <div class="avatar-chat-wrap">
                                                                                                    <img src="{{ asset($complaint_response[$b]['customer_image']) }}" onError="this.onerror=null;this.src='{{ asset('img/img-avatar.svg') }}';" alt="">
                                                                                                </div>
                                                                                                <span style="display:block;font-weight:bold"> {{ $complaint_response[$b]['customer_name'] }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        @endif
                                                                                        <!-- <div class="col-md-2">
                                                                                            <button class="btn btn-orange" style="width:100%">{{ __('page.submit') }}</button>
                                                                                        </div> -->
                                                                                </div>
                                                                                @endfor
                                                                            </div>
                                                                            <div class="row no-margin" {{ count($complaint_response) > 0 ? 'style=margin-top:10px' : '' }}>
                                                                                <div class="entry-chat">
                                                                                    <div class="col-sm-10">
                                                                                        <div class="col-md-1 col-sm-1 no-padding res-hidden">
                                                                                            <img src="{{ asset('img/img-avatar.svg') }}" style="height:3.5rem;display:block;margin:auto" alt="">
                                                                                        </div>
                                                                                        <div class="col-md-11 col-sm-11 no-padding">
                                                                                            <textarea name="complaint_response"  rows="1" id="complaint_response" class="form-control" style="width:100%;resize:none; margin-bottom: 10px;"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <button class="btn btn-primary" style="width:100%" id="sendChatButton">{{ __('page.send') }}</button>
                                                                                    </div>
                                                                                       
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                  
                                                        </div>
                                                        
                                                    </form>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="card-footer bg-transparent ">
                                            <div class="col-xs-12 no-padding float-right">
                                                <div class="col-sm-3 col-xs-12 no-pad-left res-no-pad-sm margin-b10">
                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button" >{{ $complaint['issue_status_id'] == 3 || $complaint['issue_status_id'] == 1 || $complaint['issue_status_id'] == 4 ? __('page.back') : __('page.cancel') }}</button>
                                                </div>
                                                <div class="col-sm-3 col-xs-12 res-no-pad-sm margin-b10">
                                                    <a id="anotherApprovalMethodButton" href="#discuss" data-toggle="tab"  class="btn btn-primary btn-block btn-discuss {{ $complaint['issue_status_id'] >= 2 ? 'pointer-none' : '' }}" style="white-space:normal" {{ $complaint['issue_status_id'] >= 2 ? 'disabled' : '' }}>{{ $complaint['issue_solution_id'] == 1 ? 'Cashback' : 'Ajukan solusi lain' }}</a>
                                                </div>
                                                <div class="col-sm-3 col-xs-12 res-no-pad-sm margin-b10">
                                                    <button type="button" class="btn btn-danger btn-block {{ $complaint['issue_status_id'] == 2 || $complaint['issue_status_id'] == 3 || $complaint['issue_status_id'] == 4 ? 'pointer-none' : ''}}" id="rejectComplaintButton" {{ $complaint['issue_status_id'] == 2 || $complaint['issue_status_id'] == 3 || $complaint['issue_status_id'] == 4 ? 'disabled' : ''}}>{{ __('page.reject') }}</button>
                                                </div>
                                                <div class="col-sm-3 col-xs-12 no-pad-right res-no-pad-sm margin-b10">
                                                    <button type="button" class="btn btn-orange btn-block {{ $complaint['issue_status_id'] == 3 || $complaint['issue_status_id'] == 4 ? 'pointer-none' : ($complaint['processed_by'] != null && $complaint['processed_by'] != $users['id'] ? 'pointer-none' : '') }}" id="{{ $complaint['issue_status_id'] == 1 ? 'complaintTriggerButton' : 'approveComplaintButton' }}" {{ $complaint['issue_status_id'] == 3 || $complaint['issue_status_id'] == 4 ? 'disabled' : ($complaint['processed_by'] != null && $complaint['processed_by'] != $users['id'] ? 'disabled' : '') }}>{{ ($complaint['issue_status_id'] == 1) ? __('page.process') : __('page.submit') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $('#complaintButton').click(function(){
            $('form#complaintSubmissionForm').submit();
            showLoader();
        });

        var reject = 0;
        var process = 0;
        var submit = 0;
        $('#rejectComplaintButton').click(function(){
            // <span>Maaf, keluhan Anda ditolak karena ...</span>
            if(reject == 0){
                console.log('b');
                $(this).attr('disabled','disabled');
                $('#complaintTriggerButton').attr('disabled','disabled');
                $('input[name="just_reply"]').remove();
                $('<input name="rejected" type="hidden" value="1">').insertAfter('input[name="_token"]');
                $('<center><span class="buble-wrap"><i class="fa fa-info"></i>Maaf, keluhan Anda ditolak karena ...</span></center>').insertBefore('.complaint_response');
                reject = 1;
            }
            // $('form#complaintSubmissionForm').submit();
        });

        $('#anotherApprovalMethodButton').click(function(){
            $('<input name="another_action" value="1" class="display-none">').insertAfter('input[name="_token"]');
            $(this).attr('disabled',true);
            $('#rejectComplaintButton').attr('disabled',true);
            $('#complaintTriggerButton').attr('disabled',true);
            var text_response = "Mohon maaf, kami tidak dapat menerima solusi yang diajukan, kami memberikan alternatif solusi keluhan yaitu ";
            $('input[name="just_reply"]').remove();
            @if($complaint['issue_solution_id'] == 1)
                text_response += "Cashback.";
                $('input[name="approved_solution_id"]').val(2);
            @else
                text_response += "Tukar Produk dengan Produk Lain.";
                $('input[name="approved_solution_id"]').val(1);
            @endif
            $('<input name="another_approval_response" type="hidden" value="'+text_response+'">').insertAfter('input[name="_token"]');
            $('<center><span class="buble-wrap"><i class="fa fa-info"></i>'+text_response+'</span></center>').insertBefore ('.complaint_response');
        });

        $('input[name="return_amount"]').keyup(function(){
            var claimed_amount = parseInt($('input[name="claimed_balance_amount"]').val().split('.').join(''));
            var return_amount = parseInt($(this).val().split('.').join(''));
            // console.log(return_amount);
            // console.log(claimed_amount);
            // console.log(claimed_amount < return_amount);
            if(claimed_amount < return_amount){
                $(this).val($('input[name="claimed_balance_amount"]').val());
            }
            // console.log(claimed_amount);
        });

        $('#complaintTriggerButton').click(function(){
            if(process == 0){
                // console.log('b');
                $(this).attr('disabled','disabled');
                $('input[name="just_reply"]').remove();
                $('#rejectComplaintButton').attr('disabled','disabled');
                $('#anotherApprovalMethodButton').attr('disabled',true);
                $('<input name="process" type="hidden" value="1">').insertAfter('input[name="_token"]');
                $('<center><span class="buble-wrap"><i class="fa fa-info"></i>Keluhan Anda diterima oleh pihak YukMarket ...</span></center>').insertBefore('.complaint_response');
                process = 1;
            }
        });

        $('#approveComplaintButton').click(function(){
            if(submit == 0){
                $(this).attr('disabled','disabled');
                $('input[name="just_reply"]').remove();
                $('<input name="approved" type="hidden" value="1">').insertAfter('input[name="_token"]');
                $('#anotherApprovalMethodButton').attr('disabled',true);
                @if($complaint['approved_solution_id'] == 1)
                    $('<center><span class="buble-wrap"><i class="fa fa-info"></i>Barang Anda akan dikirim dengan barang yang baru.</span></center>').insertBefore ('.complaint_response');
                @else
                    $('<center><span class="buble-wrap"><i class="fa fa-info"></i>Anda akan menerima cashback yang akan dikirim ke akunnya sebesar '+$('input[name="claimed_balance_amount"]').val()+' ...</span></center>').insertBefore ('.complaint_response');
                @endif
                submit = 1;
            }
        });

        var validationRules = {
            complaint_response: {
                required: true,
                minlength: 10
            },
            claimed_balance_amount: {
                required: true
            }
        };

        var validationMessages = {
            complaint_response: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.complaint_response')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.complaint_response'), 'min' => 10]) }}"
            },
            claimed_balance_amount: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.claimed_balance_amount')]) }}"
            }
        };

        $('form#complaintSubmissionForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        var second_flag = 0;

        setInterval(function(){
            $.ajax({
                url: "{{ route('admin/complaint/getUnreadResponse') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    issue_id: "{{ Request::get('id') }}"
                },
                success: function(resultData){
                    second_flag += 1;
                    console.log("second: "+second_flag);
                    console.log(resultData);
                    var data = resultData['data'];
                    var html = "";
                    if(data.length > 0){
                        for (let b = 0; b < data.length; b++) {
                            if(data[b]['customer_id'] != null){
                                html += '<div class="row no-margin complaintResponseDiv" style="margin-top:10px">'
                                html += '<div class="col-md-12 col-sm-12 no-padding margin-b10">';
                                html += '<div class="col-md-11 col-sm-10">'
                                html += '<div class="buble-wrap-right">'
                                html += '<div class="triangle-right">';
                                html += '<div class="inner-triangle-right"></div>';
                                html += '</div>';
                                html += '<span style="width:100%;display:block">'+data[b]['response']+'</span>'
                                html += '<small>'+data[b]['created_date']+'</small>'
                                html += '</div></div>'
                                html += '<div class="col-md-1 col-sm-2 text-center margin-auto">'
                                html += '<div class="avatar-chat-wrap">';
                                html += '<img class="customerProfilePict" style="width:5rem;display:block;margin:auto" alt="">'
                                html += '</div>';
                                html += '<span style="display:block;font-weight:bold">'+data[b]['customer_name']+'</span>'
                                html += '</div>'
                                html += '</div>';
                                html += '</div>';
                            }else{
                                html += '<div class="row no-margin" style="margin-top:10px">'
                                html += '<div class="col-md-12 col-sm-12 no-padding margin-b10">';
                                html += '<div class="col-md-1 col-sm-2 text-center margin-auto">'
                                html += '<div class="avatar-chat-wrap">';
                                html += '<img class="customerProfilePict" style="width:5rem;display:block;margin:auto" alt="">'
                                html += '</div>';
                                html += '<span style="display:block;font-weight:bold">'+data[b]['user_name']+'</span>'
                                html += '</div>';
                                html += '<div class="col-md-11 col-sm-10">'
                                html += '<div class="buble-wrap-left">'
                                html += '<div class="triangle-left">';
                                html += '<div class="inner-triangle-left"></div>';
                                html += '</div>';
                                html += '<span style="width:100%;display:block">'+data[b]['response']+'</span>'
                                html += '<small class="date-chat">'+data[b]['created_date']+'</small>'
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            }
                        }
                        $(html).insertAfter('.complaintResponseDiv:last-child');
                        $('img.customerProfilePict:last-child').attr('onError',"this.onerror=null;this.src={{ asset('img/img-avatar.svg') }}");
                    }
                }
            });
        },1000);

        clickCancelButton('{{ route("admin/complaint") }}')

        exitPopup('{{ route("admin/complaint") }}');
        $(".btn-discuss").click(function(){
            $(".tab-discuss").addClass("active");
            $(".tab-one").removeClass("active");
        });
        $(".tab-one").click(function(){
            $(".tab-discuss").removeClass("active");
        });

    </script>
@endsection
