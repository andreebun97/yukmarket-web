@extends('admin::layouts.master')

@section('title',__('menubar.dashboard'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container"> 
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'dashboard'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><a href="index.html"><i class="icon-home2 position-left"></i></a></li>
                                <li class="active"><a href="c3_lines_areas.html">{{ __('menubar.dashboard') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        @if($accessed_menu == 0)
                            @include('prohibited_page')
                        @else
                        <div class="col-md-12 no-padding">
                            @if(array_search(45, array_column($user['dashboard'], 'menu_id')) !== false)
                            <div  class="col-md-6 data-wrapper-left">
                                <h5 class="color-primary text-bold" >Pesanan</h5>
                                <div class="col-md-12 d-flex no-padding dashboard-info-mobile">
                                    <div class="col-sm-6 col-xs-6 res-fourcol dashboard-cursor">
                                        <div class="panel panel-flat">
                                            <div class="panel-body no-padding">
                                                <div class="card-custom res-dashcard">
                                                    <div class="col-md-12">
                                                        <div class="info-wrap">
                                                            <div class="col-md-12 no-padding">
                                                                <div class="col-sm-4 padright-only">
                                                                    <h1 class="font-bold color-green text-align-center" >{{ $count_new_order }} </h1>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <h3 class="color-black"> {{ __('page.new_order') }}</h3>
                                                                    <div class="clearfix"></div>
                                                                    <span onclick="window.location.href = '{{ route("admin/picking","picker=0") }}'"> Detail</span>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-6 res-fourcol dashboard-cursor">
                                        <div class="panel panel-flat">
                                            <div class="panel-body no-padding">
                                                <div class="card-custom res-dashcard">
                                                    <div class="col-md-12">
                                                        <div class="info-wrap">
                                                            <div class="col-md-12 no-padding">
                                                                <div class="col-sm-4 padright-only">
                                                                    <h1 class="font-bold color-green text-align-center" >{{ $count_picking }} </h1>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <h3 class="color-black"> {{ __('page.ready_to_pick') }}</h3>
                                                                    <div class="clearfix"></div>
                                                                    <span onclick="window.location.href = '{{ route("admin/picking","picker=1") }}'"> Detail</span>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="clearfix"></div>
                                <div class="col-md-12 d-flex no-padding dashboard-info-mobile">
                                    <div class="col-sm-6 col-xs-6 res-fourcol dashboard-cursor">
                                        <div class="panel panel-flat">
                                            <div class="panel-body no-padding">
                                                <div class="card-custom res-dashcard">
                                                    <div class="col-md-12">
                                                        <div class="info-wrap">
                                                            <div class="col-md-12 no-padding">
                                                                <div class="col-sm-4 padright-only">
                                                                    <h1 class="font-bold color-green text-align-center" >{{ $count_packing }} </h1>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <h3 class=" color-black"> {{ __('page.ready_to_pack') }}</h3>
                                                                    <div class="clearfix"></div>
                                                                    <span onclick="window.location.href = '{{ route("admin/packing") }}'"> Detail</span>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-6 res-fourcol dashboard-cursor">
                                        <div class="panel panel-flat">
                                            <div class="panel-body no-padding">
                                                <div class="card-custom res-dashcard">
                                                    <div class="col-md-12">
                                                        <div class="info-wrap">
                                                            <div class="col-md-12 no-padding">
                                                                <div class="col-sm-4 padright-only">
                                                                    <h1 class="font-bold color-green text-align-center" >{{ $count_shipping }} </h1>
                                                                </div>
                                                                <div class="col-sm-8 no-padding">
                                                                    <h3 class="color-black">{{ __('page.ready_to_deliver') }}</h3>
                                                                    <div class="clearfix"></div>
                                                                    <span onclick="window.location.href = '{{ route("admin/shipment/history") }}'"> Detail</span>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            @endif
                            @if(array_search(46, array_column($user['dashboard'], 'menu_id')) !== false)
                            <div  class="col-md-6 data-wrapper-right dashboard-info-mobile">
                                <h5 class="color-secondary text-bold">{{ __('menubar.complaint') }}</h5>
                                @for($b=0; $b < count($pengaduan); $b++)
                            
                                <div class="col-sm-6 col-xs-6 res-fourcol dashboard-cursor">
                                    <div class="panel panel-flat">
                                        <div class="panel-body no-padding">
                                            <div class="card-custom res-dashcard">
                                                <div class="col-md-12">
                                                    <div class="info-wrap">
                                                        <div class="col-md-12 no-padding">
                                                            <div class="col-sm-4 padright-only">
                                                                <h1 class="font-bold color-secondary text-align-center" >
                                                                {{ $pengaduan[$b]['jumlah'] }}
                                                                </h1>
                                                            </div>
                                                            <div class="col-sm-8 no-padding">
                                                                <h3 class="color-black"> {{ $pengaduan[$b]['issue_status_name'] }}</h3>
                                                                <div class="clearfix"></div>
                                                                <span onclick="window.location.href = '{{ route("admin/complaint","issue_status=".$pengaduan[$b]['issue_status_id']) }}'"> Info</span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                @endfor
                                <div class="clearfix"></div>
                            </div>
                            @endif
                        </div>
                        @if(array_search(47, array_column($user['dashboard'], 'menu_id')) !== false)
                        <div  class="col-md-12 data-wrapper no-padding">
                            <div class="col-md-4 dashboard-cursor no-pad-left">
                                <div class="panel panel-flat">
                                    <div class="panel-body no-padding">
                                        <div class="card-custom res-dashcard">
                                            <div class="col-md-2">
                                                <div class="arrow-wrap">
                                                    <img src="{{ $selling['today_price'] < $selling['yesterday_price'] ? asset('img/arrow-down.png') : ($selling['today_price'] == $selling['yesterday_price'] ? asset('img/icon-equalto.png') : asset('img/arrow-up.png')) }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="info-wrap"  onclick="window.location.href = '{{ route("admin/dashboard","time=today") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                            <h4 class="font-600{{ Request::get('time') == 'today' || Request::get('time') == null ? ' active-statistics' : '' }}">{{ __('time.today') }}</h4>
                                                            @php
                                                                $today_date = $full_date['today_date'];
                                                                $date = explode('-',$today_date)[2];
                                                                $month = explode('-',$today_date)[1];
                                                                $year = explode('-',$today_date)[0]
                                                            @endphp
                                                            <span class="dashboard-date-detail">{{ $date . '/' . $month . '/' . $year }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-sm-6 padright-only">
                                                            <span> Jumlah transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">{{ $selling['today_qty'] }}</h3>
                                                        </div>
                                                        <div class="col-sm-6 no-padding">
                                                            <span> Total Transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['today_price']) }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="border-dotted"></div>
                                                <div class="clearfix"></div>
                                                <div class="info-wrap"  onclick="window.location.href = '{{ route("admin/dashboard","time=yesterday") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                            <h4 class="font-600{{ Request::get('time') == 'yesterday' ? ' active-statistics' : '' }}">{{ __('time.yesterday') }}</h4>
                                                            @php
                                                                $yesterday_date = $full_date['yesterday_date'];
                                                                $date = explode('-',$yesterday_date)[2];
                                                                $month = explode('-',$yesterday_date)[1];
                                                                $year = explode('-',$yesterday_date)[0]
                                                            @endphp
                                                            <span class="dashboard-date-detail">{{ $date . '/' . $month . '/' . $year }}</span>
                                                        </div>
                                                        <div class="col-sm-6 padright-only">
                                                            <span> Jumlah transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">{{ $selling['yesterday_qty'] }}</h3>
                                                        </div>
                                                        <div class="col-sm-6 no-padding">
                                                            <span> Total Transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['yesterday_price']) }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 dashboard-cursor pad-on">
                                <div class="panel panel-flat">
                                    <div class="panel-body no-padding">
                                        <div class="card-custom res-dashcard">
                                            <div class="col-md-2">
                                                <div class="arrow-wrap">
                                                    <img src="{{ $selling['this_week_price'] < $selling['last_week_price'] ? asset('img/arrow-down.png') : ($selling['this_week_price'] == $selling['last_week_price'] ? asset('img/icon-equalto.png') : asset('img/arrow-up.png')) }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="info-wrap" onclick="window.location.href = '{{ route("admin/dashboard","time=this_week") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                            <h4 class="font-600{{ Request::get('time') == 'this_week' ? ' active-statistics' : '' }}">{{ __('time.this_week') }}</h4>
                                                            @php
                                                                $current_week_start_date_range = $full_date['current_week_start_date_range'];
                                                                $current_week_start_date = explode('-',$current_week_start_date_range)[2];
                                                                $current_week_start_month = explode('-',$current_week_start_date_range)[1];
                                                                $current_week_start_year = explode('-',$current_week_start_date_range)[0];

                                                                $today_date = $full_date['today_date'];
                                                                $current_week_end_date = explode('-',$today_date)[2];
                                                                $current_week_end_month = explode('-',$today_date)[1];
                                                                $current_week_end_year = explode('-',$today_date)[0];

                                                                $range = ($current_week_start_date_range == $today_date ? ($current_week_start_date . '/' . $current_week_start_month . '/' . $current_week_start_year . ' - sekarang') : ($current_week_start_date . '/' . $current_week_start_month . '/' . $current_week_start_year . ' - ' . $current_week_end_date . '/' . $current_week_end_month . '/' . $current_week_end_year))
                                                            @endphp
                                                            <span class="dashboard-date-detail">{{ $range }}</span>
                                                        </div>
                                                        <div class="col-sm-6 padright-only">
                                                            <span> Jumlah transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">{{ $selling['this_week_qty'] }}</h3>
                                                        </div>
                                                        <div class="col-sm-6 no-padding">
                                                            <span> Total Transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['this_week_price']) }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="border-dotted"></div>
                                                <div class="clearfix"></div>
                                                <div class="info-wrap" onclick="window.location.href = '{{ route("admin/dashboard","time=last_week") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                            <h4 class="font-600{{ Request::get('time') == 'last_week' ? ' active-statistics' : '' }}">{{ __('time.last_week') }}</h4>
                                                            @php
                                                                $last_week_start_date_range = $full_date['last_week_start_date_range'];
                                                                $last_week_start_date = explode('-',$last_week_start_date_range)[2];
                                                                $last_week_start_month = explode('-',$last_week_start_date_range)[1];
                                                                 $last_week_start_year = explode('-',$last_week_start_date_range)[0];

                                                                $last_week_end_date_range = $full_date['last_week_end_date_range'];
                                                                $last_week_end_date = explode('-',$last_week_end_date_range)[2];
                                                                $last_week_end_month = explode('-',$last_week_end_date_range)[1];
                                                                $last_week_end_year = explode('-',$last_week_end_date_range)[0];

                                                                $range = ($last_week_start_date_range == $last_week_end_date_range ? ($last_week_start_date . '/' . $current_week_start_month . '/' . $last_week_start_year) : ($last_week_start_date . '/' . $last_week_start_month . '/' . $last_week_start_year . ' - ' . $last_week_end_date . '/' . $last_week_end_month . '/' . $last_week_end_year))
                                                            @endphp
                                                            <span class="dashboard-date-detail">{{ $range }}</span>
                                                        </div>
                                                    <div class="col-sm-6 padright-only">
                                                        <span> Jumlah transaksi</span>
                                                        <div class="clearfix"></div>
                                                        <h3 class="font-bold color-black">{{ $selling['last_week_qty'] }}</h3>
                                                    </div>
                                                    <div class="col-sm-6 no-padding">
                                                        <span> Total Transaksi</span>
                                                        <div class="clearfix"></div>
                                                        <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['last_week_price']) }}</h3>
                                                    </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 dashboard-cursor no-pad-right">
                                <div class="panel panel-flat">
                                    <div class="panel-body no-padding">
                                    <div class="card-custom res-dashcard">
                                            <div class="col-md-2">
                                                <div class="arrow-wrap">
                                                    <img src="{{ $selling['current_month_price'] < $selling['last_month_price'] ? asset('img/arrow-down.png') : ($selling['current_month_price'] == $selling['last_month_price'] ? asset('img/icon-equalto.png') : asset('img/arrow-up.png')) }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="info-wrap" onclick="window.location.href = '{{ route("admin/dashboard","time=this_month") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                            <h4 class="font-600{{ Request::get('time') == 'this_month' ? ' active-statistics' : '' }}">{{ __('time.this_month') }}</h4>
                                                            @php
                                                                $current_month_start_date_range = $full_date['current_month_start_date_range'];
                                                                $current_month_start_date = explode('-',$current_month_start_date_range)[2];
                                                                $current_month_start_month = explode('-',$current_month_start_date_range)[1];
                                                                $current_month_start_year = explode('-',$current_month_start_date_range)[0];

                                                                $current_month_end_date_range = $full_date['current_month_end_date_range'];
                                                                $current_month_end_date = explode('-',$current_month_end_date_range)[2];
                                                                $current_month_end_month = explode('-',$current_month_end_date_range)[1];
                                                                $current_month_end_year = explode('-',$current_month_end_date_range)[0];

                                                                $range = ($current_month_start_date_range == $current_month_end_date_range ? ($current_month_start_date . '/' . $current_month_start_month . '/' . $current_month_start_year . ' - sekarang') : ($current_month_start_date . '/' . $current_month_start_month . '/' . $current_month_start_year . ' - ' . $current_month_end_date . '/' . $current_month_end_month . '/' . $current_month_end_year))
                                                            @endphp
                                                            <span class="dashboard-date-detail">{{ $range }}</span>
                                                        </div>
                                                        <div class="col-sm-6 padright-only">
                                                            <span> Jumlah transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">{{ $selling['current_month_qty'] }}</h3>
                                                        </div>
                                                        <div class="col-sm-6 no-padding">
                                                            <span> Total Transaksi</span>
                                                            <div class="clearfix"></div>
                                                            <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['current_month_price']) }}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="border-dotted"></div>
                                                <div class="clearfix"></div>
                                                <div class="info-wrap" onclick="window.location.href = '{{ route("admin/dashboard","time=last_month") }}'">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="col-xs-12 no-padding">
                                                        <h4 class="font-600{{ Request::get('time') == 'last_month' ? ' active-statistics' : '' }}">{{ __('time.last_month') }}</h4>
                                                        @php
                                                            $last_month_start_date_range = $full_date['last_month_start_date_range'];
                                                            $last_month_start_date = explode('-',$last_month_start_date_range)[2];
                                                            $last_month_start_month = explode('-',$last_month_start_date_range)[1];
                                                            $last_month_start_year = explode('-',$last_month_start_date_range)[0];

                                                            $last_month_end_date_range = $full_date['last_month_end_date_range'];
                                                            $last_month_end_date = explode('-',$last_month_end_date_range)[2];
                                                            $last_month_end_month = explode('-',$last_month_end_date_range)[1];
                                                            $last_month_end_year = explode('-',$last_month_end_date_range)[0];

                                                            $range = ($last_month_start_date_range == $last_month_end_date_range ? ($last_month_start_date . '/' . $last_month_start_month . '/' . $last_month_start_year) : ($last_month_start_date . '/' . $last_month_start_month . '/' . $last_month_start_year . ' - ' . $last_month_end_date . '/' . $last_month_end_month . '/' . $last_month_end_year))
                                                        @endphp
                                                        <span class="dashboard-date-detail">{{ $range }}</span>
                                                    </div>
                                                    <div class="col-sm-6 padright-only">
                                                        <span> Jumlah terjual</span>
                                                        <div class="clearfix"></div>
                                                        <h3 class="font-bold color-black">{{ $selling['last_month_qty'] }}</h3>
                                                    </div>
                                                    <div class="col-sm-6 no-padding">
                                                        <span> Total Transaksi</span>
                                                        <div class="clearfix"></div>
                                                        <h3 class="font-bold color-black">Rp {{ $currency->convertToCurrency($selling['last_month_price']) }}</h3>
                                                    </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(array_search(48, array_column($user['dashboard'], 'menu_id')) !== false)
                        <div class="col-md-12 chart-wrapper no-padding">
                            <div class="panel panel-flat">
                                <div class="card-custom">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">Grafik Penjualan</h4>
                                        {{-- <div class="float-right">
                                            <form>
                                                <select name="category" class="form- single-select">
                                                    <option value="">{{ __('page.choose_category') }}</option>
                                                    <option value="all" {{ Request::get('category') == 'all' ? 'selected' : '' }}>{{ __('field.all') }}</option>
                                                    @for($b = 0; $b < count($category); $b++)
                                                        <option value="{{ $category[$b]['category_id'] }}" {{ Request::get('category') == $category[$b]['category_id'] ? 'selected' : '' }}>{{ $category[$b]['category_name'] }}</option>
                                                    @endfor
                                                </select>
                                            </form>
                                        </div> --}}
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content col-md-12 no-padding" id="dashboardChart">
                                            <div class="col-md-12 margin-b20 no-padding">
                                                <div class="row mb-3 no-margin no-padding">
                                                    <form id="salesForm" action="#">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 margin-b10 no-padding">
                                                            <h6 class="no-margin">Menampilkan Data</h6>
                                                            <input type="text" id="reportrange" class="form-control" name="reportrange" value="{{ Request::get('reportrange')}}">
                                                        </div>
                                                    </form>

                                                    {{-- <div id="reportrange" name="reportrange" value="{{ Request::get('reportrange')}}" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                        <input type="text" name="reportrange" value="{{ Request::get('reportrange')}}" />
                                                        <i class="fa fa-calendar"></i>&nbsp;
                                                        <span></span> <i class="fa fa-caret-down"></i>
                                                    </div> --}}
                                                    
                                                    {{-- <ul class="nav nav-tabs btn-tab col">
                                                        <li class="active">
                                                            <a class="btn btn-fourth mr-2" type="button" data-toggle="tab" data-target="#dashboardChart">1 Hari</a>
                                                        </li>
                                                        <li>
                                                            <a class="btn btn-fourth mr-2" type="button" data-toggle="tab" data-target="#dashboardChart">7 Hari</a>
                                                        </li>
                                                        <li>
                                                            <a class="btn btn-fourth mr-2" type="button" data-toggle="tab" data-target="#dashboardChart">1 Bulan</a>
                                                        </li>
                                                        <li>
                                                            <a class="btn btn-fourth mr-2 " type="button" data-toggle="tab" data-target="#dashboardChart">3 Bulan</a>
                                                        </li>
                                                    </ul> --}}
                                                </div>
                                                <div class="card-header">
                                                <h6 class="card-title">Grafik Penjualan</h6>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart">
                                                        <canvas id="lineChart1" style="min-height: 250px; max-width: 100%; display: block;" class=""></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12 graph-wrapper no-padding">
                            @if(array_search(49, array_column($user['dashboard'], 'menu_id')) !== false)
                            <div class="col-md-6 no-padding">
                                <div class="panel panel-flat">
                                    <div class="card-custom">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-blue-400 text-bold margin-b10">Revenue</h4>
                                            <form id="revenueForm">
                                                <label for="daterange">Periode Waktu</label>
                                                <input type="text" class="form-control" name="datefilterRevenue" value="{{ Request::get('datefilterRevenue')}}" />
                                                <input type="hidden" name="time" value="period">
                                            <!-- <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                    <li><a data-action="reload"></a></li>
                                                </ul>
                                            </div> -->
                                            </form>
                                        </div>
                                        <div class="panel-body padding-10" style="min-height:440px;">
                                            <center>
                                                <div class="chart" id="c3-pie-chart"></div>
                                                @php
                                                    $total_purchased_price = explode(',',$purchased_price);
                                                    $total_purchased_price = array_sum($total_purchased_price);
                                                @endphp
                                                @if($total_purchased_price == 0)
                                                <div class="nodata-img" style="display:block"><img src="{{ asset('img/noresult-image-bw.png') }}" alt=""><h5>Belum ada data penjualan.</h5></div>
                                                @endif
                                            </center>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(array_search(50, array_column($user['dashboard'], 'menu_id')) !== false)
                            <div class="col-md-3 res-nopad">
                                <div class="panel panel-flat">
                                    <div class="card-custom">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-blue-400 text-bold">{{ __('page.best_selling_product') }}</h4>
                                            <!-- <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                    <li><a data-action="reload"></a></li>
                                                </ul>
                                            </div> -->
                                        </div>
                                        <div class="panel-body padding-10">
                                            <div class="most-container">
                                                <form id="bestSellingProductForm">
                                                    <input name="stock_level" type="hidden" value="{{ Request::get('stock_level') == null ? 'running_out_of_stock' : Request::get('stock_level') }}">
                                                    <input type="hidden" name="time" value="{{ $time }}">
                                                    <select name="category" class="form-control single-select">
                                                        <option value="">{{ __('page.choose_category') }}</option>
                                                        <option value="all" {{ Request::get('category') == 'all' ? 'selected' : '' }}>{{ __('field.all') }}</option>
                                                        @for($b = 0; $b < count($category); $b++)
                                                            <option value="{{ $category[$b]['category_id'] }}" {{ Request::get('category') == $category[$b]['category_id'] ? 'selected' : '' }}>{{ $category[$b]['category_name'] }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="margin-tb10"></div>
                                                    <select name="value" class="form-control margin-t10 single-select">
                                                        <option value="">{{ __('field.searching_value') }}</option>
                                                        <!-- <option value="transaction" {{ Request::get('value') == 'transaction' ? 'selected' : '' }}>{{ __('field.total_transaction') }}</option> -->
                                                        <!-- <option value="sold_item" {{ Request::get('value') == 'sold_item' ? 'selected' : '' }}>{{ __('field.total_sold_out_item') }}</option> -->
                                                        <option value="total_price" {{ Request::get('value') == 'total_price' ? 'selected' : '' }}>{{ __('field.total_price') }}</option>
                                                        <option value="product_quantity" {{ Request::get('value') == 'product_quantity' ? 'selected' : '' }}>{{ __('field.product_quantity') }}</option>
                                                        <!-- <option value="product_category" {{ Request::get('value') == 'product_category' ? 'selected' : '' }}>{{ __('field.product_category') }}</option> -->
                                                    </select>
                                                    <div class="margin-tb10"></div>
                                                    <label for="daterange">Periode Waktu</label>
                                                    <input type="text" class="form-control" name="datefilter" value="{{ Request::get('datefilter')}}" />
                                                    <div class="clearfix"></div>
                                                </form>
                                                <!--data result-->
                                                <div class="scroll-y  scroll-terlaris margin-t10">
                                                    @for($b = 0; $b < count($best_selling_product); $b++)
                                                    <div class="most-cards">
                                                    <span class="stock-date">{{ date('d-m-Y', strtotime($best_selling_product[$b]['order_date']))  }}</span>
                                                        <div class="col-md-3 no-padding">
                                                            <div class="card-img ">
                                                                <img src="{{ asset($best_selling_product[$b]['prod_image']) }}" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <h6>{{ $best_selling_product[$b]['prod_name'] . ($best_selling_product[$b]['product_sku_id'] == null ? '' : (' '.($best_selling_product[$b]['uom_value']+0) . ' ' . $best_selling_product[$b]['uom_name'])) }}</h6>
                                                            <span>{{ $best_selling_product[$b]['category_name'] }}</span>
                                                            @if(Request::get('value') == 'total_price')
                                                            @php $total_transaction = $best_selling_product[$b]['total_transaction'];
                                                            $total_transaction = (string)$total_transaction;
                                                            @endphp
                                                            <p>{{ "Rp. ". $currency->convertToCurrency($total_transaction) }}</p>
                                                            @else
                                                            <p>{{ $best_selling_product[$b]['sold_qty'] }} {{ __('page.sold') }}</p>
                                                            @endif
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    @endfor
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(array_search(51, array_column($user['dashboard'], 'menu_id')) !== false)
                            <div class="col-md-3 no-padding">
                                <div class="panel panel-flat">
                                    <div class="card-custom">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-blue-400 text-bold margin-b10">{{ __('page.running_out_of_stock') }}</h4>
                                            @if($user['role_type_id'] == 3 || $user['role_type_id'] == null || $user['role_type_id'] == 1 || $user['role_type_id'] == 4)
                                            <select name="stock_level" id="stock_level" class="form-control single-select">
                                                <option value="">{{ __('page.choose_stock_level') }}</option>
                                                <option value="running_out_of_stock" {{ Request::get('stock_level') == 'running_out_of_stock' || Request::get('stock_level') == null ? 'selected' : '' }}>Stock Hampir Habis</option>
                                                <option value="best_selling_stock" {{ Request::get('stock_level') == 'best_selling_stock' ? 'selected' : '' }}>Stock Terlaris</option>
                                                <option value="unsold_product" {{ Request::get('stock_level') == 'unsold_product' ? 'selected' : '' }}>Stock Tidak Laku</option>
                                            </select>
                                            @endif
                                            <!-- <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                    <li><a data-action="reload"></a></li>
                                                </ul>
                                            </div> -->
                                        </div>
                                        <!--result hampir habis-->
                                        <div class="panel-body padding-10">
                                            <div class="most-container">
                                                <div class="scroll-y scroll-stock">
                                                    @if(count($product) > 0)
                                                    @for($b = 0; $b < count($product); $b++)
                                                    <div class="most-cards">
                                                        <div class="col-md-3 no-padding">
                                                            <div class="card-img ">
                                                                <img src="{{ asset($product[$b]['prod_image']) }}" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <h6>{{ $product[$b]['prod_name'] . ($product[$b]['product_sku_id'] == null ? '' : (' '.($product[$b]['uom_value']+0) . ' ' . $product[$b]['uom_name'])) }}</h6>
                                                            <span>{{ $product[$b]['category_name'] }}</span>
                                                            <div class="clearfix"></div>
                                                            <span>{{ $product[$b]['warehouse_name'] }}</span>
                                                            <div class="clearfix"></div>
                                                            <table class="info-stock">
                                                                <tr>
                                                                    <td>
                                                                        <p>{{ __('page.stock') }} </p>
                                                                    </td>
                                                                    <td><p>:</p></td>
                                                                    <td><p>{{ $product[$b]['total_available_stock'] }}</p></td>
                                                                </tr>
                                                                @if(Request::get('stock_level') == 'best_selling_stock' || Request::get('stock_level') == 'unsold_product')
                                                                <tr>
                                                                    <td>
                                                                        <p>{{ __('page.sold') }} </p>
                                                                    </td>
                                                                    <td><p>:</p></td>
                                                                    <td><p> {{ $product[$b]['sold_qty'] }}</p></td>
                                                                </tr>
                                                                 @elseif(Request::get('stock_level') == null || Request::get('stock_level') == 'running_out_of_stock')
                                                                <tr>
                                                                    <td><p>{{ __('field.minimum_stock') }} </p></td>
                                                                    <td><p>:</p></td>
                                                                    <td><p> {{ $product[$b]['minimum_stock'] }}</p></td>
                                                                </tr>
                                                            @endif
                                                            </table>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                @endfor
                                                @else
                                                <span>{{ __('page.no_data') }}</span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        @endif
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @if(Session::get('password_alert') != null)
    <!-- Modal -->
    <div class="modal fade" id="passwordPopup" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header success">
                    <h5 class="modal-title" id="passwordPopupLabel">Welcome!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-custom-text">
                    <p>{!! Session::get('password_alert') !!}</p>
                </div>
            </div>
        </div>
    </div>
    @endif
    <script>
        var purchasedQty = 'Revenue,'+'{{ $purchased_quantity }}';
        var purchasedPrice = 'Net Income,'+'{{ $purchased_price }}';
        var categoryName = '{{ $category_name }}';
        var price = '{{ $purchased_price }}';
        var categoryArray = categoryName.split(',');
        var priceArray = price.split(',');

        var result = [];
        for(var l = 0;l < categoryArray.length; l++){
            if (priceArray[l] != 0) {
                result[l] = [
                    categoryArray[l], priceArray[l]
                ];
            }
        }
    </script>
    @include('assets_link.js_list', ['page' => 'dashboard'])
    <script>
        $('#bestSellingProductForm select').change(function(){
            $('#bestSellingProductForm').submit();
            showLoader();
        });

        $('select[name="stock_level"]').change(function(){
            // $('<input name="stock_level" type="hidden" value="'+$(this).val()+'">').insertAfter('select[name="category"]');
            $('input[name="stock_level"]').val($(this).val());
            $('#bestSellingProductForm').submit();
            showLoader();
        });

        $(function() {

            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + '-' + picker.endDate.format('MM/DD/YYYY'));
                // console.log(this.value);
                $('#bestSellingProductForm').submit();
            showLoader();
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
            

        });

        $('#revenueForm select').change(function(){
            $('#revenueForm').submit();
            showLoader();
        });

        $(function() {

            $('input[name="datefilterRevenue"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilterRevenue"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + '-' + picker.endDate.format('MM/DD/YYYY'));
                // console.log(this.value);
                $('#revenueForm').submit();
            showLoader();
            });

            $('input[name="datefilterRevenue"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });


     });
    
    //  $('#salesForm select').change(function(e){
    //     e.preventDefault();
    //         $('#salesForm').submit();
    //         showLoader();
    //     });

     $(function() {
            <?php if(isset($_GET['reportrange'])){ ?>
                var astart = "{{ $another_from }}";
                var starts = new Date(astart);
                var aend =  "{{ $another_to }}";
                var ends = new Date(aend);
                var start = moment(starts);
                var end = moment(ends);
                console.log(start+ '-' +end);
                // $('#reportrange').html(moment(start).format('MM/DD/YYYY') + '-' + moment(end).format('MM/DD/YYYY'));
            <?php }else{ ?>
                var start = moment();
                var end = moment();
                // console.log(start+ '-' +end);
            <?php } ?>           
            
            var lstart,lend;
            // console.log(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
            function cb(start, end) {
            $('#reportrange').html(start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY'));
                lstart = moment($('#reportrange').data('daterangepicker').startDate).format('MM/DD/YYYY'),
                lend = moment($('#reportrange').data('daterangepicker').endDate).format('MM/DD/YYYY');
                // console.log(lstart+ '-' +lend);
            }
            
            // console.log(start+' - '+end);
            $('input[name="reportrange"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(7, 'days'), moment()],
                'Last 1 Month': [moment().subtract(29, 'days'), moment()],
                // 'This Month': [moment().startOf('month'), moment().endOf('month')],
                // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                'Last 3 Month': [moment().subtract(87, 'days'), moment()]

            }
            }, cb);
            $('#reportrange').on('apply.daterangepicker', (e, picker) => {
                e.preventDefault();
                // picker.preventDefault();
                $('#salesForm').submit();
                showLoader();
                // return false;
            });
            $('input[name="reportrange"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            cb(start, end);

        });
        //chart
        
        var ctx1 = document.getElementById("lineChart1").getContext('2d');
        ctx1.height = 250;
        var green_gradient = ctx1.createLinearGradient(0, 0, 0, 300);
            green_gradient.addColorStop(0, '#63DB9A');
            green_gradient.addColorStop(1, 'rgba(115, 212, 160, 0)');
        // Array tolol = "";
        // var coba = "{{$bener}}".replace(/&quot;/g, "'");
        // var tolollagi = coba.replace(/&quot;/g, "'").replace(/,/g, " ");
        // var quotedAndCommaSeparated = "'" + coba.join("','") + "'";
            // var results = '['+coba+']';
            var sales = <?php echo json_encode($sales); ?>;
            var dates = <?php echo json_encode($dates); ?>;
            // console.log(sales);
            // console.log(dates);
            // console.log({{ $bener }});
        var myLineChart1 = new Chart(ctx1, {
            type: "line",
            data: {labels:  dates,
            datasets: [{label: "", lineTension: 0,  
            data: sales,
            backgroundColor: green_gradient,
            hoverBackgroundColor: green_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: "#F26327",
            borderWidth: 2,
            borderColor: "#009444",
            pointColor : "#fff",
            pointStrokeColor : "#009444",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#009444",
            }
            ]

            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                    }
                },
                scales: { 
                    xAxes: [{ gridLines: { color: "rgba(0, 0, 0, 0)", } }], 
                    yAxes: [{ 
                        gridLines: { color: "rgb(166, 172, 190, 0.5)", 
                        borderDash: [4, 6] },
                        ticks: {
                            suggestedMin: 0,
                            beginAtZero: true,
                            steps: 5,
                            stepSize: 0
                        }
                     }] 
                }
            }
        });
    </script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
