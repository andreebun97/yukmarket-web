
@extends('admin::layouts.master')

@section('title',__('page.user_detail'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'customer'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-user position-left"></i>{{ __('menubar.customer') }}</li>
                                <li class="active text-bold ">{{ __('page.user_detail') }} ({{ $role_name == 'customer' ? __('role.customer') : __('role.admin') }})</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.user_detail') }} ({{ $role_name == 'customer' ? __('role.customer') : __('role.admin') }})</h4>
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <div class="panel-body">
                                                <div class="col-md-12 no-padding">
                                                    <div id="user_profile " class="info-detail">
                                                        <div class="col-md-12 no-padding">
                                                            <h2>{{ __('page.profile') }}</h2>
                                                        </div>
                                                        <div class="col-md-12 no-padding float-left usr-detail">
                                                            <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                                                                <div class="form-group no-padding">
                                                                    <label class="col-md-5 col-sm-4 col-lg-4 col-xs-5" for="user_name">{{ __('field.user_name') }}</label>
                                                                    <span class="col-md-7 col-sm-8 col-lg-8 col-xs-7 user_data_information">
                                                                    {{ $user_by_id['user_name'] == null ? '-' : $user_by_id['user_name'] }}
                                                                    </span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="form-group no-padding">
                                                                    <label class="col-md-5 col-sm-4 col-lg-4 col-xs-5" for="user_name">{{ __('field.user_email') }}</label>
                                                                    <span class="col-md-7 col-sm-8 col-lg-8 col-xs-7 user_data_information">
                                                                    {{ $user_by_id['user_email'] == null ? '-' : $user_by_id['user_email'] }}
                                                                    </span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="form-group no-padding">
                                                                    <label class="col-md-5 col-sm-4 col-lg-4 col-xs-5" for="user_name">{{ __('field.user_phone_number') }}</label>
                                                                    <span class="col-md-7 col-sm-8 col-lg-8 col-xs-7 user_data_information">
                                                                    {{ $user_by_id['user_phone_number'] == null ? '-' : $user_by_id['user_phone_number'] }}
                                                                    </span>
                                                                </div>
                                                                <!-- <div class="clearfix"></div> -->

                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                                                                <div class="form-group no-padding">
                                                                    <label class="col-md-5 col-sm-4 col-xs-5" for="user_name">{{ __('field.created_date') }}</label>
                                                                    <span class="col-md-7 col-sm-8 col-xs-7 user_data_information">
                                                                    {{ $user_by_id['created_date'] == null ? '-' : date("d-m-Y",strtotime($user_by_id['created_date'])) }}
                                                                    </span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="form-group no-padding no-margin">
                                                                    <label class="col-md-5 col-sm-4 col-xs-5" for="user_name" for="user_address">{{ __('field.user_address') }}</label>
                                                                    @if(count($address_detail) == 0)
                                                                        <span class="user_data_information">-</span>
                                                                    @else
                                                                    <span class="col-md-7 col-sm-8 col-xs-7 user_data_information">
                                                                        <ul class="user_data_information">
                                                                        @for($b = 0; $b < count($address_detail); $b++)
                                                                            <li>{{ $address_detail[$b]['address_detail'] . ', ' . $address_detail[$b]['kabupaten_kota_name'] . ', ' . $address_detail[$b]['kecamatan_name'] . ', '. $address_detail[$b]['kelurahan_desa_name'] . ', '. $address_detail[$b]['kode_pos'] . ' (' . ($address_detail[$b]['address_info']  == null ? 'Tidak ada informasi' : $address_detail[$b]['address_info']) . ', '.($address_detail[$b]['contact_person']  == null ? 'Tidak ada orang yang dapat dihubungi' : $address_detail[$b]['contact_person']).'/'.($address_detail[$b]['phone_number']  == null ? 'Tidak ada orang yang dapat dihubungi' : $address_detail[$b]['phone_number']).', '.($address_detail[$b]['label_address']  == null ? 'Tidak ada label' : $address_detail[$b]['label_address']).')'  }}</li>
                                                                        @endfor
                                                                        </ul>
                                                                    </span>
                                                                    @endif
                                                                <div class="clearfix"></div>
                                                                <form class="no-margin" method="POST" action="{{ $role_name == 'admin' ? route('admin/yukmarket/user/admin/form') : route('admin/yukmarket/user/customer/form') }}">
                                                                    @csrf
                                                                        <input type="hidden" name="user_id" value="{{ Request::get('id') }}">
                                                                        <label class="col-md-5 col-sm-4 col-xs-5"  for="role_name">{{ __('field.user_status') }}</label>
                                                                        <span class="col-md-7 col-sm-8 col-xs-7 user_data_information">
                                                                        <div class="form-check float-left margin-r10">
                                                                            <input class="form-check-input" type="radio" name="user_active_status" {{ $user_by_id['user_active_status'] == 1 ? 'checked' : '' }} value="1">
                                                                            <label class="form-check-label" for="super_active_status">
                                                                            {{ __('field.active_status') }}
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check float-left">
                                                                            <input class="form-check-input" type="radio" name="user_active_status" {{ $user_by_id['user_active_status'] == 0 ? 'checked' : '' }} value="0">
                                                                            <label class="form-check-label" for="inactive_status">
                                                                                {{ __('field.not_active_status') }}
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </form>
                                                                <!-- <div class="col-md-2 col-sm-6 col-xs-12 float-right no-padding">
                                                                    <button type="submit" class="btn btn-orange btn-block" >{{ __('page.submit') }}</button>
                                                                </div> -->
                                                            </div>
                                                            <div class="col-md-3 col-sm-6 col-xs-12 margin-t20 float-right no-padding">
                                                                <button type="submit" class="btn btn-orange btn-block" >{{ __('page.submit') }}</button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="panel-body">
                                                <div class="col-md-12 no-padding">
                                                    <div id="user_transaction" class="info-detail">
                                                        <div id="user_transaction_header" style="width:100%">
                                                            <h2 style="margin-right:auto; float:left">{{ __('page.transactions') }}</h2>
                                                            <div class="float-right">
                                                                <a class="btn btn-orange" style="margin-left:auto" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i> Pencarian Detail</a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="margin-tb-10"></div>
                                                        <div class="separator no-margin"></div>
                                                        <div class="margin-tb-10"></div>
                                                        @if($role_name == 'customer')
                                                        <div id="purchase_transaction">
                                                            <div class="form-group">
                                                                <div id="purchase_transaction_detail">
                                                                    <table class="table display w-100 table-brown" id="payment_list_table">
                                                                        <thead class="bg-darkgrey">
                                                                            <tr role="row">
                                                                                <th>No.</th>
                                                                                <th>{{ __('field.order_code') }}</th>
                                                                                <th>{{ __('field.total_price') }}</th>
                                                                                <th>{{ __('field.purchased_date') }}</th>
                                                                                <th>{{ __('field.order_status') }}</th>
                                                                                <th>{{ __('field.action') }}</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <!-- <tfoot class="bg-darkgrey">
                                                                            <tr role="row">
                                                                                <td colspan="5" style="text-align:right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </tfoot> -->
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        @endif
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="transactionFilterForm" method="POST" action="{{ route('admin/yukmarket/filter') }}">
                        @csrf
                        <input type="hidden" name="customer_id" value="{{ Request::get('id') }}">
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <input type="text" class="form-control daterange-single" id="transaction_start_date" name="transaction_start_date" @if(Request::get('start') != null) value="{{ Request::get('start') }}" @else value="{{ date('Y-m-d') }}" @endif>
                                    <!-- <input type="date" class="form-control" id="transaction_start_date" name="transaction_start_date" @if(Request::get('start') != null) value="{{ Request::get('start') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <input type="text" class="form-control daterange-single" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('end') != null) value="{{ Request::get('end') }}" @else value="{{ date('Y-m-d') }}"  @endif>
                                     <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('end') != null) value="{{ Request::get('end') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.order_status') }}</label>
                                    <select name="order_status" id="order_status" class="form-control single-select" placeholder="{{ __('page.choose_order_status') }}">
                                        <option value="">{{ __('page.choose_order_status') }}</option>
                                        @for($b = 0; $b < count($order_status); $b++)
                                            <option value="{{ $order_status[$b]['order_status_id'] }}"  @if(Request::get('status') != null) @if($order_status[$b]['order_status_id'] == Request::get('status')) selected @endif @endif>{{ $order_status[$b]['order_status_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="minimum_total">{{ __('field.minimum_total') }}</label>
                                    <input type="text" onkeyup="currencyFormat(this)" class="form-control" id="minimum_total" name="minimum_total" aria-describedby="minimum_total"  @if(Request::get('minimum_total') != null) value="{{ Request::get('minimum_total') }}" @endif autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="maximum_total">{{ __('field.maximum_total') }}</label>
                                    <input type="text" onkeyup="currencyFormat(this)" class="form-control" id="maximum_total" name="maximum_total" aria-describedby="maximum_total" @if(Request::get('maximum_total') != null) value="{{ Request::get('maximum_total') }}" @endif autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding margin-t10">
                                <div class="col-md-6 padding-r-10 res-btn-mid">
                                    <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('start') == null && Request::get('end') == null && Request::get('status') == null && Request::get('minimum_total') == null && Request::get('maximum_total') == null) disabled @endif>{{ __('page.reset') }}</button>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Request::get('order_id') != null)
    <!-- Modal -->
    <div class="modal fade" id="transactionDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="transactionDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <div class="modal-header">
                    <h5 class="modal-title" id="transactionDetailModalLabel">{{ __('page.transaction_detail') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body rincian-transaksi" style="max-height:90vh">
                    <div class="info-orange display-block font-17">
                        <i class="fa fa-info-circle"></i>
                        {{ __('field.order_code') }}: <bold>{{ $order_master->order_code }}</bold>
                    </div>



                    <table class="table display w-100 table-brown total-transaksi res-scroll-md" id="banner_list_table">
                        <thead class="bg-darkgrey">
                            <tr role="row">
                                <th>Gambar</th>
                                <th>Kode</th>
                                <th style="min-width:200px;" >Nama</th>
                                <th class="text-right">Jumlah</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $subtotal = 0; @endphp
                        @for($b = 0; $b < count($order_detail); $b++)
                            @php
                                $total_per_product = ($order_detail[$b]['quantity'] * $order_detail[$b]['price']) - $order_detail[$b]['promo_value'];
                                $subtotal = $subtotal + $total_per_product;
                            @endphp
                            <tr  @if($b < count($order_detail) - 1) @endif>
                                <td><img class="img-50" src="{{ asset($order_detail[$b]['prod_image']) }}" alt="" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></td>
                                <td>{{ $order_detail[$b]['prod_code'] }}</td>
                                <td> {{ $order_detail[$b]['prod_name'] . ($order_detail[$b]['sku_status'] == 0 ? '' : (' '.$order_detail[$b]['uom_value'].' '.$order_detail[$b]['uom_name'])) }}</td>
                                <td class="text-right"> {{ $order_detail[$b]['quantity'] }}</td>
                                <td class="text-right">{{ $currency->convertToCurrency($order_detail[$b]['price']) }}</td>
                                <td class="text-right">{{ $currency->convertToCurrency($total_per_product) }}</td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                    <div class="col-md-12 res-dtl-text no-padding">
                        <div class="total-wrap">
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.subtotal') }} </span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black">{{ $currency->convertToCurrency($subtotal) }}</span>
                        </div>
                        @php
                            $national_income_tax = 0;
                            $tax_basis = 0;
                            if($order_master->pricing_include_tax == 0){
                                $national_income_tax = $subtotal * $order_master->national_income_tax;
                                $subtotal = $subtotal + $national_income_tax;
                            }else{
                                $tax_basis = ROUND($subtotal / 1.1);
                                $national_income_tax = $subtotal - $tax_basis;
                            }
                        @endphp
                        @if($order_master->pricing_include_tax == 1)
                        <div class="col-md-6 no-padding tax-dpp d-none">
                            <span class="dp-block">{{ __('field.tax_basis') }} </span>
                        </div>
                        <div class="col-md-6 no-padding tax-dpp d-none">
                            <span style="display:block" class="float-right color-black">{{ $currency->convertToCurrency($tax_basis) }}</span>
                        </div>
                        @endif
                        <div class="col-md-6 no-padding tax-ppn {{ $order_master->pricing_include_tax == 1 ? 'd-none' : '' }}">
                            <span class="dp-block color-black">{{ __('field.national_income_tax') }} @if($order_master->pricing_include_tax == 0) {{ '('.($order_master->national_income_tax*100).'%)' }} @endif </span>
                        </div>
                        <div class="col-md-6 no-padding tax-ppn {{ $order_master->pricing_include_tax == 1 ? 'd-none' : '' }}">
                            <span style="display:block" class="float-right color-black">{{ $currency->convertToCurrency($national_income_tax) }}</span>
                        </div>
                        <div class="col-md-6 no-padding d-none">
                            <span class="dp-block color-black">{{ __('field.voucher') }}</span>
                        </div>
                        <div class="col-md-6 no-padding d-none">
                            @php
                                $voucher_amount = 0;

                            @endphp
                            <span style="display:block;" class="float-right color-red"> {{ $order_master->voucher_id == null ? '-' : ($order_master->is_fixed == 'Y' ? $order_master->voucher_amount : '-'.$currency->convertToCurrency($voucher_amount)) }}</span>
                            @php $subtotal_and_amount = $subtotal - $voucher_amount; @endphp
                        </div>
                        <div class="col-md-6 no-padding">
                            <span class="dp-block color-black">{{ __('field.shipment_fee') }}</span>
                        </div>

                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black" > {{ $order_master->shipment_price == null ? '-' : $currency->convertToCurrency($order_master->shipment_price) }}</span>
                            @php
                                $all_admin_fee = 0;
                                $admin_fee = 0;

                            @endphp
                            <!-- @php $grand_total = $all_admin_fee + ($order_master->shipment_price == null ? 0 : $order_master->shipment_price) + $subtotal_and_amount; @endphp -->
                        </div>
                        <div class="col-md-6 no-padding d-none">
                                <span class="dp-block color-black">{{ __('field.admin_fee') }}</span>
                        </div>
                        <div class="col-md-6 no-padding d-none">
                                <span style="display:block" class="float-right color-black">{{ $order_master->admin_fee == null && $order_master->admin_fee_percentage ? '-' : $admin_fee }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="color-black font-bold">{{ __('field.total') }}</span>
                        </div>
                        <div class="col-md-6 no-padding">
                            <span style="display:block" class="float-right color-black font-bold">{{ $currency->convertToCurrency($grand_total) }}</span>
                        </div>
                        <div class="clearfix"></div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <div class="separator margin-tb10"></div>
                    <div class="col-md-12 no-padding res-dtl-text">
                        <div class="col-md-4 no-padding">
                            <span style="display:block">{{ __('field.purchased_date') }}</span>
                        </div>
                        <div class="col-md-8 no-padding">
                            <span style="display:block">: {{ $order_master->order_date }}</span>
                        </div>
                        <div class="col-md-4 no-padding">
                        <span style="display:block">{{ __('field.destination_address') }}</span>
                        </div>
                        <div class="col-md-8 no-padding">
                            <span style="display:block"> : {{ $order_master->address_detail . ', ' . $order_master->kabupaten_kota_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kelurahan_desa_name . ', ' . $order_master->kode_pos }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $('input[name="minimum_total"]').val(currencyFormat(document.getElementById('minimum_total')));
        $('input[name="maximum_total"]').val(currencyFormat(document.getElementById('maximum_total')));
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ Request::url().'?id='.Request::get('id') }}";
            showLoader();
        });

        var string_length = 0;
        $('#transactionFilterForm input[type="date"]').change(function(){
            string_length = 0;
            // if($('#transactionFilterForm select').val() == ""){
            //     string_length = 1;
            // }
            if($('#transactionFilterForm select').val() == ""){
                string_length = 1;
            }
            var length = $('#transactionFilterForm input[type="text"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="text"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }

            length = $('#transactionFilterForm input[type="date"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="date"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }
            // console.log(string_length);
            if(string_length == 1) {
                $('#resetFilterButton').attr('disabled',true);
            }else{
                $('#resetFilterButton').attr('disabled',false);
            }
        });

        $('#transactionFilterForm input[type="text"]').keyup(function(){
            string_length = 0;
            if($('#transactionFilterForm select').val() == ""){
                string_length = 1;
            }
            var length = $('#transactionFilterForm input[type="text"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="text"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }

            length = $('#transactionFilterForm input[type="date"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="date"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }
            // console.log(string_length);
            if(string_length == 1) {
                $('#resetFilterButton').attr('disabled',true);
            }else{
                $('#resetFilterButton').attr('disabled',false);
            }

        });

        // $('#transactionFilterForm input[name="maximum_total"]').change(function(){
        //     var minimum_total = $('#transactionFilterForm input[name="minimum_total"]').val().split('.').join('');
        //     var maximum_total = $(this).val().split('.').join('');
        //     console.log(minimum_total);
        //     console.log(maximum_total);
        //     if(minimum_total > maximum_total){
        //         $(this).val(convertToCurrency(minimum_total));
        //     }
        //     // console.log(parseInt(maximum_total));
        // });

        $('#transactionFilterForm select').change(function(){
            string_length = 0;
            // var length = $('#transactionFilterForm select').length;
            // // console.log(length);
            // for (let b = 0; b < length; b++) {
            //     // const element = array[b];
            //     // string_length += (b+1);
            //     if($('#transactionFilterForm select:eq('+b+')').val().length == 0){
            //         string_length = 1;
            //     }
            // }
            // console.log(string_length);
            if($(this).val() == ""){
                string_length = 1;
            }
            var length = $('#transactionFilterForm input[type="text"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="text"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }

            length = $('#transactionFilterForm input[type="date"]').length;
            // console.log(length);
            for (let b = 0; b < length; b++) {
                // const element = array[b];
                // string_length += (b+1);
                if($('#transactionFilterForm input[type="date"]:eq('+b+')').val().length == 0){
                    string_length = 1;
                }
            }

            if(string_length == 1) {
                $('#resetFilterButton').attr('disabled',true);
            }else{
                $('#resetFilterButton').attr('disabled',false);
            }
        });


        // $('input[name="transaction_start_date"]').change(function(){
        //     // var index = $('input[name="multiple_starts_at[]"]').index(this);
        //     var chosenDateTemp = new Date($(this).val());
        //     var chosenDate = chosenDateTemp.getFullYear()+'-'+((chosenDateTemp.getMonth()+1) < 10 ? ("0" + (chosenDateTemp.getMonth()+1)) : (chosenDateTemp.getMonth()+1))+'-'+(chosenDateTemp.getDate() < 10 ? ("0"+chosenDateTemp.getDate()) : chosenDateTemp.getDate());
        //     var fullChosenDateTime = chosenDate;
        //     $('input[name="transaction_end_date"]').attr('min',fullChosenDateTime);
        //     $('input[name="transaction_end_date"]').val(fullChosenDateTime);
        // });

        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });

        var paymentListTable = $('#payment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            order: [[ 1, "desc" ]],
            ajax: {
                url:'{{ route("datatable/get_payment") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.customer_id = "{{ $user_by_id['user_id'] }}";
                    d.method = "user_detail";
                    d.url = "{{ Request::fullUrl() }}";
                    d.start_date = "{{ Request::get('start') }}";
                    d.end_date = "{{ Request::get('end') }}";
                    d.order_status = "{{ Request::get('status') }}";
                    d.minimum_total = "{{ Request::get('minimum_total') }}";
                    d.maximum_total = "{{ Request::get('maximum_total') }}";
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.agent_user_id = "{{ $user['organization_id'] }}";
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {
                    data: 'total_price',
                    name: 'total_price',
                    className: 'dt-body-right'
                },
                {data: 'purchased_date', name: 'purchased_date'},
                {data: 'order_status_name', name: 'order_status_name'},
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
    </script>
@endsection
