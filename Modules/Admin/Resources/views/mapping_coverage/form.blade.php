@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_location_coverage') : __('page.edit_location_coverage')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.location_coverage') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_location_coverage') : __('page.edit_location_coverage') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_location_coverage') : __('page.edit_location_coverage') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            @php $route = route('admin/mapping_coverage/form'); @endphp
                                        	<div class="clearfix"></div>
                                         		<form method="POST" id="mappingCoverageForm" action="{{ $route }}" class="form-style">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="panel-body">
                                                            @csrf
                                                            @if(Request::get('id') != null)
                                                                <input type="hidden" name="mapping_coverage_id" value="{{ Request::get('id') }}">
                                                            @endif
                                                            <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="organization_name">{{ __('field.organization_name') }} <span class="required-label">*</span></label>
                                                                        <select name="organization_name" class="form-control single-select">
                                                                            @for($b = 0; $b < count($organization); $b++)
                                                                            <option value="{{ $organization[$b]['organization_id'] }}">{{ $organization[$b]['organization_name'] }}</option>
                                                                            @endfor
                                                                        </select>
                                                                        @error('organization_name')
                                                                            <span class="invalid-feedback">{{ $errors->first('organization_name') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="province_coverage">{{ __('field.province_coverage') }} <span class="required-label">*</span></label>
                                                                        <select name="province_coverage[]" class="form-control single-select">
                                                                        </select>
                                                                        @error('province_coverage')
                                                                            <span class="invalid-feedback">{{ $errors->first('province_coverage') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="regency_coverage">{{ __('field.regency_coverage') }} <span class="required-label">*</span></label>
                                                                        <select name="regency_coverage" class="form-control single-select">
                                                                        </select>
                                                                        @error('regency_coverage')
                                                                            <span class="invalid-feedback">{{ $errors->first('regency_coverage') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="district_coverage">{{ __('field.district_coverage') }} <span class="required-label">*</span></label>
                                                                        <select name="district_coverage" class="form-control single-select">
                                                                        </select>
                                                                        @error('district_coverage')
                                                                            <span class="invalid-feedback">{{ $errors->first('district_coverage') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="village_coverage">{{ __('field.village_coverage') }} <span class="required-label">*</span></label>
                                                                        <select name="village_coverage" class="form-control single-select">
                                                                        </select>
                                                                        @error('village_coverage')
                                                                            <span class="invalid-feedback">{{ $errors->first('village_coverage') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- <div class="location_coverage_detail">
                                                    <table class="display" id="locationCoverageDetail">
                                                        <tr>
                                                            <th>{{ __('field.organization_name') }}</th>
                                                            <th>{{ __('field.province_coverage') }}</th>
                                                            <th>{{ __('field.regency_coverage') }}</th>
                                                            <th>{{ __('field.district_coverage') }}</th>
                                                            <th>{{ __('field.village_coverage') }}</th>
                                                        </tr>
                                                    </table>
                                                </div> -->
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var validationRules = {
            organization_name: {
                required: true
            },
            province_coverage: {
                required: true
            },
            regency_coverage: {
                required: true
            },
            district_coverage: {
                required: true
            },
            village_coverage: {
                required: true
            }
        };
        var validationMessages = {
            organization_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.organization_name')]) }}"
            },
            province_coverage: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.province_coverage')]) }}"
            },
            regency_coverage: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.regency_coverage')]) }}"
            },
            district_coverage: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.district_coverage')]) }}"
            },
            village_coverage: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.village_coverage')]) }}"
            }
        }

        $('form#mappingCoverageForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        var province_array = [];
        var regency_array = [];
        var district_array = [];
        var village_array = [];
        getProvince();
        $('select[name="province_coverage[]"]').html(province_array);

        @if(Request::get('id') != null)
            $('select[name="organization_name"]').val("{{ $mapping_coverage_by_id['organization_id'] }}").trigger('change');
            $('select[name="province_coverage[]"]').val("{{ $mapping_coverage_by_id['provinsi_id'] }}");
            getRegency($('select[name="province_coverage[]"]').val());
            $('select[name="regency_coverage"]').val("{{ $mapping_coverage_by_id['kabupaten_kota_id'] }}");
            getDistrict($('select[name="regency_coverage"]').val());
            $('select[name="district_coverage"]').val("{{ $mapping_coverage_by_id['kecamatan_id'] }}");
            getVillage($('select[name="district_coverage"]').val());
            $('select[name="village_coverage"]').val("{{ $mapping_coverage_by_id['kelurahan_desa_id'] }}");
        @endif

        $('select[name="province_coverage[]"]').change(function(){
            province_coverage = $(this).val();
            getRegency(province_coverage);
            $('select[name="district_coverage"]').val("").trigger('change');
            $('select[name="village_coverage"]').val("").trigger('change');
            regency_array = [];
        });
        $('select[name="regency_coverage"]').change(function(){
            regency_coverage = $(this).val();
            getDistrict(regency_coverage);
            $('select[name="village_coverage"]').val("").trigger('change');
            district_array = [];
        });
        $('select[name="district_coverage"]').change(function(){
            district_coverage = $(this).val();
            getVillage(district_coverage);
            village_array = [];
        });

        function getProvince(){
            $.ajax({
                url: "/api/provinsi",
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var province = resultData['data'];
                    for (let b = 0; b < province.length; b++) {
                        province_array.push("<option value='"+province[b]['provinsi_id']+"'>"+province[b]['provinsi_name']+"</option>");                        
                    }
                }
            });
        }

        function getRegency(province_id, regency_div = null){
            $.ajax({
                url: "/api/kabkot/"+province_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var regency = resultData['data'];
                    console.log(resultData);
                    if(regency.length > 0){
                        for (let b = 0; b < regency.length; b++) {
                            regency_array.push("<option value='"+regency[b]['kabupaten_kota_id']+"'>"+regency[b]['kabupaten_kota_name']+"</option>");                        
                        }
                        $('select[name="regency_coverage"]').html(regency_array);
                    }
                }
            });
        }

        function getDistrict(regency_id){
            $.ajax({
                url: "/api/kecamatan/"+regency_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(regency_id);
                    // console.log(resultData);
                    var district = resultData['data'];
                    console.log(resultData);
                    if(district.length > 0){
                        for (let b = 0; b < district.length; b++) {
                            district_array.push("<option value='"+district[b]['kecamatan_id']+"'>"+district[b]['kecamatan_name']+"</option>");                        
                        }
                        $('select[name="district_coverage"]').html(district_array);
                    }
                }
            });
        }

        function getVillage(district_id){
            $.ajax({
                url: "/api/kelurahan/"+district_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(district_id);
                    // console.log(resultData);
                    var village = resultData['data'];
                    console.log(resultData);
                    if(village.length > 0){
                        for (let b = 0; b < village.length; b++) {
                            village_array.push("<option value='"+village[b]['kelurahan_desa_id']+"'>"+village[b]['kelurahan_desa_name']+"</option>");                        
                        }
                        $('select[name="village_coverage"]').html(village_array);
                    }
                }
            });
        }

        clickCancelButton('{{ route("admin/mapping_coverage") }}')

        exitPopup('{{ route("admin/mapping_coverage") }}');
    </script>
    @endif
@endsection
