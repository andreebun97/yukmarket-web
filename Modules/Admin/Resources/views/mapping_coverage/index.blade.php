
@extends('admin::layouts.master')

@section('title',__('menubar.location_coverage'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li class="active text-bold ">{{ __('menubar.location_coverage') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.location_coverage') }}</h4>
                                            <div class="float-right col-xs-12 col-sm-7 no-padding">
                                                <div class="col-xs-12 col-sm-8 margin-b10 no-padding-left res-no-pad-sm">
                                                @if($accessed_menu == 1 && Request::get('organization') != null)
                                                
                                                    <a class="btn btn-orange btn-block" role="button" id="addMappingCoverageButton" data-toggle="modal" data-target="#filterModalForm" data-id="1"><i class="fa fa-plus"></i> {{ __('page.add_location_coverage') }}</a>
                                                
                                                @endif
                                                </div>
                                                <div class="col-xs-12 col-sm-4 no-padding-right res-no-pad-sm">
                                                    <form method="GET" id="organizationForm">
                                                        <select name="organization" id="organization_name" class="form-control single-select">
                                                            <option value="">{{ __('page.choose_organization_name') }}</option>
                                                            @for($b = 0; $b < count($organization); $b++)
                                                                <option value="{{ $organization[$b]['organization_id'] }}" @if(Request::get('organization') != null && Request::get('organization') == $organization[$b]['organization_id']) selected @endif>{{ $organization[$b]['organization_name'] }}</option>
                                                            @endfor
                                                        </select>
                                                    </form>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="panel-body" >
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                           
                                            <table class="table display w-100 table-brown" id="mapping_coverage_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No</th>
                                                        <th>{{ __('field.province_coverage') }} </th>
                                                        <th>{{ __('field.regency_coverage') }}</th>
                                                        <th>{{ __('field.district_coverage') }}</th>
                                                        <th>{{ __('field.village_coverage') }}</th>
                                                        <th>{{ __('field.postal_code') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="filterModalFormLabel">Tambah Cakupan Lokasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <form method="POST" action="{{ route('admin/mapping_coverage/form') }}">
                        @csrf
                        <input type="hidden" name="organization_id" value="{{ Request::get('organization') }}">
                        <div class="col-md-12 margin-b20 no-padding res-no-pad">
                            <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                <div class="form-group margin-b10">
                                    <label for="province_coverage">{{ __('field.province_coverage') }}</label>
                                    <select name="province_coverage[]" class="select-state multi-select" multiple></select>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="regency_coverage">{{ __('field.regency_coverage') }}</label>
                                    <select name="regency_coverage[]" class="select-state multi-select" multiple></select>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                <div class="form-group margin-b10">
                                    <label for="district_coverage">{{ __('field.district_coverage') }}</label>
                                    <select name="district_coverage[]" class="select-state multi-select" multiple></select>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="village_coverage">{{ __('field.village_coverage') }}</label>
                                    <select name="village_coverage[]" class="select-state multi-select" multiple></select>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12 res-scroll no-padding">
                            <table id="locationCoverageView" class="table">
                                <thead class="bg-darkgrey">
                                    <tr role="row">
                                        <th><a id="deleteAllMappingButton" role="button"><i class="fa fa-times-circle color-red ico-size-td"></i></a></th>
                                        <th>{{ __('field.province_coverage') }} <span class="required-label">*</span></th>
                                        <th>{{ __('field.regency_coverage') }}</th>
                                        <th>{{ __('field.district_coverage') }}</th>
                                        <th>{{ __('field.village_coverage') }}</th>
                                        <th>{{ __('field.postal_code') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                                                            
                            
                        </div>
                    
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                        <div class="col-xs-6 padding-r-10 res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <button type="submit" disabled id="submitFilterButton" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var columnArray = [
            {
                data: 'action',
                sortable: false,
                searchable: false
            },
            {
                data: null,
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {data: 'province_coverage_name', name: 'province_coverage_name'},
            {data: 'regency_coverage_name', name: 'regency_coverage_name'},
            {data: 'district_coverage_name', name: 'district_coverage_name'},
            {data: 'village_coverage_name', name: 'village_coverage_name'},
            {data: 'postal_code', name: 'postal_code'},
        ];
        
        var mappingCoverageTable = $('#mapping_coverage_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                lengthMenu: '{{ __("page.showing") }} <select name="location_coverage_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_mapping_coverage") }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    d.organization_id = "{{ Request::get('organization') }}";
                }
            },
            columns: columnArray,
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
        // $('.deleteMappingButton').click(function(){
        //     index = $('.deleteMappingButton').index(this);
        //     deleteMappingTable(index);
        // });
        $('select[name="organization"]').change(function(){
            $('form#organizationForm').submit();
        });
        @if(Request::get('organization') != null)

        var province_coverage = [];
        var regency_coverage  = [];
        var district_coverage  = [];
        var village_coverage  = [];
        // $('select[name="province_coverage[]"]').change(function(){
        //     province_id = $(this).val();
        //     getMappingCoverage(province_id,null, null, null);
        // });
        // $('select[name="regency_coverage[]"]').change(function(){
        //     regency_id = $(this).val();
        //     getMappingCoverage(province_id,regency_id, null, null);
        // });

        $('#filterModalForm').on('shown.bs.modal', function(){
            getProvince();
        });
        
        $('#filterModalForm').on('hidden.bs.modal', function(){
            $('table#locationCoverageView tbody').empty();
            $('table#locationCoverageView tbody').html('<tr><td colspan="6" style="text-align:center">{{ __("page.no_data") }}</td></tr>');
            $('select[name="regency_coverage[]"]').val([]).trigger('change');
            $('select[name="district_coverage[]"]').val([]).trigger('change');
            $('select[name="village_coverage[]"]').val([]).trigger('change');
            $('#submitFilterButton').attr('disabled',true);
        });

        $('select[name="province_coverage[]"]').change(function(){
            province_coverage = $(this).val();
            regency_coverage = null;
            district_coverage = null;
            village_coverage = null;
            getRegency({ province: province_coverage});
            $('select[name="district_coverage[]"]').val(district_coverage).trigger('change');
            $('select[name="village_coverage[]"]').val(village_coverage).trigger('change');
            getMappingCoverage(province_coverage,regency_coverage,district_coverage, village_coverage);
        });
        $('select[name="regency_coverage[]"]').change(function(){
            regency_coverage = $(this).val();
            district_coverage = null;
            village_coverage = null;
            getDistrict({ regency: regency_coverage});
            getMappingCoverage(province_coverage,regency_coverage, district_coverage, village_coverage);
            $('select[name="village_coverage[]"]').val(null).trigger('change');
        });
        $('select[name="district_coverage[]"]').change(function(){
            district_coverage = $(this).val();
            village_coverage = null;
            getVillage({ district: district_coverage});
            getMappingCoverage(province_coverage,regency_coverage, district_coverage, village_coverage);
        });
        $('select[name="village_coverage[]"]').change(function(){
            village_coverage = $(this).val();
            // getVillageDetail(village_coverage);
            getMappingCoverage(province_coverage,regency_coverage, district_coverage, village_coverage);
        });

        // $('input[name="province_coverage_checkbox"]').click(function(){
        //     if($(this).prop('checked') == false){
        //         $('input[name="regency_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="district_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="village_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="regency_coverage_checkbox"]').siblings().addClass('d-none')
        //         $('input[name="district_coverage_checkbox"]').siblings().addClass('d-none')
        //         $('input[name="village_coverage_checkbox"]').siblings().addClass('d-none')
        //         $(this).siblings().addClass('d-none');
        //     }else{
        //         $(this).siblings().removeClass('d-none');
        //     }
        // });

        // $('input[name="regency_coverage_checkbox"]').click(function(){
        //     console.log($(this).prop('checked'));
        //     if($(this).prop('checked') == true){
        //         $('input[name="province_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="regency_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="province_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $(this).siblings().removeClass('d-none');
        //     }else{
        //         $('input[name="district_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="village_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="district_coverage_checkbox"]').siblings().addClass('d-none');
        //         $('input[name="village_coverage_checkbox"]').siblings().addClass('d-none');
        //         $(this).siblings().addClass('d-none');
        //     }
        // });

        // $('input[name="district_coverage_checkbox"]').click(function(){
        //     console.log($(this).prop('checked'));
        //     if($(this).prop('checked') == true){
        //         $('input[name="province_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="regency_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="district_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="province_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $('input[name="regency_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $(this).siblings().removeClass('d-none');
        //     }else{
        //         // $('input[name="district_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="village_coverage_checkbox"]').prop('checked',false);
        //         $('input[name="province_coverage_checkbox"]').siblings().addClass('d-none');
        //         $('input[name="regency_coverage_checkbox"]').siblings().addClass('d-none');
        //         $(this).siblings().addClass('d-none');
        //     }
        // });

        // $('input[name="village_coverage_checkbox"]').click(function(){
        //     console.log($(this).prop('checked'));
        //     if($(this).prop('checked') == true){
        //         $('input[name="province_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="regency_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="district_coverage_checkbox"]').prop('checked',true);
        //         $('input[name="province_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $('input[name="regency_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $('input[name="district_coverage_checkbox"]').siblings().removeClass('d-none');
        //         $(this).siblings().removeClass('d-none');
        //     }else{
        //         $(this).siblings().addClass('d-none');
        //     }
        // })

        function getProvince(){
            $.ajax({
                url: "{{ route('admin/getProvince') }}",
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var province_array = ["<option></option>"];
                    var province = resultData['data'];
                    console.log(province);
                    for (let b = 0; b < province.length; b++) {
                        province_array.push("<option value='"+province[b]['provinsi_id']+"'>"+province[b]['provinsi_name']+"</option>");                        
                    }
                    $('select[name="province_coverage[]"]').html(province_array);
                }
            });
        }

        function getMappingCoverage(province_id = null, regency_id = null, district_id = null, village_id = null){
            $.ajax({
                url: '/admin/getMappingCoverage',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'province' : province_id,
                    'regency' : regency_id,
                    'district': district_id,
                    'village': village_id,
                    'organization_id': '{{ Request::get("organization") }}'
                },
                async: false,
                success: function(resultData){
                    console.log(province_id);
                    console.log(resultData);
                    var html = "";
                    var data = resultData['data'];
                    for (let b = 0; b < data.length; b++) {
                        html += '<tr>';
                        
                            html += '<td><a class="deleteMappingButton" role="button"><i class="fa fa-times-circle color-red ico-size-td"></i></a></td>';
                        html += '<td>'
                            html +=  '<span>'+data[b]['province_name']+'</span><input name="province_coverage_id[]" type="hidden" value='+data[b]['province_id']+'></input>'
                        html += '</td>'
                        html += '<td>'
                            html +=  '<span>'+data[b]['regency_name']+'</span><input name="regency_coverage_id[]" type="hidden" value='+data[b]['regency_id']+'></input>'
                        html += '</td>'
                        html += '<td>'
                            html +=  '<span>'+data[b]['district_name']+'</span><input name="district_coverage_id[]" type="hidden" value='+data[b]['district_id']+'></input>'
                        html += '</td>'
                        html += '<td>'
                            html +=  '<span>'+data[b]['village_name']+'</span><input name="village_coverage_id[]" type="hidden" value='+data[b]['village_id']+'></input>'
                        html += '</td>'
                        html += '<td>'
                                html +=  '<span>'+data[b]['postal_code']+'</span>'
                            html += '</td>'
                        html += '</tr>';
                    }

                    $('table#locationCoverageView tbody').html(html);

                    $('.deleteMappingButton').click(function(){
                        index = $('.deleteMappingButton').index(this);
                        $('#locationCoverageView tbody tr').eq(index).remove();
                    })

                    if(data.length == 0){
                        $('#submitFilterButton').attr('disabled',true);
                    }else{
                        $('#submitFilterButton').attr('disabled',false);
                    }
                }
                
            });
        }

        function getRegency(data){
            $.ajax({
                url: "{{ route('admin/getRegency') }}",
                type: "POST",
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var regency_array = ["<option></option>"];
                    var regency = resultData['data'];
                    console.log(resultData);
                    if(regency.length > 0){
                        for (let b = 0; b < regency.length; b++) {
                            regency_array.push("<option value='"+regency[b]['kabupaten_kota_id']+"'>"+regency[b]['kabupaten_kota_name']+"</option>");                        
                        }
                        $('select[name="regency_coverage[]"]').html(regency_array);
                    }
                }
            });
        }

        function getDistrict(data){
            $.ajax({
                url: "{{ route('admin/getDistrict') }}",
                type: "POST",
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var district_array = ["<option></option>"];
                    var district = resultData['data'];
                    console.log(resultData);
                    if(district.length > 0){
                        for (let b = 0; b < district.length; b++) {
                            district_array.push("<option value='"+district[b]['kecamatan_id']+"'>"+district[b]['kecamatan_name']+"</option>");                        
                        }
                        $('select[name="district_coverage[]"]').html(district_array);
                    }
                }
            });
        }

        function getVillage(data){
            $.ajax({
                url: "{{ route('admin/getVillage') }}",
                type: "POST",
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var village_array = ["<option></option>"];
                    var village = resultData['data'];
                    console.log(resultData);
                    if(village.length > 0){
                        for (let b = 0; b < village.length; b++) {
                            village_array.push("<option value='"+village[b]['kelurahan_desa_id']+"'>"+village[b]['kelurahan_desa_name']+"</option>");                        
                        }
                        $('select[name="village_coverage[]"]').html(village_array);
                    }
                }
            });
        }

        $('#deleteAllMappingButton').click(function(){
            $('table#locationCoverageView tbody').empty();
            $('table#locationCoverageView tbody').html('<tr><td colspan="6" style="text-align:center">{{ __("page.no_data") }}</td></tr>');
            province_coverage = [];
            regency_coverage  = [];
            district_coverage  = [];
            village_coverage  = [];
            $('select[name="province_coverage[]"]').val(province_coverage).trigger("change");
            $('select[name="regency_coverage[]"]').html("");
            $('select[name="district_coverage[]"]').html("");
            $('select[name="village_coverage[]"]').html("");
        });
        @endif
        
    </script>
    @endif
@endsection
