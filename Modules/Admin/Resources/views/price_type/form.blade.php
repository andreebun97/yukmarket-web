@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_price_type') : __('page.edit_price_type')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.price_type') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_price_type') : __('page.edit_price_type') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_price_type') : __('page.edit_price_type') }}</h4>

                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            @php $route = route('admin/price_type/form'); @endphp
                                        	<div class="clearfix"></div>

                                         		<form method="POST" id="priceTypeForm" action="{{ $route }}" class="form-style">
                                                    <div class="col-md-12 no-padding">
                                                        <div class="panel-body">
                                                            @csrf
                                                            @if(Request::get('id') != null)
                                                                <input type="hidden" name="price_type_id" value="{{ Request::get('id') }}">
                                                            @endif
                                                            <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                                <div class="col-md-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="price_type_name">{{ __('field.price_type_name') }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="price_type_name" class="form-control @error('price_type_name') is-invalid @enderror" value="{{ Request::get('id') == null ? old('price_type_name') : $price_type_by_id['price_type_name'] }}">
                                                                        @error('price_type_name')
                                                                            <span class="invalid-feedback">{{ $errors->first('price_type_name') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="form-group">
                                                                        <label for="price_type_description">{{ __('field.price_type_description') }} <span class="required-label">*</span></label>
                                                                        <textarea name="price_type_description" id="price_type_description" class="form-control" style="resize:none;height:192px">@isset($price_type_by_id){{ $price_type_by_id['price_type_desc']}}@endisset</textarea>
                                                                        @error('price_type_description')
                                                                            <span class="invalid-feedback">{{ $errors->first('price_type_description') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                @if(Request::input('id') != null)
                                                <div class="panel-heading border-bottom-grey">
                                                	<h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.price_type_detail') }}</h4>
		                                            <div class="clearfix"></div>
		                                        </div>
                                                <form id="priceTypeDetailForm" action="{{ $route }}" method="POST">
		                                            <div class="col-md-12 no-padding">
                                                        <div class="panel-body">
		                                                    @csrf
		                                                    <input type="hidden" name="parent_id" value="{{ Request::get('id') }}">
		                                                    @if($price_type_by_parent_id['price_type_id'] != null)
		                                                        <input type="hidden" name="price_type_id" value="{{ $price_type_by_parent_id['price_type_id'] }}">
		                                                    @endif
		                                                    <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
		                                                        <div class="col-md-12 no-padding" style="display:none">
		                                                            <div class="form-group">
		                                                                <label for="price_type_name">{{ __('field.price_type_name') }} <span class="required-label">*</span></label>
		                                                                <input type="text" name="price_type_name" class="form-control @error('price_type_name') is-invalid @enderror" value="{{ Request::get('id') == null ? old('price_type_name') : $price_type_by_id['price_type_name'] }}">
		                                                                @error('price_type_name')
		                                                                    <span class="invalid-feedback">{{ $errors->first('price_type_name') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-xs-12 no-padding" style="display:none">
		                                                            <div class="form-group">
		                                                                <label for="price_type_description">{{ __('field.price_type_description') }} <span class="required-label">*</span></label>
		                                                                <textarea name="price_type_description" id="price_type_description" class="form-control" style="resize:none;height:192px">@isset($price_type_by_id){{ $price_type_by_id['price_type_desc']}}@endisset</textarea>
		                                                                @error('price_type_description')
		                                                                    <span class="invalid-feedback">{{ $errors->first('price_type_description') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <!-- {{ $price_type_by_parent_id['discount_type'] }} -->
		                                                        <div class="col-md-12 no-padding">
		                                                            <div class="form-group">
		                                                                <label for="discount_type">{{ __('field.discount_type') }} <span class="required-label">*</span></label>
		                                                                <select name="discount_type" id="discount_type" class="form-control single-select">
		                                                                    <option value=""></option>
		                                                                    <option value="0" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['discount_type'] == 0) selected @endif>{{ __('field.custom') }}</option>
		                                                                    <option value="1" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['discount_type'] == 1) selected @endif>{{ __('field.percentage') }}</option>
		                                                                    <option value="2" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['discount_type'] == 2) selected @endif>{{ __('field.absolute') }}</option>
		                                                                </select>
		                                                                @error('discount_type')
		                                                                    <span class="invalid-feedback">{{ $errors->first('discount_type') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-md-12 no-padding">
		                                                            <div class="form-group">
		                                                                <label for="applicable_on">{{ __('field.applicable_on') }} <span class="required-label">*</span></label>
		                                                                <select name="applicable_on" id="applicable_on" class="form-control single-select">
		                                                                    <option value=""></option>
		                                                                    <option value="1" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 1) selected @endif>{{ __('page.all') }}</option>
		                                                                    <option value="2" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 2) selected @endif>{{ __('page.category') }}</option>
		                                                                    <option value="3" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 3) selected @endif>{{ __('page.product') }}</option>
		                                                                </select>
		                                                                @error('applicable_on')
		                                                                    <span class="invalid-feedback">{{ $errors->first('applicable_on') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-md-12 no-padding applicable_on_all {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 1 ? '' : 'd-none' }}">
		                                                            <div class="form-group">
		                                                                <label for="discount_type">{{ __('field.starts_at') }} <span class="required-label">*</span></label>
		                                                                <input type="datetime-local" name="starts_at" class="form-control" value="{{ Request::get('id') == null ? old('starts_at') : str_replace(' ','T',$price_type_by_parent_id['starts_at']) }}">
		                                                                @error('starts_at')
		                                                                    <span class="invalid-feedback">{{ $errors->first('starts_at') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-md-12 no-padding applicable_on_all {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 1 ? '' : 'd-none' }}">
		                                                            <div class="form-group">
		                                                                <label for="discount_type">{{ __('field.expires_at') }} <span class="required-label">*</span></label>
		                                                                <input type="datetime-local" name="expires_at" class="form-control" value="{{ Request::get('id') == null ? old('expires_at') : str_replace(' ','T',$price_type_by_parent_id['expires_at']) }}">
		                                                                @error('expires_at')
		                                                                    <span class="invalid-feedback">{{ $errors->first('expires_at') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-md-12 no-padding applicable_on_all {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 1 ? '' : 'd-none' }}">
		                                                            <div class="form-group">
		                                                                <label for="discount_type">{{ __('field.discount_amount') }} <span class="required-label">*</span></label>
		                                                                <input type="number" name="discount_amount" class="form-control" @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 1) value="{{ $price_type_by_parent_id['rate'] }}" @endif>
		                                                                @error('discount_amount')
		                                                                    <span class="invalid-feedback">{{ $errors->first('discount_amount') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <!-- <div class="col-md-12 no-padding applied_product {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 3 ? '' : 'd-none' }}">
		                                                            <div class="form-group">
		                                                                <label for="applied_product">{{ __('field.applied_product') }} <span class="required-label">*</span></label>
		                                                                <select name="applied_product[]" id="applied_product" class="form-control multi-select" multiple>
		                                                                    <option value=""></option>
		                                                                </select>
		                                                                @error('applied_product')
		                                                                    <span class="invalid-feedback">{{ $errors->first('applied_product') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div>
		                                                        <div class="col-md-12 no-padding applied_category {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 2 ? '' : 'd-none' }}">
		                                                            <div class="form-group">
		                                                                <label for="applied_category[]">{{ __('field.applied_category') }} <span class="required-label">*</span></label>
		                                                                <select name="applied_category[]" id="applied_category" class="form-control multi-select" multiple>
		                                                                    <option value=""></option>
		                                                                </select>
		                                                                @error('applied_category')
		                                                                    <span class="invalid-feedback">{{ $errors->first('applied_category') }}</span>
		                                                                @enderror
		                                                            </div>
		                                                        </div> -->
		                                                    </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                                <div class="discount_type_detail {{ isset($price_type_by_parent_id) && ($price_type_by_parent_id['applicable_on'] == 2 || $price_type_by_parent_id['applicable_on'] == 3 )? '' : 'd-none' }} table-responsive">
                                                                    <table id="discount_type_detail_table" class="table display w-100 table-brown total-transaksi no-margin">
                                                                        <thead class="bg-darkgrey">
                                                                            <tr>
                                                                                <th class="field_name">{{ __('field.category_name') }}</th>
                                                                                <th>{{ __('field.voucher_value_type') }}</th>
                                                                                <th>{{ __('field.voucher_amount') }}</th>
                                                                                <th>{{ __('field.starts_at') }}</th>
                                                                                <th>{{ __('field.expires_at') }}</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @if( isset($price_type_by_parent_id) && $price_type_by_id['price_type_id'] != null)
                                                                            @if($price_type_by_parent_id['applicable_on'] == 2)
                                                                            @for($b = 0; $b < count($category_price_type_by_parent_id); $b++)
                                                                                <tr>
                                                                                    <td><select name="applied_items[]" class="form-control single-select" id=""></select></td>
                                                                                    <td>
                                                                                        <select name="multiple_discount_type[]" class="form-control single-select" style="width:350px">
                                                                                            <option value=""></option>
                                                                                            <option value="1" {{ $category_price_type_by_parent_id[$b]['discount_type'] == 1 ? 'selected' : '' }}>{{ __('field.percentage') }}</option>
                                                                                            <option value="2" {{ $category_price_type_by_parent_id[$b]['discount_type'] == 2 ? 'selected' : '' }}>{{ __('field.absolute') }}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" name="multiple_discount_amount[]" style="width:150px" class="form-control" value="{{ $category_price_type_by_parent_id[$b]['rate'] }}" required="true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="datetime-local" name="multiple_starts_at[]" style="width:220px" class="form-control" value="{{ str_replace(' ','T',$category_price_type_by_parent_id[$b]['starts_at']) }}" required="true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="datetime-local" name="multiple_expires_at[]" style="width:220px" class="form-control" value="{{ str_replace(' ','T',$category_price_type_by_parent_id[$b]['expires_at']) }}" required="true">
                                                                                    </td>
                                                                                </tr>
                                                                            @endfor
                                                                            <tr>
                                                                                <td colspan="5"><button class="btn btn-orange" id="addDiscountTypeColumnButton" type="button"><i class="fa fa-plus"></i> Tambah Produk/Kategori</button></td>
                                                                            </tr>
                                                                            @elseif($price_type_by_parent_id['applicable_on'] == 3)
                                                                            @for($b = 0; $b < count($product_price_type_by_parent_id); $b++)
                                                                                <tr>
                                                                                    <td><select name="applied_items[]" class="form-control single-select" id=""></select></td>
                                                                                    <td>
                                                                                        <select name="multiple_discount_type[]" class="form-control single-select">
                                                                                            <option value=""></option>
                                                                                            <option value="1" {{ $product_price_type_by_parent_id[$b]['discount_type'] == 1 ? 'selected' : '' }}>{{ __('field.percentage') }}</option>
                                                                                            <option value="2" {{ $product_price_type_by_parent_id[$b]['discount_type'] == 2 ? 'selected' : '' }}>{{ __('field.absolute') }}</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" name="multiple_discount_amount[]" style="width:150px" class="form-control" value="{{ $product_price_type_by_parent_id[$b]['rate'] }}" required="true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="datetime-local" name="multiple_starts_at[]" style="width:220px" class="form-control" value="{{ str_replace(' ','T',$product_price_type_by_parent_id[$b]['starts_at']) }}" required="true">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="datetime-local" name="multiple_expires_at[]" style="width:220px" class="form-control" value="{{ str_replace(' ','T',$product_price_type_by_parent_id[$b]['expires_at']) }}" required="true">
                                                                                    </td>
                                                                                </tr>
                                                                                @endfor
                                                                                <tr>
                                                                                    <td colspan="5"><button class="btn btn-orange" id="addDiscountTypeColumnButton" type="button"><i class="fa fa-plus"> Tambah Produk/Kategori</i></button></td>
                                                                                </tr>
                                                                            @endif
                                                                        @else
                                                                            <tr>
                                                                                <td>
                                                                                    <select name="applied_items[]" class="form-control single-select" id=""></select>
                                                                                </td>
                                                                                <td>
                                                                                    <select name="multiple_discount_type[]" class="form-control single-select" style="width:350px">
                                                                                        <option value=""></option>
                                                                                        <option value="1">{{ __('field.percentage') }}</option>
                                                                                        <option value="2">{{ __('field.absolute') }}</option>
                                                                                    </select>
                                                                                </td>
                                                                                <td><input type="text" name="multiple_discount_amount[]" style="width:150px" class="form-control" required="true"></td>
                                                                                <td><input type="datetime-local" name="multiple_starts_at[]" style="width:220px" class="form-control" required="true"></td>
                                                                                <td><input type="datetime-local" name="multiple_expires_at[]" style="width:220px" class="form-control" required="true"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="5"><button class="btn btn-orange" id="addDiscountTypeColumnButton" type="button"><i class="fa fa-plus"> Tambah Produk/Kategori</i></button></td>
                                                                            </tr>
                                                                        @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-12 no-padding minimum_order_div {{ isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 3 ? '' : 'd-none' }}">
                                                                    <div class="form-group">
                                                                        <label for="minimum_order">{{ __('field.minimum_order') }} <span class="required-label">*</span></label>
                                                                        <input type="number" name="minimum_order" id="minimum_order" class="form-control" @isset($price_type_by_parent_id) value="{{ $price_type_by_parent_id['min_purchased_qty'] }}" @endisset>
                                                                        @error('minimum_order')
                                                                            <span class="invalid-feedback">{{ $errors->first('minimum_order') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
	                                                    </div>
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                    <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                                @endif
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var item_array = [];
        var today = new Date();
        var todayDate = today.getFullYear()+'-'+((today.getMonth()+1) < 10 ? ("0" + (today.getMonth()+1)) : (today.getMonth()+1))+'-'+(today.getDate() < 10 ? ("0"+today.getDate()) : today.getDate());
        var todayTime = (today.getHours() < 10 ? ("0"+today.getHours()) : today.getHours()) + ":" + (today.getMinutes() < 10 ? ("0"+today.getMinutes()) : today.getMinutes()) + ":" + (today.getSeconds() < 10 ? ("0" + today.getSeconds()) : today.getSeconds());
        var dateTime = todayDate+'T'+todayTime;

        $('input[name="multiple_starts_at[]"]').change(function(){
            var index = $('input[name="multiple_starts_at[]"]').index(this);
            var chosenDateTemp = new Date($(this).val());
            var chosenDate = chosenDateTemp.getFullYear()+'-'+((chosenDateTemp.getMonth()+1) < 10 ? ("0" + (chosenDateTemp.getMonth()+1)) : (chosenDateTemp.getMonth()+1))+'-'+(chosenDateTemp.getDate() < 10 ? ("0"+chosenDateTemp.getDate()) : chosenDateTemp.getDate());
            var chosenTime = (chosenDateTemp.getHours() < 10 ? ("0"+chosenDateTemp.getHours()) : chosenDateTemp.getHours()) + ":" + (chosenDateTemp.getMinutes() < 10 ? ("0"+chosenDateTemp.getMinutes()) : chosenDateTemp.getMinutes()) + ":" + (chosenDateTemp.getSeconds() < 10 ? ("0" + chosenDateTemp.getSeconds()) : chosenDateTemp.getSeconds());
            var fullChosenDateTime = chosenDate+'T'+chosenTime;
            console.log(index);
            $('input[name="multiple_starts_at[]"]:eq('+index+')').attr('min',fullChosenDateTime);
            $('input[name="multiple_expires_at[]"]:eq('+index+')').attr('min',fullChosenDateTime);
            $('input[name="multiple_starts_at[]"]:eq('+index+')').val(fullChosenDateTime);
            $('input[name="multiple_expires_at[]"]:eq('+index+')').val(fullChosenDateTime);
        });

        jQuery.validator.addMethod("greaterThan",
        function(value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }

            return isNaN(value) && isNaN($(params).val())
                || (Number(value) > Number($(params).val()));
        },'Must be greater than {0}.');

        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="price_type_name"], textarea[name="price_type_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        $('button#addDiscountTypeColumnButton').click(function(){
            var html = "";
            html += '<tr>';
            html += '<td>';
            html += '<select name="applied_items[]" class="form-control single-select" id=""></select>'
            html += '</td>'
            html += '<td>'
            html += '<select name="multiple_discount_type[]" class="form-control single-select" style="width:350px">'
            html += '<option value=""></option>'
            html += '<option value="1">{{ __('field.percentage') }}</option>'
            html += '<option value="2">{{ __('field.absolute') }}</option>'
            html += '</select>'
            html += '</td>'
            html += '<td><input type="text" name="multiple_discount_amount[]" style="width:150px" class="form-control" required="true"></td>'
            html += '<td><input type="datetime-local" name="multiple_starts_at[]" style="width:220px" class="form-control" required="true"></td>'
            html += '<td><input type="datetime-local" name="multiple_expires_at[]" style="width:220px" class="form-control" required="true"></td>'
            html += '</tr>'
            $(html).insertBefore('#discount_type_detail_table tbody tr:last-child');
            if($('select[name="applicable_on"]').val() == 2){
                getCategory("last");
            }else if($('select[name="applicable_on"]').val() == 3){
                getProduct("last");
            }
            $('select[name="multiple_discount_type[]"]:last-child').val($('select[name="discount_type"]').val()).trigger('change');
            $('.single-select:last-child').select2();
            $('input[name="multiple_starts_at[]"]:last-child').attr('min',dateTime);
            $('input[name="multiple_expires_at[]"]:last-child').attr('min',dateTime);
            $('input[name="multiple_starts_at[]"]:last-child').val(dateTime);
            $('input[name="multiple_expires_at[]"]:last-child').val(dateTime);
            $('input[name="multiple_starts_at[]"]').change(function(){
                var index = $('input[name="multiple_starts_at[]"]').index(this);
                var chosenDateTemp = new Date($(this).val());
                var chosenDate = chosenDateTemp.getFullYear()+'-'+((chosenDateTemp.getMonth()+1) < 10 ? ("0" + (chosenDateTemp.getMonth()+1)) : (chosenDateTemp.getMonth()+1))+'-'+(chosenDateTemp.getDate() < 10 ? ("0"+chosenDateTemp.getDate()) : chosenDateTemp.getDate());
                var chosenTime = (chosenDateTemp.getHours() < 10 ? ("0"+chosenDateTemp.getHours()) : chosenDateTemp.getHours()) + ":" + (chosenDateTemp.getMinutes() < 10 ? ("0"+chosenDateTemp.getMinutes()) : chosenDateTemp.getMinutes()) + ":" + (chosenDateTemp.getSeconds() < 10 ? ("0" + chosenDateTemp.getSeconds()) : chosenDateTemp.getSeconds());
                var fullChosenDateTime = chosenDate+'T'+chosenTime;
                console.log(index);
                $('input[name="multiple_starts_at[]"]:eq('+index+')').attr('min',fullChosenDateTime);
                $('input[name="multiple_expires_at[]"]:eq('+index+')').attr('min',fullChosenDateTime);
                $('input[name="multiple_starts_at[]"]:eq('+index+')').val(fullChosenDateTime);
                $('input[name="multiple_expires_at[]"]:eq('+index+')').val(fullChosenDateTime);
            });
            // $('')
            // console.log("test");
            // $('select[name="applied_items[]"]').change(function(){
            //     var item = $(this).val();
            //     console.log(item);
            //     item_array.push(item);
            //     console.log(item_array);
            // });
        });

        // $('select[name="applied_items[]"]').change(function(){
        //     console.log($(this).val());
        //     item_array.push($(this).val());
        //     console.log(item_array);
        // });

        @if(Request::get('id') != null)
        $('select[name="applicable_on"]').change(function(){
            // console.log($('table#discount_type_detail_table tbody').length);
            // if($('table#discount_type_detail_table tbody tr').length > 0){
            //     $('table#discount_type_detail_table tbody').empty();
            //     // $('table#discount_type_detail_table').addClass('d-none');
            // }
            if($(this).val() == "" || $(this).val() == "1"){
                // if(!$('.applied_product').hasClass('d-none')){
                //     $('.applied_product').addClass('d-none');
                // }

                // if(!$('.applied_category').hasClass('d-none')){
                //     $('.applied_category').addClass('d-none');
                // }

                $('.applicable_on_all').removeClass('d-none');
                $('.minimum_order_div').addClass('d-none');
                $('.discount_type_detail').addClass('d-none');
                // remove required true
                $('input[name="multiple_discount_amount[]"]').each(function () {
                    $(this).removeAttr("required");
                });

            }else{
                var discount_type = $('select[name="discount_type"]').val() == 0 ? 1 : $('select[name="discount_type"]').val();
                $('.applicable_on_all').addClass('d-none');
                $('select[name="multiple_discount_type[]"]').val(discount_type).trigger('change');
                $('input[name="discount_amount"]').val("");
                // $('input[name="starts_at"]').val("");
                // $('input[name="expires_at"]').val("");
                if($(this).val() == "2"){
                    // $('.applied_product').addClass('d-none');
                    // $('.applied_category').removeClass('d-none');
                    // $('select[name="applied_category[]"]').html([]);
                    // $('select[name="applied_product[]"]').html([]);
                    $('.minimum_order_div').addClass('d-none');
                    $('.discount_type_detail').removeClass('d-none');
                    getCategory();

                    // add required true
                    $('input[name="multiple_discount_amount[]"]').each(function () {
                        $(this).attr('required', 'true');
                    });

                }else{
                    // $('.applied_product').removeClass('d-none');
                    // $('.applied_category').addClass('d-none');
                    // $('select[name="applied_category[]"]').html([]);
                    // $('select[name="applied_product[]"]').html([]);
                    $('.minimum_order_div').removeClass('d-none');
                    $('.discount_type_detail').removeClass('d-none');
                    getProduct();

                    // add required true
                    $('input[name="multiple_discount_amount[]"]').each(function () {
                        $(this).attr('required', 'true');
                    });

                }
            }
        });

        // var dropdown_length = 0;
        var product_detail_array = [];
        var multiple_discount_amount = 0;
        // $('select[name="applied_product[]"], select[name="applied_category[]"]').change(function(){
        //     var dropdown_length = $(this).find('option:selected').length;
        //     if(dropdown_length > 0){
        //         $('.discount_type_detail').removeClass('d-none');
        //     }else{
        //         $('.discount_type_detail').addClass('d-none');
        //     }
        //     // console.log($(this).find('option:selected').text());
        //     var dropdown_text = "";
        //     $(this).find('option:selected').each(function(){
        //         // console.log($(this).text());
        //         dropdown_text += "<tr><td class='product_name'>"+$(this).text()+"</td>"+"<td><select name='multiple_discount_type[]' class='form-control' style='width:150px'><option value=''></option><option value='1'>{{ __('field.percentage') }}</option><option value='2'>{{ __('field.absolute') }}</option></select></td>"+"<td><input type='text' name='multiple_discount_amount[]' style='width:150px' class='form-control'></td>"+"<td><input type='datetime-local' required='true' name='multiple_starts_at[]' style='width:250px' class='form-control'></td>"+"<td><input type='datetime-local' name='multiple_expires_at[]' style='width:250px' class='form-control'></td></tr>";
        //         console.log(dropdown_text);
        //         // if($('select[name="multiple_discount_type[]"]').val() > 0){
        //             // }
        //     });

        //     $('table#discount_type_detail_table tbody').html(dropdown_text);
        //     console.log($('select[name="discount_type"]').val());
        //     $("select[name='multiple_discount_type[]']").val($('select[name="discount_type"]').val());
        //     // dropdown_length += $(this).length;
        //     // console.log(dropdown_length);
        //     // $('input[name="multiple_discount_amount[]"]').change(function(){
        //     //     console.log($(this).val());
        //     //     console.log($('input[name="multiple_discount_amount[]"]').index(this));
        //     // });
        //     multiple_discount_amount = $('select[name="applied_category[]"]').find('option:selected').length;
        //     // console.log(multiple_discount_amount);
        //     // for (let k = 0; k < multiple_discount_amount.length; k++) {
        //         // }
        //     // $('input[name="multiple_discount_amount[]"]').attr('required',true);
        //     var today = new Date();
        //     var date = today.getFullYear()+'-'+((today.getMonth()+1) < 10 ? ("0" + (today.getMonth()+1)) : (today.getMonth()+1))+'-'+(today.getDate() < 10 ? ("0"+today.getDate()) : today.getDate());
        //     var time = (today.getHours() < 10 ? ("0"+today.getHours()) : today.getHours()) + ":" + (today.getMinutes() < 10 ? ("0"+today.getMinutes()) : today.getMinutes()) + ":" + (today.getSeconds() < 10 ? ("0" + today.getSeconds()) : today.getSeconds());
        //     var dateTime = date+'T'+time;
        //     console.log(dateTime);
        //     $('input[name="multiple_starts_at[]"]').val(dateTime);
        //     $('input[name="multiple_expires_at[]"]').val(dateTime);
        //     $('input[name="multiple_starts_at[]"]').attr('min',dateTime);
        //     $('input[name="multiple_expires_at[]"]').attr('min',dateTime);
        // });

        // console.log("{{ $category_list }}");

        $('select[name="discount_type"]').change(function(){
            if($(this).val() > 0){
                $("select[name='multiple_discount_type[]']").val($(this).val());
            }
        });

        // console.log("{{ $price_type_by_parent_id == null && $price_type_by_parent_id['price_type_id'] == null ? 0 : 1 }}");

        @if($price_type_by_parent_id == null)
        $('input[name="starts_at"]').val(dateTime);
        $('input[name="expires_at"]').val(dateTime);
        $('input[name="multiple_starts_at[]"]').val(dateTime);
        $('input[name="multiple_expires_at[]"]').val(dateTime);
        $('input[name="starts_at"]').attr('min',dateTime);
        $('input[name="expires_at"]').attr('min',dateTime);
        $('input[name="multiple_starts_at[]"]').attr('min',dateTime);
        $('input[name="multiple_expires_at[]"]').attr('min',dateTime);
        @endif

        @if(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 2)
            var category_list = "{{ $category_list }}";
            getCategory();
            category_list = category_list.split(',');
            for (let b = 0; b < category_list.length; b++) {
                // const element = array[b];
                $('select[name="applied_items[]"]').eq(b).val(category_list[b]).trigger('change');
            }
        @elseif(isset($price_type_by_parent_id) && $price_type_by_parent_id['applicable_on'] == 3)
            getProduct();
            var product_list = "{{ $product_list }}";
            console.log(product_list);
            product_list = product_list.split(',');
            for (let b = 0; b < product_list.length; b++) {
                // const element = array[b];
                $('select[name="applied_items[]"]').eq(b).val(product_list[b]).trigger('change');
            }
            // $('select[name="applied_items[]"]').val(product_list.split(','));
        @endif

        function getCategory(last = null){
            $.ajax({
                url: "{{ route('admin/category/get') }}",
                type: "GET",
                async: false,
                success: function(resultData){
                    var data = resultData['data'];
                    var category_array = [];
                    for (let k = 0; k < data.length; k++) {
                        var obj = "<option value='"+data[k]['category_id']+"'>"+data[k]['category_name']+"</option>";
                        category_array.push(obj);
                    }
                    console.log(category_array);
                    if(last != null){
                        $('select[name="applied_items[]"]:last-child').html(category_array);
                    }else{
                        $('select[name="applied_items[]"]').html(category_array);
                    }
                }
            });
        }

        function getProduct(last = null){
            $.ajax({
                url: "{{ route('admin/product/get') }}",
                type: "GET",
                async: false,
                success: function(resultData){
                    var data = resultData['data'];
                    var product_array = [];
                    for (let k = 0; k < data.length; k++) {
                        var obj = "<option value='"+data[k]['prod_id']+"'>"+data[k]['prod_name']+"</option>";
                        product_array.push(obj);
                    }
                    console.log(product_array);
                    if(last != null){
                        $('select[name="applied_items[]"]:last-child').html(product_array);
                    }else{
                        $('select[name="applied_items[]"]').html(product_array);
                    }
                }
            });
        }
        @endif

        var validationRules = {
            price_type_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            price_type_description: {
                required: true
            }
        };
        var validationMessages = {
            price_type_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.price_type_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.price_type_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.price_type_name')]) }}"
            },
            price_type_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.price_type_description')]) }}"
            }
        }

        $('form#priceTypeForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        @if(Request::get('id') != null)
        var priceTypeDetailErrorRules = {
            price_type_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            price_type_description: {
                required: true
            },
            applicable_on: {
                required: true
            },
            discount_type: {
                required: true
            },
            starts_at: {
                required: function(element) {
                    return $("select[name='applicable_on']").val() == 1;
                }
            },
            expires_at: {
                required: function(element) {
                    return $("select[name='applicable_on']").val() == 1;
                }
            },
            minimum_order: {
                required: function(element) {
                    return $("select[name='applicable_on']").val() == 3;
                }
            },
            discount_amount: {
                required: function(element){
                    return ($("select[name='applicable_on']").val() == 1 && $("select[name='discount_type']").val() != 0);
                },
                max: function(element){
                    if($("select[name='applicable_on']").val() == 1 && $("select[name='discount_type']").val() == 1){
                        return 100;
                    }
                },
                min: function(element){
                    if($("select[name='applicable_on']").val() == 1 && $("select[name='discount_type']").val() == 1){
                        return 0;
                    }
                },
            }
        };
        var priceTypeDetailErrorMessages = {
            price_type_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.price_type_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.price_type_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.price_type_name')]) }}"
            },
            price_type_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.price_type_description')]) }}"
            },
            applicable_on: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.applicable_on')]) }}"
            },
            discount_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.discount_type')]) }}"
            },
            starts_at: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.starts_at')]) }}"
            },
            expires_at: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.expires_at')]) }}"
            },
            minimum_order: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_order')]) }}"
            }
        }

        function getHtmlDateString(date) {
            var dd = date.getDate();
            var mm = date.getMonth()+1; //January is 0!
            var yyyy = date.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }

            return yyyy+'-'+mm+'-'+dd;
        }

        $('form#priceTypeDetailForm').validate({
            errorClass: 'invalid-feedback',
            rules: priceTypeDetailErrorRules,
            messages: priceTypeDetailErrorMessages,
            ignore: [],
            submitHandler: function(form){
                var applicable_on = $('select[name="applicable_on"]').val();
                var flag = 0;
                console.log($('input[name="multiple_discount_amount[]"]').val().length);
                console.log(multiple_discount_amount);
                if(applicable_on != 1){
                    $('input[name="multiple_discount_amount[]"]').each(function(){
                        if( $(this).val() == "" ){
                            alert('Please fill all the fields');
                            flag = 1;
                        }
                    });

                    if(flag == 0){
                        form.submit();
                        showLoader();
                    }
                }else{
                    form.submit();
                    showLoader();
                }
            }
        });
        @endif

        clickCancelButton('{{ route("admin/price_type") }}')

        exitPopup('{{ route("admin/price_type") }}');

        $('#product_variant_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('input[name="product_picture"]').click();
        });
    </script>
    @endif
@endsection
