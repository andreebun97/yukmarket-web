@extends('admin::layouts.master')

@section('title',__('menubar.profile'))

@section('content')
    <div class="profile-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin')
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-user position-left"></i>Profile</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">Profile</h4>
                                        
                                            <div class="clearfix"></div>
                                        </div>
                                    
                                       
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        <div class="col-md-6 col-sm-12 ">
                                                            <ul class="nav nav-tabs text-center">
                                                                <li class="nav-item col-md-6 col-sm-6 col-xs-6 no-padding float-left">
                                                                    <a class="nav-link text-center active" href="{{ route('admin/profile') }}">{{ __('menubar.profile') }}</a>
                                                                </li>
                                                                <li class="nav-item col-md-6 col-sm-6 col-xs-6 no-padding float-left">
                                                                    <a class="nav-link text-center" href="{{ route('admin/password') }}">{{ __('menubar.change_password') }}</a>
                                                                </li>
                                                            </ul>
                                                            <div id="profile_picture_div" >
                                                                <img src="{{ asset($user['profile_picture']) }}" alt=""  onError="this.onerror=null;this.src='{{ asset('img/img-avatar.svg') }}';">
                                                                <div class="clearfix"></div>
                                                                <div class="info">
                                                                    <i class="fa fa-info-circle"></i>
                                                                    {{ __('notification.click_profile_picture') }}
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <form action="{{ route('admin/profile/change') }}" id="profileForm" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="file" name="profile_picture" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="user_name">{{ __('field.user_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ $logged_in_user['user_name'] }}" autocomplete="off">
                                                                    @error('user_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="user_email">{{ __('field.user_email') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_email') is-invalid @enderror" name="user_email" value="{{ $logged_in_user['user_email'] }}" autocomplete="off">
                                                                    @error('user_email')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_email') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="user_phone_number">{{ __('field.user_phone_number') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('user_phone_number') is-invalid @enderror" name="user_phone_number" value="{{ $logged_in_user['user_phone_number'] }}" autocomplete="off">
                                                                    @error('user_phone_number')
                                                                        <span class="invalid-feedback">{{ $errors->first('user_phone_number') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-3 col-xs-6 no-padding float-right">
                                                                    <button type="submit" class="btn btn-orange btn-block no-margin">{{ __('page.submit') }}</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $.validator.addMethod("isEmail", function(value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        }, "Email format please");

        $('input[name="user_name"], input[name="user_email"], input[name="user_phone_number"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            user_name: {
                required: true,
                minlength: 4,
                alpha: true
            },
            user_email: {
                required: true,
                minlength: 10,
                isEmail: true
            },
            user_phone_number: {
                required: true,
                minlength: 10,
                maxlength: 15,
                digits: true
            }
        };
        var validationMessages = {
            user_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_name')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_name'), 'min' => 4]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.user_name')]) }}"
            },
            user_email: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_email')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_email'), 'min' => 10]) }}",
                isEmail: "{{ __('validation.email',['attribute' => __('validation.attributes.user_email')]) }}"
            },
            user_phone_number: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.user_phone_number')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.user_phone_number'), 'min' => 10]) }}",
                maxlength: "{{ __('validation.max.string',['attribute' => __('validation.attributes.user_phone_number'), 'max' => 15]) }}",
                digits: "{{ __('validation.numeric',['attribute' => __('validation.attributes.user_phone_number')]) }}"
            }
        };

        $('form#profileForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile_picture_div img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#profile_picture_div img').click(function(e){
            e.preventDefault();
            $('input[name="profile_picture"]').click();
        });

        $('input[name="profile_picture"]').change(function(e){
            var fileName = e.target.files[0].name;
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });
    </script>
@endsection
