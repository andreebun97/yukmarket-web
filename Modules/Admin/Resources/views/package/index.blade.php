@extends('admin::layouts.master')

@section('title',__('menubar.package'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <!-- <li><i class="fa fa-book position-left"></i>{{ __('menubar.master') }}</li> -->
                                <li class="active text-bold ">{{ __('menubar.package') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.package') }}</h4>
                                        @if($accessed_menu == 1)
                                       <!--  <div class="float-right">
                                            <a  class="btn btn-orange" href="{{ route('admin/product/create') }}">
                                                <i class="fa fa-plus"></i> {{ __('page.add_product') }}
                                            </a>
                                        </div>
                                        -->
                                        <div class="float-right">
                                            <a  class="btn btn-orange" href="{{ route('admin/package/form') }}">
                                                <i class="fa fa-plus"></i> {{ __('page.add_package') }}
                                            </a>                                          
                                        </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" >
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <table class="table display w-100 table-brown" id="package_table">
                                            <thead class="bg-darkgrey">
                                                <tr role="row">
                                                    <th>{{ __('field.action') }}</th>
                                                    <th>No.</th>
                                                    <th>{{ __('field.package_code') }}</th>
                                                    <th>Tanggal Dibuat</th>
                                                    <!-- <th>{{ __('field.category_name') }}</th>
                                                    <th>{{ __('field.product_price') }}</th>
                                                    <th>{{ __('field.product_brand') }}</th> -->
                                                </tr>
                                            </thead>
                                        </table>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var packageTable = $('#package_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable();
    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>

    @endif
@endsection
