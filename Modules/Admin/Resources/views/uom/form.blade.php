@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_uom') : __('page.edit_uom')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.uom') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_uom') : __('page.edit_uom') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                                <div class="panel panel-flat" >
                                    <div class="card">
                                            <div class="panel-heading border-bottom-grey">
                                                <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_uom') : __('page.edit_uom') }}</h4>
                                                <div class="clearfix"></div>
                                            </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form method="POST" action="{{ route('admin/uom/form') }}" id="uomForm">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body" >
                                                        <div class="col-md-4 ">
                                                            <div class="card-body">
                                                                @csrf
                                                                @if(Request::get('id') !== null)
                                                                    <input type="hidden" name="uom_id" value="{{ Request::get('id') }}">
                                                                @endif
                                                                <div class="form-group">
                                                                    <label for="uom_name">{{ __('field.uom_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('uom_name') is-invalid @enderror" name="uom_name" @if(Request::get('id') !== null) value="{{ $uom_by_id['uom_name'] }}" @else value="{{ old('uom_name') }}" @endif autocomplete="off">
                                                                    @error('uom_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('uom_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="product_weight_type">{{ __('field.product_weight_type') }} <span class="required-label">*</span></label>
                                                                    <select name="product_weight_type" class="form-control single-select">
                                                                        <option value=""></option>
                                                                        <option value="0" {{ Request::get('id') == null ? (old('product_weight_type') == 0 ? 'selected' : '') : ($uom_by_id['is_decimal'] == 0 ? 'selected' : '') }}>{{ __('field.round') }}</option>
                                                                        <option value="1" {{ Request::get('id') == null ? (old('product_weight_type') == 1 ? 'selected' : '') : ($uom_by_id['is_decimal'] == 1 ? 'selected' : '') }}>{{ __('field.decimal') }}</option>
                                                                    </select>
                                                                    @error('product_weight_type')
                                                                        <span class="invalid-feedback">{{ $errors->first('product_weight_type') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="uom_description">{{ __('field.uom_description') }} <span class="required-label">*</span></label>
                                                                    <textarea class="form-control @error('uom_description') is-invalid @enderror" style="resize: none" name="uom_description">@if(Request::get('id') !== null){{ $uom_by_id['uom_desc'] }}@else{{ old('uom_description') }}@endif</textarea>
                                                                    @error('uom_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('uom_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class="col-md-12 no-padding">
                                                    <div class="card-footer bg-transparent ">
                                                        <div class="col-md-4 no-padding float-right">
                                                            <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                            </div>
                                                            <div class="col-xs-6 padding-l-10">
                                                            <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                             </form>
                                            @if(Request::get('id') != null)
                                            <div class="separator no-margin"></div>
                                            <div class="col-md-12 no-padding">
                                                <div class="panel-body no-padbot">
                                                    <div class="margin-tb-10"></div>
                                                    <button class="btn btn-orange" id="addUomConvertionButton"><i class="fa fa-plus"></i> Tambah Konversi Satuan Pengukuran</button>
                                                </div>
                                            </div>
                                            <form method="POST" action="{{ route('admin/uom/conversion/form') }}">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body" >
                                                    @csrf
                                                        <input type="hidden" name="uom_id" value="{{ Request::get('id') }}">
                                                        <div class="col-md-12 no-padding-right res-no-pad res-scroll">
                                                            <table id="uomConvertionTable" class="table">
                                                                <thead class="bg-darkgrey">
                                                                    <tr>
                                                                        <th>Satuan Pengukuran 1</th>
                                                                        <th>Rumus</th>
                                                                        <th>Satuan Pengukuran 2</th>
                                                                        <th>Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if(count($uom_convertion) > 0)
                                                                        @for($c = 0; $c < count($uom_convertion); $c++)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="form-group no-margin">
                                                                                    <select name="first_uom_id[]" class="form-control single-select">
                                                                                        @for($b = 0; $b < count($uom); $b++)
                                                                                            <option value="{{ $uom[$b]['uom_id'] }}" {{ $uom_convertion[$c]['uom_first_id'] == $uom[$b]['uom_id'] && $uom_convertion[$c]['uom_first_id'] != Request::get('id') ? 'selected' : ($uom_convertion[$c]['uom_second_id'] == $uom[$b]['uom_id'] && $uom_convertion[$c]['uom_second_id'] != Request::get('id') ? 'selected' : '') }}>{{ $uom[$b]['uom_name'] }}</option>
                                                                                        @endfor
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group no-margin">
                                                                                            <select name="operation[]" class="form-control">
                                                                                                <option value="division" {{ $uom_convertion[$c]['uom_first_id'] == Request::get('id') ? 'selected' : '' }}>Dibagi</option>
                                                                                                <option value="multiplicity" {{ $uom_convertion[$c]['uom_second_id'] == Request::get('id') ? 'selected' : '' }}>Dikali</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group no-margin">
                                                                                            <input type="text" name="formula[]" class="form-control" value="{{ $uom_convertion[$c]['formula'] }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="form-group no-margin">
                                                                                    <input type="text" class="form-control" name="second_uom_id[]" value="{{ $uom_by_id['uom_name'] }}" readonly>
                                                                                </div>
                                                                            </td>
                                                                            <td><div style="cursor:pointer" class="deleteUom"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td>
                                                                        </tr>
                                                                        @endfor
                                                                    @else
                                                                    <tr>
                                                                        <td>
                                                                            <div class="form-group no-margin">
                                                                                <select name="first_uom_id[]" class="form-control single-select">
                                                                                    @for($b = 0; $b < count($uom); $b++)
                                                                                        <option value="{{ $uom[$b]['uom_id'] }}" {{ $uom[$b]['uom_id'] == Request::get('id') ? 'disabled' : '' }}>{{ $uom[$b]['uom_name'] }}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group no-margin">
                                                                                        <select name="operation[]" class="form-control">
                                                                                            <option value="division">Dibagi</option>
                                                                                            <option value="multiplicity">Dikali</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group no-margin">
                                                                                        <input type="text" name="formula[]" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group no-margin">
                                                                                <input type="text" class="form-control" value="{{ $uom_by_id['uom_name'] }}" readonly>
                                                                            </div>
                                                                        </td>
                                                                        <td><div style="cursor:pointer" class="deleteUom"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td>
                                                                    </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 no-padding float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="uom_name"], textarea[name="uom_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            uom_name: {
                required: true,
                // minlength: 2,
                unique: true,
                alpha: true
            },
            product_weight_type: {
                required: true
            },
            uom_description: {
                required: true,
                // minlength: 2
            }
        };
        var validationMessages = {
            uom_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.uom_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.uom_name'), 'min' => 2]) }}",
                unique: "{{ __('validation.unique',['attribute' => __('validation.attributes.uom_name')]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.uom_name')]) }}"
            },
            product_weight_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight_type')]) }}"
            },
            uom_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.uom_description')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.uom_description'), 'min' => 2]) }}"
            }
        };
        
        $('form#uomForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        @if(Request::get('id') != null)
        var html = "";
        $('#addUomConvertionButton').click(function(){
            html = "";
            html += "<tr>";
            html += "<td>";
            html += "<div class='form-group'>";
            html += "<select name='first_uom_id[]' class='form-control single-select'></select>";
            html += "</div>";
            html += "</td>";
            html += "<td>";
            html += "<div class='row'>";
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group'>";
            html += '<select name="operation[]" class="form-control">';
            html += '<option value="division">Dibagi</option>';
            html += '<option value="multiplicity">Dikali</option>';
            html += '</select>';
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group'>";
            html += "<input name='formula[]' class='form-control'>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</td>";
            html += "<td>";
            html += "<input type='text' value='{{ $uom_by_id['uom_name'] }}' class='form-control' readonly>";
            html += "</td>";
            html += "<td><div style='cursor:pointer' class='deleteUom'><i class='fa fa-times-circle color-red ico-size-td'></i></div></td>";
            html += "</tr>";
            
            $(html).insertAfter('table#uomConvertionTable tbody tr:last-child');
            $.ajax({
                url: "{{ route('admin/uom/get') }}",
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var data = resultData['data'];
                    var uom_array = [];
                    for (let b = 0; b < data.length; b++) {
                        // const element = array[b];
                        uom_array.push("<option value='"+data[b]['uom_id']+"' "+(data[b]['uom_id'] == "{{ Request::get('id') }}" ? 'disabled' : '')+">"+data[b]['uom_name']+"</option>");
                    }
                    $('select[name="first_uom_id[]"]:last-child').html(uom_array);
                    // $('select[name="first_uom_id"]:last-child option[value="{{ Request::get("id") }}"]').prop('disabled',true);
                    // $('select[name="second_uom_id[]"]:last-child').html(uom_array);
                }
            });
            $('.deleteUom').click(function(){
                var index = $('.deleteUom').index(this);
                $('#uomConvertionTable tbody tr').eq(index).remove();
            });
            $('.single-select').select2();
            // $('table#uomConvertionTable ').html();
        });
        @endif

        clickCancelButton('{{ route("admin/uom") }}')

        exitPopup('{{ route("admin/uom") }}');

        $('.deleteUom').click(function(){
            var index = $('.deleteUom').index(this);
            $('#uomConvertionTable tbody tr').eq(index).remove();
        });
    </script>
    @endif
@endsection
