
@extends('admin::layouts.master')

@section('title',__('menubar.offline_transaction'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'offline_transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i></li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li class="active text-bold ">{{ __('menubar.offline_transaction') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.offline_transaction') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a href="{{url('admin/transaction/create')}}" class="btn btn-orange"><i class="fa fa-plus"></i> Tambah Transaksi</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a class="btn btn-orange" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i>Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="payment_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>Kode Transaksi</th>
                                                        <th>Gudang</th>
                                                        <th>Tanggal</th>
                                                        <th>Konsumen</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <form id="transactionFilterForm">
                        @csrf
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transactionCode">Kode Transaksi</label>
                                    <input type="text" class="form-control" id="transactionCode" name="transactionCode" @if(Request::get('transactionCode') != null) value="{{ Request::get('transactionCode') }}" @endif>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif>
                                            <!-- <input type="date" class="form-control" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif>
                                        <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="gudang">Gudang</label>
                                    <select name="gudang" id="gudang" class="form-control single-select" placeholder="Gudang">
                                        <option value="">--Pilih Gudang--</option>
                                        @for($b = 0; $b < count($all_warehouse); $b++)
                                            <option value="{{ $all_warehouse[$b]['warehouse_id'] }}"  @if(Request::get('warehouse_id') != null) @if($all_warehouse[$b]['warehouse_id'] == Request::get('gudang')) selected @endif @endif>{{ $all_warehouse[$b]['warehouse_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="konsumen">Konsumen</label>
                                    <select name="konsumen" id="konsumen" class="form-control single-select" placeholder="Konsumen">
                                        <option value="">--Pilih Konsumen--</option>
                                        @for($b = 0; $b < count($customer); $b++)
                                            <option value="{{ $customer[$b]['customer_id'] }}" @if(Request::get('customer_id') != null) @if($customer[$b]['customer_id'] == Request::get('konsumen')) selected @endif @endif>{{ $customer[$b]['customer_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-sm-12 no-padding btn-footer margin-t10">
                                <div class="col-md-6 padding-r-10 ">
                                    <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('warehouse_id') == null && Request::get('customer_id') == null) disabled @endif>{{ __('page.reset') }}</button>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/transaction/offline') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var paymentTable = $('#payment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            searching: false,
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_payment_offline") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    transactionCode: '{{ Request::get("transactionCode") }}',
                    startDate: '{{ Request::get("transaction_start_date") }}',
                    endDate: '{{ Request::get("transaction_end_date") }}',
                    gudang: '{{ Request::get("gudang") }}',
                    konsumen: '{{ Request::get("konsumen") }}'
                }
            },
            columns: [
                {data: 'action', name: 'action'},
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'trx_code', name: 'trx_code'},
                {data: 'warehouse_name', name: 'warehouse_name'},
                {data: 'created_date', name: 'created_date'},
                {data: 'customer_name', name: 'customer_name'},
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        var idx = 0;
        setInterval( function () {
            idx += 1;
            // console.log(idx);
            if(idx > 0 && idx % 180 == 0){
                paymentTable.ajax.reload(null, false);
            }
        }, 1000);
    </script>
    @endif
@endsection
