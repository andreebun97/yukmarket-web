
@extends('admin::layouts.master')

@section('title',__('menubar.offline_transaction'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'offline_transaction'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i></li>
                                <li class="active text-bold ">{{ __('menubar.transaction') }}</li>
                                <li class="active text-bold ">{{ __('menubar.offline_transaction') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-gray no-margin-bottom">
                                            <h4 class="panel-title text-bold title-color-green">Transaksi Manual</h4>
                                            @if($accessed_menu == 1)
                                            <!-- <div class="float-right">
                                                <a role="button" href="{{ route('admin/inventory/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_inventory') }}</a>
                                            </div> -->
                                            @endif
                                            <div class="clearfix"></div>
                                     
                                        </div>
                                        
                                        <div class="panel-body" >
                                                <form id="inventory_form" action="" style="margin-top: 15px;">
                                                    <div class="form_header">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="inventory_org" value="{{$user_wh_org->organization_id}}">
                                                        <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="product_stock">No. Transaksi</label>
                                                                <input type="text" name="inventory_trx" class="form-control inventory_trx" value="{{$no_trx}}" readonly="readonly" />
                                                                <input type="hidden" name="invoice_no" class="form-control invoice_no" value="{{$invoice_no}}" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 no-padding-right res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                @if($user_wh_org->warehouse_id != null)
                                                                    <input type="hidden" name="inventory_warehouse" class="inventory_warehouse" value="{{$user_wh_org->warehouse_id}}">
                                                                    <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user_wh_org->warehouse_name}}" readonly="readonly" />
                                                                @else
                                                                    <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('product_category') is-invalid @enderror">
                                                                        <option value="">-- Pilih Gudang --</option>
                                                                        @foreach($all_warehouse as $warehouse)
                                                                        <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 no-padding-left res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="inventory_date">Tanggal Masuk <span class="required-label">*</span></label>
                                                                <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <input type="text" class="form-control daterange-single" name="inventory_date" value="" min="{{date('Y-m-d')}}" >
                                                                        <!-- <input type="date" class="form-control" name="inventory_date" value="" min="{{date('Y-m-d')}}"> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-4 res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="dim_length">Status<span class="required-label">*</span></label>
                                                                <select name="inventory_status" class="form-control single-select inventory_status @error('product_category') is-invalid @enderror">
                                                                    <option value="">-- Pilih Status --</option>
                                                                    <option value="0">Tertunda</option>
                                                                    <option value="1">Selesai</option>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                        <div class="col-sm-6 no-padding-right res-no-pad-sm">
                                                            <div class="form-group">
                                                                <label for="customer_name">Konsumen</label>
                                                                <select name="customer" class="form-control single-select customer_name @error('product_category') is-invalid @enderror">
                                                                    <option value="">-- Pilih Konsumen --</option>
                                                                    @foreach($customer as $cust)
                                                                    <option value="{{$cust->customer_id}}"> {{$cust->customer_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="table-no-datatable no-padding" >
                                                        <div class="col-xs-12 no-padding  res-scroll-md">
                                                            <table class="table display w-100 table-brown border-head datatable-basic" id="table-form" style="display: none;">
                                                                <thead class="bg-darkgrey w-100">
                                                                    <tr>
                                                                        <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                                                                        <th class="text-center">ID Produk</th>
                                                                        <th class="text-center">Harga</th>
                                                                        <th class="text-center">Satuan</th>
                                                                        <th colspan="2" class="text-center">Kuantitas<span class="required-label">*</span></th>
                                                                        <th class="text-center">Total</th>
                                                                        <!-- <th></th> -->
                                                                        {{-- <th></th> --}}
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="inventory_item">
                                                                    <tr class="inventory_item_1">
                                                                        <td>
                                                                            <div class="form-group no-margin">
                                                                                @if($wh_product != null)
                                                                                    <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                        <option value="">-- Pilih Produk --</option>
                                                                                        @foreach($wh_product as $prd)
                                                                                            <option value="{{$prd->prod_id}}">{{$prd->prod_name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <span class="invalid-feedback error_inventory_product"></span>
                                                                                @else
                                                                                    <select id="inventory_product_1" name="inventory_product[]" class="form-control inventory_product single-select @error('product_category') is-invalid @enderror" onchange="inventoryProduct(this)">
                                                                                        <option value="">-- Pilih Produk --</option>
                                                                                    </select>
                                                                                    <span class="invalid-feedback error_inventory_product"></span>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group no-margin">
                                                                                <input type="text" id="inventory_produk_code_1" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group no-margin">
                                                                                        <input type="text" id="inventory_price_1" name="inventory_price[]" class="form-control inventory_price" onkeyup="currencyFormat(this)" value="" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group no-margin">
                                                                                        <input type="text" id="uom_name_1" name="uom_name[]" class="form-control satuan_ajah" readonly/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <div class="row">
                                                                                <div class="col-sm-8">
                                                                                    <div class="form-group no-margin">
                                                                                        <input type="text" id="inventory_stock_1" name="inventory_stock[]" class="form-control inventory_stock" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" required/>
                                                                                        <input type="hidden" id="final_stock_1" name="final_stock[]" class="form-control final_stock" />
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-sm-4">
                                                                                    <div class="form-group no-margin">
                                                                                        <select type="text" id="satuan_produk_1" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>
                                                                                            @if(!empty($satuan))
                                                                                            <option value="">-- Satuan --</option>
                                                                                            @foreach($satuan as $st)
                                                                                                <option value="{{$st->uom_id}}">{{$st->uom_name}}</option>
                                                                                            @endforeach
                                                                                            @else
                                                                                                <option value="">-- Pilih Produk --</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="form-group no-margin">
                                                                                <input type="text" id="inventory_price_total_1" name="inventory_price_total[]" class="form-control inventory_price_total" value="0" readonly="readonly" />
                                                                            </div>
                                                                        </td>
                                                                        <!-- <td></td> -->
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot>
                                                                    <td colspan="3">
                                                                        <button class="btn btn-orange" id="addInvenProduct" data-id="1"><i class="fa fa-plus"></i> Tambah Produk</button>
                                                                    </td>
                                                                    <td class="text-right">Grand Total</td>
                                                                    <td colspan="2" class="text-left stock_total">0</td>
                                                                    <td class="text-left price_total">0</td>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <!-- <div class="form_footer text-right">
                                                        <div class="col-md-12">
                                                            <div class="bg-transparent " style="margin: 0 15px 15px 0;">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div> -->

                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                        
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <input type="submit" name="addInven" id="addInven" class="btn btn-orange btn-block" value="Simpan">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    @include('loader')
    @include('error_popup')
    <?php /* ?>
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    */ ?>
    @include('assets_link.js_list')
    <script>
        $('#addInvenProduct').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            $(this).attr('data-id', i);
            var wh_id = $('.inventory_warehouse').val();

            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
                success: function (r) {
                    html = '<tr class="inventory_item_'+i+'">';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<select id="inventory_product_'+i+'" name="inventory_product[]" class="form-control inventory_product single-select" onchange="inventoryProduct(this)">';
                    html += '<option value="">-- Pilih Produk --</option>';
                    $.each(r.product, function (i, item) {
                        html += '<option value="'+item.prod_id+'" class="inventory_list_product">'+item.prod_name+'</option>';
                    });
                    html += '</select>';
                    html += '<span class="invalid-feedback error_inventory_product"></span>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_produk_code'+i+'" name="inventory_produk_code" class="form-control inventory_produk_code" value="" disabled="disabled" />';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="row">';
                    html += '<div class="col-sm-12">';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_price'+i+'" name="inventory_price[]" onkeyup="currencyFormat(this)" class="form-control inventory_price" value=""/>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="row">';
                    html += '<div class="col-sm-12">';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="uom_name'+i+'" name="uom_name[]" class="form-control satuan_ajah" readonly/>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="row">';
                    html += '<div class="col-sm-8">';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_stock'+i+'" name="inventory_stock[]" class="form-control inventory_stock" onkeyup="inventoryStock(this)" onkeypress="return numeric(event)" value="" required />';
                    html += '<input type="hidden" id="final_stock_'+i+'" name="final_stock[]" class="form-control final_stock" />';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-sm-4">';
                    html += '<div class="form-group">';
                    html += '<select type="text" id="satuan_produk'+i+'" name="satuan_produk[]" class="form-control satuan_produk single-select @error('product_category') is-invalid @enderror" onchange="convertStock(this)" required>';
                    html += '<option value="">-- Pilih Produk --</option>';
                    $.each(r.uom, function (k, satu) {
                        html += '<option value="'+satu.uom_id+'" class="inventory_list_product">'+satu.uom_name+'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="form-group">';
                    html += '<input type="text" id="inventory_price_total'+i+'" name="inventory_price_total[]" class="form-control inventory_price_total" value="0" readonly="readonly" />';
                    html += '</div>';
                    html += '</td>';
                    html+= '<td>';
                    html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteInvenProduct('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
                    html+= '</td>';
                    html += '</tr>';
                    $('#inventory_item').append(html);
                    $(".single-select").select2();
                    hideLoader();
                }
            });
        });

        function deleteInvenProduct(i){
            $('.inventory_item_'+i).remove();
        }

        function inventoryProduct(i){
            var row = $(i).closest('tr');
            var prod_id = $(i).val();
            var wh_id = $('.inventory_warehouse').val();
            showLoader();
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/inventory/getProductByIdAndWh') }}/"+prod_id+"/"+wh_id,
                success: function (r) {
                    console.log(r);
                    if (r.price_warehouse != null) {
                        var prod_price = r.price_warehouse;
                    } else {
                        if (r.prod_price != null) {
                            var prod_price = r.prod_price;
                        } else {
                            var prod_price = 0;
                        }
                    }
                    row.find('.inventory_produk_code').val(r.prod_code);
                    row.find('.inventory_price').val(currency(prod_price.toString()));
                    row.find('.satuan_ajah').val(r.uom_name);
                    row.find('.satuan_produk').val(r.uom_id).trigger('change');
                    hideLoader();
                }
            });
        }

        function inventoryPrice(i){
            currencyFormat(i);
            var price = $(i).val().split(".").join("");
            var row = $(i).closest('tr');
            var qty = row.find('.inventory_stock').val();
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            }
            var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
        }

        function inventoryStock(i){
            var qty = $(i).val();
            var row = $(i).closest('tr');
            var price = row.find('.inventory_price').val().split(".").join("");
            var total = price * qty;
            if (qty != '') {
                row.find('.inventory_price_total').val(currency(total.toString()));
            } else {
                row.find('.inventory_price_total').val("0");
            }
            var stock_total = sum($("input[name='inventory_stock[]']").map(function(){return $(this).val();}).get());
            var price_total = sum($("input[name='inventory_price_total[]']").map(function(){return $(this).val();}).get());
            $('.stock_total').text(stock_total);
            $('.price_total').text(currency(price_total.toString()));
            convertStock(i);
        }

        function currency(i){
            var total = i.length;
            var currencyTemp  = "";
            for (let k = 0; k < i.length; k++) {
                total -= 1;
                currencyTemp = currencyTemp + i[k];
                if(total > 0 && total % 3 == 0){
                    currencyTemp = currencyTemp + ".";
                }
            }
            return currencyTemp;
        }

        function sum(input){
             
            if (toString.call(input) !== "[object Array]"){
                return false;
            }
            
            var total =  0;
            for(var i=0;i<input.length;i++){
                var num = input[i].split(".").join("");
                if(isNaN(num)){
                    continue;
                }
                  total += Number(num);
            }

            return total;
        }

        $('#inventory_form').submit(function(e){
            e.preventDefault();
            showLoader();
            $.ajax({
                type: "POST",
                url: '{{ route("admin/transaction/form") }}',
                data: $(this).serialize(),
                success: function (r) {
                    if (r.code != '200') {
                        $.each(r.messages, function (i, item) {
                            $('.error_'+i).text(item);
                        });
                        hideLoader();
                    } else {
                        hideLoader();
                        $('.modal-custom-text p').text(r.messages);
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        // $('#inventory_form')[0].reset();
                        window.location.replace("<?= url('admin/transaction/offline') ?>");
                    }
                    // $('.inventory_product_code').val(r.prod_code);
                }
            });
        });


        function convertStock(i) {
            var row = $(i).closest('tr');
            var productId = row.find('.inventory_product').val();
            var uom_id = row.find('.satuan_produk').val();
            var uom_value = row.find('.inventory_stock').val();
            if (uom_value != '' && uom_id != '' && productId != '') {
                showLoader();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/inventory/converterStock") }}',
                    data: {
                        productId: productId,
                        satuan_produk: uom_id,
                        inventory_stock: uom_value
                    },
                    success: function (r) {
                        if (r.flag !== false) {
                            row.find('.final_stock').val(r);
                            $('#addInven').attr('disabled', false);
                            hideLoader();
                        } else {
                            hideLoader();
                            $('.modal-custom-text p').text(r.message);
                            $('#notificationPopup  .modal-header').removeClass('success');
                            $('#notificationPopup  .modal-header').addClass('error');
                            $('#TambahStok').modal('hide');
                            $('#notificationPopup').modal('show');
                            $('#notificationPopupLabel').text('Gagal');
                            $('#addInven').attr('disabled', true);
                        }
                    },
                    error: function(r) {
                        hideLoader();
                        $('.modal-custom-text p').text(r.responseJSON.message);
                        $('#notificationPopup  .modal-header').removeClass('success');
                        $('#notificationPopup  .modal-header').addClass('error');
                        $('#TambahStok').modal('hide');
                        $('#notificationPopup').modal('show');
                        $('#notificationPopupLabel').text('Gagal');
                        $('#addInven').attr('disabled', true);
                    }
                });
            }
        }

        $(".inventory_warehouse").change(function(e){
            e.preventDefault();
            showLoader();
            var wh_id = $('.inventory_warehouse').val();
            if (wh_id != '') {
                $('#table-form').fadeIn();
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
                    success: function (r) {
                        $.each(r.product, function() {
                            $('#inventory_product_1').val(null);
                            var newOption = new Option(this.prod_name, this.prod_id, false, false);
                            $('#inventory_product_1').append(newOption);
                            //$(".inventory_warehouse").select2({ disabled:'readonly' })
                        });
                        hideLoader();
                    }
                });
            } else {
                $('#table-form').fadeOut();
                hideLoader();
            }
            
        });

        $('.discard_changes_button').click(function(){
            clickCancelButton('{{ route("admin/transaction/offline") }}')
        })

    </script>
@endsection
