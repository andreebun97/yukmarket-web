@extends('admin::layouts.master')

@section('title',(Request::get('product') == null ? __('page.add_product') : __('page.edit_product')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-book position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.product') }}</li>
                                <li class="active text-bold ">{{ Request::get('product') == null ? __('page.add_product') : __('page.edit_product') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('product') == null ? __('page.add_product') : __('page.edit_product') }}</h4>
                                           
                                            <div class="clearfix"></div>
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <div class="clearfix"></div>
                                        
                                        
                                            <div class="tabbable">
                                                <!-- <div class="panel-heading no-padding-bottom">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item col-md-4 col-sm-4 col-xs-6 text-center no-padding active"><a class="nav-link" href="#master" data-toggle="tab"> Master </a></li>
                                                        @if(Request::get('product') != null)
                                                        <li class="nav-item col-md-4 col-sm-4 col-xs-6 text-center no-padding"><a class="nav-link" href="#product-ecommerce_tab" data-toggle="tab">Sinkronisasi Ecommerce </a></li>
                                                        @endif
                                                    </ul>
                                                </div> -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="master">
                                                        <form method="POST" id="productForm" action="{{ route('admin/product/form') }}" enctype="multipart/form-data" class="form-style">
                                                    
                                                            <div class="col-md-12 no-padding">
                                                                <div class="panel-body">
                                                                    <div class="col-md-4 med-img padding-r-10">
                                                                        <div id="showing_product_image">
                                                                            <center><img src="{{ Request::get('product') == null ? asset('img/default_product.jpg') : '/'.$product_based_on_id['prod_image'] }}" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></center>
                                                                        </div>
                                                                    </div>
                                                                    @csrf
                                                                    @if(Request::get('product') !== null)
                                                                        <input type="hidden" name="product_id" value="{{ $product_based_on_id['prod_id'] }}">
                                                                        <input type="hidden" name="product_sku" value="{{ $product_based_on_id['product_sku_id'] }}">
                                                                        <input type="hidden" name="product_image_temp" value="{{ $product_based_on_id['prod_image'] }}">
                                                                        <input type="hidden" name="product_ecommerce_code" value="{{ $product_based_on_id['prod_ecom_code'] }}">
                                                                        <input type="hidden" name="product_code" value="{{ $product_based_on_id['prod_code'] }}">
                                                                        <input type="hidden" name="total_stock" value="{{ $product_based_on_id['total_stock'] == null ? 1 : $product_based_on_id['total_stock'] }}">
                                                                    @endif
                                                                    <div class="col-md-8 no-padding">
                                                                        <div class="col-md-6 col-sm-12 res-no-pad">
                                                                            <!-- <div class="form-group">
                                                                                <label for="product_sku">{{ __('menubar.stock_keeping_unit') }} </label>
                                                                                <select name="product_sku" class="form-control single-select @error('product_sku') is-invalid @enderror" placeholder="{{ __('page.choose_stock_keeping_unit') }}">
                                                                                    <option value="">{{ __('page.choose_stock_keeping_unit') }}</option>
                                                                                    @for($b = 0; $b < count($sku); $b++)
                                                                                        <option value="{{ $sku[$b]['product_sku_id'] }}" {{ (isset($product_based_on_id) && $product_based_on_id['product_sku_id'] == $sku[$b]['product_sku_id'] ? 'selected' : '') }}>{{ $sku[$b]['product_sku_name'] }}</option>
                                                                                    @endfor
                                                                                </select>
                                                                                @error('product_sku')
                                                                                    <span class="invalid-feedback">{{ $errors->first('product_sku') }}</span>
                                                                                @enderror
                                                                            </div> -->
                                                                            <div class="form-group">
                                                                                <label for="product_name">{{ __('field.product_name') }} <span class="required-label">*</span></label>
                                                                                <input type="text" class="form-control @error('product_name') is-invalid @enderror" name="product_name" value="{{ Request::get('product') == null ? old('product_name') : $product_based_on_id['prod_name'] }}">
                                                                                @error('product_name')
                                                                                    <span class="invalid-feedback">{{ $errors->first('product_name') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <!-- OLD PRICE -->
                                                                                <!-- <div class="form-group">
                                                                                    <label for="product_price">{{ __('field.product_price') }} <span class="required-label">*</span></label>
                                                                                    <input type="text" class="form-control @error('product_price') is-invalid @enderror" onkeyup="currencyFormat(this)" name="product_price" value="{{ Request::get('product') == null ? old('product_price') : $product_based_on_id['prod_price'] }}">
                                                                                    @error('product_price')
                                                                                        <span class="invalid-feedback">{{ $errors->first('product_price') }}</span>
                                                                                    @enderror
                                                                                </div> -->
                                                                            <!-- OLD PRICE -->
                                                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 no-padding">
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="product_category">{{ __('field.category_name') }} <span class="required-label">*</span></label>
                                                                                        <select name="product_category" id="product_category" onchange="load_categorydua()" class="form-control single-select @error('product_category') is-invalid @enderror">
                                                                                            <option value=""></option>
                                                                                            @for($b = 0; $b < count($category); $b++)
                                                                                                <option value="{{ $category[$b]['category_id'] }}" @if(Request::get('product') == null) @if($category[$b]['category_id'] == old('product_category')) selected @endif @else @if($category[$b]['category_id'] == $product_based_on_id['category_id']) selected @endif @endif>{{ $category[$b]['category_name'] }}</option>
                                                                                            @endfor
                                                                                        </select>
                                                                                        @error('product_category')
                                                                                            <span class="invalid-feedback">{{ $errors->first('product_category') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group">
                                                                                        <label for="product_brand">{{ __('field.product_brand') }}</label>
                                                                                        <select name="product_brand" class="form-control single-select @error('product_brand') is-invalid @enderror">
                                                                                            <option value=""></option>
                                                                                                @for($b = 0; $b < count($brand); $b++)
                                                                                                <option value="{{ $brand[$b]['brand_id'] }}" @if(Request::get('product') == null) @if($brand[$b]['brand_id'] == old('product_brand')) selected @endif @else @if($brand[$b]['brand_id'] == $product_based_on_id['brand_id']) selected @endif @endif>{{ $brand[$b]['brand_name'] }}</option>
                                                                                            @endfor
                                                                                        </select>
                                                                                        @error('product_brand')
                                                                                            <span class="invalid-feedback">{{ $errors->first('product_brand') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <div class="form-group select-box">
                                                                                <label for="product_tag">{{ __('field.product_tag') }} <span class="required-label">*</span></label>
                                                                                <div class="clearfix"></div>
                                                                                <select class="select-state multi-select" name="product_tag[]"  placeholder="{{ __('page.choose_tag') }}..." multiple="multiple">
                                                                                    <option value="">{{ __('page.choose_tag') }}...</option>
                                                                                    @for($b = 0; $b < count($tags); $b++)
                                                                                        <option value="{{ $tags[$b]['tag_id'] }}" {{ Request::get('product') !== null ? (count($tags_by_id) == 0 ? '' : (array_search($tags[$b]['tag_id'], array_column($tags_by_id,'tag_id')) !== false ? 'selected' : '')) : '' }}>{{ $tags[$b]['tag_name'] }}</option>
                                                                                    @endfor
                                                                                </select>
                                                                                @error('product_tag')
                                                                                    <span class="invalid-feedback">{{ $errors->first('product_tag') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <!-- <div class="form-group">
                                                                                <label for="stockable_flag">{{ __('field.stockable_flag') }}</label>
                                                                                <select name="stockable_flag" class="form-control single-select @error('stockable_flag') is-invalid @enderror">
                                                                                    <option value=""></option>
                                                                                    <option value="1" {{ Request::get('product') == null ? (old('stockable_flag') == 1 ? 'selected' : '') : ($product_based_on_id['stockable_flag'] == 1 ? 'selected' : '') }}>{{ __('notification.yes') }}</option>
                                                                                    <option value="0" {{ Request::get('product') == null ? (old('stockable_flag') == 0 ? 'selected' : '') : ($product_based_on_id['stockable_flag'] == 0 ? 'selected' : '') }}>{{ __('notification.no') }}</option>
                                                                                </select>
                                                                                @error('stockable_flag')
                                                                                    <span class="invalid-feedback">{{ $errors->first('stockable_flag') }}</span>
                                                                                @enderror
                                                                            </div> -->
                                                                            <div class="col-xs-6 no-padding product_main_stock d-none">
                                                                                <div class="form-group padding-r-10">
                                                                                    <label for="product_stock">{{ __('field.product_stock') }} <span class="required-label">*</span></label>
                                                                                    <input type="number" name="product_stock" class="form-control @error('product_stock') is-invalid @enderror" value="{{ Request::get('product') == null ? old('product_stock') : $product_based_on_id['stock'] }}">
                                                                                    @error('product_stock')
                                                                                        <span class="invalid-feedback">{{ $errors->first('product_stock') }}</span>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6 no-padding product_main_stock d-none">
                                                                                <div class="form-group">
                                                                                    <label for="minimum_stock">{{ __('field.minimum_stock') }} <span class="required-label">*</span></label>
                                                                                    <input type="text" class="form-control @error('minimum_stock') is-invalid @enderror" name="minimum_stock" value="{{ Request::get('product') == null ? old('minimum_stock') : $product_based_on_id['minimum_stock'] }}">
                                                                                    @error('minimum_stock')
                                                                                        <span class="invalid-feedback">{{ $errors->first('minimum_stock') }}</span>
                                                                                    @enderror
                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="col-xs-12 no-padding">
                                                                                <div class="form-group">
                                                                                    <label for="minimum_stock">{{ __('field.minimum_stock') }} <span class="required-label">*</span></label>
                                                                                    <input type="text" class="form-control @error('minimum_stock') is-invalid @enderror" name="minimum_stock" value="{{ Request::get('product') == null ? old('minimum_stock') : $product_based_on_id['minimum_stock'] }}">
                                                                                    @error('minimum_stock')
                                                                                        <span class="invalid-feedback">{{ $errors->first('minimum_stock') }}</span>
                                                                                    @enderror
                                                                                </div>
                                                                            </div> -->
                                                                            <div class="form-group">
                                                                                <label for="minimum_order">{{ __('field.minimum_order') }} <span class="required-label"></span></label>
                                                                                <input type="number" class="form-control" name="minimum_order" value="{{ Request::get('product') == null ? old('minimum_order') : $product_based_on_id['minimum_order'] }}">
                                                                                @error('minimum_order')
                                                                                    <span class="invalid-feedback">{{ $errors->first('minimum_order') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="is_taxable_product">{{ __('field.is_taxable_product') }} <span class="required-label">*</span></label>
                                                                                        <select class="form-control @error('is_taxable_product') is-invalid @enderror" name="is_taxable_product">
                                                                                            <option value="">{{ __('page.choose') }}</option>
                                                                                            <option value="Y" {{ Request::get('product') == null ? old('is_taxable_product') : ($product_based_on_id['is_taxable_product'] == 'Y' ? 'selected' : '') }}>{{ __('notification.yes') }}</option>
                                                                                            <option value="N" {{ Request::get('product') == null ? old('is_taxable_product') : ($product_based_on_id['is_taxable_product'] == 'N' ? 'selected' : '') }}>{{ __('notification.no') }}</option>
                                                                                        </select>
                                                                                        @error('is_taxable_product')
                                                                                            <span class="invalid-feedback">{{ $errors->first('is_taxable_product') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group">
                                                                                        <label for="tax_value">{{ __('field.tax_value') }} <span class="required-label">*</span></label>
                                                                                        <input type="number" name="tax_value" class="form-control @error('tax_value') is-invalid @enderror" value="{{ Request::get('product') == null ? 0 : ($product_based_on_id['tax_value'] * 100) }}" readonly>
                                                                                        @error('tax_value')
                                                                                            <span class="invalid-feedback">{{ $errors->first('tax_value') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <!-- GUDANG OLD  -->
                                                                                <!-- <div class="form-group select-box">
                                                                                    <label for="product_warehouse">{{ __('field.product_warehouse') }} <span class="required-label">*</span></label>
                                                                                    <div class="clearfix"></div>
                                                                                    <select class="select-state multi-select" name="product_warehouse[]"  placeholder="{{ __('page.choose_tag') }}..." multiple="multiple">
                                                                                        <option value="">{{ __('page.choose_warehouse') }}...</option>
                                                                                        @for($b = 0; $b < count($warehouse); $b++)
                                                                                            <option value="{{ $warehouse[$b]['warehouse_id'] }}">{{ $warehouse[$b]['warehouse_name'] }}</option>
                                                                                        @endfor
                                                                                    </select>
                                                                                </div> -->
                                                                            <!-- GUDANG OLD  -->
                                                                            <div class="clearfix"></div>
                                                                            <div class="form-group">
                                                                                <label for="product_picture" class="dp-block">{{ __('field.product_picture') }} @if(Request::get('product') == null) <span class="required-label">*</span></label> @endif
                                                                                <div class="clearfix"></div>
                                                                                <div class="input-img-name">
                                                                                    <span id="image_name">{{ __('page.no_data') }}</span>
                                                                                    <input type="file" name="product_picture" class="form-control" style="display:none;">
                                                                                    <button type="button" class="btn btn-green-upload no-margin" id="product_picture_button">{{ __('page.upload') }}</button> 
                                                                                </div>
                                                                                @error('product_picture')
                                                                                    <span class="invalid-feedback dp-block">{{ $errors->first('product_picture') }}</span>
                                                                                @enderror
                                                                                <div class="info-yellow margin-t10">
                                                                                    <i class="fa fa-info-circle"></i>
                                                                                    {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-12 no-padding-right res-no-pad">
                                                                           
                                                                            <div class="form-group">
                                                                                <label for="product_description">{{ __('field.product_description') }} ({{ __('notification.maximum_characters_of_product_description',['value' => '500']) }})<span class="required-label">*</span></label>
                                                                                <textarea name="product_description" class="form-control @error('product_description') is-invalid @enderror" style="resize:none; height:90px;">{{ Request::get('product') == null ? old('product_description') : $product_based_on_id['prod_desc'] }}</textarea>
                                                                                @error('product_description')
                                                                                    <span class="invalid-feedback">{{ $errors->first('product_description') }}</span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <div class="col-md-12 no-padding" id="uomAdd">
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="product_weight">{{ __('field.product_weight') }} <span class="required-label">*</span></label>
                                                                                        <input type="text" name="product_weight" class="master_product_weight form-control @error('product_weight') is-invalid @enderror" value="{{ Request::get('product') == null ? old('product_weight') : ($product_based_on_id['uom_value']+0) }}">
                                                                                        @error('product_weight')
                                                                                            <span class="invalid-feedback">{{ $errors->first('product_weight') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group">
                                                                                        <label for="product_uom">{{ __('field.uom') }} <span class="required-label">*</span></label>
                                                                                        <select name="product_uom" class="form-control single-select @error('product_uom') is-invalid @enderror" id="product_uom_master">
                                                                                            <option value=""></option>
                                                                                            @for($b = 0; $b < count($uom); $b++)
                                                                                                <option value="{{ $uom[$b]['uom_id'] }}" @if(Request::get('product') == null) @if($uom[$b]['uom_id'] == old('product_uom')) selected @endif @else @if($uom[$b]['uom_id'] == $product_based_on_id['uom_id']) selected @endif @endif>{{ $uom[$b]['uom_name'] }}</option>
                                                                                            @endfor
                                                                                        </select>
                                                                                        @error('product_uom')
                                                                                            <span class="invalid-feedback">{{ $errors->first('product_uom') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <div class="col-md-12 no-padding">
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="bruto">{{ __('field.bruto') }} <span class="required-label">*</span></label>
                                                                                        <input type="text" name="bruto" class="form-control @error('bruto') is-invalid @enderror" value="{{ Request::get('product') == null ? old('bruto') : ($product_based_on_id['bruto']+0) }}">
                                                                                        @error('bruto')
                                                                                            <span class="invalid-feedback">{{ $errors->first('bruto') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 no-padding">
                                                                                    <div class="form-group">
                                                                                        <label for="bruto_uom">{{ __('field.bruto_uom') }} <span class="required-label">*</span></label>
                                                                                        <input type="hidden" name="bruto_uom" class="form-control @error('bruto_uom') is-invalid @enderror" readonly="readonly" value="1">
                                                                                        <input type="text" name="bruto_uom_name" class="form-control @error('bruto_uom_name') is-invalid @enderror" readonly="readonly" value="kg">
                                                                                        <!-- OLD  -->
                                                                                            <!-- <select name="bruto_uom" class="form-control single-select @error('bruto_uom') is-invalid @enderror">
                                                                                                <option value=""></option>
                                                                                                @for($b = 0; $b < count($uom); $b++)
                                                                                                    <option value="{{ $uom[$b]['uom_id'] }}" {{ strtolower($uom[$b]['uom_name']) == strtolower('kg') ? 'selected' : 'disabled' }}>{{ $uom[$b]['uom_name'] }}</option>
                                                                                                @endfor
                                                                                            </select>
                                                                                            @error('bruto_uom')
                                                                                                <span class="invalid-feedback">{{ $errors->first('bruto_uom') }}</span>
                                                                                            @enderror -->
                                                                                        <!-- OLD -->
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                            <div class="col-md-12 no-padding">
                                                                                <div class="col-xs-4 no-padding d-none">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="dim_length">{{ __('field.dim_length') }} (cm)</label>
                                                                                        <input type="text" name="dim_length" class="form-control @error('dim_length') is-invalid @enderror" value="{{ Request::get('product') == null ? old('dim_length') : $product_based_on_id['dim_length'] }}">
                                                                                        @error('dim_length')
                                                                                            <span class="invalid-feedback">{{ $errors->first('dim_length') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-4 no-padding d-none">
                                                                                    <div class="form-group padding-r-10">
                                                                                        <label for="dim_width">{{ __('field.dim_width') }} (cm)</label>
                                                                                        <input type="text" name="dim_width" class="form-control @error('dim_width') is-invalid @enderror" value="{{ Request::get('product') == null ? old('dim_width') : $product_based_on_id['dim_width'] }}">
                                                                                        @error('dim_width')
                                                                                            <span class="invalid-feedback">{{ $errors->first('dim_width') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-4 no-padding d-none">
                                                                                    <div class="form-group">
                                                                                        <label for="dim_height">{{ __('field.dim_height') }} (cm)</label>
                                                                                        <input type="text" name="dim_height" class="form-control @error('dim_height') is-invalid @enderror" value="{{ Request::get('product') == null ? old('dim_height') : $product_based_on_id['dim_height'] }}">
                                                                                        @error('dim_height')
                                                                                            <span class="invalid-feedback">{{ $errors->first('dim_height') }}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                </div>
                                                                            <h6 class="d-none" style="font-weight:bold">{{ __('page.shipping_durability') }}</h6>
                                                                            <div class="col-md-12 no-padding d-none">
                                                                                <div class="col-xs-4 padding-r-10">
                                                                                    <div class="form-group">
                                                                                        <label for="shipping_durability_min">{{ __('field.shipping_durability_min') }} </label>
                                                                                        <input type="number" name="shipping_durability_min" class="form-control" autocomplete="off" @isset($product_based_on_id) value="{{ $product_based_on_id['shipping_durability_min'] }}" @endisset>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-4 padding-r-10">
                                                                                <div class="form-group">
                                                                                    <label for="shipping_durability_max">{{ __('field.shipping_durability_max') }} </label>
                                                                                    <input type="number" name="shipping_durability_max" class="form-control" autocomplete="off" @isset($product_based_on_id) value="{{ $product_based_on_id['shipping_durability_max'] }}" @endisset>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-4 no-padding">
                                                                                    <div class="form-group">
                                                                                        <label for="shipping_durability_unit">{{ __('field.shipping_durability_unit') }} </label>
                                                                                        <select name="shipping_durability_unit" class="form-control single-select" placeholder="{{ __('page.choose_shipping_durability_unit') }}">
                                                                                        <option value="">{{ __('page.choose_shipping_durability_unit') }}</option>
                                                                                        @for($b = 0; $b < count($shipping_durability); $b++)
                                                                                            <option value="{{ $shipping_durability[$b]['shipping_durability_id'] }}" {{ isset($product_based_on_id) && $product_based_on_id['shipping_durability_id'] == $shipping_durability[$b]['shipping_durability_id']? 'selected' : '' }}>{{ $shipping_durability[$b]['shipping_durability_name'] }}</option>
                                                                                        @endfor
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12 no-padding-right res-no-pad res-scroll">
                                                                            <table class="table display w-100 table-brown border-head datatable-basic" >
                                                                                <thead class="bg-darkgrey w-100">
                                                                                    <tr>
                                                                                        <th class="text-center">Nama Gudang<span class="required-label">*</span></th>
                                                                                        <th class="text-center"  style="min-width: 120px;">Harga Produk<span class="required-label">*</span></th>
                                                                                        <th class="text-center">Stok Minimal<span class="required-label">*</span></th>
                                                                                        <th></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="prd_wh_item">
                                                                                @if(Request::get('product') != null)
                                                                                    @foreach($warehouse_list as $key => $wh_list)
                                                                                        <tr class="prd_wh_item_{{$key+1}}">
                                                                                            <td>
                                                                                                <div class="form-group" style="margin: 0 !important;">
                                                                                                    <!-- <label for="product_warehouse">{{ __('field.product_warehouse') }} <span class="required-label">*</span></label> -->
                                                                                                    <div class="clearfix"></div>
                                                                                                    <select class="select-state single-select product_warehouse_item_{{ $key + 1}}" name="product_warehouse[]"  placeholder="{{ __('page.choose_tag') }}...">

                                                                                                        <option value="">-- Pilih Gudang --</option>
                                                                                                        @foreach($warehouse as $wh)
                                                                                                            <option class="wh_{{$wh->warehouse_id}}" value="{{$wh->warehouse_id}}" {{ $wh_list->warehouse_id == $wh->warehouse_id ? 'selected' : '' }}>{{$wh->warehouse_name}} </option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <input type="hidden" value="{{ $wh_list->warehouse_type_id }}" name="warehouse_type[]">
                                                                                                <div class="form-group" style="margin: 0 !important;">
                                                                                                    <!-- <label for="product_price">{{ __('field.product_price') }} <span class="required-label">*</span></label> -->
                                                                                                    <input type="text" data-warehouse_type="{{ $wh_list->warehouse_type_id }}" class="form-control product_price {{ $wh_list->warehouse_type_id == 1 ? 'pointer-none' : '' }}" onkeyup="currencyFormat(this)" name="warehouse_price[]" value="{{ $wh_list->warehouse_type_id == 1 ? 0 : $wh_list->prod_price}}" {{ $wh_list->warehouse_type_id == 1 ? 'readonly' : '' }}>
                                                                                                    @error('product_price')
                                                                                                        <span class="invalid-feedback">{{ $errors->first('product_price') }}</span>
                                                                                                    @enderror
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div class="form-group" style="margin: 0 !important;">
                                                                                                    <!-- <label for="product_price">{{ __('field.product_price') }} <span class="required-label">*</span></label> -->
                                                                                                    <input type="text" class="form-control product_price {{ $wh_list->warehouse_type_id == 1 ? 'pointer-none' : '' }}" name="warehouse_min_stock[]" onkeypress="return numeric(event)" value="{{ $wh_list->warehouse_type_id == 1 ? 0 : $wh_list->minimum_stock}}" {{ $wh_list->warehouse_type_id == 1 ? 'readonly' : '' }}>
                                                                                                    @error('product_price')
                                                                                                        <span class="invalid-feedback">{{ $errors->first('product_price') }}</span>
                                                                                                    @enderror
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                                @if($key+1 > 1)
                                                                                                <div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteWh('{{$key+1}}')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>
                                                                                                @endif
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                @else
                                                                                    <tr class="prd_wh_item_1">
                                                                                        <td>
                                                                                            <div class="form-group" style="margin: 0 !important;">
                                                                                                <!-- <label for="product_warehouse">{{ __('field.product_warehouse') }} <span class="required-label">*</span></label> -->
                                                                                                <div class="clearfix"></div>
                                                                                                <select class="select-state single-select" name="product_warehouse[]"  placeholder="{{ __('page.choose_tag') }}...">

                                                                                                    <option value="">-- Pilih Gudang --</option>
                                                                                                    @foreach($warehouse as $wh)
                                                                                                        <option class="wh_{{$wh->warehouse_id}}" value="{{$wh->warehouse_id}}">{{$wh->warehouse_name}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                                @error('product_warehouse')
                                                                                                     <span class="invalid-feedback">{{ $errors->first('product_warehouse') }}</span>
                                                                                                 @enderror                                                                   
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div class="form-group" style="margin: 0 !important;">
                                                                                                <!-- <label for="product_price">{{ __('field.product_price') }} <span class="required-label">*</span></label> -->
                                                                                                <input type="text" class="form-control warehouse_price @error('warehouse_price') is-invalid @enderror" onkeyup="currencyFormat(this)" name="warehouse_price[]" value="">
                                                                                                @error('warehouse_price')
                                                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_price') }}</span>
                                                                                                @enderror
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div class="form-group" style="margin: 0 !important;">
                                                                                                <!-- <label for="product_price">{{ __('field.product_price') }} <span class="required-label">*</span></label> -->
                                                                                                <input type="text" class="form-control warehouse_min_stock @error('warehouse_min_stock') is-invalid @enderror" name="warehouse_min_stock[]" onkeypress="return numeric(event)" value="">
                                                                                                @error('warehouse_min_stock')
                                                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_min_stock') }}</span>
                                                                                                @enderror
                                                                                            </div>
                                                                                        </td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                @endif
                                                                                </tbody>
                                                                                <tfoot>
                                                                                    <td colspan="4">
                                                                                       <center> <button class="btn btn-orange"  id="addwarehouse" type="button" data-id="{{Request::get('product') != null ? count($warehouse_list) : 1}}"><i class="fa fa-plus"> </i>Tambah Gudang</button></center>
                                                                                    </td>
                                                                                </tfoot>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="col-md-12 no-padding" >
                                                                        <div class="panel-heading no-padding-bottom">
                                                                            <div class="border-bottom-gray"></div>
                                                                            <h4 class="panel-title text-blue-700 text-bold float-left">Variasi Produk</h4>
                                                                            <div class="float-right">
                                                                                <span class="selection-wrapper">
                                                                                    @if(Request::get('product') != null)
                                                                                    <select name="warehouse_list" class="single-select">
                                                                                        <option value="">Pilih Gudang</option>
                                                                                    @foreach($warehouse_list as $wh)
                                                                                        <option value="{{ $wh['warehouse_id'] }}">{{ $wh['warehouse_name'] }}</option>
                                                                                    @endforeach
                                                                                    </select>
                                                                                    @endif
                                                                                </span>
                                                                                <!-- <a class="btn btn-orange" data-toggle="modal" data-target="#productVariantModal" id="add-variant"><i class="fa fa-plus"></i> Tambah Variasi</a> -->
                                                                                <a class="btn btn-orange" id="add-variant"><i class="fa fa-plus"></i> Tambah Variasi</a>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>

                                                                        <div class="panel-body">
                                                                            <div class="border-bottom-gray"></div>
                                                                            <div class="table-no-datatable no-padding res-scroll-md" style="width:auto">
                                                                                <table class="table display w-100 table-brown border-head" id="productVariantTable">
                                                                                    <thead class="bg-darkgrey w-100">
                                                                                        <tr>
                                                                                            <th>{{ __('field.action') }}</th>
                                                                                            <th {{ Request::get('product') == null ? 'style=display:none' : '' }}>Index</th>
                                                                                            <th style="min-width: 200px">{{ __('field.product_name') }}</th>
                                                                                            <!-- <th>{{ __('field.dim_length') }} (cm)</th>
                                                                                            <th>{{ __('field.dim_width') }} (cm)</th>
                                                                                            <th>{{ __('field.dim_height') }} (cm)</th> -->
                                                                                            <th>{{ __('field.product_picture') }}</th>
                                                                                            <th>{{ __('field.product_weight') }}</th>
                                                                                            <th>{{ __('field.uom') }}</th>
                                                                                            <th>{{ __('field.bruto') }}</th>
                                                                                            @if(Request::get('product') != null)
                                                                                            <!-- <th>{{ __('field.warehouse_price') }}</th>
                                                                                            <th>{{ __('field.minimum_stock') }}</th> -->
                                                                                            @endif
                                                                                            <th>{{ __('field.stockable_flag') }}</th>
                                                                                            <th style="min-width: 200px;">Detail Gudang</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody></tbody>
                                                                                </table>
                                                                            </div>
                                                                            <!-- @if(Request::get('product') != null)
                                                                            <button type="button" class="btn btn-orange float-right" id="changeVariantPriceButton">Simpan Varian</button>
                                                                            @endif -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="col-md-12 no-padding">
                                                                        <div class="panel-heading no-padding-bottom">
                                                                            <div class="border-bottom-gray"></div>
                                                                            <h4 class="panel-title text-blue-700 text-bold">Sinkronisasikan dengan Ecommerce</h4>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <div class="border-bottom-gray"></div>
                                                                            @if(count($ecommerce) > 0)
                                                                                @for($b = 0; $b < count($ecommerce); $b++)
                                                                                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                                                                                        <div class="ecommerce-check">
                                                                                            <div class="checkbox">
                                                                                                <label>
                                                                                                    <input type="checkbox" name="active_ecommerce_flag[]" value="{{ $ecommerce[$b]['organization_id'] }}" data-toggle="toggle" class="my_switch" checked>{{ $ecommerce[$b]['organization_name'] }}
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                        @if($ecommerce[$b]['organization_id'] == 28)
                                                                                            <div class="form-group">
                                                                                                <label for="">Etalase Tokopedia</label>
                                                                                                <select name="etalase" id="etalase" class="form-control single-select">
                                                                                                @for($e = 0; $e < count($etalase); $e++)
                                                                                                    <option value="{{ $etalase[$e]['etalase_id'] }}" {{ isset($product_ecommerce_by_id) && $product_ecommerce_by_id != null && $product_ecommerce_by_id['etalase_id'] == $etalase[$e]['etalase_id'] ? 'selected' : '' }}>{{ $etalase[$e]['etalase_name'] }}</option>
                                                                                                @endfor
                                                                                                </select>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label for="">Kategori Tokopedia</label>
                                                                                                <select name="tokopedia_category" id="tokopedia_category" class="form-control single-select">
                                                                                                @for($e = 0; $e < count($tokopedia_category); $e++)
                                                                                                    <option value="{{ $tokopedia_category[$e]['category_id_ecommerce'] }}" {{ $tokopedia_category[$e]['category_id_ecommerce'] == $tokopedia_category[$e]['tokped_category_id'] ? 'selected' : '' }}>{{ $tokopedia_category[$e]['category_name'] }}</option>
                                                                                                @endfor
                                                                                                </select>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>

                                                                                    @if($b < count($ecommerce) - 1)
                                                                                        <div class="clearfix"></div>
                                                                                    @endif
                                                                                @endfor
                                                                            @else
                                                                            <span>{{ __('page.no_data') }}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="col-md-12 no-padding">
                                                                        <div class="card-footer bg-transparent ">
                                                                                <div class="col-md-4 no-padding float-right">
                                                                                    <div class="col-xs-6 padding-r-10">
                                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                                    </div>
                                                                                    <div class="col-xs-6 padding-l-10">
                                                                                        <button type="submit" class="btn btn-orange btn-block " id="submitProductForm">{{ __('page.submit') }}</button>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </form>
                                                        <form action="{{ route('admin/product/changeVariantPricePerWarehouse') }}" id="productVariantPriceForm" method="POST">
                                                            @csrf
                                                        </form>
                                                        @endif  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                           
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    

    <!-- Modal Add Variasi -->
    <div class="modal fade" id="addProductVariantModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="varianProductLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content res-lg-modal">
                <div class="modal-header">
                    <h5 class="modal-title" id="varianProductLabel">Tambah Variasi Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                    
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin/product/variant') }}" id="addProductVariantForm" method="POST">
                        @csrf
                        @if(Request::get('product') != null)
                            <input type="hidden" name="parent_id" value="{{ Request::get('product') }}">
                            <input type="hidden" name="product_brand" value="{{ $product_based_on_id['brand_id'] }}">
                            <input type="hidden" name="product_image_temp" value="{{ $product_based_on_id['prod_image'] }}">
                            <input type="hidden" name="product_sku" value="{{ $product_based_on_id['product_sku_id'] }}">
                            <input type="hidden" name="shipping_durability_min" value="{{ $product_based_on_id['shipping_durability_min'] }}">
                            <input type="hidden" name="shipping_durability_max" value="{{ $product_based_on_id['shipping_durability_max'] }}">
                            <input type="hidden" name="shipping_durability_unit" value="{{ $product_based_on_id['shipping_durability_id'] }}">
                            <input type="hidden" name="minimum_order" value="{{ $product_based_on_id['minimum_order'] }}">
                            <input type="hidden" name="category_id" value="{{ $product_based_on_id['category_id'] }}">
                            <input type="hidden" name="is_taxable_product" value="{{ $product_based_on_id['is_taxable_product'] }}">
                            <input type="hidden" name="tax_value" value="{{ $product_based_on_id['tax_value'] * 100 }}">
                            @if(Request::get('product') != null)
                                @for($b = 0; $b < count($tags_by_id); $b++)
                                    <input type="hidden" name="product_tag[]" value="{{ $tags_by_id[$b]['tag_id'] }}">
                                @endfor
                                @foreach($warehouse_list as $wh_list)
                                    <input type="hidden" name="product_warehouse[]" value="{{ $wh_list->warehouse_id }}">
                                    <input type="hidden" name="warehouse_price[]" value="{{ $wh_list->prod_price }}">
                                    <input type="hidden" name="warehouse_min_stock[]" value="{{ $wh_list->minimum_stock }}">
                                @endforeach
                            @endif
                        @endif
                        <div class="col-md-12 res-tbl-btn no-padding">
                            <div class="panel-heading no-padding-bottom">
                                <!-- <h4 class="panel-title text-blue-700 text-bold float-left">Variasi Produk</h4> -->
                                <div class="float-right">
                                    <!-- <a class="btn btn-orange" data-toggle="modal" data-target="#productVariantModal" id="add-variant"><i class="fa fa-plus"></i> Tambah Variasi</a> -->
                                    <a class="btn btn-orange" id="add-product-variant"><i class="fa fa-plus"></i> Tambah Variasi</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <table id="productVariantList" class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('field.action') }}</th>
                                        <th>{{ __('field.product_name') }}</th>
                                        <th>{{ __('field.product_picture') }}</th>
                                        <!-- <th>{{ __('field.dim_length') }} (cm)</th>
                                        <th>{{ __('field.dim_width') }} (cm)</th>
                                        <th>{{ __('field.dim_height') }} (cm)</th> -->
                                        <th>{{ __('field.product_weight') }}</th>
                                        <th>{{ __('field.uom') }}</th>
                                        <th>{{ __('field.bruto') }}</th>
                                        <th>{{ __('field.stockable_flag') }}</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 no-padding margin-t10">
                        <div class="col-md-6 padding-r-10 res-btn-mid">
                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="button" class="btn btn-orange btn-block" id="submitVariantModalButton">{{ __('page.submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="productVariantModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="varianProductLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="varianProductLabel">Ubah Variasi Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                    
                </div>
                <div class="modal-body">
                    <form method="POST" id="productVariantForm" enctype="multipart/form-data" action="{{ route('admin/product/form') }}">
                        @csrf
                        @if(Request::get('product') != null)
                            <input type="hidden" name="product_image_temp" value="{{ $product_based_on_id['prod_image'] }}">
                        @endif
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding d-none">
                                <div class="form-group margin-b10">
                                    <label for="product_name">{{ __('field.product_name') }} <span class="required-label">*</span></label>
                                    <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" @isset($product_based_on_id) value="{{ $product_based_on_id->prod_name }}" @endisset>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding d-none">
                                <div class="form-group margin-b10">
                                    <label for="dim_length">{{ __('field.product_description') }} </label>
                                    <textarea name="product_description" class="form-control @error('product_description') is-invalid @enderror" style="resize:none">@isset($product_based_on_id){{ $product_based_on_id->prod_desc }}@endisset</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" style="display:none">
                                <div class="form-group select-box">
                                    <label for="product_tag">{{ __('field.product_tag') }} <span class="required-label">*</span></label>
                                    <div class="clearfix"></div>
                                    <select class="select-state multi-select" name="product_tag[]"  placeholder="{{ __('page.choose_tag') }}..." multiple="multiple">
                                        <option value="">{{ __('page.choose_tag') }}...</option>
                                        @for($b = 0; $b < count($tags); $b++)
                                            <option value="{{ $tags[$b]['tag_id'] }}" {{ Request::get('product') !== null ? (count($tags_by_id) == 0 ? '' : (array_search($tags[$b]['tag_id'], array_column($tags_by_id,'tag_id')) !== false ? 'selected' : '')) : '' }}>{{ $tags[$b]['tag_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" style="display:none">
                                <div class="form-group">
                                    <label for="product_category">{{ __('field.category_name') }} <span class="required-label">*</span></label>
                                    <select name="product_category" class="form-control single-select @error('product_category') is-invalid @enderror">
                                        <option value=""></option>
                                        @for($b = 0; $b < count($category); $b++)
                                            <option value="{{ $category[$b]['category_id'] }}" @if(Request::get('product') == null) @if($category[$b]['category_id'] == old('product_category')) selected @endif @else @if($category[$b]['category_id'] == $product_based_on_id['category_id']) selected @endif @endif>{{ $category[$b]['category_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" style="display:none">
                                <div class="form-group">
                                    <label for="product_brand">{{ __('field.product_brand') }}</label>
                                    <select name="product_brand" class="form-control single-select @error('product_brand') is-invalid @enderror">
                                        <option value=""></option>
                                        @for($b = 0; $b < count($brand); $b++)
                                            <option value="{{ $brand[$b]['brand_id'] }}" @if(Request::get('product') == null) @if($brand[$b]['brand_id'] == old('product_brand')) selected @endif @else @if($brand[$b]['brand_id'] == $product_based_on_id['brand_id']) selected @endif @endif>{{ $brand[$b]['brand_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" style="display:none">
                                <div class="form-group">
                                    <label for="minimum_stock">{{ __('field.minimum_stock') }} <span class="required-label">*</span></label>
                                    <input type="text" class="form-control @error('minimum_stock') is-invalid @enderror" name="minimum_stock" value="{{ Request::get('product') == null ? old('minimum_stock') : $product_based_on_id['minimum_stock'] }}">
                                </div>
                            </div>
                            <div class="col-md-12 no-padding" style="display:none">
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group padding-r-10">
                                        <label for="is_taxable_product">{{ __('field.is_taxable_product') }} <span class="required-label">*</span></label>
                                        <select class="form-control @error('is_taxable_product') is-invalid @enderror" name="is_taxable_product">
                                            <option value="">{{ __('page.choose') }}</option>
                                            <option value="Y" {{ Request::get('product') == null ? old('is_taxable_product') : ($product_based_on_id['is_taxable_product'] == 'Y' ? 'selected' : '') }}>{{ __('notification.yes') }}</option>
                                            <option value="N" {{ Request::get('product') == null ? old('is_taxable_product') : ($product_based_on_id['is_taxable_product'] == 'N' ? 'selected' : '') }}>{{ __('notification.no') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group">
                                        <label for="tax_value">{{ __('field.tax_value') }} <span class="required-label">*</span></label>
                                        <input type="number" name="tax_value" class="form-control @error('tax_value') is-invalid @enderror" value="{{ Request::get('product') == null ? 0 : ($product_based_on_id['tax_value'] * 100) }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="parent_product" value="{{ Request::get('product') == null ? '' : Request::get('product') }}">
                            <input type="hidden" name="product_sku" @if(Request::get('product') != null) value="{{ $product_based_on_id['product_sku_id'] }}" @endif>
                            <div class="col-md-12 no-padding d-none">
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group margin-r-10">
                                        <label for="dim_length">{{ __('field.dim_length') }} (cm)</label>
                                        <input type="numeric" name="dim_length" class="form-control @error('dim_length') is-invalid @enderror" value="">
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group margin-r-10">
                                        <label for="dim_width">{{ __('field.dim_width') }} (cm)</label>
                                        <input type="numeric" name="dim_width" class="form-control @error('dim_width') is-invalid @enderror" value="">
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group margin-b10">
                                        <label for="dim_height">{{ __('field.dim_height') }} (cm)</label>
                                        <input type="numeric" name="dim_height" class="form-control @error('dim_height') is-invalid @enderror" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group margin-r-10">
                                        <label for="product_weight">{{ __('field.product_weight') }}<span class="required-label">*</span></label>
                                        <input type="numeric" name="product_weight" class="form-control @error('product_weight') is-invalid @enderror satuan_jual" value="">
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group">
                                        <label for="product_uom">{{ __('field.uom') }} <span class="required-label">*</span></label>
                                        <select name="product_uom" id="product_uom" class="form-control single-select @error('product_uom') is-invalid @enderror">
                                            <option value=""></option>
                                            @for($b = 0; $b < count($uom); $b++)
                                                <option value="{{ $uom[$b]['uom_id'] }}">{{ $uom[$b]['uom_name'] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group margin-r-10">
                                        <label for="bruto">{{ __('field.bruto') }} <span class="required-label">*</span></label>
                                        <input type="text" name="bruto" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group ">
                                        <label for="bruto_uom">{{ __('field.bruto_uom') }} <span class="required-label">*</span></label>
                                        <select name="bruto_uom" class="form-control single-select">
                                            <option value=""></option>
                                            @for($b = 0; $b < count($uom); $b++)
                                                <option value="{{ $uom[$b]['uom_id'] }}" {{ strtolower($uom[$b]['uom_name']) == strtolower('kg') ? 'selected' : 'disabled' }}>{{ $uom[$b]['uom_name'] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group margin-r-10">
                                        <label for="minimum_order">{{ __('field.minimum_order') }}</label>
                                        <input type="number" class="form-control" name="minimum_order">
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <div class="form-group ">
                                        <label for="stockable_flag">{{ __('field.stockable_flag') }}</label>
                                        <select name="stockable_flag" class="form-control @error('stockable_flag') is-invalid @enderror" id="stockable_flag">
                                            <option value="1">{{ __('notification.yes') }}</option>
                                            <option value="0">{{ __('notification.no') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <h6 class="d-none" style="font-weight:bold">{{ __('page.shipping_durability') }}</h6>
                            <div class="col-md-12 no-padding d-none">
                                <div class="col-xs-4 padding-r-10">
                                    <div class="form-group">
                                        <label for="shipping_durability_min">{{ __('field.shipping_durability_min') }} </label>
                                        <input type="number" name="shipping_durability_min" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-xs-4 padding-r-10">
                                    <div class="form-group">
                                        <label for="shipping_durability_max">{{ __('field.shipping_durability_max') }} </label>
                                        <input type="number" name="shipping_durability_max" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-xs-4 no-padding">
                                    <div class="form-group">
                                        <label for="shipping_durability_unit">{{ __('field.shipping_durability_unit') }} </label>
                                        <select name="shipping_durability_unit" class="form-control single-select" placeholder="{{ __('page.choose_shipping_durability_unit') }}">
                                        <option value="">{{ __('page.choose_shipping_durability_unit') }}</option>
                                        @for($b = 0; $b < count($shipping_durability); $b++)
                                            <option value="{{ $shipping_durability[$b]['shipping_durability_id'] }}">{{ $shipping_durability[$b]['shipping_durability_name'] }}</option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                @if(Request::get('product') != null)
                                <table class="table display w-100 table-brown border-head datatable-basic" >
                                    <thead class="bg-darkgrey w-100">
                                        <tr>
                                            <th class="text-center">Nama Gudang<span class="required-label">*</span></th>
                                            <th class="text-center" >Harga Produk<span class="required-label">*</span></th>
                                            <th class="text-center">Stok Minimal<span class="required-label">*</span></th>
                                        </tr>
                                    </thead>
                                    <tbody id="modal_prd_wh_item">
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                       
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 no-padding margin-t10">
                        <div class="col-md-6 padding-r-10 res-btn-mid">
                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="button" class="btn btn-orange btn-block" id="submitModalButton">{{ __('page.submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Request::get('product') != null)
    <!-- Modal -->
    <div class="modal fade" id="deletionPopupModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog width-auto modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <h5 class="modal-title" id="cancelPopupModalLabel"></h5> -->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align:center">
                    <span class="product_code_detail" style="font-weight:bold;text-align:center"></span>
                    <div class="clearfix"></div>
                    <span>{{ __('notification.deletion_confirmation') }}</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('notification.no') }}</button>
                    <button type="button" class="btn btn-primary deletion_modal_button" data-dismiss="modal">{{ __('notification.yes') }}</button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var promoTable;
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $.validator.addMethod("product_name_validation", function(value, element){
            return value.includes("-");
        },"Harus ada tanda strip");

        var k = 0;
        var currentIndex = 0;
        var productUomList = $('select[name="product_uom"]:first option');
        var productTable;

        var productUomValues = $.map(productUomList, function(uom){
            return "<option value="+uom.value+">"+uom.text+"</option>";
        })

        @if(Request::get('product') != null)
            k = "{{ $total_variant }}";
            currentIndex = "{{ $total_variant }}";
            k = parseInt(k);
            currentIndex = parseInt(k);
            var warehouseList = "{{ $warehouse_list_64 }}";
            warehouseList = atob(warehouseList);
            // $.each(JSON.parse(warehouseList), function (i, item) {
            //     $(".wh_"+item.warehouse_id).prop('disabled', true);
            // });
            $('.product_price').keyup();
            
            // $('select[name="product_warehouse[]"]').val([warehouseList.split(',')]).trigger('change');
            loadProductTable();

            $('select[name="warehouse_list"]').change(function(){
                loadProductTable($(this).val());
            })

            $('#deletionPopupModal').on('hidden.bs.modal',function(){
                console.log('lalala');
                product_url = "";
                // $('input[name="product_url"]').remove();
            });

            // $('#productVariantTable img.product_variant_image').attr('onerror','this.src="{{ asset("img/default_product.jpg") }}"');
            function loadProductTable(warehouse_id = null){
                console.log("warehouse: "+warehouse_id);
                if(warehouse_id == null || warehouse_id == ""){
                    // $('#changeVariantPriceButton').hide();
                    // $('#submitProductForm').removeClass('d-none');
                    // $('input[name="product_name"]').attr('readonly',false);
                    // $('select[name="product_category"]').siblings('span.select2-container--default').removeClass('pointer-none');
                    // $('select[name="product_brand"]').siblings('span.select2-container--default').removeClass('pointer-none');
                    // $('select[name="product_tag[]"]').siblings('span.select2-container--default').removeClass('pointer-none');
                    // $('input[name="minimum_order"]').attr('readonly',false);
                    // $('select[name="is_taxable_product"]').removeClass('pointer-none');
                    // $('input[name="tax_value"]').attr('readonly',false);
                    // $('textarea[name="product_description"]').attr('readonly',false);
                    // $('input[name="product_weight"]').attr('readonly',false);
                    // $('input[name="bruto"]').attr('readonly',false);
                    // $('select[name="product_uom"]').siblings('span.select2-container--default').removeClass('pointer-none');
                    // $('select[name="product_warehouse[]"]').siblings('span.select2-container--default').removeClass('pointer-none');
                    // $('input[name="warehouse_price[]"]').attr('readonly',false);
                    // $('input[name="warehouse_min_stock[]"]').attr('readonly',false);
                    // $('#product_picture_button').attr('disabled',false);
                    // $('#addwarehouse').attr('disabled',false);
                    $('#add-variant').attr('disabled',false);
                    // $('#product_picture_button').attr('onclick','return false');
                }else{
                    // $('#changeVariantPriceButton').show();
                    // $('#submitProductForm').addClass('d-none');
                    // $('input[name="product_name"]').attr('readonly',true);
                    // $('select[name="product_category"]').siblings('span.select2-container--default').addClass('pointer-none');
                    // $('select[name="product_brand"]').siblings('span.select2-container--default').addClass('pointer-none');
                    // $('select[name="product_tag[]"]').siblings('span.select2-container--default').addClass('pointer-none');
                    // $('input[name="minimum_order"]').attr('readonly',true);
                    // $('select[name="is_taxable_product"]').addClass('pointer-none');
                    // $('input[name="tax_value"]').attr('readonly',true);
                    // $('textarea[name="product_description"]').attr('readonly',true);
                    // $('input[name="product_weight"]').attr('readonly',true);
                    // $('input[name="bruto"]').attr('readonly',true);
                    // $('select[name="product_uom"]').siblings('span.select2-container--default').addClass('pointer-none');
                    // $('select[name="product_warehouse[]"]').siblings('span.select2-container--default').addClass('pointer-none');
                    // $('input[name="warehouse_price[]"]').attr('readonly',true);
                    // $('input[name="warehouse_min_stock[]"]').attr('readonly',true);
                    // $('#product_picture_button').attr('disabled',true);
                    // $('#addwarehouse').attr('disabled',true);
                    $('#add-variant').attr('disabled',true);
                    // $('#product_picture_button').attr('onclick','return true');
                }
                $('#productVariantTable').DataTable().clear().destroy();
                productTable = $('#productVariantTable').on('processing.dt', function ( e, settings, processing ) {
                    $('.dataTables_processing').remove();
                    if(processing){
                        showLoader();
                    }else{
                        hideLoader();
                    }
                }).DataTable({
                    "fnDrawCallback": function() {
                        $('.my_switch').bootstrapToggle();
                    },
                    paging: false,
                    scrollX: true,
                    language: {
                        paginate: {
                            previous: '<i class="fa fa-chevron-left"></i>',
                            next: '<i class="fa fa-chevron-right"></i>'
                        },
                        lengthMenu: '{{ __("page.showing") }} <select name="product_list_length">'+
                            '<option value="5">5</option>'+
                            '<option value="10">10</option>'+
                            '<option value="20">20</option>'+
                            '<option value="25">25</option>'+
                            '<option value="50">50</option>'+
                            '</select> data',
                        emptyTable: '{{ __("page.no_data") }}'
                    },
                    oLanguage: {
                        sSearch: "{{ __('page.search') }}:"
                    },
                    lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:'{{ route("datatable/get_products") }}',
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: function(d){
                            console.log(d);
                            d.parent_product = "{{ Request::get('product') }}";
                            d.warehouse_id  = warehouse_id;
                            d.additional_search = $('#productVariantTable_filter input[type = "search"]').val();
                        }
                        // dataSrc: function (json) {
                        //     console.log( json.recordsTotal );
                        // }
                    },
                    "order": [[ 1, "ASC" ]],
                    columns: [
                        {
                            data: 'action',
                            sortable: false
                        },
                        {
                            data: 'index',
                            name: 'index',
                            visible: false
                        },
                        {
                            data: "prod_name",
                            name: "prod_name",
                            render: function (data, type, row, meta) {
                                return "<input type='text' "+(warehouse_id == null || warehouse_id == '' ? '' : 'readonly')+" class='form-control' name='product_variant_name[]' value='"+data+"'><input type='hidden' name='product_variant_id[]' value='"+row['prod_id']+"'><input type='hidden' name='product_warehouse_main_index[]' value='"+row['index']+"'>";
                            }
                        },
                        {
                            data: 'prod_image',
                            render: function(data, type, row, meta){
                                // return '<input type="file" name="product_variant_picture[]"><input type="hidden" name="product_variant_picture_temp[]" value="'+data+'">';
                                return '<img src="'+'/'+data+'" onerror="imgError(this)" class="product_variant_image">'+(warehouse_id == null || warehouse_id == '' ? '<input type="file" name="product_variant_picture[]" class="d-none">' : '')+'<input type="hidden" name="product_variant_picture_temp[]" value="'+data+'">';
                            }
                        },
                        // {
                        //     data: "dim_length",
                        //     name: "dim_length",
                        //     render: function (data, type, row, meta) {
                        //         return "<input type='number' class='form-control' name='product_variant_length[]' value='"+data+"'>";
                        //     }
                        // },
                        // {
                        //     data: "dim_width",
                        //     name: "dim_width",
                        //     render: function (data, type, row, meta) {
                        //         return "<input type='number' class='form-control' name='product_variant_width[]' value='"+data+"'>";
                        //     }
                        // },
                        // {
                        //     data: 'dim_height',
                        //     name: 'dim_height',
                        //     render: function (data, type, row, meta) {
                        //         return "<input type='number' class='form-control' name='product_variant_height[]' value='"+data+"'>";
                        //     }
                        // },
                        {
                            data: 'uom_value',
                            name: 'uom_value',
                            render: function (data, type, row, meta) {
                                return "<input type='text' class='form-control' name='product_variant_uom_value[]' "+(warehouse_id == null || warehouse_id == '' ? '' : 'readonly')+" value='"+data+"'>";
                            }
                        },
                        {
                            data: 'uom_name',
                            name: 'uom_name',
                            render: function (data, type, row, meta) {
                                productUomValues = $.map(productUomList, function(uom){
                                    return "<option value="+uom.value+" "+(uom.value == row['uom_id'] ? 'selected' : '')+">"+uom.text+"</option>";
                                });
                                console.log(data);
                                return "<select class='form-control single-select "+(warehouse_id == null || warehouse_id == '' ? '' : 'pointer-none')+"' name='product_variant_uom_name[]'>"+productUomValues+"</select>";
                            } 
                        },
                        {
                            data: 'bruto_value',
                            name: 'bruto_value',
                            render: function (data, type, row, meta) {
                                return "<input type='text' class='form-control' name='bruto_value[]' value='"+(data == null ? "" : data)+"' "+(warehouse_id == null || warehouse_id == '' ? '' : 'readonly')+">";
                            }
                        },
                        // {
                        //     data: 'warehouse_price',
                        //     name: 'warehouse_price',
                        //     render: function (data, type, row, meta) {
                        //         return "<input type='text' class='form-control' name='variant_warehouse_price[]' "+(warehouse_id == null ? 'readonly' : '')+" value='"+(data == null ? "" : data)+"'><input type='hidden' name='variant_highest_price[]' value='"+(row['warehouse_highest_price'])+"'>";
                        //     }
                        // },
                        // {
                        //     data: 'warehouse_minimum_stock',
                        //     name: 'warehouse_minimum_stock',
                        //     render: function (data, type, row, meta) {
                        //         return "<input type='text' class='form-control' name='variant_warehouse_minimum_stock[]' value='"+(data == null ? "" : data)+"' "+(warehouse_id == null || warehouse_id == '' ? 'readonly' : '')+">";
                        //     }
                        // },
                        {
                            data: 'stockable_flag',
                            name: 'stockable_flag',
                            render: function(data, type, row, meta){
                                return "<select name='stockable_value_flag[]' class='"+(warehouse_id == null || warehouse_id == '' ? '' : 'pointer-none')+"'><option value='1' "+(data == 1 ? 'selected' : '')+">{{ __('notification.yes') }}</option><option value='0' "+(data == 0 ? 'selected' : '')+">{{ __('notification.no') }}</option></select>";
                            }
                        },
                        {
                            data: 'warehouse_detail',
                            name: 'warehouse_detail',
                            render: function(data, type, row, meta){
                                console.log(data);
                                var warehouse_detail_div = "";
                                for (let b = 0; b < data.length; b++) {
                                    // const element = array[b];
                                    warehouse_detail_div += "<span class='d-block margin-tb10 product_warehouse_name' style='display:block'>"+data[b]['warehouse_name']+"</span><input name='product_warehouse_price[]' onkeyup='currencyFormat(this)' type='text' class='form-control d-block margin-tb10 "+(data[b]['warehouse_type_id'] == 1 ? 'pointer-none' : '')+"' value='"+(data[b]['warehouse_type_id'] == 1 ? 0 : (data[b]['prod_price'] == null ? '' : data[b]['prod_price']))+"' placeholder='Harga per gudang' "+(data[b]['warehouse_type_id'] == 1 ? 'readonly' : '')+"><input type='text' name='product_warehouse_minimum_stock[]' class='form-control d-block margin-b10 "+(data[b]['warehouse_type_id'] == 1 ? 'pointer-none' : '')+"' value='"+(data[b]['warehouse_type_id'] == 1 ? 0 : (data[b]['minimum_stock'] == null ? '' : data[b]['minimum_stock']))+"' placeholder='Stok minimum' "+(data[b]['warehouse_type_id'] == 1 ? 'readonly' : '')+"><input type='hidden' name='product_warehouse_index[]' class='form-control' value='"+data[b]['index']+"'><input type='hidden' name='product_warehouse_id[]' class='form-control' value='"+data[b]['warehouse_id']+"'>";
                                }
                                return warehouse_detail_div;
                            }
                        }
                    ],
                    fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                        return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
                    }
                    // fnDrawCallback: function () {
                    //     console.log(this.api().page.info().recordsTotal);
                    //     k = this.api().page.info().recordsTotal;
                    //     console.log(k);
                    // }
                });

                // k = productTable.page.info().recordsTotal;
                // k = productVariantTable
                // k = $('#productVariantTable tbody tr').length;
                console.log('jml kolom: '+k);
                // $('select[name="stockable_value_flag[]"]').addClass('pointer-none');

                var current_active_status = 2;
            
                productTable.on('click', 'img.product_variant_image', function(){
                    var index = $('#productVariantTable img.product_variant_image').index(this);
                    $('#productVariantTable input[name="product_variant_picture[]"]').eq(index).click();
                    // console.log(index);
                });

                productTable.on('click','.deleteProductVariant',function(){
                    // var index = $('#productVariantTable .deleteProductVariant').index(this);
                    // console.log('deleted index: '+index);
                    // $('table#productVariantTable tbody tr').eq(index).remove();
                    $('.deleteProductVariant').closest('tr').remove();
                })

                var product_url = "";
                productTable.on('click','a.productDeletionButton',function(){
                    // console.log('aaaa');
                    // var product_name = 
                    var index = $('a.productDeletionButton').index(this);
                    var data = productTable.data();
                    var product_code = data[index]['prod_code'];
                    product_url = $(this).data('url');
                    $('span.product_code_detail').html(product_code);
                    // $('<input name="product_url" type="hidden" value="'+url+'">').insertAfter('span.product_code_detail');
                });

                $('.deletion_modal_button').click(function(){
                    if(product_url != ""){
                        window.location.href = product_url;
                        showLoader();
                    }
                });

                productTable.on('click', 'div.checkbox', function(){
                    var index = $('div.checkbox').index(this);
                    var data = productTable.data();
                    // if(active)
                    var active_status = data[index]['product_active_status'];
                    if(current_active_status == 2){
                        current_active_status = active_status;
                    }
                    if(current_active_status == 1){
                        current_active_status = 0;
                    }else{
                        current_active_status = 1;
                    }
                    console.log(current_active_status);
                    $('input[name="product_active_status[]"]').eq(index).val(current_active_status);
                    // console.log(active_status);
                });

                productTable.on('change','input[name="product_variant_picture[]"]', function(e){
                    var fileName = e.target.files[0].name;
                    console.log('The file "' + fileName +  '" has been selected.');

                    var index = $('#productVariantTable input[name="product_variant_picture[]"]').index(this);
                    console.log('index: '+index);
                    readURL(this,'img.product_variant_image:eq('+index+')');
                    // $('input[name="product_variant_picture[]"]').change(function(e){
                    // });
                })

                productTable.on('click','.editProductVariantButton',function(){
                    showLoader();
                    var index = $('.editProductVariantButton').index(this);
                    var data = productTable.data();
                    console.log(data);
                    console.log(index);
                    var dim_length = data[index]['dim_length'];
                    var dim_width = data[index]['dim_width'];
                    var dim_height = data[index]['dim_height'];
                    var uom_value = data[index]['uom_value'];
                    var uom_id = data[index]['uom_id'];
                    var product_price = data[index]['prod_price'];
                    var product_id = data[index]['prod_id'];
                    var parent_product = data[index]['variant_id'];
                    var minimum_order = data[index]['minimum_order'];
                    var stockable_flag = data[index]['stockable_flag'];
                    var product_sku = data[index]['product_sku_id'];
                    var shipping_durability_min = data[index]['shipping_durability_min'];
                    var shipping_durability_max = data[index]['shipping_durability_max'];
                    var shipping_durability_unit_id = data[index]['shipping_durability_unit_id'];
                    var product_name = data[index]['prod_name'];
                    var product_description = data[index]['prod_desc'];
                    var bruto = data[index]['bruto'];
                    console.log(parent_product);
                    console.log(product_sku);
                    console.log(product_id);
                    console.log(minimum_order);
                    $('<input type="hidden" name="product_id" value="'+product_id+'">').insertAfter('#productVariantModal input[name="_token"]');
                    $('#productVariantModal input[name="dim_length"]').val(dim_length);
                    $('#productVariantModal input[name="product_name"]').val(product_name);
                    $('#productVariantModal textarea[name="product_description"]').val(product_description);
                    $('#productVariantModal select[name="stockable_flag"]').val(stockable_flag).trigger('change');
                    $('#productVariantModal input[name="dim_width"]').val(dim_width);
                    $('#productVariantModal input[name="dim_height"]').val(dim_height);
                    $('#productVariantModal input[name="minimum_order"]').val(minimum_order);
                    $('#productVariantModal input[name="product_weight"]').val(uom_value);
                    $('#productVariantModal select[name="product_uom"]').val(uom_id).trigger('change');
                    $('#productVariantModal input[name="product_price"]').val(product_price.replace('Rp. ',''));
                    $('#productVariantModal input[name="bruto"]').val(bruto.replace(' kg',''));
                    $('#productVariantModal input[name="parent_product"]').val(parent_product);
                    $('#productVariantModal input[name="product_sku"]').val(product_sku);
                    $('#productVariantModal input[name="shipping_durability_min"]').val(shipping_durability_min);
                    $('#productVariantModal input[name="shipping_durability_max"]').val(shipping_durability_max);
                    $('#productVariantModal select[name="shipping_durability_unit"]').val(shipping_durability_unit_id).trigger("change");
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/admin/product/prodWh') }}/"+product_id,
                        success: function (r) {
                            console.log(r);
                            var html = '';
                            $.each(r, function (i, item) {
                                var key = i+1;
                                html += '<tr class="prd_wh_item_'+key+' prod_wh">';
                                html += '<td>';
                                html += '<div class="form-group" style="margin: 0 !important;">';
                                html += '<input type="hidden" name="product_warehouse[]" value="'+item.warehouse_id+'">';
                                html += '<input type="text" readonly="readonly" class="form-control product_warehouse_name" name="product_warehouse_name[]" value="'+item.warehouse_name+'">';
                                html += '</div>';
                                html += '</td>';
                                html += '<td>';
                                html += '<div class="form-group" style="margin: 0 !important;">';
                                html += '<input type="text" class="form-control product_price" onkeyup="currencyFormat(this)" name="warehouse_price[]" value="'+item.prod_price+'">';
                                html += '</div>';
                                html += '</td>';
                                html += '<td>';
                                html += '<div class="form-group" style="margin: 0 !important;">';
                                html += '<input type="text" class="form-control product_price" onkeyup="currencyFormat(this)" name="warehouse_min_stock[]" value="'+item.minimum_stock+'">';
                                html += '</div>';
                                html += '</td>';
                                html += '</tr>';
                            });
                            $('#modal_prd_wh_item').append(html);
                            $('.product_price').keyup();
                            hideLoader();
                        }
                    });
                });
            }
            function imgError(image) {
                image.onerror = "";
                image.src = "{{ asset('img/default_product.jpg') }}";
                return true;
            }
            $('#changeVariantPriceButton').click(function(){
                var product_variant_ids = $('#productVariantTable input[name="product_variant_id[]"]').length;
                // console.log(product_variant_ids);
                var product_variant_list = "<input type='hidden' name='warehouse_list' value='"+($('select[name="warehouse_list"]').val())+"'><input name='parent_product_id' type='hidden' value='{{ Request::get('product') }}'><input name='parent_product_name' type='hidden' value='"+($('input[name="product_name"]').val())+"'><input name='bruto' type='hidden' value='"+($('input[name="bruto"]').val())+"'><input name='tokopedia_category' type='hidden' value='"+($('select[name="tokopedia_category"]').val())+"'><input name='product_description' type='hidden' value='"+($('textarea[name="product_description"]').val())+"'><input name='product_code' type='hidden' value='"+($('input[name="product_code"]').val())+"'>";
                var parent_warehouse_price = $('input[name="warehouse_price[]"]').length;
                var price = 0;
                for (let b = 0; b < parent_warehouse_price; b++) {
                    // const element = array[b];
                    if($('input[name="warehouse_price[]"]').eq(b).val() > 0 && price == 0){
                        product_variant_list += "<input name='parent_warehouse_price' value='"+($('input[name="warehouse_price[]"]').eq(b).val())+"'>";
                        price = $('input[name="warehouse_price[]"]').eq(b).val();
                    }
                }
                var ecommerce_list = $("input[name='active_ecommerce_flag[]']").length;
                var tokopedia = 0;
                for (let b = 0; b < ecommerce_list; b++) {
                    if($("input[name='active_ecommerce_flag[]']").eq(b).val() == 28){
                        tokopedia = 1;
                    }
                }
                for (let b = 0; b < product_variant_ids; b++) {
                    // const element = array[b];
                    product_variant_list += ("<input type='hidden' name='product_variant_id[]' value='"+($('#productVariantTable input[name="product_variant_id[]"]').eq(b).val())+"'><input type='hidden' name='varian_warehouse_name[]' value='"+($('#productVariantTable input[name="product_variant_name[]"]').eq(b).val())+"'><input type='hidden' name='variant_warehouse_price[]' value='"+($('#productVariantTable input[name="variant_warehouse_price[]"]').eq(b).val())+"'><input type='hidden' name='variant_warehouse_minimum_stock[]' value='"+($('#productVariantTable input[name="variant_warehouse_minimum_stock[]"]').eq(b).val())+"'><input name='variant_name[]' type='hidden' value='"+($('input[name="product_variant_name[]"]').eq(b).val())+"'><input name='product_ecommerce_code' value='"+($('input[name="product_ecommerce_code"]').val())+"'><input name='etalase' type='hidden' value='"+($('select[name="etalase"]').val())+"'>"+(tokopedia == 1 ? "<input type='hidden' name='tokopedia' value='1'>" : "")+"");
                }
                console.log(product_variant_list);
                $(product_variant_list).insertAfter('#productVariantPriceForm input[name="_token"]');
                $('form#productVariantPriceForm').submit();
                showLoader();
            });
            // $('img.product_variant_image').on('error',function(){
            //     // console.log('baby');
            //     $(this).attr('onError','{{ asset("img/default_product.jpg") }}');
            //     $(this).css({'width':'10rem'});
            // })

            $('#productVariantModal').on('hidden.bs.modal', function(){
                $('#productVariantModal input[name="product_id"]').remove();
                $('#productVariantModal input[name="dim_length"]').val("");
                $('#productVariantModal input[name="dim_width"]').val("");
                $('#productVariantModal input[name="parent_product"]').val("{{ Request::get('product') }}");
                $('#productVariantModal input[name="product_name"]').val("{{ Request::get('product') != null ? $product_based_on_id['prod_name'] : '' }}");
                $('#productVariantModal textarea[name="product_description"]').val("");
                $('#productVariantModal input[name="dim_height"]').val("");
                $('#productVariantModal input[name="minimum_order"]').val("");
                $('#productVariantModal input[name="product_weight"]').val("");
                $('#productVariantModal select[name="stockable_flag"]').val("").trigger('change');
                $('#productVariantModal select[name="product_uom"]').val("").trigger('change');
                $('#productVariantModal input[name="product_price"]').val("");
                $('#productVariantModal input[name="bruto"]').val("");
                $('#productVariantModal input[name="product_sku"]').val("{{ Request::get('product') != null ? $product_based_on_id['product_sku_id'] : '' }}");
                $('.prod_wh').remove();
                $('#productVariantModal input[name="shipping_durability_min"]').val("");
                $('#productVariantModal input[name="shipping_durability_max"]').val("");
                $('#productVariantModal select[name="shipping_durability_unit"]').val("").trigger("change");
            })

            // $('.editProductVariantButton').click(function(e){
            //     e.preventDefault();
            //     showLoader();
            //     alert('Test');
            //     // var id = parseInt($(this).attr('data-id'));
            //     // i = id + 1;
            //     // console.log(id+'-'+i);
            //     // $(this).attr('data-id', i);
            //     // var wh_id = $('.inventory_warehouse').val();
                
            //     $.ajax({
            //         type: "GET",
            //         url: "{{ url('/admin/inventory/getProductByWhId') }}/"+wh_id,
            //         success: function (r) {
            //             hideLoader();
            //         }
            //     });
            // });
        @endif
        // $('select[name="product_sku"]').change(function(){
        //     var productName = $("select[name='product_sku'] option:selected").text();
        //     if($(this).val() == ""){
        //         $('input[name="product_name"]').attr('readonly',false);
        //         $('input[name="product_name"]').val("");
        //     }else{
        //         $('input[name="product_name"]').attr('readonly',true);
        //         $('input[name="product_name"]').val(productName);
        //     }
        // });

        $('input[name="product_name"], textarea[name="product_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });
        
        $('#productForm select[name="is_taxable_product"]').change(function(){
            if($(this).val() == "N"){
                $('#productForm input[name="tax_value"]').attr('readonly',false);
                $('#productForm input[name="tax_value"]').val(0);
            }else{
                $('#productForm input[name="tax_value"]').attr('readonly',true);
                $('#productForm input[name="tax_value"]').val(0);
            }
        })

        $.validator.addMethod("greaterThan",
            function (value, element, param) {
            //     var $otherElement = $(param);
            //     console.log(value);
            //     console.log($otherElement.val());
            return parseFloat(value) > 0;
            //     return parseInt(value, 10) >= parseInt($otherElement.val(), 10);
        },"must be greater than ...");

        var shipping_durability_max_error_msg = "{{ __('validation.gte.numeric',['attribute' => __('validation.attributes.shipping_durability_max')]) }}";
        // );

        @if(Request::get('product') == null)
        var validationRules = {
            product_name: {
                required: true,
                product_name_validation: true
                // minlength: 5,
                // alpha: true
            },
            product_weight: {
                required: true
            },
            product_uom: {
                required: true
            },
            product_price: {
                required: true
            },
            // product_stock: {
            //     required: true
            // },
            product_description: {
                required: true,
                maxlength: 500,
                // minlength: 10,
            },
            // minimum_stock: {
            //     required: true
            // },
            'product_tag[]': {
                required: true
            },
            'product_warehouse[]': {
                required: true
            },
            'warehouse_price[]': {
                required: true,
                // min: 1
                // min: function(element){
                //     var product_warehouse_name = $('select[name="product_warehouse[]"]').length;

                //     for (let b = 0; b < product_warehouse_name.length; b++) {
                //         // const element = array[b];
                //         if($('warehouse_type[]"]').eq(b).val() == 2){
                //             return 1;
                //         }
                //     }
                // }
            },
            'warehouse_min_stock[]': {
                required: true,
                // min: 1
                // min: function(element){
                //     var product_warehouse_name = $('select[name="product_warehouse[]"]').length;

                //     for (let b = 0; b < product_warehouse_name.length; b++) {
                //         // const element = array[b];
                //         if($('warehouse_type[]"]').eq(b).val() == 2){
                //             return 1;
                //         }
                //     }
                // }
            },
            'product_category': {
                required: true
            },
            product_picture: {
                required: true,
                extension: "png|pneg|svg|jpg|jpeg",
                maxsize: 2000000
            },
            is_taxable_product: {
                required: true
            },
            minimum_order: {
                required: true
            },
            tax_value: {
                required: true,
                max: 100
            },
            bruto: {
                required: true,
                greaterThan: '0'
            },
            bruto_uom: {
                required: true
            }
        };
        var validationMessages = {
            product_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.product_name'), 'min' => 5]) }}",
                // alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.product_name')]) }}"
            },
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            product_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.uom')]) }}"
            },
            product_price: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}"
            },
            // product_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_stock')]) }}"
            // },
            product_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_description')]) }}",
                maxlength: "{{ __('validation.max.string',['attribute' => __('validation.attributes.product_name'), 'max' => 500]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.product_description'), 'min' => 10]) }}",
            },
            // minimum_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}"
            // },
            'product_tag[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_tag')]) }}"
            },
            'product_warehouse[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_warehouse')]) }}"
            },
            'warehouse_price[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}",
                // min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.price'), 'min' => 1]) }}"
            },
            'warehouse_min_stock[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}",
                // min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.minimum_stock'), 'min' => 1]) }}"
            },
            'product_category': {
                 required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_category')]) }}"
            },
            product_picture: {
                required: "{{ __('validation.custom.product_image.required',['attribute' => __('validation.attributes.product_picture')]) }}",
                extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.product_picture'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
            },
            is_taxable_product: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.is_taxable_product')]) }}"
            },
            minimum_order: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_order')]) }}"
            },
            tax_value: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.tax_value')]) }}",
                max: "{{ __('validation.max.numeric',['attribute' => __('validation.attributes.tax_value'), 'max' => 100]) }}"
            },
            bruto: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto')]) }}",
                greaterThan: "{{ __('validation.gt.numeric',['value' => 0,'attribute' => __('validation.attributes.bruto')]) }}"
            },
            bruto_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto_uom')]) }}"
            }
        }
        @else
        // $('input[name="product_price"]').val(currencyFormat(document.getElementsByName('product_price')[0]));
        
        var validationRules = {
            product_name: {
                required: true,
                product_name_validation: true
            },
               
            product_weight: {
                required: true
            },
            product_uom: {
                required: true
            },
            product_price: {
                required: true
            },
            // product_stock: {
            //     required: true                
            // },
            product_description: {
                required: true
            },
            // minimum_stock: {
            //     required: true
            // },
            minimum_order: {
                required: true
            },
            'product_tag[]': {
                required: true
            },
            'product_warehouse[]': {
                required: true
            },
            'warehouse_price[]': {
                required: true,
                // min: 1
            },
            'warehouse_min_stock[]': {
                required: true,
                // min: 1
            },
            'product_category': {
                required: true
            },
            is_taxable_product: {
                required: true
            },
            tax_value: {
                required: true,
                max: 100
            },
            bruto: {
                required: true
            },
            bruto_uom: {
                required: true
            }
        };
        var validationMessages = {
            product_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_name')]) }}"
            },
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            product_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.uom')]) }}"
            },
            product_price: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}"
            },
            // product_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_stock')]) }}"
            // },
            product_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_description')]) }}"
            },
            // minimum_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}"
            // },
            'product_tag[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_tag')]) }}"
            },
            'product_warehouse[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_warehouse')]) }}"
            },
            'warehouse_price[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}",
                // min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.price'), 'min' => 1]) }}"
            },
            'warehouse_min_stock[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}",
                // min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.minimum_stock'), 'min' => 1]) }}"
            },
            'product_category': {
                 required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_category')]) }}"
            },
            is_taxable_product: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.is_taxable_product')]) }}"
            },
            minimum_order: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_order')]) }}"
            },
            tax_value: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.tax_value')]) }}",
                max: "{{ __('validation.max.numeric',['attribute' => __('validation.attributes.tax_value'), 'max' => 100]) }}"
            },
            bruto: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto')]) }}"
            },
            bruto_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto_uom')]) }}"
            }
        }

        var varianFormErrorRules = {
            product_name: {
                required: true
            },
            product_weight: {
                required: true
            },
            product_uom: {
                required: true
            },
            product_price: {
                required: true
            },
            // product_stock: {
            //     required: true                
            // },
            product_description: {
                required: true
            },
            // minimum_stock: {
            //     required: true
            // },
            'product_tag[]': {
                required: true
            },
            'product_warehouse[]': {
                required: true
            },
            'warehouse_price[]': {
                required: true,
                min: 1
            },
            'warehouse_min_stock[]': {
                required: true,
                min: 1
            },
            'product_category': {
                required: true
            },
            is_taxable_product: {
                required: true
            },
            minimum_order: {
                required: true
            },
            tax_value: {
                required: true,
                max: 100
            },
            bruto: {
                required: true
            },
            bruto_uom: {
                required: true
            }
        };
        var varianFormErrorMessages = {
            product_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_name')]) }}"
            },
            product_weight: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_weight')]) }}"
            },
            product_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.uom')]) }}"
            },
            product_price: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}"
            },
            // product_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_stock')]) }}"
            // },
            product_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_description')]) }}"
            },
            // minimum_stock: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}"
            // },
            'product_tag[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_tag')]) }}"
            },
            'product_warehouse[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_warehouse')]) }}"
            },
            'warehouse_price[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_price')]) }}",
                min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.price'), 'min' => 1]) }}"
            },
            'warehouse_min_stock[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_stock')]) }}",
                min: "{{ __('validation.min.numeric',['attribute' => __('validation.attributes.minimum_stock'), 'min' => 1]) }}"
            },
            'product_category': {
                 required: "{{ __('validation.required',['attribute' => __('validation.attributes.product_category')]) }}"
            },
            is_taxable_product: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.is_taxable_product')]) }}"
            },
            minimum_order: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.minimum_order')]) }}"
            },
            tax_value: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.tax_value')]) }}",
                max: "{{ __('validation.max.numeric',['attribute' => __('validation.attributes.tax_value'), 'max' => 100]) }}"
            },
            bruto: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto')]) }}"
            },
            bruto_uom: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.bruto_uom')]) }}"
            }
        }

        $('#submitModalButton').click(function(){
            $('form#productVariantForm').submit();
        });

        $('form#productVariantForm').validate({
            errorClass: 'invalid-feedback',
            rules: varianFormErrorRules,
            messages: varianFormErrorMessages,
            ignore: [],
            submitHandler: function(form) {
                var flag = 0;
                var indexx = 0;
                // var product_warehouse_name = $('select[name="product_warehouse[]"]').length;

                //     for (let b = 0; b < product_warehouse_name.length; b++) {
                //         // const element = array[b];
                //         if($('warehouse_type[]"]').eq(b).val() == 2){
                //             return 1;
                //         }
                //     }
                $('form#productVariantForm input[name="warehouse_price[]"]').each(function()
                {
                      	if($(this).val() == "" || ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() < 1)){
                            flag = 1;
                        }
                        indexx++;
                });

                indexx = 0;
                $('form#productVariantForm input[name="warehouse_min_stock[]"]').each(function()
                {
                      	if($(this).val() == "" || ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() < 1)){
                            flag = 1;
                        }
                        indexx++;
                });

                if(flag == 1){
                    alert('Field harga/minimum stok per gudang tidak boleh kurang dari satu jika tipe gudang adalah gudang penjualan');
                }else{
                    form.submit();
                    showLoader();
                }
            }
        });

        @endif
        $('form#productForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function(form) {
                var flag = 0;
                var indexx = 0;
                var warehouse_type = 2;
                var assigned_warehouse = 1;
                var main_warehouse = "{{ $main_warehouse->warehouse_id }}";
                main_warehouse = parseInt(main_warehouse);
                $('#productForm select[name="product_warehouse[]"]').each(function()
                {
                    if($(this).val() == ""){
                        flag = 1;
                    }
                    if($(this).val() == main_warehouse){
                        assigned_warehouse = 0;
                    }
                });

                indexx = 0;
                $('#productForm input[name="warehouse_price[]"]').each(function()
                {
                    console.log("index: "+indexx);
                    console.log("warehouse type: "+$('input[name="warehouse_type[]"]').eq(indexx).val());
                    console.log("price: "+$(this).val());
                    if($(this).val() == "" || ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() < 1)){
                        console.log('harga: '+$(this).val());
                        flag = 1;
                    }
                    if($(this).val() != "" && ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() >= 1)){
                        console.log('harga: '+$(this).val());
                        warehouse_type = 3;
                    }
                    indexx++;
                    
                });

                indexx = 0;
                $('#productForm input[name="warehouse_min_stock[]"]').each(function()
                {
                    if($(this).val() == ""  || ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() < 1)){
                        console.log('stok minimum: '+$(this).val());
                        flag = 1;
                    }
                    if($(this).val() != "" && ($('input[name="warehouse_type[]"]').eq(indexx).val() == 2 && $(this).val() >= 1)){
                        console.log('harga: '+$(this).val());
                        warehouse_type = 3;
                    }
                    indexx++;
                });

                if(flag == 1){
                    alert('Field harga/minimum stok per gudang tidak boleh kurang dari satu jika tipe gudang adalah gudang penjualan');
                }else if(warehouse_type == 2){
                    alert('Harus terdapat gudang penjualan di setiap produk/varian');
                }else if(assigned_warehouse == 1){
                    alert('Produk juga harus diassign ke gudang utama');
                }else{
                    form.submit();
                    showLoader();
                }
            }
        });


        function readURL(input,show_image) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(show_image).attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('select[name="product_warehouse[]"]').change(function(){
            var dropdownValue = $(this).val();
            var index = $('select[name="product_warehouse[]"]').index(this);
            console.log(index);
            $.ajax({
                url : "{{ route('admin/warehouse/get') }}",
                type: "POST",
                headers : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    warehouse_id: $(this).val()
                },
                success: function(resultData){
                    // console.log(resultData);
                    var data = resultData['data'];
                    console.log(data);
                    if(data != null){
                        $('input[name="warehouse_type[]"]').eq(index).remove();
                        console.log('warehouse type: '+data['warehouse_type_id']);
                        $('<input type="hidden" name="warehouse_type[]" value="'+(data['warehouse_type_id'])+'">').insertAfter('select[name="product_warehouse[]"]:eq('+index+')');
                        console.log($('input[name="product_warehouse_index[]"]').val());
                        product_warehouse_main_indexx = $('input[name="product_warehouse_main_index[]"]').length;
                        for (let m = 0; m < product_warehouse_main_indexx; m++) {
                            $('<span class="d-block margin-tb10" style="display:block">'+($('select[name="product_warehouse[]"]:eq('+index+')').find('option:selected').text())+'</span><input name="product_warehouse_price[]" onkeyup="currencyFormat(this)" type="text" class="form-control d-block margin-tb10 '+(data['warehouse_type_id'] == 1 ? 'pointer-none' : '')+'" value="'+(data['warehouse_type_id'] == 1 ? 0 : '')+'" '+(data['warehouse_type_id'] == 1 ? 'readonly' : '')+' placeholder="Harga per gudang"><input type="text" name="product_warehouse_minimum_stock[]" class="form-control d-block margin-b10 valid '+(data['warehouse_type_id'] == 1 ? 'pointer-none' : '')+'" '+(data['warehouse_type_id'] == 1 ? 'readonly' : '')+' value="'+(data['warehouse_type_id'] == 1 ? 0 : '')+'" placeholder="Stok minimum" aria-invalid="false"><input type="hidden" name="product_warehouse_index[]" class="form-control" value="'+($('table#productVariantTable tbody tr:eq('+m+') input[name="product_warehouse_main_index[]"]').val())+'"><input type="hidden" name="product_warehouse_id[]" class="form-control" value="'+dropdownValue+'">').insertAfter('table#productVariantTable tbody tr:eq('+m+') td:last-child input[name="product_warehouse_id[]"]:last-child');
                        }
                        // $('table#productVariantTable tbody tr td:last-child').html("blablabla");
                        if(data['warehouse_type_id'] == 1){
                            $('input[name="warehouse_price[]"]').eq(index).val(0);
                            $('input[name="warehouse_price[]"]').eq(index).attr('readonly',true);
                            $('input[name="warehouse_price[]"]').eq(index).addClass('pointer-none');
                            $('input[name="warehouse_min_stock[]"]').eq(index).val(0);
                            $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',true);
                            $('input[name="warehouse_min_stock[]"]').eq(index).addClass('pointer-none');
                        }else{
                            $('input[name="warehouse_price[]"]').eq(index).attr('readonly',false);
                            $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',false);
                            $('input[name="warehouse_price[]"]').eq(index).removeClass('pointer-none');
                            $('input[name="warehouse_min_stock[]"]').eq(index).removeClass('pointer-none');
                        }
                    }else{
                        $('input[name="warehouse_price[]"]').eq(index).attr('readonly',false);
                        $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',false);
                        $('input[name="warehouse_price[]"]').eq(index).removeClass('pointer-none');
                        $('input[name="warehouse_min_stock[]"]').eq(index).removeClass('pointer-none');
                    }
                }
            });
        });

        $('#product_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('form#productForm input[name="product_picture"]').click();
        });

        $('#product_variant_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('input[name="product_picture"]').click();
        });

        $('form#productForm input[name="product_picture"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this,'#showing_product_image img');
            console.log('The file "' + fileName +  '" has been selected.');
        });

        $('#productVariantModal input[name="product_picture"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#product_variant_image_name').html(fileName);
            // readURL(this,'#showing_product_image img');
            console.log('The file "' + fileName +  '" has been selected.');
        });

        // $('select[name="stockable_flag"]').change(function(){
        //     if($(this).val() == 1){
        //         $('.product_main_stock').removeClass('d-none');
        //     }else{
        //         $('.product_main_stock').addClass('d-none');
        //     }
        // })

        $('#product_uom').change(function () {
            var satuanMaster = $('#product_uom_master').children("option:selected").val();
            var valueMaster = $('.master_product_weight').val();
            var satuan = $('#product_uom').find('option:selected').val();
            var value = $('.satuan_jual').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: 'post',
                url: '{{ route("admin/product/checkUOM") }}',
                data: {
                    satuanMaster: satuanMaster,
                    valueMaster: valueMaster,
                    satuan: satuan,
                    value: value
                },
                success: function(result){
                    if (result == "1") {
                        // $("#stockable_flag option[value='1']").prop('disabled', false);
                        $("#stockable_flag").val("1");
                    } else {
                        // $("#stockable_flag option[value='1']").prop('disabled', true);
                        $("#stockable_flag").val("0");
                    }
                }
            })
        });

        clickCancelButton('{{ route("admin/product") }}')

        exitPopup('{{ route("admin/product") }}');      

        $('#addwarehouse').click(function(e){
            e.preventDefault();
            showLoader();
            var id = parseInt($(this).attr('data-id'));
            i = id + 1;
            $(this).attr('data-id', i);

            var allWarehouse = "{{ $warehouse_64 }}";
            allWarehouse = atob(allWarehouse);
            var whObj = JSON.parse(allWarehouse);
            console.log(whObj);

            html = '<tr class="prd_wh_item_'+i+'">';
            html += '<td>';
            html += '<div class="form-group" style="margin: 0 !important;">';
            html += '<div class="clearfix"></div>';
            html += '<select class="select-state single-select" name="product_warehouse[]">';
            html += '<option value="">-- Pilih Gudang --</option>';
            $.each(whObj, function (i, item) {
                html += '<option class="wh_'+item.warehouse_id+'" value="'+item.warehouse_id+'">'+item.warehouse_name+'</option>';
            });
            html += '</select>';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group" style="margin: 0 !important;">';
            html += '<input type="text" class="form-control product_price" onkeyup="currencyFormat(this)" name="warehouse_price[]" value="">';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<div class="form-group" style="margin: 0 !important;">';
            html += '<input type="text" class="form-control warehouse_min_stock" onkeypress="return numeric(event)" name="warehouse_min_stock[]" value="">';
            html += '</div>';
            html += '</td>';
            html+= '<td>';
            html+= '<div style="cursor:pointer" class="deleteInvenProduct" onclick="deleteWh('+i+')"><i class="fa fa-times-circle color-red ico-size-td"></i></div>';
            html+= '</td>';
            html += '</tr>';
            $('#prd_wh_item').append(html);
            $(".single-select").select2();
            hideLoader();

            $('select[name="product_warehouse[]"]').change(function(){
                var dropdownValue = $(this).val();
                var index = $('select[name="product_warehouse[]"]').index(this);
                console.log(index);
                $.ajax({
                    url : "{{ route('admin/warehouse/get') }}",
                    type: "POST",
                    headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data : {
                        warehouse_id: $(this).val()
                    },
                    success: function(resultData){
                        // console.log(resultData);
                        var data = resultData['data'];
                        console.log(data);
                        if(data != null){
                            $('input[name="warehouse_type[]"]').eq(index).remove();
                            console.log('warehouse type: '+data['warehouse_type_id']);
                            $('<input type="hidden" name="warehouse_type[]" value="'+(data['warehouse_type_id'])+'">').insertAfter('select[name="product_warehouse[]"]:eq('+index+')');
                            // console.log($('input[name="product_warehouse_main_index[]"]').length);
                            product_warehouse_main_indexx = $('input[name="product_warehouse_main_index[]"]').length;
                            // $('table#productVariantTable tbody tr td:last-child').html("blablabla"); 
                            for (let m = 0; m < product_warehouse_main_indexx; m++) {
                                $('<span class="d-block margin-tb10" style="display:block">'+($('select[name="product_warehouse[]"]:eq('+index+')').find('option:selected').text())+'</span><input name="product_warehouse_price[]" onkeyup="currencyFormat(this)" type="text" class="form-control d-block margin-tb10 '+(data['warehouse_type_id'] == 1 ? 'pointer-none' : '')+'" value="'+(data['warehouse_type_id'] == 1 ? 0 : '')+'" '+(data['warehouse_type_id'] == 1 ? 'readonly' : '')+' placeholder="Harga per gudang"><input type="text" name="product_warehouse_minimum_stock[]" class="form-control d-block margin-b10 valid '+(data['warehouse_type_id'] == 1 ? 'pointer-none' : '')+'" '+(data['warehouse_type_id'] == 1 ? 'readonly' : '')+' value="'+(data['warehouse_type_id'] == 1 ? 0 : '')+'" placeholder="Stok minimum" aria-invalid="false"><input type="hidden" name="product_warehouse_index[]" class="form-control" value="'+($('table#productVariantTable tbody tr:eq('+m+') input[name="product_warehouse_main_index[]"]').val())+'"><input type="hidden" name="product_warehouse_id[]" class="form-control" value="'+dropdownValue+'">').insertAfter('table#productVariantTable tbody tr:eq('+m+') td:last-child input[name="product_warehouse_id[]"]:last-child');
                            }
                            if(data['warehouse_type_id'] == 1){
                                $('input[name="warehouse_price[]"]').eq(index).val(0);
                                $('input[name="warehouse_price[]"]').eq(index).attr('readonly',true);
                                $('input[name="warehouse_price[]"]').eq(index).addClass('pointer-none');
                                $('input[name="warehouse_min_stock[]"]').eq(index).val(0);
                                $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',true);
                                $('input[name="warehouse_min_stock[]"]').eq(index).addClass('pointer-none');
                            }else{
                                $('input[name="warehouse_price[]"]').eq(index).attr('readonly',false);
                                $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',false);
                                $('input[name="warehouse_price[]"]').eq(index).removeClass('pointer-none');
                                $('input[name="warehouse_min_stock[]"]').eq(index).removeClass('pointer-none');
                            }
                        }else{
                            $('input[name="warehouse_price[]"]').eq(index).attr('readonly',false);
                            $('input[name="warehouse_min_stock[]"]').eq(index).attr('readonly',false);
                            $('input[name="warehouse_price[]"]').eq(index).removeClass('pointer-none');
                            $('input[name="warehouse_min_stock[]"]').eq(index).removeClass('pointer-none');
                        }
                    }
                });
            });
        }); 

        function deleteWh(i){
            // console.log($('table#productVariantTable tbody tr').length);
            var total_variant = $('table#productVariantTable tbody tr').length;
            var chosen_warehouse = $('select.product_warehouse_item_'+i).val();
            var warehouse_per_product = $('input[name="product_warehouse_id[]"]').length;
            // for (let k = 0; k < total_variant; k++) {
            //     // const element = array[k];
            // }
            for (let j = 0; j < warehouse_per_product; j++) {
                // const element = array[j];
                if($('input[name="product_warehouse_id[]"]').eq(j).val() == chosen_warehouse){
                    $('input[name="product_warehouse_id[]"]').eq(j).remove();
                    $('input[name="product_warehouse_price[]"]').eq(j).remove();
                    $('input[name="product_warehouse_minimum_stock[]"]').eq(j).remove();
                    $('input[name="product_warehouse_index[]"]').eq(j).remove();
                    $('span.product_warehouse_name').eq(j).remove();
                }
            }
            console.log(chosen_warehouse);
            $('.prd_wh_item_'+i).remove();
        }

        @if(Request::get('product') != null)
            $('#submitVariantModalButton').click(function(){
                $('#addProductVariantForm').submit();
                showLoader();
            })
        // $('#add-variant').click(function(){
        //     showLoader();
        //     var warehouseList = "{{ $warehouse_list_64 }}";
        //     warehouseList = atob(warehouseList);
        //     var wh_list = JSON.parse(warehouseList);
        //     console.log(wh_list);
        //     var html = '';
        //     $.each(wh_list, function (i, item) {
        //         var key = i+1;
        //         html += '<tr class="prd_wh_item_'+key+' prod_wh">';
        //         html += '<td>';
        //         html += '<div class="form-group" style="margin: 0 !important;">';
        //         html += '<input type="hidden" name="product_warehouse[]" value="'+item.warehouse_id+'">';
        //         html += '<input type="text" class="form-control product_warehouse_name" name="product_warehouse_name[]" value="'+item.warehouse_name+'" required="required">';
        //         html += '</div>';
        //         html += '</td>';
        //         html += '<td>';
        //         html += '<div class="form-group" style="margin: 0 !important;">';
        //         html += '<input type="text" class="form-control product_price" onkeyup="currencyFormat(this)" name="warehouse_price[]" value="'+item.prod_price+'" required="required">';
        //         html += '</div>';
        //         html += '</td>';
        //         html += '<td>';
        //         html += '<div class="form-group" style="margin: 0 !important;">';
        //         html += '<input type="text" class="form-control product_price" onkeyup="currencyFormat(this)" name="warehouse_min_stock[]" onkeypress="return numeric(event)" value="'+item.minimum_stock+'" required="required">';
        //         html += '</div>';
        //         html += '</td>';
        //         html += '</tr>';
        //     });
        //     console.log(html);
        //     $('#modal_prd_wh_item').append(html);
        //     $('.product_price').keyup();
        //     hideLoader();
        // });
        // console.log("jml kolom: "+$('table#productVariantTable tbody tr').length);
        $('#add-variant').click(function(){
            if(k == 0){
                var product_warehouse_dropdown = $('select[name="product_warehouse[]"]').length;
                var product_warehouse_column = "<td>";
                for (let h = 0; h < product_warehouse_dropdown; h++) {
                    var readonlyFlag = "";
                    var inputFlag = "";
                    // const element = array[h];
                    var product_warehouse_name = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').text();
                    var product_warehouse_id = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').val();
                    var product_warehouse_index = "<input type='hidden' name='product_warehouse_index[]' value='"+currentIndex+"'>";
                    var disabledPointer = "";
                    // console.log($('select[name="product_warehouse[]"]:eq('+h+') option:selected').text());
                    if($('input[name="warehouse_type[]"]').eq(h).val() == 1){
                        readonlyFlag = "readonly";
                        inputFlag = "value=0";
                        disabledPointer = "pointer-none";
                    }
                    product_warehouse_column += ("<span class='d-block margin-b10 product_warehouse_name'>"+product_warehouse_name+"</span><input name='product_warehouse_id[]' type='hidden' value='"+product_warehouse_id+"'><input name='product_warehouse_price[]' type='text' placeholder='Harga per gudang' onkeyup='currencyFormat(this)' "+readonlyFlag+' '+inputFlag+" class='form-control d-block margin-tb10 "+disabledPointer+"'><input name='product_warehouse_minimum_stock[]' placeholder='Stok Minimum' type='text' class='form-control d-block margin-b10 "+disabledPointer+"' "+readonlyFlag+' '+inputFlag+">"+product_warehouse_index);
                }
                product_warehouse_column += "</td>";
                $('table#productVariantTable tbody').html('<tr><td><div style="cursor:pointer" onclick="deleteProductVariant(this)" class="deleteProductVariant deleteProductVariantInUpdate"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td><td style="display:none"><input type="hidden" name="product_warehouse_main_index[]" value="'+(currentIndex)+'"></td><td><input name="product_variant_name[]" class="form-control" type="text"></td><td><img src="{{ asset("img/default_product.jpg") }}" class="product_variant_image_in_popup"><input type="file" class="d-none" name="product_variant_picture[]"></td><td><input name="product_variant_uom_value[]" class="form-control" type="number"></td><td><select name="product_variant_uom_name[]" class="form-control single-select">'+productUomValues+'</select></td><td><input name="bruto_value[]" class="form-control" type="number"></td><td><select name="stockable_value_flag[]"><option value="1">{{ __("notification.yes") }}</option><option value="0">{{ __("notification.no") }}</option></select></td>'+product_warehouse_column+'</tr>');
                
                $('.single-select:last-child').select2();
                k = parseInt(k) + 1;
                currentIndex = parseInt(currentIndex) + 1;
            }else{
                var product_warehouse_dropdown = $('select[name="product_warehouse[]"]').length;
                var product_warehouse_column = "<td>";
                for (let h = 0; h < product_warehouse_dropdown; h++) {
                    // const element = array[h];
                    var readonlyFlag = "";
                    var inputFlag = "";
                    var disabledPointer =  "";
                    if($('input[name="warehouse_type[]"]').eq(h).val() == 1){
                        readonlyFlag = "readonly";
                        inputFlag = "value=0";
                        disabledPointer = "pointer-none";
                    }
                    var product_warehouse_name = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').text();
                    var product_warehouse_id = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').val();
                    var product_warehouse_index = "<input type='hidden' name='product_warehouse_index[]' value='"+currentIndex+"'>";
                    
                    // console.log($('select[name="product_warehouse[]"]:eq('+h+') option:selected').text());
                    product_warehouse_column += ("<span class='d-block margin-b10 product_warehouse_name'>"+product_warehouse_name+"</span><input name='product_warehouse_id[]' type='hidden' value='"+product_warehouse_id+"'><input name='product_warehouse_price[]' type='text' onkeyup='currencyFormat(this)' placeholder='Harga per gudang' class='form-control d-block margin-tb10 "+disabledPointer+"' "+readonlyFlag+' '+inputFlag+"><input name='product_warehouse_minimum_stock[]' placeholder='Stok Minimum' type='text' class='form-control d-block margin-b10 "+disabledPointer+"' "+readonlyFlag+' '+inputFlag+">"+product_warehouse_index);
                }
                product_warehouse_column += "</td>";
                $('<tr><td><div style="cursor:pointer" class="deleteProductVariant" onclick="deleteProductVariant(this)"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td><td style="display:none"><input type="hidden" name="product_warehouse_main_index[]" value="'+(currentIndex)+'"></td><td><input name="product_variant_name[]" class="form-control" type="text"></td><td><img src="{{ asset("img/default_product.jpg") }}" class="product_variant_image_in_popup"><input type="file" class="d-none" name="product_variant_picture[]"></td><td><input name="product_variant_uom_value[]" class="form-control" type="number"></td><td><select name="product_variant_uom_name[]" class="form-control single-select">'+productUomValues+'</select></td><td><input name="bruto_value[]" class="form-control" type="number"></td><td><select name="stockable_value_flag[]"><option value="1">{{ __("notification.yes") }}</option><option value="0">{{ __("notification.no") }}</option></select></td>'+product_warehouse_column+'</tr>').insertAfter('table#productVariantTable tbody tr:last-child');
                k = parseInt(k) + 1;
                currentIndex = parseInt(currentIndex) + 1;
                $('.single-select:last-child').select2();
            }

            // $('#productVariantTable img.product_variant_image_in_popup').click(function(){
            //     var index = $('#productVariantTable img.product_variant_image_in_popup').index(this);
                
            //     $('input[name="product_variant_picture[]"]').eq(index).click();
            // });

            // $('#productVariantTable input[name="product_variant_picture[]"]').on('change', function(e){
            //     var fileName = e.target.files[0].name;
            //     console.log('The file "' + fileName +  '" has been selected.');

            //     var index = $('#productVariantTable input[name="product_variant_picture[]"]').index(this);
            //     console.log('indexxx: '+index);
            //     readURL(this,'#productVariantTable img.product_variant_image_in_popup:eq('+index+')');
            //         // $('input[name="product_variant_picture[]"]').change(function(e){
            //         // });
            // })
        });
        function deleteProductVariant(className){
            var index = $('.deleteProductVariant').index(className);
            console.log($('.deleteProductVariant').index(className));
            
            $('#productVariantList tbody tr').eq(index).remove();

            k -= 1;
            if( k < 0 ){
                k = 0;
            }
        }
        
        @else
        // for (let k = 0; k < array.length; k++) {
        //     const element = array[k];
            
        // }
        console.log(productUomValues);
        $('#add-variant').click(function(){
            var product_warehouse_dropdown = $('select[name="product_warehouse[]"]').length;
            var product_warehouse_column = "<td>";
            for (let h = 0; h < product_warehouse_dropdown; h++) {
                // const element = array[h];
                var product_warehouse_name = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').text();
                var product_warehouse_id = $('select[name="product_warehouse[]"]:eq('+h+') option:selected').val();
                var product_warehouse_index = "<input type='hidden' name='product_warehouse_index[]' value='"+currentIndex+"'>";
                var readonlyFlag = "";
                var inputFlag = "";
                var disabledPointer = "";
                // console.log($('select[name="product_warehouse[]"]:eq('+h+') option:selected').text());
                if($('input[name="warehouse_type[]"]').eq(h).val() == 1){
                    readonlyFlag = "readonly";
                    inputFlag = "value=0";
                    disabledPointer = "pointer-none";
                }
                product_warehouse_column += ("<span class='d-block margin-b10 product_warehouse_name'>"+product_warehouse_name+"</span><input name='product_warehouse_id[]' type='hidden' value='"+product_warehouse_id+"'><input name='product_warehouse_price[]' onkeyup='currencyFormat(this)' placeholder='Harga per gudang' type='text' class='form-control d-block margin-tb10 "+disabledPointer+"' "+readonlyFlag+' '+inputFlag+"><input name='product_warehouse_minimum_stock[]' placeholder='Stok Minimum' type='text' class='form-control d-block margin-b10 "+disabledPointer+"' "+readonlyFlag+' '+inputFlag+">"+product_warehouse_index);
            }
            product_warehouse_column += "</td>";
            // console.log($('select[name="product_warehouse[]"]:eq(0) option:selected').text());
            if(k == 0){
                $('table#productVariantTable tbody').html('<tr><td><div style="cursor:pointer" onclick="deleteProductVariant(this)" class="deleteProductVariant"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td><td style="display:none"><input type="hidden" name="product_warehouse_main_index[]" value="'+(currentIndex)+'"></td><td><input name="product_variant_name[]" class="form-control" type="text"></td><td><img src="{{ asset("img/default_product.jpg") }}" class="product_variant_image_in_popup"><input type="file" class="d-none" name="product_variant_picture[]"></td><td><input name="product_variant_uom_value[]" class="form-control" type="number"></td><td><select name="product_variant_uom_name[]" class="form-control single-select">'+productUomValues+'</select></td><td><input name="bruto_value[]" class="form-control" type="number"></td><td><select name="stockable_value_flag[]"><option value="1">{{ __("notification.yes") }}</option><option value="0">{{ __("notification.no") }}</option></select></td>'+product_warehouse_column+'</tr>');
                
                $('.single-select:last-child').select2();
                k = parseInt(k) + 1;
                currentIndex = parseInt(currentIndex) + 1;
            }else{
                $('<tr><td><div style="cursor:pointer" class="deleteProductVariant" onclick="deleteProductVariant(this)"><i class="fa fa-times-circle color-red ico-size-td"></i></div></td><td style="display:none"><input type="hidden" name="product_warehouse_main_index[]" value="'+(currentIndex)+'"></td><td><input name="product_variant_name[]" class="form-control" type="text"></td><td><img src="{{ asset("img/default_product.jpg") }}" class="product_variant_image_in_popup"><input type="file" name="product_variant_picture[]" class="d-none"></td><td><input name="product_variant_uom_value[]" class="form-control" type="number"></td><td><select name="product_variant_uom_name[]" class="form-control single-select">'+productUomValues+'</select></td><td><input name="bruto_value[]" class="form-control" type="number"></td><td><select name="stockable_value_flag[]"><option value="1">{{ __("notification.yes") }}</option><option value="0">{{ __("notification.no") }}</option></select></td>'+product_warehouse_column+'</tr>').insertAfter('table#productVariantTable tbody tr:last-child');
                k = parseInt(k) + 1;
                currentIndex = parseInt(currentIndex) + 1;
                $('.single-select:last-child').select2();
            }
            product_warehouse_column = "";

            $('img.product_variant_image_in_popup').click(function(){
                var index = $('img.product_variant_image_in_popup').index(this);

                $('input[name="product_variant_picture[]"]').eq(index).click();
            });

            $('input[name="product_variant_picture[]"]').on('change', function(e){
                var fileName = e.target.files[0].name;
                console.log('The file "' + fileName +  '" has been selected.');

                var index = $('input[name="product_variant_picture[]"]').index(this);
                readURL(this,'img.product_variant_image_in_popup:eq('+index+')');
                    // $('input[name="product_variant_picture[]"]').change(function(e){
                    // });
            })
        });

        function deleteProductVariant(className){
            var index = $('.deleteProductVariant').index(className);
            console.log($('.deleteProductVariant').index(className));
            
            $('table#productVariantTable tbody tr').eq(index).remove();

            k -= 1;
            if( k < 0 ){
                k = 0;
            }
        }
        @endif
    </script>
<!--    <script>
                        function load_categorydua(){
                            let product_category = $('#product_category').val();
                            if (product_category === null) {
                                id = $('#product_category').eq(0).val();
                            }
                            $.ajax({
                                url: './api/category/getSubcategoryBycategoryId/' + encodeURI(categoryId),
                                type: "GET",
                                dataType: "json",
                                success: function (data) {
                                    $("#subcategoryId").empty();
                                    $.each(data, function (key, value) {
                                        let selected = null;
                                        if ($('#subcategoryId').data('old-id') && value.id == $('#subcategoryId').data('old-id')) {
                                            selected = 'selected';
                                        }
                                        let optionTag = `<option value="` + value.id + `" ` + selected + `>` + value.nama_subkategori + `</option>`;
                                        $('#subcategoryId').append(optionTag);
                                    });
                                    $('#category_id_subcategory').val(categoryId);
                                }
                            });
                        }
    </script>-->
    @endif
@endsection
