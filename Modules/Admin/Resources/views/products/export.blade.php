<table border="2" width="100%">
    <thead>
    <tr>
        <th align="center" style="background-color:yellow">Kode Produk</th>
        <th align="center" style="background-color:yellow">Kode Produk Induk</th>
        <th align="center" style="background-color:yellow">Kode Produk Stok</th>
        <th align="center" style="background-color:yellow">Nama Produk</th>
        <th align="center" style="background-color:yellow">Deskripsi</th>
        <th align="center" style="background-color:yellow">Etalase Tokopedia</th>
        <th align="center" style="background-color:yellow">Kategori Tokopedia</th>
        <th align="center" style="background-color:yellow">Kategori</th>
        <th align="center" style="background-color:yellow">Tag</th>
        <th align="center" style="background-color:yellow">Bruto</th>
        <th align="center" style="background-color:yellow">Satuan Jual</th>
        <th align="center" style="background-color:yellow">Satuan Pengukuran</th>
        <th align="center" style="background-color:yellow">Gudang,Harga Produk,Stok Minimum</th>
        <th align="center" style="background-color:yellow">Tipe Stok</th>
    </tr>
    </thead>
    <tbody>
    @foreach($product as $data)
        <tr>
            <td>{{ $data->prod_code }}</td>
            <td>{{ $data->parent_prod_code }}</td>
            <td>{{ $data->prod_stok_code }}</td>
            <td>{{ $data->prod_name }}</td>
            <td>{{ $data->prod_desc }}</td>
            <td>{{ $data->etalase_name }}</td>
            <td>{{ $data->category_tokopedia }}</td>
            <td>{{ $data->category_name }}</td>
            <td>{{ $data->tag_name }}</td>
            <td>{{ $data->bruto }}</td>
            <td>{{ $data->uom_value }}</td>
            <td>{{ $data->uom_name }}</td>
            <td>
                <?php 
                    $arr_whName = explode(",",$data->wh_name);
                    $arr_price = explode(",",$data->price);
                    $arr_stock_min = explode(",",$data->stock_min);
                    $count_data = count($arr_whName);
                    foreach($arr_whName as $key => $val):
                        $wh_name = $val;
                        $price = isset($arr_price[$key]) ? $arr_price[$key] : 0;
                        $stock_min = isset($arr_stock_min[$key]) ? $arr_stock_min[$key] : 0;
                        if ($count_data == $key+1) {
                            echo $wh_name.','.$price.','.$stock_min;
                        } else {
                            echo $wh_name.','.$price.','.$stock_min.';';
                        }
                    endforeach;
                ?>  
            </td>
            <td>{{ $data->stockable_flag == 1 ? 'Ya' : 'Tidak' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>