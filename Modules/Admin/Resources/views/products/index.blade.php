@extends('admin::layouts.master')

@section('title',__('menubar.product'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <!-- <li><i class="fa fa-book position-left"></i>{{ __('menubar.master') }}</li> -->
                                <li class="active text-bold ">{{ __('menubar.product') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.product') }}</h4>
                                        @if($accessed_menu == 1)
                                       <!--  <div class="float-right">
                                            <a  class="btn btn-orange" href="{{ route('admin/product/create') }}">
                                                <i class="fa fa-plus"></i> {{ __('page.add_product') }}
                                            </a>
                                        </div>
                                        -->
                                        <div class="float-right">
                                             <a class="btn btn-green" id="templat" href="{{ asset('files/template_upload_produk_1.xlsx') }}">
                                                <i class="fa fa-file-text"></i> Template
                                            </a>
                                            <!-- <button class="btn btn-green" id="importproduk" data-toggle="modal" data-target="#importIncoming" onclick="import_varian();">
                                                <i class="fa fa-upload"></i> {{ __('page.import_varian_product') }}
                                            </button> -->
                                            <button class="btn btn-green" id="importproduk" data-toggle="modal" data-target="#importIncoming" onclick="import_product();">
                                                <i class="fa fa-upload"></i> {{ __('page.import_product') }}
                                            </button>
                                             <a class="btn btn-green" href="{{ route('admin/product/export') }}">
                                                <i class="fa fa-download"></i> {{ __('page.export_product') }}
                                            </a>
                                            <a  class="btn btn-orange" href="{{ route('admin/product/create') }}">
                                                <i class="fa fa-plus"></i> {{ __('page.add_product') }}
                                            </a>
                                        </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" >
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <table class="table display w-100 table-brown" id="product_table">
                                            <thead class="bg-darkgrey">
                                                <tr role="row">
                                                    <th>{{ __('field.action') }}</th>
                                                    <th>{{ __('field.product_code') }}</th>
                                                    <th>{{ __('field.position_date') }}</th>
                                                    <th>{{ __('field.product_name') }}</th>
                                                    <th>{{ __('field.category_name') }}</th>
                                                    <th>{{ __('field.product_price') }}</th>
                                                    <th>{{ __('field.product_brand') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deletionPopupModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog width-auto modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <h5 class="modal-title" id="cancelPopupModalLabel"></h5> -->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"style="text-align:center">
                    <span class="product_code_detail" style="font-weight:bold;text-align:center"></span>
                    <div class="clearfix"></div>
                    <span>{{ __('notification.deletion_confirmation') }}</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('notification.no') }}</button>
                    <button type="button" class="btn btn-primary deletion_modal_button" data-dismiss="modal">{{ __('notification.yes') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="importIncoming" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importIncomingLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Import Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <form method="POST" enctype="multipart/form-data" id="formImport">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="wh_id" id="wh_id" value="">
                        <div class="form-group">
                            <label for="product_picture" class="dp-block">File Excel  <span class="required-label">*</span></label>
                            <div class="clearfix"></div>
                            <div class="input-img-name">
                                <span id="image_name">Tidak Ada Data</span>
                                <input type="file" name="file" class="form-control" value="" style="display:none;">
                                <button type="button" class="btn btn-green-upload no-margin" id="product_picture_button">Cari</button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Footer -->
                    <div class="modal-footer border-top-grey" >
                        <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                            <div class="col-xs-6 padding-r-10 res-no-pad-sm margin-b10">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">{{ __('page.cancel') }}</button>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <button type="submit" id="submitButton" value="1" name="activity_button" class="btn btn-orange btn-block">Unggah</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importfail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="importfailLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content res-lg-modal">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="importIncomingLabel">Produk Gagal Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <table class="table display w-100 table-brown border-head datatable-basic" id="table-fail">
                        <thead class="bg-darkgrey w-100">
                        <tr>
                            <th class="text-center">Kode Produk<span class="required-label">*</span></th>
                            <th class="text-center">Nama Produk<span class="required-label">*</span></th>
                            <th class="text-center">Pesan<span class="required-label">*</span></th>
                        </tr>
                        </thead>
                        <tbody id="dataImportFail">

                        </tbody>
                    </table>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-xs-12 col-sm-6 no-padding btn-footer margin-t20 float-right">
                        <div class="col-xs-12 padding-r-10 res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('error_popup')
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var productTable = $('#product_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                lengthMenu: '{{ __("page.showing") }} <select name="product_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_products") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.additional_search = $('#product_table_filter input[type = "search"]').val();
                }
            },
            "order": [[ 1, "DESC" ]],
            columns: [
                {
                    data: 'action',
                    sortable: false
                },
                {
                    data: "prod_code",
                    name: "prod_code"
                },
                {
                    data: "position_date",
                    name: "position_date",
                    visible: false
                },
                {data: 'prod_name', name: 'prod_name'},
                {data: 'category_name', name: 'category_name'},
                {
                    data: 'prod_price',
                    name: 'prod_price',
                    className: 'dt-body-right',
                    visible: false
                },
                {data: 'brand_name', name: 'brand_name'},
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        var product_url = "";
        productTable.on('click','a.productDeletionButton',function(){
            // console.log('aaaa');
            // var product_name =
            var index = $('a.productDeletionButton').index(this);
            var data = productTable.data();
            var product_code = data[index]['prod_code'];
            product_url = $(this).data('url');
            $('span.product_code_detail').html(product_code);
            // $('<input name="product_url" type="hidden" value="'+url+'">').insertAfter('span.product_code_detail');
        });

        $('#product_table_filter input[type = "search"]').on('keyup',function(){
            productTable.draw();
        });
        // $('#product_table_filter input[type = "search"]').on('keyup',function(){
        //     console.log("search: "+$(this).val());
        //     productTable = $('#product_table').on('processing.dt', function ( e, settings, processing ) {
        //         $('.dataTables_processing').remove();
        //         if(processing){
        //             showLoader();
        //         }else{
        //             hideLoader();
        //         }
        //     }).DataTable({
        //         scrollX: true,
        //         language: {
        //             paginate: {
        //                 previous: '<i class="fa fa-chevron-left"></i>',
        //                 next: '<i class="fa fa-chevron-right"></i>'
        //             },
        //             lengthMenu: '{{ __("page.showing") }} <select name="product_list_length">'+
        //                 '<option value="5">5</option>'+
        //                 '<option value="10">10</option>'+
        //                 '<option value="20">20</option>'+
        //                 '<option value="25">25</option>'+
        //                 '<option value="50">50</option>'+
        //                 '</select> data',
        //             emptyTable: '{{ __("page.no_data") }}'
        //         },
        //         oLanguage: {
        //             sSearch: "{{ __('page.search') }}:"
        //         },
        //         lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
        //         processing: true,
        //         serverSide: true,
        //         ajax: {
        //             url:'{{ route("datatable/get_products") }}',
        //             type:'POST',
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             data: function(d){
        //                 console.log(d);
        //                 d.prod_name = $(this).val();
        //             }
        //         },
        //         "order": [[ 1, "DESC" ]],
        //         columns: [
        //             {
        //                 data: 'action',
        //                 sortable: false
        //             },
        //             {
        //                 data: "prod_code",
        //                 name: "prod_code"
        //             },
        //             {
        //                 data: "position_date",
        //                 name: "position_date",
        //                 visible: false
        //             },
        //             {data: 'prod_name', name: 'prod_name'},
        //             {data: 'category_name', name: 'category_name'},
        //             {
        //                 data: 'prod_price',
        //                 name: 'prod_price',
        //                 className: 'dt-body-right',
        //                 visible: false
        //             },
        //             {data: 'brand_name', name: 'brand_name'},
        //         ],
        //         // fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
        //         //     return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
        //         // }
        //     });
        // });

        // $('#product_table_filter input[type = "search"]').on( 'keyup', function () {
        //     productTable.search( this.value ).draw();
        // } );

        $('#deletionPopupModal').on('hidden.bs.modal',function(){
            console.log('lalala');
            product_url = "";
            // $('input[name="product_url"]').remove();
        });

        $('.deletion_modal_button').click(function(){
            if(product_url != ""){
                window.location.href = product_url;
                showLoader();
            }
        });

        $('#product_picture_button').click(function(e){
            e.preventDefault();
            $(this).siblings('form#formImport input[name="file"]').click();
        });

        $('form#formImport input[name="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            // console.log(fileName);
            $('#image_name').text(fileName);
            // readURL(this,'#showing_product_image img');
            // console.log('The file "' + fileName +  '" has been selected.');
        });

        $('#formImport').submit(function(e){
            e.preventDefault();
            var type = $('#submitButton').val();
            if (type == 'varian') {
                $('.modal-custom-text p').text('');
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/product/importProductVariant") }}',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (r) {
                        console.log(r);
                        if (r.isSuccess == true) {
                            $('#importIncoming').modal('hide');
                            $('#dataImportFail').html(" ");
                            $("input[name='file']").val('');
                            $('#image_name').text("Tidak Ada Data");

                            if (r.data_fail.length > 0) {
                                var htmlFail = '';
                                $.each(r.data_fail, function (x, fail) {
                                    htmlFail += '<tr class="item_fail">';
                                    htmlFail += '<th class="text-center">'+fail.prod_code+'</th>';
                                    htmlFail += '<th class="text-center">'+fail.prod_name+'</th>';
                                    htmlFail += '<th class="text-center">'+fail.message+'</th>';
                                    htmlFail += '</tr>';
                                });
                                $('#dataImportFail').append(htmlFail);
                                $('#importfail').modal('show');
                            } else {
                                // alert(r.tokped);
                                if (r.icon == 'error') {
                                    $('.modal-custom-text p').text(r.tokped.toString());
                                    $('#notificationPopup  .modal-header').removeClass('success');
                                    $('#notificationPopup  .modal-header').addClass('error');
                                    $('#notificationPopupLabel').text('Gagal');
                                } else {
                                    $('.modal-custom-text p').text('Import Berhasil');
                                    $('#notificationPopup .modal-title').html("{{ __('popup.success') }}");
                                    $('#notificationPopup .modal-header').removeClass('error');
                                    $('#notificationPopup .modal-header').addClass('success');    
                                }
                                $('#notificationPopup').modal('show');
                            }
                            hideLoader();
                        } else {
                            alert("error processing file..");
                            hideLoader();
                        }

                    }
                });
            } else if (type == 'produk') {
                $('.modal-custom-text p').text('');
                $.ajax({
                    type: "POST",
                    url: '{{ route("admin/product/importProduct") }}',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (r) {
                        console.log(r);
                        if (r.isSuccess == true) {
                            $('#importIncoming').modal('hide');
                            $('#dataImportFail').html(" ");
                            $("input[name='file']").val('');
                            $('#image_name').text("Tidak Ada Data");

                            if (r.data_fail.length > 0) {
                                var htmlFail = '';
                                $.each(r.data_fail, function (x, fail) {
                                    htmlFail += '<tr class="item_fail">';
                                    htmlFail += '<th class="text-center">'+fail.prod_code+'</th>';
                                    htmlFail += '<th class="text-center">'+fail.prod_name+'</th>';
                                    htmlFail += '<th class="text-center">'+fail.message+'</th>';
                                    htmlFail += '</tr>';
                                });
                                $('#dataImportFail').append(htmlFail);
                                $('#importfail').modal('show');
                            } else {
                                if (r.tokped == 500) {
                                    $('.modal-custom-text p').text('Gagal menambahkan produk ke tokopedia');
                                    $('#notificationPopup  .modal-header').removeClass('success');
                                    $('#notificationPopup  .modal-header').addClass('error');
                                    $('#notificationPopupLabel').text('Gagal');
                                } else {
                                    $('.modal-custom-text p').text('Import Berhasil');
                                    $('#notificationPopup .modal-title').html("{{ __('popup.success') }}");
                                    $('#notificationPopup .modal-header').removeClass('error');
                                    $('#notificationPopup .modal-header').addClass('success');    
                                }
                                // $('.modal-custom-text p').text(r.tokped);
                                // $('#notificationPopup .modal-title').html("{{ __('popup.success') }}");
                                // $('#notificationPopup .modal-header').removeClass('error');
                                // $('#notificationPopup .modal-header').addClass('success');
                                $('#notificationPopup').modal('show');
                            }
                            hideLoader();
                        } else {
                            alert("error processing file..");
                            hideLoader();
                        }

                    }
                });
            }

            showLoader();


        });

        function import_varian(){
            $('#submitButton').val('varian');
        }

        function import_product(){
            $('#submitButton').val('produk');
        }
    </script>
<!--
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>
-->
    @endif
@endsection
