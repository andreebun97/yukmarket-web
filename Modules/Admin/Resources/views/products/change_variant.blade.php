<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Product Variant || YukMarket</title>
</head>
<body>
    <form method="POST" action="{{ route('admin/product/changeVariantForm') }}" enctype="multipart/form-data">
        @csrf
        <input type="file" name="import_new_variant">
        <input type="submit" value="Submit">
    </form>
</body>
</html>