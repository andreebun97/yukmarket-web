@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_voucher') : __('page.edit_voucher')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.voucher') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_voucher') : __('page.edit_voucher') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_voucher') : __('page.edit_voucher') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            @php $route = route('admin/voucher/form'); @endphp
                                            @isset($product_flag)
                                                @php $route = route('admin/product/promo/form'); @endphp
                                            @endisset
                                            <form method="POST" id="voucherForm" action="{{ $route }}" enctype="multipart/form-data" class="form-style">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('id') != null)
                                                            <input type="hidden" name="voucher_id" value="{{ $voucher_by_id['voucher_id'] }}">
                                                        @endif
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                            @if(!isset($product_flag))
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="voucher_name">{{ __('field.voucher_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" class="form-control @error('voucher_name') is-invalid @enderror" name="voucher_name" value="{{ Request::get('id') == null ? old('voucher_name') : $voucher_by_id['voucher_name'] }}">
                                                                    @error('voucher_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group">
                                                                    <label for="voucher_code">{{ __('field.voucher_code') }} <span class="required-label">*</span></label>
                                                                    <input type="text" name="voucher_code" class="form-control @error('voucher_code') is-invalid @enderror" value="{{ Request::get('id') == null ? old('voucher_code') : $voucher_by_id['voucher_code'] }}" autocomplete="off" {{ Request::get('id') == null ? "" : "readonly" }}>
                                                                    @error('voucher_code')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_code') }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            @endif
                            
                                                            <div class="clearfix"></div>
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="voucher_start_date">{{ __('field.voucher_start_date') }} <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <span class="input-group-addon-custom"><i class="icon-calendar22"></i></span>
                                                                        <input type="text" class="form-control daterange-single @error('voucher_start_date') is-invalid @enderror" name="voucher_start_date"  id="voucher_start_date" value="{{ Request::get('id') == null ? old('voucher_start_date') : date('Y-m-d',strtotime($voucher_by_id['voucher_start_date'])) }}">
                                                                        @error('voucher_start_date')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_start_date') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group" style="padding-top:27px">
                                                                    <div class="input-group padding-left-20">
                                                                        <span class="input-group-addon-custom"><i class="icon-watch2"></i></span>
                                                                        <input type="text" name="voucher_start_time" class="form-control  @error('voucher_start_time') is-invalid @enderror" id="anytime-start" value="{{ Request::get('id') == null ? old('voucher_start_time') : $voucher_by_id['voucher_start_time'] }}" >
                                                                        @error('voucher_start_time')
                                                                            <span class="invalid-feedback">{{ $errors->first('voucher_start_time') }}</span>
                                                                        @enderror
                                                                    </div>
                                                        
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group padding-r-10">
                                                                    <label for="voucher_end_date">{{ __('field.voucher_end_date') }} <span class="required-label">*</span></label>
                                                                    <div class="input-group padding-left-20">
                                                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                                                        <div class="clearfix"></div>
                                                                        <input type="text" class="form-control daterange-single @error('voucher_end_date') is-invalid @enderror" name="voucher_end_date" id="voucher_end_date" value="{{ Request::get('id') == null ? old('voucher_end_date') : date('Y-m-d',strtotime($voucher_by_id['voucher_end_date'])) }}">
                                                                        @error('voucher_end_date')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_end_date') }}</span>
                                                                    @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 no-padding">
                                                                <div class="form-group" style="padding-top:27px">
                                                                    <div class="input-group padding-left-20">
                                                                        <span class="input-group-addon-custom"><i class="icon-watch2"></i></span>
                                                                        <input type="text" name="voucher_end_time"  class="form-control  @error('voucher_end_time') is-invalid @enderror" id="anytime-end" value="{{ Request::get('id') == null ? old('voucher_end_time') : $voucher_by_id['voucher_end_time'] }}" >
                                                                        @error('voucher_end_time')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_end_time') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 no-padding">
                                                                    <div class="form-group no-margin">
                                                                        <label for="product_weight">{{ __('field.total_provided_voucher') }} <span class="required-label">*</span></label>
                                                                        <input type="number" name="total_provided_voucher" class="form-control @error('total_provided_voucher') is-invalid @enderror" value="{{ Request::get('id') == null ? old('total_provided_voucher') : $voucher_by_id['total_provided_voucher'] }}">
                                                                        @error('total_provided_voucher')
                                                                            <span class="invalid-feedback">{{ $errors->first('total_provided_voucher') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                
                                                                <!-- <div class="clearfix"></div> -->
                                                            </div>
                                                            @isset($product_flag)
                                                                <div class="col-md-12 no-padding ">
                                                                    <div class="form-group no-margin"  style="padding-top:19px">
                                                                        <label for="applied_product">{{ __('field.applied_product') }} <span class="required-label">*</span></label>
                                                                        <select name="applied_product[]" id="applied_product" class="select-state multi-select select2-hidden-accessible" multiple>
                                                                            <option value="">{{ __('page.choose_product') }}</option>
                                                                            @for($b = 0; $b < count($products); $b++)
                                                                                <option value="{{ $products[$b]['prod_id'] }}" {{ isset($product_promo) && array_search($products[$b]['prod_id'],array_column($product_promo,'prod_id')) !== false ? (isset($edit) && array_search($products[$b]['prod_id'],array_column($product_promo_by_id,'prod_id')) !== false ? 'selected' : 'disabled') : '' }}>{{ $products[$b]['prod_name'] . ($products[$b]['product_sku_id'] == null ? '' : (' ' . ($products[$b]['uom_value']+0) . ' ' . $products[$b]['uom_name'])) }}</option>
                                                                            @endfor
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                @endisset
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad res-mg-b20">
                                                            <div class="col-md-12 no-padding">
                                                                <div class="form-group">
                                                                    <label for="voucher_type">{{ __('field.voucher_type') }} <span class="required-label">*</span></label>
                                                                    <select name="voucher_type" class="form-control single-select">
                                                                        <option value=""></option>
                                                                        @for($b = 0; $b < count($voucher_type); $b++)
                                                                            <option value="{{ $voucher_type[$b]['voucher_type_id'] }}" {{ Request::get('id') == null ? ($voucher_type[$b]['voucher_type_id'] == old('voucher_type') ? 'selected' : '') : ($voucher_type[$b]['voucher_type_id'] == $voucher_by_id['voucher_type_id'] ? 'selected' : '') }}>{{ $voucher_type[$b]['voucher_type_name'] }}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="voucher_value_type">{{ __('field.voucher_value_type') }} <span class="required-label">*</span></label><br>
                                                                    <div class="margin-tb-8">
                                                                        <input type="radio" name="voucher_value_type" value="N" {{ Request::get('id') == null ? (old('voucher_value_type') == 'N' ? 'checked' : '') : ($voucher_by_id['voucher_value_type'] == 'N' ? 'checked' : '') }}>{{ __('field.percentage') }}
                                                                        <input type="radio" name="voucher_value_type" value="Y" {{ Request::get('id') == null ? (old('voucher_value_type') == 'Y' ? 'checked' : '') : ($voucher_by_id['voucher_value_type'] == 'Y' ? 'checked' : '') }}>{{ __('field.absolute') }}
                                                                    </div>
                                                                    <!-- <input type="text" name="is_fixed" onkeyup="currencyFormat(this)" class="form-control"> -->
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group ">
                                                                <label for="voucher_amount">{{ __('field.voucher_amount') }} <span class="required-label">*</span></label>
                                                                <input type="text" name="voucher_amount" class="form-control @error('voucher_amount') is-invalid @enderror" onkeyup="currencyFormat(this)"  value="{{ Request::get('id') == null ? old('voucher_amount') : $voucher_by_id['voucher_amount'] }}">
                                                                @error('voucher_amount')
                                                                    <span class="invalid-feedback">{{ $errors->first('voucher_amount') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="col-xs-6 no-padding">
                                                                    <div class="form-group padding-r-10 no-margin">
                                                                        <label for="min_price_requirement">{{ __('field.min_price_requirement') }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="min_price_requirement" onkeyup="currencyFormat(this)" class="form-control @error('min_price_requirement') is-invalid @enderror" value="{{ Request::get('id') == null ? old('min_price_requirement') : $voucher_by_id['min_price_requirement'] }}">
                                                                        @error('min_price_requirement')
                                                                            <span class="invalid-feedback">{{ $errors->first('min_price_requirement') }}</span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 no-padding">
                                                                    <div class="form-group no-margin">
                                                                        <label for="max_value_price">{{ __('field.max_value_price') }} <span class="required-label">*</span></label>
                                                                        <input type="text" name="max_value_price" onkeyup="currencyFormat(this)" class="form-control" value={{ Request::get('id') == null ? old('max_value_price') : $voucher_by_id['max_value_price'] }}>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad"> 
                                                            <div class="col-md-12 no-padding">
                                                                <div class="form-group">
                                                                    <label for="voucher_description">{{ __('field.voucher_description') }} <span class="required-label">*</span></label>
                                                                    <textarea name="voucher_description" class="form-control @error('voucher_description') is-invalid @enderror" style="resize:none;height:285px">{{ Request::get('id') == null ? old('voucher_description') : $voucher_by_id['voucher_description'] }}</textarea>
                                                                    @error('voucher_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('voucher_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                  
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                    </div>  
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var dtToday = new Date();
        
        var currentMonth = dtToday.getMonth() + 1;
        if(currentMonth < 10){
            currentMonth = "0"+currentMonth;
        }
        var currentDay = dtToday.getDate();
        if(currentDay < 10){
            currentDay = "0"+currentDay;
        }
        var currentYear = dtToday.getFullYear();
        var minDate = currentYear + '-' + currentMonth + '-' + currentDay;

        $('input[name="voucher_start_date"]').attr('min',minDate);
        $('input[name="voucher_end_date"]').attr('min',minDate);

        @if(Request::get('id') != null)
            var min_price_requirement = currencyFormat(document.getElementsByName('min_price_requirement')[0]);
            var amount = currencyFormat(document.getElementsByName('voucher_amount')[0]);
            var max_value_price = currencyFormat(document.getElementsByName('max_value_price')[0]);
            $('input[name="min_price_requirement"]').val(min_price_requirement);
            $('input[name="amount"]').val(amount);
            $('input[name="max_value_price"]').val(max_value_price);
        @endif

        jQuery.validator.addMethod("greaterThan", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }

            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'Must be greater than {0}.');

        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="voucher_name"], input[name="voucher_code"], textarea[name="voucher_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        @isset($product_flag)
        var validationRules = {
            voucher_start_date: {
                required: true
            },
            voucher_start_time: {
                required: true
            },
            voucher_end_date: {
                required: true,
                greaterThan: 'input[name="voucher_start_date"]'
            },
            voucher_end_time: {
                required: true
            },
            total_provided_voucher: {
                required: true
            },
            voucher_type: {
                required: true
            },
            voucher_description: {
                required: true
            },
            voucher_amount: {
                required: true
            },
            min_price_requirement: {
                required: true
            },
            max_value_price: {
                required: true
            },
            voucher_value_type: {
                required: true
            },
            'applied_product[]': {
                required: true
            }
        };
        var validationMessages = {
            voucher_start_date: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_start_date')]) }}"
            },
            voucher_start_time: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_start_time')]) }}"
            },
            voucher_end_date: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_end_date')]) }}",
                greaterThan: "{{ __('validation.after_or_equal',['attribute' => __('validation.attributes.voucher_end_date'), 'date' => __('validation.attributes.voucher_start_date')]) }}"
            },
            voucher_end_time: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_end_time')]) }}"
            },
            total_provided_voucher: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.total_provided_voucher')]) }}"
            },
            voucher_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_type')]) }}"
            },
            voucher_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_description')]) }}"
            },
            voucher_amount: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_amount')]) }}"
            },
            min_price_requirement: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.min_place_requirement')]) }}"
            },
            max_value_price: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.max_value_price')]) }}"
            },
            voucher_value_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_value_type')]) }}"
            },
            'applied_product[]': {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.applied_product')]) }}"
            }
        }
        @else
        var validationRules = {
            voucher_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            voucher_code: {
                required: true
                // minlength: 5
            },
            voucher_start_date: {
                required: true
            },
            voucher_start_time: {
                required: true
            },
            voucher_end_date: {
                required: true,
                greaterThan: 'input[name="voucher_start_date"]'
            },
            voucher_end_time: {
                required: true
            },
            total_provided_voucher: {
                required: true
            },
            voucher_type: {
                required: true
            },
            voucher_description: {
                required: true
            },
            voucher_amount: {
                required: true
            },
            min_price_requirement: {
                required: true
            },
            max_value_price: {
                required: true
            },
            voucher_value_type: {
                required: true
            }
        };
        var validationMessages = {
            voucher_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.voucher_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.voucher_name')]) }}"
            },
            voucher_code: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_code')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.voucher_code'), 'min' => 5]) }}"
            },
            voucher_start_date: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_start_date')]) }}"
            },
            voucher_start_time: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_start_time')]) }}"
            },
            voucher_end_date: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_end_date')]) }}",
                greaterThan: "{{ __('validation.after_or_equal',['attribute' => __('validation.attributes.voucher_end_date'), 'date' => __('validation.attributes.voucher_start_date')]) }}"
            },
            voucher_end_time: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_end_time')]) }}"
            },
            total_provided_voucher: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.total_provided_voucher')]) }}"
            },
            voucher_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_type')]) }}"
            },
            voucher_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_description')]) }}"
            },
            voucher_amount: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_amount')]) }}"
            },
            min_price_requirement: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.min_place_requirement')]) }}"
            },
            max_value_price: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.max_value_price')]) }}"
            },
            voucher_value_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.voucher_value_type')]) }}"
            }
        }
        @endisset
        $('form#voucherForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/voucher") }}')

        exitPopup('{{ route("admin/voucher") }}');
    </script>
    @endif
@endsection
