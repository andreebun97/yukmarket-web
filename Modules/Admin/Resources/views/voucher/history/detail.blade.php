
@extends('admin::layouts.master')

@section('title',__('page.voucher_usage_history_detail'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.voucher') }}</li>
                                <li>{{ __('page.voucher_usage_history') }}</li>
                                <li class="active text-bold ">{{ __('page.voucher_usage_history_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-8">
                                                <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.voucher_usage_history_detail') }}</h4>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-6 col-xs-4">
                                                <a href="{{url('/admin/voucher/history')}}" class="btn btn-gray discard_changes_button float-right" >Kembali</a>
                                            </div>
                                        </div>
                                    </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                        <div class="row no-padding">
                                            <div class="col-md-12 trs-detail">
                                                <div class="col-md-12">
                                                    <div id="user_profile " class="info-detail">
                                                        <h2>{{ __('page.voucher_information') }}</h2>
                                                        <div class="table-label">
                                                            <table>
                                                                <tr>
                                                                    <td class="width-label"><label>{{ __('field.voucher_name') }}</label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <input type="hidden" id="voucherId" value="{{ $voucher_id }}" />
                                                                        <span class="user_data_information">&nbsp;{{ $voucher_name }}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><label>{{ __('field.voucher_code') }}</label></td>
                                                                    <td>:</td>
                                                                    <td><span class="user_data_information"></span>&nbsp;{{ $voucher_code }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><label>{{ __('field.voucher_description') }}</label></td>
                                                                    <td>:</td>
                                                                    <td><span class=" dd user_data_information">&nbsp;{{ $voucher_desc }}</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div id="voucher_history" class="info-detail">
                                                    <h2>{{ __('page.transactions') }}</h2>
                                                    <table id="voucher_usage_detail" class="table display w-100 table-brown">
                                                        <thead class="bg-darkgrey">
                                                            <tr>
                                                                <th>No.</th>
                                                                <th>{{ __('field.customer_name') }}</th>
                                                                <th>{{ __('field.user_email') }}</th>
                                                                <th>{{ __('field.user_phone_number') }}</th>
                                                                <th>{{ __('field.voucher_amount') }}</th>
                                                                <th>{{ __('field.purchased_date') }}</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        $('#voucher_usage_detail').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            searching: false,
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/details") }}',
                type:'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    voucherId: $('#voucherId').val()
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'cust_name', name: 'cust_name'},
                {data: 'cust_email', name: 'cust_email'},
                {data: 'cust_phone', name: 'cust_phone'},
                {
                    data: 'v_amount', 
                    name: 'v_amount',
                    // render: function(data, type, row){
                    //     console.log(row['v_value_type']);
                    //     return row['v_value_type'] == 1 ? convertToCurrency(data) : (data+'%');
                    // },
                    render: $.fn.dataTable.render.number( ',', '.', 2 )
                },
                {data: 'paid_date', name: 'paid_date'}
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                var api = this.api(), oSettings;
                    
                    var intVal = function ( i ) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };

                    total = api.column(4).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    pageTotal = api.column(4, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
        
                    $( api.column(4).footer() ).html(
                        '( ' + $.fn.dataTable.render.number(',','','0','Rp. ').display(total) + ' )'
                    );
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
    </script>
@endsection
