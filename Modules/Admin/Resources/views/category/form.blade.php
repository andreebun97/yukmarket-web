@extends('admin::layouts.master')

@section('title',(Request::get('category') == null ? __('page.add_category') : __('page.edit_category')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.category') }}</li>
                                <li class="active text-bold ">{{ Request::get('category') == null ? __('page.add_category') : __('page.edit_category') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                            <div class="panel panel-flat">
                                <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('category') == null ? __('page.add_category') : __('page.edit_category') }}</h4>
                                            
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page') 
                                        @else
                                            <form method="POST" action="{{ route('admin/category/form') }}" id="categoryForm" enctype="multipart/form-data">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        {{-- <div class="col-md-4 small-img padding-r-10">
                                                            <div id="showing_product_image">
                                                                <img src="{{ Request::get('category') == null ? asset('img/default_product.jpg') : ('/'.$category['category_image']) }}" alt="" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                            </div>
                                                        </div> --}}
                                                        @csrf
                                                        @if(Request::get('category') !== null)
                                                            <input type="hidden" name="category_id" value="{{ Request::get('category') }}">
                                                        @endif     
                                                        <div class="col-md-4 res-left-form res-no-pad-left">                 
                                                            <div class="info">
                                                                <i class="fa fa-info-circle"></i>
                                                                {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="no-padding" for="category_name">{{ __('field.category_name') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control @error('category_name') is-invalid @enderror" name="category_name" autocomplete="off" @if(Request::get('category') == null) value="{{ old('category_name') }}" @else value="{{ $category['category_name'] }}"  @endif>
                                                                @error('category_name')
                                                                    <span class="invalid-feedback">{{ $errors->first('category_name') }}</span>
                                                                @enderror
                                                            </div>
                                                            {{-- <div class="form-group">
                                                                <label style="display: block;" for="category_description">{{ __('field.category_image') }} <span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <div class="input-img-name">
                                                                    <span id="image_name">{{ __('page.no_data') }}</span>
                                                                    <button type="button" class="btn btn-green-upload no-margin" id="category_image_button">{{ __('page.upload') }}</button> 
                                                                </div>
                                                                <input type="file" name="category_image" class="form-control" style="display: none;">
                                                                @error('category_image')
                                                                    <span class="invalid-feedback">{{ $errors->first('category_image') }}</span>
                                                                @enderror
                                                            </div> --}}
                                                            <div class="form-group">
                                                                <label for="category_level">{{ __('field.category_level') }} <span class="required-label">*</span></label>
                                                                <input type="text" readonly name="category_level" class="form-control" @if(Request::get('category') == null) value="1" @else value="{{ $category['category_level'] }}" @endif>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="parent_category_id">{{ __('field.parent_category_id') }} </label>
                                                                <select name="parent_category_id" class="form-control single-select"></select>
                                                            </div>
                                                        </div>
                                                        {{-- <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="category_description">{{ __('field.category_description') }} <span class="required-label">*</span></label>
                                                                <textarea class="form-control @error('category_description') is-invalid @enderror" name="category_description" style="resize: none; height: 202px;">@if(Request::get('category') == null){{ old('category_description') }}@else{{ $category['category_desc'] }}@endif</textarea>
                                                                @error('category_description')
                                                                    <span class="invalid-feedback">{{ $errors->first('category_description') }}</span>
                                                                @enderror
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12 no-padding">
                                                    <div class="card-footer bg-transparent ">
                                                        <div class="col-md-4 no-padding float-right">
                                                            <div class="col-xs-6 padding-r-10">
                                                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                            </div>
                                                            <div class="col-xs-6 padding-l-10">
                                                            <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var parent_category_id = "";
        var category_id = null;
        @if(Request::get('category') != null)
            parent_category_id = "{{ $category['parent_category_id'] }}";
            category_id = "{{ Request::get('category') }}";
        @endif
        console.log(parent_category_id);

        getCategory(null, category_id);
        
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="category_name"], textarea[name="category_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var categoryId = $('input[name=category_id]').val();
        if (typeof categoryId === "undefined") {
            var validationRules = {
                category_name: {
                    required: true,
                    // minlength: 5,
                    alpha: true
                },
                category_description: {
                    required: true,
                    minlength: 10
                },
                category_image: {
                    required: true,
                    extension: "png|pneg|svg|jpg|jpeg",
                    maxsize: 2000000
                }
            };
            var validationMessages = {
                category_name: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_name')]) }}",
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.category_name'), 'min' => 5]) }}",
                    alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.category_name')]) }}"
                },
                category_description: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_description')]) }}",
                    minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.category_description'), 'min' => 10]) }}"
                },
                category_image: {
                    required: "{{ __('validation.custom.category_image.required',['attribute' => __('validation.attributes.category_image')]) }}",
                    extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.category_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                    maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
                }
            };
        } else {
            var validationRules = {
                category_name: {
                    required: true,
                    // minlength: 5,
                    alpha: true
                },
                category_description: {
                    required: true,
                    minlength: 10
                }
            };
            var validationMessages = {
                category_name: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_name')]) }}",
                    // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.category_name'), 'min' => 5]) }}",
                    alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.category_name')]) }}"
                },
                category_description: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_description')]) }}",
                    minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.category_description'), 'min' => 10]) }}"
                }
            };
        }

        $('form#categoryForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#category_image_button').click(function(e){
            e.preventDefault();
            $('input[name="category_image"]').click();
        });

        $('input[name="category_image"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });

        function getCategory(id = null, parent = null){
            var url = "{{ route('admin/category/get') }}"+(id == null ? '' : '?id='+id)+(parent != null ? ((id == null ? '?' : '&')+'parent='+parent) : '');
            console.log(url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(resultData);
                    var data = resultData['data'];
                    // console.log(data);
                    var category_array = ["<option value=''>Pilih kategori di atasnya...</option>"];
                    for (let k = 0; k < data.length; k++) {
                        var obj = "<option value='"+data[k]['category_id']+"'>"+data[k]['category_name']+"</option>";
                        category_array.push(obj);
                    }
                    console.log(category_array);
                    if(id == null){
                        $('select[name="parent_category_id"]').html(category_array);
                        @if(Request::get('category') != null)
                            $('select[name="parent_category_id"]').val(parent_category_id).trigger("change");
                        @endif
                    }else{
                        $('input[name="category_level"]').val(parseInt(data[0]['category_level']) + 1);
                    }
                }
            });
        }

        $('select[name="parent_category_id"]').change(function(){
            var id = $(this).val();
            if($(this).val() == ""){
                $('input[name="category_level"]').val("1");
            }else{
                getCategory(id);
            }
        });

        clickCancelButton('{{ route("admin/category") }}')

        exitPopup('{{ route("admin/category") }}');

    </script>
@endsection
