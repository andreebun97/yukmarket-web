@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_warehouse') : __('page.edit_warehouse')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_warehouse') : __('page.edit_warehouse') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat ">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-bold title-color-green">{{ Request::get('id') == null ? __('page.add_warehouse') : __('page.edit_warehouse') }}</h4>
                                        </div>
                                        <!-- <div class="row no-padding">
                                            <div class="col-md-6 col-md-offset-3 separator-margin-10 form-padding">
                                                <div class="separator"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div> -->
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form action="{{ route('admin/warehouse/form') }}" id="warehouseForm" method="POST">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('id') != null)
                                                            <input type="hidden" name="address_id" value="{{ Request::get('id') }}">
                                                            <input type="hidden" name="warehouse_id" value="{{ $warehouse_by_id['warehouse_id'] }}">
                                                        @endif
                                                        <div class="col-md-3 no-padding-left res-no-pad">                                                            
                                                            <div class="form-group">
                                                                <label for="warehouse_label">{{ __('field.warehouse_label') }} <span class="required-label">*</span></label>
                                                                <input type="text" name="warehouse_label" autocomplete="off" class="form-control @error('warehouse_label') is-invalid @enderror" value="{{ Request::get('id') == null ? old('warehouse_label') : $warehouse_by_id['warehouse_label'] }}">
                                                                @error('warehouse_label')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_label') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="phone_number">{{ __('field.phone_number') }} <span class="required-label">*</span></label>
                                                                <input type="text" name="phone_number" autocomplete="off" class="form-control @error('phone_number') is-invalid @enderror" value="{{ Request::get('id') == null ? old('phone_number') : $warehouse_by_id['phone_number'] }}">
                                                                @error('phone_number')
                                                                    <span class="invalid-feedback">{{ $errors->first('phone_number') }}</span>
                                                                @enderror
                                                            </div>
                                                         
                                                            <div class="form-group">
                                                                <label for="address_detail">{{ __('field.address_detail') }} <span class="required-label">*</span></label>
                                                                <textarea name="address_detail" class="form-control @error('address_detail') is-invalid @enderror" style="resize:none;height:200px">{{ Request::get('id') == null ? old('address_detail') : $warehouse_by_id['address_detail'] }}</textarea>
                                                                @error('address_detail')
                                                                    <span class="invalid-feedback">{{ $errors->first('address_detail') }}</span>
                                                                @enderror
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="warehouse_type">{{ __('field.warehouse_type') }} <span class="required-label">*</span></label>
                                                                <select name="warehouse_type" class="form-control single-select">
                                                                    <option value="">{{ __('page.choose_warehouse_type') }}</option>
                                                                    @for($b = 0; $b < count($warehouse_type); $b++)
                                                                        <option value="{{ $warehouse_type[$b]['warehouse_type_id'] }}" @if(Request::get('id') != null && $warehouse_by_id['warehouse_type'] == $warehouse_type[$b]['warehouse_type_id']) selected @endif>{{ $warehouse_type[$b]['warehouse_type_name'] }}</option>
                                                                    @endfor
                                                                </select>
                                                                @error('warehouse_type')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_type') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group {{ Request::get('id') == null ? 'd-none' : '' }} chiller_or_sales_div">
                                                                <label for="chiller_or_sales" id="chiller_or_sales_label">Gudang Penjualan</label>
                                                                <select name="children_warehouse" class="form-control {{ Request::get('id') == null ? 'single-select' : ($warehouse_by_id['warehouse_type'] == 2 ? 'multi-select' : 'single-select') }} children_warehouse_dropdown" {{ Request::get('id') == null ? '' : ($warehouse_by_id['warehouse_type'] == 2 ? 'multiple' : '') }}>
                                                                    @php
                                                                        $children_or_parent = []; 
                                                                        if(Request::get('id') != null)
                                                                        {
                                                                            if($warehouse_by_id['warehouse_type'] == 1){
                                                                                $children_or_parent = $parent_warehouse;
                                                                            }else{
                                                                                $children_or_parent = $children_warehouse;
                                                                            }
                                                                        }
                                                                    @endphp
                                                                    <option value="">Pilih gudang penyimpanan/penjualan</option>
                                                                    @for($b = 0; $b < count($children_or_parent); $b++)
                                                                        <option value="{{ $children_or_parent[$b]['warehouse_id'] }}" {{ $warehouse_by_id['warehouse_type'] == 2 ? (array_search($children_or_parent[$b]['warehouse_id'], array_column($children_or_parent_warehouse_by_id,'warehouse_id')) !== false ? 'selected' : '') : ($warehouse_by_id['parent_warehouse_id'] == $children_or_parent[$b]['warehouse_id'] ? 'selected' : '') }}>{{ $children_or_parent[$b]['warehouse_name'] }}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 res-no-pad">   
                                                            <div class="form-group">
                                                                <label for="contact_person">{{ __('field.contact_person') }} <span class="required-label">*</span></label>
                                                                <input type="text" name="contact_person" autocomplete="off" class="form-control @error('contact_person') is-invalid @enderror" value="{{ Request::get('id') == null ? old('contact_person') : $warehouse_by_id['contact_person'] }}">
                                                                @error('contact_person')
                                                                    <span class="invalid-feedback">{{ $errors->first('contact_person') }}</span>
                                                                @enderror
                                                            </div>                                                         
                                                            <div class="form-group">
                                                                <label for="warehouse_province">{{ __('field.warehouse_province') }} <span class="required-label">*</span></label>
                                                                <select name="warehouse_province" class="form-control single-select">
                                                                </select>
                                                                @error('warehouse_province')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_province') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="warehouse_regency">{{ __('field.warehouse_regency') }} <span class="required-label">*</span></label>
                                                                <select name="warehouse_regency" class="form-control single-select"></select>
                                                                @error('warehouse_regency')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_regency') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="warehouse_district">{{ __('field.warehouse_district') }} <span class="required-label">*</span></label>
                                                                <select name="warehouse_district" class="form-control single-select"></select>
                                                                @error('warehouse_district')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_district') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="warehouse_village">{{ __('field.warehouse_village') }} <span class="required-label">*</span></label>
                                                                <select name="warehouse_village" class="form-control single-select"></select>
                                                                @error('warehouse_village')
                                                                    <span class="invalid-feedback">{{ $errors->first('warehouse_village') }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 no-padding-right res-no-pad">
                                                            <div class="form-group">
                                                                <input id="pac-input" class="form-control form-control-map" type="text" placeholder="Search Warehouse"/>
                                                                <input type="hidden" name="latitude" id="latitude" class="form-control" required value="{{ Request::get('id') == null ? old('latitude') : $warehouse_by_id['latitude'] }}">
                                                                <input type="hidden" name="longitude" id="longitude" class="form-control" required value="{{ Request::get('id') == null ? old('longitude') : $warehouse_by_id['longitude'] }}">
                                                                <div id="googleMap" style="width:100%;height:395px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 float-right no-padding">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                        <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                
                                                
                                            </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="warehouse_label"], textarea[name="address_detail"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            warehouse_label: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            warehouse_province: {
                required: true
            },
            warehouse_regency: {
                required: true
            },
            warehouse_district: {
                required: true
            },
            warehouse_village: {
                required: true,
            },
            address_detail: {
                required: true
            },
            contact_person: {
                required: true,
            },
            warehouse_type: {
                required: true,
            },
            phone_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            },
            // main_flag: {
            //     required: true
            // }
        };
        var validationMessages = {
            warehouse_label: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_label')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.warehouse_label'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.warehouse_label')]) }}"
            },
            warehouse_province: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_province')]) }}"
            },
            warehouse_regency: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_regency')]) }}"
            },
            warehouse_district: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_district')]) }}"
            },
            warehouse_village: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_village')]) }}",
            },
            address_detail: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.address_detail')]) }}"
            },
            contact_person: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.contact_person')]) }}",
            },
            warehouse_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.warehouse_type')]) }}",
            },
            phone_number: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.phone_number')]) }}",
                digits: "{{ __('validation.numeric',['attribute' => __('validation.attributes.phone_number')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.phone_number'), 'min' => 10]) }}",
                maxlength: "{{ __('validation.max.string',['attribute' => __('validation.attributes.phone_number'), 'max' => 15]) }}"
            },
            // main_flag: {
            //     required: "{{ __('validation.required',['attribute' => __('validation.attributes.main_flag')]) }}"
            // }
        };

        $('form#warehouseForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        $('select[name="warehouse_type"]').change(function(){
            if($(this).val() != ""){
                $('.chiller_or_sales_div').removeClass('d-none');
                if($(this).val() == 1){
                    $('#chiller_or_sales_label').html('Gudang Penjualan');
                    $('select[name="children_warehouse"]').prop('multiple',false).select2();
                    $('select[name="children_warehouse[]"]').prop('multiple',false).attr('name','children_warehouse').attr('class','multi-select children_warehouse_dropdown').select2();
                    // $('select[name="children_warehouse"]').attr('multiple',false);
                }else{
                    $('#chiller_or_sales_label').html('Gudang Penyimpanan');
                    $('select[name="children_warehouse"]').prop('multiple',true).attr('name','children_warehouse[]').attr('class','single-select children_warehouse_dropdown').select2();
                }
                console.log("value: "+$(this).val());
                getChildrenOrParentWarehouse($(this).val());
            }else{
                $('.chiller_or_sales_div').addClass('d-none');
            }
        });

        function getChildrenOrParentWarehouse(parent_warehouse_type){
            $.ajax({
                url: '{{ route("admin/inventory/getChildrenWarehouse") }}',
                type : "POST",
                data : {
                    'parent_warehouse_type' : parent_warehouse_type
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: false,
                success: function(resultData){
                    console.log(resultData);
                    var data = resultData['data'];
                    var warehouseData = [];
                    $('select.children_warehouse_dropdown').html(warehouseData);
                    // $('select.children_warehouse_dropdown').val("")
                    var warehouseIdList = [];
                    if(data.length > 0){
                        for (let b = 0; b < data.length; b++) {
                            // const element = array[b];
                            var obj = "<option value='"+data[b]['warehouse_id']+"'>"+data[b]['warehouse_name']+"</option>";
                            warehouseIdList.push(data[b]['warehouse_id']);
                            warehouseData.push(obj);
                        }
                        $('select.children_warehouse_dropdown').html(warehouseData);
                        if(parent_warehouse_type == 2){
                            $('select.children_warehouse_dropdown').val(warehouseIdList);
                        }
                    }
                }
            });
        }

        var province_array = [];
        var regency_array = [];
        var district_array = [];
        var village_array = [];
        getProvince();
        $('select[name="warehouse_province"]').html(province_array);
        @if(Request::get('id') != null)
            $('select[name="warehouse_province"]').val("{{ $warehouse_by_id['province_id'] }}");
            getRegency($('select[name="warehouse_province"]').val());
            $('select[name="warehouse_regency"]').val("{{ $warehouse_by_id['regency_id'] }}");
            getDistrict($('select[name="warehouse_regency"]').val());
            $('select[name="warehouse_district"]').val("{{ $warehouse_by_id['district_id'] }}");
            getVillage($('select[name="warehouse_district"]').val());
            $('select[name="warehouse_village"]').val("{{ $warehouse_by_id['village_id'] }}");
        @endif

        $('select[name="warehouse_province"]').change(function(){
            warehouse_province = $(this).val();
            getRegency(warehouse_province);
            $('select[name="warehouse_district"]').val("").trigger('change');
            $('select[name="warehouse_village"]').val("").trigger('change');
            regency_array = [];
        });
        $('select[name="warehouse_regency"]').change(function(){
            warehouse_regency = $(this).val();
            getDistrict(warehouse_regency);
            $('select[name="warehouse_village"]').val("").trigger('change');
            district_array = [];
        });
        $('select[name="warehouse_district"]').change(function(){
            warehouse_district = $(this).val();
            getVillage(warehouse_district);
            village_array = [];
        });

        function getProvince(){
            $.ajax({
                url: "/api/provinsi",
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var province = resultData['data'];
                    province_array.push("<option value=''>-- Pilih Provinsi --</option>");                        
                    for (let b = 0; b < province.length; b++) {
                        province_array.push("<option value='"+province[b]['provinsi_id']+"'>"+province[b]['provinsi_name']+"</option>");                        
                    }
                }
            });
        }

        function getRegency(province_id, regency_div = null){
            $.ajax({
                url: "/api/kabkot/"+province_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    // console.log(resultData);
                    var regency = resultData['data'];
                    console.log(resultData);
                    if(regency.length > 0){
                        for (let b = 0; b < regency.length; b++) {
                            regency_array.push("<option value='"+regency[b]['kabupaten_kota_id']+"'>"+regency[b]['kabupaten_kota_name']+"</option>");                        
                        }
                        $('select[name="warehouse_regency"]').html(regency_array);
                    }
                }
            });
        }

        function getDistrict(regency_id){
            $.ajax({
                url: "/api/kecamatan/"+regency_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(regency_id);
                    // console.log(resultData);
                    var district = resultData['data'];
                    console.log(resultData);
                    if(district.length > 0){
                        for (let b = 0; b < district.length; b++) {
                            district_array.push("<option value='"+district[b]['kecamatan_id']+"'>"+district[b]['kecamatan_name']+"</option>");                        
                        }
                        $('select[name="warehouse_district"]').html(district_array);
                    }
                }
            });
        }

        function getVillage(district_id){
            $.ajax({
                url: "/api/kelurahan/"+district_id,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(district_id);
                    // console.log(resultData);
                    var village = resultData['data'];
                    console.log(resultData);
                    if(village.length > 0){
                        for (let b = 0; b < village.length; b++) {
                            village_array.push("<option value='"+village[b]['kelurahan_desa_id']+"'>"+village[b]['kelurahan_desa_name']+"</option>");                        
                        }
                        $('select[name="warehouse_village"]').html(village_array);
                    }
                }
            });
        }

        clickCancelButton('{{ route("admin/warehouse") }}')

        exitPopup('{{ route("admin/warehouse") }}');
    </script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY', false)}}&libraries=places"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC47jAGLaWLGMQFZejIMdE_xUHNWCh-BsM&libraries=places"></script>
    <script>
        var marker;
        var markers = [];

        function initialize() {
            var lat = -6.1826302;
            var lng = 106.6326605;
            var propertiMap = {
                center:new google.maps. LatLng(lat,lng),
                zoom:11,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            
            var map = new google.maps.Map(document.getElementById("googleMap"), propertiMap);

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    if(document.getElementById("latitude").value == null ||document.getElementById("latitude").value == ''){
                        var initial_lat = position.coords.latitude;
                        var initial_lng = position.coords.longitude;
                    }else{
                        var initial_lat = parseFloat(document.getElementById("latitude").value);
                        var initial_lng = parseFloat(document.getElementById("longitude").value);
                    }
                    var pos = {
                        lat: initial_lat,
                        lng: initial_lng
                    };
                    map.setCenter(pos);
                    taruhMarker(map, pos);
                    document.getElementById("latitude").value = pos.lat;
                    document.getElementById("longitude").value = pos.lng;
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
                } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
                }

            google.maps.event.addListener(map, 'click', function(event) {
                taruhMarker(this, event.latLng);
            });

            var input = document.getElementById("pac-input");
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            var service = new google.maps.places.PlacesService(map);
            $('select[name="warehouse_village"]').change(function(){
                //console.log($(this).find(":selected").text());
                var input = document.getElementById('pac-input');
                $('#pac-input').val($(this).find(":selected").text());

                google.maps.event.trigger(input, 'focus', {});
                google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
                google.maps.event.trigger(this, 'focus', {});
            });

            function taruhMarker(map, posisiTitik){
                //console.log(posisiTitik);
                if( marker ){
                marker.setPosition(posisiTitik);
                } else {
                marker = new google.maps.Marker({
                    position: posisiTitik,
                    map: map,
                    draggable:true
                });
                }
                markers.push(marker);
                document.getElementById("latitude").value = marker.position.lat();
                document.getElementById("longitude").value = marker.position.lng();
                google.maps.event.addListener(marker, 'dragend', function(evt){
                    document.getElementById("latitude").value = evt.latLng.lat().toFixed(3);
                    document.getElementById("longitude").value = evt.latLng.lng().toFixed(3);
                });
            }

            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                const bounds = new google.maps.LatLngBounds();
                places.forEach(place => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                var pos_search = {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng()
                };
                //console.log(pos_search);
                taruhMarker(map, pos_search);
                document.getElementById("latitude").value = place.geometry.location.lat();
                document.getElementById("longitude").value = place.geometry.location.lng();

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
                });
                map.fitBounds(bounds);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
    @endif
@endsection
