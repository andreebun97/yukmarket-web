
@extends('admin::layouts.master')

@section('title',__('menubar.quality_control'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange"></i> {{ __('menubar.transaction') }}</li>
                                <li><i class="active text-bold"></i>{{ __('menubar.quality_control') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div  class="card">
                                        <div class="panel-heading border-bottom-grey">
                                                <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.quality_control') }}</h4>
                                               
                                                <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-heading padding-b-0">
                                            <form>
                                                @csrf
                                                <div class="col-md-12 no-padding">
                                                    <div class="col-md-9 padding-r-10">
                                                        <input type="text" name="order_code" id="order_code" value="{{ Request::get('order_code') }}" autocomplete="off" class="form-control margin-t25">
                                                    </div>
                                                    <div class="col-md-3 no-padding">
                                                        <button class="btn btn-orange btn-block margin-t25" type="submit" id="filterButton" disabled>{{ __('page.search') }}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                                @if(Request::get('order_code') != null)
                                                    @if($order != null)
                                                    <form id="qcForm" method="POST" action="{{ route('admin/order/qualityControl') }}">
                                                        @csrf
                                                        <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                                                        <div class="form-group">
                                                            <label for="order_status">{{ __('field.order_code') }}</label>
                                                            <input type="text" class="form-control" id="order_code" aria-describedby="order_code" value="{{ $order->order_code }}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_status">{{ __('field.destination_address') }}</label>
                                                            <textarea class="form-control" id="destination_address" readonly rows="5" style="resize:none">{{ $order->address_detail . ', ' . $order->kecamatan_name . ', ' .  $order->kelurahan_desa_name . ', ' .  $order->kabupaten_kota_name . ', ' . $order->provinsi_name . ', ' . $order->kode_pos }}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_status">{{ __('field.contact_person') }}</label>
                                                            <input type="text" class="form-control" id="contact_person" aria-describedby="contact_person" value="{{ $order->contact_person . ' ('.$order->phone_number.')'}}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="quality_control_status">{{ __('field.quality_control_status') }} <span class="required-label">*</span></label>
                                                            <input type="radio" id="quality_control_status" name="quality_control_status" aria-describedby="quality_control_status" value="Sesuai">Sesuai
                                                            <input type="radio" id="quality_control_status" name="quality_control_status" aria-describedby="quality_control_status" value="Tidak Sesuai">Tidak Sesuai
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_status">{{ __('field.quality_control_notes') }} <span class="required-label">*</span></label>
                                                            <textarea class="form-control" id="quality_control_notes" name="quality_control_notes" rows="5" style="resize:none;"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_status">{{ __('field.order_detail') }}</label>
                                                            <div id="order_detail_table">
                                                                <table class="w50">
                                                                    <thead class="text-bold">
                                                                        <tr>
                                                                            <td>{{ __('field.product_name') }}</td>
                                                                            <td>{{ __('field.product_quantity') }}</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-gray discard_changes_button">{{ __('page.cancel') }}</button>
                                                        <button type="submit" class="btn btn-orange">{{ __('page.confirm') }}</button>
                                                    </form>
                                                    @else
                                                    <div class="col-md-12 no-padding"> 
                                                        <div class="img-no-data">
                                                            <center>
                                                                <img src="{{asset('img/img-search-produk-terlaris.png')}}"/>
                                                                <h4>{{ __('page.no_data') }}</h4>
                                                            </center>
                                                        </div>
                                                        
                                                    </div>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>    
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var url  = "{{ Request::fullUrl() }}";
        url  = url.replace(/&amp;/g, '&');
        clickCancelButton(url)

        exitPopup(url);

        $('input[name="order_code"]').keyup(function(){
            if($(this).val().length > 0){
                $('button#filterButton').attr('disabled',false);
            }else{
                $('button#filterButton').attr('disabled',true);
            }
        });

        @if(Request::get('order_code') != null && $order != null)
            $('button#filterButton').attr('disabled',false);
            var order_id = $('input[name="order_id"]').val();
            $.ajax({
                url: "{{ route('admin/order/history') }}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    order_id: order_id
                },
                async: false,
                success: function(resultData){
                    var orderDetail = [];
                    var data = resultData['data'];
                    for (let b = 0; b < data.length; b++) {
                        orderDetail.push("<tr><td>"+data[b]['prod_name']+"</td><td>"+data[b]['quantity']+" Pcs</td></tr>");                        
                    }

                    $('#order_detail_table table tbody').html(orderDetail);
                }
            });

            $('form#qcForm').validate({
                errorClass: 'invalid-feedback',
                rules: {
                    quality_control_status: {
                        required: true
                    },
                    quality_control_notes: {
                        required: true
                    }
                },
                messages: {
                    quality_control_status: {
                        required: "{{ __('validation.required',['attribute' => __('validation.attributes.quality_control_status')]) }}"
                    },
                    quality_control_notes: {
                        required: "{{ __('validation.required',['attribute' => __('validation.attributes.quality_control_notes')]) }}"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                    showLoader();
                }
            });
        @endif
    </script>
    @endif
@endsection
