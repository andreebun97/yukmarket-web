
@extends('admin::layouts.master')

@section('title',__('menubar.arrival_confirmation'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li class="active text-bold ">{{ __('menubar.arrival_confirmation') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.arrival_confirmation') }}</h4>
                                            <div class="clearfix"></div>
                                        </div>
                                   
                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="payment_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.order_code') }}</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.total_price') }}</th>
                                                        <th>{{ __('field.purchased_date') }}</th>
                                                        <th>{{ __('field.order_status') }}</th>
                                                        <th>{{ __('field.payment_method') }}</th>
                                                    </tr>
                                                </thead>
                                                <tfoot >
                                                    <tr role="row">
                                                        <td colspan="7" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/transaction') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var paymentTable = $('#payment_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            order: [[ 5, "asc" ]],
            ajax: {
                url:'{{ route("datatable/get_payment") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.order_status = 8;
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.agent_user_id = "{{ $user['organization_id'] }}";
                    d.organization_type_id = "{{ $user['organization_type_id'] }}";
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'total_price',
                    name: 'total_price',
                    className: 'dt-body-right'
                },
                {
                    data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {data: 'order_status_name', name: 'order_status_name'},
                {data: 'payment_method_name', name: 'payment_method_name'},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        var idx = 0;
        setInterval( function () {
            idx += 1;
            console.log(idx);
            if(idx > 0 && idx % 180 == 0){
                paymentTable.ajax.reload(null, false);
            }
        }, 1000);
    </script>
    @endif
@endsection
