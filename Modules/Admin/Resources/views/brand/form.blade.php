@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_brand') : __('page.edit_brand')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.brand') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_brand') : __('page.edit_brand') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">  
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_brand') : __('page.edit_brand') }}</h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                        <form method="POST" action="{{ route('admin/brand/form') }}" id="brandForm" enctype="multipart/form-data" class="form-style">
                                            <div class="col-md-12 no-padding">
                                                <div class="panel-body" >
                                                    <div class="col-md-4 small-img padding-r-10">
                                                        <div id="showing_product_image">
                                                            <img src="{{ Request::get('id') == null ? asset('img/default_product.jpg') : '/'.$brand_by_id['brand_image'] }}" alt="" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                        </div>
                                                    </div>
                                                    @csrf
                                                    @if(Request::get('id') !== null)
                                                        <input type="hidden" name="brand_id" value="{{ $brand_by_id['brand_id'] }}">
                                                        <input type="hidden" name="brand_image_temp" value="{{ $brand_by_id['brand_image'] }}">
                                                    @endif
                                                    <div class="col-md-4 res-left-form res-no-pad-left">
                                                        <div class="info">
                                                            <i class="fa fa-info-circle"></i>
                                                            {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="brand_name">{{ __('field.brand_name') }} <span class="required-label">*</span></label>
                                                            <input type="text" name="brand_name" class="form-control @error('brand_name') is-invalid @enderror" value="{{ Request::get('id') == null ? old('brand_name') : $brand_by_id['brand_name'] }}">
                                                            @error('brand_name')
                                                                <span class="invalid-feedback">{{ $errors->first('brand_name') }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="brand_description">{{ __('field.brand_description') }} <span class="required-label">*</span></label>
                                                            <textarea name="brand_description" style="resize:none; height: 185px;" class="form-control @error('brand_description') is-invalid @enderror">{{ Request::get('id') == null ? old('brand_description') : $brand_by_id['brand_desc'] }}</textarea>
                                                            @error('brand_description')
                                                                <span class="invalid-feedback">{{ $errors->first('brand_description') }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group no-margin">
                                                                <label style="display: block;" for="brand_image">{{ __('field.brand_image') }}</label>
                                                                <div class="clearfix"></div>
                                                                <div class="input-img-name">
                                                                    <span id="image_name">{{ __('page.no_data') }}</span>
                                                                    <button class="btn btn-green-upload no-margin" id="brand_image_button">{{ __('page.upload') }}</button>
                                                                </div>
                                                                <input type="file" name="brand_image" style="display: none;">
                                                                @error('brand_image')
                                                                    <span class="invalid-feedback">{{ $errors->first('brand_image') }}</span>
                                                                @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 no-padding">
                                                <div class="card-footer bg-transparent ">
                                                    <div class="col-md-4 no-padding float-right">
                                                        <div class="col-xs-6 padding-r-10">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                        </div>
                                                        <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="brand_name"], textarea[name="brand_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        @if(Request::get('id') == null)
        var validationRules = {
            brand_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            brand_description: {
                required: true
                // minlength: 10
            },
            brand_image: {
                required: true,
                extension: "png|pneg|svg|jpg|jpeg",
                maxsize: 2000000
            }
        };
        var validationMessages = {
            brand_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.brand_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.brand_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.brand_name')]) }}"
            },
            brand_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.brand_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.brand_description'), 'min' => 10]) }}"
            },
            brand_image: {
                required: "{{ __('validation.custom.brand_image.required',['attribute' => __('validation.attributes.brand_image')]) }}",
                extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.brand_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
            }
        };
        @else
        var validationRules = {
            brand_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            brand_description: {
                required: true
                // minlength: 10
            },
            // brand_image: {
            //     required: true,
            //     extension: "png|pneg|svg|jpg|jpeg"
            // }
        };
        var validationMessages = {
            brand_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.brand_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.brand_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.brand_name')]) }}"
            },
            brand_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.brand_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.brand_description'), 'min' => 10]) }}"
            },
            // brand_image: {
            //     required: "{{ __('validation.custom.brand_image.required',['attribute' => __('validation.attributes.brand_image')]) }}",
            //     extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.brand_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}"
            // }
        };
        @endif

        $('form#brandForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#brand_image_button').click(function(e){
            e.preventDefault();
            $('input[name="brand_image"]').click();
        });

        $('input[name="brand_image"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });

        clickCancelButton('{{ route("admin/brand") }}')

        exitPopup('{{ route("admin/brand") }}');
    </script>
    @endif
@endsection
