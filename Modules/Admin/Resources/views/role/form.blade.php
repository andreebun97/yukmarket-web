@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_role') : __('page.edit_role')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'user_management'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-group position-left"></i>{{ __('menubar.user_management') }}</li>
                                <li>{{ __('menubar.role') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_role') : __('page.edit_role') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat fit-height">
                                    <div class="card">
                                            <div class="panel-heading border-bottom-grey">
                                                <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_role') : __('page.edit_role') }}</h4>
                                               
                                                <div class="clearfix"></div>
                                            </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <form action="{{ route('admin/role/form') }}" id="roleForm" method="POST">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body" >
                                                        <div class="col-md-4 no-padding">
                                                            <div class="card-body">
                                                                @csrf
                                                                @if(Request::get('id') !== null)
                                                                    <input type="hidden" name="role_id" value="{{ Request::get('id') }}">
                                                                @endif
                                                                <div class="form-group">
                                                                    <label for="role_name">{{ __('field.role_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" name="role_name" class="form-control @error('role_name') is-invalid @enderror" value="{{ Request::get('id') == null ? old('role_name') : $role_by_id['role_name'] }}">
                                                                    @error('role_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('role_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <!-- <div class="form-group">
                                                                    <label for="role_type">{{ __('field.role_type') }}</label>
                                                                    <select name="role_type" id="role_type" class="form-control single-select">
                                                                        <option value="">{{ __('page.choose_role_type') }}</option>
                                                                        @for($b = 0; $b < count($role_type); $b++)
                                                                            <option value="{{ $role_type[$b]['role_type_id'] }}" {{ Request::get('id') != null && $role_by_id['role_type_id'] == $role_type[$b]['role_type_id'] ? 'selected' : '' }}>{{ $role_type[$b]['role_type_name'] }}</option>
                                                                        @endfor
                                                                    </select>
                                                                    @error('role_type')
                                                                        <span class="invalid-feedback">{{ $errors->first('role_type') }}</span>
                                                                    @enderror
                                                                </div> -->
                                                                <div class="form-group">
                                                                    <label for="role_description">{{ __('field.role_description') }} <span class="required-label">*</span></label>
                                                                    <textarea name="role_description" class="form-control @error('role_description') is-invalid @enderror" style="resize:none;height:180px">{{ Request::get('id') == null ? old('role_description') : $role_by_id['role_desc'] }}</textarea>
                                                                    @error('role_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('role_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 no-padding float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                    <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </form>
                                        @endif
                                      
                                            
                                            @if(Request::get('id') !== null && $accessed_menu == 1)
                                        <div class="separator no-margin"></div>
                                        <div class="panel-body" >
                                            <div class="margin-tb-10"></div>
                                            <button class="btn btn-orange" data-toggle="modal" data-target="#roleMenuAccess"><i class="fa fa-plus"></i> {{ __('page.modify_menu_settings') }}</button>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="table-no-datatable res-no-pad">
                                                <h3>Menu</h3>
                                                <table class="table display w-100 table-brown">
                                                    <thead class="bg-darkgrey">
                                                        <tr>
                                                            <th>{{ __('field.menu_name') }}</th>
                                                            <th>{{ __('field.menu_description') }}</th>
                                                            <th>{{ __('field.access_status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @for($b = 0; $b < count($all_menu_by_role); $b++)
                                                            <tr>
                                                                <td>{{ __('menubar.'.$all_menu_by_role[$b]['menu_label']) }}</td>
                                                                <td>{{ $all_menu_by_role[$b]['menu_desc'] }}</td>
                                                                <td class="{{ $all_menu_by_role[$b]['access_status'] == 1 ? 'active-status' : 'inactive-status' }}"></td>
                                                            </tr>
                                                        @endfor
                                                    </tbody>
                                                </table>
                                                <h3>Dashboard</h3>
                                                <table class="table display w-100 table-brown">
                                                    <thead class="bg-darkgrey">
                                                        <tr>
                                                            <th>{{ __('field.menu_name') }}</th>
                                                            <th>{{ __('field.menu_description') }}</th>
                                                            <th>{{ __('field.access_status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @for($b = 0; $b < count($dashboard_special_features); $b++)
                                                            <tr>
                                                                <td>{{ __('menubar.'.$dashboard_special_features[$b]['menu_label']) }}</td>
                                                                <td>{{ $dashboard_special_features[$b]['menu_desc'] }}</td>
                                                                <td class="{{ $dashboard_special_features[$b]['access_status'] == 1 ? 'active-status' : 'inactive-status' }}"></td>
                                                            </tr>
                                                        @endfor
                                                    </tbody>
                                                </table>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @if(Request::get('id') !== null && $accessed_menu == 1)
        <!-- Modal -->
        <div class="modal fade" id="roleMenuAccess" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="roleMenuAccessLabel" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content border-radius-10">
                    <div class="modal-header">
                        <h5 class="modal-title" id="roleMenuAccessLabel">{{ __('field.menu_access') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('admin/menu_access/form') }}">
                            @csrf
                            <input type="hidden" name="role_id" value="{{ Request::get('id') }}">
                            <h3>Menu</h3>
                            <ul class="list-group menu_access_list">
                                <li class="list-group-item no-children"><input type="checkbox" name="select_all_menu"> {{ __('field.select_all') }}</li>
                                @for($b = 0; $b < count($all_menu_by_role); $b++)
                                <li class="list-group-item{{ $all_menu_by_role[$b]['parent_id'] == null ? ' no-children' : '' }}" @if($all_menu_by_role[$b]['parent_id'] == null) data-menu-id="{{ $all_menu_by_role[$b]['menu_id'] }}" @else data-parent-id="{{ $all_menu_by_role[$b]['parent_id'] }}" @endif><input class="input-access" type="checkbox" name="access_status[]" value="{{ $all_menu_by_role[$b]['menu_id'] }}" @if($all_menu_by_role[$b]['access_status'] == 1) checked @endif> {{ __('menubar.'.$all_menu_by_role[$b]['menu_label']) }}</li>
                                @endfor
                            </ul>
                            <h3>Dashboard</h3>
                            <ul class="list-group menu_access_list">
                                <li class="list-group-item no-children"><input type="checkbox" name="select_all_dashboard_features"> {{ __('field.select_all') }}</li>
                                @for($b = 0; $b < count($dashboard_special_features); $b++)
                                    <li class="list-group-item"><input class="input-access dashboard_special_features" type="checkbox" name="access_status[]" value="{{ $dashboard_special_features[$b]['menu_id'] }}" @if($dashboard_special_features[$b]['access_status'] == 1) checked @endif> {{ __('menubar.'.$dashboard_special_features[$b]['menu_label']) }}</li>
                                @endfor
                            </ul>
                        </form>
                    </div>
                    <div class="modal-footer">
                    <div class="col-sm-12 no-padding margin-t10">
                        <div class="col-md-6 padding-r-10 res-btn-mid">
                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="button" class="btn btn-orange btn-block" id="submitModalButton" data-dismiss="modal">{{ __('page.submit') }}</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endif
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="role_name"], textarea[name="role_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var validationRules = {
            role_name: {
                required: true,
                alpha: true,
                minlength: 5
            },
            role_description: {
                required: true,
                minlength: 10
            }
        };
        var validationMessages = {
            role_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.role_name')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.role_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.role_name')]) }}"
            },
            role_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.role_description')]) }}",
                minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.role_description'), 'min' => 10]) }}"
            }
        };

        $('form#roleForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        @if(Request::get('id') != null)
        var checked_select_menu = 0;
        var checked_select_dashboard = 0;
        var accessed_menu = "{{ $accessed_menu_by_role }}".split(',');
        console.log(accessed_menu);
        $('#roleMenuAccess').on('shown.bs.modal', function(){
            cancel = 0;
            shown = 0;
            modal = '#roleMenuAccess';
        });

        $('#roleMenuAccess').on('hidden.bs.modal', function(){
            cancel = 0;
            shown = 0;
            console.log(accessed_menu);
            @if(Request::get('id') != null)
                $('input[name="access_status[]"]').val(accessed_menu);
            @endif
            // $('input[name="access_status[]"]').prop('checked',false);
            // $('input[name="select_all"]').prop('checked',false);
        });

        $('#roleMenuAccess input[type="checkbox"]').click(function(){
            cancel = 1;
            shown = 1;
        });

        $('#submitModalButton').click(function(){
            $('#roleMenuAccess form').submit();
        });

        $('.list-group-item input[type="checkbox"]').click(function(){
            checkbox_value = $(this).val();
            console.log(checkbox_value);
            console.log($('.list-group-item[data-parent-id="'+checkbox_value+'"]').length);
            // flag = true;
            if($(this).prop('checked') == true){
                $('.list-group-item[data-parent-id="'+checkbox_value+'"] input[type="checkbox"]').prop('checked',true);
            }else{
                $('.list-group-item[data-parent-id="'+checkbox_value+'"] input[type="checkbox"]').prop('checked',false);
                
            }
        });

        $('input[name="select_all_menu"]').click(function(){
            if(checked_select_menu == 0){
                checked_select_menu = 1;
                $('input[name="access_status[]"]:not(.dashboard_special_features)').prop('checked',true);
            }else{
                checked_select_menu = 0;
                $('input[name="access_status[]"]:not(.dashboard_special_features)').prop('checked',false);
            }
        });

        $('input[name="select_all_dashboard_features"]').click(function(){
            // console.log('all dashboard features');
            if(checked_select_dashboard == 0){
                checked_select_dashboard = 1;
                $('input[name="access_status[]"].dashboard_special_features').prop('checked',true);
            }else{
                checked_select_dashboard = 0;
                $('input[name="access_status[]"].dashboard_special_features').prop('checked',false);
            }
        });
        @endif

        clickCancelButton('{{ route("admin/role") }}')

        exitPopup('{{ route("admin/role") }}');
    </script>
    @endif
@endsection
