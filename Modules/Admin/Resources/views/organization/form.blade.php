@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_organization') : __('page.edit_organization')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.organization') }}</li>
                                <li class="active text-bold ">{{ Request::get('id') == null ? __('page.add_organization') : __('page.edit_organization') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_organization') : __('page.edit_organization') }}</h4>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            @php $route = route('admin/organization/form'); @endphp
                                            <form method="POST" id="organizationForm" action="{{ $route }}" class="form-style" enctype="multipart/form-data">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('id') != null)
                                                            <input type="hidden" name="organization_id" value="{{ $organization_by_id['organization_id'] }}">
                                                        @endif
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad">
                                                            <div class="info">
                                                                <i class="fa fa-info-circle"></i>
                                                                {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="organization_name">{{ __('field.organization_name') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control @error('organization_name') is-invalid @enderror" name="organization_name" value="{{ Request::get('id') == null ? old('organization_name') : $organization_by_id['organization_name'] }}" autocomplete="off">
                                                                @error('organization_name')
                                                                    <span class="invalid-feedback">{{ $errors->first('organization_name') }}</span>
                                                                @enderror
                                                            </div>
                            
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label for="rating">{{ __('field.rating') }} <span class="required-label">*</span></label>
                                                                <div class="col-xs-12 no-padding">
                                                                   <div class="out-of no-padding mb-20">
                                                                        <input type="text" class="form-control @error('rating') is-invalid @enderror" name="rating" value="{{ Request::get('id') == null ? old('rating') : $organization_by_id['rating'] }}" autocomplete="off">
                                                                        <span> /10</span>
                                                                        @error('rating')
                                                                            <span class="invalid-feedback">{{ $errors->first('rating') }}</span>
                                                                        @enderror
                                                                   </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label for="organization_type">{{ __('field.organization_type') }} <span class="required-label">*</span></label>
                                                                <select name="organization_type" class="form-control single-select">
                                                                    <option value="">{{ __('page.choose_organization_type') }}</option>
                                                                    @for($b = 0; $b < count($organization_type); $b++)
                                                                        <option value="{{ $organization_type[$b]['organization_type_id'] }}" {{ (Request::get('id') != null && $organization_type[$b]['organization_type_id'] == $organization_by_id['organization_type_id']) ? 'selected' : '' }}>{{ $organization_type[$b]['organization_type_name'] }}</option>
                                                                    @endfor
                                                                </select>
                                                                @error('organization_type')
                                                                    <span class="invalid-feedback">{{ $errors->first('organization_type') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="form-group">
                                                                <label for="price_type">{{ __('field.price_type') }} <span class="required-label">*</span></label>
                                                                <select name="price_type" class="form-control single-select">
                                                                    <option value="">{{ __('page.choose_price_type') }}</option>
                                                                    @for($b = 0; $b < count($price_type); $b++)
                                                                        <option value="{{ $price_type[$b]['price_type_id'] }}" {{ (Request::get('id') != null && $price_type[$b]['price_type_id'] == $organization_by_id['price_type_id']) ? 'selected' : '' }}>{{ $price_type[$b]['price_type_name'] }}</option>
                                                                    @endfor
                                                                </select>
                                                                @error('price_type')
                                                                    <span class="invalid-feedback">{{ $errors->first('price_type') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="organization_logo">{{ __('field.organization_logo') }} <span class="required-label">*</span></label>
                                                                <div class="clearfix"></div>
                                                                <div class="input-img-name">
                                                                    <span id="image_name">{{ __('page.no_data') }}</span>
                                                                    <input type="file" name="organization_logo" class="form-control" style="display:none;">
                                                                    <button type="button" class="btn btn-green-upload no-margin" id="organization_logo_button">{{ __('page.upload') }}</button> 
                                                                </div>
                                                                @error('organization_logo')
                                                                    <span class="invalid-feedback">{{ $errors->first('organization_logo') }}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12 col-xs-12 res-nopad">
                                                            <div class="form-group">
                                                                <label for="total_order">{{ __('field.total_order') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="form-control @error('total_order') is-invalid @enderror" name="total_order" value="{{ Request::get('id') == null ? old('total_order') : $organization_by_id['total_order'] }}" autocomplete="off">
                                                                @error('total_order')
                                                                    <span class="invalid-feedback">{{ $errors->first('total_order') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="organization_description">{{ __('field.organization_description') }} <span class="required-label">*</span></label>
                                                                <textarea name="organization_description" class="form-control @error('organization_description') is-invalid @enderror" style="resize:none;height:285px">{{ Request::get('id') == null ? old('organization_description') : $organization_by_id['organization_desc'] }}</textarea>
                                                                @error('organization_description')
                                                                    <span class="invalid-feedback">{{ $errors->first('organization_description') }}</span>
                                                                @enderror
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-12 no-padding">
                                                        <div class="card-footer bg-transparent ">
                                                            <div class="col-md-4 float-right">
                                                                <div class="col-xs-6 padding-r-10">
                                                                <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                </div>
                                                                <div class="col-xs-6 padding-l-10">
                                                                <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                                    </div>  
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        jQuery.validator.addMethod("greaterThan", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }

            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'Must be greater than {0}.');

        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="organization_name"], textarea[name="organization_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        $('#organization_logo_button').click(function(e){
            e.preventDefault();
            $(this).siblings('input[name="organization_logo"]').click();
        });

        $('input[name="organization_logo"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            // readURL(this,'#showing_product_image img');
            console.log('The file "' + fileName +  '" has been selected.');
        });

        var validationRules = {
            organization_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            organization_description: {
                required: true
                // minlength: 5
            },
            rating: {
                required: true,
                max: 10
            },
            organization_type: {
                required: true
            },
            price_type: {
                required: true
            },
            organization_logo: {
                required: true,
                extension: "png|pneg|svg|jpg|jpeg",
                maxsize: 2000000
            },
            total_order: {
                required: true
            }
        };
        var validationMessages = {
            organization_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.organization_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.organization_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.organization_name')]) }}"
            },
            organization_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.organization_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.organization_description'), 'min' => 5]) }}"
            },
            rating: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.rating')]) }}",
                max: "{{ __('validation.max.numeric',['attribute' => __('validation.attributes.rating'), 'max' => 10]) }}"
            },
            organization_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.organization_type')]) }}"
            },
            price_type: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.price_type')]) }}"
            },
            organization_logo: {
                required: "{{ __('validation.custom.organization_logo.required',['attribute' => __('validation.attributes.product_picture')]) }}",
                extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.organization_logo'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
            },
            total_order: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.total_order')]) }}"
            }
        }
        $('form#organizationForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        clickCancelButton('{{ route("admin/organization") }}')

        exitPopup('{{ route("admin/organization") }}');
    </script>
    @endif
@endsection
