
@extends('admin::layouts.master')

@section('title',__('menubar.aboutus'))

@section('content')
<div class="dashboard-container">
    @include('menubar.admin')
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            @include('sidebar.admin',['sidebar' => 'cms'])

            <!-- Main content -->
            <div class="content-wrapper  padding-t47">
                <div class="page-header page-header-default">
                    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                            <li><i class="fa fa-folder position-left"></i>CMS</li>
                            <li class="active text-bold ">Tentang Kita</li>
                        </ul>
                    </div>
                </div>
                <!-- Content area -->
                <div class="content" >
                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-flat" >
                                <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.aboutus') }}</h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if($accessed_menu == 0)
                                    @include('prohibited_page')
                                    @else
                                    <form method="POST" id="aboutusform" action="{{ route('admin/cms/aboutus/update') }}" enctype="multipart/form-data" id="settingsForm">
                                        @csrf
                                        @if(count($aboutus) > 0)
                                        @foreach($aboutus as $row)
                                        <div class="col-md-12 no-padding">
                                            <div class="panel-body" >
                                                <div class="col-md-4 med-img padding-r-10">
                                                    <div id="showing_product_image">
                                                        <center><img src="{{ $row->image == null ? asset('img/default_product.jpg') : asset('img/uploads/aboutus').'/'.$row->image }}" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></center>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 no-padding">
                                                    <div class="form-group">
                                                        <label for="aboutus_title">{{ __('field.aboutus_title') }}</label>
                                                        <input type="text" class="form-control" name="title" value="{{ $row->title}}" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="product_picture" class="dp-block">{{ __('field.aboutus_image') }} @if(Request::get('product') == null)</label> @endif
                                                        <div class="clearfix"></div>
                                                        <div class="input-img-name">
                                                            <span id="image_name">{{ __('page.no_data') }}</span>
                                                            <input type="file" name="aboutus_picture" class="form-control" style="display:none;">
                                                            <input type="hidden" name="aboutus_picture_temp" value="{{ $row->image }}">
                                                            <button type="button" class="btn btn-green-upload no-margin" id="aboutus_picture_button">{{ __('page.upload') }}</button> 
                                                        </div>
                                                        @error('aboutus_picture')
                                                        <span class="invalid-feedback dp-block">{{ $errors->first('aboutus_picture') }}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="aboutus_title">{{ __('field.aboutus_body') }}</label>
                                                        <textarea id="myEditor" class="form-control" name="body">{{ $row->body }}</textarea>
                                                    </div>
                                                </div>

                                            </div>                                       

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="card-footer bg-transparent ">
                                                <div class="col-md-4 no-padding float-right">
                                                    <div class="col-xs-6 padding-r-10">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                    </div>
                                                    <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="col-md-12 no-padding">
                                            <div class="panel-body" >
                                                <div class="col-md-4 med-img padding-r-10">
                                                    <div id="showing_product_image">
                                                        <center><img src="{{ asset('img/default_product.jpg') }}" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></center>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 no-padding">
                                                    <div class="form-group">
                                                        <label for="aboutus_title">{{ __('field.aboutus_title') }}</label>
                                                        <input type="text" class="form-control" name="title" value="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="product_picture" class="dp-block">{{ __('field.aboutus_image') }} </label>
                                                        <div class="clearfix"></div>
                                                        <div class="input-img-name">
                                                            <span id="image_name">{{ __('page.no_data') }}</span>
                                                            <input type="file" name="aboutus_picture" class="form-control" style="display:none;">
                                                            <input type="hidden" name="aboutus_picture_temp" value="">
                                                            <button type="button" class="btn btn-green-upload no-margin" id="aboutus_picture_button">{{ __('page.upload') }}</button> 
                                                        </div>
                                                        @error('aboutus_picture')
                                                        <span class="invalid-feedback dp-block">{{ $errors->first('aboutus_picture') }}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="aboutus_title">{{ __('field.aboutus_body') }}</label>
                                                        <textarea id="myEditor" class="form-control" name="body"></textarea>
                                                    </div>
                                                </div>

                                            </div>                                       

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="card-footer bg-transparent ">
                                                <div class="col-md-4 no-padding float-right">
                                                    <div class="col-xs-6 padding-r-10">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                    </div>
                                                    <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        @endif

                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</div>
@include('loader')
@if(Session::get('message_alert') != null)
@include('error_popup')
@endif
@include('assets_link.js_list')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showing_product_image img').attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#aboutus_picture_button').click(function (e) {
        e.preventDefault();
        $(this).siblings('form#aboutusform input[name="aboutus_picture"]').click();
    });

    $('#aboutus_logo_button').click(function (e) {
        e.preventDefault();
        $('input[name="aboutus_picture"]').click();
    });

    $('input[name="aboutus_picture"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#image_name').html(fileName);
        readURL(this);
        console.log('The file "' + fileName + '" has been selected.');
    });

    $('form#aboutusform input[name="aboutus_picture"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#image_name').html(fileName);
        readURL(this, '#showing_product_image img');
        console.log('The file "' + fileName + '" has been selected.');
    });

    var validationRules = {
        title: {
            required: true
        },
        aboutus_picture: {
            extension: "png|pneg|svg|jpg|jpeg"
        },
        body: {
            required: true
        }
    };
    var validationMessages = {
        title: {
            required: "{{ __('validation.required',['attribute' => __('validation.attributes.title')]) }}",
        },
        aboutus_picture: {
            extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.aboutus_picture'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}"
        },
        body: {
            required: "{{ __('validation.required',['attribute' => __('validation.attributes.body')]) }}"
        }
    }

    $('form#aboutusform').validate({
        errorClass: 'invalid-feedback',
        rules: validationRules,
        messages: validationMessages,
        ignore: [],
        submitHandler: function (form) {
            form.submit();
            showLoader();
        }
    });
</script>
<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script>tinymce.init({selector: 'textarea', menubar: false});</script>
@endsection
