@extends('admin::layouts.master')

@section('title',(Request::get('help') == null ? __('page.add_help') : __('page.edit_help')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'cms'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>Cms</li>
                                <li>Bantuan</li>
                                <li class="active text-bold ">{{ Request::get('help') == null ? __('page.add_help') : __('page.edit_help') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                            <div class="panel panel-flat">
                                <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('help') == null ? __('page.add_help') : __('page.edit_help') }}</h4>
                                            
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page') 
                                        @else
                                            <form method="POST" action="{{ route('admin/cms/help/form') }}" id="helpForm" enctype="multipart/form-data">
                                                <div class="col-md-12 no-padding">
                                                    <div class="panel-body">
                                                        @csrf
                                                        @if(Request::get('help') !== null)
                                                            <input type="hidden" name="help_id" value="{{ Request::get('help') }}">
                                                        @endif     
                                                        <div class="col-md-12 res-left-form res-no-pad-left">                 
                                                            <div class="form-group col-md-12 no-padding">
                                                                <label class="col-md-4 no-padding" for="category_name">{{ __('field.question_name') }} <span class="required-label">*</span></label>
                                                                <input type="text" class="col-md-8 form-control @error('question_name') is-invalid @enderror" name="question" autocomplete="off" @if(Request::get('help') == null) value="{{ old('question') }}" @else value="{{ $help['question'] }}"  @endif>
                                                                @error('question')
                                                                    <span class="invalid-feedback">{{ $errors->first('question') }}</span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="category_level">{{ __('field.answer_name') }} <span class="required-label">*</span></label>
                                                                <input type="text" name="answer" class="form-control" @if(Request::get('help') == null) value="" @else value="{{ $help['answer'] }}" @endif>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 no-padding">
                                                    <div class="card-footer bg-transparent ">
                                                        <div class="col-md-4 no-padding float-right">
                                                            <div class="col-xs-6 padding-r-10">
                                                            <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                            </div>
                                                            <div class="col-xs-6 padding-l-10">
                                                            <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        var parent_category_id = "";
        var category_id = null;
        @if(Request::get('category') != null)
            parent_category_id = "{{ $category['parent_category_id'] }}";
            category_id = "{{ Request::get('category') }}";
        @endif
        console.log(parent_category_id);

        getCategory(null, category_id);
        
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        $('input[name="category_name"], textarea[name="category_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });

        var helpId = $('input[name=help_id]').val();
        if (typeof helpId === "undefined") {
            var validationRules = {
                question: {
                    required: true,
                },
                answer: {
                    required: true
                }
            };
            var validationMessages = {
                question: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.question')]) }}",
                },
                answer: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.answer')]) }}",
                },
            };
        } else {
            var validationRules = {
                question: {
                    required: true,
                },
                answer: {
                    required: true
                }
            };
            var validationMessages = {
                question: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_name')]) }}",
                },
                answer: {
                    required: "{{ __('validation.required',['attribute' => __('validation.attributes.category_description')]) }}",
                }
            };
        }

        $('form#helpForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function getCategory(id = null, parent = null){
            var url = "{{ route('admin/category/get') }}"+(id == null ? '' : '?id='+id)+(parent != null ? ((id == null ? '?' : '&')+'parent='+parent) : '');
            console.log(url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function(resultData){
                    console.log(resultData);
                    var data = resultData['data'];
                    // console.log(data);
                    var category_array = ["<option value=''>Pilih kategori di atasnya...</option>"];
                    for (let k = 0; k < data.length; k++) {
                        var obj = "<option value='"+data[k]['category_id']+"'>"+data[k]['category_name']+"</option>";
                        category_array.push(obj);
                    }
                    console.log(category_array);
                    if(id == null){
                        $('select[name="parent_category_id"]').html(category_array);
                        @if(Request::get('category') != null)
                            $('select[name="parent_category_id"]').val(parent_category_id).trigger("change");
                        @endif
                    }else{
                        $('input[name="category_level"]').val(parseInt(data[0]['category_level']) + 1);
                    }
                }
            });
        }

        $('select[name="parent_category_id"]').change(function(){
            var id = $(this).val();
            if($(this).val() == ""){
                $('input[name="category_level"]').val("1");
            }else{
                getCategory(id);
            }
        });

        clickCancelButton('{{ route("admin/cms/help") }}')

        exitPopup('{{ route("admin/cms/help") }}');

    </script>
@endsection
