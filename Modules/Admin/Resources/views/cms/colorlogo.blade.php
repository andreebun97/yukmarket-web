
@extends('admin::layouts.master')

@section('title',__('menubar.header'))

<style>
    .input-color {
        position: relative;
    }
    .input-color input {
        padding-left: 20px;
    }
    .input-color .color-box {
        width: 10px;
        height: 10px;
        display: inline-block;
        background-color: #ccc;
        position: absolute;
        left: 5px;
        top: 5px;
    }
</style>

@section('content')
<div class="dashboard-container">
    @include('menubar.admin')
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            @include('sidebar.admin',['sidebar' => 'cms'])

            <!-- Main content -->
            <div class="content-wrapper  padding-t47">
                <div class="page-header page-header-default">
                    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                            <li><i class="fa fa-folder position-left"></i>CMS</li>
                            <li class="active text-bold ">Warna Dan Logo</li>
                        </ul>
                    </div>
                </div>
                <!-- Content area -->
                <div class="content" >
                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-flat" >
                                <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.header') }}</h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if($accessed_menu == 0)
                                    @include('prohibited_page')
                                    @else
                                    <form method="POST" id="logoform" action="{{ route('admin/cms/header/update') }}" enctype="multipart/form-data" id="settingsForm">
                                        @csrf
                                        @if(count($colorlogo) > 0)
                                        @foreach($colorlogo as $row)
                                        <div class="col-md-12 no-padding">
                                            <div class="panel-body" >
                                               
                                                <div class="col-md-12 no-padding">
                                                    <div class="form-group">
                                                        <label for="color">Warna Utama</label>
                                                        <select class="form-control" name="color" id="color" onchange="colourFunction()">
                                                            <option value="">-- Pilih Warna --</option>
                                                            @foreach($color as $cl)
                                                            <option value="{{ $cl->code }}" {{$cl->code == $row->color ? 'selected' : ''}}> {{ $cl->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="color">Warna Kedua</label>
                                                        <select class="form-control" name="secondary_color" id="color">
                                                            <option value="">-- Pilih Warna --</option>
                                                            @foreach($color as $cl)
                                                            <option value="{{ $cl->code }}" {{$cl->code == $row->secondary_color ? 'selected' : ''}}> {{ $cl->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="logo_picture" class="dp-block">{{ __('field.logo_picture') }} @if(Request::get('header') == null)</label> @endif
                                                        <div class="clearfix"></div>
                                                        <div class="input-img-name">
                                                            <span id="image_name">{{ __('page.no_data') }}</span>
                                                            <input type="file" name="logo_picture" class="form-control" style="display:none;">
                                                            <input type="hidden" name="logo_picture_temp" value="{{ $row->logo }}">
                                                            <button type="button" class="btn btn-green-upload no-margin" id="logo_picture_button">{{ __('page.upload') }}</button> 
                                                        </div>
                                                        @error('logo_picture')
                                                        <span class="invalid-feedback dp-block">{{ $errors->first('logo_picture') }}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group" style="text-align: center;">
                                                        <div id="showing_product_image" style="width: 30%; height: 30%; text-align: center;">
                                                            <center><img src="{{ $row->logo == null ? asset('img/default_product.jpg') : asset('img/uploads/logo').'/'.$row->logo }}" style="width: 50px;" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></center>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="logo_page_picture" class="dp-block">Logo Halaman @if(Request::get('header') == null)</label> @endif
                                                        <div class="clearfix"></div>
                                                        <div class="input-img-name">
                                                            <span id="image_name_picture">{{ __('page.no_data') }}</span>
                                                            <input type="file" name="logo_page_picture" class="form-control" style="display:none;">
                                                            <input type="hidden" name="logo_page_picture_temp" value="{{ $row->logo_page }}">
                                                            <button type="button" class="btn btn-green-upload no-margin" id="logo_page_picture_button">{{ __('page.upload') }}</button> 
                                                        </div>
                                                        @error('logo_page_picture')
                                                        <span class="invalid-feedback dp-block">{{ $errors->first('logo_page_picture') }}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group" style="text-align: center;">
                                                        <div id="showing_logo_page_image" style="width: 30%; height: 30%; text-align: center;">
                                                            <center><img src="{{ $row->logo_page == null ? asset('img/default_product.jpg') : asset('img/uploads/logo').'/'.$row->logo_page }}" style="width: 50px;" alt="" class="dp-block mg-auto" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';"></center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="card-footer bg-transparent ">
                                                <div class="col-md-4 no-padding float-right">
                                                    <div class="col-xs-6 padding-r-10">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        
                                        @endif
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</div>
@include('loader')
@if(Session::get('message_alert') != null)
@include('error_popup')
@endif
@include('assets_link.js_list')
<script>
    function readURL(input, div) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(div).attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    
    }

    $('#logo_picture_button').click(function (e) {
        e.preventDefault();
        $(this).siblings('form#logoform input[name="logo_picture"]').click();
    });
    
    $('#logo_page_picture_button').click(function (e) {
        e.preventDefault();
        $(this).siblings('form#logoform input[name="logo_page_picture"]').click();
    });

    // $('#logo_picture_button').click(function (e) {
    //     e.preventDefault();
    //     $('input[name="logo_picture"]').click();
    // });
    
    // $('#logo_page_picture_button').click(function (e) {
    //     e.preventDefault();
    //     $('input[name="logo_page_picture"]').click();
    // });

    // $('input[name="logo_picture"]').change(function (e) {
    //     var fileName = e.target.files[0].name;
    //     $('#image_name').html(fileName);
    //     readURL(this);
    //     console.log('The file "' + fileName + '" has been selected.');
    // });
    
    $('input[name="logo_page_picture"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#image_name_picture').html(fileName);
        readURL(this, '#showing_logo_page_image img');
        console.log('The file "' + fileName + '" has been selected.');
    });

    $('form#logoform input[name="logo_picture"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $('#image_name').html(fileName);
        readURL(this, '#showing_product_image img');
        console.log('The file "' + fileName + '" has been selected.');
    });
    
    // $('form#logoform input[name="logo_page_picture"]').change(function (e) {
    //     var fileName = e.target.files[0].name;
    //     $('#image_name_page').html(fileName);
    //     readURL(this, '#showing_logo_page_image img');
    //     console.log('The file "' + fileName + '" has been selected.');
    // });

    var validationRules = {
        color: {
            required: true
        },
        logo_picture: {
            extension: "png|pneg|svg|jpg|jpeg"
        },
        logo_page_picture: {
            extension: "png|pneg|svg|jpg|jpeg"
        }
    };
    var validationMessages = {
        color: {
            required: "{{ __('validation.required',['attribute' => __('validation.attributes.color')]) }}",
        },
        logo_picture: {
            extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.logo_picture'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}"
        },
        logo_page_picture: {
            extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.logo_picture'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}"
        }
    }

    $('form#logoform').validate({
        errorClass: 'invalid-feedback',
        rules: validationRules,
        messages: validationMessages,
        ignore: [],
        submitHandler: function (form) {
            form.submit();
            showLoader();
        }
    });
</script>
@endsection
