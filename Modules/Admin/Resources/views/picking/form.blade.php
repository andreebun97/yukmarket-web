
@extends('admin::layouts.master')

@section('title',__('page.picking_form'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.transaction') }}</li>
                                <!-- <li class="active text-bold ">{{ __('menubar.transaction') }}</li> -->
                                <li>{{ __('menubar.picking') }}</li>
                                <li class="active text-bold ">{{ __('page.picking_form') . ' ('.$order_master->order_code.')' }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.picking_form') . ' ('.$order_master->order_code.')' }}</h4>
                                        <div class="clearfix"></div>
                                    </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                        <div class="row no-padding">
                                            <div class="col-md-12 trs-detail">
                                                <div class="col-md-6 no-padding">
                                                    <div id="user_profile " class="info-detail">
                                                        <h2>{{ __('page.customer_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_name">{{ __('field.user_name') }}</label>
                                                            <span class="col-md-6 user_data_information">
                                                            {{ $order_master->contact_person == null ? ($order_master->customer_name != null ? $order_master->customer_name : $order_master->buyer_name): ($order_master->contact_person) }}
                                                            </span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_email">{{ __('field.user_email') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->customer_email }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6" for="user_phone_number">{{ __('field.user_phone_number') }}</label>
                                                            <span class="col-md-6 dd user_data_information">{{ $order_master->phone_number == null ? $phone_number->convert($order_master->customer_phone_number): $phone_number->convert($order_master->phone_number) }}</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 no-padding">
                                                    <div class="info-detail">
                                                        <h2>{{ __('page.shipment_info') }}</h2>
                                                        <div class="form-group no-padding">
                                                            <label class="col-md-6  " for="destination_address">{{ __('field.destination_address') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->destination_address == null ? ($order_master->buyer_address == null ? "-" : $order_master->buyer_address) :  $order_master->address_detail . ', ' . ($order_master->kelurahan_desa_name . ', ' . $order_master->kecamatan_name . ', ' . $order_master->kabupaten_kota_name . ', ' . $order_master->kode_pos . " (".($order_master->address_info == null ? "-" : $order_master->address_info).", ".($order_master->address_name == null ? "-" : $order_master->address_name).")") }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <!-- <div class="form-group  no-padding">
                                                            <label class="col-md-6" for="contact_person">{{ __('field.contact_person') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->contact_person == null ? "-" : ($order_master->contact_person . ' (' . $phone_number->convert($order_master->phone_number) . ')') }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div> -->
                                                        <div class="form-group  no-padding">
                                                            <label class="col-md-6"   for="payment_method">{{ __('field.shipment_method') }}</label>
                                                            <span class="col-md-6 user_data_information">{{ $order_master->shipment_method_name }}</span>    
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                    <div id="user_transaction" class="info-detail">
                                                        <h2>{{ __('page.transactions') }}</h2>
                                                        <div id="purchase_transaction">
                                                            <div class="form-group">
                                                                <div id="purchase_transaction_detail">
                                                                        @if(count($transactions) == 0)  
                                                                            <span>{{ __('page.no_data') }}</span>
                                                                        @else
                                                                        
                                                                        
                                                                        <div class="col-xs-12 no-padding res-scroll-md">
                                                                            <table class="table display w-100 table-brown total-transaksi" id="banner_list_table">
                                                                                <thead class="bg-darkgrey">
                                                                                    <tr role="row">
                                                                                        <th>Gambar</th>
                                                                                        <th>Kode</th>
                                                                                        <th style="min-width: 200px;">Nama</th>
                                                                                        <th>Jumlah</th>
                                                                                        <th>Stock Tersedia</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                @php $subtotal = 0; @endphp
                                                                                    @for($b = 0; $b < count($transactions); $b++)
                                                                                    @php $subtotal += $transactions[$b]->quantity *  $transactions[$b]->price; @endphp
                                                                                    <tr {{ $b < count($transactions) - 1 ? '' : ''}}>
                                                                                        <td><img src="{{ asset($transactions[$b]->prod_image) }}" class="img-50" alt="" onerror="this.src='{{ asset('img/default_product.jpg') }}'"></td>
                                                                                        <td>{{ $transactions[$b]->prod_code }}</td>
                                                                                        <td>
                                                                                            {{ $transactions[$b]->prod_name . ($transactions[$b]->sku_status == 0 ? '' : (' '.($transactions[$b]->uom_value+0).' '.$transactions[$b]->uom_name)) }}
                                                                                            @if($transactions[$b]->quantity > $transactions[$b]->stock)
                                                                                            <br><span style="color: red">Stok produk ini tidak memenuhi, silahkan hubungi gudang penyimpanan anda.</span>
                                                                                            @endif
                                                                                        </td>
                                                                                        @if(isset($transactions[$b]->qty_uom))
                                                                                            <td class="text-right">{{ $transactions[$b]->quantity }} {{$transactions[$b]->qty_uom}}</td>
                                                                                        @else
                                                                                            <td class="text-right">{{ $transactions[$b]->quantity }}</td>
                                                                                        @endif

                                                                                        @if(isset($transactions[$b]->stock_av_uom))
                                                                                            <td class="text-right">{{ $transactions[$b]->stock }} {{$transactions[$b]->stock_av_uom}}</td>
                                                                                        @else
                                                                                            <td class="text-right">{{ $transactions[$b]->stock }}</td>
                                                                                        @endif
                                                                                    </tr>
                                                                                    @endfor
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        @php $route = ""; @endphp
                                                                        @if($order_master->picker_name == null)
                                                                            @php $route = "admin/order/setPicker"; @endphp
                                                                        @else
                                                                            @if($order_master->is_printed_label == 0 || $order_master->is_printed_label == 1)
                                                                                @php $route = "admin/order/submitPicking"; @endphp
                                                                            @else
                                                                                @php $route = "admin/order/pickUpOrder"; @endphp
                                                                            @endif
                                                                        @endif
                                                                        <form method="POST" id="pickingConfirmationForm" action="{{ route($route) }}">
                                                                            @csrf
                                                                            <input type="hidden" name="customer_id" value="{{ $order_master->buyer_user_id }}">
                                                                            <input type="hidden" name="is_agent" value="{{ $order_master->is_agent }}">
                                                                            <input type="hidden" name="fcm_token" value="{{ $order_master->fcm_token }}">
                                                                            <input type="hidden" name="order_id" value="{{ $order_master->order_id }}">
                                                                            <input type="hidden" name="order_code" value="{{ $order_master->order_code }}">
                                                                            <input type="hidden" name="preparation_id" value="{{ $order_master->preparation_id }}">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="picked_by">{{ __('field.picked_by') }}</label>
                                                                                    <select {{$disabled_picker > 0 ? 'disabled' : ''}} name="picker_name" id="picker_name" class="form-control single-select" placeholder="{{ __('page.choose_picker_name') }}">
                                                                                    <option value="">{{ __('page.choose_picker_name') }}</option>
                                                                                    @for($b = 0; $b < count($pickers); $b++)
                                                                                        <option value="{{ $pickers[$b]['user_id'] }}" {{ $order_master->picker_id != null && $order_master->picker_id == $pickers[$b]['user_id'] ? 'selected' : '' }}>{{ $pickers[$b]['user_name'] }}</option>
                                                                                    @endfor
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-transparent">
                                                <div class="col-md-6 col-sm-8 col-xs-12 res-btn no-padding float-right">
                                                    @if($order_master->is_printed_label != 0)
                                                    <div class="col-md-4 col-sm-4 col-xs-12 btn-3-col pad-ol-10">
                                                        <button type="button" id="pickingConfirmationButton" class="btn btn-green btn-block">{{ $order_master->picker_name == null ? __('page.submit') : ($order_master->is_printed_label == 0 || $order_master->is_printed_label == 1 ? __('page.submit') : __('page.confirm')) }}</button>
                                                    </div>
                                                    @endif
                                                    @if($order_master->picker_name != null && ($order_master->is_printed_label == 0))
                                                    <div class="col-md-4 col-sm-4 col-xs-12 btn-3-col pad-ol-10">
                                                        <button type="button" class="btn btn-orange btn-block" onclick="window.location.href = '{{ route("admin/transaction/label","?id=".$order_master->order_id) }}'">{{ __('page.print_label') }}</button>
                                                    </div>
                                                    @endif
                                                    <div class="col-md-4 col-sm-4 col-xs-12 btn-3-col pad-ol-10">
                                                        <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.back') }}</button>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @endif
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    <script>
        clickCancelButton('{{ route("admin/picking") }}')

        exitPopup('{{ route("admin/picking") }}');

        var validationRules = {
            picker_name: {
                required: true
            }
        };

        var validationMessages = {
            picker_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.picker_name')]) }}"
            }
        };

        $('select[name="picker_name"]').change(function(){
            $('#pickingConfirmationForm').submit();
        });

        $('#pickingConfirmationButton').click(function(){
            $('#pickingConfirmationForm').submit();
        });

        $('form#pickingConfirmationForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();                
            }
        });
        
        @if($order_master->picker_name != null)
            $('.select2-container').addClass('pointer-none');
        @endif
    </script>
@endsection
