
@extends('admin::layouts.master')

@section('title',__('menubar.picking'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'transaction'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>Transaksi</li>
                                <li class="active text-bold ">{{ __('menubar.picking') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.picking') }}</h4>
                                            <div class="col-xs-4 no-padding float-right">
                                                <div class="form-group no-margin">
                                                    <label for="activity">{{ __('page.activity') }}</label>
                                                    <select name="picking_activity" id="picking_activity" class="form-control single-select">
                                                        <option value="">{{ __('page.show_all') }}</option>
                                                        <option value="set_picker" {{ Request::get('activity') == 'set_picker' ? 'selected' : '' }}>{{ __('page.set_picker') }}</option>
                                                        <option value="print_shipping_label" {{ Request::get('activity') == 'print_shipping_label' ? 'selected' : '' }}>{{ __('page.print_shipping_label') }}</option>
                                                        <!-- <option value="confirm_picking" {{ Request::get('activity') == 'confirm_picking' ? 'selected' : '' }}>{{ __('page.confirm_picking') }}</option> -->
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- <div class="panel-heading no-heading">
                                        </div> -->
                                        <div class="panel-body">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                                <table class="table display w-100 table-brown" id="picking_list_table">
                                                    <thead class="bg-darkgrey">
                                                        <tr role="row">
                                                            <th>{{ __('field.action') }}</th>
                                                            <th>No.</th>
                                                            <th>{{ __('field.order_code') }}</th>
                                                            <th>{{ __('field.customer_name') }}</th>
                                                            <th>{{ __('field.total_price') }}</th>
                                                            <th>{{ __('field.verified_date') }}</th>
                                                            <th>{{ __('field.purchased_date') }}</th>
                                                            <th>{{ __('field.order_status') }}</th>
                                                            <th>{{ __('field.payment_method') }}</th>
                                                            <th>{{ __('field.picked_by') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot >
                                                        <tr role="row">
                                                            <td colspan="7" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                @php
                                                    $button_name = (Request::get('activity') == 'print_shipping_label' ? 'printMultipleLabelButton' : 'confirmMultipleLabelButton');
                                                    $button_type = (Request::get('activity') == 'print_shipping_label' ? 'button' : 'submit');
                                                @endphp
                                                @if(Request::get('activity') == 'print_shipping_label' || Request::get('activity') == 'confirm_picking')
                                                    <form>
                                                        @csrf
                                                        <input type="hidden" name="order_id">
                                                    </form>
                                                    <button class="btn btn-orange d-none" id="{{ $button_name }}" type="{{ $button_type }}"><i class="fa fa-print"></i> {{ Request::get('activity') == 'print_shipping_label' ?  __('page.print_label') : __('page.confirm_picking') }}</button>
                                                @elseif(Request::get('activity') == 'set_picker')
                                                    <form method="POST" action="{{ route('admin/order/setPicker') }}" id="pickingConfirmationForm" class="d-none">
                                                        @csrf
                                                        <input type="hidden" name="preparation_id">
                                                        <div class="form-group">
                                                            <label for="picker_name">{{ __('field.picked_by') }}</label>
                                                            <select name="picker_name" id="picker_name" class="form-control single-select">
                                                                <option value="">{{ __('page.choose_picker_name') }}</option>
                                                                @for($b = 0; $b < count($pickers); $b++)
                                                                    <option value="{{ $pickers[$b]['user_id'] }}">{{ $pickers[$b]['user_name'] }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </form>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        clickCancelButton('{{ Request::url() }}')

        exitPopup('{{ Request::url() }}');        

        $('select[name="picking_activity"]').change(function(){
            if($(this).val() == 'set_picker'){
                window.location.href = "{{ route('admin/picking') }}"+'?picker=0&activity=set_picker';
            }else if($(this).val() == 'print_shipping_label'){
                window.location.href = "{{ route('admin/picking') }}"+'?picker=0&activity=print_shipping_label';
            }else if($(this).val() == 'confirm_picking'){
                window.location.href = "{{ route('admin/picking') }}"+'?picker=1&activity=confirm_picking';
            }else{
                window.location.href = "{{ route('admin/picking') }}";
            }
            showLoader();
        });
        // $('#shipmentActivityButton').click(function(){
        //     $('form#confirmShipmentForm').submit();
        //     $('#confirmShipmentModalForm').modal('hide');
        //     showLoader();
        // })
        var pickingTable = $('#picking_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX:true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="picking_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            order: [[ 5, "asc" ]],
            ajax: {
                url:'{{ route("datatable/get_verified_order") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.url = "{{ Request::fullUrl() }}";
                    d.picker = "{{ Request::get('picker') }}";
                    d.warehouse_id = "{{ $user['warehouse_id'] }}";
                    d.organization = "{{ $user['organization_type_id'] == 1 ? 'all_agent' : $user['organization_id'] }}";
                    d.activity = "{{ Request::get('activity') }}";
                    d.parent_organization = "{{ $user['organization_type_id'] == 1 ? 'all_agent' : $user['parent_organization_id'] }}";
                }
            },
            columns: [
                {
                    data: 'action',
                    sortable: false,
                    searchable: false
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'total_price',
                    name: 'total_price',
                    className: 'dt-body-right'
                },
                {
                    data: 'preparation_date',
                    name: 'preparation_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }    
                },
                {
                    data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {data: 'order_status_name', name: 'order_status_name'},
                {data: 'payment_method_name', name: 'payment_method_name', visible: false},
                {data: 'picker_name', name: 'picker_name'},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        $('select[name="picker_name"]').change(function(){
            $('#pickingConfirmationForm').submit();
        });

        $('#picking_list_table tbody').on('click','tr td input[name="checked_order[]"]',function(){
            // console.log('test');
            // console.log($('input[name="checked_order[]"]:checked').val());
            var checked_box = [];
            var checked_box_url = "";
            $('#picking_list_table tbody tr input[name="checked_order[]"]:checked').each(function(i){
                checked_box[i] = $(this).val();
            });
            @if(Request::get('activity') == 'print_shipping_label' || Request::get('activity') == 'confirm_picking')
                if(checked_box.length == 0){
                    $('#'+'{{ $button_name }}').addClass('d-none');
                }else{
                    $('#'+'{{ $button_name }}').removeClass('d-none');
                    url = "{{ route('admin/picking/label/multiple') }}";
                    @if(Request::get('activity') == 'print_shipping_label')
                        checked_box_url = url+'?id='+checked_box.toString();
                        $('#'+'{{ $button_name }}').attr('onclick','window.location.href = "'+checked_box_url+'"');
                    @endif
                }
            @elseif(Request::get('activity') == 'set_picker')
                if(checked_box.length == 0){
                    $('input[name="preparation_id"]').val("");
                    $('#pickingConfirmationForm').addClass('d-none');
                }else{
                    $('input[name="preparation_id"]').val(checked_box);
                    $('#pickingConfirmationForm').removeClass('d-none');
                    // url = "{{ route('admin/picking/label/multiple') }}";
                    // checked_box_url = url+'?id='+checked_box.toString();
                    // $('#printMultipleLabelButton').attr('onclick','window.location.href = "'+checked_box_url+'"');
                }
            
            @endif
            console.log(checked_box);
            console.log(checked_box_url);
        });
    </script>
    @endif
@endsection
