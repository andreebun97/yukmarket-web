
@extends('admin::layouts.master')

@section('title',__('menubar.privacy'))

@section('content')
<div class="dashboard-container">
    @include('menubar.admin')
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            @include('sidebar.admin',['sidebar' => 'privacy'])

            <!-- Main content -->
            <div class="content-wrapper  padding-t47">
                <div class="page-header page-header-default">
                    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                        <ul class="breadcrumb">
                            <li><i class="fa fa-folder position-left"></i>Kebijakan Pribadi</li>
                        </ul>
                    </div>
                </div>
                <!-- Content area -->
                <div class="content" >
                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-flat" >
                                <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">Kebijakan Pribadi</h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if($accessed_menu == 0)
                                    @include('prohibited_page')
                                    @else
                                    <form method="POST" id="privacyform" action="{{ route('admin/privacy/update') }}" enctype="multipart/form-data" id="settingsForm">
                                        @csrf
                                        @if(count($privacy) > 0)
                                        @foreach($privacy as $row)
                                        <div class="col-md-12 no-padding">
                                            <div class="panel-body">
                                                <div class="col-md-12 no-padding">
                                                    <div class="form-group">
                                                        <label for="privacy_value">&nbsp;</label>
                                                        <textarea id="myEditor" style="height: 300px;" class="form-control" name="privacy_value">{{ $row->value }}</textarea>
                                                    </div>
                                                </div>
                                            </div>       
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="card-footer bg-transparent ">
                                                <div class="col-md-4 no-padding float-right">
                                                    <div class="col-xs-6 padding-r-10">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="col-md-12 no-padding">
                                            <div class="panel-body" >
                                                <div class="col-md-12 no-padding">
                                                    <div class="form-group">
                                                        <label for="privacy_value">Kebijakan Pribadi</label>
                                                        <textarea id="myEditor" class="form-control" name="privacy_value"></textarea>
                                                    </div>
                                                </div>

                                            </div>                                       

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <div class="card-footer bg-transparent ">
                                                <div class="col-md-4 no-padding float-right">
                                                    <div class="col-xs-6 padding-r-10">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-xs-6 padding-l-10">
                                                        <button type="submit" class="btn btn-orange btn-block ">{{ __('page.submit') }}</button>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        @endif

                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</div>
@include('loader')
@if(Session::get('message_alert') != null)
@include('error_popup')
@endif
@include('assets_link.js_list')
<script>
    var validationRules = {
        privacy_value: {
            required: true
        }
    };
    var validationMessages = {
        privacy_value: {
            required: "{{ __('validation.required',['attribute' => __('validation.attributes.privacy')]) }}",
        }
    }

    $('form#privacyform').validate({
        errorClass: 'invalid-feedback',
        rules: validationRules,
        messages: validationMessages,
        ignore: [],
        submitHandler: function (form) {
            form.submit();
            showLoader();
        }
    });
</script>
<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script>tinymce.init({selector: 'textarea', menubar: false});</script>
@endsection
