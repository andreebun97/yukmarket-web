
@extends('admin::layouts.master')

@section('title',__('menubar.cashback'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li class="active text-bold ">{{ __('menubar.cashback') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.cashback') }}</h4>
                                            
                                        </div>
                                        <div class="panel-heading no-padding-bottom">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item col-md-6 col-lg-3 col-sm-6 col-xs-6 text-center no-padding {{ Request::get('role') == null || Request::get('role') == 'customer' ? 'active' : '' }}">
                                                    <a class="nav-link" href="{{ route('admin/cashback','role=customer') }}">{{ __('role.customer') }}</a>
                                                </li>
                                                <li class="nav-item col-md-6 col-lg-3 col-sm-6 col-xs-6 text-center no-padding {{ Request::get('role') == 'agent' ? 'active' : '' }}">
                                                    <a class="nav-link" href="{{ route('admin/cashback','role=agent') }}">{{ __('role.agent') }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" >
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="cashback_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.user_email') }}</th>
                                                        <th>{{ __('field.total_balance') }}</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var columnArray = [
            {
                data: 'action',
                sortable: false,
                searchable: false
            },
            {
                data: null,
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {data: 'customer_name', name: 'customer_name'},
            {data: 'customer_email', name: 'customer_email'},
            {data: 'total_balance', name: 'total_balance'},
        ];
        
        var categoryTable = $('#cashback_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                lengthMenu: '{{ __("page.showing") }} <select name="cashback_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_cashback") }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.role = "{{ Request::get('role') }}";
                }
            },
            columns: columnArray,
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
    </script>
    @endif
@endsection
