
@extends('admin::layouts.master')

@section('title',__('page.cashback_detail'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-exchange position-left"></i>{{ __('menubar.master') }}</li>
                                <li>{{ __('menubar.cashback') }}</li>
                                <li class="active text-bold ">{{ __('page.cashback_detail') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat">  
                                    <div class="card">
                                    <div class="panel-heading border-bottom-grey">
                                        <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('page.cashback_detail') }}</h4>
                                        <div class="clearfix"></div>
                                    </div>                            
                                        <div class="panel-body">
                                        
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                        <div class="row no-padding">
                                            <div class="col-md-12 trs-detail">
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div id="user_profile " class="info-detail">
                                                        <h2>{{ __('page.customer_info') }}</h2>
                                                        <div class="table-label">
                                                            <table>
                                                                <tr>
                                                                    <td class="width-label"><label>{{ __('field.user_name') }}</label></td>
                                                                    <td>:</td>
                                                                    <td><span class="user_data_information"> {{ $customer->customer_name }}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><label>{{ __('field.user_email') }}</label></td>
                                                                    <td>:</td>
                                                                    <td><span class="user_data_information"> {{ $customer->customer_email }}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><label>{{ __('field.user_phone_number') }}</label></td>
                                                                    <td>:</td>
                                                                    <td><span class=" dd user_data_information"> {{ $customer->customer_phone_number }}</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="separator no-margin"></div>
                                            <div class="clearfix"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                    <div id="user_transaction" class="info-detail">
                                                        <h2>{{ __('menubar.cashback') }}</h2>
                                                        <div id="purchase_transaction">
                                                            <div class="form-group">
                                                                <div id="purchase_transaction_detail" class="res-scroll">
                                                                        @if(count($cashback) == 0)  
                                                                            <span>{{ __('page.no_data') }}</span>
                                                                        @else
                                                                        <table class="table display w-100 table-brown total-transaksi" id="cashback_list_table">
                                                                            <thead class="bg-darkgrey">
                                                                                <tr role="row">
                                                                                    <th>Nilai Saldo</th>
                                                                                    <th>Transaksi</th>
                                                                                    <th>Jenis</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @php $total_amount = 0; @endphp
                                                                                @for($b = 0; $b < count($cashback); $b++)
                                                                                    @php
                                                                                    
                                                                                    if($cashback[$b]['transaction_flag'] == 1){
                                                                                        $total_amount += $cashback[$b]['return_amount'];
                                                                                    }else{
                                                                                        $total_amount -= $cashback[$b]['return_amount'];
                                                                                    }
                                                                                    
                                                                                    @endphp
                                                                                    <tr>
                                                                                        <td class="text-right">{{ ($cashback[$b]['transaction_flag'] == 1 ? "+" : "-"). $currency->convertToCurrency($cashback[$b]['return_amount']) }}</td>
                                                                                        <td>{!! $cashback[$b]['issue_id'] == null ? ($cashback[$b]['payment_id'] == null ? '-' : ('Pengurangan saldo dengan kode pembayaran: '.'<b>'.$cashback[$b]['invoice_no'].'</b>')) : 'Solusi dari pengaduan dengan nomor pesanan '.('<b>'.$cashback[$b]['order_code'].'</b>') !!}</td>
                                                                                        <td>{{ $cashback[$b]['transaction_flag'] == 1 ? 'Penambahan Saldo' : 'Pengurangan Saldo' }}</td>
                                                                                    </tr>
                                                                                @endfor
                                                                            </tbody>                                                                            
                                                                        </table>
                                                                       
                                                                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-7 no-padding float-right">
                                                                            <div class="total-wrap">
                                                                                <div class="col-md-12 no-padding">
                                                                                    <div class="col-md-8 col-sm-8 col-xs-8 no-padding">
                                                                                        <span  class="dp-block color-black">{{ __('field.total') }}</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                                                                                        <span class="float-right dp-block color-black">{{ "Rp. ". $currency->convertToCurrency($total_amount) }}</span>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        
                                                                        <div class="clearfix"></div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
@endsection
