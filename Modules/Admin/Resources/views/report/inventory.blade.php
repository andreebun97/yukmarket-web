
@extends('admin::layouts.master')

@section('title',__('menubar.inventory_report'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'report'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-report"></i></li>
                                <li class="active text-bold ">{{ __('menubar.inventory_report') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.inventory_report') .
                                                (Request::get('transaction_start_date') == 'null' ?
                                                (' (Berdasarkan Tanggal : '.(Request::get('transaction_start_date').' s/d '.Request::get('transaction_end_date')).')') : ' ' .
                                                ($user_wh_org->warehouse_id == null ?
                                                ' Semua Warehouse' :
                                                ($user_wh_org->warehouse_name)) ) }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a class="btn btn-orange float-left" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i>Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="report_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>{{ __('field.product_name') }}</th>
                                                        <th>{{ __('field.balance_opening') }}</th>
                                                        <th>{{ __('field.balance_in') }}</th>
                                                        <th>{{ __('field.balance_out') }}</th>
                                                        <th>{{ __('field.balance_end') }}</th>
                                                    </tr>
                                                </thead>
                                                <tfoot >
                                                    <tr role="row">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <form id="transactionFilterForm">
                    <div class="modal-body">
                        @csrf
                        <div class="col-md-12 no-padding">
                            <div class="form-group margin-b10">
                                <label for="dim_length">Gudang<span class="required-label">*</span></label>
                                <div class="clearfix"></div>
                                @if($user_wh_org->warehouse_id != null)
                                    <input type="hidden" name="inventory_warehouse" class="inventory_warehouse" value="{{$user_wh_org->warehouse_id}}">
                                    <input type="text" name="inventory_warehouse_name" class="form-control inventory_warehouse_name" value="{{$user_wh_org->warehouse_name}}" readonly="readonly" />
                                @else
                                    <select name="inventory_warehouse" class="form-control single-select inventory_warehouse @error('product_category') is-invalid @enderror">
                                        <option value="">-- Pilih Gudang --</option>
                                        @foreach($all_warehouse as $warehouse)
                                        <option value="{{$warehouse->warehouse_id}}"> {{$warehouse->warehouse_name}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="form-group margin-b10">
                                <label for="dim_length">Kategori Produk<span class="required-label">*</span></label>
                                <div class="clearfix"></div>
                                <select name="category" class="form-control single-select category @error('product_category') is-invalid @enderror">
                                    <option value="">-- Pilih Kategori --</option>
                                    @foreach($list_category as $category)
                                    <option value="{{$category->category_id}}"> {{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                        <div class="input-group padding-left-20">
                                            <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                             <input type="text" class="form-control daterange-single" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @endif>
                                             <!-- <input type="date" class="form-control" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @endif> -->
                                        </div>


                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @endif>
                                         <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer border-top-grey">
                        <div class="col-sm-12 margin-t20 no-padding btn-footer">
                            <div class="col-md-6 no-pad-left margin-b10 res-no-pad-sm">
                                <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('order_status') == null && Request::get('payment_method') == null) disabled @endif>{{ __('page.reset') }}</button>
                            </div>
                            <div class="col-md-6 no-padding">
                                <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js"></script>
    <script>
        $('#report_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            "scrollX": true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_report_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            },
            "processing": true,
            "serverSide": true,
            'filter': false,
            dom: '<"bottom"lBfrtip><"clear">',
            buttons: [
                {
                    extend:'print',
                    footer: true
                }
            ],
            'columnDefs': [
                {
                    "targets": 0,
                    "orderable": false
                }
            ],
            "ajax": {
                url: "<?= url('datatable/get_inventory_report') ?>"
            },
            "columns": [
                {data: "no"},
                {data: "product_name"},
                {data: "balance_opening"},
                {data: "balance_in"},
                {data: "balance_out"},
                {data: "balance_end"}
            ]
        });

        $(document).on('submit', '#transactionFilterForm', function (e) {
            e.preventDefault();
                $('#report_table').DataTable().destroy();
                $('#report_table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    'filter': false,
                    dom: '<"bottom"lBfrtip><"clear">',
                    buttons: [
                        {
                            extend:'print',
                            footer: true
                        }
                    ],
                    'columnDefs': [
                        {
                            "targets": 0,
                            "orderable": false
                        },
                    ],
                    "ajax": {
                        url: "<?= url('datatable/get_inventory_report') ?>",
                        type: "get",
                        data: getFormData($(this))
                    },
                    "columns": [
                        {data: "no"},
                        {data: "product_name"},
                        {data: "balance_opening"},
                        {data: "balance_in"},
                        {data: "balance_out"},
                        {data: "balance_end"}
                    ]
                });
                $('#filterModalForm').modal('toggle');
        });

        function getFormData(form, checkbox) {
            var unindexed_array = form.serializeArray();
            console.log(unindexed_array);
            var indexed_array = {};
            $.map(unindexed_array, function (n, i) {
                if (checkbox) {
                    if (n['name'].indexOf("[]") >= 0) {
                        var replace = n['name'].replace("[]", "");
                        indexed_array[replace] = checkbox;
                    } else {
                        indexed_array[n['name']] = n['value'];
                    }
                } else {
                    indexed_array[n['name']] = n['value'];
                }
            });

            return indexed_array;
        }
    </script>
    @endif
@endsection
