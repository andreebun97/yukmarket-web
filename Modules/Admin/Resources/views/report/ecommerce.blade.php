
@extends('admin::layouts.master')

@section('title',__('menubar.ecommerce_report'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'report'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-info position-left"></i>{{ __('menubar.report') }}</li>
                                <li class="active text-bold ">{{ __('menubar.ecommerce_report') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.ecommerce_report') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a class="btn btn-orange float-left" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i>Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="ecommerce_report_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>Tanggal</th>
                                                        <th>Deskripsi</th>
                                                        <th>Nominal</th>
                                                        <!-- <th>Flag</th> -->
                                                        <th>Balance</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Pendapatan Kotor</td>
                                                        <td id="total_gross_income" style="text-align:right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Biaya TopAds</td>
                                                        <td id="total_topads_income" style="text-align:right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Cashback yang diberikan</td>
                                                        <td id="total_cashback" style="text-align:right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Ongkos Kirim</td>
                                                        <td id="total_shipping_fee" style="text-align:right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Asuransi</td>
                                                        <td id="total_insurance" style="text-align:right"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;font-weight:bold">Total Pendapatan Bersih</td>
                                                        <td id="total_bruto" style="text-align:right"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @endif
                                            <button onclick="backTop()" id="topBtn" title="Back to top"><i class="fa fa fa-arrow-circle-up"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <form id="transactionFilterForm">
                        @csrf
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single cdk-overlay-container" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}"  @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single cdk-overlay-container" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}"  @endif>
                                        <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-sm-12 no-padding btn-footer margin-t20">
                        <div class="col-md-6 no-pad-left res-no-pad margin-b10">
                            <button type="button" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('order_status') == null && Request::get('payment_method') == null) disabled @endif>{{ __('page.reset') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="submit" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js"></script>
    <script>
        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/report/ecommerce') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var data = 0;
        var sortingIdx = 1;
        var salesReportTable = $('#ecommerce_report_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            paging:false,
            searching:false,
            scrollX: true,
            "scrollY":        "350px",
            "scrollCollapse": true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                // lengthMenu: '{{ __("page.showing") }} <select name="ecommerce_list_length">'+
                //     '<option value="5">5</option>'+
                //     '<option value="10">10</option>'+
                //     '<option value="20">20</option>'+
                //     '<option value="25">25</option>'+
                //     '<option value="50">50</option>'+
                //     '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            // "paging":         false,
            // dom: '<"bottom"lBfrtip><"clear">',
            // buttons: [
            //     {
            //         extend:'print',
            //         footer: true
            //     }
            // ],
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="ecommerce_report_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '<option value="-1">{{ __("page.all") }}</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50,-1], [5,10, 25, 50,"{{ __('page.all') }}"]],
            processing: true,
            serverSide: true,
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            // searching: false,
            order: [[ sortingIdx, "asc" ]],
            ajax: {
                url:'{{ route("datatable/get_ecommerce_report") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.start_date = "{{ Request::get('transaction_start_date') == null ? '' : Request::get('transaction_start_date') }}";
                    d.end_date = "{{ Request::get('transaction_end_date') == null ? '' : Request::get('transaction_end_date') }}";
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'payment_date',
                    name: 'payment_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    },orderable:false
                },
                {data: 'description', name: 'description',orderable:false},
                {data: 'amount', name: 'amount',orderable:false, className: 'dt-body-right'},
                // {data: 'flag', name: 'flag',orderable:false, visible: false},
                {data: 'balance', name: 'balance',orderable:false, className: 'dt-body-right'}
                
            ],
            // footerCallback: function ( row, data, start, end, display ) {
            //     var api = this.api(), data;
                
            //     // Remove the formatting to get integer data for summation
            //     var intVal = function ( i ) {
            //         console.log(i);
            //         return typeof i === 'string' ?
            //             i.replace(/[\Rp.]/g, '')*1 :
            //             typeof i === 'number' ?
            //                 i : 0;
            //     };

            //     // Total over all pages
            //     // var total_gross = data.filter(item => item.flag == 3);
            //     // console.log(total_gross);
            //     var dataa = total = api
            //         .column( [3,4] )
            //         .data();
            //     console.log(dataa);
            //     total = api
            //         .column( 3 )
            //         .data()
            //         .reduce( function (a, b) {
            //             return intVal(a) + intVal(b);
            //         }, 0 );
            //     console.log(total);

            //     // Total over this page
            //     // pageTotal = api
            //     //     .column( 3, { page: 'current'} )
            //     //     .data()
            //     //     .reduce( function (a, b) {
            //     //         return intVal(a) + intVal(b);
            //     //     }, 0 );

            //     // Update footer
            //     // console.log(api.column( 4 ).footer());
            //     $('#total_gross_income').html(
            //         convertToCurrency(total)+',-'
            //     );
            // },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        $.ajax({
            url: "{{ route('admin/report/get_total_ecommerce_report') }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                start_date: "{{ Request::get('transaction_start_date') }}",
                end_date: "{{ Request::get('transaction_end_date') }}"
            },
            success: function(resultData){
                console.log(resultData);
                $('td#total_gross_income').html(convertToCurrency(resultData['total_gross_income']));
                $('td#total_cashback').html("-"+convertToCurrency(resultData['total_cashback']));
                $('td#total_shipping_fee').html("-"+convertToCurrency(resultData['total_shipping_fee']));
                $('td#total_insurance').html(convertToCurrency(resultData['total_insurance']));
                $('td#total_topads_income').html("-"+convertToCurrency(resultData['total_top_ads_cost']));
                $('td#total_bruto').html(convertToCurrency(resultData['total_bruto']));
            }
        });
    </script>
    @endif
@endsection
