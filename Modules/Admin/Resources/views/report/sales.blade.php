
@extends('admin::layouts.master')

@section('title',__('menubar.sales_report'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'report'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-info position-left"></i>{{ __('menubar.report') }}</li>
                                <li class="active text-bold ">{{ __('menubar.sales_report') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.sales_report') . (Request::get('based_on') == 'shipping_date' ? (' (Berdasarkan Tanggal Kirim: '.(Request::get('transaction_start_date').' s/d '.Request::get('transaction_end_date')).')') : (Request::get('based_on') == null ? '' : (' (Berdasarkan Tanggal Penjualan: '.(Request::get('transaction_start_date').' s/d '.Request::get('transaction_end_date')).')')) ) }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a class="btn btn-orange float-left" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i>Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="sales_report_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>No.</th>
                                                        <th>Kode Penjualan</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.agent_name') }}</th>
                                                        <th>{{ __('field.sales_date') }}</th>
                                                        <th>{{ __('field.shipping_date') }}</th>
                                                        <th>{{ __('field.total_price') }}</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr role="row">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td class="text-right" id="total_reckon"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <form id="transactionFilterForm">
                        @csrf
                        <div class="col-md-12 no-padding">
                            @if($user['organization_type_id'] == 1)
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="agent_name">{{ __('field.agent_name') }}</label>
                                    <select name="organization" class="form-control single-select">
                                        <option value="">{{ __('page.choose_agent') }}</option>
                                        <option value="all_agent" @if(Request::get('organization') != null && Request::get('organization') == 'all_agent') selected @endif>{{ __('page.all_agent') }}</option>
                                        @for($b = 0; $b < count($organization); $b++)
                                            <option value="{{ $organization[$b]['organization_id'] }}" @if(Request::get('organization') != null && Request::get('organization') == $organization[$b]['organization_id']) selected @endif>{{ $organization[$b]['organization_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="warehouse_name">{{ __('field.warehouse_name') }}</label>
                                    <select name="warehouse" class="form-control single-select">
                                        <option value="">{{ __('page.choose_warehouse') }}</option>
                                        <option value="all_warehouse" @if(Request::get('warehouse') != null && Request::get('warehouse') == 'all_warehouse') selected @endif>{{ __('page.all_warehouse') }}</option>
                                        @for($b = 0; $b < count($warehouse); $b++)
                                            <option value="{{ $warehouse[$b]['warehouse_id'] }}" @if(Request::get('warehouse') != null && Request::get('warehouse') == $warehouse[$b]['warehouse_id']) selected @endif>{{ $warehouse[$b]['warehouse_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="based_on">{{ __('field.based_on') }}</label>
                                    <select name="based_on" class="form-control single-select">
                                        <option value="">{{ __('page.choose_based_on') }}</option>
                                        <option value="shipping_date" @if(Request::get('based_on') != null && Request::get('based_on') == 'shipping_date') selected @endif>{{ __('field.shipping_date') }}</option>
                                        <option value="purchased_date" @if(Request::get('based_on') != null && Request::get('based_on') == 'purchased_date') selected @endif>{{ __('field.purchased_date') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single cdk-overlay-container" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}"  @endif>
                                         <!-- <input type="date" class="form-control " id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single cdk-overlay-container" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}"  @endif>
                                        <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-sm-12 no-padding btn-footer margin-t20">
                        <div class="col-md-6 no-pad-left res-no-pad margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('order_status') == null && Request::get('payment_method') == null) disabled @endif>{{ __('page.reset') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js"></script>
    <script>
        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/report/sales') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var data = 0;
        var sortingIdx = 1;
        var salesReportTable = $('#sales_report_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            paging:false,
            scrollX: true,
            dom: '<"bottom"lBfrtip><"clear">',
            buttons: [
                {
                    extend:'print',
                    footer: true
                }
            ],
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="sales_report_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '<option value="-1">{{ __("page.all") }}</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50,-1], [5,10, 25, 50,"{{ __('page.all') }}"]],
            processing: true,
            serverSide: true,
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            order: [[ sortingIdx, "asc" ]],
            ajax: {
                url:'{{ route("datatable/get_payment") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.start_date = "{{ Request::get('transaction_start_date') == null ? date('Y-m-d') : Request::get('transaction_start_date') }}";
                    d.end_date = "{{ Request::get('transaction_end_date') == null ? date('Y-m-d') : Request::get('transaction_end_date') }}";
                    d.based_on = "{{ Request::get('based_on') }}";
                    d.warehouse_id = "{{ $user['organization_type_id'] == 1 ? Request::get('warehouse') : $user['warehouse_id'] }}";
                    d.order_status = 10;
                    d.organization = "{{ $user['organization_type_id'] == 1 ? Request::get('organization') : $user['organization_id'] }}";
                    d.parent_organization = "{{ $user['organization_type_id'] == 1 ? Request::get('organization') : $user['parent_organization_id'] }}";
                    d.report = 1;
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'order_code', name: 'order_code'},
                {data: 'customer_name', name: 'customer_name'},
                {data: 'organization_name', name: 'organization_name'},
                {
                    data: 'purchased_date',
                    name: 'purchased_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {
                    data: 'shipping_date',
                    name: 'shipping_date',
                    render: function(data, type, row, meta){
                        var full_date = data == null ? null : data.split(' ');

                        var time = "";
                        var date = "";
                        if(full_date != null){
                            time = full_date[1];
                            date = full_date[0].split('-').reverse().join('-');
                        }

                        return (full_date == null ? "" : date+' '+time);
                    }
                },
                {
                    data: 'total_price',
                    name: 'total_price',
                    className: 'dt-body-right'
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );
            },
            // fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
            //     return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            // }
        });

        // $('#reckonPaymentList').prop('disabled',true);
        // var selected = [];
        // var selectedTotalPayment = [];
        // $(document).on('click', '#sales_report_list_table tbody tr', function () {
        //     var index = $('#sales_report_list_table tbody tr').index(this);
        //     var data = salesReportTable.data();
        //     var payment_id = data[index]['payment_id'];
        //     var total_payment = data[index]['unformatted_grand_total'];
        //     console.log(payment_id);

        //     var id = payment_id;
        //     var index = $.inArray(id, selected);
        //     var paymentIndex = $.inArray(total_payment, selectedTotalPayment);

        //     if ( index === -1 ) {
        //         selected.push( id );
        //         selectedTotalPayment.push(total_payment);
        //     } else {
        //         selected.splice( index, 1 );
        //         selectedTotalPayment.splice(paymentIndex, 1);
        //     }

        //     $('input[name="payment_list"]').val(selected);
        //     if(selected.length > 0){
        //         $('#reckonPaymentList').prop('disabled',false);
        //     }else{
        //         $('#reckonPaymentList').prop('disabled',true);
        //     }

        //     var total = 0;
        //     for (let b = 0; b < selectedTotalPayment.length; b++) {
        //         total += selectedTotalPayment[b];
        //     }
        //     $('tfoot:last-child').find('td:last-child').html(convertToCurrency(total)+',-');
        //     console.log(selectedTotalPayment);
        //     // $(this).toggleClass('selected');
        //     console.log(selected);
        // });

        // function getSelectedCheckbox(){
        //     var tblData = salesReportTable.rows('tr.selected').data();
        //     // console.log(paymentTable.rows('tr.selected').index(tblData));
        //     var payment_id_list = salesReportTable.rows('tr.selected')
        //     .nodes()
        //     .to$()
        //     .find('td:eq(1)')
        //     .val();
        //     var tmpData = [];
        //     $.each(tblData, function(i, val) {
        //         tmpData.push(tblData[i][1]);
        //     });
        //     console.log(payment_id_list);
        //     console.log('length: '+tmpData.length);
        //     return tmpData;
        // }

        $('input[name="transaction_start_date"]').change(function(){
            var chosenDateTemp = new Date($(this).val());
            var chosenDate = chosenDateTemp.getFullYear()+'-'+((chosenDateTemp.getMonth()+1) < 10 ? ("0" + (chosenDateTemp.getMonth()+1)) : (chosenDateTemp.getMonth()+1))+'-'+(chosenDateTemp.getDate() < 10 ? ("0"+chosenDateTemp.getDate()) : chosenDateTemp.getDate());
            // var chosenTime = (chosenDateTemp.getHours() < 10 ? ("0"+chosenDateTemp.getHours()) : chosenDateTemp.getHours()) + ":" + (chosenDateTemp.getMinutes() < 10 ? ("0"+chosenDateTemp.getMinutes()) : chosenDateTemp.getMinutes()) + ":" + (chosenDateTemp.getSeconds() < 10 ? ("0" + chosenDateTemp.getSeconds()) : chosenDateTemp.getSeconds());
            var fullChosenDate = chosenDate
            // $(this).attr('min',fullChosenDateTime);
            $('input[name="transaction_end_date"]').attr('min',fullChosenDate);
            $(this).val(fullChosenDate);
            // $('input[name="transaction_end_date"]').val(fullChosenDate);
        });

    // function getSelectedIndex(){
    //     paymentTable.rows('tr.selected')
    //     .nodes()
    //     .to$()
    //     .find('td:eq(1) .buruh_info_detail')
    //     .removeClass( 'bg-light' );

    //     paymentTable.rows('tr:not(.selected)')
    //     .nodes()
    //     .to$()
    //     .find('td:eq(1) .buruh_info_detail')
    //     .addClass( 'bg-light' );
    //     // return indexes;
    // }
        // var idx = 0;
        // setInterval( function () {
        //     idx += 1;
        //     console.log(idx);
        //     if(idx > 0 && idx % 180 == 0){
        //         paymentTable.ajax.reload(null, false);
        //     }
        // }, 1000);
    </script>
    @endif
@endsection
