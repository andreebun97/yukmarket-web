
@extends('admin::layouts.master')

@section('title',__('menubar.payment_report'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'report'])

                <!-- Main content -->
                <div class="content-wrapper  padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-info position-left"></i>{{ __('menubar.report') }}</li>
                                <li class="active text-bold ">{{ __('menubar.payment_report') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.payment_report') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                                <a class="btn btn-orange float-left" data-toggle="modal" data-target="#filterModalForm"><i class="fa fa-search"></i>Pencarian Detail</a>
                                            </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="panel-body ">
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            <table class="table display w-100 table-brown" id="payment_report_list_table">
                                                <thead class="bg-darkgrey">

                                                    <tr role="row">
                                                        <th><div class="aksi-check">
                                                            <div class="label-check">
                                                              <div class="checkbox">
                                                                <input type="checkbox" name="checkGrp1" id="check1_Opt1">
                                                                <label for="check1_Opt1" id="checkAll"></label>
                                                              </div>
                                                            </div>
                                                            </div>
                                                        </th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.payment_date') }}</th>
                                                        <th>{{ __('field.payment_method') }}</th>
                                                        <th>{{ __('field.invoice_num') }}</th>
                                                        <th>{{ __('field.customer_name') }}</th>
                                                        <th>{{ __('field.total_price') }}</th>
                                                        <th>{{ __('field.invoice_status') }}</th>


                                                    </tr>
                                                </thead>
                                                <tfoot >
                                                    <tr role="row">
                                                        <!-- <td colspan="5" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td class="text-right"></td> -->
                                                        <td colspan="5" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td class="text-right" id="total_reckon"></td>
                                                    </tr>
                                                    <!-- <tr role="row">
                                                        <td colspan="7" class="text-right">{{ __('field.total_transaction').' (Rp.)' }}</td>
                                                        <td></td>
                                                    </tr> -->
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="col-md-12 no-padding" >
                                            <div class="card-footer bg-transparent " >
                                                <div class="col-md-3 no-padding float-right" >
                                                    <form method="POST" action="{{ route('admin/report/reckon') }}">
                                                        @csrf
                                                        <input type="hidden" name="payment_list">
                                                        <input type="submit" id="reckonPaymentList" class="btn btn-orange btn-block" value="{{ __('page.submit') }}">
                                                    </form>
                                                    @endif
                                                </div>
                                                <div class="clearfix" ></div>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="filterModalForm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="filterModalFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header border-bottom-grey padding-bottom-20">
                    <h5 class="modal-title" id="filterModalFormLabel">{{ __('page.filter') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <!-- Modal Content -->
                <div class="modal-body">
                    <form id="transactionFilterForm">
                        @csrf
                        <div class="col-md-12 no-padding">
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_start_date">{{ __('field.transaction_start_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                        <input type="text" class="form-control daterange-single" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else  value="{{ date('Y-m-d') }}" @endif>
                                        <!-- <input type="date" class="form-control" id="transaction_start_date" name="transaction_start_date" @if(Request::get('transaction_start_date') != null) value="{{ Request::get('transaction_start_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="transaction_end_date">{{ __('field.transaction_end_date') }}</label>
                                    <div class="input-group padding-left-20">
                                        <div class="input-group-addon-custom"><i class="icon-calendar22"></i></div>
                                         <input type="text" class="form-control daterange-single" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else  value="{{ date('Y-m-d') }}"  @endif>
                                            <!-- <input type="date" class="form-control" id="transaction_end_date" name="transaction_end_date" aria-describedby="transaction_end_date"  @if(Request::get('transaction_end_date') != null) value="{{ Request::get('transaction_end_date') }}" @else value="{{ date('Y-m-d') }}" @endif> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group margin-b10">
                                    <label for="payment_method">{{ __('field.payment_method') }}</label>
                                    <select name="payment_method" id="payment_method" class="form-control single-select" placeholder="{{ __('page.choose_payment_method') }}">
                                        <option value="">{{ __('page.choose_payment_method') }}</option>
                                        @for($b = 0; $b < count($payment_method); $b++)
                                            <option value="{{ $payment_method[$b]['payment_method_id'] }}" @if(Request::get('payment_method') != null) @if($payment_method[$b]['payment_method_id'] == Request::get('payment_method')) selected @endif @endif>{{ $payment_method[$b]['payment_method_name'] }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                        </div>


                </div>
                <!-- Modal Footer -->
                <div class="modal-footer border-top-grey" >
                    <div class="col-sm-12 no-padding btn-footer margin-t20">
                        <div class="col-md-6 no-pad-left res-no-pad-sm margin-b10">
                            <button type="button" value="-1" id="resetFilterButton" name="activity_button" class="btn btn-gray btn-block" @if(Request::get('transaction_start_date') == null && Request::get('transaction_end_date') == null && Request::get('order_status') == null && Request::get('payment_method') == null) disabled @endif>{{ __('page.reset') }}</button>
                        </div>
                        <div class="col-md-6 no-padding">
                            <button type="submit" value="1" name="activity_button" class="btn btn-orange btn-block">{{ __('page.search') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @include('loader')
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css"> -->
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script>
        var recordsTotal;
        $('#resetFilterButton').click(function(){
            window.location.href = "{{ route('admin/report/payment') }}";
            showLoader();
        });
        $('form#transactionFilterForm').submit(function(){
            showLoader();
        });
        var paymentReportTable = $('#payment_report_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                lengthMenu: '{{ __("page.showing") }} <select name="payment_report_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}',
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                }
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            paging: false,
            searching: false,
            order: [[ 1, "desc" ]],
            ajax: {
                url:'{{ route("datatable/get_payment_report") }}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function(d){
                    console.log(d);
                    d.start_date = "{{ Request::get('transaction_start_date') == null ? date('Y-m-d') : Request::get('transaction_start_date') }}";
                    d.end_date = "{{ Request::get('transaction_end_date') == null ? date('Y-m-d') : Request::get('transaction_end_date') }}";
                    // d.order_status = "{{ Request::get('order_status') }}";
                    d.payment_method = "{{ Request::get('payment_method') }}";
                }
            },
            columns: [
                {
                    data: null,
                    orderable: false,
                    targets: [0],
                    className: 'select-checkbox',
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'payment_date',
                    name: 'payment_date',
                    render: function(data, type, row, meta){
                        var full_date = data.split(' ');

                        var date = full_date[0].split('-').reverse().join('-');
                        var time = full_date[1];

                        return date+' '+time;
                    }
                },
                {data: 'payment_method_name', name: 'payment_method_name'},
                {data: 'invoice_num', name: 'invoice_num'},
                {data: 'customer_name', name: 'customer_name'},
                {
                    data: 'grand_total',
                    name: 'grand_total',
                    className: 'dt-body-right'
                },
                {data: 'invoice_status_name', name: 'invoice_status_name'},
            ],
            select: {
                style:    'multi',
                selector: 'td:last-child'
            },
            'fnCreatedRow': function(nRow, aData, iDataIndex) {
                $(nRow).attr('data-id', aData.DT_RowId); // or whatever you choose to set as the id
                $(nRow).attr('id', 'id_' + aData.DT_RowId); // or whatever you choose to set as the id
            },
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    console.log(i);
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                    convertToCurrency(total)+',-'
                );

                $( api.column( 7 ).footer() ).html(
                    convertToCurrency(0)+',-'
                );
            },
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });

        $('#payment_report_list_table tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('selected');
        } );

        $('#reckonPaymentList').prop('disabled',true);
        var selected = [];
        var selectedTotalPayment = [];
        var all = 0;
        var current_total = 0;
        paymentReportTable.on('draw', function(){
            // console.log(total);
            // $('tfoot:last-child').find('td:last-child').html(convertToCurrency(total)+',-');
            paymentReportTable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                if(selected.indexOf(this.data().payment_id) >= 0){
                    // var node=this.node();
                    // $(node).toggleClass('selected');
                    this.select ();
                }

                // console.log(this.data().payment_id);
            } );
            console.log(current_total);
            $('tfoot:last-child').find('td:last-child').html(convertToCurrency(current_total)+',-');
            // for (let b = 0; b < selectedTotalPayment.length; b++) {
            //     total += selectedTotalPayment[b];
            // }

        });

        $(document).on('click', '#payment_report_list_table tbody tr', function () {
            var index = $('#payment_report_list_table tbody tr').index(this);
            var data = paymentReportTable.data();
            var payment_id = data[index]['payment_id'];
            var total_payment = data[index]['unformatted_grand_total'];
            console.log(payment_id);

            var id = payment_id;
            var index = $.inArray(id, selected);
            var paymentIndex = $.inArray(total_payment, selectedTotalPayment);

            if ( index === -1 ) {

                selected.push( id );
                selectedTotalPayment.push(total_payment);
            } else {
                selected.splice( index, 1 );
                selectedTotalPayment.splice(paymentIndex, 1);
            }

            $('input[name="payment_list"]').val(selected);
            if(selected.length > 0){
                $('#reckonPaymentList').prop('disabled',false);
            }else{
                $('#reckonPaymentList').prop('disabled',true);
            }

            var total = 0;
            for (let b = 0; b < selectedTotalPayment.length; b++) {
                total += selectedTotalPayment[b];
                current_total = total;
            }
            $('tfoot:last-child').find('td:last-child').html(convertToCurrency(total)+',-');
            if(selectedTotalPayment.length == paymentReportTable.rows('tr').data().length){
                $("#check1_Opt1").attr('checked',false);
            }
            console.log(selectedTotalPayment);
            // $(this).toggleClass('selected');
            console.log(selected);

        });

        function getSelectedCheckbox(){
            var tblData = paymentReportTable.rows('tr.selected').data();
            // console.log(paymentTable.rows('tr.selected').index(tblData));
            var payment_id_list = paymentReportTable.rows('tr.selected')
            .nodes()
            .to$()
            .find('td:eq(1)')
            .val();
            var tmpData = [];
            $.each(tblData, function(i, val) {
                tmpData.push(tblData[i][1]);
            });
            console.log(payment_id_list);
            console.log('length: '+tmpData.length);
            return tmpData;
        }

         $("#checkAll").click(function () {
            $('.select-checkbox').trigger('click');
         });


    // function getSelectedIndex(){
    //     paymentTable.rows('tr.selected')
    //     .nodes()
    //     .to$()
    //     .find('td:eq(1) .buruh_info_detail')
    //     .removeClass( 'bg-light' );

    //     paymentTable.rows('tr:not(.selected)')
    //     .nodes()
    //     .to$()
    //     .find('td:eq(1) .buruh_info_detail')
    //     .addClass( 'bg-light' );
    //     // return indexes;
    // }
        // var idx = 0;
        // setInterval( function () {
        //     idx += 1;
        //     console.log(idx);
        //     if(idx > 0 && idx % 180 == 0){
        //         paymentTable.ajax.reload(null, false);
        //     }
        // }, 1000);
    </script>
    @endif
@endsection
