@extends('admin::layouts.master')

@section('title',(Request::get('id') == null ? __('page.add_banner') : __('page.edit_banner')))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                @include('sidebar.admin',['sidebar' => 'master'])
                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li >{{ __('menubar.banner') }}</li>
                                <li class="active text-bold">{{ Request::get('id') == null ? __('page.add_banner') : __('page.edit_banner') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content" >
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ Request::get('id') == null ? __('page.add_banner') : __('page.edit_banner') }}</h4>
                                        
                                            <div class="clearfix"></div>
                                        </div>
                                        @if($accessed_menu == 0)
                                            @include('prohibited_page')
                                        @else
                                                    <form method="POST" action="{{ route('admin/banner/form') }}" id="bannerForm" enctype="multipart/form-data" class="form-style">
                                                        <div class="col-md-12 no-padding">
                                                            <div class="panel-body">
                                                                <div class="col-md-4 small-img padding-r-10">
                                                                    <div id="showing_product_image">
                                                                        <img src="{{ Request::get('id') == null ? asset('img/default_product.jpg') : '/'.$banner_by_id['banner_image'] }}" alt="" onError="this.onerror=null;this.src='{{ asset('img/default_product.jpg') }}';">
                                                                    </div>
                                                                </div>
                                                                @csrf
                                                                @if(Request::get('id') !== null)
                                                                    <input type="hidden" name="banner_id" value="{{ $banner_by_id['banner_id'] }}">
                                                                    <input type="hidden" name="banner_image_temp" value="{{ $banner_by_id['banner_image'] }}">
                                                                @endif
                                                                <div class="col-md-4 res-left-form res-no-pad-left">
                                                                <div class="info">
                                                                    <i class="fa fa-info-circle"></i>
                                                                    {{ __('notification.maximum_filesize_image',['value' => '2MB']) }}
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="banner_name">{{ __('field.banner_name') }} <span class="required-label">*</span></label>
                                                                    <input type="text" name="banner_name" class="form-control @error('banner_name') is-invalid @enderror" value="{{ Request::get('id') == null ? old('banner_name') : $banner_by_id['banner_name'] }}">
                                                                    @error('banner_name')
                                                                        <span class="invalid-feedback">{{ $errors->first('banner_name') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="banner_description">{{ __('field.banner_description') }} <span class="required-label">*</span></label>
                                                                    <textarea name="banner_description" class="form-control @error('banner_description') is-invalid @enderror" style="height:185px">{{ Request::get('id') == null ? old('banner_description') : $banner_by_id['banner_desc'] }}</textarea>
                                                                    @error('banner_description')
                                                                        <span class="invalid-feedback">{{ $errors->first('banner_description') }}</span>
                                                                    @enderror
                                                                </div>
                                                                <div class="form-group">
                                                                        <label for="banner_image">{{ __('field.banner_image') }}</label>
                                                                        <div class="clearfix"></div>
                                                                        <div class="input-img-name">
                                                                            
                                                                            <span id="image_name">{{ __('page.no_data') }}</span>
                                                                            <button class="btn btn-green-upload no-margin" id="banner_image_button">{{ __('page.upload') }}</button>
                                                                        </div>
                                                                        <input type="file" name="banner_image">
                                                                        @error('banner_image')
                                                                            <span class="invalid-feedback">{{ $errors->first('banner_image') }}</span>
                                                                        @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 no-padding">
                                                            <div class="card-footer bg-transparent ">
                                                                <div class="col-md-4 no-padding float-right">
                                                                    <div class="col-xs-6 padding-r-10">
                                                                    <button type="button" class="btn btn-gray btn-block discard_changes_button">{{ __('page.cancel') }}</button>
                                                                    </div>
                                                                    <div class="col-xs-6 padding-l-10">
                                                                    <button type="submit" class="btn btn-orange btn-block">{{ __('page.submit') }}</button>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>
    @include('loader')
    @include('cancel_popup')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, "Letters only please");

        jQuery.validator.addMethod("filesize_max", function(value, element, param) {
            var isOptional = this.optional(element),
                file;
            
            if(isOptional) {
                return isOptional;
            }
            
            if ($(element).attr("type") === "file") {
                
                if (element.files && element.files.length) {
                    
                    file = element.files[0];            
                    return ( file.size && file.size <= param ); 
                }
            }
            return false;
        }, "File size is too large.");

        $('input[name="banner_name"], textarea[name="banner_description"]').change(function(){
            $(this).val($.trim($(this).val()));
        });
        
        @if(Request::get('id') == null)
        var validationRules = {
            banner_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            banner_description: {
                required: true
                // minlength: 10
            },
            banner_image: {
                required: true,
                extension: "png|pneg|svg|jpg|jpeg",
                maxsize: 2000000
            }
        };
        var validationMessages = {
            banner_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.banner_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.banner_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.banner_name')]) }}"
            },
            banner_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.banner_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.banner_description'), 'min' => 10]) }}"
            },
            banner_image: {
                required: "{{ __('validation.custom.banner_image.required',['attribute' => __('validation.attributes.banner_image')]) }}",
                extension: "{{ __('validation.mimes',['attribute' => __('validation.attributes.category_image'), 'values' => 'png,pneg,svg,jpg,jpeg']) }}",
                maxsize: "{{ __('notification.maximum_filesize_image',['value' => '2MB']) }}"
            }
        };
        @else
        var validationRules = {
            banner_name: {
                required: true,
                // minlength: 5,
                alpha: true
            },
            banner_description: {
                required: true
                // minlength: 10
            }
        };
        var validationMessages = {
            banner_name: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.banner_name')]) }}",
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.banner_name'), 'min' => 5]) }}",
                alpha: "{{ __('validation.alpha_space',['attribute' => __('validation.attributes.banner_name')]) }}"
            },
            banner_description: {
                required: "{{ __('validation.required',['attribute' => __('validation.attributes.banner_description')]) }}"
                // minlength: "{{ __('validation.min.string',['attribute' => __('validation.attributes.banner_description'), 'min' => 10]) }}"
            }
        };
        @endif

        $('form#bannerForm').validate({
            errorClass: 'invalid-feedback',
            rules: validationRules,
            messages: validationMessages,
            ignore: [],
            submitHandler: function(form) {
                form.submit();
                showLoader();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showing_product_image img').attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#banner_image_button').click(function(e){
            e.preventDefault();
            $('input[name="banner_image"]').click();
        });

        $('input[name="banner_image"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#image_name').html(fileName);
            readURL(this);
            console.log('The file "' + fileName +  '" has been selected.');
        });

        clickCancelButton('{{ route("admin/banner") }}')

        exitPopup('{{ route("admin/banner") }}');
    </script>
    @endif
@endsection
