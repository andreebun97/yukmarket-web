
@extends('admin::layouts.master')

@section('title',__('menubar.banner'))

@section('content')
    <div class="dashboard-container">
        @include('menubar.admin')
        <!-- Page container -->
	    <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                @include('sidebar.admin',['sidebar' => 'master'])

                <!-- Main content -->
                <div class="content-wrapper padding-t47">
                    <div class="page-header page-header-default">
                        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                            <ul class="breadcrumb">
                                <li><i class="fa fa-folder position-left"></i>{{ __('menubar.master') }}</li>
                                <li class="active text-bold">{{ __('menubar.banner') }}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Content area -->
                    <div class="content">
                        <!-- Main charts -->
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="panel panel-flat" >
                                    <div class="card">
                                        <div class="panel-heading border-bottom-grey">
                                            <h4 class="panel-title text-blue-700 text-bold float-left">{{ __('menubar.banner') }}</h4>
                                            @if($accessed_menu == 1)
                                            <div class="float-right">
                                            <a href="{{ route('admin/banner/create') }}" class="btn btn-orange"><i class="fa fa-plus"></i> {{ __('page.add_banner') }}</a>
                                            @endif
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        
                                        <div class="panel-body" >
                                            @if($accessed_menu == 0)
                                                @include('prohibited_page')
                                            @else
                                            
                                            <div class="clearfix"></div>
                                            <div class="info">
                                                <i class="fa fa-info-circle"></i>
                                                {{ __('notification.click_banner_image') }}
                                            </div>
                                            <div class="separator no-margin"></div>
                                            <div class="margin-tb-10"></div>
                                            <div class="clearfix"></div>
                                            <table class="table display w-100 table-brown" id="banner_list_table">
                                                <thead class="bg-darkgrey">
                                                    <tr role="row">
                                                        <th>{{ __('field.action') }}</th>
                                                        <th>No.</th>
                                                        <th>{{ __('field.banner_name') }}</th>
                                                        <th>{{ __('field.banner_description') }}</th>
                                                        <th>{{ __('field.banner_image') }}</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            @endif
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
            <!-- /page container -->
    </div>

    <!-- Modal -->
    @if($accessed_menu == 1)
    <div class="modal fade" id="bannerImageModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="bannerImageModalTitle" aria-hidden="true">
        <div class="modal-dialog width-auto">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="bannerImageModalTitle">{{ __('field.banner_image') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('loader')
    @if(Session::get('message_alert') != null)
        @include('error_popup')
    @endif
    @include('assets_link.js_list')
    @if($accessed_menu == 1)
    <script>
        var bannerTable = $('#banner_list_table').on('processing.dt', function ( e, settings, processing ) {
            $('.dataTables_processing').remove();
            if(processing){
                showLoader();
            }else{
                hideLoader();
            }
        }).DataTable({
            scrollX: true,
            language: {
                paginate: {
                    previous: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                lengthMenu: '{{ __("page.showing") }} <select name="banner_list_length">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="20">20</option>'+
                    '<option value="25">25</option>'+
                    '<option value="50">50</option>'+
                    '</select> data',
                emptyTable: '{{ __("page.no_data") }}'
            },
            oLanguage: {
                sSearch: "{{ __('page.search') }}:"
            },
            lengthMenu: [[5,10, 25, 50], [5,10, 25, 50]],
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ route("datatable/get_banner") }}'
            },
            columns: [
                {
                    data: 'action',
                    sortable: false
                },
                {
                    data: null,
                    sortable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'banner_name', name: 'banner_name'},
                {data: 'banner_description', name: 'banner_description'},
                {
                    data: null,
                    name: null,
                    sortable:false,
                    render: function (data, type, row, meta) {
                        return "<img class='banner_image' data-toggle='modal' data-target='#bannerImageModal' src='{{ asset('img/default_product.jpg') }}'>";
                    }
                },
            ],
            fnInfoCallback: function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                return '{{ __("page.showing") }} ' + (iTotal == 0 ? 0 : (iStart + ' {{ __("page.to") }} ' + iEnd + ' {{ __("page.of") }} ' + iTotal)) + ' {{ __("page.entries") }}';
            }
        });
        
        bannerTable.on('click','img.banner_image',function(){
            console.log('lalala');
            var index = $('img.banner_image').index(this);
            var data = bannerTable.data();
            var banner_img = data[index]['banner_image'];
            $('#bannerImageModal .modal-body').html('<img src="'+banner_img+'">');
            $('#bannerImageModal .modal-body img').attr('onerror','this.src="{{ asset("img/default_product.jpg") }}"');
        });
    </script>
    @endif
@endsection
