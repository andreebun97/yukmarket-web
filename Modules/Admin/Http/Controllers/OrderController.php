<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App;
use Intervention\Image\Facades\Image;
use Lang;
use App\Preparation;
use App\Order;
use App\OrderDetail;
use App\OrderHistory;
use App\Payment;
use App\GlobalSettings;
use App\Helpers\Currency;
use DB;
use Config;
use Mail;
use Exception;
use Illuminate\Support\Facades\Http;
use App\Notification;
use App\Inventory;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $this->currency = $currency;
    }

    public function index()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],1)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::order', $data);
    }

    public function quality_control(Request $request){
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],21)[0]['access_status'];
        $order = array();
        if($request->get('order_code') != null){
            $order = Order::join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_type','10_order.max_price AS max_provided_voucher_value','10_order.admin_fee','10_order.admin_fee_percentage','10_order.shipment_price','00_customer.customer_name','00_customer.customer_email', '10_order.payment_method_id','00_payment_method.payment_method_name', '00_customer.customer_phone_number','10_order.destination_address','10_order.order_status_id','00_order_status.order_status_name','10_order.order_date AS purchased_date','00_address.address_name','00_address.contact_person','00_address.phone_number','00_address.address_detail','00_address.address_info','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity * 10_order_detail.price) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity * 10_order_detail.price) END) FROM 10_order_detail WHERE 10_order_detail.order_id = 10_order.order_id) AS total_price'), DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity * 10_order_detail.price) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity * 10_order_detail.price) END) FROM 10_order_detail JOIN 10_order ON 10_order_detail.order_id = 10_order.order_id) AS grand_total'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->distinct()->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[2])->where('10_order.order_status_id','6')->where('10_order.order_code',$request->get('order_code'))->orderBy('10_preparation.updated_date','ASC')->first();
        }
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order'] = $order;
        return view('admin::quality_control/index', $data);
    }

    public function getOrderHistory(Request $request){
        $data = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->where('order_id', $request->input('order_id'))->get();

        return response()->json(array('data' => $data),200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    // public function takeOrder(Request $request)
    // {
    //     $preparation = Preparation::insertGetId([
    //         'preparation_status_id' => 1,
    //         'order_id' => $request->input('order_id'),
    //         'updated_by' => Session::get('users')['id']
    //     ]);
        
    //     return redirect('admin/picking');
    // }
    
    public function setPicker(Request $request)
    {
        // print_r($request->all());
        // exit;
        $preparation = Preparation::whereIn('preparation_id', explode(',',$request->input('preparation_id')))->update([
            'assigned_to' => $request->input('picker_name')
        ]);
        
        return redirect()->back();
    }

    public function submitPicking(Request $request)
    {
        // $preparation = Preparation::where('preparation_id', $request->input('preparation_id'))->update([
        //     'assigned_to' => $request->input('picker_name')
        // ]);
        
        Order::where('order_id', $request->input('order_id'))->update(['is_printed_label' => 2, 'is_confirmed' => 1]);
        
        return redirect('admin/picking');
    }

    public function pickUpOrder(Request $request)
    {
        // $folder_name = 'img/uploads/picking/';
        // $file = $request->file('delivery_proof');
        // $ext = $file == null ? '' : $file->getClientOriginalExtension();
        // $name = uniqid().'.'.$ext;

        // if (!file_exists($folder_name)) {
        //     mkdir($folder_name, 777, true);
        // }
        // Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
    
        $preparation = Preparation::insertGetId([
            'preparation_status_id' => 2,
            'assigned_to' => $request->input('picker_name'),
            'order_id' => $request->input('order_id'),
            'updated_by' => Session::get('users')['id']
        ]);
        
        $preparation = Preparation::insertGetId([
            'preparation_status_id' => 3,
            'assigned_to' => $request->input('picker_name'),
            'preparation_notes' => $request->input('quality_control_status').', '.$request->input('quality_control_notes'),
            'order_id' => $request->input('order_id'),
            'updated_by' => Session::get('users')['id']
        ]);
        OrderHistory::insert([
            'order_id' => $request->input('order_id'),
            'order_status_id' => 7,
            'active_flag' => 1
        ]);
        Order::where('order_id', $request->input('order_id'))->update(['order_status_id' => 7]);
        
        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Pesanan Anda sedang diproses (".$request->input('order_code').")";
        $message = "Terima kasih telah melakukan transaksi di YukMarket! Pesanan anda dengan kode pesanan (".$request->input('order_code').") sedang diproses. Mohon menunggu!";
        $type = "transaction";
        if($request->input('fcm_token') != null){
            $this->processing_email($request->input('order_id'));
            $parameter = array(
                "to" => $request->input('fcm_token'),
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $request->input('is_agent') == 0 ? $request->input('customer_id') : null,
            "user_id" => $request->input('is_agent') == 1 ? $request->input('customer_id') : null,
            "order_id" => $request->input('order_id'),
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
        // $alert = Lang::get('notification.has been picked',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        // if($preparation > 0){
        //     $alert = Lang::get('notification.has been picked',['result' => strtolower(Lang::get('notification.successfully'))]);
        // }
        // Session::flash('message_alert', $alert);
        $this->accpetOrderTokped($request->input('order_id'));
        return redirect('admin/picking');
    }
    //---------------------------PUSH UPDATE STATUS TOKPED
    public function accpetOrderTokped($orderId){
        echo "\nstart";
        $ecomCode=$this->getOrderEcommerceCode($orderId);
        if($ecomCode){
            $token=$this->generateToken();
            $url="https://api.ecomm.inergi.id/tokopedia/acceptOrder?orderId=".$ecomCode->order_ecom_code;
            echo "URL: ".$url;
            try {
                Http::withToken($token)->timeout(3)->retry(2, 100)->post($url)->json();
                $this->updateOrderEcommerce($orderId);
            } catch (ConnectionException $e) {
                echo "\nurl: ".$url." TIMEOUT";
                return null;
            }
        }
    }
    public function getOrderEcommerceCode($orderId){
        return DB::table('10_order_ecommerce')
        ->select('order_ecom_code')
        ->where('order_id',$orderId)
        ->first();
    }
    public function generateToken(){
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        $getToken= Http::timeout(3)->retry(2, 100)->get($url)->json();
        $getAuth = explode(" ",$getToken['data']['token']);
        return array_pop($getAuth);
    }
    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }
    public function updateOrderEcommerce($orderId){
        DB::table('10_order_ecommerce')
        ->where('order_id',$orderId)
        ->update(['order_status_ecom_id'=>400]);
    }
    //---------------------------PUSH UPDATE STATUS TOKPED

    public function processing_email($id = null){
        // $id = 309;
        $transaction = Order::select('10_order.order_id','00_payment_method.payment_method_name','10_order.order_date','10_order.order_code','10_order.destination_address','00_address.address_name','00_address.address_detail','00_address.contact_person','00_address.phone_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'))->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('order_id', $id)->first();
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $logo = Config::get('logo.email');
        $whatsapp_number = GlobalSettings::where('global_parameter_name','whatsapp_customer_service')->first();
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        $data['whatsapp_number'] = $whatsapp_number;
        Mail::send('order_processing_email',$data, function ($message) use ($transaction)
            {
                $message->subject('Pemrosesan Barang ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        // return view("order_processing_email", $data);
    }

    public function qualityControl(Request $request){    
        $preparation = Preparation::insertGetId([
            'preparation_status_id' => 3,
            'assigned_to' => Session::get('users')['id'],
            'preparation_notes' => $request->input('quality_control_status').', '.$request->input('quality_control_notes'),
            'order_id' => $request->input('order_id'),
            'updated_by' => Session::get('users')['id']
        ]);
        OrderHistory::insert([
            'order_id' => $request->input('order_id'),
            'order_status_id' => 7,
            'active_flag' => 1
        ]);
        Order::where('order_id', $request->input('order_id'))->update(['order_status_id' => 7]);
        
        return redirect()->back();
    }

    public function packing(Request $request){
        // print_r($request->all());
        // exit;
        $order_id_list = explode(',',$request->input('order_id'));
        // $purchased_products = $request->input('purchased_products');
        // $purchased_quantity = $request->input('purchased_quantity');
        // print_r($order_id_list);
        // exit;
        $preparation_array = array();
        for ($k=0; $k < count($order_id_list); $k++) {
            $obj = [
                'preparation_status_id' => 4,
                'assigned_to' => Session::get('users')['id'],
                'order_id' => $order_id_list[$k],
                'updated_by' => Session::get('users')['id']
            ];
            array_push($preparation_array,$obj);
        }
        // for ($k=0; $k < count($purchased_products); $k++) { 
        //     $inventory = Inventory::where('organization_id', 1)->where('prod_id', $purchased_products[$k])->first();

        //     if($inventory != null){
        //         $goods_in_transit = $inventory['goods_in_transit'] == null ? 0 : $inventory['goods_in_transit'];
        //         Inventory::where('inventory_id',$inventory['inventory_id'])->update(array(
        //             'booking_qty' => ($inventory['booking_qty'] - $purchased_quantity[$k]),
        //             'goods_in_transit' => ($goods_in_transit + $purchased_quantity[$k])
        //         ));
        //     }
        // }
        Preparation::insert($preparation_array);
        
        return redirect('admin/packing');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
