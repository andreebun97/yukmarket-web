<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use Validator;
use App;
use Lang;
use App\Brand;
use Intervention\Image\Facades\Image;
use File;
use Config;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],10)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::brand/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/brand');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],10)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::brand/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        
        if($request->input('brand_id') == null){
            $validator = Validator::make($request->all(),[
                'brand_name' => ['required'],
                'brand_description' => ['required'],
                'brand_image' => ['required','mimes:jpg,jpeg,png,pneg,svg']
            ]);
        }else{
            if($request->file('brand_image') == null){
                $validator = Validator::make($request->all(),[
                    'brand_name' => ['required'],
                    'brand_description' => ['required']
                ]);
            }else{
                $validator = Validator::make($request->all(),[
                    'brand_name' => ['required'],
                    'brand_description' => ['required'],
                    'brand_image' => ['required','mimes:jpg,jpeg,png,pneg,svg']
                ]);    
            }
            
        }

        if($validator->fails()){
            if($request->input('brand_id') == null){
                Session::flash('message_alert',Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }else{
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $folder_name = 'img/uploads/brands/';
            $file = $request->file('brand_image');
            $ext = $file == null ? '' : $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;

            if($request->input('brand_id') == null){
                // $file->move($folder_name,$name);
                $brand_id = Brand::insertGetId([
                    'brand_name' => $request->input('brand_name'),
                    'brand_desc' => $request->input('brand_description'),
                    'brand_image' => $folder_name.$name,
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                ]);
                if($brand_id > 0){
                    // $brand_id = $request->input('product_brand');
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                    chmod($folder_name.$name, 0777);
                    $user_id = Session::get('users')['id'];
                    $brand_id_temp = (string)$brand_id;
                    for ($i= 0; $i < 5; $i++) {
                        if($i >= strlen($brand_id_temp)){
                            $brand_id_temp = "0".$brand_id_temp;
                        }
                    }
                    $user_id_temp = (string)$user_id;
                    for ($i= 0; $i < 3; $i++) {
                        if($i >= strlen($user_id_temp)){
                            $user_id_temp = "0".$user_id_temp;
                        }
                    }
                    $brand_code = "B".$user_id_temp.$brand_id_temp;
                    Brand::where('brand_id',$brand_id)->update(['brand_code' => $brand_code]);
                    Session::flash('message_alert',Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
                }
            }else{
                $updated_obj = array(
                    'brand_name' => $request->input('brand_name'),
                    'brand_desc' => $request->input('brand_description'),
                    'updated_by' => Session::get('users')['id']
                );
                if($request->file('brand_image') !== null){
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    chmod($folder_name.$name, 0777);
                    $updated_obj = array(
                        'brand_name' => $request->input('brand_name'),
                        'brand_desc' => $request->input('brand_description'),
                        'brand_image' => $folder_name.$name,
                        'updated_by' => Session::get('users')['id']
                    );
                    File::delete($request->input('brand_image_temp'));
                }
                Brand::where('brand_id',$request->input('brand_id'))->update($updated_obj);
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
            }
            return redirect('admin/brand');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/brand');
        }
        $brand_by_id = Brand::where('brand_id', $request->get('id'))->first();
        $user = Session::get('users');
        $data['user'] = $user;
        $data['brand_by_id'] = $brand_by_id;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],10)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::brand/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Brand::where('brand_id', $request->input('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

        if($update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/brand');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Brand::where('brand_id', $request->input('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

        if($update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/brand');
    }
}
