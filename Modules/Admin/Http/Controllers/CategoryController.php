<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Validator;
use App;
use Lang;
use App\Category;
use Session;
use Config;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function get(Request $request){
        $category = Category::where('active_flag',1)->where('category_id','<>',$request->get('parent'));
        if($request->get('id') != null){
            $category->where('category_id', $request->get('id'));
        }
        $category = $category->get();
        
        return response()->json(array('data' => $category));
    }
    
    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],3)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::category/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('category') != null){
            return redirect('admin/category');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],3)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::category/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $validator;
        if($request->input('category_id') == null){
            $validator = Validator::make($request->all(),[
                'category_name' => ['required']
                // 'category_description' => ['required'],
                // 'category_image' => ['required', 'mimes:jpeg,bmp,png,jpg,pneg']
            ]);
        }else{
            if($request->file('category_image') == null){
                $validator = Validator::make($request->all(),[
                    'category_name' => ['required']
                    // 'category_description' => ['required']
                ]);
            }else{
                $validator = Validator::make($request->all(),[
                    'category_name' => ['required']
                    // 'category_description' => ['required'],
                    // 'category_image' => ['required', 'mimes:jpeg,bmp,png,jpg,pneg']
                ]);
            }
        }
        
        if($validator->fails()){
            if($request->input('category_id') == null){
                Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }else{
                Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            // $folder_name = 'img/uploads/category/';
            // $file = $request->file('category_image');
            // $ext = $file == null ? '' : $file->getClientOriginalExtension();
            // $name = uniqid().'.'.$ext;
            if($request->input('category_id') == null){
                $category_id = Category::insertGetId([
                    'category_name' => $request->input('category_name'),
                    // 'category_desc' => $request->input('category_description'),
                    // 'category_image' => $folder_name.$name,
                    'parent_category_id' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? null : $request->input('parent_category_id'),
                    'category_level' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? 1 : $request->input('category_level'),
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                ]);
                // if (!file_exists($folder_name)) {
                //     mkdir($folder_name, 777, true);
                // }
                // ini_set('memory_limit', '256M');
                // Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                // chmod($folder_name.$name, 0777);
                Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
                return redirect('admin/category');                
            }else{
                $updated_obj = [
                    'category_name' => $request->input('category_name'),
                    // 'category_desc' => $request->input('category_description'),
                    'parent_category_id' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? null : $request->input('parent_category_id'),
                    'category_level' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? 1 : $request->input('category_level'),
                    'updated_by' => Session::get('users')['id']
                ];
                // if($file !== null){
                //     $folder_name = 'img/uploads/category/';
                //     $file = $request->file('category_image');
                //     $ext = $file == null ? '' : $file->getClientOriginalExtension();
                //     $name = uniqid().'.'.$ext;
                //     $updated_obj = [
                //         'category_name' => $request->input('category_name'),
                //         'category_desc' => $request->input('category_description'),
                //         'category_image' => $folder_name.$name,
                //         'parent_category_id' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? null : $request->input('parent_category_id'),
                //         'category_level' => $request->input('parent_category_id') == null || $request->input('parent_category_id') == "" ? 1 : $request->input('category_level'),
                //         'updated_by' => Session::get('users')['id']
                //     ];
                //     if (!file_exists($folder_name)) {
                //         mkdir($folder_name, 777, true);
                //     }
                //     ini_set('memory_limit', '256M');
                //     Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                //     chmod($folder_name.$name, 0777);
                // }
                Category::where('category_id', $request->input('category_id'))->update($updated_obj);
                
                Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
                return redirect('admin/category');
            }
            
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('category') == null){
            return redirect('admin/category');
        }
        $user = Session::get('users');
        $category = Category::where('category_id', $request->get('category'))->first();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['category'] = $category;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],3)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::category/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        //
        Category::where('category_id',$request->get('category'))->update(['active_flag' => 1]);
        Session::flash('message_alert', Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
        return redirect('admin/category');
    }

    public function delete(Request $request)
    {
        //
        Category::where('category_id',$request->get('category'))->update(['active_flag' => 0]);
        Session::flash('message_alert', Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
        return redirect('admin/category');
    }
}
