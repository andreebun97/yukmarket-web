<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Validator;
use App;
use App\Aboutus;
use App\Help;
use App\ColorLogo;
use App\Color;
use Lang;
use Session;
use Config;

class CmsController extends Controller {

    public function __construct() {
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function header(){
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['color'] = Color::get();
        $data['colorlogo'] = ColorLogo::get();
        // print_r($data['colorlogo']);
        // exit;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cms/colorlogo', $data);
    }
    
    public function updateheader(Request $request){
        try {
            $file = $request->file('logo_picture');
            $folder_name = 'img/uploads/logo/';

            $product_image_location = "";
            if ($file != null) {
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid() . '.' . $ext;
                $product_image_location = $folder_name . $name;
                $product_image_name = $name;
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->save(public_path($product_image_location), 80);
                chmod($product_image_location, 0777);
            } else {
                $product_image_name = $request->input('logo_picture_temp');
            }
            
            $file = $request->file('logo_page_picture');
            if ($file != null) {
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid() . '.' . $ext;
                $logo_page_location = $folder_name . $name;
                $logo_page_name = $name;
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->save(public_path($logo_page_location), 80);
                chmod($logo_page_location, 0777);
            } else {
                $logo_page_name = $request->input('logo_page_picture_temp');
            }
            $updated_obj = [
                'color' => $request->input('color'),
                'secondary_color' => $request->input('secondary_color'),
                'logo' => $product_image_name,
                'logo_page' => $logo_page_name,
                'updated_by' => Session::get('users')['id'],
                'updated_date' => date('Y-m-d H:i:s'),
            ];
            // print_r($updated_obj);
            // exit;
            ColorLogo::where('id', 1)->update($updated_obj);
            Session::flash('message_alert', Lang::get('Warna dan logo berhasil diubah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/cms/header');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function aboutus() {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['aboutus'] = Aboutus::get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cms/aboutus', $data);
    }

    public function updateaboutus(Request $request) {
        try {
            $file = $request->file('aboutus_picture');
            $folder_name = 'img/uploads/aboutus/';

            $product_image_location = "";
            if ($file != null && $request->input('aboutus_picture') == null) {
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid() . '.' . $ext;
                $product_image_location = $folder_name . $name;
                $product_image_name = $name;
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->save(public_path($product_image_location), 80);
                chmod($product_image_location, 0777);
            } else {
                $product_image_name = $request->input('aboutus_picture_temp');
            }
            $updated_obj = [
                'title' => $request->input('title'),
                'body' => $request->input('body'),
                'image' => $product_image_name,
                'updated_by' => Session::get('users')['id'],
                'updated_date' => date('Y-m-d H:i:s'),
            ];
            Aboutus::where('id', 1)->update($updated_obj);
            Session::flash('message_alert', Lang::get('Tentang Kita berhasil diubah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/cms/aboutus');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function help() {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['help'] = Help::where('row_status', 1)->get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cms/help', $data);
    }

    public function getHelp(Request $request) {
        $help = Help::where('row_status', 1)->where('id', '<>', $request->get('parent'));
        if ($request->get('id') != null) {
            $help->where('id', $request->get('id'));
        }
        $help = $help->get();

        return response()->json(array('data' => $help));
    }

    public function createHelp(Request $request) {
        if ($request->get('help') != null) {
            return redirect('admin/cms/help');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 3)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cms/formHelp', $data);
    }

    public function insertHelp(Request $request) {
        try {
            if ($request->input('help_id') == null) {
                $insert_obj = Help::insert([
                            'question' => $request->input('question'),
                            'answer' => $request->input('answer'),
                            'created_by' => Session::get('users')['id'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'row_status' => 1
                ]);
                Session::flash('message_alert', Lang::get('Bantuan telah ditambah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            } else {
                $updated_obj = [
                    'question' => $request->input('question'),
                    'answer' => $request->input('answer'),
                    'updated_by' => Session::get('users')['id'],
                    'updated_date' => date('Y-m-d H:i:s')
                ];
                Help::where('id', $request->input('help_id'))->update($updated_obj);
                Session::flash('message_alert', Lang::get('Bantuan telah diubah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            }
            return redirect('admin/cms/help');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function editHelp(Request $request) {
        if ($request->get('help') == null) {
            return redirect('admin/cms/help');
        }
        $user = Session::get('users');
        $help = Help::where('id', $request->get('help'))->first();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['help'] = $help;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 3)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cms/formHelp', $data);
    }

    public function deleteHelp(Request $request) {
        try {
            $updated_obj = [
                'updated_by' => Session::get('users')['id'],
                'updated_date' => date('Y-m-d H:i:s'),
                'row_status' => 0
            ];
            Help::where('id', $request->get('help'))->update($updated_obj);
            Session::flash('message_alert', Lang::get('Bantuan telah dihapus', ['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/cms/help');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

}
