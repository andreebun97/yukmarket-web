<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use DB;
use App\CategoryEcommerce;
use App\Product;
use App\Organization;
use Carbon\Carbon;

class TokopediaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    private $token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJpbnRpc2luZXJnaXRla25vbG9naSIsImF1ZCI6InVzZXIiLCJzdWIiOiJhZG1pbkB0ZXN0LmNvbSIsImV4cCI6MTYwNTg1ODkzMiwicm9sZSI6MSwiaWQiOjEsImZ1bGxOYW1lIjoiYWRtaW4ifQ.v25xCYFWy_l_6NG1sixyXZGOGvE_DzP7RgSY1AWPW58CgfcBRTTDCATr-U1zdHc-5neqDcpgyGUAxfgLFmuwxw";

    public function generateToken(){
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();                            
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);            
        } catch (ConnectionException $e) {
            $token=null;            
        }         
        $this->token=$token;
    }

    public function push(Request $request){
        // print_r($request->all());
        // exit;
        
        // echo "<pre>";
        // dd($variant);
        // exit;
        $this->generateToken();
        $shopConfig = $this->getShopName();
        $uploaded_file = $request->file('product_image');
        $description = $request->input('product_description');
        $folder_name = "img/uploads/products/";
        $product_weight = $this->getBeratProductTokped($request->input('internal_category'));
        $product_weight = array_filter($product_weight, function($value){
            return $value['variant_id'] == 1;
        });
        $product_weight = array_values($product_weight);
        $product_weight_unit = $product_weight[0]['units'][0]['values'];
        // dd($product_weight_unit);
        // exit;
        // $file = $request->file('product_variant_picture')[$b];
        // print_r($file);
        $ext = $uploaded_file == null ? '' : $uploaded_file->getClientOriginalExtension();
        $name = uniqid().'.'.$ext;
        $product_image_location = $folder_name.$name;
        if (!file_exists($folder_name)) {
            mkdir($folder_name, 777, true);
        }
        // ini_set('memory_limit', '256M');
        Image::make($uploaded_file)->fit(300,300)->save(public_path($product_image_location),80);
        chmod($product_image_location, 0777);

        $product_image_location = request()->root().'/'.$product_image_location;
        $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
        $dbEcomId=Organization::where('organization_name','TOKOPEDIA')->first();

        $img='http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg';
        // echo 'test';
        // exit;
        $preorder=(Object)['duration'=>0,'is_active'=>false,'time_unit'=>'DAY'];
        $etalase =(Object)['id'=>26345293];//------------------------------------form select
        $picture1=(Object)['file_path' => $product_image_location];//---------------------------------parent/primary product
        $pictures = array($picture1);
        $prodName = $request->input('product_name');
        $sku = $request->input('parent_product_code');
        // $file_path_array = array();
        $file_path_obj[0]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];//----img parent/variant primary
        $file_path_obj[1]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];//-------img variant not primary
        $file_path_obj[2]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];
        $file_path_array[0]=array($file_path_obj[0]);//----img parent/variant primary
        $file_path_array[1]=array($file_path_obj[1]);//-------img variant not primary
        $file_path_array[2]=array($file_path_obj[2]);
        $combination = array([0],[1],[2]);
        $variantPicture=[
            $file_path_obj[0],$file_path_obj[1],$file_path_obj[2]
            // $file_path_array[0],$file_path_array[1]
        ];
        $productsVarian[0]=(Object)[//-------------------------product parent/variant primary
            'combination'=>$combination[0],
            'is_primary'=>true,
            'pictures'=>array(['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"]),
            'price'=>1000,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>10
        ];
         $productsVarian[1]=(Object)[//-------------------------product variant not primary
            'combination'=>$combination[1],
            'is_primary'=>false,
            'pictures'=>array(['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"]),
            'price'=>1000,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>20
        ];
        $productsVarian[2]=(Object)[//-------------------------product variant not primary
            'combination'=>$combination[2],
            'is_primary'=>false,
            'pictures'=>array(['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"]),
            'price'=>750,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>20
        ];
        // $productsVarianArray=[$productsVarian[0],$productsVarian[1]];
        $productsVarianArray=[$productsVarian[0],$productsVarian[1],$productsVarian[2]];
        // dd($productsVarianArray);
        // exit;
        // $uploaded_variant_file = $request->input('product_variant_image');
        $selectionId=1;
        $selectionUnitId=0;
        $selectionOptionObject[0]=(Object)[
            'hex_code'=>"",//-------------------------------string empty
            'unit_value_id'=>$product_weight_unit[0]['value_id'],//------------------------form id berat
            'value'=>'Kating 1KG'//------------------------------varian name
        ];
        $selectionOptionObject[1]=(Object)[
            'hex_code'=>"",
            'unit_value_id'=>$product_weight_unit[1]['value_id'],
            'value'=>'Besar 1KG'
        ];
        $selectionOptionObject[2]=(Object)[
            'hex_code'=>"#1d6cbb",
            'unit_value_id'=>$product_weight_unit[2]['value_id'],
            'value'=>'Biru 1KG'
        ];
        // $selectionOptionArray=[$selectionOptionObject[0],$selectionOptionObject[1]];
        $selectionOptionArray=[$selectionOptionObject[0],$selectionOptionObject[1],$selectionOptionObject[2]];
        $selectionObj=(Object)[
            'id'=>$selectionId,
            'options'=>$selectionOptionArray,
            'unit_id'=>$selectionUnitId
        ];
        $selectionArray=[$selectionObj];
        $sizechartsObject[0]=(Object)array('file_path'=>$img);
        $sizechartsArray=[$sizechartsObject[0]];
        $bruto = $request->input('bruto');
        $internal_category_id = $request->input('internal_category');
        // echo $internal_category_id;
        // exit;
        $category = $internal_category_id;
        // print_r($category);
        // exit;
        $ecommerce_category_id = $category;
        // for ($k=0; $k < count($uploaded_variant_file); $k++) {
        //     $image_obj = array('file_path' => '');
        //     $selectionOptionObject = array(
        //         'hex_code'=>"",//-------------------------------string empty
        //         'unit_value_id'=>838,//------------------------form id berat
        //         'value'=>'varian 1 gram2'//------------------------------varian name
        //     );
        //     if(isset($uploaded_variant_file[$k])){
        //         $uploaded_file = $request->file('product_image');
        //         // $file = $request->file('product_variant_picture')[$b];
        //         // print_r($file);
        //         $ext = $uploaded_file == null ? '' : $uploaded_file->getClientOriginalExtension();
        //         $name = uniqid().'.'.$ext;
        //         $product_variant_image_location = $folder_name.$name;
        //         if (!file_exists($folder_name)) {
        //             mkdir($folder_name, 777, true);
        //         }
        //         // ini_set('memory_limit', '256M');
        //         Image::make($uploaded_file)->fit(300,300)->save(public_path($product_variant_image_location),80);
        //         chmod($product_variant_image_location, 0777);
        //         $image_obj = array('file_path' => $product_variant_image_location);
        //         // array_push($file_path_array, $obj);
        //         // array_push($obj);
                
        //     }
        //     $obj = array(
        //         "combination" => array($k),
        //         "is_primary" => $k == 0 ? true : false,
        //         "pictures" => $image_obj,
        //         "price" => 1000,
        //         "sku" => $sku,
        //         "status" => "LIMITED",
        //         "stock" => $request->input('stock')
        //     );
        //     array_push($productsVarianArray, $obj);
        //     array_push($selectionOptionArray, $selectionOptionObject);

        //     // array_push($combination, array($k));
        // }
        $variant = null;
        $variant = (Object)array(
            'products'=>$productsVarianArray,
            'selection'=>$selectionArray,
            // 'sizecharts'=>$sizechartsArray
        );
        // dd($variant);
        // exit;
        // $variant = array(
        //     "variant_id" => 1,
        //     "name" => "Warna",
        //     "identifier" => "colour",
        //     "status" => 2,
        //     "has_unit" => 0,
        //     "units" => array(
        //         array(
        //             "unit_id" => 0,
        //             "name" => "",
        //             "short_name" => "",
        //             "values" => array(
        //                 array(
        //                     "value_id" => 1,
        //                     "value" => "Black",
        //                     "hex_code" => "",
        //                     "icon" => ""
        //                 ),
        //                 array(
        //                     "value_id" => 2,
        //                     "value" => "White",
        //                     "hex_code" => "",
        //                     "icon" => ""
        //                 )
        //             )
        //         )
        //     )
        // );
        $products= (Object)[
            'id' => null,
            'category_id'=>$ecommerce_category_id,
            'condition'=>'NEW', //-------------- string option: NEW, USED
            'description'=>$description,
            'etalase'=>$etalase,
            'is_free_return'=>false, //-----------boolean option: true, false
            'is_must_insurance'=>false, //------------boolean option: true, false
            'min_order'=>1,
            'name'=>$prodName,
            'pictures'=> $variantPicture,
            'preorder'=>$preorder,
            'price'=>$request->input('product_price'), //---------------------parent price, jika ada variant maka tidak berlaku
            'price_currency'=>"IDR",
            'sku'=> $request->input('parent_product_code'),
            'status'=>'LIMITED',
            'stock'=>10,
            'variant'=>$variant,
            // 'videos'=>$videosArray,
            'weight'=>$bruto,
            'weight_unit'=>"KG",
            // 'wholesale'=>$wholesaleArray
        ];
        $productObject=[$products];
        $obj=['products'=>$productObject];
        echo "<pre>";
        dd($obj);
        exit;
        
        // dd($obj);
        $shopId=$dbShopId->organization_code;
        // echo $shopId;
        // exit;
        if($products->id==null){//--------------------jika tambah baru
            $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
        }else{//-------------------------------------jika update
            $url='https://api.ecomm.inergi.id/tokopedia/updateProductWithVariant?shopId='.$shopId;
        }
        // echo $url;
        // exit;

        $pushProduct=Http::withToken($this->token)->timeout(3)->retry(2, 100)->post($url,$obj);
        // print_r($pushProduct);
        // exit;
        $statusPushProduct=json_decode($pushProduct->body());
        $product_ecommerce_code;
        // print_r($statusPushProduct);
        // exit;
        // echo "\n".$statusPushProduct->data->upload_id;
        $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$statusPushProduct->data->upload_id;
        echo $statusPAushProduct->data->upload_id.'<br>';
        // exit;
        $status = "loading";
        do{
            sleep(5);
            $getUploadedProductStatus = Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
            $getUploadedProductResponse = json_decode($getUploadedProductStatus->body());
            if($getUploadedProductResponse->data->failed_rows > 0){
                $status = 'failed';
                dd($getUploadedProductResponse);
            }else{
                $status = 'success';
                dd($getUploadedProductResponse);
            }
        }while($status == "loading");
        // print_r($getUploadedProductResponse);
        exit;
    }

    public function getShopName(){
        $shop=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokped_shop')
        ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
        ->first();
        return $shop;
    }

    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function getBeratProductTokped($category){
        $url='https://api.ecomm.inergi.id/tokopedia/getAllVariantsByCategoryId?categoryId='.$category;
        $this->generateToken();
        $berat=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $berat['data'];
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function pushByObject(Request $request){
        // echo "test";
        // exit;
        $this->generateToken();
        $shopConfig = $this->getShopName();

        $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
        $dbEcomId=Organization::where('organization_name','TOKOPEDIA')->first();
        $shopId=$dbShopId->organization_code;
        // $products = $request->all();
        // $products = array(
        //     "products" => [
        //         array(
        //             "id" => 1432129176,
        //             "category_id" => 1226,
        //             "condition" => "NEW",
        //             "description" => "Durian Medan Dinginnnn",
        //             "etalase" => array(
        //                 "id" => 24532950
        //             ),
        //             "is_free_return" => false,
        //             "is_must_insurance" => false,
        //             "min_order"=> 1,
        //             "name"=> "Durian Medan Dingin",
        //             "pictures"=> [
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 )
        //             ],
        //             "preorder" => array(
        //                 "duration" => 0,
        //                 "is_active" => false,
        //                 "time_unit" => "DAY"
        //             ),
        //             "price" => 5000,
        //             "price_currency" => "IDR",
        //             "sku" => "P0050051190250077",
        //             "status" => "LIMITED",
        //             "stock" => 1,
        //             "variant" => array(
        //                 "products" => [
        //                     array(
        //                         "combination" => [0],
        //                         "is_primary" => true,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 5000,
        //                         "sku" => "P0050051190250077",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination"=> [1],
        //                         "is_primary"=> false,
        //                         "pictures"=> [
        //                             array(
        //                             "file_path"=> "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 10000,
        //                         "sku" => "P0050051190250077",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination" => [2],
        //                         "is_primary" => false,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 8000,
        //                         "sku" => "P0050051190250077",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination" => [3],
        //                         "is_primary" => false,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 5000,
        //                         "sku" => "P0050051190250077",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     )
        //                 ],
        //                 "selection" => [
        //                     array(
        //                         "id" => 1,
        //                         "options" => [
        //                             array(
        //                                 "hex_code" => "#102ce3",
        //                                 "unit_value_id" => 1,
        //                                 "value" => "Bau 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "#69a832",
        //                                 "unit_value_id" => 2,
        //                                 "value" => "Wangi 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "#ffffff",
        //                                 "unit_value_id" => 5,
        //                                 "value" => "Pahit 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "#000000",
        //                                 "unit_value_id" => 9,
        //                                 "value" => "Manis 1KG"
        //                             )
        //                         ]
        //                     )
        //                 ]
        //             ),
        //             "weight" => 1,
        //             "weight_unit" => "KG"
        //         )
        //     ]
        // );


        // $products = array(
        //     "products" => [
        //         array(
        //             "id" => 1399950921,
        //             "category_id" => 1226,
        //             "condition" => "NEW",
        //             "description" => "Anggur merah dingin alamiah",
        //             "etalase" => array(
        //                 "id" => 24532950
        //             ),
        //             "is_free_return" => false,
        //             "is_must_insurance" => false,
        //             "min_order"=> 1,
        //             "name"=> "Durian Medann 1 KG",
        //             "pictures"=> [
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 ),
        //                 array(
        //                     "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                 )
        //             ],
        //             "preorder" => array(
        //                 "duration" => 0,
        //                 "is_active" => false,
        //                 "time_unit" => "DAY"
        //             ),
        //             "price" => 10000,
        //             "price_currency" => "IDR",
        //             "sku" => "P0050051190250075",
        //             "status" => "LIMITED",
        //             "stock" => 1,
        //             "variant" => array(
        //                 "products" => [
        //                     array(
        //                         "combination" => [0],
        //                         "is_primary" => true,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 10000,
        //                         "sku" => "P0050051190250075",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination"=> [1],
        //                         "is_primary"=> false,
        //                         "pictures"=> [
        //                             array(
        //                             "file_path"=> "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 9500,
        //                         "sku" => "P0050051190250075",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination" => [2],
        //                         "is_primary" => false,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 7000,
        //                         "sku" => "P0050051190250075",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     array(
        //                         "combination" => [3],
        //                         "is_primary" => false,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"
        //                         )],
        //                         "price" => 9000,
        //                         "sku" => "P0050051190250075",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     )
        //                 ],
        //                 "selection" => [
        //                     array(
        //                         "id" => 1,
        //                         "options" => [
        //                             array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 1,
        //                                 "value" => "Dingin 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 2,
        //                                 "value" => "SbDingin 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 5,
        //                                 "value" => "Phanas 1KG"
        //                             ),
        //                             array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 9,
        //                                 "value" => "Merah 1KG"
        //                             )
        //                         ]
        //                     )
        //                 ]
        //             ),
        //             "weight" => 1000,
        //             "weight_unit" => "GR"
        //         )
        //     ]
        // );


        // $products = array(
        //     "products" => [
        //         (Object)array(
        //             // "id" => 1540242833,
        //             "category_id" => 3473,
        //             "condition" => "NEW",
        //             "description" => "kukus manies",
        //             "etalase" => (Object)array(
        //                 "id" => 26345293
        //             ),
        //             "is_free_return" => false,
        //             "is_must_insurance" => false,
        //             "min_order"=> 1,
        //             "name"=> "Kukus Manis",
        //             "pictures"=> [
        //                 (Object)array(
        //                     "file_path" => "http://yukmarket.id/img/ecomm-default.png"
        //                 ),
        //                 (Object)array(
        //                     "file_path" => "http://yukmarket.id/img/ecomm-default.png"
        //                 )
        //             ],
        //             "preorder" => (Object)array(
        //                 "duration" => 0,
        //                 "is_active" => false,
        //                 "time_unit" => "DAY"
        //             ),
        //             "price" => 25000,
        //             "price_currency" => "IDR",
        //             "sku" => "P0000051190253231",
        //             "status" => "LIMITED",
        //             "stock" => 1,
        //             "variant" => array(
        //                 "products" => [
        //                     (Object)array(
        //                         "combination" => [0],
        //                         "is_primary" => true,
        //                         "pictures" => [array(
        //                             "file_path" => "http://yukmarket.id/img/ecomm-default.png"
        //                         )],
        //                         "price" => 24000,
        //                         "sku" => "P0000051190253231",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     ),
        //                     (Object)array(
        //                         "combination"=> [1],
        //                         "is_primary"=> false,
        //                         "pictures"=> [
        //                             array(
        //                             "file_path"=> "http://yukmarket.id/img/ecomm-default.png"
        //                         )],
        //                         "price" => 25000,
        //                         "sku" => "P0000051190253231",
        //                         "status" => "LIMITED",
        //                         "stock" => 1
        //                     )
        //                 ],
        //                 "selection" => [
        //                     (Object)array(
        //                         "id" => 1,
        //                         "options" => [
        //                             (Object)array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 1,
        //                                 "value" => "Manis 200gr"
        //                             ),
        //                             (Object)array(
        //                                 "hex_code" => "",
        //                                 "unit_value_id" => 2,
        //                                 "value" => "Pahit 200gr"
        //                             )
        //                         ]
        //                     )
        //                 ]
        //             ),
        //             "sizecharts" => null,
        //             "weight" => 250,
        //             "weight_unit" => "GR"
        //         )
        //     ]
        // );
        // echo "<pre>";
        // echo json_encode($products, JSON_PRETTY_PRINT);
        // exit;
        // print_r($request->all());
        $products = json_encode($request->all());
        $products = json_decode($products);
        $products = array("products"=>$products->products);
        // $product_object = (Object)$request->products[0];
        // $products = ['products' => [$product_object]];
        // $products = $request->all();
        // dd($products);
        // exit;
        $url='https://api.ecomm.inergi.id/tokopedia/updateProductWithVariant?shopId='.$shopId;
        if($request->products[0]['id'] == null || !isset($request->products[0]['id'])){
            $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
        }
        // echo $url;
        // exit;
        // echo $this->token;
        // exit;
        $pushProduct=Http::withToken($this->token)->timeout(3)->retry(2, 100)->post($url,$products);
        // print_r($pushProduct);
        // exit;
        $statusPushProduct=json_decode($pushProduct->body());
        // print_r($statusPushProduct);
        // exit;
        $upload_id = $statusPushProduct->data->upload_id;
        // echo $upload_id;
        // exit;
        $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$upload_id;
        echo "<pre>";
        echo json_encode($statusPushProduct,JSON_PRETTY_PRINT);
        echo "<br>";
        echo "response: <br>";
        echo "<pre>";
        $status = "loading";
        $response = array();
        do{
            sleep(6);
            $getUploadedProductStatus = Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
            $getUploadedProductResponse = json_decode($getUploadedProductStatus->body());

            $response = $getUploadedProductResponse;
            // dd($getUploadedProductResponse);
            // exit;
            // echo $product_ecommerce_code;
            // exit;
            if($getUploadedProductResponse->data->success_rows > 0){
                $product_ecommerce_code = $getUploadedProductResponse->data->success_rows_data[0]->product_id;
                // print_r($getUploadedProductResponse);
                // exit;
                $status = "success";
                $prod_ecom_code = $getUploadedProductResponse->data->success_rows_data[0]->product_id;
            }elseif($getUploadedProductResponse->data->unprocessed_rows > 0){
                $status = "loading";
            }else{
                if($getUploadedProductResponse->data->failed_rows > 0){
                    $tokopedia_error_list = $getUploadedProductResponse->data->failed_rows_data[0]->error;
                    // print_r($tokopedia_error_list);
                    // exit;
                }
                $status = "failed";
            }
        }while($status == "loading");
        if($status != "loading"){
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
        exit;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
