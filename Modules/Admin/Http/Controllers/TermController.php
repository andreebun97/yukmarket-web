<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Validator;
use App;
use App\PrivacyTerm;
use Lang;
use Session;
use Config;

class TermController extends Controller {

    public function __construct() {
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index(){
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['term'] = PrivacyTerm::where('key', 'term_condition')->get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::term/index', $data);
    }
    
    public function termUpdate(Request $request){
        try {
            $updated_obj = [
                'value' => $request->input('term_value'),
                'updated_by' => Session::get('users')['id'],
                'updated_date' => date('Y-m-d H:i:s'),
            ];
            PrivacyTerm::where('key', 'term_condition')->update($updated_obj);
            Session::flash('message_alert', Lang::get('Syarat dan Ketentuan berhasil diubah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/term');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}