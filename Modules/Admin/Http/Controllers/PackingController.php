<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\Helpers\Currency;
use App\Helpers\PhoneNumber;
use App\Helpers\UomConvert;
use App;
use Lang;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Shipment;
use App\Order;
use App\OrderHistory;
use App\Preparation;
use App\OrderStatus;
use App\OrderDetail;
use App\AddressDetail;
use Validator;
use Mail;
use DB;
use Config;

class PackingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    protected $phone_number;
    protected $currency;

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $phone_number = new PhoneNumber();
        $this->currency = $currency;
        $this->phone_number = $phone_number;
    }

    public function index(Request $request)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],22)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        // $uom_conversion = $uom_conversion->convert("kg","hg");
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        return view('admin::packing/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/transaction');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],19)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $transactions = OrderDetail::select('10_order_detail.dim_length','00_product.prod_image','10_order_detail.dim_width','10_order_detail.dim_height','10_order_detail.price','10_order_detail.quantity','10_order_detail.order_id','00_product.prod_name','00_product.prod_code','10_order_detail.uom_value','10_order_detail.bruto','00_product.uom_id','00_uom.uom_name','10_order_detail.sku_status','00_product.variant_id','00_product.prod_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id', $request->get('id'))->get();
        $order_master = Order::select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.admin_fee','10_order.admin_fee_percentage','10_order.transfer_date','10_order.pricing_include_tax','10_order.buyer_name','10_order.buyer_address','10_order.national_income_tax','10_order.shipping_receipt_num',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.shipment_price','00_address.address_name','00_address.address_info','10_order.dim_length','10_order.dim_width','10_order.dim_height','10_order.weight AS total_weight','10_order.total_product','10_order.total_pack','00_address.contact_person','00_address.phone_number','10_order.is_fixed','10_order.is_confirmed','10_order.is_printed_invoice','10_order.voucher_amount','10_order.destination_address','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','10_order.midtrans_transaction_id','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos',DB::raw('(SELECT 10_preparation.preparation_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 3 ORDER BY 10_preparation.preparation_id DESC LIMIT 1)'),'10_order.max_price','10_order.voucher_id','10_order.min_price','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.order_status_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'00_order_status.order_status_name','10_order.payment_method_id','00_payment_method.payment_method_name','00_preparation_status.preparation_status_id AS current_preparation_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->where('10_order.order_id', $request->get('id'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->where('10_order.order_status_id','7')->orderBy('10_preparation.preparation_id','DESC')->first();
        // $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_customer','00_customer.customer_id','=','00_address.customer_id')->join('10_order','10_order.buyer_user_id','=','00_customer.customer_id')->where('order_id', $request->get('id'))->get();
        $preparation = Preparation::where('order_id', $request->get('id'))->orderBy('preparation_status_id','DESC')->first();
        // $verification_steps = array("Pesanan Diterima","Pembayaran Diterima","Verifikasi","Picking","Quality Control","Packing","Delivery","Pesanan Selesai");
        // $uom_convertion = new UomConvert();
        // print_r($uom_convertion->convert("kg","mg"));
        // exit;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['transactions'] = $transactions;
        $data['order_master'] = $order_master;
        $data['currency'] = $this->currency;
        $data['preparation'] = $preparation;
        $data['phone_number'] = $this->phone_number;
        // $data['address_detail'] = $address_detail;
        // $data['uom_convertion'] = $uom_convertion;
        return view('admin::packing/form', $data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function printInvoice(Request $request)
    {
        $update = Order::whereIn('order_id', explode(',',$request->input('order_id')))->update(['is_printed_invoice' => 1]);
        $response = array();
        $responseCode = 200;

        if($update){
            $response = array('isSuccess' => true, 'message' => __('notification.has been updated',['result' => strtolower(__('notification.successfully'))]));
        }else{
            $response = array('isSuccess' => false, 'message' => __('notification.has been updated',['result' => strtolower(__('notification.inaccurately'))]));
            $responseCode = 500;
        }
        return response()->json($response, $responseCode);
    }

    public function multiple_invoice(Request $request)
    {
        $purchased_products = array();
        $purchased_products_array = array();
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order_detail.sku_status','10_order_detail.uom_value','10_payment.invoice_no','00_uom.uom_name','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','10_order.shipment_method_id','10_order.shipping_receipt_num','10_order.buyer_name','10_order.buyer_address',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','10_order.pricing_include_tax','10_order.national_income_tax','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','10_order_detail.promo_value','00_vouchers.voucher_name','00_vouchers.voucher_code',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),'00_address.contact_person','00_address.phone_number','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->whereIn('10_order.order_id',explode(',',$request->get('id')))->get();
        }
        foreach($purchased_products as $pp){ 
            $purchased_products_array[$pp['order_id']]['order_id'] = $pp['order_id'];
            $purchased_products_array[$pp['order_id']]['order_code'] = $pp['order_code'];
            $purchased_products_array[$pp['order_id']]['order_status_name'] = $pp['order_status_name'];
            $purchased_products_array[$pp['order_id']]['picker_name'] = $pp['picker_name'];
            $purchased_products_array[$pp['order_id']]['pricing_include_tax'] = $pp['pricing_include_tax'];
            $purchased_products_array[$pp['order_id']]['national_income_tax'] = $pp['national_income_tax'];
            $purchased_products_array[$pp['order_id']]['shipment_method_id'] = $pp['shipment_method_id'];
            $purchased_products_array[$pp['order_id']]['shipment_method_name'] = $pp['shipment_method_name'];
            $purchased_products_array[$pp['order_id']]['destination_address'] = $pp['destination_address'];
            $purchased_products_array[$pp['order_id']]['shipment_price'] = $pp['shipment_price'];
            $purchased_products_array[$pp['order_id']]['admin_fee'] = $pp['admin_fee'];
            $purchased_products_array[$pp['order_id']]['organization_type_id'] = $pp['organization_type_id'];
            $purchased_products_array[$pp['order_id']]['organization_logo'] = $pp['organization_logo'];
            $purchased_products_array[$pp['order_id']]['shipment_logo'] = $pp['shipment_logo'];
            $purchased_products_array[$pp['order_id']]['address_name'] = $pp['address_name'];
            $purchased_products_array[$pp['order_id']]['address_info'] = $pp['address_info'];
            $purchased_products_array[$pp['order_id']]['customer_name'] = $pp['customer_name'];
            $purchased_products_array[$pp['order_id']]['customer_email'] = $pp['customer_email'];
            $purchased_products_array[$pp['order_id']]['customer_phone_number'] = $pp['customer_phone_number'];
            $purchased_products_array[$pp['order_id']]['address_detail'] = $pp['address_detail'];
            $purchased_products_array[$pp['order_id']]['invoice_no'] = $pp['invoice_no'];
            $purchased_products_array[$pp['order_id']]['order_date'] = $pp['order_date'];
            $purchased_products_array[$pp['order_id']]['kabupaten_kota_name'] = $pp['kabupaten_kota_name'];
            $purchased_products_array[$pp['order_id']]['kecamatan_name'] = $pp['kecamatan_name'];
            $purchased_products_array[$pp['order_id']]['kelurahan_desa_name'] = $pp['kelurahan_desa_name'];
            $purchased_products_array[$pp['order_id']]['kode_pos'] = $pp['kode_pos'];
            $purchased_products_array[$pp['order_id']]['contact_person'] = $pp['contact_person'];
            $purchased_products_array[$pp['order_id']]['phone_number'] = $pp['phone_number'];
            $purchased_products_array[$pp['order_id']]['detail'][] = array(
                'sku_status' => $pp['sku_status'],
                'uom_value' => $pp['uom_value'],
                'uom_name' => $pp['uom_name'],
                'purchased_price' => $pp['purchased_price'],
                'purchased_quantity' => $pp['purchased_quantity'],
                'purchased_product_name' => $pp['purchased_product_name'],
                'promo_value' => $pp['promo_value'],
            );
        }
        // ini_set('memory_limit', '8192M');
        $purchased_products_array = array_values($purchased_products_array);
        $user = Session::get('users');
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['user'] = $user;
        $data['purchased_products'] = $purchased_products_array;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        return view('admin::packing/multiple_invoice', $data);
    }

    public function label(Request $request)
    {   
        $purchased_products = array();
        
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.shipping_receipt_num','10_order.pricing_include_tax','10_order.national_income_tax','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','10_order.buyer_name','10_order.buyer_address','10_order.shipment_method_id','00_organization.organization_type_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.dim_length','10_order.dim_width','10_order.dim_height','10_order.weight AS total_weight','10_order.total_product','10_order.total_pack','10_payment.payment_id','10_payment.invoice_no','10_order_detail.promo_value','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','00_vouchers.voucher_name','00_vouchers.voucher_code',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->where('10_order.order_id',$request->get('id'))->get();
        }


        // echo "<pre>";
        // print_r(range(1, $purchased_products[0]->total_pack));
        // // print_r($purchased_products->all());
        // print_r($data['loop_receipt']);
        // echo "<pre>";
        // exit();
        // echo $purchased_products[0]->total_pack == null ? 1 : $purchased_products[0]->total_pack;
        // exit;
        $total_pack = $purchased_products[0]->total_pack == null ? 1 : $purchased_products[0]->total_pack;

        $data['loop_receipt'] = range(1, $total_pack);
        $user = Session::get('users');
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['user'] = $user;
        $data['purchased_products'] = $purchased_products;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        $data['total_pack'] = $total_pack;

        return view('admin::packing/label', $data);
    }

    public function printLabel(Request $request)
    {
        $update = Order::whereIn('order_id', explode(',',$request->input('order_id')))->update(['is_printed_label' => 1]);
        $response = array();
        $responseCode = 200;

        if($update){
            Order::whereIn('order_id', explode(',',$request->input('order_id')))->update(['is_printed_label' => 2, 'is_confirmed' => 1]);
            $response = array('isSuccess' => true, 'message' => __('notification.has been updated',['result' => strtolower(__('notification.successfully'))]));
        }else{
            $response = array('isSuccess' => false, 'message' => __('notification.has been updated',['result' => strtolower(__('notification.inaccurately'))]));
            $responseCode = 500;
        }
        return response()->json($response, $responseCode);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function submitPackingDetail(Request $request)
    {
        $update = Order::whereIn('order_id', explode(',',$request->input('order_id')))->update([
            'weight' => $request->input('product_weight') != null ? $request->input('product_weight') : null,
            'total_pack' => $request->input('total_pack') != null ? $request->input('total_pack') : null,
            'total_dimension' => str_replace(".","",$request->input('total_dimension')) != null ? str_replace(".","",$request->input('total_dimension')) : null,
        ]);

        return redirect('admin/packing/label/?id='.$request->input('order_id'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function reject(Request $request)
    {
        Preparation::where('preparation_id', $request->input('preparation_id'))->update(['preparation_notes' => $request->input('rejected_reason')]);

        $preparation = Preparation::insertGetId([
            'preparation_status_id' => 1,
            'order_id' => $request->input('order_id'),
            'assigned_to' => Session::get('users')['id'],
            'updated_by' => Session::get('users')['id']
        ]);
        
        OrderHistory::insert([
            'order_id' => $request->input('order_id'),
            'order_status_id' => 6,
            'active_flag' => 1
        ]);
        Order::where('order_id', $request->input('order_id'))->update(['order_status_id' => 6]);

        return redirect('admin/packing');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
