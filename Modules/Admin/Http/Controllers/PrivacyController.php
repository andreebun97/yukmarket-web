<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Validator;
use App;
use App\PrivacyTerm;
use Lang;
use Session;
use Config;

class PrivacyController extends Controller {

    public function __construct() {
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index(){
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'], 24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['privacy'] = PrivacyTerm::where('key', 'privacy_policy')->get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::privacy/index', $data);
    }
    
    public function privacyUpdate(Request $request){
        try {
            $updated_obj = [
                'value' => $request->input('privacy_value'),
                'updated_by' => Session::get('users')['id'],
                'updated_date' => date('Y-m-d H:i:s'),
            ];
            PrivacyTerm::where('key', 'privacy_policy')->update($updated_obj);
            Session::flash('message_alert', Lang::get('Kebijakan Pribadi berhasil diubah', ['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/privacy');
        } catch (\Exception $e) {
            Session::flash('message_alert', $e->getMessage());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}