<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use App;
use Lang;
use App\MappingCoverage;
use App\Province;
use App\Regency;
use App\District;
use App\Village;
use App\Organization;
use Session;
use Config;

class MappingCoverageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
     public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index(Request $request)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],41)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $organization = Organization::where('organization_type_id',2)->where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['organization'] = $organization;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::mapping_coverage/index', $data);
    }

    public function getMappingCoverage(Request $request){
        $organization_id = $request->input('organization_id');
        $mapping_coverage = Province::select('00_provinsi.provinsi_id AS province_id', '00_provinsi.provinsi_name AS province_name', '00_kabupaten_kota.kabupaten_kota_id AS regency_id', '00_kabupaten_kota.kabupaten_kota_name AS regency_name','00_kecamatan.kecamatan_id AS district_id', '00_kecamatan.kecamatan_name AS district_name','00_kelurahan_desa.kelurahan_desa_id AS village_id', '00_kelurahan_desa.kelurahan_desa_name AS village_name', '00_kelurahan_desa.kode_pos AS postal_code')->join('00_kabupaten_kota','00_kabupaten_kota.provinsi_id','=','00_provinsi.provinsi_id')->join('00_kecamatan','00_kecamatan.kabupaten_kota_id','=','00_kabupaten_kota.kabupaten_kota_id')->join('00_kelurahan_desa','00_kelurahan_desa.kecamatan_id','=','00_kecamatan.kecamatan_id');
        if($request->input('province') != null){
            $mapping_coverage->whereIn('00_provinsi.provinsi_id', $request->input('province'));
        }
        if($request->input('regency') != null){
            $mapping_coverage->whereIn('00_kabupaten_kota.kabupaten_kota_id', $request->input('regency'));
        }
        if($request->input('district') != null){
            $mapping_coverage->whereIn('00_kecamatan.kecamatan_id', $request->input('district'));
        }
        $mapping_coverage->whereNotIn('00_kelurahan_desa.kelurahan_desa_id', function($query) use($organization_id){
            $query->from('00_mapping_coverage')->select('00_mapping_coverage.kelurahan_desa_id')->whereIn('00_mapping_coverage.active_flag',[0,1])->where('00_mapping_coverage.organization_id', $organization_id);
        });
        
        $mapping_coverage = $mapping_coverage->get();
        return response()->json(array('data' => $mapping_coverage, 'request' => $request->all()));
    }

    public function getProvince(){
        $province = Province::where('active_flag',1)->get();

        return response()->json(array('data' => $province));
    }

    public function getRegency(Request $request){
        $regency = Regency::where('active_flag',1)->whereIn('provinsi_id', $request->input('province'))->get();

        return response()->json(array('data' => $regency));
    }

    public function getDistrict(Request $request){
        $district = District::where('active_flag',1)->whereIn('kabupaten_kota_id', $request->input('regency'))->get();

        return response()->json(array('data' => $district));
    }

    public function getVillage(Request $request){
        $village = Village::where('active_flag',1)->where('kecamatan_id', $request->input('district'))->get();

        return response()->json(array('data' => $village));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],41)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $organization = Organization::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['organization'] = $organization;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::mapping_coverage/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $province_coverage = $request->input('province_coverage_id');
        $regency_coverage = $request->input('regency_coverage_id');
        $district_coverage = $request->input('district_coverage_id');
        $village_coverage = $request->input('village_coverage_id');
        $mapping_coverage_array = array();
        for ($b=0; $b < count($province_coverage); $b++) { 
            $obj = array(
                "organization_id" => $request->input('organization_id'),
                "provinsi_id" => $province_coverage[$b],
                "kabupaten_kota_id" => (!isset($regency_coverage[$b]) ? null : $regency_coverage[$b]),
                "kecamatan_id" => (!isset($district_coverage[$b]) ? null : $district_coverage[$b]),
                "kelurahan_desa_id" => (!isset($village_coverage[$b]) ? null : $village_coverage[$b]),
                "active_flag" => "1"
            );

            array_push($mapping_coverage_array, $obj);
        }
        MappingCoverage::insert($mapping_coverage_array);
        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/mapping_coverage');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],41)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $mapping_coverage_by_id = MappingCoverage::where('mapping_coverage_id', $request->get('id'))->first();
        $organization = Organization::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['organization'] = $organization;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['mapping_coverage_by_id'] = $mapping_coverage_by_id;
        return view('admin::mapping_coverage/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = MappingCoverage::where('mapping_coverage_id', $request->get('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = MappingCoverage::where('mapping_coverage_id', $request->get('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $update = MappingCoverage::where('mapping_coverage_id', $request->get('id'))->update(['active_flag' => -1]);

        $alert = Lang::get('notification.has been deleted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been deleted',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }
}
