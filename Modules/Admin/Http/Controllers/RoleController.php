<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use Validator;
use App;
use Lang;
use App\Role;
use App\RoleType;
use App\Menu;
use App\RoleMenu;
use DB;
use Config;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],9)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::role/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/role');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $role_type = RoleType::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['role_type'] = $role_type;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],9)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::role/form',$data);
    }

    public function get(Request $request){
        $role = Role::where('role_id',$request->input('role_id'))->first();

        return response()->json(array('data' => $role));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'role_name' => ['required'],
            'role_description' => ['required']
        ]);

        if($validator->fails()){
            if($request->input('role_id') == null){
                Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }else{
                Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if($request->input('role_id') == null){
                $role_id = Role::insertGetId([
                    'role_name' => $request->input('role_name'),
                    'role_desc' => $request->input('role_description'),
                    'role_type_id' => $request->input('role_type') == null || $request->input('role_type') == "" ? null : $request->input('role_type'),
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                ]);
                
                $alert = Lang::get('notification.has been inserted', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

                if($role_id > 0){
                    Session::flash('redirect_to_edit',1);
                    $alert = Lang::get('notification.has been inserted', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
                }
                Session::flash('message_alert', $alert);
                return redirect('admin/role/edit/?id='.$role_id);
            }else{
                $update = Role::where('role_id',$request->input('role_id'))->update([
                    'role_name' => $request->input('role_name'),
                    'role_desc' => $request->input('role_description'),
                    'role_type_id' => $request->input('role_type') == null || $request->input('role_type') == "" ? null : $request->input('role_type'),
                    'updated_by' => Session::get('users')['id']
                ]);

                $alert = Lang::get('notification.has been updated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

                if($update){
                    $alert = Lang::get('notification.has been updated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
                }
                Session::flash('message_alert', $alert);
                return redirect('admin/role');
            }
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function menu_access(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'access_status' => ['required']
        ]);        

        if($validator->fails()){
            Session::flash('message_alert', Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]));
            Session::flash('error_role',1);
            return redirect()->back()->withErrors($validator);
        }else{
            $menu_access = array();
            for ($b=0; $b < count($request->input('access_status')); $b++) {
                $obj = array(
                    'role_id' => $request->input('role_id'),
                    'menu_id' => $request->input('access_status')[$b],
                    'allow_create' => 1,
                    'allow_update' => 1,
                    'allow_delete' => 1,
                    'allow_create' => 1,
                    'allow_print' => 1,
                    'allow_export' => 1,
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                );
                array_push($menu_access,$obj);
            }
            RoleMenu::where('role_id',$request->input('role_id'))->update(['active_flag' => 0, 'updated_by' => Session::get('users')['id']]);
            RoleMenu::insert($menu_access);
            Session::flash('message_alert', Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect()->back();
        }
        // exit;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/role');
        }
        $user = Session::get('users');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $role_type = RoleType::where('active_flag',1)->get();
        $data['role_type'] = $role_type;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $role_by_id = Role::where('role_id', $request->get('id'))->first();
        $all_menu_by_role = Menu::select('98_menu.*',DB::raw('(CASE WHEN `98_menu`.`menu_id` IN (SELECT `98_role_menu`.`menu_id` FROM `98_role_menu` WHERE `98_role_menu`.`active_flag` = 1 AND `98_role_menu`.`role_id` = '.$request->input('id').') THEN 1 ELSE 0 END) AS `access_status`'),DB::raw('(CASE WHEN 98_menu.parent_id IS NULL THEN 98_menu.orderno ELSE (SELECT dd.orderno FROM 98_menu AS dd WHERE dd.menu_id = 98_menu.parent_id) END) AS ordernum'))->where('98_menu.active_flag',1)->orderBy('ordernum','ASC')->orderBy('98_menu.parent_id','ASC')->where('98_menu.is_menu',1)->get()->toArray();
        $dashboard_special_features = Menu::select('98_menu.*',DB::raw('(CASE WHEN `98_menu`.`menu_id` IN (SELECT `98_role_menu`.`menu_id` FROM `98_role_menu` WHERE `98_role_menu`.`active_flag` = 1 AND `98_role_menu`.`role_id` = '.$request->input('id').') THEN 1 ELSE 0 END) AS `access_status`'),DB::raw('(CASE WHEN 98_menu.parent_id IS NULL THEN 98_menu.orderno ELSE (SELECT dd.orderno FROM 98_menu AS dd WHERE dd.menu_id = 98_menu.parent_id) END) AS ordernum'))->where('98_menu.active_flag',1)->orderBy('ordernum','ASC')->orderBy('98_menu.parent_id','ASC')->where('98_menu.is_menu',0)->get()->toArray();
        $accessed_menu_by_role = array_filter($all_menu_by_role, function($value){
            return $value['access_status'] == 1;
        });
        $accessed_menu_by_role = array_map(function($value){
            return $value['menu_id'];
        }, $accessed_menu_by_role);
        $accessed_menu_by_role = join(",", $accessed_menu_by_role);
        $data['user'] = $user;
        $data['role_by_id'] = $role_by_id;
        $data['all_menu_by_role'] = $all_menu_by_role;
        $data['accessed_menu_by_role'] = $accessed_menu_by_role;
        $data['dashboard_special_features'] = $dashboard_special_features;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],9)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::role/form',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Role::where('role_id',$request->get('id'))->update(['active_flag' => 1]);
        $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);
        
        if($update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/role');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Role::where('role_id',$request->get('id'))->update(['active_flag' => 0]);
        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

        if($update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/role');
    }
}
