<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use Illuminate\Support\Facades\Http;
use App\Voucher;
use App\VoucherType;
use App\User;
use App\Customer;
use App\Notification;
use App\Rules\VoucherCodeValidation;
use App\Rules\VoucherValueValidation;
use Validator;
use App;
use Lang;
use Config;
use DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::voucher/index', $data);
    }

    public function history()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['history'] = 1;
        return view('admin::voucher/index', $data);
    }

    public function history_detail(Request $request, $id)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;

        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $sql = Voucher::select('voucher_name','voucher_code','voucher_desc')->where('voucher_id','=',$id)->first();
        $data['voucher_id'] = $id;
        $data['voucher_name'] = $sql->voucher_name;
        $data['voucher_code'] = $sql->voucher_code;
        $data['voucher_desc'] = $sql->voucher_desc;

        return view('admin::voucher/history/detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/voucher');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $voucher_type = VoucherType::where('active_flag', 1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['voucher_type'] = $voucher_type;
        return view('admin::voucher/form', $data);
    }

    

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        // print_r($request->all());
        // exit;
        $validator = Validator::make($request->all(), [
            'voucher_name' => ['required'],
            'voucher_code' => ['required','unique:00_vouchers'],
            'voucher_start_date' => ['required'],
            'voucher_start_time' => ['required'],
            'voucher_end_date' => ['required'],
            'voucher_end_time' => ['required'],
            'voucher_type' => ['required'],
            'total_provided_voucher' => ['required'],
            'voucher_description' => ['required'],
            'min_price_requirement' => ['required'],
            'max_value_price' => ['required'],
            'voucher_value_type' => ['required'],
            'voucher_amount' => ['required', new VoucherValueValidation($request->input('voucher_value_type'))]
        ]);
        if($request->input('voucher_id') != null){
            $validator = Validator::make($request->all(), [
                'voucher_name' => ['required'],
                'voucher_code' => ['required', new VoucherCodeValidation($request->input('voucher_id'))],
                'voucher_start_date' => ['required'],
                'voucher_start_time' => ['required'],
                'voucher_end_date' => ['required'],
                'voucher_end_time' => ['required'],
                'voucher_type' => ['required'],
                'total_provided_voucher' => ['required'],
                'voucher_description' => ['required'],
                'min_price_requirement' => ['required'],
                'max_value_price' => ['required'],
                'voucher_value_type' => ['required'],
                'voucher_amount' => ['required', new VoucherValueValidation($request->input('voucher_value_type'))]
            ]);
        }

        if($validator->fails()){
            $alert = Lang::get('notification.has been inserted',['result' => Lang::get('notification.inaccurately')]);
            if($request->input('voucher_id') != null){
                $alert = Lang::get('notification.has been updated',['result' => Lang::get('notification.inaccurately')]);
            }
            Session::flash('message_alert', $alert);
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $alert = "";
            if($request->input('voucher_id') == null){
                $voucher_id = Voucher::insertGetId([
                    'voucher_name' => $request->input('voucher_name'),
                    'voucher_code' => strtoupper($request->input('voucher_code')),
                    'voucher_type_id' => $request->input('voucher_type'),
                    'starts_at' => date('Y-m-d',strtotime($request->input('voucher_start_date'))).' '.$request->input('voucher_start_time'),
                    'expires_at' => date('Y-m-d',strtotime($request->input('voucher_end_date'))).' '.$request->input('voucher_end_time'),
                    'max_uses' => $request->input('total_provided_voucher'),
                    'min_price_requirement' => str_replace(".","",$request->input('min_price_requirement')),
                    'max_value_price' => str_replace(".","",$request->input('max_value_price')),
                    'is_fixed' => $request->input('voucher_value_type'),
                    'amount' => str_replace(".","",$request->input('voucher_amount')),
                    'voucher_desc' => $request->input('voucher_description'),
                    'created_by' => Session::get('users')['id']
                ]);
                
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($voucher_id > 0){
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                }

                $customer = Customer::select('customer_id',DB::raw('0 AS is_agent'),'fcm_token')->where('active_flag',1)->whereNotNull('fcm_token');
                $user = User::select('user_id AS customer_id',DB::raw('1 AS is_agent'),'fcm_token')->where('active_flag',1)->whereNotNull('fcm_token');
                $users = $user->union($customer)->get();
                $user_list = array();
                $user_data = array();
                $title = "Ada Promo Menguntungkan!";
                $message = "Hi, KuyMarketers! Ada kabar menyenangkan bagi kalian semua! Bagi kalian yang melakukan pembelanjaan dari: ".($request->input('voucher_start_date').' '.$request->input('voucher_start_time'))." hingga: ".($request->input('voucher_end_date').' '.$request->input('voucher_end_time')).". Anda mendapat potongan sebesar ".$request->input('voucher_amount').($request->input("is_fixed") == 1 ? "" : ("% dengan minimal belanja Rp.".$request->input('min_price_requirement')." dan maksimal potongan Rp. ".$request->input("max_value_price"))).". Buruan segera belanja dengan kode promo: ".$request->input("voucher_code")." sebelum kehabisan.";
                for ($b=0; $b < count($users); $b++) { 
                    $obj = array(
                        "customer_id" => $users[$b]['is_agent'] == 0 ? $users[$b]['customer_id'] : null,
                        "user_id" => $users[$b]['is_agent'] == 1 ? $users[$b]['customer_id'] : null,
                        "message" => $message,
                        "voucher_id" => $voucher_id,
                        "type" => "promo"
                    );

                    array_push($user_list, $users[$b]['fcm_token']);
                    array_push($user_data, $obj);
                }

                if(count($user_list) > 0){
                    $url = Config::get('fcm.url');
                    $server_key = Config::get('fcm.token');
                    $headers = array(
                        'Content-Type'=>'application/json',
                        'Authorization'=>'key='.$server_key
                    );

                    $parameter = array(
                        "registration_ids" => $user_list,
                        "notification" => array(
                            "title" => $title,
                            "body" => $message,
                            "message" => "message",
                            "sound" => "default",
                            "badge" => "1",
                            "icon" => "@mipmap/ic_stat_ic_notification"
                        ),
                        "data" => array(
                            "target" => "MainActivity",
                            "notifId" => "1",
                            "dataId" => "1"
                        )
                    );
                    $response = Http::withHeaders($headers)->post($url, $parameter);

                    if($response['success'] >= 1){
                        Notification::insert($user_data);
                    }
                }
            }else{
                $update = Voucher::where('voucher_id', $request->input('voucher_id'))->update([
                    'voucher_name' => $request->input('voucher_name'),
                    'voucher_code' => strtoupper($request->input('voucher_code')),
                    'voucher_type_id' => $request->input('voucher_type'),
                    'starts_at' => date('Y-m-d',strtotime($request->input('voucher_start_date'))).' '.$request->input('voucher_start_time'),
                    'expires_at' => date('Y-m-d',strtotime($request->input('voucher_end_date'))).' '.$request->input('voucher_end_time'),
                    'max_uses' => $request->input('total_provided_voucher'),
                    'min_price_requirement' => str_replace(".","",$request->input('min_price_requirement')),
                    'max_value_price' => str_replace(".","",$request->input('max_value_price')),
                    'is_fixed' => $request->input('voucher_value_type'),
                    'amount' => str_replace(".","",$request->input('voucher_amount')),
                    'voucher_desc' => $request->input('voucher_description')
                ]);
                
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update > 0){
                    Voucher::where('voucher_id', $request->input('voucher_id'))->update([
                        'updated_by' => Session::get('users')['id']
                    ]);
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);

                    $customer = Customer::select('customer_id',DB::raw('0 AS is_agent'),'fcm_token')->where('active_flag',1)->whereNotNull('fcm_token');
                    $user = User::select('user_id AS customer_id',DB::raw('1 AS is_agent'),'fcm_token')->where('active_flag',1)->whereNotNull('fcm_token');
                    $users = $user->union($customer)->get();
                    $user_list = array();
                    $user_data = array();
                    $title = "Update Promo Menguntungkan!";
                    $message = "Hi, KuyMarketers! Ada kabar menyenangkan bagi kalian semua! Bagi kalian yang melakukan pembelanjaan dari: ".($request->input('voucher_start_date').' '.$request->input('voucher_start_time'))." hingga: ".($request->input('voucher_end_date').' '.$request->input('voucher_end_time')).". Anda mendapat potongan sebesar ".$request->input('voucher_amount').($request->input("is_fixed") == 1 ? "" : ("% dengan minimal belanja Rp.".$request->input('min_price_requirement')." dan maksimal potongan Rp. ".$request->input("max_value_price"))).". Buruan segera belanja dengan kode promo: ".$request->input("voucher_code")." sebelum kehabisan.";
                    for ($b=0; $b < count($users); $b++) { 
                        $obj = array(
                            "customer_id" => $users[$b]['is_agent'] == 0 ? $users[$b]['customer_id'] : null,
                            "user_id" => $users[$b]['is_agent'] == 1 ? $users[$b]['customer_id'] : null,
                            "message" => $message,
                            "voucher_id" => $request->input('voucher_id'),
                            "title" => $title,
                            "type" => "promo"
                        );

                        array_push($user_list, $users[$b]['fcm_token']);
                        array_push($user_data, $obj);
                    }

                    if(count($user_list) > 0){
                        $url = Config::get('fcm.url');
                        $server_key = Config::get('fcm.token');
                        $headers = array(
                            'Content-Type'=>'application/json',
                            'Authorization'=>'key='.$server_key
                        );
                        
                        $parameter = array(
                            "registration_ids" => $user_list,
                            "notification" => array(
                                "title" => $title,
                                "body" => $message,
                                "message" => "message",
                                "sound" => "default",
                                "badge" => "1",
                                "icon" => "@mipmap/ic_stat_ic_notification"
                            ),
                            "data" => array(
                                "target" => "MainActivity",
                                "notifId" => "1",
                                "dataId" => "1"
                            )
                        );
                        $response = Http::withHeaders($headers)->post($url, $parameter);

                        if($response['success'] >= 1){
                            Notification::insert($user_data);
                        }
                    }
                }
            }
            Session::flash('message_alert', $alert);
            return redirect('admin/voucher');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/voucher');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $voucher_type = VoucherType::where('active_flag', 1)->get();
        $voucher_by_id = Voucher::where('voucher_id', $request->get('id'))->first();
        // print_r($voucher_by_id);
        // exit;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $voucher_by_id_temp = array(
            "voucher_id" => $voucher_by_id['voucher_id'],
            "voucher_code" => $voucher_by_id['voucher_code'],
            "voucher_name" => $voucher_by_id['voucher_name'],
            "voucher_description" => $voucher_by_id['voucher_desc'],
            "total_provided_voucher" => $voucher_by_id['max_uses'],
            "min_price_requirement" => $voucher_by_id['min_price_requirement'],
            "max_value_price" => $voucher_by_id['max_value_price'],
            "voucher_value_type" => $voucher_by_id['is_fixed'],
            "voucher_amount" => $voucher_by_id['amount'],
            "voucher_type_id" => $voucher_by_id['voucher_type_id'],
            "voucher_start_date" => date("Y-m-d",strtotime($voucher_by_id['starts_at'])),
            "voucher_start_time" => date("H:i:s",strtotime($voucher_by_id['starts_at'])),
            "voucher_end_date" => date("Y-m-d",strtotime($voucher_by_id['expires_at'])),
            "voucher_end_time" => date("H:i:s",strtotime($voucher_by_id['expires_at']))
        );
        // print_r($voucher_by_id_temp);
        // exit;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['voucher_type'] = $voucher_type;
        $data['voucher_by_id'] = $voucher_by_id_temp;
        return view('admin::voucher/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Voucher::where('voucher_id', $request->get('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Voucher::where('voucher_id', $request->get('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }
}
