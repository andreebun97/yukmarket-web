<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Currency;
use App\Helpers\UomConversion;
use Intervention\Image\Facades\Image;
use App;
use App\GlobalSettings;
use Lang;
use DB;
use Validator;
use Session;
use App\PriceType;
use Config;

class GlobalSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        // $uom_conversion = new UomConversion();
        // $uom_conversion = $uom_conversion->convert("kg","hg");
        // print_r($uom_conversion);
        // exit;
        
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],24)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $company_name = GlobalSettings::where('global_parameter_name','company_name')->first();
        $company_address = GlobalSettings::where('global_parameter_name','company_address')->first();
        $company_province = GlobalSettings::where('global_parameter_name','company_province')->first();
        $company_regency = GlobalSettings::where('global_parameter_name','company_regency')->first();
        $company_district = GlobalSettings::where('global_parameter_name','company_district')->first();
        $company_village = GlobalSettings::where('global_parameter_name','company_village')->first();
        // $company_name = GlobalSettings::where('global_parameter_name','company_name')->first();
        $pricing_include_tax = GlobalSettings::where('global_parameter_name','pricing_include_tax')->first();
        $tax_value = GlobalSettings::where('global_parameter_name','tax_value')->first();
        $implemented_price_type = GlobalSettings::where('global_parameter_name','implemented_price_type')->first();
        $tokopedia = GlobalSettings::where('global_parameter_name','tokopedia')->first();
        $whatsapp_customer_service = GlobalSettings::where('global_parameter_name','whatsapp_customer_service')->first();
        $schedule_config = GlobalSettings::where('global_parameter_name','schedule_config')->first();
        $user_token_integrator = GlobalSettings::where('global_parameter_name','user_token_integrator')->first();

        $global_settings = array(
            'company_name' => $company_name['global_parameter_value'] == null ? null : $company_name['global_parameter_value'],
            'company_address' => $company_address['global_parameter_value'] == null ? null : $company_address['global_parameter_value'],
            'company_province' => $company_province['global_parameter_value'] == null ? null : $company_province['global_parameter_value'],
            'company_regency' => $company_regency['global_parameter_value'] == null ? null : $company_regency['global_parameter_value'],
            'company_district' => $company_district['global_parameter_value'] == null ? null : $company_district['global_parameter_value'],
            'company_village' => $company_village['global_parameter_value'] == null ? null : $company_village['global_parameter_value'],
            'company_logo' => null,
            'pricing_include_tax' => $pricing_include_tax['global_parameter_value'] == null ? null : $pricing_include_tax['global_parameter_value'],
            'tax_value' => $tax_value['global_parameter_value'] == null ? null : $tax_value['global_parameter_value'],
            'implemented_price_type' => $implemented_price_type['global_parameter_value'] == null ? null : $implemented_price_type['global_parameter_value'],
            'tokopedia' => $tokopedia['global_parameter_value'] == null ? null : $tokopedia['global_parameter_value'],
            'whatsapp_customer_service' => $whatsapp_customer_service['global_parameter_value'] == null ? null : $whatsapp_customer_service['global_parameter_value'],
            'schedule_config' => $schedule_config['global_parameter_value'] == null ? null : $schedule_config['global_parameter_value'],
            'user_token_integrator' => $user_token_integrator['global_parameter_value'] == null ? null : $user_token_integrator['global_parameter_value']
        );
        // print_r($global_settings);
        // exit;
        $price_type = PriceType::whereNull('parent_id')->where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['global_settings'] = $global_settings;
        $data['price_type'] = $price_type;
        return view('admin::settings', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $data = GlobalSettings::count();
        // echo $data;
        // exit;
        $validator = Validator::make($request->all(),[
            'company_name' => ['required'],
            'company_address' => ['required'],
            // 'company_province' => ['required'],
            // 'company_regency' => ['required'],
            // 'company_district' => ['required'],
            // 'company_village' => ['required'],
            'pricing_include_tax' => ['required'],
            // 'tax_value' => ['required','max:100'],
            // 'implemented_price_type' => ['required']
        ]);

        if($data == 0){
            $validator = Validator::make($request->all(),[
                'company_name' => ['required'],
                'company_address' => ['required'],
                // 'company_province' => ['required'],
                // 'company_regency' => ['required'],
                // 'company_district' => ['required'],
                // 'company_village' => ['required'],
                'company_logo' => ['required', 'mimes:jpeg,bmp,png,jpg,pneg'],
                'pricing_include_tax' => ['required'],
                // 'tax_value' => ['required','max:100'],
                // 'implemented_price' => ['required']
            ]);
        }

        if($request->input('company_logo') != null){
            $validator = Validator::make($request->all(),[
                'company_name' => ['required'],
                'company_address' => ['required'],
                // 'company_province' => ['required'],
                // 'company_regency' => ['required'],
                // 'company_district' => ['required'],
                // 'company_village' => ['required'],
                'pricing_include_tax' => ['required'],
                'company_logo' => ['required','mimes:jpeg,bmp,png,jpg,pneg']
                // 'tax_value' => ['required','max:100'],
                // 'implemented_price_type' => ['required']
            ]);
        }
        
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $field_array = array(
                array(
                    'global_parameter_name' => 'company_name',
                    'global_parameter_value' => $request->input('company_name'),
                    'global_parameter_desc' => "Company Name",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_address',
                    'global_parameter_value' => $request->input('company_address'),
                    'global_parameter_desc' => "Company Address",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_province',
                    'global_parameter_value' => $request->input('company_province'),
                    'global_parameter_desc' => "Company Province",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_regency',
                    'global_parameter_value' => $request->input('company_regency'),
                    'global_parameter_desc' => "Company Regency",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_district',
                    'global_parameter_value' => $request->input('company_district'),
                    'global_parameter_desc' => "Company District",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_village',
                    'global_parameter_value' => $request->input('company_village'),
                    'global_parameter_desc' => "Company Village",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'company_logo',
                    'global_parameter_value' => null,
                    'global_parameter_desc' => "Company Logo",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'pricing_include_tax',
                    'global_parameter_value' => $request->input('pricing_include_tax'),
                    'global_parameter_desc' => "Pricing Include Tax",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'tax_value',
                    'global_parameter_value' => ($request->input('pricing_include_tax') ==  1 ? 0 : $request->input('tax_value') / 100),
                    'global_parameter_desc' => "Tax Value",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'implemented_price_type',
                    'global_parameter_value' => $request->input('implemented_price_type'),
                    'global_parameter_desc' => "Implemented Price Type",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'tokopedia',
                    'global_parameter_value' => $request->input('tokopedia'),
                    'global_parameter_desc' => "Scan Tokopedia",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'whatsapp_customer_service',
                    'global_parameter_value' => $request->input('whatsapp_customer_service'),
                    'global_parameter_desc' => "WhatsApp Customer Service",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'schedule_config',
                    'global_parameter_value' => $request->input('schedule_config'),
                    'global_parameter_desc' => "Schedule Config",
                    'created_by' => Session::get('users')['id']
                ),
                array(
                    'global_parameter_name' => 'user_token_integrator',
                    'global_parameter_value' => $request->input('user_token_integrator'),
                    'global_parameter_desc' => "User Token Integrator",
                    'created_by' => Session::get('users')['id']
                )
            );
            if($request->file('company_logo') != null){
                $folder_name = 'img/uploads/logo/';
                $file = $request->file('company_logo');
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid().'.'.$ext;
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
                $field_array = array(
                    array(
                        'global_parameter_name' => 'company_name',
                        'global_parameter_value' => $request->input('company_name'),
                        'global_parameter_desc' => "Company Name",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_address',
                        'global_parameter_value' => $request->input('company_address'),
                        'global_parameter_desc' => "Company Address",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_province',
                        'global_parameter_value' => $request->input('company_province'),
                        'global_parameter_desc' => "Company Province",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_regency',
                        'global_parameter_value' => $request->input('company_regency'),
                        'global_parameter_desc' => "Company Regency",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_district',
                        'global_parameter_value' => $request->input('company_district'),
                        'global_parameter_desc' => "Company District",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_village',
                        'global_parameter_value' => $request->input('company_village'),
                        'global_parameter_desc' => "Company Village",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'company_logo',
                        'global_parameter_value' => $folder_name.$name,
                        'global_parameter_desc' => "Company Logo",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'pricing_include_tax',
                        'global_parameter_value' => $request->input('pricing_include_tax'),
                        'global_parameter_desc' => "Pricing Include Tax",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'tax_value',
                        'global_parameter_value' => $request->input('tax_value') / 100,
                        'global_parameter_desc' => "Tax Value",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'implemented_price_type',
                        'global_parameter_value' => $request->input('implemented_price_type'),
                        'global_parameter_desc' => "Implemented Price Type",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'tokopedia',
                        'global_parameter_value' => $request->input('tokopedia'),
                        'global_parameter_desc' => "Scan Tokopedia",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'whatsapp_customer_service',
                        'global_parameter_value' => $request->input('whatsapp_customer_service'),
                        'global_parameter_desc' => "WhatsApp Customer Service",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'schedule_config',
                        'global_parameter_value' => $request->input('schedule_config'),
                        'global_parameter_desc' => "Schedule Config",
                        'created_by' => Session::get('users')['id']
                    ),
                    array(
                        'global_parameter_name' => 'user_token_integrator',
                        'global_parameter_value' => $request->input('user_token_integrator'),
                        'global_parameter_desc' => "User Token Integrator",
                        'created_by' => Session::get('users')['id']
                    )
                );
            }
            $alert = "";
            if($data == 0){               
                $insert = GlobalSettings::insert($field_array);
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
            }else{
                // GlobalSettings::find(collect($field_array)->pluck('global_parameter_name')->toArray())->map(function($item, $key) use ($field_array){
                //     $item['global_parameter_value'] = $data[$key]['global_parameter_value'];
                //     return $item->save();
                // });
                GlobalSettings::where('global_parameter_name','company_name')->update(['global_parameter_value' => $field_array[0]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','company_address')->update(['global_parameter_value' => $field_array[1]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','company_province')->update(['global_parameter_value' => $field_array[2]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','company_regency')->update(['global_parameter_value' => $field_array[3]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','company_district')->update(['global_parameter_value' => $field_array[4]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','company_village')->update(['global_parameter_value' => $field_array[5]['global_parameter_value']]);
                if($request->file('company_logo') != null){
                    GlobalSettings::where('global_parameter_name','company_logo')->update(['global_parameter_value' => $field_array[6]['global_parameter_value']]);
                }
                GlobalSettings::where('global_parameter_name','pricing_include_tax')->update(['global_parameter_value' => $field_array[7]['global_parameter_value']]);
                GlobalSettings::where('global_parameter_name','tax_value')->update(['global_parameter_value' => $field_array[8]['global_parameter_value']]);
                $implemented_price = GlobalSettings::where('global_parameter_name','implemented_price_type')->first();
                if($implemented_price != null){
                    GlobalSettings::where('global_parameter_name','implemented_price_type')->update(['global_parameter_value' => $field_array[9]['global_parameter_value'] == "" ? null : $field_array[9]['global_parameter_value']]);
                }else{
                    if($request->input('implemented_price_type') != ""){
                        GlobalSettings::insert(['global_parameter_value' => $field_array[9]['global_parameter_value'],'global_parameter_name' => 'implemented_price_type']);
                    }
                }
                $tokopedia = GlobalSettings::where('global_parameter_name','tokopedia')->first();
                if($tokopedia != null){
                    GlobalSettings::where('global_parameter_name','tokopedia')->update(['global_parameter_value' => $field_array[10]['global_parameter_value'] == "" ? null : $field_array[10]['global_parameter_value']]);
                }else{
                    if($request->input('tokopedia') != ""){
                        GlobalSettings::insert(['global_parameter_value' => $field_array[10]['global_parameter_value'],'global_parameter_name' => 'tokopedia']);
                    }
                }
                $whatsapp_customer_service = GlobalSettings::where('global_parameter_name','whatsapp_customer_service')->first();
                if($whatsapp_customer_service != null){
                    GlobalSettings::where('global_parameter_name','whatsapp_customer_service')->update(['global_parameter_value' => $field_array[11]['global_parameter_value'] == "" ? null : $field_array[11]['global_parameter_value']]);
                }else{
                    if($request->input('whatsapp_customer_service') != ""){
                        GlobalSettings::insert(['global_parameter_value' => $field_array[11]['global_parameter_value'],'global_parameter_name' => 'whatsapp_customer_service']);
                    }
                }
                $schedule_config = GlobalSettings::where('global_parameter_name','schedule_config')->first();
                if($schedule_config != null){
                    GlobalSettings::where('global_parameter_name','schedule_config')->update(['global_parameter_value' => $field_array[12]['global_parameter_value'] == "" ? null : $field_array[12]['global_parameter_value']]);
                }else{
                    if($request->input('schedule_config') != ""){
                        GlobalSettings::insert(['global_parameter_value' => $field_array[12]['global_parameter_value'],'global_parameter_name' => 'schedule_config']);
                    }
                }
                $user_token_integrator = GlobalSettings::where('global_parameter_name','schedule_config')->first();
                if($user_token_integrator != null){
                    GlobalSettings::where('global_parameter_name','user_token_integrator')->update(['global_parameter_value' => $field_array[13]['global_parameter_value'] == "" ? null : $field_array[13]['global_parameter_value']]);
                }else{
                    if($request->input('user_token_integrator') != ""){
                        GlobalSettings::insert(['global_parameter_value' => $field_array[13]['global_parameter_value'],'global_parameter_name' => 'user_token_integrator']);
                    }
                }
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                // GlobalSettings::where('global_parameter_name','company_name')->update(['global_parameter_value' => $request->input('company_name')]);
            }
            Session::flash('message_alert', $alert);
            return redirect()->back();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
