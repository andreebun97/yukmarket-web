<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\Helpers\Currency;
use App\Helpers\PhoneNumber;
use App;
use Lang;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Shipment;
use App\Order;
use App\User;
use App\OrderHistory;
use App\Preparation;
use App\OrderStatus;
use App\OrderDetail;
use App\AddressDetail;
use App\Notification;
use Validator;
use DB;
use Mail;
use Config;
use App\Product;
use App\Helpers\UomConvert;


class PickingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $phone_number = new PhoneNumber();
        $this->currency = $currency;
        $this->phone_number = $phone_number;
    }

    public function index(Request $request)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $pickers = User::join('98_user_role','98_user_role.user_id','=','98_user.user_id')->join('98_role','98_role.role_id','=','98_user_role.role_id')->where('98_role.role_type_id',5)->where('98_user.active_flag',1)->get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        $data['pickers'] = $pickers;
        return view('admin::picking/index', $data);
    }

    public function pickingNotification(Request $request)
    {
        Notification::where('notification_id', '=', $request->id)->update(['read_by_cms' => 1]);
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $all_menu = $user_menu->get($user['id']);
        $data['logo'] = Config::get('logo.menubar');
        $data['favicon'] = Config::get('logo.favicon');
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        return view('admin::picking/index', $data);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function form(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/transaction');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $string_query = 'SELECT
                            *
                        FROM
                            `10_order_detail`
                        INNER JOIN `00_product` ON `00_product`.`prod_id` = `10_order_detail`.`prod_id`
                        LEFT JOIN `00_uom` ON `00_uom`.`uom_id` = `10_order_detail`.`uom_id`
                        LEFT JOIN `00_inventory` ON ((`00_inventory`.`prod_id` = `00_product`.`prod_id`
                        AND `00_product`.`stockable_flag` = "1")
                        OR (`00_inventory`.`prod_id` = `00_product`.`parent_prod_id`
                        AND `00_product`.`stockable_flag` = "0"))
                        AND `00_inventory`.`warehouse_id` = `10_order_detail`.`warehouse_id`
                        AND `00_inventory`.`organization_id` = "1"
                        WHERE
                            `order_id` = "'.$request->get('id').'"';

        $data_trans = DB::select(DB::raw($string_query));
        // dd($data_trans);
        $transactions = array();
        foreach ($data_trans as $trans) {
            if (isset($trans->parent_prod_id)) {
                $get_uom_prod = Product::select('00_product.prod_id','00_product.uom_id','00_product.uom_value')
                            ->where('00_product.prod_id',$trans->parent_prod_id)
                            ->first();
                $uom_id = $get_uom_prod->uom_id;
                $uom_value = $get_uom_prod->uom_value;
                if ($uom_id == $trans->uom_id) {
                    $stock_av = $trans->stock;
                    $stock = $trans->stock;
                    $stock_av_uom = '( '.$stock_av.' '.$trans->uom_name.' )';

                    $qty = $trans->quantity;
                    $qty_uom = '( '.$qty.' '.$trans->uom_name.' )';
                } else {
                    $convert = new UomConvert;
                    $stock_av_uom = $convert->convertToStock($uom_id, $uom_value, '2', '1');
                    $qty_uom = $convert->convertToStock($trans->uom_id, $trans->uom_value, '2', '1');
                    if (!isset($stock_av_uom['flag']) or !isset($qty_uom['flag'])) {
                        $stock_av = $trans->stock*$stock_av_uom;
                        $stock = $trans->stock*$stock_av_uom/$trans->uom_value;
                        $stock_av_uom = '( '.$stock_av.' Gram )';

                        $qty = $trans->quantity*$qty_uom;
                        $qty_uom = '( '.$qty.' Gram )';
                    } else {
                        $stock_av = $trans->stock;
                        $stock = $trans->stock;
                        $stock_av_uom = '( '.$stock_av.' '.$trans->uom_name.' )';

                        $qty = $trans->quantity;
                        $qty_uom = '( '.$qty.' '.$trans->uom_name.' )';
                    }
                }
                
            } else {
                $uom_id = $trans->uom_id;
                $uom_value = $trans->uom_value;

                $stock_av = $trans->stock;
                $stock = $trans->stock;
                $stock_av_uom = '( '.$stock_av.' '.$trans->uom_name.' )';

                $qty = $trans->quantity;
                $qty_uom = '( '.$qty.' '.$trans->uom_name.' )';
            }

            

            $res_trans = (object)array(
                "prod_image" => $trans->prod_image,
                "quantity" => $trans->quantity,
                "stock" => $stock,
                "stock_av_uom" => $stock_av_uom,
                "qty_uom" => $qty_uom,
                "price" => $trans->price,
                "prod_name" => $trans->prod_name,
                "sku_status" => $trans->sku_status,
                "uom_value" => $trans->uom_value,
                "uom_name" => $trans->uom_name,
                "prod_code" => $trans->prod_code
            );
            array_push($transactions, $res_trans);
        }
        // $transactions = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')
        //         ->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')
        //         ->leftJoin('00_inventory', function($join){
        //                                  $join->on('00_inventory.prod_id', '=', '00_product.prod_id')->on('00_product.stockable_flag', '=', DB::raw("'1'"))->orOn('00_inventory.prod_id', '=', '00_product.parent_prod_id')->on('00_product.stockable_flag', '=', DB::raw("'0'"));
        //                                  $join->on('00_inventory.warehouse_id', '=', '10_order_detail.warehouse_id');
        //                                  $join->on('00_inventory.organization_id', '=', DB::raw("'1'"));
        //                              })
        //         ->where('order_id', $request->get('id'))
        //         ->get();
        
        $disabled_picker = 0;
        foreach ($transactions as $trans) {
            if ($trans->quantity > $trans->stock) {
                $disabled_picker = 1;
            }
            break;
        }
        $order_master = Order::select('10_order.order_id',
                '10_order.buyer_user_id',
                '10_order.is_printed_label',
                '10_order.order_code',
                '10_order.order_date',
                '10_order.admin_fee',
                '10_order.admin_fee_percentage',
                '10_order.transfer_date',
                '10_order.pricing_include_tax',
                '10_order.national_income_tax',
                '10_order.is_agent',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.shipment_price',
                '10_order.agent_user_id',
                '00_address.address_name',
                '00_address.address_info',
                '00_address.contact_person',
                '00_address.phone_number',
                '10_order.is_fixed',
                '10_order.voucher_amount',
                '10_order.destination_address',
                '00_address.address_detail',
                '00_kabupaten_kota.kabupaten_kota_name',
                '10_order.midtrans_transaction_id',
                '00_kecamatan.kecamatan_name',
                '00_kelurahan_desa.kelurahan_desa_name',
                '00_kelurahan_desa.kode_pos',
                '10_order.max_price',
                '10_order.voucher_id',
                '10_order.min_price',
                '10_order.invoice_status_id',
                '00_invoice_status.invoice_status_name',
                '10_order.order_status_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'00_order_status.order_status_name',
                '10_order.payment_method_id',
                '00_payment_method.payment_method_name', DB::raw('(SELECT 10_preparation.preparation_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS preparation_id'), DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS picker_name'), '10_order.is_confirmed', DB::raw('(SELECT 98_user.user_id FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS picker_id'))
                ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
                ->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
                ->where('10_order.order_id', $request
                    ->get('id'))
                ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
                ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
                ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
                ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
                ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
                ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
                ->first();
        $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_customer','00_customer.customer_id','=','00_address.customer_id')->join('10_order','10_order.buyer_user_id','=','00_customer.customer_id')->where('order_id', $request->get('id'))->get();
        $preparation = Preparation::where('order_id', $request->get('id'))->orderBy('preparation_status_id','DESC')->first();
        $pickers = User::join('98_user_role','98_user_role.user_id','=','98_user.user_id')->join('98_role','98_role.role_id','=','98_user_role.role_id')->where('98_role.role_type_id',5)->where('98_user.active_flag',1)->get();
        // $verification_steps = array("Pesanan Diterima","Pembayaran Diterima","Verifikasi","Picking","Quality Control","Packing","Delivery","Pesanan Selesai");
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['transactions'] = $transactions;
        $data['order_master'] = $order_master;
        $data['currency'] = $this->currency;
        $data['preparation'] = $preparation;
        $data['pickers'] = $pickers;
        $data['address_detail'] = $address_detail;
        $data['phone_number'] = $this->phone_number;
        $data['disabled_picker'] = $disabled_picker;
        return view('admin::picking/form', $data);
    }

    public function requestPickup(Request $request){
        $request_pickup = app('App\Http\Controllers\OrderTokopediaController')->requestPickup($request->input('order_id'));

        // print_r($request_pickup);
        $alert = Lang::get('notification.has been accepted',['result' => Lang::get('notification.inaccurately')]);
        $tokopedia_error_list = [1];
        if($request_pickup->isSuccess == true){
            $alert = Lang::get('notification.has been accepted',['result' => Lang::get('notification.successfully')]);
            $tokopedia_error_list = [];
        }
        Session::flash('message_alert', $alert);
        if(count($tokopedia_error_list) > 0){
            Session::flash('tokopedia_error_list',$tokopedia_error_list);
        }
        return redirect()->back();
    }

    public function acceptTokpedOrder(Request $request){
        $tokopedia_order = app('App\Http\Controllers\OrderTokopediaController')->acceptOrder($request->get('id'));

        // print_r($tokopedia_order);
        $alert = Lang::get('notification.has been accepted',['result' => Lang::get('notification.inaccurately')]);
        $tokopedia_error_list = [1];
        if($tokopedia_order->isSuccess == true){
            $alert = Lang::get('notification.has been accepted',['result' => Lang::get('notification.successfully')]);
            $tokopedia_error_list = [];
        }
        Session::flash('message_alert', $alert);
        if(count($tokopedia_error_list) > 0){
            Session::flash('tokopedia_error_list',$tokopedia_error_list);
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function multiple_label(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/picking');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],19)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $purchased_products = array();
        $purchased_products_array = array();
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order_detail.sku_status','10_order_detail.uom_value','10_order.shipping_receipt_label','10_order_detail.bruto','10_payment.invoice_no','00_product_sku.product_sku_name','00_uom.uom_name','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.shipping_receipt_num','10_order.buyer_name','10_order.buyer_address','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','00_organization.organization_logo','00_organization.organization_type_id','10_order.pricing_include_tax','10_order.national_income_tax','10_order.shipment_method_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_logo FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_logo FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_logo'),DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','10_order_detail.promo_value','00_vouchers.voucher_name','00_vouchers.voucher_code',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_address.contact_person','00_address.phone_number','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name',DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS picker_name'))->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->leftJoin('00_product_sku','00_product_sku.product_sku_id','=','00_product.product_sku_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->whereIn('10_order.order_id',explode(',',$request->get('id')))->get()->toArray();
        }
        foreach($purchased_products as $pp){ 
            $purchased_products_array[$pp['order_id']]['order_id'] = $pp['order_id'];
            $purchased_products_array[$pp['order_id']]['order_code'] = $pp['order_code'];
            $purchased_products_array[$pp['order_id']]['order_status_name'] = $pp['order_status_name'];
            $purchased_products_array[$pp['order_id']]['picker_name'] = $pp['picker_name'];
            $purchased_products_array[$pp['order_id']]['shipment_method_id'] = $pp['shipment_method_id'];
            $purchased_products_array[$pp['order_id']]['shipment_method_name'] = $pp['shipment_method_name'];
            $purchased_products_array[$pp['order_id']]['shipment_price'] = $pp['shipment_price'];
            $purchased_products_array[$pp['order_id']]['destination_address'] = $pp['destination_address'];
            $purchased_products_array[$pp['order_id']]['shipping_receipt_num'] = $pp['shipping_receipt_num'];
            $purchased_products_array[$pp['order_id']]['invoice_no'] = $pp['invoice_no'];
            $purchased_products_array[$pp['order_id']]['organization_type_id'] = $pp['organization_type_id'];
            $purchased_products_array[$pp['order_id']]['shipping_receipt_label'] = $pp['shipping_receipt_label'];
            $purchased_products_array[$pp['order_id']]['product_sku_name'] = $pp['product_sku_name'];
            $purchased_products_array[$pp['order_id']]['organization_logo'] = $pp['organization_logo'];
            $purchased_products_array[$pp['order_id']]['shipment_logo'] = $pp['shipment_logo'];
            $purchased_products_array[$pp['order_id']]['address_name'] = $pp['address_name'];
            $purchased_products_array[$pp['order_id']]['address_info'] = $pp['address_info'];
            $purchased_products_array[$pp['order_id']]['customer_name'] = $pp['customer_name'];
            $purchased_products_array[$pp['order_id']]['customer_email'] = $pp['customer_email'];
            $purchased_products_array[$pp['order_id']]['customer_phone_number'] = $pp['customer_phone_number'];
            $purchased_products_array[$pp['order_id']]['address_detail'] = $pp['address_detail'];
            $purchased_products_array[$pp['order_id']]['kabupaten_kota_name'] = $pp['kabupaten_kota_name'];
            $purchased_products_array[$pp['order_id']]['kecamatan_name'] = $pp['kecamatan_name'];
            $purchased_products_array[$pp['order_id']]['kelurahan_desa_name'] = $pp['kelurahan_desa_name'];
            $purchased_products_array[$pp['order_id']]['kode_pos'] = $pp['kode_pos'];
            $purchased_products_array[$pp['order_id']]['contact_person'] = $pp['contact_person'];
            $purchased_products_array[$pp['order_id']]['phone_number'] = $pp['phone_number'];
            $purchased_products_array[$pp['order_id']]['detail'][] = array(
                'sku_status' => $pp['sku_status'],
                'uom_value' => $pp['uom_value'],
                'uom_name' => $pp['uom_name'],
                'purchased_price' => $pp['purchased_price'],
                'purchased_quantity' => $pp['purchased_quantity'],
                'purchased_product_name' => $pp['purchased_product_name'],
                'promo_value' => $pp['promo_value'],
                'product_sku_name' => $pp['product_sku_name'],
                'bruto' => $pp['bruto']
            );
            $total_bruto = array_map(function($value){
                return $value['bruto'];
            }, $purchased_products_array[$pp['order_id']]['detail']);
            $total_bruto = array_sum($total_bruto);
            $purchased_products_array[$pp['order_id']]['total_bruto'] = $total_bruto;
        }
        // echo "<pre>";
        // print_r($purchased_products_array);
        // exit;
        // ini_set('memory_limit', '8192M');
        $purchased_products_array = array_values($purchased_products_array);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        $data['purchased_products'] = $purchased_products_array;
        return view('admin::picking/multiple_label', $data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
