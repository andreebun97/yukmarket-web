<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Session;
use App\Tag;
use Validator;
use App;
use Lang;
use Config;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $accessed_menu = $user_menu->get($user['id'],11)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::tag/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/category?type=tag');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],11)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::tag/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $obj =  [
            'tag_name' => ['required'],
            'tag_description' => ['required']
        ];
        if($request->input('tag_id') == null){
            $obj = [
                'tag_name' => ['required'],
                'tag_description' => ['required'],
                'tag_image' => ['required','mimes:jpeg,bmp,png,jpg,pneg']
            ];
        }else{
            if($request->file('tag_image') == null){
                $obj =  [
                    'tag_name' => ['required'],
                    'tag_description' => ['required']
                ];
            }else{
                $obj = [
                    'tag_name' => ['required'],
                    'tag_description' => ['required'],
                    'tag_image' => ['required','mimes:jpeg,bmp,png,jpg,pneg']
                ];
            }
        }
        $validator = Validator::make($request->all(), $obj);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if($request->input('tag_id') == null){
                $folder_name = 'img/uploads/tags/';
                $file = $request->file('tag_image');
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid().'.'.$ext;

                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                chmod($folder_name.$name, 0777);
                $inserted_obj = [
                    'tag_name' => $request->input('tag_name'),
                    'tag_desc' => $request->input('tag_description'),
                    'tag_image' => $folder_name.$name,
                    'active_flag' => 1
                ];
                $tag_id = Tag::insertGetId($inserted_obj);


                $alert = Lang::get('notification.has been inserted', ['result' => Lang::get('notification.inaccurately')]);
                if($tag_id > 0){
                    $alert = Lang::get('notification.has been inserted', ['result' => Lang::get('notification.successfully')]);
                }
            }else{
                $updated_obj = [
                    'tag_name' => $request->input('tag_name'),
                    'tag_desc' => $request->input('tag_description')
                ];
                if($request->file('tag_image') != null){
                    $folder_name = 'img/uploads/tags/';
                    $file = $request->file('tag_image');
                    $ext = $file == null ? '' : $file->getClientOriginalExtension();
                    $name = uniqid().'.'.$ext;
    
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                    chmod($folder_name.$name, 0777);

                    $updated_obj = [
                        'tag_name' => $request->input('tag_name'),
                        'tag_desc' => $request->input('tag_description'),
                        'tag_image' => $folder_name.$name
                    ];
                }

                $update = Tag::where('tag_id',$request->input('tag_id'))->update($updated_obj);

                $alert = Lang::get('notification.has been updated', ['result' => Lang::get('notification.inaccurately')]);
                if($update){
                    $alert = Lang::get('notification.has been updated', ['result' => Lang::get('notification.successfully')]);
                }
            }
            return redirect('admin/tag');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/tag');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],11)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $tag_by_id = Tag::where('tag_id', $request->get('id'))->first();
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['tag_by_id'] = $tag_by_id;
        return view('admin::tag/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Tag::where('tag_id',$request->input('id'))->update(['active_flag' => 1]);
        $alert = Lang::get('notification.has been activated', ['result' =>strtolower (Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/tag');
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Tag::where('tag_id',$request->input('id'))->update(['active_flag' => 0]);
        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/tag');
        //
    }
}
