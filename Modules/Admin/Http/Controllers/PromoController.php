<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\Voucher;
use App\VoucherType;
use App\Product;
use App\ProductVoucher;
use App\Rules\VoucherCodeValidation;
use App\Rules\VoucherValueValidation;
use Validator;
use App;
use DB;
use Lang;
use Config;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::voucher/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/voucher');
        }
        $user = Session::get('users');
        $products = Product::leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->where('00_product.active_flag',1)->orderBy('00_product.position_date','DESC')->get();
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $product_promo = ProductVoucher::join('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])->orWhere(DB::raw('CURRENT_TIMESTAMP'),'<',DB::raw('00_vouchers.starts_at'))->get()->toArray();
        $all_menu = $user_menu->get($user['id']);
        $voucher_type = VoucherType::where('active_flag', 1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['voucher_type'] = $voucher_type;
        $data['products'] = $products;
        $data['product_promo'] = $product_promo;
        $data['product_flag'] = 1;
        return view('admin::voucher/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        // print_r($request->input('applied_product'));
        // exit;
        $validator = Validator::make($request->all(), [
            // 'voucher_name' => ['required'],
            // 'voucher_code' => ['required','unique:00_vouchers'],
            'voucher_start_date' => ['required'],
            'voucher_start_time' => ['required'],
            'voucher_end_date' => ['required'],
            'voucher_end_time' => ['required'],
            'voucher_type' => ['required'],
            'total_provided_voucher' => ['required'],
            'voucher_description' => ['required'],
            'min_price_requirement' => ['required'],
            'max_value_price' => ['required'],
            'voucher_value_type' => ['required'],
            'voucher_amount' => ['required', new VoucherValueValidation($request->input('voucher_value_type'))]
        ]);
        if($request->input('voucher_id') != null){
            $validator = Validator::make($request->all(), [
                // 'voucher_name' => ['required'],
                // 'voucher_code' => ['required', new VoucherCodeValidation($request->input('voucher_id'))],
                'voucher_start_date' => ['required'],
                'voucher_start_time' => ['required'],
                'voucher_end_date' => ['required'],
                'voucher_end_time' => ['required'],
                'voucher_type' => ['required'],
                'total_provided_voucher' => ['required'],
                'voucher_description' => ['required'],
                'min_price_requirement' => ['required'],
                'max_value_price' => ['required'],
                'voucher_value_type' => ['required'],
                'voucher_amount' => ['required', new VoucherValueValidation($request->input('voucher_value_type'))]
            ]);
        }

        if($validator->fails()){
            $alert = Lang::get('notification.has been inserted',['result' => Lang::get('notification.inaccurately')]);
            if($request->input('voucher_id') != null){
                $alert = Lang::get('notification.has been updated',['result' => Lang::get('notification.inaccurately')]);
            }
            Session::flash('message_alert', $alert);
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $alert = "";
            if($request->input('voucher_id') == null){
                $voucher_id = Voucher::insertGetId([
                    // 'voucher_name' => $request->input('voucher_name'),
                    // 'voucher_code' => strtoupper($request->input('voucher_code')),
                    'voucher_type_id' => $request->input('voucher_type'),
                    'starts_at' => $request->input('voucher_start_date').' '.$request->input('voucher_start_time'),
                    'expires_at' => $request->input('voucher_end_date').' '.$request->input('voucher_end_time'),
                    'max_uses' => $request->input('total_provided_voucher'),
                    'min_price_requirement' => str_replace(".","",$request->input('min_price_requirement')),
                    'max_value_price' => str_replace(".","",$request->input('max_value_price')),
                    'is_fixed' => $request->input('voucher_value_type'),
                    'amount' => str_replace(".","",$request->input('voucher_amount')),
                    'voucher_desc' => $request->input('voucher_description'),
                    'created_by' => Session::get('users')['id']
                ]);
                
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($voucher_id > 0){
                    $product_array = array();
                    $product_list = $request->input('applied_product');
                    // echo count($product_list);
                    // exit;
                    for ($b=0; $b < count($product_list); $b++) { 
                        $obj = array(
                            'prod_id' => $product_list[$b],
                            'voucher_id' => $voucher_id,
                            'active_flag' => 1
                        );
                        array_push($product_array, $obj);
                    }
                    // echo count($product_array);
                    ProductVoucher::insert($product_array);
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }else{
                $update = Voucher::where('voucher_id', $request->input('voucher_id'))->update([
                    // 'voucher_name' => $request->input('voucher_name'),
                    // 'voucher_code' => strtoupper($request->input('voucher_code')),
                    'voucher_type_id' => $request->input('voucher_type'),
                    'starts_at' => $request->input('voucher_start_date').' '.$request->input('voucher_start_time'),
                    'expires_at' => $request->input('voucher_end_date').' '.$request->input('voucher_end_time'),
                    'max_uses' => $request->input('total_provided_voucher'),
                    'min_price_requirement' => str_replace(".","",$request->input('min_price_requirement')),
                    'max_value_price' => str_replace(".","",$request->input('max_value_price')),
                    'is_fixed' => $request->input('voucher_value_type'),
                    'amount' => str_replace(".","",$request->input('voucher_amount')),
                    'voucher_desc' => $request->input('voucher_description')
                ]);
                
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update > 0){
                    Voucher::where('voucher_id', $request->input('voucher_id'))->update([
                        'updated_by' => Session::get('users')['id']
                    ]);
                    $product_array = array();
                    $product_list = $request->input('applied_product');
                    // echo count($product_list);
                    // exit;
                    for ($b=0; $b < count($product_list); $b++) { 
                        $obj = array(
                            'prod_id' => $product_list[$b],
                            'voucher_id' => $request->input('voucher_id'),
                            'active_flag' => 1
                        );
                        array_push($product_array, $obj);
                    }
                    ProductVoucher::where('voucher_id', $request->input('voucher_id'))->delete();
                    ProductVoucher::insert($product_array);
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }
            Session::flash('message_alert', $alert);
            return redirect('admin/voucher?type=product');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/voucher');
        }
        $user = Session::get('users');
        $products = Product::leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->where('00_product.active_flag',1)->orderBy('00_product.position_date','DESC')->get();
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],15)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $voucher_type = VoucherType::where('active_flag', 1)->get();
        $product_promo = ProductVoucher::join('00_vouchers','00_vouchers.voucher_id','=','00_product_voucher.voucher_id')->whereBetween(DB::raw('CURRENT_TIMESTAMP'),[DB::raw('00_vouchers.starts_at'),DB::raw('00_vouchers.expires_at')])->orWhere(DB::raw('CURRENT_TIMESTAMP'),'<',DB::raw('00_vouchers.starts_at'))->get()->toArray();
        $product_promo_by_id = ProductVoucher::where('voucher_id', $request->get('id'))->get()->toArray();
        // print_r($product_promo_by_id);
        // exit;
        $voucher_by_id = Voucher::where('voucher_id', $request->get('id'))->first();
        // print_r($voucher_by_id);
        // exit;
        $voucher_by_id_temp = array(
            "voucher_id" => $voucher_by_id['voucher_id'],
            // "voucher_code" => $voucher_by_id['voucher_code'],
            // "voucher_name" => $voucher_by_id['voucher_name'],
            "voucher_description" => $voucher_by_id['voucher_desc'],
            "total_provided_voucher" => $voucher_by_id['max_uses'],
            "min_price_requirement" => $voucher_by_id['min_price_requirement'],
            "max_value_price" => $voucher_by_id['max_value_price'],
            "voucher_value_type" => $voucher_by_id['is_fixed'],
            "voucher_amount" => $voucher_by_id['amount'],
            "voucher_type_id" => $voucher_by_id['voucher_type_id'],
            "voucher_start_date" => date("Y-m-d",strtotime($voucher_by_id['starts_at'])),
            "voucher_start_time" => date("H:i:s",strtotime($voucher_by_id['starts_at'])),
            "voucher_end_date" => date("Y-m-d",strtotime($voucher_by_id['expires_at'])),
            "voucher_end_time" => date("H:i:s",strtotime($voucher_by_id['expires_at']))
        );
        // print_r($voucher_by_id_temp);
        // exit;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['voucher_type'] = $voucher_type;
        $data['voucher_by_id'] = $voucher_by_id_temp;
        $data['products'] = $products;
        $data['product_flag'] = 1;
        $data['product_promo'] = $product_promo;
        $data['product_promo_by_id'] = $product_promo_by_id;
        $data['edit'] = 1;
        return view('admin::voucher/form', $data);
    }
}
