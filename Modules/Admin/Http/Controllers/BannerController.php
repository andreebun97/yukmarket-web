<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use App\Banner;
use Session;
use Intervention\Image\Facades\Image;
use Validator;
use App;
use Lang;
use Config;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],5)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::banner/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/banner');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],5)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::banner/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/banner');
        }
        $user = Session::get('users');
        $banner_by_id = Banner::where('banner_id',$request->get('id'))->first();
        $data['user'] = $user;
        $data['banner_by_id'] = $banner_by_id;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],5)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::banner/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function form(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'banner_name' => ['required'],
            'banner_description' => ['required'],
            'banner_image' => ['required','image','max:2000']
        ]);
        if($request->input('banner_id') !== null){
            if($request->file('banner_image') !== null){
                $validator = Validator::make($request->all(),[
                    'banner_name' => ['required'],
                    'banner_description' => ['required'],
                    'banner_image' => ['required','image','max:2000']
                ]);
            }else{
                $validator = Validator::make($request->all(),[
                    'banner_name' => ['required'],
                    'banner_description' => ['required'],
                ]);    
            }
        }

        if($validator->fails()){
            if($request->input('banner_id') == null){
                Session::flash('message_alert',Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }else{
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $folder_name = 'img/uploads/banner/';
            $file = $request->file('banner_image');
            $ext = $file == null ? '' : $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;

            if($request->input('banner_id') == null){
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(700,420)->save(($folder_name.$name),80);
                chmod($folder_name.$name, 0777);
                $banner_id = Banner::insertGetId([
                    'banner_name' => $request->input('banner_name'),
                    'banner_desc' => $request->input('banner_description'),
                    'banner_image' => $folder_name.$name,
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                ]);
                if($banner_id > 0){
                    Session::flash('message_alert',Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
                }
            }else{
                $updated_obj = array(
                    'banner_name' => $request->input('banner_name'),
                    'banner_desc' => $request->input('banner_description'),
                    'updated_by' => Session::get('users')['id']
                );
                if($request->file('banner_image') !== null){
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(700,420)->save(($folder_name.$name),80);
                    $updated_obj = array(
                        'banner_name' => $request->input('banner_name'),
                        'banner_desc' => $request->input('banner_description'),
                        'banner_image' => $folder_name.$name,
                        'updated_by' => Session::get('users')['id']
                    );
                    File::delete($request->input('banner_image_temp'));
                }
                Banner::where('banner_id',$request->input('banner_id'))->update($updated_obj);
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
            }
            return redirect('admin/banner');
        }
    }

    /**
     * Remove and activate the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function activate(Request $request){
        $update = Banner::where('banner_id',$request->get('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

        if($update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/banner');
    }
    
    public function destroy(Request $request)
    {   $update = Banner::where('banner_id',$request->get('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]);

        if($update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(strtolower(Lang::get('notification.successfully')))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/banner');
        //
    }
}
