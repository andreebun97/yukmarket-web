<?php
namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Http\Controllers\IntegrationTokopediaController;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class PushProductTokpedController extends Controller
{
    private $token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJpbnRpc2luZXJnaXRla25vbG9naSIsImF1ZCI6InVzZXIiLCJzdWIiOiJhZG1pbkB0ZXN0LmNvbSIsImV4cCI6MTYwNTg1ODkzMiwicm9sZSI6MSwiaWQiOjEsImZ1bGxOYW1lIjoiYWRtaW4ifQ.v25xCYFWy_l_6NG1sixyXZGOGvE_DzP7RgSY1AWPW58CgfcBRTTDCATr-U1zdHc-5neqDcpgyGUAxfgLFmuwxw";

    public function pushProductToTokpedWithVariant(){
        $this->generateToken();//-------------------generate token
        $shopConfig=$this->getShopName();//------------------------get shop name
        $img='http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg';
        $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
        $dbEcomId=Organization::where('organization_name','TOKOPEDIA')->first();
        $preorder=(Object)['duration'=>0,'is_active'=>false,'time_unit'=>'DAY'];
        $etalase =(Object)['id'=>26345293];//------------------------------------form select
        $picture1=(Object)['file_path'=>$img];//---------------------------------parent/primary product
        $pictures=array($picture1);
        $prodName='Tokped-III-5-update';
        $sku='SKU-Tokped-III-5';//-------------------------------------------parent product id
        $file_path_obj[0]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];//----img parent/variant primary
        $file_path_obj[1]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];//-------img variant not primary
        $file_path_obj[2]=(Object)['file_path'=>"http://yukmarket.id/img/uploads/products/5fae513898b5f.jpg"];
        $file_path_array[0]=array($file_path_obj[0]);//----img parent/variant primary
        $file_path_array[1]=array($file_path_obj[1]);//-------img variant not primary
        $file_path_array[2]=array($file_path_obj[2]);
        $combination[0]=[0];
        $combination[1]=[1];
        $combination[2]=[2];
        $variantPicture=[
            $file_path_array[0],$file_path_array[1],$file_path_array[2]
            // $file_path_array[0],$file_path_array[1]
        ];
        $productsVarian[0]=(Object)[//-------------------------product parent/variant primary
            'combination'=>$combination[0],
            'is_primary'=>true,
            'pictures'=>$file_path_array[0],
            'price'=>1000,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>10
        ];
         $productsVarian[1]=(Object)[//-------------------------product variant not primary
            'combination'=>$combination[1],
            'is_primary'=>false,
            'pictures'=>$file_path_array[1],
            'price'=>1000,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>20
        ];
        $productsVarian[2]=(Object)[//-------------------------product variant not primary
            'combination'=>$combination[2],
            'is_primary'=>false,
            'pictures'=>$file_path_array[2],
            'price'=>750,
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>20
        ];
        // $productsVarianArray=[$productsVarian[0],$productsVarian[1]];
        $productsVarianArray=[$productsVarian[0],$productsVarian[1],$productsVarian[2]];
        $selectionId=54;
        $selectionUnitId=63;
        $selectionOptionObject[0]=(Object)[
            'hex_code'=>"",//-------------------------------string empty
            'unit_value_id'=>838,//------------------------form id berat
            'value'=>'varian 1 gram2'//------------------------------varian name
        ];
        $selectionOptionObject[1]=(Object)[
            'hex_code'=>"",
            'unit_value_id'=>841,
            'value'=>'varian 2 gram2'
        ];
        $selectionOptionObject[2]=(Object)[
            'hex_code'=>"",
            'unit_value_id'=>830,
            'value'=>'varian 5 gram'
        ];
        // $selectionOptionArray=[$selectionOptionObject[0],$selectionOptionObject[1]];
        $selectionOptionArray=[$selectionOptionObject[0],$selectionOptionObject[1],$selectionOptionObject[2]];
        $selectionObj=(Object)[
            'id'=>$selectionId,
            'options'=>$selectionOptionArray,
            'unit_id'=>$selectionUnitId
        ];
        $selectionArray=[$selectionObj];
        // $sizechartsObject[0]=(Object)['file_path'=>'https://ecs7.tokopedia.net/img/cache/700/product-1/2017/9/27/5510391/5510391_9968635e-a6f4-446a-84d0-ff3a98a5d4a2.jpg'];
        $sizechartsObject[0]=(Object)['file_path'=>$img];
        $sizechartsArray=[$sizechartsObject[0]];
        $variant=(Object)[
            'products'=>$productsVarianArray,
            'selection'=>$selectionArray,
            'sizecharts'=>$sizechartsArray
        ];
        $videosObject[0]=(Object)['source'=>'null','url'=>'null'];
        // $videosArray=[$videosObject[0]];
        $parentWeight=250;
        $parentWeightUnit='GR';//--------------------------string option : GR, KG
        $wholesaleObject[0]=(Object)['min_qty'=>100,'price'=>1000];
        $wholesaleArray=[$wholesaleObject[0]];
        $description='description';
        // $idProduct=array('id'=>1);
        $products= (Object)[
            'id'=>1356211892,
            'category_id'=>2751,
            'condition'=>'NEW', //-------------- string option: NEW, USED
            'description'=>$description,
            'etalase'=>$etalase,
            'is_free_return'=>false, //-----------boolean option: true, false
            'is_must_insurance'=>false, //------------boolean option: true, false
            'min_order'=>1,
            'name'=>$prodName,
            'pictures'=>$pictures,
            'preorder'=>$preorder,
            'price'=>1000, //---------------------parent price, jika ada variant maka tidak berlaku
            'price_currency'=>"IDR",
            'sku'=>$sku,
            'status'=>'LIMITED',
            'stock'=>10,
            'variant'=>$variant,
            // 'videos'=>$videosArray,
            'weight'=>$parentWeight,
            'weight_unit'=>$parentWeightUnit,
            // 'wholesale'=>$wholesaleArray
        ];
        $productObject=[$products];
        $obj=['products'=>$productObject];
        // dd($obj);
        $shopId=$dbShopId->organization_code;
        if($products->id==null){//--------------------jika tambah baru
            $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
        }else{//-------------------------------------jika update
            $url='https://api.ecomm.inergi.id/tokopedia/updateProductWithVariant?shopId='.$shopId;
        }
        $pushProduct=Http::withToken($this->token)->timeout(3)->retry(2, 100)->post($url,$obj);
        $statusPushProduct=json_decode($pushProduct->body());
        echo "\n".$statusPushProduct->data->upload_id;
        $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$statusPushProduct->data->upload_id;
        //---------------------------------------------------------get status push
        
        do {
            sleep(5);
            $hitStatusPush=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlStatusPush)->json();
            if($hitStatusPush['data']['unprocessed_rows']>0){
                $statusPushCode='loading';
                $statusPushProduct="data sedang dalam proses, coba ulangi beberapa saat lagi!";
            }elseif($hitStatusPush['data']['success_rows']>0){
                $statusPushCode='success';
                $statusPushProduct="produk berhasil ditambahkan ke tokopedia";
                $getProdId=$hitStatusPush['data']['success_rows_data'][0]['product_id'];
                //-----------insert ke table 00_product_ecommerce
                //----------------------------------jika produk tidak memiliki varian
                if($products->variant==null){
                    $dbGetProdId=1;
                    if($products->id==null){//-------------------------------jika insert
                        DB::table('00_product_ecommerce')
                        ->insert([
                            'ecommerce_id'=>$dbEcomId->organization_id,
                            'organization_id'=>$dbShopId->organization_id,
                            'prod_id'=>$dbGetProdId,
                            'prod_ecom_code'=>$getProdId,
                            'prod_ecom_name'=>$prodName,
                            'description'=>$description,
                            'active_flag'=>1
                        ]);
                    }else{//-------------------------------jika update
                        DB::table('00_product_ecommerce')
                        ->where('prod_id',$dbGetProdId)
                        ->update([
                            'ecommerce_id'=>$dbEcomId->organization_id,
                            'organization_id'=>$dbShopId->organization_id,
                            'prod_ecom_code'=>$getProdId,
                            'prod_ecom_name'=>$prodName,
                            'description'=>$description,
                            'active_flag'=>1
                        ]);
                    }
                }else{//---------------------------jika produk memiliki variant, data parent diambil dari variant primary
                    $dbGetProdId=[5,6,7];
                    $indexProductDB=0;
                    if($products->id==null){//-------------------------------jika insert
                        $parent=$this->getSingleProduct($getProdId);
                        $totalIndexVariant=count($parent['variant']['childrenID'])-1;
                        for($i=$totalIndexVariant;$i>=0;$i--){
                            $getVarian=$this->getSingleProduct($parent['variant']['childrenID'][$i]);
                            DB::table('00_product_ecommerce')
                            ->insert([
                                'ecommerce_id'=>$dbEcomId->organization_id,
                                'organization_id'=>$dbShopId->organization_id,
                                'prod_id'=>$dbGetProdId[$indexProductDB],
                                'prod_ecom_code'=>$getVarian['basic']['productID'],
                                'prod_ecom_name'=>$getVarian['basic']['name'],
                                'description'=>$getVarian['basic']['shortDesc'],
                                'active_flag'=>1
                            ]);
                            $indexProductDB++;
                        }
                    }else{//-------------------------------jika update
                        echo "\nUpdate";
                        $dbGetProdId=[5,6];
                        $currentVarianTokped=$this->getSingleProduct($getProdId);
                        $countVarianTokped=count($currentVarianTokped['variant']['childrenID'])-1;
                        //-----------get prod ecom code
                        for($i=0;$i<count($dbGetProdId);$i++){
                            //--------------------------------check ketersediaan
                            $checkVarian=DB::table('00_product_ecommerce')
                            ->where('prod_id',$dbGetProdId[$i])
                            ->select('prod_ecom_code')
                            ->first();
                            if($checkVarian){
                                $updateVarianSingleProd=$this->getSingleProduct($getProdId);
                                DB::table('00_product_ecommerce')
                                ->where('prod_ecom_code',$checkVarian->prod_ecom_code)
                                ->update([
                                    'prod_ecom_name'=>$updateVarianSingleProd['basic']['name'],
                                    'description'=>$updateVarianSingleProd['basic']['shortDesc'],
                                ]);
                            }else{
                                $addVarianSingleProd=$this->getSingleProduct($currentVarianTokped[$countVarianTokped]);
                                DB::table('00_product_ecommerce')
                                ->insert([
                                    'ecommerce_id'=>$dbEcomId->organization_id,
                                    'organization_id'=>$dbShopId->organization_id,
                                    'prod_id'=>$dbGetProdId[$i],
                                    'prod_ecom_code'=>$addVarianSingleProd['basic']['productID'],
                                    'prod_ecom_name'=>$addVarianSingleProd['basic']['name'],
                                    'description'=>$addVarianSingleProd['basic']['shortDesc'],
                                    'active_flag'=>1
                                ]);
                            }
                            $countVarianTokped--;
                        }
                    }
                }
            }elseif($hitStatusPush['data']['failed_rows']>0){
                $statusPushCode='error';
                $statusPushProduct=$hitStatusPush['data']['failed_rows_data'][0]['error'][0];
            }    
            echo "\n".$statusPushCode;
        } while ($statusPushCode=='loading');
        $returnResult=array(
            'status'=>$statusPushCode,
            'message'=>$statusPushProduct
        );
        // return $returnResult;
        dd($statusPushProduct);
    }
    public function etalaseTokped($etalase){
        $urlGetEtalase='https://api.ecomm.inergi.id/tokopedia/getAllEtalase?shopId='.$etalase;
        $hitEtalase=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlGetEtalase)->json();
        return $hitEtalase['data']['etalase'];
    }
    public function getBeratProductTokped($category){
        $url='https://api.ecomm.inergi.id/tokopedia/getAllVariantsByCategoryId?categoryId='.$category;
        $this->generateToken();
        $berat=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $berat['data'];
    }
    public function getShopName(){
        $shop=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokped_shop')
        ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
        ->first();
        return $shop;
    }
    public function getSingleProduct($id){
        $url='https://api.ecomm.inergi.id/tokopedia/getSingleProductByProductId?productId='.$id;
        $product=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $product['data'][0];
    }
    public function getProductIdEcommerce($id){
        $prodEcom=DB::table('00_product_ecommerce')
        ->where('prod_id',$id)
        ->select('prod_ecom_code')->first();
        return $prodEcom->prod_ecom_code;
    }
    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }
    public function generateToken(){
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();                            
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);            
        } catch (ConnectionException $e) {
            $token=null;            
        }         
        $this->token=$token;
    }
}
