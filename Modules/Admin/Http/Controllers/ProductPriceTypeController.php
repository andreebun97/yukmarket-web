<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App;
use Session;
use Validator;
use Lang;
use App\PriceType;
use App\ProductPriceType;

class ProductPriceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function form(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     ''
        // ]);
        if($request->input('product_price_type_id') == null){
            $product_price_type_id = ProductPriceType::insertGetId([
                'prod_id' => ($request->input('applied_product') == null ? null : $request->input('applied_product')),
                'price_type_id' => $request->input('price_type'),
                'price' => str_replace(".","",$request->input('price_amount')),
                'min_purchased_qty' => $request->input('min_purchased_qty'),
                'applicable_on' => $request->input('applicable_on'),
                'uom_promo' => $request->input('uom_promo'),
                'discount_type' => $request->input('discount_type'),
                'rate' => $request->input('rate'),
                'applicable_on' => $request->input('applicable_on'),
                'active_flag' => 1,
                'created_by' => Session::get('users')['id']
            ]);
        }else{
            $update = ProductPriceType::where('product_price_type',$request->input('product_price_type_id'))->update([
                'prod_id' => ($request->input('applied_product') == null ? null : $request->input('applied_product')),
                'category_id' => ($request->input('applied_category') == null ? null : $request->input('applied_category')),
                'price_type_id' => $request->input('price_type'),
                'price' => str_replace(".","",$request->input('price_amount')),
                'min_purchased_qty' => $request->input('min_purchased_qty'),
                'applicable_on' => $request->input('applicable_on'),
                'uom_promo' => $request->input('uom_promo'),
                'discount_type' => $request->input('discount_type'),
                'rate' => $request->input('rate'),
                'applicable_on' => $request->input('applicable_on'),
                'active_flag' => 1,
                'created_by' => Session::get('users')['id']
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
