<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App;
use Session;
use Validator;
use Lang;
use App\PriceType;
use App\ProductPriceType;
use App\Product;
use App\Category;
use Config;

class PriceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $accessed_menu = $user_menu->get($user['id'],32)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::price_type/index', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function create()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $accessed_menu = $user_menu->get($user['id'],32)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::price_type/form', $data);
    }

    public function edit(Request $request)
    {        
        if($request->get('id') == null){
            return redirect('admin/price_type');
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $accessed_menu = $user_menu->get($user['id'],32)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $price_type_by_id = PriceType::where('price_type_id', $request->get('id'))->first();
        $price_type_by_parent_id = PriceType::where('parent_id', $request->get('id'))->first();
        // print_r($price_type_by_parent_id);
        // exit;
        $product_price_type_by_parent_id = ProductPriceType::select('00_product.prod_id','00_product.prod_name','00_product_price_type.starts_at','00_product_price_type.expires_at','00_product_price_type.discount_type','00_product_price_type.rate','00_price_type.parent_id','00_price_type.price_type_id')->join('00_product','00_product.prod_id','=','00_product_price_type.prod_id')->join('00_price_type','00_price_type.price_type_id','=','00_product_price_type.price_type_id')->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->where('00_price_type.parent_id', $request->get('id'))->whereNotNull('00_product_price_type.prod_id')->get()->toArray();
        $product_list = "";
        // echo count($product_price_type_by_parent_id);
        // exit;
        if(count($product_price_type_by_parent_id) > 0){
            $product_array = array_map(function($value){
                return $value['prod_id'];
            }, $product_price_type_by_parent_id);
            $product_list = join(",", $product_array);
        }
        
        $category_price_type_by_parent_id = ProductPriceType::select('00_category.category_id','00_category.category_name','00_product_price_type.starts_at','00_product_price_type.expires_at','00_product_price_type.discount_type','00_product_price_type.rate','00_price_type.parent_id','00_price_type.price_type_id')->join('00_category','00_category.category_id','=','00_product_price_type.category_id')->join('00_price_type','00_price_type.price_type_id','=','00_product_price_type.price_type_id')->where('00_price_type.parent_id', $request->get('id'))->whereNotNull('00_product_price_type.category_id')->get()->toArray();
        $category_list = "";
        if(count($category_price_type_by_parent_id) > 0){
            $category_array = array_map(function($value){
                return $value['category_id'];
            }, $category_price_type_by_parent_id);
            $category_list = join(",", $category_array);
        }
        // print_r($category_list);
        // exit;
        // $category_id_list = array();
        // echo $category_price_type_by_parent_id;
        // exit;
        $data['price_type_by_id'] = $price_type_by_id;
        $data['price_type_by_parent_id'] = $price_type_by_parent_id;
        $data['product_price_type_by_parent_id'] = $product_price_type_by_parent_id;
        $data['category_price_type_by_parent_id'] = $category_price_type_by_parent_id;
        $data['product_list'] = $product_list;
        $data['category_list'] = $category_list;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::price_type/form', $data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */

    public function form(Request $request){
        $obj = [
            'price_type_name' => ['required'],
            'price_type_description' => ['required'],
            // 'minimum_order' => ['required'],
            'discount_type' => ['required'],
            'applicable_on' => ['required'],
            // 'starts_at' => ['required'],
            // 'expires_at' => ['required']
        ];
        if($request->input('parent_id') == null){
            $obj = [
                'price_type_name' => ['required'],
                'price_type_description' => ['required']
            ];
        }
        $validator = Validator::make($request->all(), $obj);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            // echo 'insert';
            // exit;
            $alert = "";
            if($request->input('price_type_id') == null){
                $inserted_obj = [
                    'price_type_name' => $request->input('price_type_name'),
                    'price_type_desc' => $request->input('price_type_description'),
                    'min_purchased_qty' => ($request->input('minimum_order') == null ? null : $request->input('minimum_order')),
                    'discount_type' => $request->input('discount_type'),
                    'applicable_on' => $request->input('applicable_on'),
                    'starts_at' => ($request->input('starts_at') == null || $request->input('starts_at') == "" ? null : $request->input('starts_at')),
                    'expires_at' => ($request->input('expires_at') == null || $request->input('expires_at') == "" ? null : $request->input('expires_at')),
                    'parent_id' => $request->input('parent_id'),
                    'active_flag' => 1,
                    'rate' => $request->input('discount_amount'),
                    'created_by' => Session::get('users')['id']
                ];
                if($request->input('parent_id') == null){
                    $inserted_obj = [
                        'price_type_name' => $request->input('price_type_name'),
                        'price_type_desc' => $request->input('price_type_description')
                    ];
                }
                $price_type_id  = PriceType::insertGetId($inserted_obj);
                
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($price_type_id > 0){
                    if($request->input('parent_id') != null){
                        if($request->input('applicable_on') == 2){
                            // $categories = $request->input('applied_category');
                            
                            // $inserted_obj = array(
                            //     'rate' => $request->input('discount_amount'),
                            //     'starts_at' => $request->input('starts_at'),
                            //     'expires_at' => $request->input('expires_at'),
                            //     'price_type_id' => $price_type_id
                            // );
                            $categories = $request->input('applied_items');
                            $product_price_type_array = array();
                            for ($b=0; $b < count($categories); $b++) {
                                $inserted_obj = array(
                                    'discount_type' => $request->input('multiple_discount_type')[$b],
                                    'rate' => $request->input('multiple_discount_amount')[$b],
                                    'starts_at' => $request->input('multiple_starts_at')[$b],
                                    'expires_at' => $request->input('multiple_expires_at')[$b],
                                    'price_type_id' => $price_type_id,
                                    'category_id' => $categories[$b]
                                );
                                array_push($product_price_type_array,$inserted_obj);
                            }
                            // echo count($product_price_type_array);
                            // exit;
                            ProductPriceType::insert($product_price_type_array);
                            $price_type_id = $request->input('parent_id');
                        }elseif($request->input('applicable_on') == 3){
                            $products = $request->input('applied_items');
                            $product_price_type_array = array();
                            for ($b=0; $b < count($products); $b++) {
                                $inserted_obj = array(
                                    'discount_type' => $request->input('multiple_discount_type')[$b],
                                    'rate' => $request->input('multiple_discount_amount')[$b],
                                    'starts_at' => $request->input('multiple_starts_at')[$b],
                                    'expires_at' => $request->input('multiple_expires_at')[$b],
                                    'price_type_id' => $price_type_id,
                                    'prod_id' => $products[$b]
                                );
                                array_push($product_price_type_array,$inserted_obj);
                            }
                            ProductPriceType::insert($product_price_type_array);
                            $price_type_id = $request->input('parent_id');
                        }
                    }
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    Session::flash('message_alert', $alert);
                    return redirect("admin/price_type/edit/?id=".$price_type_id);
                }
            }else{
                // echo 'update';
                // exit;
                $updated_obj = [
                    'price_type_name' => $request->input('price_type_name'),
                    'price_type_desc' => $request->input('price_type_description'),
                    'min_purchased_qty' => ($request->input('minimum_order') == null ? null : $request->input('minimum_order')),
                    'discount_type' => $request->input('discount_type'),
                    'applicable_on' => $request->input('applicable_on'),
                    'starts_at' => $request->input('starts_at'),
                    'expires_at' => $request->input('expires_at'),
                    'active_flag' => 1,
                    'rate' => $request->input('discount_amount'),
                    'parent_id' => $request->input('parent_id')
                ];
                if($request->input('parent_id') == null){
                    $updated_obj = [
                        'price_type_name' => $request->input('price_type_name'),
                        'price_type_desc' => $request->input('price_type_description')
                    ];
                }
                $update = PriceType::where('price_type_id', $request->input('price_type_id'))->update($updated_obj);
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update){
                    PriceType::where('price_type_id', $request->input('price_type_id'))->update(['updated_by' => Session::get('users')['id']]);
                    if($request->input('parent_id') != null){
                        ProductPriceType::where('price_type_id', $request->input('price_type_id'))->delete();
                        if($request->input('applicable_on') == 2){
                            $categories = $request->input('applied_items');
                            $product_price_type_array = array();
                            for ($b=0; $b < count($categories); $b++) {
                                $inserted_obj = array(
                                    'discount_type' => $request->input('multiple_discount_type')[$b],
                                    'rate' => $request->input('multiple_discount_amount')[$b],
                                    'price_type_id' => $request->input('price_type_id'),
                                    'starts_at' => $request->input('multiple_starts_at')[$b],
                                    'expires_at' => $request->input('multiple_expires_at')[$b],
                                    'category_id' => $categories[$b]
                                );
                                array_push($product_price_type_array,$inserted_obj);
                            }
                            ProductPriceType::insert($product_price_type_array);
                        }elseif($request->input('applicable_on') == 3){
                            $products = $request->input('applied_items');
                            $product_price_type_array = array();
                            for ($b=0; $b < count($products); $b++) {
                                $inserted_obj = array(
                                    'discount_type' => $request->input('multiple_discount_type')[$b],
                                    'rate' => $request->input('multiple_discount_amount')[$b],
                                    'price_type_id' => $request->input('price_type_id'),
                                    'starts_at' => $request->input('multiple_starts_at')[$b],
                                    'expires_at' => $request->input('multiple_expires_at')[$b],
                                    'prod_id' => $products[$b]
                                );
                                array_push($product_price_type_array,$inserted_obj);
                            }
                            ProductPriceType::insert($product_price_type_array);
                        }
                    }
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
                Session::flash('message_alert', $alert);
                return redirect('admin/price_type');
            }
            Session::flash('message_alert', $alert);
        }
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = PriceType::where('price_type_id', $request->get('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/price_type');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = PriceType::where('price_type_id', $request->get('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/price_type');
    }
}
