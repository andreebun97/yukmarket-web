<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use App\Helpers\Currency;
use App\Helpers\PhoneNumber;
use Config;
use App;
use Lang;
use Session;
use App\Order;
use App\Organization;
use App\OrderDetail;
use App\AddressDetail;
use App\OrderStatus;
use App\OrderHistory;
use App\PaymentMethod;
use App\Customer;
use App\Preparation;
use App\ProductWarehouse;
use App\User;
use App\TransactionHeader;
use App\TransactionDetail;
use App\Payment;
use App\Inventory;
use App\StockCard;
use App\Warehouse;
use App\UOM;
use App\Helpers\Transaction;
use App\Helpers\UomConvert;
use App\Product;
use App\Notification;
use Mail;
use DB;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use App\Exports\TransactionExport;
use App\Imports\TransactionImport;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;
    protected $phone_number;

    public function __construct($currency = null, $phone_number = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $phone_number = new PhoneNumber();
        $this->currency = $currency;
        $this->phone_number = $phone_number;
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],19)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $organization = Organization::whereIn('organization_type_id',[1,2,4])->get();
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        $data['payment_method'] = $payment_method;
        $data['organization'] = $organization;
        return view('admin::payment/index', $data);
    }

    public function export(Request $request){
        // $products = Product::all();
        
        // print_r($products);
        // exit;
        return Excel::download(new TransactionExport($request->all()), ('transaction_'.date('dmy').'_'.strtotime(date('d-m-y H:i:s')).'.xlsx'));
        
    }

    public function import() {
        Excel::import(new TransactionImport,request()->file('excel'));
        return back();
    }

    public function offline()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],34)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        $data['payment_method'] = $payment_method;
        $data['customer'] = Customer::select('customer_id', 'customer_name')->where('active_flag', '=', 1)->get();
        $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
        return view('admin::offline/index', $data);
    }

    // public function getParentOrderStatus(Request $request){
    //     $order_status = DB::table('00_order_status')
    //     ->whereIn('order_status_id',[1,2,3,4])
    //     ->get();

    //     $order_history = DB::table('10_order')
    //     ->select('10_order.order_id','10_order.order_status_id','00_order_status.parent_order_status_id')
    //     ->join('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
    //     ->where('order_id', $request->input('order_id'))
    //     ->first();
        
    //     $order_status_flag = 0;
    //     if($order_history != null){
    //         $order_status_flag = $order_history->parent_order_status_id;
    //     }
    //     $order_status_array = array();

    //     for ($b=0; $b < count($order_status); $b++) { 
    //         $obj = array(
    //             "order_status_id" => $order_status[$b]->order_status_id,
    //             "order_status_name" => $order_status[$b]->order_status_name,
    //             "order_status_logo" => $order_status[$b]->order_status_id <= $order_status_flag ? asset($order_status[$b]->order_status_active_logo) : asset($order_status[$b]->order_status_inactive_logo),
    //             "order_status_description" => $order_status[$b]->order_status_desc
    //         );

    //         array_push($order_status_array, $obj);
    //     }
        
    //     if(!$order_status){
    //         return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
    //     }else{
    //         return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => $order_status_array], 200);
    //     }
    // }

    public function getTrackingHistory(Request $request){
        $order_history = OrderHistory::select('10_order.order_id','00_order_status.order_status_id','00_order_status.order_status_name','00_order_status.order_status_desc','10_order_history.updated_date')
        ->join('10_order','10_order.order_id','=','10_order_history.order_id')
        ->join('00_order_status','00_order_status.order_status_id','=','10_order_history.order_status_id')
        ->where('10_order_history.order_id', $request->input('order_id'))
        ->orderBy('10_order_history.order_status_id','DESC')
        ->get()->toArray();

        $order_status = OrderStatus::select('00_order_status.*',DB::raw('(SELECT 10_order.order_status_id FROM 10_order WHERE 10_order.order_id = '.$request->input('order_id').') AS current_order_status_id'),DB::raw('(SELECT os.order_status_id FROM 00_order_status AS os WHERE os.order_status_id = (SELECT op.parent_order_status_id FROM 00_order_status AS op JOIN 10_order AS ord ON ord.order_status_id = op.order_status_id WHERE ord.order_id = '.$request->input('order_id').')) AS current_parent_order_id'))->whereIn('00_order_status.order_status_id',[1,2,3,4])->get()->toArray();
        $tracking_history = array();
        $order_status_array = array();

        for ($b=0; $b < count($order_history); $b++) { 
            $obj = array(
                "order_id" => $order_history[$b]['order_id'],
                "order_status_id" => $order_history[$b]['order_status_id'],
                "order_status_name" => $order_history[$b]['order_status_name'],
                "order_status_description" => $order_history[$b]['order_status_desc'],
                "created_date" => date("Y-m-d H:i:s",strtotime($order_history[$b]['updated_date']))
            );

            array_push($tracking_history, $obj);
        }

        for ($b=0; $b < count($order_status); $b++) { 
            $obj = array(
                "order_status_id" => $order_status[$b]['order_status_id'],
                "order_status_name" => $order_status[$b]['order_status_name'],
                "order_status_description" => ($order_status[$b]['order_status_desc'] == null ? '-' : $order_status[$b]['order_status_desc']),
                "current_parent_order_id" => $order_status[$b]['current_parent_order_id'],
                "order_status_logo" => $order_status[$b]['current_parent_order_id'] != null && $order_status[$b]['order_status_id'] <= $order_status[$b]['current_parent_order_id'] ? asset($order_status[$b]['order_status_active_logo']) : asset($order_status[$b]['order_status_inactive_logo'])
            );

            array_push($order_status_array, $obj);
        }

        if(!$order_history && !$order_status){
            return response()->json(['isSuccess' => false, 'message' => 'Data not found'], 500);
        }else{
            return response()->json(['isSuccess' => true, 'message' => 'OK', 'data' => array('history' => $tracking_history, 'status' => $order_status_array)], 200);
        }
    }

    public function create()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],34)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        $data['payment_method'] = $payment_method;
        $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();

        $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
        
        $second_uom = UOM::where('active_flag','=',1)
        ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
        ->select('00_uom.uom_id', '00_uom.uom_name');
        
        $uom = $first_uom->union($second_uom)
            ->groupBy('00_uom.uom_id');

        $data['satuan'] = $uom->get();

        // USER ORGANIZATION & WAREHOUSE
        $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
            ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
            ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
            ->where('98_user.user_id',$user['id'])
            ->first();
        $data['user_wh_org'] = $user_wh_org;
        // USER ORGANIZATION & WAREHOUSE

        // PRODUCT WAREHOUSE
        if ($user_wh_org->warehouse_id != null) {
            $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                    ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                    ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
                                    ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                    ->whereNull('00_product.parent_prod_id')
                                    ->where('00_product.active_flag',1)
                                    ->get();
        } else {
            $wh_product = null;
        }
        $data['wh_product'] = $wh_product;
        // PRODUCT WAREHOUSE

        $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
        $no_trx_first = substr($no_trx_shuffle,0,3);
        $no_trx_last = substr($no_trx_shuffle,-3);
        $data['no_trx'] = "YMOD".date('Ymd').$no_trx_first."".$no_trx_last;
        $data['invoice_no'] = "YMIV".date('Ymd').$no_trx_first."".$no_trx_last;

        $customer = Customer::select('customer_id', 'customer_name')->where('active_flag', '=', 1)->get();
        $data['customer'] = $customer;

        return view('admin::offline/create', $data);
    }

    public function getProductByWhId($id){
        $query = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
                                ->where('00_product_warehouse.warehouse_id',$id)
                                ->whereNull('00_product.parent_prod_id')
                                ->where('00_product.active_flag',1)
                                ->get();

        $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
        
        $second_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
        
        $uom = $first_uom->union($second_uom)
            ->groupBy('00_uom.uom_id');

        $data = array('product' => $query, 'uom' => $uom->get());
        return response()->json($data);
    }

    public function converterStock(Request $request) {
        if (!empty($request->input('satuan_produk')) && !empty($request->input('inventory_stock'))) {
            $convert = new UomConvert;
            $product = Product::select('uom_id','uom_value')->where('prod_id','=',$request->input('productId'))->first();
            $finalStock = $convert->convertToStock($request->input('satuan_produk'), $request->input('inventory_stock'), $product->uom_id, $product->uom_value);

            return ($finalStock) ? response()->json($finalStock, 200) : response()->json($finalStock, 400);
        } else {
            $response = array('flag'=>false, 'message'=>'Konversi tidak ditemukan!');
            return response()->json($response, 400);
        }
    }

    public function form(Request $request)
    {        
        $user = Session::get('users');
        $inventory_org = $request->input('inventory_org');
        $inventory_trx = $request->input('inventory_trx');
        $invoice_no = $request->input('invoice_no');
        $inventory_date = $request->input('inventory_date');
        // $inventory_status = $request->input('inventory_status');
        $inventory_status = 1;
        $inventory_warehouse = $request->input('inventory_warehouse');
        $inventory_warehouse_name = $request->input('inventory_warehouse_name');
        $inventory_desc = $request->input('inventory_desc');
        $inventory_product = $request->input('inventory_product');
        $inventory_price = $request->input('inventory_price');
        $inventory_price_total = $request->input('inventory_price_total');
        $inventory_stock = $request->input('inventory_stock');
        $inventory_uom = $request->input('satuan_produk');
        $inventory_customer = $request->input('customer');
        $inventory_final_stock = $request->input('final_stock');
        
        // ORDER HEADER 
        $check = Order::where('order_code', $inventory_trx)->first();
        if (!$check) {
            $trx_id = Order::insertGetId([
                'order_code' => $inventory_trx,
                'agent_user_id' => $inventory_org,
                'warehouse_id' => $inventory_warehouse,
                'online_flag' => 0,
                'order_status_id' => $inventory_status,
                'order_date' => $inventory_date,
                'created_by' => $user['id'],
                'buyer_user_id' => $inventory_customer
            ]);
        } else  {
            $trx_id = $check->order_id;
            $updated_obj = [
                'order_code' => $inventory_trx,
                'agent_user_id' => $inventory_org,
                'warehouse_id' => $inventory_warehouse,
                'online_flag' => 0,
                'order_status_id' => $inventory_status,
                'order_date' => $inventory_date,
                'created_by' => $user['id'],
                'buyer_user_id' => $inventory_customer
            ];
            Order::where('order_id', $trx_id)->update($updated_obj);
            OrderDetail::where('order_id', $trx_id)->delete();
        }
        // END ORDER HEADER 

        if ($trx_id > 0) {
            $grandTotal = 0;
            for ($i=0; $i < count($inventory_product) ; $i++) { 
                $prod_id = $inventory_product[$i];
                $pord_stock = $inventory_stock[$i];
                $uom_id = $inventory_uom[$i];
                $uom_value = $inventory_stock[$i];
                $final_stok = $inventory_final_stock[$i];
                $prod_price = str_replace(".", "", $inventory_price[$i]);
                $prod_total = str_replace(".", "", $inventory_price_total[$i]);
                $stock_sales = (0 - $final_stok);

                OrderDetail::insertGetId([
                    'order_id' => $trx_id,
                    'prod_id' => $prod_id,
                    'quantity' => $pord_stock,
                    'price' => $prod_price,
                    'created_by' => $user['id'],
                    'uom_id' => $uom_id,
                    'uom_value' => $uom_value
                ]);
                
                if ($inventory_status > 0) :
                    $check = Inventory::where([
                        ['warehouse_id', '=', $inventory_warehouse],
                        ['prod_id', '=', $prod_id],
                        ['active_flag', '=', 1],
                    ])->first();

                    $old_price = $check->inventory_price;
                    $old_stock = $check->stock;
                    // $new_stock = $old_stock - $pord_stock;
                    $new_stock = $old_stock - $final_stok;
                    $avg_price = $old_price;
                    $prod_price_total = (int)$stock_sales * $avg_price;
                    $grandTotal += (int) $prod_total;

                    $inventory_id = $check->inventory_id;
                    Inventory::where('inventory_id', $inventory_id)->update([
                        'organization_id' => $inventory_org,
                        'warehouse_id' => $inventory_warehouse,
                        'prod_id' => $prod_id,
                        'inventory_date' => $inventory_date,
                        'inventory_price' => $avg_price,
                        'stock' => $new_stock,
                        'created_by' => $user['id']
                    ]);                    
                    $this->validateUpdateStockTokped($prod_id,$new_stock,$inventory_org,$inventory_warehouse);

                    StockCard::insertGetId([
                        'stock_card_code' => $inventory_trx,
                        'organization_id' => $inventory_org,
                        'warehouse_id' => $inventory_warehouse,
                        'prod_id' => $prod_id,
                        'stock' => $stock_sales,
                        'transaction_type_id' => 3,
                        'price' => $avg_price,
                        'total_harga' => $prod_price_total,
                        'transaction_desc' => $inventory_desc,
                        'created_by' => $user['id']
                    ]);
                endif;
            }

            // PAYMENT INVOICE
            $buyer = Customer::join('00_address','00_customer.customer_id','=','00_address.customer_id')
                ->select('00_customer.customer_name', '00_address.address_name')
                ->where('00_customer.customer_id','=',$inventory_customer)
                ->where('00_address.isMain','=',1)
                ->first();

            $payment_id = Payment::insertGetId([
                'payment_name' => 'Cash', 
                'invoice_no' => $invoice_no,
                'payment_method_id' => 12,
                'online_flag' => 0,
                'customer_id' => $inventory_customer,
                'payment_date' => date('Y-m-d H:i:s'),
                'invoice_status_id' => $inventory_status,
                'reckon_flag' => 0,
                'buyer_name' => (!empty($buyer)) ? $buyer->customer_name : null,
                'buyer_address' => (!empty($buyer)) ? $buyer->address_name : null,
                'grandtotal_payment' => $grandTotal,
                'is_agent' => 0
            ]);

            Order::where('order_id', $trx_id)->update(['payment_id' => $payment_id]);
            // END PAYMENT INVOICE

            $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
            $result = array(
                'code' => '200',
                'status' => 'success',
                'messages' => $alert
            );
        } else {
            $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
            $result = array(
                'code' => '500',
                'status' => 'failed',
                'messages' => $alert
            );
        }

        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function detail(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/transaction');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        
        $accessed_menu = $user_menu->get($user['id'],19)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $transactions = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')
            ->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')
            ->where('order_id', $request->get('id'))->get();
        // $midtrans_transaction = $this->getMidtransTransaction($request->get('id'));
        $payment = Order::select(
            '10_order.order_id',
            '10_order.order_code',
            '10_order.shipment_price',
            '10_order.grand_total',
            '10_payment.voucher_id',
            '10_payment.voucher_amount',
            '10_payment.pricing_include_tax',
            '10_payment.admin_fee','10_payment.admin_fee_percentage',
            '10_payment.national_income_tax',
            '10_payment.invoice_no','10_cashback.return_amount')
            ->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')
            ->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')
            ->where('10_order.payment_id', function($query) use($request){
            $query->select('pa.payment_id')
            ->from('10_payment AS pa')
            ->join('10_order AS od','od.payment_id','=','pa.payment_id')
            ->where('od.order_id', $request->get('id'));
        })->get();
        // dd($payment);
        // print_r($payment);
        // echo count($payment);
        // exit;
        $order_master = Order::select(
            '10_order.order_id',
            '10_order.order_code',
            '10_order.order_date',
            '10_order.admin_fee',
            '10_order.admin_fee_percentage',
            '10_order.transfer_date',
            '10_order.pricing_include_tax',
            '10_order.national_income_tax',
            '10_order.buyer_name',
            '10_order.buyer_address',
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) 
            ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) 
            ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),
            DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) 
            ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),
            '10_order.shipment_price',
            '00_address.address_name',
            '00_address.address_info',
            '00_address.contact_person',
            '00_address.phone_number',
            '10_order.is_fixed',
            '10_order.voucher_amount',
            '10_order.destination_address',
            '00_address.address_detail',
            '00_kabupaten_kota.kabupaten_kota_name',
            '10_order.midtrans_transaction_id',
            '00_kecamatan.kecamatan_name',
            '00_kelurahan_desa.kelurahan_desa_name',
            '00_kelurahan_desa.kode_pos',
            '10_order.max_price',
            '10_order.voucher_id',
            '10_order.min_price',
            '10_order.invoice_status_id',
            '00_invoice_status.invoice_status_name',
            '10_order.order_status_id',
            DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN 
            (SELECT 00_shipment_method_ecommerce.shipment_method_name 
            FROM 00_shipment_method_ecommerce
             WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) 
             ELSE (SELECT 00_shipment_method.shipment_method_name 
             FROM 00_shipment_method 
             WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),
             '00_order_status.order_status_name',
             '10_order.payment_method_id',
             '00_payment_method.payment_method_name')
             ->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')
             ->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
             ->where('10_order.order_id', $request->get('id'))
             ->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')
             ->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')
             ->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')
             ->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')
             ->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')
             ->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')
             ->first();
        // print_r($payment);
        // exit;
        // $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_customer','00_customer.customer_id','=','00_address.customer_id')->join('10_order','10_order.buyer_user_id','=','00_customer.customer_id')->where('order_id', $request->get('id'))->get();
        $preparation = Preparation::where('order_id', $request->get('id'))->orderBy('preparation_status_id','DESC')->first();
        // $verification_steps = array("Pesanan Diterima","Pembayaran Diterima","Verifikasi","Picking","Quality Control","Packing","Delivery","Pesanan Selesai");
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['transactions'] = $transactions;
        $data['order_master'] = $order_master;
        $data['currency'] = $this->currency;
        $data['preparation'] = $preparation;
        $data['payment'] = $payment;
        $data['phone_number'] = $this->phone_number;
        // $data['address_detail'] = $address_detail;
        return view('admin::payment/detail', $data);
    }

    public function reminder_email(){
        $id = 1;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'),'10_cashback.return_amount')->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();
        $payment['status'] = "Belum Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['payment'] = $payment;
        $data['currency'] = $this->currency;
        $data['product_array'] = $product_array;
        // $data['products'] = $products;
        // Mail::send('payment_reminder_email',$data, function ($message) use ($transaction)
        //     {
        //         $message->from('support@yukmarket.com', 'YukMarket');
        //         $message->subject('Peringatan Pembayaran ('.$transaction->order_code.')');
        //         $message->to("suci.putri@indocyber.co.id");
        //     }
        // );
        // Mail::send('payment_reminder_email',$data, function ($message) use ($payment)
        //     {
        //         $message->from('support@yukmarket.com', 'YukMarket');
        //         $message->subject('Peringatan Pembayaran ('.$payment->invoice_no.')');
        //         $message->to(["suci.putri@indocyber.co.id","widiyanto.ramadhan@indocyber.co.id","achmad.rodhi@indocyber.co.id","andreebun97@gmail.com"]);
        //         // $message->to(["suci.putri@indocyber.co.id","andreebun97@gmail.com"]);
        //     }
        // );
        return view("payment_reminder_email" ,$data);
    }

    public function confirmation_email(){
        $id = 1;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_cashback.return_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();
        $payment['status'] = "Sudah Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['payment'] = $payment;
        $data['product_array'] = $product_array;
        // Mail::send('payment_confirmation_email',$data, function ($message) use ($transaction)
        //     {
        //         $message->subject('Konfirmasi Pembayaran ('.$transaction->invoice_no.')');
        //         $message->from('support@yukmarket.com', 'YukMarket');
        //         $message->to("suci.putri@indocyber.co.id");
        //         // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
        //     }
        // );
        return view("payment_confirmation_email", $data);
    }

    public function cancellation_email(){
        $id = 1;
        $payment = Payment::select('10_payment.payment_id','10_payment.invoice_no','10_payment.invoice_status_id','00_invoice_status.invoice_status_id','00_address.address_name','00_address.address_detail','00_address.contact_person','10_payment.admin_fee','10_payment.admin_fee_percentage','00_address.phone_number','10_payment.voucher_id','10_payment.voucher_amount','10_payment.voucher_value_type','10_payment.voucher_max_provided_amount','10_payment.pricing_include_tax','10_payment.national_income_tax','00_payment_method.payment_method_name','10_payment.created_date','10_payment.account_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_cashback.return_amount',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->where('10_payment.payment_id',$id)->join('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->join('00_invoice_status','00_invoice_status.invoice_status_id','=','10_payment.invoice_status_id')->leftJoin('00_address','00_address.address_id','=','10_payment.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')->first();
        $payment['status'] = "Tidak Bayar";
        $products = OrderDetail::select('10_order_detail.quantity','10_order_detail.price','10_order_detail.sku_status','10_order_detail.uom_id','00_uom.uom_name','00_product.uom_value','00_product.prod_name','10_order_detail.prod_id','10_order.warehouse_id','00_warehouse.warehouse_name','10_order.shipment_price')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('10_order.payment_id',$id)->get()->toArray();
        $product_array = array();
        foreach ($products as $element) {
            $product_array[$element['warehouse_id']]['warehouse_id'] = $element['warehouse_id'];
            $product_array[$element['warehouse_id']]['warehouse_name'] = $element['warehouse_name'];
            $product_array[$element['warehouse_id']]['shipment_price'] = $element['shipment_price'];
            $product_array[$element['warehouse_id']]['detail'][] = $element;
        }
        $product_array = array_values($product_array);
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['payment'] = $payment;
        $data['product_array'] = $product_array;
        // Mail::send('cancellation_payment',$data, function ($message) use ($payment)
        //     {
        //         $message->subject('Pembatalan Pembayaran ('.$payment->invoice_no.')');
        //         $message->from('support@yukmarket.com', 'YukMarket');
        //         $message->to("suci.putri@indocyber.co.id");
        //         // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
        //     }
        // );
        return view("cancellation_payment", $data);
    }

    public function filter(Request $request){
        // print_r($request->all());
        // exit;
        $activity_button = $request->input('activity_button');
        $transaction_start_date = $request->input('transaction_start_date');
        if(($transaction_start_date == null || $transaction_start_date == "")){
            $transaction_start_date = "";
        }else{
            $transaction_start_date = "?start=".$transaction_start_date;
        }
        $transaction_end_date = $request->input('transaction_end_date');
        if(($transaction_end_date == null || $transaction_end_date == "")){
            $transaction_end_date = "";
        }else{
            if(($transaction_start_date == null || $transaction_start_date == "")){
                $transaction_end_date = "?";
            }else{
                $transaction_end_date = "&";
            }
            $transaction_end_date .= "end=".$transaction_end_date;
        }
        // ($transaction_end_date == null || $transaction_end_date == "" ? "" : (($transaction_start_date == null || $transaction_start_date == "" ? "?" : "&")."end=".$transaction_end_date)) . ($transaction_end_date == null || $transaction_end_date == "" ? "" : (($transaction_start_date == null || $transaction_start_date == "" ? "?" : "&")."end=".$transaction_end_date))
        $order_status = $request->input('order_status');
        $order_code = $request->input('order_code');
        $url = "admin/transaction".$transaction_start_date.$end_date;
        if($activity_button == -1){
            $url = "admin/transaction";
        }
        return redirect($url);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function getMidtransTransaction($transaction_id)
    {
        $order = Order::where('order_id', $transaction_id)->first();

        $response = array();
        if($order->invoice_status_id == 3){

            $url = "https://api.sandbox.midtrans.com/v2/".$order->order_code."/status";
            
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => "Basic ".base64_encode(Config::get('midtrans.server_key'))
            ])->get($url);
    
            if($response['status_code'] == 201 || $response['status_code'] == 200 || $response['status_code'] == 407){
                if($response['transaction_status'] == "settlement"){
                    Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 2, 'order_status_id' => 6, "transfer_date" => $response['settlement_time']]);
                    OrderHistory::insert([
                        'order_id' => $transaction_id,
                        'order_status_id' => 6,
                        'active_flag' => 1,
                        'updated_by' => Session::get('users')['id']
                    ]);
                    Preparation::insert([
                        'preparation_status_id' => 1,
                        'order_id' => $transaction_id,
                        'updated_by' => Session::get('users')['id']
                    ]);
                }else if($response['transaction_status'] == "expire"){
                    Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 5, 'order_status_id' => 12]);
                    OrderHistory::insert([
                        'order_id' => $transaction_id,
                        'order_status_id' => 12,
                        'active_flag' => 1,
                        'updated_by' => Session::get('users')['id']
                    ]);
                }else if($response['transaction_status'] == "deny"){
                    Order::where('order_id', $transaction_id)->update(['invoice_status_id' => 4, 'order_status_id' => 11]);
                    OrderHistory::insert([
                        'order_id' => $transaction_id,
                        'order_status_id' => 11,
                        'active_flag' => 1,
                        'updated_by' => Session::get('users')['id']
                    ]);
                }
            }
        }

        return $response;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function confirmPayment(Request $request)
    {
        $update = Order::where('order_id',$request->input('order_id'))->update(['order_status_id' => 7, 'invoice_status_id' => 2]);

        OrderHistory::insert([
            'order_id' => $request->input('order_id'),
            'order_status_id' => 7,
            'active_flag' => 1,
            'updated_by' => Session::get('users')['id']
        ]);

        $preparation = Preparation::insertGetId([
            'preparation_status_id' => 1,
            'order_id' => $request->input('order_id'),
            'updated_by' => Session::get('users')['id']
        ]);

        return redirect('admin/picking');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function invoice(Request $request)
    {
        $purchased_products = array();
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order_detail.sku_status','10_order_detail.uom_value','10_payment.invoice_no','10_order.shipping_receipt_num','00_uom.uom_name','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','10_order.shipment_method_id','10_order.shipping_receipt_num','10_order.buyer_name','10_order.buyer_address',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','10_order.pricing_include_tax','10_order.national_income_tax','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','10_order_detail.promo_value','00_vouchers.voucher_name','00_vouchers.voucher_code',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),'00_address.contact_person','00_address.phone_number','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->where('10_order.order_id',$request->get('id'))->get();
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $accessed_menu = $user_menu->get($user['id'],22)[0]['access_status'];
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['user'] = $user;
        $data['purchased_products'] = $purchased_products;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        return view('admin::payment/invoice', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function label(Request $request)
    {
        $purchased_products = array();
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order_detail.sku_status','10_order_detail.uom_value','10_order.shipping_receipt_num','10_order.shipping_receipt_label','10_order_detail.bruto','00_uom.uom_name','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','10_order.pricing_include_tax','10_order.national_income_tax','10_order.shipment_method_id','00_organization.organization_type_id','00_organization.organization_logo','10_payment.invoice_no',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_logo FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_logo FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_logo'),DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','10_order_detail.promo_value','00_vouchers.voucher_name','00_vouchers.voucher_code','10_order.buyer_name','10_order.buyer_address',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_address.contact_person','00_address.phone_number','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name',DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS picker_name'),'00_product_sku.product_sku_id','00_product_sku.product_sku_name')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->leftJoin('00_product_sku','00_product_sku.product_sku_id','=','00_product.product_sku_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->where('10_order.order_id',$request->get('id'))->get()->toArray();
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        // print_r($all_menu);
        // echo "<br>";
        // echo $accessed_menu;
        // exit;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $total_bruto = array_map(function($value){
            return $value['bruto'];
        }, $purchased_products);
        $total_bruto = array_sum($total_bruto);
        // echo $total_bruto;
        // print_r($purchased_products);
        // exit;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['user'] = $user;
        $data['purchased_products'] = $purchased_products;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        $data['total_bruto'] = $total_bruto;
        return view('admin::payment/label', $data);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function detailOffline(Request $request, $id) {
        if($id == null){
            return redirect('admin/transaction');
        }

        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;

        $trx_header = Order::select(
                        '10_order.order_id as trx_id',
                        '10_order.order_code as trx_code',
                        '10_order.order_date as trx_date',
                        '10_order.order_status_id as trx_status',
                        '00_warehouse.warehouse_id',
                        '00_warehouse.warehouse_name',
                        '00_customer.customer_name')
                        ->leftJoin('00_warehouse','10_order.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')
                        ->where('10_order.order_id',$id)
                        ->first();

        $data['trx_header'] = $trx_header;
        $data['id_detail'] = $id;

        // PRODUCT WAREHOUSE
            if ($trx_header->warehouse_id != null) {
                $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                        ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                        ->where('00_product_warehouse.warehouse_id',$trx_header->warehouse_id)
                                        ->whereNull('00_product.parent_prod_id')
                                        ->where('00_product.active_flag',1)
                                        ->get();
            } else {
                $wh_product = null;
            }
            $data['wh_product'] = $wh_product;
        // PRODUCT WAREHOUSE

        return view('admin::offline/detail', $data);
    }

    public function historyOffline(Request $request, $id) {
        $draw = $request->get('draw');
        $start = $request->has('start') ? $request->get('start') : 0;
        $length = $request->has('length') ? $request->get('length') : 10;
        $search = $request->has('search') ? $request->get('search')['value'] : '';

        $total = OrderDetail::select('10_order_detail.prod_id',
                        '10_order_detail.price',
                        '10_order_detail.quantity',
                        '00_product.prod_code',
                        '00_product.prod_name',
                        '00_uom.uom_id',
                        '00_uom.uom_name',
                        '00_product.uom_value')
                        ->leftJoin('00_product','10_order_detail.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('10_order_detail.order_id',$id)
                        ->count();

        $sql = OrderDetail::select('10_order_detail.prod_id',
                        '10_order_detail.price',
                        '10_order_detail.quantity',
                        '00_product.prod_code',
                        '00_product.prod_name',
                        '00_uom.uom_id',
                        '00_uom.uom_name',
                        '00_product.uom_value')
                        ->leftJoin('00_product','10_order_detail.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('10_order_detail.order_id',$id)
                        ->limit($length)
                        ->offset($start)
                        ->get();

        $no = 1;
        $data = array();
        foreach ($sql as $row):
            $data_object = (object) array(
                'no' => $no++,
                'prod_name' => $row->prod_name,
                'prod_code' => $row->prod_code,
                'uom_id' => $row->uom_id,
                'uom_name' => $row->uom_name,
                'uom_value' => $row->uom_value,
                'formatted_product_name' => $row->prod_name . " " . $row->uom_value . " " . $row->uom_name,
                'price' => $row->price,
                'stock' => $row->quantity,
                'total' => $row->price * $row->quantity
            );
            array_push($data, $data_object);
        endforeach;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $data
        );

        echo json_encode($data);
    }
    //----------------------------INTEGRATION TOKPED
    public function validateUpdateStockTokped($prodId,$stock,$org,$warehouse){
    //---------------check global config
        $checkInventory=DB::table('00_inventory')
        ->select('booking_qty','goods_in_transit','goods_arrived')
        ->where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouse],
            ['organization_id',$org]
        ])->first();
        $config=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokopedia')
        ->select('global_parameter_desc')
        ->first();
        if($config->global_parameter_desc=='on'){
            $product =$this->checkProduct($prodId);
            if($product!=null){
                $namaToko=$this->getGlobalParameter('tokped_shop');
                $shopId=$this->getOrgTokped($namaToko->global_parameter_value);
                // $warehouseTokped=$this->getWarehouseTokped();
                $currentWarehouse=$this->checkCurrentWarehouse($warehouse);
                if($org==$shopId->parent_organization_id&&$currentWarehouse==$shopId->warehouse_id){
                    //---------------------------------------------get cold storage
                    $totalStock=$this->getStockColdStorage($org,$currentWarehouse,$prodId);
                    $url = config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$product->prod_ecom_code.'&productStocks='.$totalStock.'&shopId='.$shopId->organization_code;
                   $token=$this->generateToken('user_token_integrator');
                   try {
                       Http::withToken($token)->timeout(10)->retry(2, 1000)->post($url);
                   } catch (ConnectionException $e) {
                       return null;
                   }
                }
            }
        }
    }
    public function checkCurrentWarehouse($id){
        $data=DB::table('00_warehouse')
        ->where('warehouse_id',$id)
        ->select('warehouse_id','parent_warehouse_id')
        ->first();
        $warehouse=$data->parent_warehouse_id == null ? $data->warehouse_id : $data->parent_warehouse_id;
        return $warehouse;
    }
    public function checkProduct($id){
        $data = DB::table('00_product_ecommerce AS p')
        ->select('p.prod_ecom_code','o.organization_code')
        ->join('00_organization AS o','o.organization_id','=','p.organization_id')
        ->where('p.prod_id',$id)
        ->orderBy('p.prod_ecom_id','desc')
        ->first();
        return $data;
    }
    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }
    public function generateToken($globalParam){
        $credential=$this->getGlobalParameter($globalParam);
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(10)->retry(2, 1000)->get($url)->json();
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);
        } catch (ConnectionException $e) {
            $token=null;
        }
        return $token;
    }
    public function getOrgTokped($shopname){
        $org=DB::table('00_organization')
        ->select('organization_id','organization_name','organization_desc','organization_code','parent_organization_id','warehouse_id')
        ->where('organization_name','=',$shopname)
        ->first();
        return $org;
    }
    public function getWarehouseTokped(){
        $warehouseTokped=DB::table('00_organization')
        ->select('warehouse_id')
        ->where('organization_name','TOKOPEDIA')
        ->first();
        return $warehouseTokped->warehouse_id;
    }
    public function getStorage($warehouseId){
        $warehouseType=$this->checkWarehouseType($warehouseId);
        if($warehouseType->warehouse_type_id==2){
            $paramWarehouseId='parent_warehouse_id';
            $paramWarehouseValue=$warehouseId;
        }else{
            $paramWarehouseId='warehouse_id';
            $paramWarehouseValue=$warehouseType->parent_warehouse_id;
        }
        $warehouseList=DB::table('00_warehouse')
        ->where($paramWarehouseId,$paramWarehouseValue)
        ->select('warehouse_id','warehouse_type_id')
        ->get();
        return $warehouseList;
    }
    public function checkInventoryColdStorage($orgId,$coldStorageId,$prodId){
        $coldInventory=DB::table('00_inventory')
        ->where([
            ['organization_id',$orgId],
            ['prod_id',$prodId]
        ])
        ->whereIn('warehouse_id',$coldStorageId)
        ->select('stock','booking_qty','goods_in_transit','goods_arrived')
        ->get();
        return $coldInventory;
    }
    public function getStockColdStorage($orgId,$coldStorageId,$prodId){
        $coldStock=0;
        $warehouseList=$this->checkWarehouseType($coldStorageId);
        $warehouseArray=array();
        for($i=0;$i<count($warehouseList);$i++){
            $warehouseArray[$i]=$warehouseList[$i]->warehouse_id;
        }
        $coldStorageInventory=$this->checkInventoryColdStorage($orgId,$warehouseArray,$prodId);
        for($i=0;$i<count($coldStorageInventory);$i++){
            $coldStock+=floor($coldStorageInventory[$i]->stock-$coldStorageInventory[$i]->booking_qty-$coldStorageInventory[$i]->goods_in_transit-$coldStorageInventory[$i]->goods_arrived);
        }
        return $coldStock;
    }
    public function checkWarehouseType($warehouseId){
        $warehouseType=DB::table('00_warehouse')
        ->orWhere('warehouse_id',$warehouseId)
        ->orWhere('parent_warehouse_id',$warehouseId)
        ->select('warehouse_id')
        ->get()->toArray();
        return $warehouseType;
    }
    //----------------------------INTEGRATION TOKPED
}
