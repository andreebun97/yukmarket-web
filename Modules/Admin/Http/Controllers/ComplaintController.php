<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Routing\Controller;
use App\Notification;
use App\Helpers\Currency;
use App\Helpers\Transaction;
use App\ComplaintStatus;
use App\Complaint;
use App\ComplaintReplies;
use App\ComplaintSolution;
use App\ComplaintAttachment;
use App\ComplaintDetail;
use App\Cashback;
use App\Order;
use App\OrderHistory;
use App\OrderDetail;
use App\Payment;
use App\Preparation;
use App\User;
use DB;
use App\Customer;
use Session;
use App;
use Lang;
use Mail;
use Config;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    protected $currency;

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $this->currency = $currency;
    }

    public function index(Request $request)
    {
        if($request->get('issue_status') == null){
            return redirect('admin/complaint?issue_status=1');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],14)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $complaint_status = ComplaintStatus::all();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['complaint_status'] = $complaint_status;
        return view('admin::complaint/index', $data);
    }

    public function complaintNotification(Request $request)
    {
        Notification::where('notification_id', '=', $request->id)->update(['read_by_cms' => 1]);
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],14)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $complaint_status = ComplaintStatus::all();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['complaint_status'] = $complaint_status;
        return view('admin::complaint/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function detail(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/complaint');
        }
        $complaint = Complaint::select('00_issue.issue_id',DB::raw('(SELECT is2.issue_solution_name FROM 00_issue_solution AS is2 WHERE is2.issue_solution_id = 00_issue.approved_solution_id) AS approved_solution_name'),'00_issue.approved_solution_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),'10_order.is_agent','10_order.order_code','10_order.order_id','10_order.destination_address','10_cashback.return_amount','00_shipment_method.shipment_method_id','00_shipment_method.shipment_method_name','00_shipment_method.is_delivery')->join('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->where('00_issue.issue_id', $request->get('id'))->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->first();

        $complaint_response = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue_replies.customer_id','00_issue_replies.created_date','00_issue_replies.user_id',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 00_issue_replies.user_id) AS user_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_image FROM 00_customer WHERE 00_customer.customer_id = 00_issue_replies.customer_id) ELSE (SELECT 98_user.user_image FROM 98_user WHERE 98_user.user_id = 00_issue_replies.customer_id) END) AS customer_image'),'10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','00_shipment_method.shipment_method_id','00_shipment_method.shipment_method_name','00_shipment_method.is_delivery')->join('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->where('00_issue.issue_id', $request->get('id'))->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->orderBy('00_issue_replies.created_date','ASC')->get();

        $unread = 0;
        if(count($complaint_response) > 0){
            // if($unread != 0){
            //     for ($b=0; $b < count($complaint_response); $b++) { 
            //         if($complaint_response[$b]['is_read'] == 0){
            //             $unread = 1;
            //         }
            //     }
            //     ComplaintReplies::where('issue_id',$request->get('id'))->where('is_read',0)->update(['is_read' => 1]);
            // }
            // for ($b=0; $b < count($complaint_response); $b++) { 
            //     if($complaint_response[$b]['is_read'] == 0){
            //         $unread = 1;
            //     }
            // }
            ComplaintReplies::where('issue_id',$request->get('id'))->where('is_read',0)->update(['is_read' => 1]);
        }
        $complaint_attachment = ComplaintAttachment::where('issue_id', $request->get('id'))->get();
        // $complained_products = ComplaintDetail::select('00_product.prod_id', '00_product.prod_name', '10_order_detail.price', '10_order_detail.quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value')->distinct()->join('00_product','00_product.prod_id','=','00_issue_detail.prod_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->where('00_issue_detail.issue_id', $request->get('id'))->get();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '00_issue_detail.quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->leftJoin('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->leftJoin('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$request->input('id'))->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],14)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['complaint'] = $complaint;
        $data['complaint_attachment'] = $complaint_attachment;
        $data['complaint_response'] = $complaint_response;
        $data['currency'] = $this->currency;
        $data['complained_products'] = $complained_products;
        return view('admin::complaint/form', $data);
    }

    public function getUnreadResponse(Request $request){
        $complaint_response = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue_replies.customer_id','00_issue_replies.created_date','00_issue_replies.user_id',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 00_issue_replies.user_id) AS user_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_image FROM 00_customer WHERE 00_customer.customer_id = 00_issue_replies.customer_id) ELSE (SELECT 98_user.user_image FROM 98_user WHERE 98_user.user_id = 00_issue_replies.customer_id) END) AS customer_image'),'10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','00_shipment_method.shipment_method_id','00_shipment_method.shipment_method_name','00_shipment_method.is_delivery')->join('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->where('00_issue.issue_id', $request->get('issue_id'))->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->orderBy('00_issue_replies.created_date','ASC')->where('00_issue_replies.is_read',0)->get();

        if(count($complaint_response) > 0){
            ComplaintReplies::where('is_read',0)->update(['is_read' => 1]);
            return response()->json(array('data' => $complaint_response));
        }else{
            return response()->json(array('data' => []));
        }
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function process(Request $request)
    {
        $complaint = Complaint::where('issue_id', $request->input('issue_id'))->where('processed_by','!=',Session::get('users')['id'])->first();

        $alert = "";

        $additional_message = "";

        if($request->input('just_reply') == null && $complaint == null && ($request->input('rejected') != null || $request->input('process') != null || $request->input('another_action') != null)){
            $alert = Lang::get('notification.issue_processing', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
            $update = Complaint::where('issue_id',$request->input('issue_id'))->update(['processed_by' => Session::get('users')['id'],'processed_date' => date('Y-m-d H:i:s'),'issue_status_id' => ($request->input('rejected') == null ? 2 : 4), 'approved_solution_id' => $request->input('approved_solution_id')]);
            $alert = Lang::get('notification.issue_processing', ['result' => strtolower(Lang::get('notification.successfully'))]);
            if($request->input('rejected') == null){
                if($request->input('process') != null){
                    $additional_message = "Kabar Gembira! Keluhan Anda diterima oleh pihak YukMarket. ";
                    $this->processing_email($request->input('issue_id'));
                }else{
                    $additional_message = ($request->input('another_approval_response') == null || $request->input('another_approval_response') == "" ? "" : $request->input('another_approval_response'));
                    $this->another_approval_method($request->input('issue_id'));
                }
                // if($request->input(''))
            }else{
                $additional_message = "Maaf, keluhan Anda ditolak karena ";
                $this->rejection_email($request->input('issue_id'));
            }
            ComplaintReplies::insert([
                'issue_id' => $request->input('issue_id'),
                // 'customer_id' => $request->input('customer_id'),
                'user_id' => Session::get('users')['id'],
                'response' => $additional_message.$request->input('complaint_response'),
                'created_by' => Session::get('users')['id']
            ]);
        }else{
            $url = Config::get('fcm.url');
            $server_key = Config::get('fcm.token');
            $headers = array(
                'Content-Type'=>'application/json',
                'Authorization'=>'key='.$server_key
            );
            $complaint = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $request->issue_id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
            $title = "Admin YukMarket membalas chat Anda (".$complaint->order_code.")";
            $message = "Admin YukMarket membalas chat Anda dengan kode pesanan ".$complaint->order_code.": \"".$additional_message.$request->input('complaint_response')."\"";
            $type = "complaint";
            if($complaint->fcm_token != null){
                $parameter = array(
                    "to" => $complaint->fcm_token,
                    "notification" => array(
                        "title" => $title,
                        "body" => $message,
                        "message" => "message",
                        "sound" => "default",
                        "badge" => "1",
                        "image" => "null",
                        "icon" => "@mipmap/ic_stat_ic_notification"
                    ),
                    "data" => array(
                        "target" => "MainActivity",
                        "notifId" => "1",
                        "dataId" => "1"
                    )
                );
                $response = Http::withHeaders($headers)->post($url, $parameter);
            }
            Notification::insert([
                "customer_id" => $request->is_agent == 0 ? $request->customer_id : null,
                "user_id" => $request->is_agent == 1 ? $request->customer_id : null,
                "issue_id" => $request->issue_id,
                "order_id" => $request->order_id,
                "message" => $message,
                "title" => $title,
                "type" => $type
            ]);
            ComplaintReplies::insert([
                'issue_id' => $request->input('issue_id'),
                // 'customer_id' => $request->input('customer_id'),
                'user_id' => Session::get('users')['id'],
                'response' => $additional_message.$request->input('complaint_response'),
                'created_by' => Session::get('users')['id']
            ]);
        }

        Session::flash('message_alert', $alert);

        return redirect()->back();
    }

    public function email($id = null){
        $id = 1;
        $complaint = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '10_order_detail.quantity AS purchased_quantity', '00_issue_detail.quantity AS complained_quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$id)->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $this->currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        // Mail::send('complaint_email',$data, function ($message) use ($complaint)
        //     {
        //         $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
        //         $message->to($complaint->customer_email);
        //         $message->from('support@yukmarket.com', 'YukMarket');
        //         // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
        //     }
        // );
        return view('complaint_email', $data);
    }

    public function another_approval_method($id = null){
        // $id = 38;
        $complaint = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '10_order_detail.quantity AS purchased_quantity', '00_issue_detail.quantity AS complained_quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$id)->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $this->currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        // print_r($complaint);
        // exit;
        Mail::send('another_complaint_approval_method',$data, function ($message) use ($complaint)
            {
                $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
                $message->to($complaint->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
    
        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Solusi Keluhan yang Diajukan Tidak Dapat Kami Terima (".$complaint->order_code.")";
        $message = "Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak dengan kode pesanan ".$complaint->order_code.", kami ingin menginformasikan bahwa solusi yang diajukan tidak dapat kami terima dan kami gantikan dengan alternatif lainnya. Mohon menunggu hingga kabar berikutnya.";
        $type = "complaint";
        if($complaint->fcm_token != null){
            $parameter = array(
                "to" => $complaint->fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $complaint->is_agent == 0 ? $complaint->buyer_user_id : null,
            "user_id" => $complaint->is_agent == 1 ? $complaint->buyer_user_id : null,
            "issue_id" => $id,
            "order_id" => $complaint->order_id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
        // return view('another_complaint_approval_method', $data);
    }

    public function processing_email($id = null){
        // $id = 1;
        $complaint = Complaint::select('00_issue.issue_id',DB::raw('(SELECT is2.issue_solution_name FROM 00_issue_solution AS is2 WHERE is2.issue_solution_id = 00_issue.approved_solution_id) AS approved_solution_name'),'00_issue.approved_solution_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '10_order_detail.quantity AS purchased_quantity', '00_issue_detail.quantity AS complained_quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$id)->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $this->currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        // print_r($complaint);
        // exit;
        Mail::send('complaint_processing_email',$data, function ($message) use ($complaint)
            {
                $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
                $message->to($complaint->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
    
        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Keluhan Anda sedang diproses (".$complaint->order_code.")";
        $message = "Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak dengan kode pesanan ".$complaint->order_code.", kami ingin menginformasikan bahwa keluhan Anda sedang diproses oleh pihak YukMarket! Mohon menunggu hingga kabar berikutnya.";
        $type = "complaint";
        if($complaint->fcm_token != null){
            $parameter = array(
                "to" => $complaint->fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $complaint->is_agent == 0 ? $complaint->buyer_user_id : null,
            "user_id" => $complaint->is_agent == 1 ? $complaint->buyer_user_id : null,
            "issue_id" => $id,
            "order_id" => $complaint->order_id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
        // return view('complaint_processing_email', $data);
    }

    public function rejection_email($id = null){
        // $id = 1;
        $complaint = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '10_order_detail.quantity AS purchased_quantity', '00_issue_detail.quantity AS complained_quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$id)->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $this->currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        Mail::send('complaint_rejection_email',$data, function ($message) use ($complaint)
            {
                $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
                $message->to($complaint->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );

        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Keluhan Anda ditolak (".$complaint->order_code.")";
        $message = "Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak dengan kode pesanan ".$complaint->order_code.", mohon maaf kami selaku pihak YukMarket menolak pengajuan keluhan Anda dikarenakan suatu dan lain hal! Terima kasih!";
        $type = "complaint";
        if($complaint->fcm_token != null){
            $parameter = array(
                "to" => $complaint->fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $complaint->is_agent == 0 ? $complaint->buyer_user_id : null,
            "user_id" => $complaint->is_agent == 1 ? $complaint->buyer_user_id : null,
            "issue_id" => $id,
            "order_id" => $complaint->order_id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
        // return view('complaint_rejection_email', $data);
    }

    public function confirmation_email($id = null){
        // $id = 1;
        $complaint = Complaint::select('00_issue.issue_id',DB::raw('(SELECT is2.issue_solution_name FROM 00_issue_solution AS is2 WHERE is2.issue_solution_id = 00_issue.approved_solution_id) AS approved_solution_name'),'00_issue.approved_solution_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
        $complained_products = Complaint::select('00_product.prod_id', '00_product.prod_name', '00_product.prod_image', '00_uom.uom_name', '10_order_detail.price', '10_order_detail.quantity AS purchased_quantity', '00_issue_detail.quantity AS complained_quantity', '10_order_detail.uom_id', '10_order_detail.uom_value', '10_order_detail.sku_status', '10_order_detail.promo_value', '10_order_detail.dim_length', '10_order_detail.dim_width', '10_order_detail.dim_height', '10_order_detail.warehouse_id', '10_order_detail.is_taxable_product', '10_order_detail.tax_value','00_issue_detail.issue_detail_notes','10_order_detail.promo_value','00_issue_detail.issue_category_id','00_issue_category.issue_category_name')->join('00_issue_detail','00_issue_detail.issue_id','=','00_issue.issue_id')->leftJoin('00_issue_category','00_issue_category.issue_category_id','=','00_issue_detail.issue_category_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->join('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('00_issue.issue_id',$id)->whereColumn('10_order_detail.prod_id','00_issue_detail.prod_id')->get();
        $data['logo'] = Config::get('logo.email');
        $data['currency'] = $this->currency;
        $data['complaint'] = $complaint;
        $data['complained_products'] = $complained_products;
        Mail::send('complaint_confirmation_email',$data, function ($message) use ($complaint)
            {
                $message->subject('Keluhan Pesanan ('.$complaint->order_code.')');
                $message->to($complaint->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        $url = Config::get('fcm.url');
        $server_key = Config::get('fcm.token');
        $headers = array(
            'Content-Type'=>'application/json',
            'Authorization'=>'key='.$server_key
        );
        $title = "Keluhan Anda telah diterima (".$complaint->order_code.")";
        $message = "Mohon maaf atas kendala yang terjadi. Terkait dengan kendala yang dialami oleh Ibu/Bapak dengan kode pesanan ".$complaint->order_code.", kami ingin menginformasikan bahwa pengajuan keluhan Anda telah kami setujui. ".($complaint['issue_solution_id'] == 2 ? ('Anda menerima cashback berupa saldo yang akan ditransfer ke dalam akun Anda sebesar '.$this->currency->convertToCurrency($complaint['return_amount'])) : 'Anda dapat menukar barang di dalam pesanan yang Anda keluhkan dan akan diproses dalam beberapa hari.');
        $type = "complaint";
        if($complaint->fcm_token != null){
            $parameter = array(
                "to" => $complaint->fcm_token,
                "notification" => array(
                    "title" => $title,
                    "body" => $message,
                    "message" => "message",
                    "sound" => "default",
                    "badge" => "1",
                    "image" => "null",
                    "icon" => "@mipmap/ic_stat_ic_notification"
                ),
                "data" => array(
                    "target" => "MainActivity",
                    "notifId" => "1",
                    "dataId" => "1"
                )
            );
            $response = Http::withHeaders($headers)->post($url, $parameter);
        }
        Notification::insert([
            "customer_id" => $complaint->is_agent == 0 ? $complaint->buyer_user_id : null,
            "user_id" => $complaint->is_agent == 1 ? $complaint->buyer_user_id : null,
            "issue_id" => $id,
            "order_id" => $complaint->order_id,
            "message" => $message,
            "title" => $title,
            "type" => $type
        ]);
        // return view('complaint_confirmation_email', $data);
    }

    public function submit(Request $request)
    {
        // $replies = ComplaintReplies::where('issue_id', $request->input('issue_id'))->first();

        $alert = Lang::get('notification.issue_submitting', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        $product_array = array();
        
        $additional_message = "";
        $update = Complaint::where('issue_id',$request->input('issue_id'))->update(['issue_status_id' => 3]);

        if($request->input('just_reply') == null && $request->input('issue_status_id') == 2){
            if($request->input('approved_solution_id') == 1){
                $additional_message = "Barang Anda akan dikirim dengan barang yang baru. ";
                // $order = Order::where('order_id', $request->input('order_id'))->first();
                // $order_code = "YMOD".date("ymd");
                $billing_id = Payment::insertGetId([
                    // 'payment_name' => $payment_name,
                    'customer_id' => $request->customer_id,  
                    'payment_method_id' => $request->payment_method_id, 
                    'invoice_status_id' => 3,
                    'online_flag' => 0,
                    'voucher_id' => null,
                    'active_flag' => 1,
                    'updated_by' => Session::get('users')['id'],
                    'is_agent' => $request->is_agent,
                    'payment_date' => null 
                ]);
    
                if($billing_id > 0){
                    $transaction = new Transaction();
                    $billing_code = $transaction->generateCode($billing_id, "bill");
                    $grand_total = str_replace('.','',$request->input('claimed_balance_amount'));
                    $grand_total -= $request->input('unpaid_amount');
                    Payment::where('payment_id', $billing_id)
                    ->update([
                        'invoice_no' => $billing_code,
                        'grandtotal_payment' => $grand_total
                    ]);
                    $order_id = Order::insertGetId([
                        'buyer_user_id' => $request->input('customer_id'),
                        'parent_order_id' => $request->input('order_id'),
                        'order_status_id' => 6,
                        'payment_id' => $billing_id,
                        'destination_address' => $request->input('destination_address'),
                        'online_flag' => 0,
                        'active_flag' => 1
                    ]);
                    if($request->input('unpaid_amount') > 0){
                        Cashback::insert([
                            'issue_id' => $request->input('issue_id'),
                            'customer_id' => $request->input('customer_id'),
                            'order_id' => $order_id,
                            'return_amount' => $request->input('unpaid_amount'),
                            'transaction_flag' => 2,
                            'created_by' => Session::get('users')['id']
                        ]);
                    }
                    $order_code = $transaction->generateCode($order_id);
                    Order::where('order_id', $order_id)->update(['order_code' => $order_code]);
                    $product_qty = $request->input('quantity');
                    for ($b=0; $b < count($product_qty); $b++) { 
                        $obj = array(
                            "prod_id" => $request->input('product_id')[$b],
                            "price" => $request->input('price')[$b],
                            "order_id" => $order_id,
                            "quantity" => $product_qty[$b],
                            "sku_status" => $request->input('sku_status')[$b],
                            "dim_length" => $request->input('dim_length')[$b],
                            "dim_width" => $request->input('dim_width')[$b],
                            "dim_height" => $request->input('dim_height')[$b],
                            "uom_id" => $request->input('uom_id')[$b],
                            "uom_value" => $request->input('uom_value')[$b],
                            "warehouse_id" => $request->input('warehouse_id')[$b],
                            "is_taxable_product" => $request->input('is_taxable_product')[$b],
                            "promo_value" => 0,
                            "tax_value" => $request->input('tax_value')[$b]
                        );
    
                        array_push($product_array, $obj);
                    }
                    OrderDetail::insert($product_array);
                    OrderHistory::insert([
                        'order_id' => $order_id,
                        'order_status_id' => 5,
                        'active_flag' => 1
                    ],[
                        'order_id' => $order_id,
                        'order_status_id' => 6,
                        'active_flag' => 1
                    ]);
                    Preparation::insert([
                        'preparation_status_id' => 1,
                        'order_id' => $order_id
                    ]);
                }
            }elseif($request->input('approved_solution_id') == 2){
                $additional_message = "Anda akan menerima cashback yang akan dikirim ke akunnya sebesar ".$request->input('claimed_balance_amount').". ";
                $total_balance = 0;
                if($request->input('is_agent') == 0){
                    Cashback::insert([
                        'issue_id' => $request->input('issue_id'),
                        'customer_id' => $request->input('customer_id'),
                        'order_id' => $request->input('order_id'),
                        'claimed_amount' => str_replace('.','',$request->input('claimed_balance_amount')),
                        'return_amount' => str_replace('.','',$request->input('return_amount')),
                        'transaction_flag' => 1,
                        'created_by' => Session::get('users')['id']
                    ]);
                    $total_balance = Cashback::where('10_cashback.customer_id', $request->input('customer_id'))->sum(DB::raw('(CASE WHEN 10_cashback.transaction_flag = 0 THEN -10_cashback.return_amount ELSE 10_cashback.return_amount END)'));
                    Customer::where('customer_id', $request->input('customer_id'))->update([
                        'saldo' => $total_balance
                    ]);
                }else{
                    Cashback::insert([
                        'issue_id' => $request->input('issue_id'),
                        'agent_id' => $request->input('customer_id'),
                        'order_id' => $request->input('order_id'),
                        'claimed_amount' => str_replace('.','',$request->input('claimed_balance_amount')),
                        'return_amount' => str_replace('.','',$request->input('return_amount')),
                        'transaction_flag' => 1,
                        'created_by' => Session::get('users')['id']
                    ]);
                    $total_balance = Cashback::where('10_cashback.agent_id', $request->input('customer_id'))->sum(DB::raw('(CASE WHEN 10_cashback.transaction_flag = 0 THEN -10_cashback.return_amount ELSE 10_cashback.return_amount END)'));
                    User::where('user_id', $request->input('customer_id'))->update([
                        'saldo' => $total_balance
                    ]);
                }
    
                Order::where('order_id', $request->order_id)
                ->update([
                    'order_status_id' => 10
                ]);
                
                OrderHistory::insert([
                    'order_id' => $request->order_id,
                    'order_status_id' => 10,
                    'active_flag' => 1
                ]);
            }
        }
        ComplaintReplies::insert([
            'issue_id' => $request->input('issue_id'),
            // 'customer_id' => $request->input('customer_id'),
            'user_id' => Session::get('users')['id'],
            'response' => $additional_message.$request->input('complaint_response'),
            'created_by' => Session::get('users')['id']
        ]);
        if($request->input('issue_status_id') == 2){
            $this->confirmation_email($request->input('issue_id'));
        }else{
            // echo 'test';
            // exit;
            $url = Config::get('fcm.url');
            $server_key = Config::get('fcm.token');
            $headers = array(
                'Content-Type'=>'application/json',
                'Authorization'=>'key='.$server_key
            );
            $complaint = Complaint::select('00_issue.issue_id','00_issue.issue_status_id','00_issue.ticketing_num','00_issue.customer_id','10_order.order_date',DB::raw('0 AS is_agent'),'00_issue.order_id','00_issue_solution.issue_solution_id','00_issue_solution.issue_solution_name','00_issue.issue_notes','00_issue.issue_date','00_payment_method.payment_method_name','00_warehouse.warehouse_id','00_warehouse.warehouse_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'),'00_address.address_name','00_address.address_detail',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','10_order.is_agent','10_order.order_code','10_order.order_id','00_issue_replies.response','10_order.destination_address','10_cashback.return_amount','10_order.buyer_user_id')->leftJoin('00_issue_replies','00_issue_replies.issue_id','=','00_issue.issue_id')->join('10_order','10_order.order_id','=','00_issue.order_id')->where('00_issue.issue_id', $request->issue_id)->leftJoin('00_issue_solution','00_issue_solution.issue_solution_id','=','00_issue.issue_solution_id')->leftJoin('00_payment_method','10_order.payment_method_id','=','00_payment_method.payment_method_id')->leftJoin('10_cashback','10_cashback.issue_id','=','00_issue.issue_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','10_order.warehouse_id')->first();
            $title = "Admin YukMarket membalas chat Anda (".$complaint->order_code.")";
            $message = "Admin YukMarket membalas chat Anda dengan kode pesanan ".$complaint->order_code.": \"".$additional_message.$request->input('complaint_response')."\"";
            $type = "complaint";
            if($complaint->fcm_token != null){
                $parameter = array(
                    "to" => $complaint->fcm_token,
                    "notification" => array(
                        "title" => $title,
                        "body" => $message,
                        "message" => "message",
                        "sound" => "default",
                        "badge" => "1",
                        "image" => "null",
                        "icon" => "@mipmap/ic_stat_ic_notification"
                    ),
                    "data" => array(
                        "target" => "MainActivity",
                        "notifId" => "1",
                        "dataId" => "1"
                    )
                );
                $response = Http::withHeaders($headers)->post($url, $parameter);
            }
            Notification::insert([
                "customer_id" => $request->is_agent == 0 ? $request->customer_id : null,
                "user_id" => $request->is_agent == 1 ? $request->customer_id : null,
                "issue_id" => $request->issue_id,
                "order_id" => $request->order_id,
                "message" => $message,
                "title" => $title,
                "type" => $type
            ]);
        }
        $alert = Lang::get('notification.issue_submitting', ['result' => strtolower(Lang::get('notification.successfully'))]);
        // if($replies == null){
        // }
        Session::flash('message_alert', $alert);

        if($request->input('issue_status_id') == 3 || $request->input('issue_status_id') == 4){
            return redirect()->back();
        }
        return redirect('admin/complaint');
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
