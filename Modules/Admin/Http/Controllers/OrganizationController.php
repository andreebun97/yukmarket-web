<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use DB;
use App;
use Lang;
use Validator;
use App\Organization;
use App\OrganizationType;
use App\PriceType;
use Config;
use Intervention\Image\Facades\Image;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],33)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::organization/index', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function create()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],33)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $price_type = PriceType::whereNull('parent_id')->where('active_flag',1)->get();
        $organization_type = OrganizationType::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['price_type'] = $price_type;
        $data['organization_type'] = $organization_type;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::organization/form', $data);
    }

    public function edit(Request $request)
    {        
        if($request->get('id') == null){
            return redirect('admin/organization');
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],33)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $price_type = PriceType::whereNull('parent_id')->where('active_flag',1)->get();
        $organization_type = OrganizationType::where('active_flag',1)->get();
        $organization_by_id = Organization::where('organization_id', $request->get('id'))->first();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['organization_by_id'] = $organization_by_id;
        $data['price_type'] = $price_type;
        $data['organization_type'] = $organization_type;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::organization/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'organization_name' => ['required'],
            'organization_description' => ['required'],
            'rating' => ['required'],
            'organization_type' => ['required'],
            'price_type' => ['required'],
            'total_order' => ['required']
        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $alert = "";
            $organization_logo_location = "";
            $file = $request->file('organization_logo');
            $folder_name = 'img/uploads/organizations/';
            if($file != null){
                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                $name = uniqid().'.'.$ext;
                $organization_logo_location = $folder_name.$name;
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(1020,400)->save(public_path($organization_logo_location),80);
                chmod($organization_logo_location, 0777);
            }
            if($request->input('organization_id') == null){
                $organization_id = Organization::insertGetId([
                    'organization_name' => $request->input('organization_name'),
                    'organization_desc' => $request->input('organization_description'),
                    'rating' => $request->input('rating'),
                    'organization_type_id' => $request->input('organization_type'),
                    'price_type_id' => $request->input('price_type'),
                    'total_order' => $request->input('total_order'),
                    'created_by' => Session::get('users')['id'],
                    'organization_logo' => $organization_logo_location
                ]);
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($organization_id > 0){
                    $organization_id = (string)$organization_id;
                    $organization_code = "";
                    for ($i = 10; $i > 0; $i--) {
                        if($i > 1){
                            $organization_code .= "0";
                        }else{
                            $organization_code .= $organization_id;
                        }
                    }
                    $organization_code = "YMORG".$organization_code;
                    Organization::where('organization_id', $organization_id)->update(['organization_code' => $organization_code]);
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }else{
                $updated_obj = [
                    'organization_name' => $request->input('organization_name'),
                    'organization_desc' => $request->input('organization_description'),
                    'rating' => $request->input('rating'),
                    'organization_type_id' => $request->input('organization_type'),
                    'price_type_id' => $request->input('price_type'),
                    'total_order' => $request->input('total_order')
                ];
                if($file != null){
                    $updated_obj = [
                        'organization_name' => $request->input('organization_name'),
                        'organization_desc' => $request->input('organization_description'),
                        'rating' => $request->input('rating'),
                        'organization_type_id' => $request->input('organization_type'),
                        'price_type_id' => $request->input('price_type'),
                        'total_order' => $request->input('total_order'),
                        'organization_logo' => $organization_logo_location
                    ];
                }
                $update = Organization::where('organization_id', $request->input('organization_id'))->update($updated_obj);
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update){
                    Organization::where('organization_id', $request->input('organization_id'))->update(['updated_by' => Session::get('users')['id']]);
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }
            Session::flash('message_alert', $alert);
            return redirect('admin/organization');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Organization::where('organization_id', $request->get('id'))->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/organization');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Organization::where('organization_id', $request->get('id'))->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/organization');
    }
}
