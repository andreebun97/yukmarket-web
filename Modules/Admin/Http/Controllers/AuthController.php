<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use Hash;
use App\User;
use App\Menu;
use App\OTP;
use Validator;
use App;
use Lang;
use DB;
use Mail;
use App\Rules\OTPValidation;
use App\Rules\LoginValidation;
use Config;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
    }
    
    public function login()
    {
        $logo = Config::get('logo.auth');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::auth/login', $data);
    }

    public function logout()
    {
        Session::flush();
        Session::flash('message_alert', Lang::get('notification.logout'));
        return redirect('admin/auth/login');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function authenticate(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'user_email' => ['required'],
            'user_password' => ['required', new LoginValidation($request->input('user_email'))]
        ]);
        
        if($validator->fails()){
            // Session::flash('message_alert', Lang::get('notification.cannot_login'));
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            return redirect()->intended('admin/dashboard');
        }
        
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function forgot_password()
    {
        $logo = Config::get('logo.auth');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::auth/password/forgot', $data);
    }

    public function forgot_password_post(Request $request){
        $validator = Validator::make($request->all(), [
            'user_email' => ['required','exists:98_user']
        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $otp = mt_rand(100000, 999999);
        $user = OTP::where('user_id', function($query) use ($request){
            $query->where('user_email', $request->input('user_email'))->from('98_user')->select('user_id');
        })->count();
        if($user > 0){
            OTP::where('user_id', function($query) use ($request){
                $query->where('user_email', $request->input('user_email'))->from('98_user')->select('user_id');
            })->delete();
        }
        $user_detail = User::select('98_user.user_id','98_user.user_name')->where('user_email', $request->input('user_email'))->first();
        $insert = DB::insert("INSERT INTO 99_otp(user_id,otp_code) SELECT (SELECT 98_user.user_id FROM 98_user WHERE 98_user.user_email = '".$request->input('user_email')."'), ".$otp."");
        if($insert){
            Mail::send('emailreset', ['nama' => $user_detail['user_name'], 'otp' => $otp], function ($message) use ($request)
            {
                $message->subject('Reset Password YukMarket');
                $message->from('support@yukmarket.com', 'YukMarket');
                $message->to($request->input('user_email'));
            });
            Session::flash('username', $request->input('user_email'));
            return redirect('admin/auth/password/otp');
        }

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function password()
    {
        if(Session::get('username') == null){
            return redirect('admin/auth/password/otp');
        }
        $logo = Config::get('logo.auth');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::auth/password/input', $data);
    }

    public function input_password(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_email' => ['required'],
            'user_new_password' => ['required','regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/','min:8'],
            'user_confirm_password' => ['required','same:user_new_password']
        ]);

        if($validator->fails()){
            Session::flash('username', $request->input('user_email'));
            Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(Lang::get('notification.inaccurately'))]));
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $update = User::where('user_email', $request->input('user_email'))->update([
            'user_password' => Hash::make($request->input('user_new_password'))
        ]);
        Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(Lang::get('notification.successfully'))]));
        return redirect('admin/auth/login');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function otp()
    {
        if(Session::get('username') == null){
            return redirect('admin/auth/password/forgot');
        }
        $data['timer'] = Session::get('timer') == null ? "05:00" : Session::get('timer');
        $logo = Config::get('logo.auth');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::auth/password/otp', $data);
    }

    public function input_otp(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_email' => ['required'],
            'otp_code' => ['required', new OTPValidation($request->input('user_email'))]
        ]);

        if($validator->fails()){
            Session::flash('timer', $request->input('timer'));
            Session::flash('username', $request->input('user_email'));
            Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(Lang::get('notification.inaccurately'))]));
            return redirect()->back()->withInput()->withErrors($validator);
        }
        Session::flash('username', $request->input('user_email'));
        OTP::where('user_id',function($query) use ($request){
            $query->where('user_email',$request->input('user_email'))->select('user_id')->from('98_user');
        })->delete();
        Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(Lang::get('notification.successfully'))]));
        return redirect('admin/auth/password/input');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
