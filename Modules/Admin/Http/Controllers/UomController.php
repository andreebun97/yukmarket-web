<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\UOM;
use App\UomConvertion;
use Validator;
use App;
use Lang;
use Config;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],4)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::uom/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/uom');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],4)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::uom/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/uom');
        }
        $user = Session::get('users');
        $uom_by_id = UOM::where('uom_id',$request->get('id'))->first();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['uom_by_id'] = $uom_by_id;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],4)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $uom = UOM::where('active_flag',1)->get();
        $uom_id = $request->get('id');
        $uom_convertion = UomConvertion::where(function($query) use($uom_id){
            $query->where('uom_first_id', $uom_id)->orWhere('uom_second_id', $uom_id);
        })->get();
        $data['uom'] = $uom;
        $data['uom_convertion'] = $uom_convertion;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::uom/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'uom_name' =>  ['required', 'unique:00_uom,uom_name'],
            'product_weight_type' => ['required'],
            'uom_description' => ['required']
        ]);

        if($validator->fails()){
            if($request->input('uom_id') == null){
                Session::flash('message_alert', Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }else{
                Session::flash('message_alert', Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.inaccurately')))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if($request->input('uom_id') == null){
                $uom_id = UOM::insertGetId([
                    'uom_name' => $request->input('uom_name'),
                    'uom_desc' => $request->input('uom_description'),
                    'is_decimal' => $request->input('product_weight_type'),
                    'active_flag' => 1,
                    'created_by' => Session::get('users')['id']
                ]);
                if($uom_id > 0){
                    Session::flash('message_alert', Lang::get('notification.has been inserted',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
                }
            }else{
                UOM::where('uom_id',$request->input('uom_id'))->update([
                    'uom_name' => $request->input('uom_name'),
                    'uom_desc' => $request->input('uom_description'),
                    'is_decimal' => $request->input('product_weight_type'),
                    'active_flag' => 1,
                    'updated_by' => Session::get('users')['id']
                ]);
                Session::flash('message_alert', Lang::get('notification.has been updated',['result' => strtolower(strtolower(Lang::get('notification.successfully')))]));
            }
            return redirect('admin/uom');
        }
    }

    public function UomConvertionForm(Request $request){
        // print_r($request->all());
        // exit;
        $uom_convertion_array = array();
        $uom_id = $request->input('uom_id');
        $uom_first_convertion = $request->input('first_uom_id');
        $operation = $request->input('operation');
        $formula = $request->input('formula');
        for ($b=0; $b < count($operation); $b++) { 
            $obj = array(
                'uom_first_id' => $operation[$b] == 'division' ? $uom_id :  $uom_first_convertion[$b],
                'uom_second_id' => $operation[$b] == 'division' ? $uom_first_convertion[$b] : $uom_id,
                'formula' => $formula[$b]
            );
            array_push($uom_convertion_array, $obj);
        }
        // print_r($uom_convertion_array);
        // exit;
        UomConvertion::where('uom_first_id',$request->input('uom_id'))->orWhere('uom_second_id', $request->input('uom_id'))->delete();
        UomConvertion::insert($uom_convertion_array);
        return redirect('admin/uom');
    }

    public function getUom(){
        $uom = Uom::where('active_flag',1)->get();

        return response()->json(array('data' => $uom));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */

    public function activate(Request $request)
    {
        $update = UOM::where('uom_id', $request->input('id'))->update(['active_flag' => 1]);
        $alert = Lang::get('notification.has been activated', ['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/uom');
    }

    public function destroy(Request $request)
    {
        $update = UOM::where('uom_id', $request->input('id'))->update(['active_flag' => 0]);
        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/uom');
    }
}
