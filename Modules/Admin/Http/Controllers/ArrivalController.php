<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use App\Helpers\Currency;
use App\Helpers\PhoneNumber;
use Config;
use App;
use Lang;
use Session;
use App\Order;
use App\OrderDetail;
use App\AddressDetail;
use App\OrderStatus;
use App\OrderHistory;
use App\PaymentMethod;
use App\Customer;
use App\Preparation;
use Mail;
use DB;

class ArrivalController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;
    protected $phone_number;

    public function __construct($currency = null, $phone_number = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $phone_number = new PhoneNumber();
        $this->currency = $currency;
        $this->phone_number = $phone_number;
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],25)[0]['access_status'];
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_status'] = $order_status;
        $data['payment_method'] = $payment_method;
        return view('admin::arrival_confirmation/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function confirm(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/transaction');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],25)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $transactions = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id', $request->get('id'))->get();
        $order_master = Order::select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.admin_fee','10_order.admin_fee_percentage','10_order.transfer_date','10_order.pricing_include_tax','10_order.national_income_tax',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.shipment_price','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.is_fixed','10_order.voucher_amount','10_order.destination_address','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','10_order.midtrans_transaction_id','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','10_order.max_price','10_order.voucher_id','10_order.min_price','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.order_status_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'00_order_status.order_status_name','10_order.payment_method_id','00_payment_method.payment_method_name')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->where('10_order.order_id', $request->get('id'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->first();
        $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_customer','00_customer.customer_id','=','00_address.customer_id')->join('10_order','10_order.buyer_user_id','=','00_customer.customer_id')->where('order_id', $request->get('id'))->get();
        $preparation = Preparation::where('order_id', $request->get('id'))->orderBy('preparation_status_id','DESC')->first();
        // $verification_steps = array("Pesanan Diterima","Pembayaran Diterima","Verifikasi","Picking","Quality Control","Packing","Delivery","Pesanan Selesai");
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['transactions'] = $transactions;
        $data['order_master'] = $order_master;
        $data['currency'] = $this->currency;
        $data['preparation'] = $preparation;
        $data['address_detail'] = $address_detail;
        $data['phone_number'] = $this->phone_number;
        return view('admin::arrival_confirmation/form', $data);
    }
}
