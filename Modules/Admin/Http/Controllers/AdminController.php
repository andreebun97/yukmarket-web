<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Currency;
use App\Helpers\Transaction;
use Hash;
use Session;
use Validator;
use App\Customer;
use App\User;
use App\UserRole;
use App\Role;
use App\Order;
use App\OrderDetail;
use App\AddressDetail;
use App\Rules\UsernameValidation;
use App\OrderStatus;
use App\Organization;
use App\Warehouse;
use Input;
use App;
use Lang;
use DB;
use Config;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $currency;

    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $this->currency = $currency;
    }
    
    public function index()
    {
        echo 'index';
        // return view('admin::index');
    }

    public function list(Request $request)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],8)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::list', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('user') != null){
            return redirect('admin/list');
        }
        $user = Session::get('users');
        $role = Role::all();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['role'] = $role;
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],8)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $organization = Organization::where('active_flag',1)->get();
        if($user['organization_type_id'] == 2){
            $organization = Organization::where('active_flag',1)->where('organization_type_id',2)->get();
        }
        $warehouse = Warehouse::where('00_warehouse.active_flag',1)->get();
        $data['warehouse'] = $warehouse;
        $data['organization'] = $organization;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function filter(Request $request)
    {
        // print_r($request->all());
        // exit;
        $start_url = "&start=".$request->input('transaction_start_date');
        $end_url = "&end=".$request->input('transaction_end_date');
        $order_status_url = "&status=".$request->input('order_status');
        $minimum_total = $request->input('minimum_total');
        $minimum_total = str_replace(".","",$minimum_total);
        $maximum_total = $request->input('maximum_total');
        $maximum_total = str_replace(".","",$maximum_total);
        $minimum_total_url = "&minimum_total=".$minimum_total;
        $maximum_total_url = "&maximum_total=".$maximum_total;

        if($request->input('transaction_start_date') == ""){
            $start_url = "";
        }
        if($request->input('transaction_end_date') == ""){
            $end_url = "";
        }
        if($request->input('order_status') == ""){
            $order_status_url = "";
        }
        if($minimum_total == ""){
            $minimum_total_url = "";
        }
        if($maximum_total == ""){ 
            $maximum_total_url = "";
        }

        return redirect('admin/yukmarket/user/customer/?id='.$request->input('customer_id').$start_url.$end_url.$order_status_url.$minimum_total_url.$maximum_total_url);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('user') == null){
            return redirect('admin/list');
        }
        $user = Session::get('users');
        $user_by_id = User::join('98_user_role','98_user_role.user_id','=','98_user.user_id')->where('98_user.user_id',$request->get('user'))->first();
        $role = Role::all();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['role'] = $role;
        $data['user'] = $user;
        $data['user_by_id'] = $user_by_id;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],8)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $organization = Organization::where('active_flag',1)->get();
        if($user['organization_type_id'] == 2){
            $organization = Organization::where('active_flag',1)->where('organization_type_id',2)->get();
        }
        $warehouse = Warehouse::where('00_warehouse.active_flag',1)->get();
        $data['warehouse'] = $warehouse;
        $data['organization'] = $organization;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */

    public function form(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_name' => ['required'],
            'user_email' => ['required','email:rfc,dns','unique:98_user','unique:00_customer,customer_email'],
            'user_phone_number' => ['required','numeric','digits_between:10,15','unique:98_user','unique:00_customer,customer_phone_number'],
            'role_id' => ['required']
        ]);

        if($request->input('user_id') !== null){
            $validator = Validator::make($request->all(),[
                'user_name' => ['required'],
                'user_email' => ['required','email:rfc,dns', new UsernameValidation($request->input('user_id'))],
                'user_phone_number' => ['required','numeric','digits_between:10,15', new UsernameValidation($request->input('user_id')),'unique:00_customer,customer_phone_number'],
                'role_id' => ['required']
            ]);
        }

        if($validator->fails()){
            if($request->input('user_id') == null){
                Session::flash('message_alert',Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]));
            }else{
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]));
            }
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if($request->input('user_id') == null){
                $user_id = User::insertGetId([
                    'user_name' => $request->input('user_name'),
                    'user_email' => strtolower($request->input('user_email')),
                    'user_phone_number' => $request->input('user_phone_number'),
                    'user_password' => Hash::make('yukmarketpassw0rd'),
                    'active_flag' => 1,
                    'organization_id' => $request->input('organization_name'),
                    'warehouse_id' => ($request->input('warehouse_name') == "all" ? null : $request->input('warehouse_name')),
                    'created_by' => Session::get('users')['id']
                ]);

                if($user_id > 0){
                    UserRole::insert([
                        'user_id' => $user_id,
                        'role_id' => $request->input('role_id'),
                        'active_flag' => 1,
                        'created_by' => Session::get('users')['id']
                    ]);
                    $transaction = new Transaction();
                    $user_code = $transaction->generateCode($user_id, "user");
                    User::where('user_id', $user_id)->update(['user_code' => $user_code]);
                }
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]).'. '.Lang::get('notification.default_password',['result' => 'yukmarketpassw0rd']);
                Session::flash('message_alert', $alert);
                return redirect('admin/list');
            }else{
                User::where('user_id',$request->input('user_id'))->update([
                    'user_name' => $request->input('user_name'),
                    'user_email' => strtolower($request->input('user_email')),
                    'user_phone_number' => $request->input('user_phone_number'),
                    'organization_id' => $request->input('organization_name'),
                    'warehouse_id' => ($request->input('warehouse_name') == "all" ? null : $request->input('warehouse_name')),
                    'updated_by' => Session::get('users')['id']
                ]);
                UserRole::where('user_id', $request->input('user_id'))->update(['role_id' => $request->input('role_id')]);
                Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]));
                return redirect('admin/list');
            }
        }
    }

    public function activate(Request $request){
        User::where('user_id',$request->get('user'))->update(['active_flag' => 1]);
        UserRole::where('user_id',$request->get('user'))->update(['active_flag' => 1]);

        Session::flash('message_alert',Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]));
        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        User::where('user_id',$request->get('user'))->update(['active_flag' => 0]);
        UserRole::where('user_id',$request->get('user'))->update(['active_flag' => 0]);

        Session::flash('message_alert',Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]));
        return redirect()->back();
    }

    public function yukmarket(Request $request){
        $roles = Role::all();
        $user = Session::get('users');
        $data['user'] = $user;
        $data['roles'] = $roles;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],6)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::yukmarket/index', $data);
    }

    public function user_detail(Request $request){
        if($request->get('id') == null){
            return redirect('admin/yukmarket/list');
        }
        $roles = Role::all();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user = Session::get('users');
        // $transactions = Order::where('seller_user_id', $request->get('id'))->join('98_user','98_user.user_id','=','10_order.seller_user_id')->join('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->simplePaginate(2);
        $user_by_id = User::join('98_user_role','98_user_role.user_id','=','98_user.user_id')->join('98_role','98_role.role_id','=','98_user_role.role_id')->select('98_user.user_id','98_user.user_name','98_user.user_email','98_user.user_phone_number','98_user.active_flag AS user_active_status','98_role.role_id','98_role.role_name','98_user.created_date AS created_date')->where('98_user.user_id',$request->get('id'))->first();
        $data['role_name'] = $request->segment(4);
        $order_detail = array();
        $order_master = array();
        $address_detail = array();
        if($data['role_name'] == 'customer'){
            $user_by_id = Customer::select('00_customer.customer_id AS user_id','00_customer.customer_name AS user_name','00_customer.customer_phone_number AS user_phone_number','00_customer.customer_email AS user_email','00_customer.active_flag AS user_active_status','00_customer.created_date')->where('00_customer.customer_id',$request->get('id'))->first();
            $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->where('customer_id', $request->get('id'))->where('isMain',1)->get();
            if($request->get('order_id') != null){
                $order_master = Order::select('10_order.order_id','10_order.order_code','10_order.order_date','10_order.admin_fee','10_order.admin_fee_percentage','10_order.transfer_date','00_customer.customer_id','00_customer.customer_name','00_customer.customer_email','00_customer.customer_phone_number','10_order.shipment_price','10_order.is_fixed','10_order.voucher_amount','10_order.destination_address','10_order.pricing_include_tax','10_order.national_income_tax','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','10_order.max_price','10_order.voucher_id','10_order.min_price','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.order_status_id','00_order_status.order_status_name','10_order.payment_method_id','00_payment_method.payment_method_name')->join('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->where('10_order.order_id', $request->get('order_id'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->first();
                $order_detail = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id', $request->get('order_id'))->get();
            }            
        }
        $data['user'] = $user;
        $data['user_by_id'] = $user_by_id;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],6)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $order_status = OrderStatus::where('order_status_id','>=','5')->where('order_status_id','<=','12')->get();
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['order_detail'] = $order_detail;
        $data['order_master'] = $order_master;
        $data['address_detail'] = $address_detail;
        $data['currency'] = $this->currency;
        $data['order_status'] = $order_status;
        return view('admin::yukmarket/data', $data);
    }

    public function yukmarket_admin_form(Request $request){
        User::where('user_id', $request->input('user_id'))->update(['active_flag' => $request->input('user_active_status')]);
        return redirect('admin/yukmarket/list/?role=admin');
    }

    public function yukmarket_customer_form(Request $request){
        Customer::where('customer_id', $request->input('user_id'))->update(['active_flag' => $request->input('user_active_status')]);
        return redirect('admin/yukmarket/list/?role=customer');
    }
}
