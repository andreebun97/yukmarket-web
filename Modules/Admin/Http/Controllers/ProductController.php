<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Session;
use App\Warehouse;
use App\ProductWarehouse;
use App;
use Lang;
use Validator;
use App\StockKeepingUnit;
use App\Product;
use App\Category;
use App\CategoryEcommerce;
use App\ProductEcommerce;
use App\Brand;
use App\UOM;
use App\Tag;
use App\ProductTag;
use App\ProductCategory;
use App\Rules\NumberValidation;
use App\Helpers\UomConvert;
use App\Helpers\PushData;
use App\ShippingDurability;
use DB;
use Config;
use App\Organization;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\InventoryImport;
use App\Imports\ProductVariantImport;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        // echo  date('Y-m-d H:i:s').'<br>'. date('Y-m-d H:i:s', strtotime('-1seconds'));
        // exit;
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],2)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::products/index', $data);
    }

    public function export(){
        // $products = Product::all();

        // print_r($products);
        // exit;
        return Excel::download(new ProductExport, ('product_'.date('dmy').'_'.strtotime(date('d-m-y H:i:s')).'.xlsx'));

    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('product') != null){
            return redirect('admin/product');
        }
        $user = Session::get('users');
        $category = Category::where('active_flag',1)->get();
        $brand = Brand::where('active_flag',1)->get();
        $uom = UOM::where('active_flag',1)->get();
        $tags = Tag::where('active_flag',1)->get();
        $sku = StockKeepingUnit::where('active_flag',1)->get();
        $warehouse = Warehouse::where('00_warehouse.active_flag',1)->get();

        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $shipping_durability = ShippingDurability::where('active_flag',1)->get();
        $ecommerce = Organization::where(['active_flag' => 1, 'organization_type_id' => 4])->get();
        $tokopedia_category = CategoryEcommerce::select('00_category_per_ecommerce.category_id','00_category_per_ecommerce.category_name','category_id_ecommerce',DB::raw('(SELECT pc.category_id FROM 00_product_ecommerce AS pc WHERE pc.prod_id = "'.$request->get('product').'") AS tokped_category_id'))->join('00_category','00_category.category_id','=','00_category_per_ecommerce.category_id')->where('category_level',3)->where('organization_id',28)->get()->toArray();
        $main_warehouse = Warehouse::whereIn('00_warehouse.warehouse_id', function($query){
            $query->select('00_organization.warehouse_id')->from('00_organization')->whereIn('00_organization.organization_name', function($query2){
                $query2->select('99_global_parameter.global_parameter_value')->from('99_global_parameter')->where('99_global_parameter.global_parameter_name','tokped_shop');
            });
        })->first();
        $data['shipping_durability'] = $shipping_durability;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['warehouse'] = $warehouse;
        $data['ecommerce'] = $ecommerce;
        $data['main_warehouse'] = $main_warehouse;
        $data['warehouse_64'] = base64_encode($warehouse);
        $data['user'] = $user;
        $data['category'] = $category;
        $data['brand'] = $brand;
        $data['uom'] = $uom;
        $data['sku'] = $sku;
        $data['tokopedia_category'] = $tokopedia_category;
        $this->generateToken();
        $data['etalase'] = $this->etalaseTokped();
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],2)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['tags'] = $tags;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::products/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function get(){
        $products = Product::where('active_flag',1)
                    ->select('00_product.*','00_product_warehouse.prod_price','00_product_warehouse.warehouse_id','00_warehouse.warehouse_name')
                    ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                    ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                    ->get();

        return response()->json(array('data' => $products));
    }

    public function stockKeepingUnit()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],27)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::sku/index', $data);
    }

    public function stockKeepingUnitForm(Request $request)
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],27)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        if($request->get('id') != null){
            $sku = StockKeepingUnit::where('product_sku_id', $request->get('id'))->first();
            $data['sku'] = $sku;
        }
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::sku/form', $data);
    }

    public function submitSkuForm(Request $request){
        $validator = Validator::make($request->all(), [
            'sku_name' => ['required'],
            'sku_description' => ['required']
        ]);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $alert = "";
            if($request->input('product_sku_id') == null){
                $sku_id = StockKeepingUnit::insertGetId([
                    'product_sku_name' => $request->input('sku_name'),
                    'product_sku_desc' => $request->input('sku_description'),
                ]);

                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($sku_id > 0){
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }else{
                $update = StockKeepingUnit::where('product_sku_id', $request->input('product_sku_id'))->update([
                    'product_sku_name' => $request->input('sku_name'),
                    'product_sku_desc' => $request->input('sku_description'),
                ]);

                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update){
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }
            Session::flash('message_alert', $alert);
            return redirect('admin/sku');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('product') == null){
            return redirect('admin/product');
        }
        // $data = array(
        //     array(
        //         "prod_id" => 2,
        //         "prod_name" => "Bawang Putih"
        //     ),
        //     array(
        //         "prod_id" => 3,
        //         "prod_name" => "Bawang Merah"
        //     ),
        //     array(
        //         "prod_name" => "Bawang Bombay"
        //     )
        // );
        // for ($i=0; $i < count($data); $i++) { 
        //     if(!isset($data[$i]['prod_id'])){
        //         echo $i;
        //     }
        // }
        // exit;
        // $environment = "locall";
        // echo strcasecmp($environment,"local");
        // exit;
        $user = Session::get('users');
        $category = Category::where('active_flag',1)->get();
        $brand = Brand::where('active_flag',1)->get();
        $uom = UOM::where('active_flag',1)->get();
        $sku = StockKeepingUnit::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $shipping_durability = ShippingDurability::where('active_flag',1)->get();
        $data['shipping_durability'] = $shipping_durability;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['total_variant'] = Product::where('variant_id', $request->get('product'))->where('active_flag',1)->count('prod_id');
        $product_based_on_id = Product::select('00_product.*','00_product_ecommerce.prod_ecom_code',DB::raw('(SELECT SUM(00_inventory.stock) FROM 00_inventory WHERE 00_inventory.prod_id = "'.$request->get('product').'" AND 00_inventory.organization_id = 28) AS total_stock'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')->leftJoin('00_category','00_category.category_id','=','00_product.category_id')->leftJoin('00_product_ecommerce','00_product_ecommerce.prod_id','=','00_product.prod_id')->where('00_product.prod_id',$request->get('product'))->first();
        if($product_based_on_id == null || ($product_based_on_id !=  null && $product_based_on_id['active_flag'] == -1)){
            return redirect('admin/product');
        }
        // $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
        //             $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
        //                     WHERE glb.global_parameter_name = "tokped_shop"';
        //             $stock_query .= ')';
        $main_warehouse = Warehouse::whereIn('00_warehouse.warehouse_id', function($query){
            $query->select('00_organization.warehouse_id')->from('00_organization')->whereIn('00_organization.organization_name', function($query2){
                $query2->select('99_global_parameter.global_parameter_value')->from('99_global_parameter')->where('99_global_parameter.global_parameter_name','tokped_shop');
            });
        })->first();
        $ecommerce = Organization::where(['active_flag' => 1, 'organization_type_id' => 4])->get();
        $warehouse_by_id = ProductWarehouse::select('00_product_warehouse.product_warehouse_id','00_product_warehouse.prod_id','00_product_warehouse.minimum_stock','00_product_warehouse.prod_price','00_product_warehouse.prod_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_warehouse.warehouse_type_id')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('prod_id',$request->get('product'))
                            ->get();
        $product_variant = Product::where('active_flag',1)->where('variant_id',$request->get('product'))->get();
        // OLD
            // $warehouse_list = array_map(function($value){
            //     return $value['warehouse_id'];
            // }, $warehouse_by_id);
            // $warehouse_list = join(",", $warehouse_list);
        // OLD
        // $warehouse_list = array();
        // print_r($warehouse_by_id);
        // exit;
        $this->generateToken();
        // $tokopedia_store = Organization::where('organization_type_id',6)->where('parent_organization_id',28)->where('active_flag',1)->first();
        $etalase =$this->etalaseTokped();
        $tokopedia_category = CategoryEcommerce::select('00_category_per_ecommerce.category_id','00_category_per_ecommerce.category_name','category_id_ecommerce',DB::raw('(SELECT pc.category_id FROM 00_product_ecommerce AS pc WHERE pc.prod_id = "'.$request->get('product').'" LIMIT 1) AS tokped_category_id'))->join('00_category','00_category.category_id','=','00_category_per_ecommerce.category_id')->where('category_level',3)->where('organization_id',28)->get()->toArray();
        // echo "<pre>";
        // print_r($tokopedia_category);
        // exit;
        $product_ecommerce_by_id = ProductEcommerce::where('prod_id', $request->get('product'))->first();
        $tags_by_id = ProductTag::where('prod_id',$request->get('product'))->get()->toArray();
        $tags = Tag::where('active_flag',1)->get();
        $warehouse = Warehouse::where('00_warehouse.active_flag',1)->get();
        $data['warehouse'] = $warehouse;
        $data['warehouse_64'] = base64_encode($warehouse);
        $data['user'] = $user;
        $data['category'] = $category;
        $data['brand'] = $brand;
        $data['uom'] = $uom;
        $data['main_warehouse'] = $main_warehouse;
        $data['sku'] = $sku;
        $data['warehouse_list'] = $warehouse_by_id;
        $data['warehouse_list_64'] = base64_encode($warehouse_by_id);
        $data['product_based_on_id'] = $product_based_on_id;
        $data['ecommerce'] = $ecommerce;
        $data['product_variant'] = $product_variant;
        $data['etalase'] = $etalase;
        $data['tokopedia_category'] = $tokopedia_category;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],2)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['tags_by_id'] = $tags_by_id;
        $data['tags'] = $tags;
        $data['accessed_menu'] = $accessed_menu;
        $data['product_ecommerce_by_id'] = $product_ecommerce_by_id;
        return view('admin::products/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function addVariantForm(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'product_variant_name.*' => ['required'],
            // 'product_variant_length.*' => ['required'],
            // 'product_variant_width.*' => ['required'],
            // 'product_variant_height.*' => ['required'],
            // 'product_variant_picture.*' => ['required'],
            'product_variant_uom_value.*' => ['required'],
            'product_variant_uom_name.*' => ['required'],
            'bruto_value.*' => ['required']
        ]);

        $product_image_location = $request->input('product_image_temp');
        $parent_sku_id = $request->input('product_sku');
        $parent_id = $request->input('parent_id');
        if($validator->fails()){
            // echo 'blelele';
            // print_r($validator->messages());
            // exit;
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            // echo 'blablabla';
            // exit;
            // echo count($request->input('product_tag'));
            // print_r($request->all());
            // exit;

            $product_variant_names = $request->input('product_variant_name');
            $product_variant_list = array();
            if($product_variant_names != null){
                for ($b=0; $b < count($product_variant_names); $b++) {
                    if(isset($request->file('product_variant_picture')[$b])){
                        $file = $request->file('product_variant_picture')[$b];
                        // print_r($file);
                        $ext = $file == null ? '' : $file->getClientOriginalExtension();
                        $name = uniqid().'.'.$ext;
                        $product_image_location = $folder_name.$name;
                        if (!file_exists($folder_name)) {
                            mkdir($folder_name, 777, true);
                        }
                        ini_set('memory_limit', '256M');
                        Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                        chmod($product_image_location, 0777);
                    }else{
                        $product_image_location = $request->input('product_image_temp');
                    }
                    $inserted_variant_obj = [
                        'prod_name' => ucwords($request->input('product_variant_name')[$b]),
                        'prod_desc' => ($request->input('product_description') == null ? null : $request->input('product_description')),
                        'brand_id' => $request->input('product_brand'),
                        'preorder_flag' => 'N',
                        'product_sku_id' => $parent_sku_id,
                        'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                        'shipping_durability_min' => $request->input('shipping_durability_min'),
                        'shipping_durability_max' => $request->input('shipping_durability_max'),
                        'shipping_durability_id' => $request->input('shipping_durability_unit'),
                            // 'stockable_flag' => $request->input('stockable_flag'),
                            // 'stock' => $request->input('product_stock'),
                            // 'minimum_stock' => $request->input('minimum_stock'),
                        'minimum_order' => $request->input('minimum_order'),
                        'uom_id' => ($request->input('product_variant_uom_name')[$b] == null  || $request->input('product_variant_uom_name')[$b] == "" ? null : $request->input('product_variant_uom_name')[$b]),
                        'uom_value' => ($request->input('product_variant_uom_value')[$b] == null || $request->input('product_variant_uom_value')[$b] == ""  ? null : $request->input('product_variant_uom_value')[$b]),
                        'prod_image' => $product_image_location,
                        'bruto' => $request->input('bruto_value')[$b],
                        'bruto_uom_id' => $request->input('bruto_uom'),
                        'active_flag' => 1,
                        'variant_id' => $parent_id,
                        'stockable_flag' => ($request->input('stockable_flag')[$b] != null || $request->input('stockable_flag') != "") ? $request->input('stockable_flag')[$b] : 1,
                        'category_id' => $request->input('product_category'),
                        'is_taxable_product' => $request->input('is_taxable_product'),
                        'tax_value' => $request->input('tax_value') / 100,
                        'created_by' => Session::get('users')['id'],
                        'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                        'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                        'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                    ];
                    $product_variant_id = Product::insertGetId($inserted_variant_obj);

                    if($product_variant_id > 0){
                        $brand_id = $request->input('product_brand');
                        $user_id = Session::get('users')['id'];
                        $brand_id_temp = (string)$brand_id;
                        for ($i= 0; $i < 3; $i++) {
                            if($i >= strlen($brand_id_temp)){
                                $brand_id_temp = "0".$brand_id_temp;
                            }
                        }
                        $user_id_temp = (string)$user_id;
                        for ($i= 0; $i < 3; $i++) {
                            if($i >= strlen($user_id_temp)){
                                $user_id_temp = "0".$user_id_temp;
                            }
                        }
                        $product_id_temp = (string)$product_variant_id;
                        for ($i= 0; $i < 5; $i++) {
                            if($i >= strlen($product_id_temp)){
                                $product_id_temp = "0".$product_id_temp;
                            }
                        }
                        $product_code = "P".$brand_id_temp.$user_id_temp.$product_id_temp;
                        $parent_product_code = $product_code;
                        Product::where('prod_id',$product_variant_id)->update(['prod_code' => $product_code]);
                    }
                    $product_tag = $request->input('product_tag');
                    // echo count($product_tag);
                    // exit;
                    $product_tag_array = array();
                    for ($j=0; $j < count($product_tag); $j++) {
                        $obj = array(
                            "prod_id" => $product_variant_id,
                            "tag_id" => $product_tag[$j],
                            "active_flag" => 1,
                            "created_by" => Session::get('users')['id']
                        );
                        array_push($product_tag_array,$obj);
                    }
                    ProductTag::insert($product_tag_array);
                    $warehouse_array = array();
                    $warehouse = $request->input('product_warehouse');
                    $warehouse_price = $request->input('warehouse_price');
                    $warehouse_min_stock = $request->input('warehouse_min_stock');
                    for ($c=0; $c < count($warehouse); $c++) {
                        $obj = array(
                            "prod_id" => $product_variant_id,
                            "warehouse_id" => $warehouse[$c],
                            "prod_price" => (int)str_replace(".","",$warehouse_price[$c]),
                            "minimum_stock" => $warehouse_min_stock[$c],
                            "active_flag" => 1
                        );
                        array_push($warehouse_array,$obj);
                    }
                    ProductWarehouse::insert($warehouse_array);
                    array_push($product_variant_list, $inserted_variant_obj);
                }
                Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(Lang::get('notification.successfully'))]));
            }
            return redirect('admin/product');
        }
    }
    public function form(Request $request)
    {
        // print_r($request->all());
        // exit;
        // $b = 0;
        // echo ($request->input('stockable_value_flag')[$b] != null || $request->input('stockable_value_flag')[$b] != "") ? $request->input('stockable_value_flag')[$b] : 1;
        // exit;
        // $warehouse_price = $request->input('warehouse_price');
        // $warehouse_price = $request->input('warehouse_price');
        // $warehouse_price = array_map(function($a){
        //     return (int)(str_replace('.','',$a));
        // },$warehouse_price);
        // $warehouse_price = array_filter($warehouse_price,function($a){
        //     return $a > 0;
        // });
        // for ($b=0; $b < count($request->input('warehouse_price')); $b++) {
        //     array_push($warehouse_price,$request->input('warehouse_price')[$b]);
        // }
        // rsort($warehouse_price);
        // print_r($warehouse_price);
        // exit;
            $validator;

            $field = [
                'product_name' => ['required'],
                'product_category' => ['required'],
                'product_uom' => ['required'],
                'product_weight' => ['required',new NumberValidation($request->input('product_uom'))],
                // 'dim_length' => ['required'],
                // 'dim_width' => ['required'],
                // 'dim_height' => ['required'],
                // 'stockable_flag' => ['required'],
                // 'minimum_order' => ['required'],
                'product_description' => ['required'],
                'bruto' => ['required'],
                'product_tag' => ['required'],
                // 'product_stock' => ['required','numeric'],
                // 'minimum_stock' => ['required'],
                // 'product_price' => ['required'],
                'is_taxable_product' => ['required'],
                'tax_value' => ['required', 'max:100']
            ];
            if ($request->input('product_id') == null) {
                if ($request->input('parent_product') == null) {
                    $field['product_picture'] = ['required','mimes:jpeg,bmp,png,jpg,pneg'];
                    $field['minimum_order'] = ['required'];
                }
            } else {
                if($request->input('parent_product') == null && $request->file('product_picture') != null){
                    $field['product_picture'] = ['required','mimes:jpeg,bmp,png,jpg,pneg'];
                }
            }
            $validator = Validator::make($request->all(),$field);

            //OLD
                // if($request->input('product_id') == null){
                //     $validator = Validator::make($request->all(),[
                //         'product_name' => ['required'],
                //         'product_category' => ['required'],
                //         'product_uom' => ['required'],
                //         'product_weight' => ['required',new NumberValidation($request->input('product_uom'))],
                //         'dim_length' => ['required'],
                //         'dim_width' => ['required'],
                //         'dim_height' => ['required'],
                //         // 'stockable_flag' => ['required'],
                //         'minimum_order' => ['required'],
                //         'product_description' => ['required'],
                //         // 'product_stock' => ['required','numeric'],
                //         'product_price' => ['required'],
                //         // 'minimum_stock' => ['required'],
                //         'product_picture' => ['required','mimes:jpeg,bmp,png,jpg,pneg'],
                //         'is_taxable_product' => ['required'],
                //         'tax_value' => ['required', 'max:100']
                //     ]);
                // }else{
                //     if($request->file('product_picture') == null){
                //         $validator = Validator::make($request->all(),[
                //             'product_name' => ['required'],
                //             'product_category' => ['required'],
                //             'product_uom' => ['required'],
                //             'product_weight' => ['required',new NumberValidation($request->input('product_uom'))],
                //             'dim_length' => ['required'],
                //             'dim_width' => ['required'],
                //             'dim_height' => ['required'],
                //             // 'stockable_flag' => ['required'],
                //             'minimum_order' => ['required'],
                //             'product_description' => ['required'],
                //             // 'product_stock' => ['required','numeric'],
                //             // 'minimum_stock' => ['required'],
                //             'product_price' => ['required'],
                //             'is_taxable_product' => ['required'],
                //             'tax_value' => ['required', 'max:100']
                //         ]);
                //     }else{
                //         $validator = Validator::make($request->all(),[
                //             'product_name' => ['required'],
                //             'product_category' => ['required'],
                //             'product_uom' => ['required'],
                //             'product_weight' => ['required','numeric',new NumberValidation($request->input('product_uom'))],
                //             'dim_length' => ['required'],
                //             'dim_width' => ['required'],
                //             'dim_height' => ['required'],
                //             // 'stockable_flag' => ['required'],
                //             'minimum_order' => ['required'],
                //             'product_description' => ['required'],
                //             // 'product_stock' => ['required','numeric'],
                //             'product_price' => ['required'],
                //             // 'minimum_stock' => ['required'],
                //             'product_picture' => ['required','mimes:jpeg,bmp,png,jpg,pneg'],
                //             'is_taxable_product' => ['required'],
                //             'tax_value' => ['required', 'max:100']
                //         ]);
                //     }
                // }
            //OLD
            $tokopedia_error_list = array();
            if($validator->fails()){
                if($request->input('product_id') == null){
                    Session::flash('message_alert', Lang::get('notification.has been inserted', ['result' => strtolower(Lang::get('notification.inaccurately'))]));
                }else{
                    Session::flash('message_alert', Lang::get('notification.has been updated', ['result' => strtolower(Lang::get('notification.inaccurately'))]));
                }
                return redirect()->back()->withInput()->withErrors($validator);
            }else{
                // echo $request->input('product_variant_name') == null ? 0 : 1;
                // print_r($request->all());
                // exit;
                $environment = Config::get('env.env_name');
                $file = $request->file('product_picture');
                $folder_name = 'img/uploads/products/';

                $product_image_location = "";
                if($file != null && $request->input('parent_product') == null){
                    // print_r($file);
                    $ext = $file == null ? '' : $file->getClientOriginalExtension();
                    $name = uniqid().'.'.$ext;
                    $product_image_location = $folder_name.$name;
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                    chmod($product_image_location, 0777);
                }else{
                    $product_image_location = $request->input('product_image_temp');
                }

                // echo $product_image_location;
                // exit;
                // $inserted_obj['prod_image'] = $product_image_location;
                // if ($request->input('parent_product') == null and $file !== null) {
                //     $inserted_obj['prod_image'] = $product_image_location;

                // }
                $product_id = 0;
                $warehouse_parent_price = 0;
                if($request->input('product_id') == null){
                    $inserted_obj = [
                        'prod_name' => ucwords($request->input('product_name')),
                        'prod_desc' => $request->input('product_description'),
                        'brand_id' => $request->input('product_brand'),
                        'preorder_flag' => 'N',
                        'product_sku_id' => $request->input('product_sku') == null || $request->input('product_sku') == "" ? null : $request->input('product_sku'),
                        'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                        'shipping_durability_min' => $request->input('shipping_durability_min'),
                        'shipping_durability_max' => $request->input('shipping_durability_max'),
                        'shipping_durability_id' => $request->input('shipping_durability_unit'),
                        // 'stockable_flag' => $request->input('stockable_flag'),
                        // 'stock' => $request->input('product_stock'),
                        // 'minimum_stock' => $request->input('minimum_stock'),
                        'minimum_order' => $request->input('minimum_order'),
                        'uom_id' => ($request->input('product_uom') == null  || $request->input('product_uom') == "" ? null : $request->input('product_uom')),
                        'uom_value' => ($request->input('product_weight') == null || $request->input('product_weight') == ""  ? null : $request->input('product_weight')),
                        'prod_image' => $product_image_location,
                        'bruto' => $request->input('bruto'),
                        'bruto_uom_id' => $request->input('bruto_uom'),
                        'active_flag' => 1,
                        'variant_id' => ($request->input('parent_product') != null || $request->input('parent_product') != "") ? $request->input('parent_product') : null,
                        'stockable_flag' => ($request->input('stockable_flag') != null || $request->input('stockable_flag') != "") ? $request->input('stockable_flag') : 1,
                        'category_id' => $request->input('product_category'),
                        'is_taxable_product' => $request->input('is_taxable_product'),
                        'tax_value' => $request->input('tax_value') / 100,
                        'created_by' => Session::get('users')['id'],
                        'dim_length' => (($request->input('dim_length') == null || $request->input('dim_length') == "") ? null : $request->input('dim_length')),
                        'dim_width' => (($request->input('dim_width') == null || $request->input('dim_width') == "") ? null : $request->input('dim_width')),
                        'dim_height' => (($request->input('dim_height') == null || $request->input('dim_height') == "") ? null : $request->input('dim_height'))
                    ];
                    $product_id = Product::insertGetId($inserted_obj);
                    $parent_id = $product_id;
                    $parent_product_code = "";
                    $parent_sku_id = null;
                    $tokopedia = "OFF";
                    if($product_id > 0){
                        $brand_id = $request->input('product_brand');
                        $user_id = Session::get('users')['id'];
                        $brand_id_temp = (string)$brand_id;
                        for ($i= 0; $i < 3; $i++) {
                            if($i >= strlen($brand_id_temp)){
                                $brand_id_temp = "0".$brand_id_temp;
                            }
                        }
                        $user_id_temp = (string)$user_id;
                        for ($i= 0; $i < 3; $i++) {
                            if($i >= strlen($user_id_temp)){
                                $user_id_temp = "0".$user_id_temp;
                            }
                        }
                        $product_id_temp = (string)$product_id;
                        for ($i= 0; $i < 5; $i++) {
                            if($i >= strlen($product_id_temp)){
                                $product_id_temp = "0".$product_id_temp;
                            }
                        }
                        $product_code = "P".$brand_id_temp.$user_id_temp.$product_id_temp;
                        $parent_product_code = $product_code;
                        Product::where('prod_id',$product_id)->update(['prod_code' => $product_code]);

                        $ecommerce_list = $request->input('active_ecommerce_flag');
                        if($ecommerce_list != null){
                            for ($b=0; $b < count($ecommerce_list); $b++) {
                                if($ecommerce_list[$b] == 28){
                                    $tokopedia = "ON";
                                }
                            }
                        }
                        $environment = Config::get('env.env_name');
                        if($request->input('product_sku') != null || $request->input('product_sku') != ""){
                            Product::where('product_sku_id', $request->input('product_sku'))->where('prod_id', '<>', $request->input('product_id'))->update(['position_date' => date('Y-m-d H:i:s', strtotime('-2seconds'))]);
                            // StockKeepingUnit::join("00_product","00_product.product_sku_id","=","00_product_sku.product_sku_id")->where("00_product_sku.product_sku_id", $request->input('product_sku'))->update(["00_product_sku.product_sku_name" => ucwords($request->input('product_name')), "00_product_sku.product_sku_desc" => $request->input('product_description')]);

                            $parent_sku_id = $request->input('product_sku');
                            // Product::where('parent_prod_id', $request->input('product_id'))->update(["prod_name" => ucwords($request->input('product_name')), "prod_desc" => $request->input('product_description')]);
                        }else{
                            $sku_id = StockKeepingUnit::insertGetId([
                                'product_sku_name' => $request->input('product_name'),
                                'product_sku_desc' => $request->input('product_description_description'),
                            ]);
                            $parent_sku_id = $sku_id;
                            Product::where('prod_id',$product_id)->update(['product_sku_id' => $sku_id]);
                        }
                        $product_tag = $request->input('product_tag');
                        $product_tag_array = array();
                        for ($b=0; $b < count($product_tag); $b++) {
                            $obj = array(
                                "prod_id" => $product_id,
                                "tag_id" => $product_tag[$b],
                                "active_flag" => 1,
                                "created_by" => Session::get('users')['id']
                            );
                            array_push($product_tag_array,$obj);
                        }
                        ProductTag::insert($product_tag_array);
                        $warehouse_array = array();
                        $warehouse = $request->input('product_warehouse');
                        $warehouse_price = $request->input('warehouse_price');
                        $warehouse_min_stock = $request->input('warehouse_min_stock');
                        for ($b=0; $b < count($warehouse); $b++) {
                            if($warehouse[$b] == 1){
                                $warehouse_parent_price = str_replace('.','',$warehouse_price[$b]);
                            }
                            $obj = array(
                                "prod_id" => $product_id,
                                "warehouse_id" => $warehouse[$b],
                                "prod_price" => (int)str_replace(".","",$warehouse_price[$b]),
                                "minimum_stock" => $warehouse_min_stock[$b],
                                "active_flag" => 1
                            );
                            array_push($warehouse_array,$obj);
                        }
                        ProductWarehouse::insert($warehouse_array);
                        // $product_category = $request->input('product_category');
                        // $product_category_array = array();
                        // for ($b=0; $b < count($product_category); $b++) {
                        //     $obj = array(
                        //         "prod_id" => $product_id,
                        //         "category_id" => $product_category[$b],
                        //         "active_flag" => 1,
                        //         "created_by" => Session::get('users')['id']
                        //     );
                        //     array_push($product_category_array,$obj);
                        // }
                        // ProductCategory::insert($product_category_array);
                    }

                    $product_variant_names = $request->input('product_variant_name');
                    $product_variant_list = array();
                    // $warehouse_price = $request->input('warehouse_price');
                    // $warehouse_price = array_map(function($a){
                    //     return (int)(str_replace('.','',$a));
                    // },$warehouse_price);
                    // $warehouse_price = array_filter($warehouse_price,function($a){
                    //     return $a > 0;
                    // });
                    $variant[0] = array(
                        "name" => ltrim(explode("-",$request->input('product_name'))[1]),
                        "picture" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                        "stock" => 1,
                        "price" => $warehouse_parent_price,
                        "id" => $product_id,
                        "is_primary" => true
                    );
                    if($product_variant_names != null){
                        for ($b=0; $b < count($product_variant_names); $b++) {
                            // $file = $request->file('product_variant_picture')[$b];
                            // $folder_name = 'img/uploads/products/';

                            // $product_image_location = "";
                            if(isset($request->file('product_variant_picture')[$b])){
                                $file = $request->file('product_variant_picture')[$b];
                                // print_r($file);
                                $ext = $file == null ? '' : $file->getClientOriginalExtension();
                                $name = uniqid().'.'.$ext;
                                $product_image_location = $folder_name.$name;
                                if (!file_exists($folder_name)) {
                                    mkdir($folder_name, 777, true);
                                }
                                ini_set('memory_limit', '256M');
                                Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                                chmod($product_image_location, 0777);
                                // $old_picture = $request->input('product_variant_picture_temp')[$b];
                                // File::delete($old_picture);
                            }

                            $inserted_variant_obj = [
                                'prod_name' => trim(ucwords($request->input('product_variant_name')[$b])),
                                'prod_desc' => $request->input('product_description'),
                                'brand_id' => $request->input('product_brand'),
                                'preorder_flag' => 'N',
                                'product_sku_id' => $parent_sku_id,
                                'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                                'shipping_durability_min' => $request->input('shipping_durability_min'),
                                'shipping_durability_max' => $request->input('shipping_durability_max'),
                                'shipping_durability_id' => $request->input('shipping_durability_unit'),
                                    // 'stockable_flag' => $request->input('stockable_flag'),
                                    // 'stock' => $request->input('product_stock'),
                                    // 'minimum_stock' => $request->input('minimum_stock'),
                                'minimum_order' => $request->input('minimum_order'),
                                'uom_id' => ($request->input('product_variant_uom_name')[$b] == null  || $request->input('product_variant_uom_name')[$b] == "" ? null : $request->input('product_variant_uom_name')[$b]),
                                'uom_value' => ($request->input('product_variant_uom_value')[$b] == null || $request->input('product_variant_uom_value')[$b] == ""  ? null : $request->input('product_variant_uom_value')[$b]),
                                'prod_image' => $product_image_location,
                                'bruto' => $request->input('bruto_value')[$b],
                                'bruto_uom_id' => $request->input('bruto_uom'),
                                'active_flag' => 1,
                                'variant_id' => $parent_id,
                                'stockable_flag' => ($request->input('stockable_value_flag')[$b] != null || $request->input('stockable_value_flag')[$b] != "") ? $request->input('stockable_value_flag')[$b] : 1,
                                'category_id' => $request->input('product_category'),
                                'is_taxable_product' => $request->input('is_taxable_product'),
                                'tax_value' => $request->input('tax_value') / 100,
                                'created_by' => Session::get('users')['id']
                                // 'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                                // 'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                                // 'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                            ];
                            $product_variant_id = Product::insertGetId($inserted_variant_obj);

                            if($product_variant_id > 0){
                                $brand_id = $request->input('product_brand');
                                $user_id = Session::get('users')['id'];
                                $brand_id_temp = (string)$brand_id;
                                for ($i= 0; $i < 3; $i++) {
                                    if($i >= strlen($brand_id_temp)){
                                        $brand_id_temp = "0".$brand_id_temp;
                                    }
                                }
                                $user_id_temp = (string)$user_id;
                                for ($i= 0; $i < 3; $i++) {
                                    if($i >= strlen($user_id_temp)){
                                        $user_id_temp = "0".$user_id_temp;
                                    }
                                }
                                $product_id_temp = (string)$product_variant_id;
                                for ($i= 0; $i < 5; $i++) {
                                    if($i >= strlen($product_id_temp)){
                                        $product_id_temp = "0".$product_id_temp;
                                    }
                                }
                                $product_code = "P".$brand_id_temp.$user_id_temp.$product_id_temp;
                                $parent_product_code = $product_code;
                                Product::where('prod_id',$product_variant_id)->update(['prod_code' => $product_code]);
                            }
                            $product_tag = $request->input('product_tag');
                            $product_tag_array = array();
                            for ($c=0; $c < count($product_tag); $c++) {
                                $obj = array(
                                    "prod_id" => $product_variant_id,
                                    "tag_id" => $product_tag[$c],
                                    "active_flag" => 1,
                                    "created_by" => Session::get('users')['id']
                                );
                                array_push($product_tag_array,$obj);
                            }
                            ProductTag::insert($product_tag_array);
                            $warehouse_array = array();
                            $warehouse = $request->input('product_warehouse_id');
                            $product_warehouse_price = $request->input('product_warehouse_price');
                            $product_warehouse_min_stock = $request->input('product_warehouse_minimum_stock');
                            $product_warehouse_index = $request->input('product_warehouse_index');
                            $product_warehouse_main_index = $request->input('product_warehouse_main_index');
                            $product_warehouse_highest_price = str_replace('.','',$product_warehouse_price[0]);

                            for ($d=0; $d < count($product_warehouse_index); $d++) {
                                if($product_warehouse_main_index[$b] == $product_warehouse_index[$d]){
                                    if($warehouse[$d] == 1){
                                        $product_warehouse_highest_price = str_replace('.','',$product_warehouse_price[$d]);
                                    }
                                    $obj = array(
                                        "prod_id" => $product_variant_id,
                                        "warehouse_id" => $warehouse[$d],
                                        "prod_price" => (int)str_replace('.','',$product_warehouse_price[$d]),
                                        "minimum_stock" => $product_warehouse_min_stock[$d],
                                        "active_flag" => 1
                                    );
                                    array_push($warehouse_array,$obj);
                                }
                            }
                            // $product_warehouse_price = array_filter($product_warehouse_price,function($a) use($b){
                            //     return $a > 0;
                            // });
                            // rsort($product_warehouse_price);
                            // $product_warehouse_highest_price = $product_warehouse_price[0];
                            // $warehouse = $request->input('product_warehouse');
                            // $warehouse_price = $request->input('warehouse_price');
                            // $warehouse_min_stock = $request->input('warehouse_min_stock');
                            // for ($c=0; $c < count($warehouse); $c++) {
                            //     $obj = array(
                            //         "prod_id" => $product_variant_id,
                            //         "warehouse_id" => $warehouse[$c],
                            //         "prod_price" => str_replace(".","",$warehouse_price[$c]),
                            //         "minimum_stock" => $warehouse_min_stock[$c],
                            //         "active_flag" => 1
                            //     );
                            //     array_push($warehouse_array,$obj);
                            // }
                            ProductWarehouse::insert($warehouse_array);
                            array_push($product_variant_list, $inserted_variant_obj);
                            array_push($variant, array(
                                "name" => Str::contains($request->input('product_variant_name')[$b],'-') == true ? trim(explode('-',ucwords($request->input('product_variant_name')[$b]))[1]) : ltrim(ucwords($request->input('product_variant_name')[$b])),
                                "picture" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                                "stock" => 1,
                                "price" => (int)$product_warehouse_highest_price,
                                "id" => $product_variant_id,
                                "is_primary" => false
                            ));
                            $product_warehouse_highest_price = (int)str_replace('.','',$product_warehouse_price[0]);
                        }
                    }
                    if($tokopedia == "ON"){
                        $warehouse_price = $request->input('warehouse_price');
                        $warehouse_price = array_map(function($a){
                            return (int)(str_replace('.','',$a));
                        },$warehouse_price);
                        $warehouse_price = array_filter($warehouse_price,function($a){
                            return $a > 0;
                        });
                        // for ($b=0; $b < count($request->input('warehouse_price')); $b++) {
                        //     array_push($warehouse_price,$request->input('warehouse_price')[$b]);
                        // }
                        rsort($warehouse_price);
                        $inserted_tokped_obj = array(
                            "internal_id" => (int)$product_id,
                            "product_image" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                            "product_description" => $request->input('product_description'),
                            "product_name" => rtrim(explode('-',$request->input('product_name'))[0]),
                            "product_id" => null,
                            "parent_product_code" => $parent_product_code,
                            "bruto" => ($request->input('bruto')),
                            "tokopedia_category" => (int)$request->input('tokopedia_category'),
                            "product_price" => $warehouse_price[0],
                            "stock" => 1,
                            "variant" => $variant,
                            "etalase" => (int)$request->input('etalase')
                        );
                        $tokopedia_error_list = $this->push($inserted_tokped_obj);
                    }
                    // DB::table('00_product')->insertGetId($product_variant_list);
                    // print_r($product_variant_list);
                    // exit;
                    $alert = Lang::get('notification.has been inserted', ['result' => strtolower(Lang::get('notification.successfully'))]);
                    if(count($tokopedia_error_list) > 0){
                        $alert = Lang::get('notification.cannot_update_or_insert_to_tokopedia');
                        Session::flash('tokopedia_error_list',$tokopedia_error_list);
                    }
                    Session::flash('message_alert', $alert);
                    if($product_id > 0){
                        $product_id = $request->input('parent_product') == null ? $product_id : $request->input('parent_product');
                        // return redirect('admin/product/edit/?product='.$product_id);
                    }
                    return redirect('admin/product');
                }else{
                    $updated_obj = [
                        'prod_name' => $request->input('product_name'),
                        'prod_desc' => $request->input('product_description'),
                        'brand_id' => $request->input('product_brand'),
                        'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                        'shipping_durability_min' => $request->input('shipping_durability_min'),
                        'shipping_durability_max' => $request->input('shipping_durability_max'),
                        'shipping_durability_id' => $request->input('shipping_durability_unit'),
                        // 'stockable_flag' => $request->input('stockable_flag'),
                        // 'stock' => $request->input('product_stock'),
                        'variant_id' => ($request->input('parent_product') != null || $request->input('parent_product') != "") ? $request->input('parent_product') : null,
                        'stockable_flag' => $request->input('stockable_flag') != null || $request->input('stockable_flag') != "" ? $request->input('stockable_flag') : 1,
                        // 'minimum_stock' => $request->input('minimum_stock'),
                        'minimum_order' => $request->input('minimum_order'),
                        'position_date' => date('Y-m-d H:i:s'),
                        'uom_id' => $request->input('product_uom'),
                        'uom_value' => $request->input('product_weight'),
                        'category_id' => $request->input('product_category'),
                        'bruto' => $request->input('bruto'),
                        'prod_image' => $request->input('product_image_temp'),
                        'bruto_uom_id' => $request->input('bruto_uom'),
                        'is_taxable_product' => $request->input('is_taxable_product'),
                        'tax_value' => $request->input('tax_value') / 100,
                        'product_sku_id' => $request->input('product_sku') == null || $request->input('product_sku') == "" ? null : $request->input('product_sku'),
                        'updated_by' => Session::get('users')['id']
                        // 'dim_length' => (($request->input('dim_length') == null || $request->input('dim_length') == "") ? null : $request->input('dim_length')),
                        // 'dim_width' => (($request->input('dim_width') == null || $request->input('dim_width') == "") ? null : $request->input('dim_width')),
                        // 'dim_height' => (($request->input('dim_height') == null || $request->input('dim_height') == "") ? null : $request->input('dim_height'))
                    ];
                    $variant_obj = [
                        // "prod_name" => ucwords($request->input('product_name')),
                        "prod_desc" => $request->input('product_description'),
                        'category_id' => $request->input('product_category'),
                        'is_taxable_product' => $request->input('is_taxable_product'),
                        'tax_value' => $request->input('tax_value') / 100,
                        'prod_image' => $request->input('product_image_temp'),
                    ];
                    if($file !== null){
                        $ext = $file == null ? '' : $file->getClientOriginalExtension();
                        $name = uniqid().'.'.$ext;
                        if (!file_exists($folder_name)) {
                            mkdir($folder_name, 777, true);
                        }
                        ini_set('memory_limit', '256M');
                        Image::make($file)->fit(200,200)->save(public_path($folder_name.$name),80);
                        chmod($folder_name.$name, 0777);
                        $updated_obj = [
                            'prod_name' => $request->input('product_name'),
                            'prod_desc' => $request->input('product_description'),
                            'brand_id' => $request->input('product_brand'),
                            'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                            'shipping_durability_min' => $request->input('shipping_durability_min'),
                            'shipping_durability_max' => $request->input('shipping_durability_max'),
                            'shipping_durability_id' => $request->input('shipping_durability_unit'),
                            'variant_id' => ($request->input('parent_product') != null || $request->input('parent_product') != "") ? $request->input('parent_product') : null,
                            'stockable_flag' => $request->input('stockable_flag') != null || $request->input('stockable_flag') != "" ? $request->input('stockable_flag') : 1,
                            // 'stockable_flag' => $request->input('stockable_flag'),
                            // 'stock' => $request->input('product_stock'),
                            // 'minimum_stock' => $request->input('minimum_stock'),
                            'minimum_order' => $request->input('minimum_order'),
                            'uom_id' => $request->input('product_uom'),
                            'position_date' => date('Y-m-d H:i:s'),
                            'uom_value' => $request->input('product_weight'),
                            'category_id' => $request->input('product_category'),
                            'is_taxable_product' => $request->input('is_taxable_product'),
                            'tax_value' => $request->input('tax_value') / 100,
                            'bruto' => $request->input('bruto'),
                            'bruto_uom_id' => $request->input('bruto_uom'),
                            'prod_image' => $folder_name.$name,
                            'product_sku_id' => $request->input('product_sku') == null || $request->input('product_sku') == "" ? null : $request->input('product_sku'),
                            'updated_by' => Session::get('users')['id']
                            // 'dim_length' => (($request->input('dim_length') == null || $request->input('dim_length') == "") ? null : $request->input('dim_length')),
                            // 'dim_width' => (($request->input('dim_width') == null || $request->input('dim_width') == "") ? null : $request->input('dim_width')),
                            // 'dim_height' => (($request->input('dim_height') == null || $request->input('dim_height') == "") ? null : $request->input('dim_height'))
                        ];
                        $variant_obj = [
                            // "prod_name" => ucwords($request->input('product_name')),
                            "prod_desc" => $request->input('product_description'),
                            'category_id' => $request->input('product_category'),
                            'is_taxable_product' => $request->input('is_taxable_product'),
                            'tax_value' => $request->input('tax_value') / 100,
                            'prod_image' => $folder_name.$name,
                        ];
                    }
                    Product::where('prod_id',$request->input('product_id'))->update($updated_obj);
                    $tokopedia = "OFF";
                    $ecommerce_list = $request->input('active_ecommerce_flag');
                    if($ecommerce_list != null){
                        for ($b=0; $b < count($ecommerce_list); $b++) {
                            if($ecommerce_list[$b] == 28){
                                $tokopedia = "ON";
                            }
                        }
                    }
                    $product_tag = $request->input('product_tag');
                    $product_tag_array = array();
                    for ($b=0; $b < count($product_tag); $b++) {
                        $obj = array(
                            "prod_id" => $request->input('product_id'),
                            "tag_id" => $product_tag[$b],
                            "active_flag" => 1,
                            "created_by" => Session::get('users')['id']
                        );
                        array_push($product_tag_array,$obj);
                    }
                    ProductTag::where('prod_id',$request->input('product_id'))->delete();
                    ProductTag::insert($product_tag_array);
                    $warehouse_array = array();
                    $warehouse = $request->input('product_warehouse');
                    $product_id = $request->input('product_id');
                    $warehouse_price = $request->input('warehouse_price');
                    $warehouse_min_stock = $request->input('warehouse_min_stock');
                    for ($b=0; $b < count($warehouse); $b++) {
                        if($warehouse[$b] == 1){
                            $warehouse_parent_price = (int)str_replace('.','',$warehouse_price[$b]);
                        }
                        $obj = array(
                            "prod_id" => $product_id,
                            "warehouse_id" => $warehouse[$b],
                            "prod_price" => (int)str_replace(".","",$warehouse_price[$b]),
                            "minimum_stock" => $warehouse_min_stock[$b],
                            "active_flag" => 1
                        );
                        array_push($warehouse_array,$obj);
                    }
                    ProductWarehouse::where('prod_id',$request->input('product_id'))->delete();
                    ProductWarehouse::insert($warehouse_array);
                    if($request->input('product_sku') != null || $request->input('product_sku') != ""){
                        Product::where('product_sku_id', $request->input('product_sku'))->where('prod_id', '<>', $request->input('product_id'))->update(['position_date' => date('Y-m-d H:i:s', strtotime('-2seconds'))]);
                        // StockKeepingUnit::join("00_product","00_product.product_sku_id","=","00_product_sku.product_sku_id")->where("00_product_sku.product_sku_id", $request->input('product_sku'))->update(["00_product_sku.product_sku_name" => ucwords($request->input('product_name')), "00_product_sku.product_sku_desc" => $request->input('product_description')]);
                        Product::where('variant_id', $request->input('product_id'))->update($variant_obj);
                    }

                    $product_variant_ids = $request->input('product_variant_id');
                    $product_variant_list = array();
                    $parent_sku_id = $request->input('product_sku');
                    $parent_id = $request->input('product_id');
                    $warehouse_price = $request->input('warehouse_price');
                    $warehouse_price = array_map(function($a){
                        return (int)(str_replace('.','',$a));
                    },$warehouse_price);
                    $warehouse_price = array_filter($warehouse_price,function($a){
                        return $a > 0;
                    });
                    rsort($warehouse_price);
                    $variant[0] = array(
                        "name" => ltrim(explode('-',$request->input('product_name'))[1]),
                        "picture" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                        "stock" => 1,
                        "price" => $warehouse_parent_price,
                        "id" => $request->input('product_id'),
                        "is_primary" => true
                    );
                    // print_r(isset($request->file('product_variant_picture')[1]) ? 0 : 1);
                    // exit;
                    // print_r($product_variant_ids);
                    // exit;
                    if($product_variant_ids != null){
                        for ($b=0; $b < count($product_variant_ids); $b++) {
                            // print_r($request->file('product_variant_picture_temp')[1]);
                            // exit;
                            // $folder_name = 'img/uploads/products/';

                            // $product_image_location = "";
                            if(isset($product_variant_ids[$b]) || $product_variant_ids != null){
                                if(isset($request->file('product_variant_picture')[$b])){
                                    $file = $request->file('product_variant_picture')[$b];
                                    // print_r($file);
                                    $ext = $file == null ? '' : $file->getClientOriginalExtension();
                                    $name = uniqid().'.'.$ext;
                                    $product_image_location = $folder_name.$name;
                                    if (!file_exists($folder_name)) {
                                        mkdir($folder_name, 777, true);
                                    }
                                    ini_set('memory_limit', '256M');
                                    Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                                    chmod($product_image_location, 0777);
                                    // $old_picture = $request->input('product_variant_picture_temp')[$b];
                                    // File::delete($old_picture);
                                }else{
                                    $product_image_location = $request->input('product_variant_picture_temp')[$b];
                                }
                                $updated_variant_obj = [
                                    'prod_name' => ucwords($request->input('product_variant_name')[$b]),
                                    'prod_desc' => $request->input('product_description'),
                                    'brand_id' => $request->input('product_brand'),
                                    'preorder_flag' => 'N',
                                    'active_flag' => $request->input('product_active_status')[$b],
                                    'product_sku_id' => $parent_sku_id,
                                    'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                                    'shipping_durability_min' => $request->input('shipping_durability_min'),
                                    'shipping_durability_max' => $request->input('shipping_durability_max'),
                                    'shipping_durability_id' => $request->input('shipping_durability_unit'),
                                        // 'stockable_flag' => $request->input('stockable_flag'),
                                        // 'stock' => $request->input('product_stock'),
                                        // 'minimum_stock' => $request->input('minimum_stock'),
                                    'minimum_order' => $request->input('minimum_order'),
                                    'uom_id' => ($request->input('product_variant_uom_name')[$b] == null  || $request->input('product_variant_uom_name')[$b] == "" ? null : $request->input('product_variant_uom_name')[$b]),
                                    'uom_value' => ($request->input('product_variant_uom_value')[$b] == null || $request->input('product_variant_uom_value')[$b] == ""  ? null : $request->input('product_variant_uom_value')[$b]),
                                    'prod_image' => $product_image_location,
                                    'bruto' => $request->input('bruto'),
                                    'bruto_uom_id' => $request->input('bruto_uom'),
                                    // 'active_flag' => 1,
                                    'variant_id' => $parent_id,
                                    'stockable_flag' => ($request->input('stockable_flag') != null || $request->input('stockable_flag') != "") ? $request->input('stockable_flag') : 1,
                                    'category_id' => $request->input('product_category'),
                                    'is_taxable_product' => $request->input('is_taxable_product'),
                                    'tax_value' => $request->input('tax_value') / 100,
                                    'created_by' => Session::get('users')['id']
                                    // 'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                                    // 'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                                    // 'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                                ];
                                $product_variant_id = $product_variant_ids[$b];
    
                                Product::where('prod_id', $product_variant_id)->update($updated_variant_obj);
    
                                $product_tag = $request->input('product_tag');
                                $product_tag_array = array();
                                for ($c=0; $c < count($product_tag); $c++) {
                                    $obj = array(
                                        "prod_id" => $product_variant_id,
                                        "tag_id" => $product_tag[$c],
                                        "active_flag" => 1,
                                        "created_by" => Session::get('users')['id']
                                    );
                                    array_push($product_tag_array,$obj);
                                }
                                ProductTag::where('prod_id',$product_variant_id)->delete();
                                ProductTag::insert($product_tag_array);
                                $warehouse_array = array();
                                $warehouse = $request->input('product_warehouse_id');
                                $product_warehouse_price = $request->input('product_warehouse_price');
                                $product_warehouse_min_stock = $request->input('product_warehouse_minimum_stock');
                                $product_warehouse_main_index = $request->input('product_warehouse_main_index');
                                $product_warehouse_index = $request->input('product_warehouse_index');
                                $product_warehouse_highest_price = str_replace('.','',$product_warehouse_price[0]);
                                
                                // echo 'harga awal: '.$product_warehouse_highest_price.'<br>';
                                for ($d=0; $d < count($product_warehouse_index); $d++) {
                                    if($product_warehouse_main_index[$b] == $product_warehouse_index[$d]){
                                        if($warehouse[$d] == 1){
                                            $product_warehouse_highest_price = str_replace('.','',$product_warehouse_price[$d]);
                                        }
                                        $obj = array(
                                            "prod_id" => $product_variant_id,
                                            "warehouse_id" => $warehouse[$d],
                                            "prod_price" => (int)str_replace('.','',$product_warehouse_price[$d]),
                                            "minimum_stock" => $product_warehouse_min_stock[$d],
                                            "active_flag" => 1
                                        );
                                        array_push($warehouse_array,$obj);
                                    }
                                }
                                // echo 'harga akhir: '.$product_warehouse_highest_price.'<br>';
                                // if($b > 0){
                                //     print_r($warehouse_array);
                                //     exit;
                                // }
                                array_push($variant,array(
                                    "name" => Str::contains($request->input('product_variant_name')[$b],'-') == true ? trim(explode('-',ucwords($request->input('product_variant_name')[$b]))[1]) : ltrim(ucwords($request->input('product_variant_name')[$b])),
                                    "picture" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                                    "stock" => 1,
                                    "price" => (int)$product_warehouse_highest_price,
                                    "id" => $product_variant_ids[$b],
                                    "is_primary" => false
                                ));
                                //     if($product_variant_id > 0){
                                //         $brand_id = $request->input('product_brand');
                                //         $user_id = Session::get('users')['id'];
                                //         $brand_id_temp = (string)$brand_id;
                                //         for ($i= 0; $i < 3; $i++) {
                                //             if($i >= strlen($brand_id_temp)){
                                //                 $brand_id_temp = "0".$brand_id_temp;
                                //             }
                                //         }
                                //         $user_id_temp = (string)$user_id;
                                //         for ($i= 0; $i < 3; $i++) {
                                //             if($i >= strlen($user_id_temp)){
                                //                 $user_id_temp = "0".$user_id_temp;
                                //             }
                                //         }
                                //         $product_id_temp = (string)$product_variant_id;
                                //         for ($i= 0; $i < 5; $i++) {
                                //             if($i >= strlen($product_id_temp)){
                                //                 $product_id_temp = "0".$product_id_temp;
                                //             }
                                //         }
                                //         $product_code = "P".$brand_id_temp.$user_id_temp.$product_id_temp;
                                //         $parent_product_code = $product_code;
                                //         Product::where('prod_id',$product_variant_id)->update(['prod_code' => $product_code]);
                                //     }
                                //     $product_tag = $request->input('product_tag');
                                //     $product_tag_array = array();
                                //     for ($c=0; $c < count($product_tag); $c++) {
                                //         $obj = array(
                                //             "prod_id" => $product_variant_id,
                                //             "tag_id" => $product_tag[$c],
                                //             "active_flag" => 1,
                                //             "created_by" => Session::get('users')['id']
                                //         );
                                //         array_push($product_tag_array,$obj);
                                //     }
                                //     ProductTag::insert($product_tag_array);
                                    // $warehouse_array = array();
                                    // $warehouse = $request->input('product_warehouse');
                                    // $warehouse_price = $request->input('warehouse_price');
                                    // $warehouse_min_stock = $request->input('warehouse_min_stock');
                                    // for ($c=0; $c < count($warehouse); $c++) {
                                    //     $obj = array(
                                    //         "prod_id" => $product_variant_id,
                                    //         "warehouse_id" => $warehouse[$c],
                                    //         "prod_price" => str_replace(".","",$warehouse_price[$c]),
                                    //         "minimum_stock" => $warehouse_min_stock[$c],
                                    //         "active_flag" => 1
                                    //     );
                                    //     array_push($warehouse_array,$obj);
                                    // }
                                ProductWarehouse::where('prod_id',$product_variant_id)->delete();
                                ProductWarehouse::insert($warehouse_array);
                            }
                        //     // array_push($product_variant_list, $inserted_variant_obj);
                        }
                    }
                    // print_r($variant);
                    // exit;

                    $product_variant_names = $request->input('product_variant_name');
                    $product_variant_list = array();
                    if($product_variant_names != null){
                        for ($b=0; $b < count($product_variant_names); $b++) {
                            if(!isset($request->input('product_variant_id')[$b])){
                                if(isset($request->file('product_variant_picture')[$b])){
                                    $file = $request->file('product_variant_picture')[$b];
                                    // print_r($file);
                                    $ext = $file == null ? '' : $file->getClientOriginalExtension();
                                    $name = uniqid().'.'.$ext;
                                    $product_image_location = $folder_name.$name;
                                    if (!file_exists($folder_name)) {
                                        mkdir($folder_name, 777, true);
                                    }
                                    ini_set('memory_limit', '256M');
                                    Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                                    chmod($product_image_location, 0777);
                                }
                                $inserted_variant_obj = [
                                    'prod_name' => ucwords($request->input('product_variant_name')[$b]),
                                    'prod_desc' => ($request->input('product_description') == null ? null : $request->input('product_description')),
                                    'brand_id' => $request->input('product_brand'),
                                    'preorder_flag' => 'N',
                                    'product_sku_id' => $parent_sku_id,
                                    'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                                    'shipping_durability_min' => $request->input('shipping_durability_min'),
                                    'shipping_durability_max' => $request->input('shipping_durability_max'),
                                    'shipping_durability_id' => $request->input('shipping_durability_unit'),
                                        // 'stockable_flag' => $request->input('stockable_flag'),
                                        // 'stock' => $request->input('product_stock'),
                                        // 'minimum_stock' => $request->input('minimum_stock'),
                                    'minimum_order' => $request->input('minimum_order'),
                                    'uom_id' => ($request->input('product_variant_uom_name')[$b] == null  || $request->input('product_variant_uom_name')[$b] == "" ? null : $request->input('product_variant_uom_name')[$b]),
                                    'uom_value' => ($request->input('product_variant_uom_value')[$b] == null || $request->input('product_variant_uom_value')[$b] == ""  ? null : $request->input('product_variant_uom_value')[$b]),
                                    'prod_image' => $product_image_location,
                                    'bruto' => $request->input('bruto_value')[$b],
                                    'bruto_uom_id' => $request->input('bruto_uom'),
                                    'active_flag' => 1,
                                    'variant_id' => $parent_id,
                                    'stockable_flag' => ($request->input('stockable_value_flag')[$b] != null || $request->input('stockable_value_flag')[$b] != "" ) ? $request->input('stockable_value_flag')[$b] : 1,
                                    'category_id' => $request->input('product_category'),
                                    'is_taxable_product' => $request->input('is_taxable_product'),
                                    'tax_value' => $request->input('tax_value') / 100,
                                    'created_by' => Session::get('users')['id']
                                    // 'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                                    // 'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                                    // 'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                                ];
                                $product_variant_id = Product::insertGetId($inserted_variant_obj);
    
                                if($product_variant_id > 0){
                                    $brand_id = $request->input('product_brand');
                                    $user_id = Session::get('users')['id'];
                                    $brand_id_temp = (string)$brand_id;
                                    for ($i= 0; $i < 3; $i++) {
                                        if($i >= strlen($brand_id_temp)){
                                            $brand_id_temp = "0".$brand_id_temp;
                                        }
                                    }
                                    $user_id_temp = (string)$user_id;
                                    for ($i= 0; $i < 3; $i++) {
                                        if($i >= strlen($user_id_temp)){
                                            $user_id_temp = "0".$user_id_temp;
                                        }
                                    }
                                    $product_id_temp = (string)$product_variant_id;
                                    for ($i= 0; $i < 5; $i++) {
                                        if($i >= strlen($product_id_temp)){
                                            $product_id_temp = "0".$product_id_temp;
                                        }
                                    }
                                    $product_code = "P".$brand_id_temp.$user_id_temp.$product_id_temp;
                                    $parent_product_code = $product_code;
                                    Product::where('prod_id',$product_variant_id)->update(['prod_code' => $product_code]);
                                }
                                $product_tag = $request->input('product_tag');
                                // echo count($product_tag);
                                // exit;
                                $product_tag_array = array();
                                for ($j=0; $j < count($product_tag); $j++) {
                                    $obj = array(
                                        "prod_id" => $product_variant_id,
                                        "tag_id" => $product_tag[$j],
                                        "active_flag" => 1,
                                        "created_by" => Session::get('users')['id']
                                    );
                                    array_push($product_tag_array,$obj);
                                }
                                ProductTag::insert($product_tag_array);
                                $warehouse_array = array();
                                $warehouse = $request->input('product_warehouse_id');
                                $product_warehouse_price = $request->input('product_warehouse_price');
                                $product_warehouse_min_stock = $request->input('product_warehouse_minimum_stock');
                                $product_warehouse_main_index = $request->input('product_warehouse_main_index');
                                $product_warehouse_index = $request->input('product_warehouse_index');
                                $product_warehouse_highest_price = (int)str_replace('.','',$product_warehouse_price[0]);

                                for ($d=0; $d < count($product_warehouse_index); $d++) {
                                    // echo $product_warehouse_index[$d]."<br>";
                                    if($product_warehouse_main_index[$b] == $product_warehouse_index[$d]){
                                        // echo "yeeesss";
                                        if($warehouse[$d] == 1){
                                            $product_warehouse_highest_price = (int)str_replace('.','',$product_warehouse_price[$d]);
                                        }
                                        $obj = array(
                                            "prod_id" => $product_variant_id,
                                            "warehouse_id" => $warehouse[$d],
                                            "prod_price" => (int)str_replace('.','',$product_warehouse_price[$d]),
                                            "minimum_stock" => $product_warehouse_min_stock[$d],
                                            "active_flag" => 1
                                        );
                                        array_push($warehouse_array,$obj);
                                    }
                                    // if($d == count($product_warehouse_index) - 1){
                                    //     echo "<br>";
                                    // }
                                }
                                // echo $b;
                                // print_r($warehouse_array);
                                // exit;
                                ProductWarehouse::insert($warehouse_array);
                                // exit;
                                array_push($product_variant_list, $inserted_variant_obj);
                                array_push($variant,array(
                                    "name" => Str::contains($request->input('product_variant_name')[$b],'-') == true ? trim(explode('-',ucwords($request->input('product_variant_name')[$b]))[1]) : ltrim(ucwords($request->input('product_variant_name')[$b])),
                                    "picture" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                                    "stock" => 1,
                                    "price" => (int)$product_warehouse_highest_price,
                                    "id" => $product_variant_id,
                                    "is_primary" => false
                                ));
                            }
                        }
                    }
                    // print_r($variant);
                    // exit;

                    if($tokopedia == "ON"){
                        $warehouse_price = $request->input('warehouse_price');
                        $warehouse_price = array_map(function($a){
                            return (int)(str_replace('.','',$a));
                        },$warehouse_price);
                        $warehouse_price = array_filter($warehouse_price,function($a){
                            return $a > 0;
                        });
                        rsort($warehouse_price);
                        $updated_tokped_obj = array(
                            "internal_id" => (int)$request->input('product_id'),
                            "product_image" => (strcasecmp($environment,"local") == 0 ? "http://yukmarket.id/img/ecomm-default.png" : (request()->root().'/'.$product_image_location)),
                            "product_description" => $request->input('product_description'),
                            "product_name" => rtrim(explode('-',$request->input('product_name'))[0]),
                            "product_id" => $request->input('product_ecommerce_code'),
                            "parent_product_code" => $request->input('product_code'),
                            "bruto" => ($request->input('bruto')),
                            "tokopedia_category" => (int)$request->input('tokopedia_category'),
                            "product_price" => $warehouse_price[0],
                            "stock" => $request->input('total_stock'),
                            "variant" => $variant,
                            "etalase" => (int)$request->input('etalase')
                        );
                        // dd($updated_tokped_obj);
                        // exit;
                        $tokopedia_error_list = $this->push($updated_tokped_obj);
                    }
                    
                    // $alert = strtolower(Lang::get('notification.successfully'));
                    $alert = Lang::get('notification.has been updated', ['result' => strtolower(Lang::get('notification.successfully'))]);
                    if(count($tokopedia_error_list) > 0){
                        $alert = Lang::get('notification.cannot_update_or_insert_to_tokopedia');
                        Session::flash('tokopedia_error_list', $tokopedia_error_list);
                    }
                    Session::flash('message_alert', $alert);
                    // print_r($error_list);
                    // exit;
                }
                // print_r($tokopedia_error_list);
                // exit;
                // return redirect('admin/product');
                return redirect('admin/product');
            }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = Product::where('prod_id',$request->get('id'))->orWhere('variant_id',$request->get('id'))->update(['active_flag' => 1]);
        $alert = Lang::get('notification.has been activated', ['result' => strtolower(Lang::get('notification.successfully'))]);
        $activate_tokopedia = app('App\Http\Controllers\ProductECommerceController')->active($request->get("id"));
        if(!$update){
            $alert = Lang::get('notification.has been activated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/product');
    }

    public function activateStockKeepingUnit(Request $request)
    {
        StockKeepingUnit::where('product_sku_id',$request->get('id'))->update(['active_flag' => 1]);
        return redirect('admin/sku');
    }

    public function deactivate(Request $request)
    {
        $update = Product::where('prod_id',$request->get('id'))->orWhere('variant_id',$request->get('id'))->update(['active_flag' => 0]);
        $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.successfully'))]);
        $deactivate_tokopedia = app('App\Http\Controllers\ProductECommerceController')->inactive($request->get("id"));
        if(!$update){
            $alert = Lang::get('notification.has been deactivated', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/product');
    }

    public function delete(Request $request)
    {
        $update = Product::where('prod_id',$request->get('id'))->orWhere('variant_id',$request->get('id'))->update(['active_flag' => -1]);
        $alert = Lang::get('notification.has been deleted', ['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been deleted', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }else{
            $deleteTokped = app('App\Http\Controllers\ProductECommerceController')->deleteProductTokped($request->get('id'));
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }

    public function changeVariantPricePerWarehouse(Request $request){
        // print_r($request->all());
        // exit;
        $product_variant_ids = $request->input('product_variant_id');
        $variant_warehouse_price = $request->input('variant_warehouse_price');
        $variant_minimum_stock = $request->input('variant_warehouse_minimum_stock');
        if($product_variant_ids != null){
            for ($b=0; $b < count($product_variant_ids); $b++) {
                $product_warehouse = ProductWarehouse::where('prod_id', $product_variant_ids[$b])->where('warehouse_id', $request->input('warehouse_list'))->first();

                if($product_warehouse != null){
                    // echo "blablabla";
                    // exit;
                    ProductWarehouse::where('prod_id', $product_variant_ids[$b])->where('warehouse_id', $request->input('warehouse_list'))->delete();
                }
                ProductWarehouse::insert(array(
                    'prod_id' => $product_variant_ids[$b],
                    'prod_price' => (int)$variant_warehouse_price[$b],
                    'warehouse_id' => (int)$request->input('warehouse_list'),
                    'minimum_stock' => (int)$variant_minimum_stock[$b],
                    'active_flag' => 1
                ));
            }
        }

        $variant = NULL;
        $variant[0] = array(
            "name" => explode("-",$request->input('parent_product_name'))[1],
            "picture" => "http://yukmarket.id/img/ecomm-default.png",
            "stock" => 1,
            "price" => (int)str_replace('.','',$request->input('parent_warehouse_price')),
            "id" => $request->input('parent_product_id'),
            "is_primary" => true
        );
        if($product_variant_ids != null){
            for ($b=0; $b < count($product_variant_ids); $b++) {
                // print_r($request->file('product_variant_picture_temp')[1]);
                // exit;
                // $folder_name = 'img/uploads/products/';

                $product_image_location = "";
                if(isset($request->file('product_variant_picture')[$b])){
                    $file = $request->file('product_variant_picture')[$b];
                    // print_r($file);
                    $ext = $file == null ? '' : $file->getClientOriginalExtension();
                    $name = uniqid().'.'.$ext;
                    $product_image_location = $folder_name.$name;
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(300,300)->save(public_path($product_image_location),80);
                    chmod($product_image_location, 0777);
                    // $old_picture = $request->input('product_variant_picture_temp')[$b];
                    // File::delete($old_picture);
                }else{
                    $product_image_location = $request->input('product_variant_picture_temp')[$b];
                }
                array_push($variant,array(
                    "name" => ltrim(ucwords($request->input('variant_name')[$b])),
                    "picture" => "http://yukmarket.id/img/ecomm-default.png",
                    "stock" => 1,
                    "price" => (int)$variant_warehouse_price[$b],
                    "id" => $product_variant_ids[$b],
                    "is_primary" => false
                ));
            }
        }
        // dd($variant);
        // exit;
        $tokopedia = $request->input('tokopedia');
        if($tokopedia != NULL){
            $tokped_obj = array(
                "internal_id" => $request->input('parent_product_id'),
                "product_image" => "http://yukmarket.id/img/ecomm-default.png",
                "product_description" => $request->input('product_description'),
                "product_name" => rtrim(explode('-',$request->input('parent_product_name'))[0]),
                "product_id" => ($request->input('product_ecommerce_code') == NULL ? NULL : $request->input('product_ecommerce_code')),
                "parent_product_code" => $request->input('product_code'),
                "bruto" => $request->input('bruto'),
                "tokopedia_category" => $request->input('tokopedia_category'),
                "product_price" => (int)str_replace('.','',$request->input('parent_warehouse_price')),
                "stock" => 1,
                "variant" => $variant,
                "etalase" => $request->input('etalase')
            );
            // dd($tokped_obj);
            // exit;
            $tokopedia_error_list = $this->push($tokped_obj);
        }

        return redirect()->back();
    }

    public function deactivateStockKeepingUnit(Request $request)
    {
        StockKeepingUnit::where('product_sku_id',$request->get('id'))->update(['active_flag' => 0]);
        return redirect('admin/sku');
    }

    public function checkUom(Request $request)
    {
        $convert = new UomConvert();
        return $convert->checkUom($request->satuanMaster, $request->valueMaster, $request->satuan, $request->value);
    }

    public function prodWh($prod_id)
    {
        $warehouse_by_id = ProductWarehouse::select('00_product_warehouse.product_warehouse_id','00_product_warehouse.prod_id','00_product_warehouse.minimum_stock','00_product_warehouse.prod_price','00_product_warehouse.prod_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('prod_id',$prod_id)
                            ->get();

        return response()->json($warehouse_by_id);
        // return DataTables::of($product_array)->make(true);
    }

    private $token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJpbnRpc2luZXJnaXRla25vbG9naSIsImF1ZCI6InVzZXIiLCJzdWIiOiJhZG1pbkB0ZXN0LmNvbSIsImV4cCI6MTYwNTg1ODkzMiwicm9sZSI6MSwiaWQiOjEsImZ1bGxOYW1lIjoiYWRtaW4ifQ.v25xCYFWy_l_6NG1sixyXZGOGvE_DzP7RgSY1AWPW58CgfcBRTTDCATr-U1zdHc-5neqDcpgyGUAxfgLFmuwxw";

    public function generateToken(){
        $credential=$this->getGlobalParameter('user_token_integrator');
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);
        } catch (ConnectionException $e) {
            $token=null;
        }
        $this->token=$token;
    }

    public function push($parameter){
        // echo "<pre>";
        // dd($parameter);
        // exit;
        $this->generateToken();
        $shopConfig = $this->getShopName();
        $id = (int)$parameter['product_id'];
        $product_image_location = $parameter['product_image'];
        $description = $parameter['product_description'];
        // $folder_name = "img/uploads/products/";
        // $file = $request->file('product_variant_picture')[$b];
        // print_r($file);
        // $ext = $uploaded_file == null ? '' : $uploaded_file->getClientOriginalExtension();
        // $name = uniqid().'.'.$ext;
        // $product_image_location = $folder_name.$name;
        // if (!file_exists($folder_name)) {
        //     mkdir($folder_name, 777, true);
        // }
        // // ini_set('memory_limit', '256M');
        // Image::make($uploaded_file)->fit(300,300)->save(public_path($product_image_location),80);
        // chmod($product_image_location, 0777);

        // $product_image_location = request()->root().'/'.$product_image_location;
        $product_weight = $this->getBeratProductTokped($parameter['tokopedia_category']);
        $product_weight = array_filter($product_weight, function($value){
            return $value['variant_id'] == 1;
        });
        $product_weight = array_values($product_weight);
        $product_weight_unit = $product_weight[0]['units'][0]['values'];
        $product_variants = array();
        $product_combinations = array();
        $product_pictures = array();
        for ($b=0; $b < count($parameter['variant']); $b++) {
            $obj = (Object)array(
                "hex_code" => "",
                "unit_value_id" => $product_weight_unit[$b]['value_id'],
                "value" => $parameter['variant'][$b]['name']
            );
            array_push($product_variants, $obj);
            if($b == 0){
                array_push($product_pictures, (Object)array(
                    "file_path" => $parameter['variant'][$b]['picture']
                ));
            }

            $product_detail = Product::where('prod_id', $parameter['variant'][$b]['id'])->first();
            $stock_query = "";
            if($product_detail != null){
                if($product_detail['stockable_flag'] == 1){
                    $stock_query .= 'SELECT SUM((00_inventory.stock - (CASE WHEN 00_inventory.booking_qty IS NULL THEN 0 ELSE 00_inventory.booking_qty END) - (CASE WHEN 00_inventory.goods_in_transit IS NULL THEN 0 ELSE 00_inventory.goods_in_transit END) - (CASE WHEN 00_inventory.goods_arrived IS NULL THEN 0 ELSE 00_inventory.goods_arrived END))) AS stock FROM 00_inventory WHERE 00_inventory.organization_id IN (';
                    $stock_query .= 'SELECT parent_organization_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                    WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ') AND (00_inventory.warehouse_id IN (';
                    $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                            WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ') OR 00_inventory.warehouse_id in(';
                    $stock_query .= 'SELECT warehouse_id FROM 00_warehouse WHERE 00_warehouse.parent_warehouse_id IN (';
                    $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ')';
                    $stock_query .= ')) AND 00_inventory.prod_id = '.$parameter['variant'][$b]['id'].' LIMIT 1;';
                }else{
                    $stock_query .= 'SELECT SUM((00_inventory.stock - (CASE WHEN 00_inventory.booking_qty IS NULL THEN 0 ELSE 00_inventory.booking_qty END) - (CASE WHEN 00_inventory.goods_in_transit IS NULL THEN 0 ELSE 00_inventory.goods_in_transit END) - (CASE WHEN 00_inventory.goods_arrived IS NULL THEN 0 ELSE 00_inventory.goods_arrived END))) AS stock FROM 00_inventory WHERE 00_inventory.organization_id IN (';
                    $stock_query .= 'SELECT parent_organization_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                    WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ') AND (00_inventory.warehouse_id IN (';
                    $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                            WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ') OR 00_inventory.warehouse_id in(';
                    $stock_query .= 'SELECT warehouse_id FROM 00_warehouse WHERE 00_warehouse.parent_warehouse_id IN (';
                    $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
                    $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb WHERE glb.global_parameter_name = "tokped_shop"';
                    $stock_query .= ')';
                    $stock_query .= ')';
                    $stock_query .= ')) AND 00_inventory.prod_id IN (SELECT 00_product.variant_id FROM 00_product WHERE 00_product.prod_id = '.$parameter['variant'][$b]['id'].') LIMIT 1;';
                }
            }
            $variant_stock = DB::select(DB::raw($stock_query));
            $stock = $variant_stock == null ? ($parameter['stock'] == 0 ? 1 : $parameter['stock']) : ($variant_stock[0]->stock == 0 ? 1 : $variant_stock[0]->stock);
            array_push($product_combinations, (Object)array(
                "combination" => [$b],
                "is_primary" => $parameter['variant'][$b]['is_primary'],
                "pictures" => array(
                    ["file_path" => $parameter['variant'][$b]['picture']]
                ),
                "price" => (int)(isset($parameter['variant'][$b]['price']) ? $parameter['variant'][$b]['price'] : $parameter['product_price']),
                "sku" => $parameter['parent_product_code'],
                "status" => "LIMITED",
                "stock" => (int)$stock
            ));
        }
        $product_variants = array((Object)array(
            "id" => 1,
            "options" => $product_variants,
            "unit_id" => 0
        ));

        // $variant_pictures = $parameter['variants'];
        $dbShopId=Organization::where('organization_name',$shopConfig->global_parameter_value)->first();
        $dbEcomId=Organization::where('organization_name','TOKOPEDIA')->first();
        // echo 'test';
        // exit;
        $preorder=(Object)['duration'=>0,'is_active'=>false,'time_unit'=>'DAY'];
        $tokopedia_store = Organization::where('organization_type_id',6)->where('parent_organization_id',28)->where('active_flag',1)->first();
        $etalase =(Object)array('id' => (int)$parameter['etalase']);
        // print_r($etalase);
        // exit;
        //------------------------------------form select
        $picture1=(Object)['file_path' => $product_image_location];//---------------------------------parent/primary product
        $pictures = array($picture1);
        $prodName = $parameter['product_name'];
        $sku = $parameter['parent_product_code'];
        $file_path_array = array();
        $combination = array();
        $productsVarianArray = array();
        // $uploaded_variant_file = $request->input('product_variant_image');
        $selectionId=54;
        $selectionUnitId=63;
        $bruto = ($parameter['bruto']) * 1000;
        // echo $bruto;
        // exit;
        $tokopedia_category_id = $parameter['tokopedia_category'];
        // echo $internal_category_id;
        // exit;
        // $category = CategoryEcommerce::where('category_id', $internal_category_id)->first();
        // print_r($category);
        // exit;
        // $ecommerce_category_id = $category['category_id_ecommerce'];

        // for ($k=0; $k < count($uploaded_variant_file); $k++) {
        //     $image_obj = array('file_path' => '');
        //     $selectionOptionObject = array(
        //         'hex_code'=>"",//-------------------------------string empty
        //         'unit_value_id'=>838,//------------------------form id berat
        //         'value'=>'varian 1 gram2'//------------------------------varian name
        //     );
        //     if(isset($uploaded_variant_file[$k])){
        //         $uploaded_file = $request->file('product_image');
        //         // $file = $request->file('product_variant_picture')[$b];
        //         // print_r($file);
        //         $ext = $uploaded_file == null ? '' : $uploaded_file->getClientOriginalExtension();
        //         $name = uniqid().'.'.$ext;
        //         $product_variant_image_location = $folder_name.$name;
        //         if (!file_exists($folder_name)) {
        //             mkdir($folder_name, 777, true);
        //         }
        //         // ini_set('memory_limit', '256M');
        //         Image::make($uploaded_file)->fit(300,300)->save(public_path($product_variant_image_location),80);
        //         chmod($product_variant_image_location, 0777);
        //         $image_obj = array('file_path' => $product_variant_image_location);
        //         // array_push($file_path_array, $obj);
        //         // array_push($obj);

        //     }
        //     $obj = array(
        //         "combination" => array($k),
        //         "is_primary" => $k == 0 ? true : false,
        //         "pictures" => $image_obj,
        //         "price" => 1000,
        //         "sku" => $sku,
        //         "status" => "LIMITED",
        //         "stock" => $request->input('stock')
        //     );
        //     array_push($productsVarianArray, $obj);
        //     array_push($selectionOptionArray, $selectionOptionObject);

        //     // array_push($combination, array($k));
        // }
        $variant = null;
        if($parameter['variant'] != null || count($parameter['variant']) > 0){
            if(count($parameter['variant']) == 1){
                $prodName = $prodName . ' - ' .$parameter['variant'][0]['name'];
                $variant = null;
            }else{
                $variant = (Object)array(
                    "products" => $product_combinations,
                    "selection" => $product_variants,
                    "sizecharts" => []
                );
            }
        }
        $stock = 1;
        $stock_query = "";
        if($parameter['internal_id'] != null || isset($parameter['internal_id'])){
            // $product_detail = Product::where('prod_id', $parameter['internal_id'])->first();
            $stock_query .= 'SELECT SUM((00_inventory.stock - (CASE WHEN 00_inventory.booking_qty IS NULL THEN 0 ELSE 00_inventory.booking_qty END) - (CASE WHEN 00_inventory.goods_in_transit IS NULL THEN 0 ELSE 00_inventory.goods_in_transit END) - (CASE WHEN 00_inventory.goods_arrived IS NULL THEN 0 ELSE 00_inventory.goods_arrived END))) AS stock FROM 00_inventory WHERE 00_inventory.organization_id IN (';
            $stock_query .= 'SELECT parent_organization_id FROM 00_organization AS org WHERE org.organization_name IN (';
            $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                    WHERE glb.global_parameter_name = "tokped_shop"';
            $stock_query .= ')';
            $stock_query .= ') AND (00_inventory.warehouse_id IN (';
            $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
            $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb
                            WHERE glb.global_parameter_name = "tokped_shop"';
            $stock_query .= ')';
            $stock_query .= ') OR 00_inventory.warehouse_id in(';
            $stock_query .= 'SELECT warehouse_id FROM 00_warehouse WHERE 00_warehouse.parent_warehouse_id IN (';
            $stock_query .= 'SELECT warehouse_id FROM 00_organization AS org WHERE org.organization_name IN (';
            $stock_query .= 'SELECT glb.global_parameter_value FROM 99_global_parameter AS glb WHERE glb.global_parameter_name = "tokped_shop"';
            $stock_query .= ')';
            $stock_query .= ')';
            $stock_query .= ')) AND 00_inventory.prod_id = '.$parameter['internal_id'].' LIMIT 1';
            $variant_stock = DB::select(DB::raw($stock_query));
            $stock = $variant_stock == null ? ($parameter['stock'] == 0 ? 1 : $parameter['stock']) : ($variant_stock[0]->stock == 0 ? 1 : $variant_stock[0]->stock);
        }
        $products= (Object)[
            'id' => isset($id) ? (int)$id : null,
            'category_id'=> (int)$tokopedia_category_id,
            'condition'=>'NEW', //-------------- string option: NEW, USED
            'description'=>$description,
            'etalase'=> $etalase,
            'is_free_return'=>false, //-----------boolean option: true, false
            'is_must_insurance'=>false, //------------boolean option: true, false
            'min_order'=>1,
            'name'=>$prodName,
            'pictures'=> $product_pictures,
            'preorder'=>$preorder,
            'price'=>$parameter['product_price'], //---------------------parent price, jika ada variant maka tidak berlaku
            'price_currency'=>"IDR",
            'sku'=> $sku,
            'status'=>'LIMITED',
            'stock'=>(int)$stock,
            'variant'=>$variant,
            // 'videos'=>$videosArray,
            'weight'=>(int)$bruto,
            'weight_unit'=>"GR",
            // 'wholesale'=>$wholesaleArray
        ];
        // $productObject=[$products];
        // $obj=['products'=>$productObject];

        // dd($obj);
        $shopId=$dbShopId->organization_code;
        // echo $shopId;
        // exit;
        $search_variant = ProductEcommerce::where('prod_id',$parameter['internal_id'])->first();
        // echo $parameter['internal_id'];
        // print_r($search_variant);
        // exit;
        // if($products->id==null){//--------------------jika tambah baru
        //     $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
        // }else{//-------------------------------------jika update


        // }
        // print_r($search_variant);
        if($search_variant != null){
            $url='https://api.ecomm.inergi.id/tokopedia/updateProductWithVariant?shopId='.$shopId;
        }else{
            $url='https://api.ecomm.inergi.id/tokopedia/createNewProductWithVariant?shopId='.$shopId;
            $products= (Object)[
                // 'id' => null,
                'category_id'=>(int)$tokopedia_category_id,
                'condition'=>'NEW', //-------------- string option: NEW, USED
                'description'=>$description,
                'etalase'=>$etalase,
                'is_free_return'=>false, //-----------boolean option: true, false
                'is_must_insurance'=>false, //------------boolean option: true, false
                'min_order'=>1,
                'name'=>$prodName,
                'pictures'=> $product_pictures,
                'preorder'=>$preorder,
                'price'=>$parameter['product_price'], //---------------------parent price, jika ada variant maka tidak berlaku
                'price_currency'=>"IDR",
                'sku'=> $sku,
                'status'=>'LIMITED',
                'stock'=>(int)$stock,
                'variant'=>$variant,
                // 'videos'=>$videosArray,
                'weight'=>(int)$bruto,
                'weight_unit'=>"GR",
                // 'wholesale'=>$wholesaleArray
            ];
        }
        $productObject=[$products];
        $obj=['products'=>$productObject];
        // echo $url.'<br>';
        // echo "<pre>";
        // echo json_encode($obj, JSON_PRETTY_PRINT);
        // echo "<br>";
        // dd($obj);
        // exit;
        // exit;
        $pushProduct=Http::withToken($this->token)->timeout(3)->retry(2, 100)->post($url,$obj);
        // print_r($pushProduct->status());
        // exit;
        $statusPushProduct=json_decode($pushProduct->body());
        // dd($statusPushProduct);
        // exit;
        // if($statusPushProduct == null){
        //     // echo "test";
        //     // exit;
        //     $products= (Object)[
        //         // 'id' => null,
        //         'category_id'=>(int)$tokopedia_category_id,
        //         'condition'=>'NEW', //-------------- string option: NEW, USED
        //         'description'=>$description,
        //         'etalase'=>$etalase,
        //         'is_free_return'=>false, //-----------boolean option: true, false
        //         'is_must_insurance'=>false, //------------boolean option: true, false
        //         'min_order'=>1,
        //         'name'=>$prodName,
        //         'pictures'=> $product_pictures,
        //         'preorder'=>$preorder,
        //         'price'=>$parameter['product_price'], //---------------------parent price, jika ada variant maka tidak berlaku
        //         'price_currency'=>"IDR",
        //         'sku'=> $sku,
        //         'status'=>'LIMITED',
        //         'stock'=>(int)$parameter['stock'],
        //         'variant'=>$variant,
        //         // 'videos'=>$videosArray,
        //         'weight'=>$bruto,
        //         'weight_unit'=>"GR",
        //         // 'wholesale'=>$wholesaleArray
        //     ];
        //     $productObject=[$products];
        //     $obj=['products'=>$productObject];
        //     // echo "<pre>";
        //     // dd($obj);
        //     // exit;
        //     // echo $url;
        //     // exit;
        //     $pushProduct=Http::withToken($this->token)->timeout(3)->retry(2, 100)->post($url,$obj);
        //     // print_r($pushProduct);
        //     // exit;
        //     $statusPushProduct=json_decode($pushProduct->body());
        // }
        // dd($statusPushProduct);
        // exit;
        // echo "\n".$statusPushProduct->data->upload_id;
        $tokopedia_error_list = array();
        $upload_id = $statusPushProduct->data->upload_id;
        // echo $upload_id;
        // exit;
        $urlStatusPush='https://api.ecomm.inergi.id/tokopedia/checkProductUpdateStatus?shopId='.$shopId.'&updateId='.$upload_id;
        // echo $upload_id;
        // exit;
        // print_r($statusPushProduct);
        // exit;
        // echo $urlStatusPush;
        // exit;
        $status = "loading";
        $error = array();
        $prod_ecom_code = "";
        $product_ecommerce_code = "";
        do{
            sleep(6);
            $getUploadedProductStatus = Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlStatusPush);
            $getUploadedProductResponse = json_decode($getUploadedProductStatus->body());
            // dd($getUploadedProductResponse);
            // exit;
            // echo $product_ecommerce_code;
            // exit;
            if($getUploadedProductResponse->data->success_rows > 0){
                $product_ecommerce_code = $getUploadedProductResponse->data->success_rows_data[0]->product_id;
                // print_r($getUploadedProductResponse);
                // exit;
                $status = "success";
                $prod_ecom_code = $getUploadedProductResponse->data->success_rows_data[0]->product_id;
            }elseif($getUploadedProductResponse->data->unprocessed_rows > 0){
                $status = "loading";
            }else{
                if($getUploadedProductResponse->data->failed_rows > 0){
                    $tokopedia_error_list = $getUploadedProductResponse->data->failed_rows_data[0]->error;
                    // print_r($tokopedia_error_list);
                    // exit;
                }
                $status = "failed";
            }
        }while($status == "loading");
        // print_r($getUploadedProductResponse);
        // exit;
        $indexProductDB=0;
        // print_r($error_list);
        // exit;
        if($status == "success"){
            // echo "success";
            // exit;
            $parent = $this->getSingleProduct($product_ecommerce_code);
            // dd($parent);
            // exit;
            if($parent != null || isset($parent)){
                if($parent['variant']['childrenID'] != null){
                    // $parentTokped = $parent['basic']['productID'];
                    $product_ecommerce_parent = ProductEcommerce::where('prod_ecom_code',$prod_ecom_code)->orderBy('created_date','ASC')->first();
                    if($product_ecommerce_parent == null){
                        ProductEcommerce::insert(array(
                            'ecommerce_id' => 28,
                            'category_id' => (int)$tokopedia_category_id,
                            'organization_id' => $tokopedia_store['organization_id'],
                            'prod_id' => $parameter['variant'][0]['id'],
                            'prod_ecom_code' => $prod_ecom_code,
                            'prod_ecom_name' => $parameter['product_name'],
                            'description' => $parameter['product_description'],
                            'preorder_flag' => 'N',
                            'price' => $parameter['product_price'],
                            'bruto' => $parameter['bruto'],
                            'etalase_id' => $parameter['etalase']
                        ));
                    }else{
                        ProductEcommerce::where('prod_ecom_code', $prod_ecom_code)->update(array(
                            'ecommerce_id' => 28,
                            'category_id' => (int)$tokopedia_category_id,
                            'organization_id' => $tokopedia_store['organization_id'],
                            'prod_id' => $parameter['variant'][0]['id'],
                            'prod_ecom_code' => $prod_ecom_code,
                            'prod_ecom_name' => $parameter['product_name'],
                            'description' => $parameter['product_description'],
                            'preorder_flag' => 'N',
                            'price' => $parameter['product_price'],
                            'bruto' => $parameter['bruto'],
                            'etalase_id' => $parameter['etalase']
                        ));
                    }
                    $totalIndexVariant = count($parent['variant']['childrenID']) - 1;
                    $productIndex = 0;
                    for ($j=$totalIndexVariant; $j >= 0; $j--) { 
                        $product_ecommerce_detail = ProductEcommerce::where('prod_ecom_code', $parent['variant']['childrenID'][$j])->orderBy('created_date','DESC')->first();
    
                        if($product_ecommerce_detail == null){
                            // echo "blablabla";
                            ProductEcommerce::insert(array(
                                'ecommerce_id' => 28,
                                'category_id' => (int)$tokopedia_category_id,
                                'organization_id' => $tokopedia_store['organization_id'],
                                'prod_id' => $parameter['variant'][$productIndex]['id'],
                                'prod_ecom_code' => $parent['variant']['childrenID'][$j],
                                'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$productIndex]['name'],
                                'description' => $parameter['product_description'],
                                'preorder_flag' => 'N',
                                'price' => isset($parameter['variant'][$productIndex]['price']) ? $parameter['variant'][$productIndex]['price'] : $parameter['product_price'],
                                'bruto' => $parameter['bruto'],
                                'etalase_id' => $parameter['etalase']
                            ));
                        }else{
                            // echo "bliblibli";
                            ProductEcommerce::where('prod_ecom_code', $parent['variant']['childrenID'][$j])->update(array(
                                'ecommerce_id' => 28,
                                'category_id' => (int)$tokopedia_category_id,
                                'organization_id' => $tokopedia_store['organization_id'],
                                'prod_id' => $parameter['variant'][$productIndex]['id'],
                                'prod_ecom_code' => $parent['variant']['childrenID'][$j],
                                'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$productIndex]['name'],
                                'description' => $parameter['product_description'],
                                'preorder_flag' => 'N',
                                'price' => isset($parameter['variant'][$productIndex]['price']) ? $parameter['variant'][$productIndex]['price'] : $parameter['product_price'],
                                'bruto' => $parameter['bruto'],
                                'etalase_id' => $parameter['etalase']
                            ));
                        }
                        $productIndex++;
                        // exit;
                    }
                }else{
                    $product_ecommerce_detail = ProductEcommerce::where('prod_ecom_code', $prod_ecom_code)->first();
    
                    if($product_ecommerce_detail == null){
                        // echo "blablabla";
                        ProductEcommerce::insert(array(
                            'ecommerce_id' => 28,
                            'category_id' => (int)$tokopedia_category_id,
                            'organization_id' => $tokopedia_store['organization_id'],
                            'prod_id' => $parameter['variant'][0]['id'],
                            'prod_ecom_code' => $prod_ecom_code,
                            'prod_ecom_name' => $prodName,
                            'description' => $parameter['product_description'],
                            'preorder_flag' => 'N',
                            'price' => isset($parameter['variant'][0]['price']) ? $parameter['variant'][0]['price'] : $parameter['product_price'],
                            'bruto' => $parameter['bruto'],
                            'etalase_id' => $parameter['etalase']
                        ));
                    }else{
                        // echo "bliblibli";
                        ProductEcommerce::where('prod_ecom_code', $prod_ecom_code)->update(array(
                            'ecommerce_id' => 28,
                            'category_id' => (int)$tokopedia_category_id,
                            'organization_id' => $tokopedia_store['organization_id'],
                            'prod_id' => $parameter['variant'][0]['id'],
                            'prod_ecom_code' => $prod_ecom_code,
                            'prod_ecom_name' => $prodName,
                            'description' => $parameter['product_description'],
                            'preorder_flag' => 'N',
                            'price' => isset($parameter['variant'][0]['price']) ? $parameter['variant'][0]['price'] : $parameter['product_price'],
                            'bruto' => $parameter['bruto'],
                            'etalase_id' => $parameter['etalase']
                        ));
                    }
                }
            }
            // echo 'SUCCESS'.'<br>';
            // if($search_variant == null){
            //     // echo "bitches";
            //     // exit;
            //     // ProductEcommerce::insert([
            //     //     'ecommerce_id' => 28,
            //     //     'category_id' => (int)$tokopedia_category_id,
            //     //     'organization_id' => $tokopedia_store['organization_id'],
            //     //     'prod_id' => $parameter['internal_id'],
            //     //     'prod_ecom_code' => $prod_ecom_code,
            //     //     'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][0]['name'],
            //     //     'description' => $parameter['product_description'],
            //     //     'preorder_flag' => 'N',
            //     //     'price' => $parameter['product_price'],
            //     //     'bruto' => $parameter['bruto']
            //     // ]);
            //     // for ($j=1; $j < count($parameter['variant']); $j++) {
            //     //     array_push($product_ecommerce_variant, array(
            //     //         'ecommerce_id' => 28,
            //     //         'category_id' => (int)$tokopedia_category_id,
            //     //         'organization_id' => $tokopedia_store['organization_id'],
            //     //         'prod_id' => $parameter['internal_id'],
            //     //         'prod_ecom_code' => $prod_ecom_code,
            //     //         'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$j]['name'],
            //     //         'description' => $parameter['product_description'],
            //     //         'preorder_flag' => 'N',
            //     //         'price' => $parameter['product_price'],
            //     //         'bruto' => $parameter['bruto']
            //     //     ));
            //     // }
            //     $parent=$this->getSingleProduct($product_ecommerce_code);
            //     $totalIndexVariant=count($parent['variant']['childrenID'])-1;
            //     for($i=$totalIndexVariant;$i>=0;$i--){
            //         $getVarian=$this->getSingleProduct($parent['variant']['childrenID'][$i]);
            //         DB::table('00_product_ecommerce')
            //                 ->insert([
            //             'ecommerce_id'=>$dbEcomId->organization_id,
            //             'organization_id'=>$dbShopId->organization_id,
            //             'prod_id'=>$parameter['variant'][$indexProductDB]['id'],
            //             'prod_ecom_code'=>$getVarian['basic']['productID'],
            //             'prod_ecom_name'=>$getVarian['basic']['name'],
            //             'description'=>$getVarian['basic']['shortDesc'],
            //             'category_id' => $tokopedia_category_id,
            //             'active_flag'=>1,
            //             'etalase_id' => $parameter['etalase']
            //         ]);
            //         $indexProductDB++;
            //     }
            //     // ProductEcommerce::insert($product_ecommerce_variant);
            // }else{
            //     // echo "bitches2";
            //     // exit;
            //     // ProductEcommerce::where('prod_id',$parameter['internal_id'])->update([
            //     //     'ecommerce_id' => 28,
            //     //     'organization_id' => $tokopedia_store['organization_id'],
            //     //     'category_id' => (int)$tokopedia_category_id,
            //     //     // 'prod_id' => $parameter['internal_id'],
            //     //     // 'prod_ecom_code' => $getUploadedProductResponse->data->success_rows_data->product_id,
            //     //     'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][0]['name'],
            //     //     'description' => $parameter['product_description'],
            //     //     'preorder_flag' => 'N',
            //     //     'price' => $parameter['product_price'],
            //     //     'bruto' => $parameter['bruto']
            //     // ]);
            //     $currentVarianTokped=$this->getSingleProduct($product_ecommerce_code);
            //     // dd($currentVarianTokped);
            //     // exit;
            //     $countVarianTokped=count($currentVarianTokped['variant']['childrenID'])-1;
            //     //-----------get prod ecom code
            //     for($i=0;$i<count($parameter['variant']);$i++){
            //         //--------------------------------check ketersediaan
            //         $checkVarian=DB::table('00_product_ecommerce')
            //         ->where('prod_id',$parameter['variant'][$i]['id'])
            //         ->select('prod_ecom_code')
            //         ->first();
            //         if($checkVarian){
            //             $updateVarianSingleProd=$this->getSingleProduct($product_ecommerce_code);
            //             DB::table('00_product_ecommerce')
            //             ->where('prod_ecom_code',$checkVarian->prod_ecom_code)
            //             ->update([
            //                 'prod_ecom_name'=>$updateVarianSingleProd['basic']['name'],
            //                 'description'=>$updateVarianSingleProd['basic']['shortDesc'],
            //                 'etalase_id' => $parameter['etalase'],
            //                 'category_id' => $tokopedia_category_id
            //             ]);
            //         }else{
            //             // dd($currentVarianTokped[$countVarianTokped]);
            //             // exit;
            //             $addVarianSingleProd=$this->getSingleProduct($currentVarianTokped['variant']['childrenID'][$countVarianTokped]);
            //             DB::table('00_product_ecommerce')
            //             ->insert([
            //                 'ecommerce_id'=>$dbEcomId->organization_id,
            //                 'organization_id'=>$dbShopId->organization_id,
            //                 'prod_id'=>$parameter['variant'][$i]['id'],
            //                 'prod_ecom_code'=>$addVarianSingleProd['basic']['productID'],
            //                 'prod_ecom_name'=>$addVarianSingleProd['basic']['name'],
            //                 'description'=>$addVarianSingleProd['basic']['shortDesc'],
            //                 'active_flag'=>1,
            //                 'category_id' => $tokopedia_category_id,
            //                 'etalase_id' => $parameter['etalase']
            //             ]);
            //         }
            //         $countVarianTokped--;
            //     }
            //     // for ($j=1; $j < count($parameter['variant']); $j++) {
            //     //     // array_push($product_ecommerce_variant, array(
            //     //     //     'ecommerce_id' => 28,
            //     //     //     'category_id' => (int)$tokopedia_category_id,
            //     //     //     'organization_id' => $tokopedia_store['organization_id'],
            //     //     //     'prod_id' => $parameter['internal_id'],
            //     //     //     'prod_ecom_code' => $prod_ecom_code,
            //     //     //     'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$j]['name'],
            //     //     //     'description' => $parameter['product_description'],
            //     //     //     'preorder_flag' => 'N',
            //     //     //     'price' => $parameter['product_price'],
            //     //     //     'bruto' => $parameter['bruto']
            //     //     // ));
            //     //     $product_ecommerce_variant = ProductEcommerce::where('prod_id', $parameter['variant'][$j]['id']);

            //     //     if($product_ecommerce_variant == null){
            //     //         ProductEcommerce::where('prod_id', $parameter['variant'][$j]['id'])->update(array(
            //     //             'ecommerce_id' => 28,
            //     //             'category_id' => (int)$tokopedia_category_id,
            //     //             'organization_id' => $tokopedia_store['organization_id'],
            //     //             'prod_id' => $parameter['variant'][$j]['id'],
            //     //             'prod_ecom_code' => $prod_ecom_code,
            //     //             'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$j]['name'],
            //     //             'description' => $parameter['product_description'],
            //     //             'preorder_flag' => 'N',
            //     //             'price' => $parameter['product_price'],
            //     //             'bruto' => $parameter['bruto']
            //     //         ));
            //     //     }else{
            //     //         ProductEcommerce::insert(array(
            //     //             'ecommerce_id' => 28,
            //     //             'category_id' => (int)$tokopedia_category_id,
            //     //             'organization_id' => $tokopedia_store['organization_id'],
            //     //             'prod_id' => $parameter['variant'][$j]['id'],
            //     //             'prod_ecom_code' => $prod_ecom_code,
            //     //             'prod_ecom_name' => $parameter['product_name'] . ' - ' . $parameter['variant'][$j]['name'],
            //     //             'description' => $parameter['product_description'],
            //     //             'preorder_flag' => 'N',
            //     //             'price' => $parameter['product_price'],
            //     //             'bruto' => $parameter['bruto']
            //     //         ));
            //     //     }
            //     // }
            // }
            echo $upload_id.": success";
            // exit;
        }else{
            echo $upload_id.": failed";
            // exit;
        }
        // exit;
        return $tokopedia_error_list;
    }

    public function getShopName(){
        $shop=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokped_shop')
        ->select('global_parameter_code','global_parameter_value','global_parameter_desc')
        ->first();
        return $shop;
    }

    public function getSingleProduct($id){
        $url='https://api.ecomm.inergi.id/tokopedia/getSingleProductByProductId?productId='.$id;
        $product=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $product['data'][0];
    }

    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }

    public function getBeratProductTokped($category){
        $url='https://api.ecomm.inergi.id/tokopedia/getAllVariantsByCategoryId?categoryId='.$category;
        $this->generateToken();
        $berat=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($url)->json();
        return $berat['data'];
    }

    public function etalaseTokped(){
        $shopName=app('App\Http\Controllers\GlobalSettingsController')->getGlobalSetting('tokped_shop');
        $shopId=app('App\Http\Controllers\OrganizationController')->organizationByName($shopName->global_parameter_value);
        // print_r($shopId);
        // exit;
        $urlGetEtalase='https://api.ecomm.inergi.id/tokopedia/getAllEtalase?shopId='.$shopId->organization_code;
        $hitEtalase=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlGetEtalase)->json();
        return $hitEtalase['data']['etalase'];
        // $urlGetEtalase='https://api.ecomm.inergi.id/tokopedia/getAllEtalase?shopId='.$etalase;
        // $hitEtalase=Http::withToken($this->token)->timeout(3)->retry(2, 100)->get($urlGetEtalase)->json();
        // return $hitEtalase['data']['etalase'];
    }

    public function importProductOLD(Request $request){
        // dd($request->all());
        $res = array();
        $res_fail = array();
        $status = 500;

        DB::beginTransaction();
        try {
            //code...
            $file = $request->file('file');
            $status = 200;
            $collection = Excel::toCollection(new InventoryImport, $file);

            // get etalase tokopedia
            try{
                $this->generateToken();
                $etalase =$this->etalaseTokped();
                $message = '';
            } catch (\Exception $e) {
                $etalase = '';
                $message = $e->getMessage();
            }
            $parent = array();
            $child = array();
            $prod_code_validation = array();
            foreach ($collection[0] as $key => $row){
                if (isset($row[0]) and $row[0] != '') {
                    $err = "";
                    $prod_code = trim($row[0]);
                    $parent_prod_code = trim($row[1]);
                    $varian_parent = trim($row[2]);
                    $prod_name = trim($row[3]);
                    $prod_desc = trim($row[4]);
                    $tokped_etalase = trim($row[5]);
                    $tokped_kategori = trim($row[6]);
                    $prod_category = trim($row[7]);
                    $prod_tag = trim($row[8]);
                    $prod_bruto = trim($row[9]);
                    $satuan_jual = trim($row[10]);
                    $satuan_pengukuran = trim($row[11]);
                    $data_gudang = trim($row[12]);
                    $tipe_stok = trim($row[13]);


                    //cek nama product 00_product_ecommerce, prod_ecom_name
                    $tokped_name = DB::table('00_product_ecommerce')
                        ->where('prod_ecom_name', '=', $prod_name)
                        ->where('active_flag', '=', 1)
                        ->first();

                    if (isset($tokped_name) && !empty($tokped_name)){
                        $status = 500;
                        $err .= "Nama Produk Sudah terdaftar di Tokopedia";
                        $err .= "<br>";
                    }

                    // cek nama harus mengandung - untuk explode.
                    $cek_name = explode("-",trim($prod_name));
                    if (count($cek_name) > 1) {
                        if (isset($cek_name[1])) {

                        } else {
                            $status = 500;
                            $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                        $err .= "<br>";
                    }

                    // cek varian_parent, jika ada isi, cek induk harus ada.
                    if (isset($varian_parent) && !empty($varian_parent) && $varian_parent!="") {
                        if (!isset($parent_prod_code) || empty($parent_prod_code) || $parent_prod_code=="") {
                            $status = 500;
                            $err .= "Harap isi kolom induk.";
                            $err .= "<br>";
                        }
                    }
                    // cek numerik bruto, sat jual
                    if (!is_numeric($prod_bruto) || !is_numeric($satuan_jual)) {
                       // var_dump($prod_bruto);
                       // var_dump($satuan_jual);
                       // var_dump($prod_code);
                        $status = 500;
                        $err .= "Bruto atau satuan jual bukan numeric";
                        $err .= "<br>";
                    }

                    // cek produk code
                    $exist_prod = DB::table('00_product')
                        ->where('prod_code', '=', $prod_code)
                        ->whereIn('active_flag', [1, 0])
                        ->count();
                    if ($exist_prod > 0) {
                        $status = 500;
                        $err .= "Kode produk telah terdaftar";
                        $err .= "<br>";
                    }

                    //cek etalase tokopedia
                    $etalase_id = "";
                    $tokped_etalase_id = "";
                    if (isset($etalase) && !empty($etalase) && $etalase != "") {
                        foreach ($etalase as $etalase_item) {
                            if (strtolower($tokped_etalase) == strtolower($etalase_item['etalase_name'])) {
                                $etalase_id = $etalase_item['etalase_id'];
                                break;
                            }
                        }

                        if (isset($etalase_id) && !empty($etalase_id) && $etalase_id != "") {
                            $tokped_etalase_id = $etalase_id;
                        } else {
                            $status = 500;
                            $err .= "Etalase produk tidak ditemukan.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Gagal mendapatkan data etalase.";
                        $err .= "<br>";
                    }

                    //cek kategori tokopedia
                    $tokopedia_category = CategoryEcommerce::select(
                        '00_category_per_ecommerce.category_id',
                        '00_category_per_ecommerce.category_id_ecommerce',
                        '00_category_per_ecommerce.category_name',
                        'category_id_ecommerce',
                        DB::raw('(SELECT pc.category_id FROM 00_product_ecommerce AS pc
                                    WHERE pc.prod_id = "'.$request->product.'") AS tokped_category_id'
                        )
                    )
                        ->join('00_category','00_category.category_id','=','00_category_per_ecommerce.category_id')
                        ->where('category_level',3)
                        ->where('organization_id',28)
                        ->where('00_category.active_flag', 1)
                        ->where('00_category_per_ecommerce.category_name','=', $tokped_kategori) //$tokped_kategori
                        ->first();

                    $tokped_kategori_id = "";
                    if (isset($tokopedia_category) && !empty($tokopedia_category)) {
                        $tokped_kategori_id = $tokopedia_category->category_id_ecommerce;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tokopedia tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek kategori produk
                    $category = DB::table('00_category')
                        ->where('category_name', '=', $prod_category)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $category_id = "";
                    if (isset($category) && !empty($category)) {
                        $category_id = $category->category_id;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek penanda
                    $tag_arr = explode(";", $prod_tag);
                    $tag_id = array();
                    $tag_status = 0;
                    foreach ($tag_arr as $tag_item) {
                        // cek master data
                        $tag = DB::table('00_tag')
                            ->where('tag_name', '=', trim($tag_item))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($tag) && !empty($tag)) {
                            array_push($tag_id, $tag->tag_id);
                        } else {
                            $tag_status = 1;
                        }
                    }
                    if ($tag_status != 0) {
                        $status = 500;
                        $err .= "Penanda tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek satuan pengukuran
                    $uom = DB::table('00_uom')
                        ->where('uom_name', '=', $satuan_pengukuran)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $uom_id = "";
                    if (isset($uom) && !empty($uom)) {
                        $uom_id = $uom->uom_id;
                    } else {
                        $status = 500;
                        $err .= "Satuan pengukuran tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek gudang
                    $arr_gudang = explode(";", $data_gudang);
                    $warehouse_id = array();
                    $harga_tokopedia = 0;
                    $warehouse_status = 0;
                    $warehouse_keterangan = "";
                    foreach ($arr_gudang as $gudang_item) {
                        $arr_gudang_detail = explode(",", $gudang_item);
                        // cek nama gudang
                        $warehouse = DB::table('00_warehouse')
                            ->where('warehouse_name', '=', trim($arr_gudang_detail[0]))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($warehouse) && !empty($warehouse)) {
                            // cek numeric
                            if (is_numeric(trim($arr_gudang_detail[1])) && is_numeric(trim($arr_gudang_detail[2]))) {
                                $temp_warehouse['warehouse_id'] = $warehouse->warehouse_id;
                                $temp_warehouse['harga'] = $arr_gudang_detail[1];
                                $temp_warehouse['min_stok'] = $arr_gudang_detail[2];
                                // cek tipe gudang untuk harga tokopedia
                                if ($warehouse->warehouse_type_id == 2) {
                                    if ($harga_tokopedia == 0) {
                                        $harga_tokopedia = $arr_gudang_detail[1];
                                    }
                                }
                                array_push($warehouse_id, $temp_warehouse);
                            } else {
                                $warehouse_status = 1;
                                $warehouse_keterangan .= "Harga produk atau minimum stok bukan numerik";
                                $warehouse_keterangan .= "<br>";
                            }

                        } else {
                            $warehouse_status = 1;
                            $warehouse_keterangan .= "Gudang tidak ditemukan";
                            $warehouse_keterangan .= "<br>";
                        }
                    }

                    if ($warehouse_status != 0) {
                        $status = 500;
                        $err .= $warehouse_keterangan;
                        $err .= "<br>";
                    }

                    if ($harga_tokopedia == 0) {
                        $status = 500;
                        $err .= "Tidak ada data untuk harga tokopedia. Harap periksa pilihan gudang.";
                        $err .= "<br>";
                    }

                    $id_tipe_stok = null;
                    if (isset($tipe_stok) && !empty($tipe_stok)) {
                        if (strtoupper('ya') == strtoupper($tipe_stok) && $varian_parent == null) {
                            $id_tipe_stok = 1;
                        } else if (strtoupper('tidak') == strtoupper($tipe_stok) && $varian_parent != null) {
                            $id_tipe_stok = 0;
                        } else {
                            $status = 500;
                            $err .= "Tipe stok tidak sesuai.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Tidak ada data tipe stok.";
                        $err .= "<br>";
                    }

                    // validasi nama varian tidak lebih dari 15 char
                    if (count($cek_name) > 1) {
                        $varian_length = strlen(trim(explode('-', $prod_name)[1]));
                        if ($varian_length > 15) {
                            $status = 500;
                            $err .= "Nama varian lebih dari 15 character.";
                            $err .= "<br>";
                        }
                    }

                    // cek varian parent

                    if (isset($varian_parent) && !empty($varian_parent) && $varian_parent != "") {
                        $validation = 0;
                        $product = DB::table('00_product')
                            ->select('prod_id')
                            ->where('prod_code', '=', $varian_parent)
                            ->first();
                        if (isset($product) && !empty($product)) {
                            $validation = 1;
                        } else {
                            foreach ($prod_code_validation as $item){
                                if ($varian_parent == $item) {
                                    $validation = 1;
                                    break;
                                }
                            }
                        }
                        if ($validation == 0) {
                            $status = 500;
                            $err .= "Kode produk untuk stok tidak ditemukan.";
                            $err .= "<br>";
                        }
                    }

                    // cek parent?
                    if ($parent_prod_code == "" || empty($parent_prod_code)) {
                        // add to arr parent
                        $temp['prod_code'] = $prod_code;
                        $temp['prod_name'] = $prod_name;
                        $temp['prod_desc'] = $prod_desc;
                        $temp['tokped_etalase_id'] = $tokped_etalase_id;
                        $temp['tokped_kategori_id'] = $tokped_kategori_id;
                        $temp['harga_tokped'] = $harga_tokopedia;
                        $temp['prod_category'] = $category_id;
                        $temp['prod_tag'] = $tag_id;
                        $temp['prod_bruto'] = $prod_bruto;
                        $temp['satuan_jual'] = $satuan_jual;
                        $temp['satuan_pengukuran'] = $uom_id;
                        $temp['gudang'] = $warehouse_id;
                        $temp['stockable_flag'] = $id_tipe_stok;

                        array_push($parent, $temp);

                        array_push($prod_code_validation, $prod_code);

                    } else {
                        // add to arr child
                        $temp['prod_code'] = $prod_code; //
                        $temp['parent_prod_code'] = $parent_prod_code; //
                        $temp['prod_name'] = $prod_name; //
                        $temp['prod_desc'] = $prod_desc; //
                        $temp['tokped_etalase_id'] = $tokped_etalase_id;
                        $temp['tokped_kategori_id'] = $tokped_kategori_id;
                        $temp['harga_tokped'] = $harga_tokopedia;
                        $temp['prod_category'] = $category_id;
                        $temp['prod_tag'] = $tag_id;
                        $temp['prod_bruto'] = $prod_bruto;
                        $temp['satuan_jual'] = $satuan_jual;
                        $temp['satuan_pengukuran'] = $uom_id;
                        $temp['gudang'] = $warehouse_id;
                        $temp['stockable_flag'] = $id_tipe_stok;
                        $temp['varian_parent'] = $varian_parent;

                        array_push($child, $temp);

                        if (isset($varian_parent) && !empty($varian_parent) && $varian_parent!="") {

                        } else {
                            array_push($prod_code_validation, $prod_code);
                        }
                    }

                    if (isset($err) && !empty($err)) {
                        $data_fail = (object)array(
                            'prod_code' => $prod_code,
                            'prod_name' => $prod_name,
                            'message' => $err,
                        );
                        array_push($res_fail, $data_fail);
                    }
                }
            }

            // cek validasi fail?
            if ($status == 500) {
                $result = (object)array(
                    'isSuccess' => true,
                    'title' => 'Success',
                    'data' => $res,
                    'data_fail' => $res_fail,
                    'message' => 'Get Data Import!',
                    'icon' => 'success'
                );
                return response()->json($result);
            }

            $arr_ecommerce = array();
            // proses to db
            foreach ($parent as $parent_item) {
                // insert parent product
                $inserted_obj = [
                    'prod_code' => $parent_item['prod_code'],
                    'prod_name' => $parent_item['prod_name'],
                    'prod_desc' => $parent_item['prod_desc'],
                    //'brand_id' => $request->input('product_brand'),
                    //'preorder_flag' => 'N',
                    //'product_sku_id' => $request->input('product_sku') == null || $request->input('product_sku') == "" ? null : $request->input('product_sku'),
                    //'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                    //'shipping_durability_min' => $request->input('shipping_durability_min'),
                    //'shipping_durability_max' => $request->input('shipping_durability_max'),
                    //'shipping_durability_id' => $request->input('shipping_durability_unit'),
                    // 'stockable_flag' => $request->input('stockable_flag'),
                    // 'stock' => $request->input('product_stock'),
                    // 'minimum_stock' => $request->input('minimum_stock'),
                    //'minimum_order' => $request->input('minimum_order'),
                    'uom_id' => $parent_item['satuan_pengukuran'],
                    'uom_value' => $parent_item['satuan_jual'],
                    // 'prod_image' => $product_image_location,
                    'bruto' => $parent_item['prod_bruto'],
                    'bruto_uom_id' => 1,
                    'active_flag' => 1,
                    //'parent_prod_id' => ($request->input('parent_product') != null || $request->input('parent_product') != "") ? $request->input('parent_product') : null,
                    'stockable_flag' => $parent_item['stockable_flag'],
                    'category_id' => $parent_item['prod_category'],
                    //'is_taxable_product' => $request->input('is_taxable_product'),
                    //'tax_value' => $request->input('tax_value') / 100,
                    'created_by' => Session::get('users')['id'],
                    //'dim_length' => (($request->input('dim_length') == null || $request->input('dim_length') == "") ? null : $request->input('dim_length')),
                    //'dim_width' => (($request->input('dim_width') == null || $request->input('dim_width') == "") ? null : $request->input('dim_width')),
                    //'dim_height' => (($request->input('dim_height') == null || $request->input('dim_height') == "") ? null : $request->input('dim_height'))
                ];
                $parent_id = Product::insertGetId($inserted_obj);

                // insert product_tag
                $product_tag_array = array();
                foreach ($parent_item['prod_tag'] as $id_tag_item) {
                    $obj = array(
                        "prod_id" => $parent_id,
                        "tag_id" => $id_tag_item,
                        "active_flag" => 1,
                        "created_by" => Session::get('users')['id']
                    );
                    array_push($product_tag_array,$obj);
                }
                ProductTag::insert($product_tag_array);

                // insert product warehouse
                $warehouse_array = array();
                foreach ($parent_item['gudang'] as $gudang_detail) {
                    $obj = array(
                        "prod_id" => $parent_id,
                        "warehouse_id" => $gudang_detail['warehouse_id'],
                        "prod_price" => $gudang_detail['harga'],
                        "minimum_stock" => $gudang_detail['min_stok'],
                        "active_flag" => 1
                    );
                    array_push($warehouse_array,$obj);
                }
                ProductWarehouse::insert($warehouse_array);

                // varian parent
                $variant[0] = array(
                    "name" => explode("-",trim($parent_item['prod_name']))[1],
                    "picture" => "http://yukmarket.id/img/ecomm-default.png",
                    "stock" => 1,
                    // "price" => $request->input('variant_highest_price'),
                    "id" => $parent_id,
                    "is_primary" => true
                );

                foreach ($child as $child_item){
                    // cek sesuai dengan parent?
                    if ($child_item['parent_prod_code'] == $parent_item['prod_code']) {
                        // insert child
                        // pengecekan insert child with parent?
                        if (isset($child_item['varian_parent']) && !empty($child_item['varian_parent']) && $child_item['varian_parent'] != "") {
                            $product = DB::table('00_product')
                                ->select('prod_id')
                                ->where('prod_code', '=', $child_item['varian_parent'])
                                ->first();

                            $varian_parent_id = $product->prod_id;

                            //insert baru varian_id & parent_id
                            $inserted_variant_obj = [
                                'prod_code' => $child_item['prod_code'],
                                'prod_name' => $child_item['prod_name'],
                                'prod_desc' => $child_item['prod_desc'],
                                //'brand_id' => $request->input('product_brand'),
                                //'preorder_flag' => 'N',
                                //'product_sku_id' => $parent_sku_id,
                                //'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                                //'shipping_durability_min' => $request->input('shipping_durability_min'),
                                //'shipping_durability_max' => $request->input('shipping_durability_max'),
                                //'shipping_durability_id' => $request->input('shipping_durability_unit'),
                                // 'stockable_flag' => $request->input('stockable_flag'),
                                // 'stock' => $request->input('product_stock'),
                                // 'minimum_stock' => $request->input('minimum_stock'),
                                //'minimum_order' => $request->input('minimum_order'),
                                'uom_id' => $child_item['satuan_pengukuran'],
                                'uom_value' => $child_item['satuan_jual'],
                                //'prod_image' => $product_image_location,
                                'bruto' => $child_item['prod_bruto'],
                                'bruto_uom_id' => 1,
                                'active_flag' => 1,
                                'parent_prod_id' => $varian_parent_id,
                                'stockable_flag' => $child_item['stockable_flag'],
                                'category_id' => $child_item['prod_category'],
                                //'is_taxable_product' => $request->input('is_taxable_product'),
                                //'tax_value' => $request->input('tax_value') / 100,
                                'created_by' => Session::get('users')['id'],
                                //'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                                //'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                                //'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                                'variant_id' => $parent_id,
                            ];
                        } else {
                            //insert baru varian_id
                            $inserted_variant_obj = [
                                'prod_code' => $child_item['prod_code'],
                                'prod_name' => $child_item['prod_name'],
                                'prod_desc' => $child_item['prod_desc'],
                                //'brand_id' => $request->input('product_brand'),
                                //'preorder_flag' => 'N',
                                //'product_sku_id' => $parent_sku_id,
                                //'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                                //'shipping_durability_min' => $request->input('shipping_durability_min'),
                                //'shipping_durability_max' => $request->input('shipping_durability_max'),
                                //'shipping_durability_id' => $request->input('shipping_durability_unit'),
                                // 'stockable_flag' => $request->input('stockable_flag'),
                                // 'stock' => $request->input('product_stock'),
                                // 'minimum_stock' => $request->input('minimum_stock'),
                                //'minimum_order' => $request->input('minimum_order'),
                                'uom_id' => $child_item['satuan_pengukuran'],
                                'uom_value' => $child_item['satuan_jual'],
                                //'prod_image' => $product_image_location,
                                'bruto' => $child_item['prod_bruto'],
                                'bruto_uom_id' => 1,
                                'active_flag' => 1,
                               // 'parent_prod_id' => $parent_id,
                                'stockable_flag' => $child_item['stockable_flag'],
                                'category_id' => $child_item['prod_category'],
                                //'is_taxable_product' => $request->input('is_taxable_product'),
                                //'tax_value' => $request->input('tax_value') / 100,
                                'created_by' => Session::get('users')['id'],
                                //'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                                //'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                                //'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                                'variant_id' => $parent_id,
                            ];
                        }
                        $product_variant_id = Product::insertGetId($inserted_variant_obj);

                        // insert product_tag
                        $product_tag_array = array();
                        foreach ($child_item['prod_tag'] as $id_tag_item) {
                            $obj = array(
                                "prod_id" => $product_variant_id,
                                "tag_id" => $id_tag_item,
                                "active_flag" => 1,
                                "created_by" => Session::get('users')['id']
                            );
                            array_push($product_tag_array,$obj);
                        }
                        ProductTag::insert($product_tag_array);

                        // insert product warehouse
                        $warehouse_array = array();
                        foreach ($child_item['gudang'] as $gudang_detail) {
                            $obj = array(
                                "prod_id" => $product_variant_id,
                                "warehouse_id" => $gudang_detail['warehouse_id'],
                                "prod_price" => $gudang_detail['harga'],
                                "minimum_stock" => $gudang_detail['min_stok'],
                                "active_flag" => 1
                             );
                            array_push($warehouse_array,$obj);
                        }
                        ProductWarehouse::insert($warehouse_array);

                        // create array child push tokped
                        array_push($variant, array(
                            "name" => explode("-",$child_item['prod_name'])[1],
                            "picture" => "http://yukmarket.id/img/ecomm-default.png",
                            "stock" => 1,
                            // "price" => $request->input('variant_highest_price')[$b],
                            "id" => $product_variant_id,
                            "is_primary" => false
                        ));
                    }
                }

                // $internal_id = $parent_id;
                $product_weight = $this->getBeratProductTokped($parent_item['tokped_kategori_id']);
                $product_weight = array_filter($product_weight, function($value){
                    return $value['variant_id'] == 1;
                });
                $product_weight = array_values($product_weight);
                $product_weight_unit = $product_weight[0]['units'][0]['values'];
                if (count($arr_ecommerce) <= 0) {
                    $arr_ecommerce[$parent_id] = (object)array(
                        'tokopedia_category_id' => $parent_item['tokped_kategori_id'],
                        'tokopedia_etalase' => $parent_item['tokped_etalase_id'],
                        'product_price' => $parent_item['harga_tokped'],
                        'tokopedia_unit_value' => $product_weight_unit,
                    );
                } else {
                    if (!isset($data_push[$parent_id])){
                        $arr_ecommerce[$parent_id] = (object)array(
                            'tokopedia_category_id' => $parent_item['tokped_kategori_id'],
                            'tokopedia_etalase' => $parent_item['tokped_etalase_id'],
                            'product_price' => $parent_item['harga_tokped'],
                            'tokopedia_unit_value' => $product_weight_unit,
                        );
                    }
                }
                // create array push tokped
                // $inserted_tokped_obj = array(
                //     "internal_id" => $parent_id,
                //     "product_image" => "http://yukmarket.id/img/noresult-image-bw.png",
                //     "product_description" => $parent_item['prod_desc'],
                //     "product_name" => rtrim(explode('-',$parent_item['prod_name'])[0]),
                //     "product_id" => null,
                //     "parent_product_code" => $parent_item['prod_code'],
                //     "bruto" => $parent_item['prod_bruto'],
                //     "tokopedia_category" => $parent_item['tokped_kategori_id'],
                //     "product_price" => $parent_item['harga_tokped'],
                //     "stock" => 1,
                //     "variant" => $variant,
                //     "etalase" => $parent_item['tokped_etalase_id']
                // );

                // // push tokped
                // $tokped = $this->push($inserted_tokped_obj);
                // if (isset($tokped)) {
                //     if (count($tokped) > 0) {
                //         $data_fail = (object)array(
                //             'prod_code' => $parent_item['prod_code'],
                //             'prod_name' => $parent_item['prod_name'],
                //             'message' => $tokped,
                //         );
                //         array_push($res_fail, $data_fail);
                //     }
                // } else {
                //     $data_fail = (object)array(
                //         'prod_code' => $parent_item['prod_code'],
                //         'prod_name' => $parent_item['prod_name'],
                //         'message' => 'Gagal Push Ke Tokopedia',
                //     );
                //     array_push($res_fail, $data_fail);
                // }
            }
            $PushData = new PushData();
            $push_data_ecommerce = $PushData->pushEcommerce($arr_ecommerce);
            if ($push_data_ecommerce->status == 200) {
                if ($push_data_ecommerce->error_rows > 0) {
                    foreach ($push_data_ecommerce->error as $error) {
                        $data_fail = (object)array(
                            'prod_code' => $error->sku,
                            'prod_name' => $error->product_name,
                            'message' => $error->error[0],
                        );
                        array_push($res_fail, $data_fail);
                    }
                }
                $tokped = 200;
            } else {
                $tokped = 500;
            }
            ob_clean();
            if (count($res_fail) > 0 ) {
                DB::rollback();
                $test = 'MAsuk Sini 1';
            } elseif ($tokped == 500) {
                DB::rollback();
                $test = 'MAsuk Sini 2';
            } else {
                DB::commit();
                $test = 'MAsuk Sini 3';
            }
            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'tokped' => $tokped,
                'tets' => $test,
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            DB::rollback();
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);
    }

    public function changeVariant(){
        // echo "test";
        // exit;

        return view('admin::products/change_variant');
    }

    public function changeVariantForm() {
        Excel::import(new ProductVariantImport,request()->file('import_new_variant'));
        // exit;
        return back();
    }


    public function importProductVariant(Request $request)
    {
        // dd($request->all());
        $res = array();
        $res_fail = array();
        $status = 500;

        DB::beginTransaction();
        try {
            //code...
            $file = $request->file('file');
            $status = 200;
            $collection = Excel::toCollection(new InventoryImport, $file);

            // get etalase tokopedia
            try {
                $this->generateToken();
                $etalase = $this->etalaseTokped();
            } catch (\Exception $e) {
                $etalase = '';
            }

            $parent = array();
            $child = array();
            foreach ($collection[0] as $key => $row) {
                if (isset($row[0]) and $row[0] != '') {
                    $err = "";
                    $prod_code = trim($row[0]);
                    $parent_prod_code = trim($row[1]);
                    $varian_parent = trim($row[2]);
                    $prod_name = trim($row[3]);
                    $prod_desc = trim($row[4]);
                    $tokped_etalase = trim($row[5]);
                    $tokped_kategori = trim($row[6]);
                    $prod_category = trim($row[7]);
                    $prod_tag = trim($row[8]);
                    $prod_bruto = trim($row[9]);
                    $satuan_jual = trim($row[10]);
                    $satuan_pengukuran = trim($row[11]);
                    $data_gudang = trim($row[12]);
                    $tipe_stok = trim($row[13]);

                    // kolom induk tidak boleh kosong.
                    if (!isset($parent_prod_code) || empty($parent_prod_code) || $parent_prod_code == "") {
                        $status = 500;
                        $err .= "Tidak ada data di kolom induk.";
                        $err .= "<br>";
                    }

                    //cek nama product 00_product_ecommerce, prod_ecom_name
                    $tokped_name = DB::table('00_product_ecommerce')
                        ->where('prod_ecom_name', '=', $prod_name)
                        ->where('active_flag', '=', 1)
                        ->first();

                    if (isset($tokped_name) && !empty($tokped_name)) {
                        $status = 500;
                        $err .= "Nama Produk Sudah terdaftar di Tokopedia";
                        $err .= "<br>";
                    }

                    // cek nama harus mengandung - untuk explode.
                    $cek_name = explode("-", trim($prod_name));
                    if (count($cek_name) > 1) {
                        if (isset($cek_name[1])) {

                        } else {
                            $status = 500;
                            $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                        $err .= "<br>";
                    }


                    // cek numerik bruto, sat jual
                    if (!is_numeric($prod_bruto) || !is_numeric($satuan_jual)) {
                        $status = 500;
                        $err .= "Bruto atau satuan jual bukan numeric";
                        $err .= "<br>";
                    }

                    // cek produk code
                    $exist_prod = DB::table('00_product')
                        ->where('prod_code', '=', $prod_code)
                        ->whereIn('active_flag', [1, 0])
                        ->count();
                    if ($exist_prod > 0) {
                        $status = 500;
                        $err .= "Kode produk telah terdaftar";
                        $err .= "<br>";
                    }

                    //cek etalase tokopedia
                    $etalase_id = "";
                    $tokped_etalase_id = "";
                    if (isset($etalase) && !empty($etalase) && $etalase != "") {
                        foreach ($etalase as $etalase_item) {
                            if (strtolower($tokped_etalase) == strtolower($etalase_item['etalase_name'])) {
                                $etalase_id = $etalase_item['etalase_id'];
                                break;
                            }
                        }

                        if (isset($etalase_id) && !empty($etalase_id) && $etalase_id != "") {
                            $tokped_etalase_id = $etalase_id;
                        } else {
                            $status = 500;
                            $err .= "Etalase produk tidak ditemukan.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Gagal mendapatkan data etalase.";
                        $err .= "<br>";
                    }

                    //cek kategori tokopedia
                    $tokopedia_category = CategoryEcommerce::select(
                        '00_category_per_ecommerce.category_id',
                        '00_category_per_ecommerce.category_id_ecommerce',
                        '00_category_per_ecommerce.category_name',
                        'category_id_ecommerce',
                        DB::raw('(SELECT pc.category_id FROM 00_product_ecommerce AS pc
                                    WHERE pc.prod_id = "' . $request->product . '") AS tokped_category_id'
                        )
                    )
                        ->join('00_category', '00_category.category_id', '=', '00_category_per_ecommerce.category_id')
                        ->where('category_level', 3)
                        ->where('organization_id', 28)
                        ->where('00_category.active_flag', 1)
                        ->where('00_category_per_ecommerce.category_name', '=', $tokped_kategori) //$tokped_kategori
                        ->first();

                    $tokped_kategori_id = "";
                    if (isset($tokopedia_category) && !empty($tokopedia_category)) {
                        $tokped_kategori_id = $tokopedia_category->category_id_ecommerce;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek kategori produk
                    $category = DB::table('00_category')
                        ->where('category_name', '=', $prod_category)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $category_id = "";
                    if (isset($category) && !empty($category)) {
                        $category_id = $category->category_id;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek penanda
                    $tag_arr = explode(";", $prod_tag);
                    $tag_id = array();
                    $tag_status = 0;
                    foreach ($tag_arr as $tag_item) {
                        // cek master data
                        $tag = DB::table('00_tag')
                            ->where('tag_name', '=', trim($tag_item))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($tag) && !empty($tag)) {
                            array_push($tag_id, $tag->tag_id);
                        } else {
                            $tag_status = 1;
                        }
                    }
                    if ($tag_status != 0) {
                        $status = 500;
                        $err .= "Penanda tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek satuan pengukuran
                    $uom = DB::table('00_uom')
                        ->where('uom_name', '=', $satuan_pengukuran)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $uom_id = "";
                    if (isset($uom) && !empty($uom)) {
                        $uom_id = $uom->uom_id;
                    } else {
                        $status = 500;
                        $err .= "Satuan pengukuran tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek gudang
                    $arr_gudang = explode(";", $data_gudang);
                    $warehouse_id = array();
                    $harga_tokopedia = 0;
                    $warehouse_status = 0;
                    $warehouse_keterangan = "";
                    foreach ($arr_gudang as $gudang_item) {
                        $arr_gudang_detail = explode(",", $gudang_item);
                        // cek nama gudang
                        $warehouse = DB::table('00_warehouse')
                            ->where('warehouse_name', '=', trim($arr_gudang_detail[0]))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($warehouse) && !empty($warehouse)) {
                            // cek numeric
                            if (is_numeric(trim($arr_gudang_detail[1])) && is_numeric(trim($arr_gudang_detail[2]))) {
                                $temp_warehouse['warehouse_id'] = $warehouse->warehouse_id;
                                $temp_warehouse['harga'] = $arr_gudang_detail[1];
                                $temp_warehouse['min_stok'] = $arr_gudang_detail[2];
                                // cek tipe gudang untuk harga tokopedia
                                if ($warehouse->warehouse_type_id == 2) {
                                    if ($harga_tokopedia == 0) {
                                        $harga_tokopedia = $arr_gudang_detail[1];
                                    }
                                }
                                array_push($warehouse_id, $temp_warehouse);
                            } else {
                                $warehouse_status = 1;
                                $warehouse_keterangan .= "Harga produk atau minimum stok bukan numerik";
                                $warehouse_keterangan .= "<br>";
                            }

                        } else {
                            $warehouse_status = 1;
                            $warehouse_keterangan .= "Gudang tidak ditemukan";
                            $warehouse_keterangan .= "<br>";
                        }
                    }

                    if ($warehouse_status != 0) {
                        $status = 500;
                        $err .= $warehouse_keterangan;
                        $err .= "<br>";
                    }

                    if ($harga_tokopedia == 0) {
                        $status = 500;
                        $err .= "Tidak ada data untuk harga tokopedia. Harap periksa pilihan gudang.";
                        $err .= "<br>";
                    }

                    $id_tipe_stok = null;
                    if (isset($tipe_stok) && !empty($tipe_stok)) {
                        if (strtoupper('ya') == strtoupper($tipe_stok) && $varian_parent == null) {
                            $id_tipe_stok = 1;
                        } else if (strtoupper('tidak') == strtoupper($tipe_stok) && $varian_parent != null) {
                            $id_tipe_stok = 0;
                        } else {
                            $status = 500;
                            $err .= "Tipe stok tidak sesuai.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Tidak ada data tipe stok.";
                        $err .= "<br>";
                    }

                    // validasi nama varian tidak lebih dari 15 char
                    $varian_length = strlen(trim(explode('-', $prod_name)[1]));
                    if ($varian_length > 15) {
                        $status = 500;
                        $err .= "Nama varian lebih dari 15 character.";
                        $err .= "<br>";
                    }


                    // cari id dari kode produk di induk
                    $parent_prod = DB::table('00_product')
                        ->where('prod_code', '=', $parent_prod_code)
                        ->whereIn('active_flag', [1, 0])
                        ->first();

                    $parent_prod_id = "";
                    if (isset($parent_prod) && !empty($parent_prod)) {
                        $parent_prod_id = $parent_prod->prod_id;
                        $parent_prod_name = $parent_prod->prod_name;
                        // cek name parent udah sesuai blm?
                        if (isset($parent_prod_name[1])) {

                        } else {
                            $status = 500;
                            $err .= "Nama parent Produk dalam database harus mengandung tanda '-' untuk memisahkan varian.";
                            $err .= "<br>";
                        }

                    } else {
                        $status = 500;
                        $err .= "Induk tidak ditemukan.";
                        $err .= "<br>";
                    }

                    // cari id dari kode produk di varian_id
                    $varian_parent_id = "";
                    if (isset($varian_parent) && !empty($varian_parent) && $varian_parent != "") {
                        $varian_prod = DB::table('00_product')
                            ->where('prod_code', '=', $varian_parent)
                            ->whereNull('parent_prod_id')
                            ->whereIn('active_flag', [1, 0])
                            ->first();


                        if (isset($varian_prod) && !empty($varian_prod)) {
                            $varian_parent_id = $varian_prod->prod_id;
                        } else {
                            $status = 500;
                            $err .= "Kode Produk untuk stok tidak ditemukan.";
                            $err .= "<br>";
                        }
                    }

                    // add to arr child
                    $temp['prod_code'] = $prod_code; //
                    $temp['parent_prod_code'] = $parent_prod_code; //
                    $temp['parent_prod_id'] = $parent_prod_id; //
                    $temp['prod_name'] = $prod_name; //
                    $temp['prod_desc'] = $prod_desc; //
                    $temp['tokped_etalase_id'] = $tokped_etalase_id;
                    $temp['tokped_kategori_id'] = $tokped_kategori_id;
                    $temp['harga_tokped'] = $harga_tokopedia;
                    $temp['prod_category'] = $category_id;
                    $temp['prod_tag'] = $tag_id;
                    $temp['prod_bruto'] = $prod_bruto;
                    $temp['satuan_jual'] = $satuan_jual;
                    $temp['satuan_pengukuran'] = $uom_id;
                    $temp['gudang'] = $warehouse_id;
                    $temp['stockable_flag'] = $id_tipe_stok;
                    $temp['varian_parent'] = $varian_parent;
                    $temp['varian_parent_id'] = $varian_parent_id;

                    array_push($child, $temp);


                    if (isset($err) && !empty($err)) {
                        $data_fail = (object)array(
                            'prod_code' => $prod_code,
                            'prod_name' => $prod_name,
                            'message' => $err,
                        );
                        array_push($res_fail, $data_fail);
                    }
                }
            }

            // cek validasi fail?
            if ($status == 500) {
                $result = (object)array(
                    'isSuccess' => true,
                    'title' => 'Success',
                    'data' => $res,
                    'data_fail' => $res_fail,
                    'message' => 'Get Data Import!',
                    'icon' => 'success'
                );
                return response()->json($result);
            }

            // proses to db
            $variant = array();
            foreach ($child as $child_item) {

                // get data untuk array parent & varian 0.
                $parent_prod = DB::table('00_product')
                    ->where('prod_id', '=', $child_item['parent_prod_id'])
                    ->whereIn('active_flag', [1, 0])
                    ->first();

                $get_price = DB::table('00_product_warehouse')
                    ->selectRaw(" MAX(00_product_warehouse.prod_price) as prod_price ")
                    ->leftJoin('00_warehouse','00_product_warehouse.warehouse_id','=','00_warehouse.warehouse_id')
                    ->where('00_product_warehouse.prod_id', '=', $child_item['parent_prod_id'])
                    ->where('00_warehouse.warehouse_type_id', '=', 2)
                    ->first();


                $parent_prod_ecom = DB::table('00_product_ecommerce')
                    ->where('prod_id', '=', $child_item['parent_prod_id'])
                    // ->whereIn('active_flag', [1, 0])
                    ->first();
                
                $internal_id = $parent_prod->prod_id;
                $product_description = $child_item['prod_desc'];
                $parent_product_name = $parent_prod->prod_name;
                $product_ecom_code = $parent_prod_ecom->prod_ecom_code;
                $parent_product_code = $parent_prod->prod_code;
                $bruto = $parent_prod->bruto;
                $kategori_tokopedia = $child_item['tokped_kategori_id'];
                $etalase = $child_item['tokped_etalase_id'];
                $product_price = isset($get_price) ? $get_price->prod_price : $parent_prod->prod_price;

                $variant[0] = array(
                    "name" => trim(explode('-', $parent_product_name)[1]),
                    "picture" => "http://yukmarket.id/img/ecomm-default.png",
                    "stock" => 1,
                    "id" => $child_item['parent_prod_id'],
                    "is_primary" => true
                );

                // cari existing produk varian, masukin ke array varian
                $varian_prod = DB::table('00_product')
                    ->where('variant_id', '=', $child_item['parent_prod_id'])
                    ->whereIn('active_flag', [1, 0])
                    ->get();

                foreach ($varian_prod as $detail_varian) {

                    if ($detail_varian->prod_image == null) {
                        $prod_image = "http://yukmarket.id/img/ecomm-default.png";
                    } else {
                        $prod_image = request()->root() . '/' . $detail_varian->prod_image;
                    }

                    array_push($variant, array(
                        "name" => trim(explode('-', $detail_varian->prod_name)[1]),
                        "picture" => $prod_image,
                        "stock" => 1,
                        "id" => $detail_varian->prod_id,
                        "is_primary" => false
                    ));
                }

                // insert child
                // pengecekan insert child with parent?
                if (isset($child_item['varian_parent']) && !empty($child_item['varian_parent']) && $child_item['varian_parent'] != "") {
                    //insert baru varian_id & parent_id
                    $inserted_variant_obj = [
                        'prod_code' => $child_item['prod_code'],
                        'prod_name' => $child_item['prod_name'],
                        'prod_desc' => $child_item['prod_desc'],
                        //'brand_id' => $request->input('product_brand'),
                        //'preorder_flag' => 'N',
                        //'product_sku_id' => $parent_sku_id,
                        //'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                        //'shipping_durability_min' => $request->input('shipping_durability_min'),
                        //'shipping_durability_max' => $request->input('shipping_durability_max'),
                        //'shipping_durability_id' => $request->input('shipping_durability_unit'),
                        // 'stockable_flag' => $request->input('stockable_flag'),
                        // 'stock' => $request->input('product_stock'),
                        // 'minimum_stock' => $request->input('minimum_stock'),
                        //'minimum_order' => $request->input('minimum_order'),
                        'uom_id' => $child_item['satuan_pengukuran'],
                        'uom_value' => $child_item['satuan_jual'],
                        //'prod_image' => $product_image_location,
                        'bruto' => $child_item['prod_bruto'],
                        'bruto_uom_id' => 1,
                        'active_flag' => 1,
                        'parent_prod_id' => $child_item['varian_parent_id'],
                        'stockable_flag' => $child_item['stockable_flag'],
                        'category_id' => $child_item['prod_category'],
                        //'is_taxable_product' => $request->input('is_taxable_product'),
                        //'tax_value' => $request->input('tax_value') / 100,
                        'created_by' => Session::get('users')['id'],
                        //'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                        //'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                        //'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                        'variant_id' => $child_item['parent_prod_id'],
                    ];
                } else {
                    //insert baru varian_id
                    $inserted_variant_obj = [
                        'prod_code' => $child_item['prod_code'],
                        'prod_name' => $child_item['prod_name'],
                        'prod_desc' => $child_item['prod_desc'],
                        //'brand_id' => $request->input('product_brand'),
                        //'preorder_flag' => 'N',
                        //'product_sku_id' => $parent_sku_id,
                        //'prod_price' => $request->input('product_price') == "" || $request->input('product_price') == null ? null : str_replace(".","",$request->input('product_price')),
                        //'shipping_durability_min' => $request->input('shipping_durability_min'),
                        //'shipping_durability_max' => $request->input('shipping_durability_max'),
                        //'shipping_durability_id' => $request->input('shipping_durability_unit'),
                        // 'stockable_flag' => $request->input('stockable_flag'),
                        // 'stock' => $request->input('product_stock'),
                        // 'minimum_stock' => $request->input('minimum_stock'),
                        //'minimum_order' => $request->input('minimum_order'),
                        'uom_id' => $child_item['satuan_pengukuran'],
                        'uom_value' => $child_item['satuan_jual'],
                        //'prod_image' => $product_image_location,
                        'bruto' => $child_item['prod_bruto'],
                        'bruto_uom_id' => 1,
                        'active_flag' => 1,
                               // 'parent_prod_id' => $parent_id,
                        'stockable_flag' => $child_item['stockable_flag'],
                        'category_id' => $child_item['prod_category'],
                        //'is_taxable_product' => $request->input('is_taxable_product'),
                        //'tax_value' => $request->input('tax_value') / 100,
                        'created_by' => Session::get('users')['id'],
                        //'dim_length' => (($request->input('product_variant_length')[$b] == null || $request->input('product_variant_length')[$b] == "") ? null : $request->input('product_variant_length')[$b]),
                        //'dim_width' => (($request->input('product_variant_width')[$b] == null || $request->input('product_variant_width')[$b] == "") ? null : $request->input('product_variant_width')[$b]),
                        //'dim_height' => (($request->input('product_variant_height')[$b] == null || $request->input('product_variant_height')[$b] == "") ? null : $request->input('product_variant_height')[$b])
                        'variant_id' => $child_item['parent_prod_id'],
                    ];
                }
                $product_variant_id = Product::insertGetId($inserted_variant_obj);

                // insert product_tag
                $product_tag_array = array();
                foreach ($child_item['prod_tag'] as $id_tag_item) {
                    $obj = array(
                        "prod_id" => $product_variant_id,
                        "tag_id" => $id_tag_item,
                        "active_flag" => 1,
                        "created_by" => Session::get('users')['id']
                    );
                    array_push($product_tag_array, $obj);
                }
                ProductTag::insert($product_tag_array);

                // insert product warehouse
                $warehouse_array = array();
                foreach ($child_item['gudang'] as $gudang_detail) {
                    $obj = array(
                        "prod_id" => $product_variant_id,
                        "warehouse_id" => $gudang_detail['warehouse_id'],
                        "prod_price" => $gudang_detail['harga'],
                        "minimum_stock" => $gudang_detail['min_stok'],
                        "active_flag" => 1
                    );
                    array_push($warehouse_array, $obj);
                }
                ProductWarehouse::insert($warehouse_array);

                $product_weight = $this->getBeratProductTokped($kategori_tokopedia);
                $product_weight = array_filter($product_weight, function($value){
                    return $value['variant_id'] == 1;
                });
                $product_weight = array_values($product_weight);
                $product_weight_unit = $product_weight[0]['units'][0]['values'];
                if (count($arr_ecommerce) <= 0) {
                    $arr_ecommerce[$internal_id] = (object)array(
                        'tokopedia_category_id' => $kategori_tokopedia,
                        'tokopedia_etalase' => $etalase,
                        'product_price' => $product_price,
                        'tokopedia_unit_value' => $product_weight_unit,
                    );
                } else {
                    if (!isset($data_push[$internal_id])){
                        $arr_ecommerce[$internal_id] = (object)array(
                            'tokopedia_category_id' => $kategori_tokopedia,
                            'tokopedia_etalase' => $etalase,
                            'product_price' => $product_price,
                            'tokopedia_unit_value' => $product_weight_unit,
                        );
                    }
                }
                // create array child push tokped
                // array_push($variant, array(
                //     "name" => trim(explode("-", $child_item['prod_name'])[1]),
                //     "picture" => "http://yukmarket.id/img/noresult-image-bw.png",
                //     "stock" => 1,
                //     "id" => $product_variant_id,
                //     "is_primary" => false
                // ));

                // $inserted_tokped_obj = array(
                //     "internal_id" => $internal_id,
                //     "product_image" => "http://yukmarket.id/img/noresult-image-bw.png",
                //     "product_description" => $product_description,
                //     "product_name" => rtrim(explode('-', $parent_product_name)[0]),
                //     "product_id" => $product_ecom_code, // product ecom code
                //     "parent_product_code" => $parent_product_code,
                //     "bruto" => $parent_prod->bruto,
                //     "tokopedia_category" => $kategori_tokopedia,
                //     "product_price" => $product_price,
                //     "stock" => 1,
                //     "variant" => $variant,
                //     "etalase" => $etalase,
                // );

                // // push tokped
                // $tokped = $this->push($inserted_tokped_obj);
                // if (isset($tokped)) {
                //     if (count($tokped) > 0) {
                //         $data_fail = (object)array(
                //             'prod_code' => $parent_product_code,
                //             'prod_name' => $parent_product_name,
                //             'message' => $tokped,
                //         );
                //         array_push($res_fail, $data_fail);
                //     }
                // } else {
                //     $data_fail = (object)array(
                //         'prod_code' => $parent_product_code,
                //         'prod_name' => $parent_product_name,
                //         'message' => 'Gagal Push Ke Tokopedia',
                //     );
                //     array_push($res_fail, $data_fail);
                //     $tokped = 'Gagal Push Ke Tokopedia';
                // }

            }
            ob_clean();
            if (count($res_fail) > 0) {
                DB::rollback();
            } else {
                DB::commit();
            }

            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'tokped' => $tokped,
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            DB::rollback();
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);
    }

    public function importProduct(Request $request){
        // dd($request->all());
        $res = array();
        $res_fail = array();
        $status = 500;

        DB::beginTransaction();
        try {
            //code...
            $file = $request->file('file');
            $status = 200;
            $collection = Excel::toCollection(new InventoryImport, $file);

            // get etalase tokopedia
            try{
                $this->generateToken();
                $etalase =$this->etalaseTokped();
                $message = '';
            } catch (\Exception $e) {
                $etalase = '';
                $message = $e->getMessage();
            }
            $parent = array();
            $child = array();
            $prod_code_validation = array();
            $arr_prod_ym = array();
            foreach ($collection[0] as $key => $row){
                if (isset($row[0]) and $row[0] != '') {
                    $err = "";
                    $prod_code = trim($row[0]);
                    $parent_prod_code = trim($row[1]);
                    $varian_parent = trim($row[2]);
                    $prod_name = trim($row[3]);
                    $prod_desc = trim($row[4]);
                    $tokped_etalase = trim($row[5]);
                    $tokped_kategori = trim($row[6]);
                    $prod_category = trim($row[7]);
                    $prod_tag = trim($row[8]);
                    $prod_bruto = trim($row[9]);
                    $satuan_jual = trim($row[10]);
                    $satuan_pengukuran = trim($row[11]);
                    $data_gudang = trim($row[12]);
                    $tipe_stok = trim($row[13]);


                    // cek nama harus mengandung - untuk explode.
                    $cek_name = explode("-",trim($prod_name));
                    if (count($cek_name) > 1) {
                        if (isset($cek_name[1])) {
                            if ($cek_name[1] == null or $cek_name[1] == '') {
                                $status = 500;
                                $err .= "Nama Produk setelah tanda '-' tidak boleh kosong.";
                                $err .= "<br>";
                            }
                        } else {
                            $status = 500;
                            $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Nama Produk harus mengandung tanda '-' untuk memisahkan varian.";
                        $err .= "<br>";
                    }

                    // cek varian_parent, jika ada isi, cek induk harus ada.
                    if (isset($varian_parent) && !empty($varian_parent) && $varian_parent!="") {
                        if (!isset($parent_prod_code) || empty($parent_prod_code) || $parent_prod_code=="") {
                            $status = 500;
                            $err .= "Harap isi kolom induk.";
                            $err .= "<br>";
                        }
                    }
                    // cek numerik bruto, sat jual
                    if (!is_numeric($prod_bruto) || !is_numeric($satuan_jual)) {
                       // var_dump($prod_bruto);
                       // var_dump($satuan_jual);
                       // var_dump($prod_code);
                        $status = 500;
                        $err .= "Bruto atau satuan jual bukan numeric";
                        $err .= "<br>";
                    }

                    // cek produk code
                    $exist_prod = DB::table('00_product')
                        ->where('prod_code', '=', $prod_code)
                        ->whereIn('active_flag', [1, 0])
                        ->count();
                    if ($exist_prod > 0) {
                        $status = 500;
                        $err .= "Kode produk telah terdaftar";
                        $err .= "<br>";
                    }

                    //cek etalase tokopedia
                    $etalase_id = "";
                    $tokped_etalase_id = "";
                    if (isset($etalase) && !empty($etalase) && $etalase != "") {
                        foreach ($etalase as $etalase_item) {
                            if (strtolower($tokped_etalase) == strtolower($etalase_item['etalase_name'])) {
                                $etalase_id = $etalase_item['etalase_id'];
                                break;
                            }
                        }

                        if (isset($etalase_id) && !empty($etalase_id) && $etalase_id != "") {
                            $tokped_etalase_id = $etalase_id;
                        } else {
                            $status = 500;
                            $err .= "Etalase produk tidak ditemukan.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Gagal mendapatkan data etalase.";
                        $err .= "<br>";
                    }

                    //cek kategori tokopedia
                    $tokopedia_category = CategoryEcommerce::select(
                        '00_category_per_ecommerce.category_id',
                        '00_category_per_ecommerce.category_id_ecommerce',
                        '00_category_per_ecommerce.category_name',
                        'category_id_ecommerce',
                        DB::raw('(SELECT pc.category_id FROM 00_product_ecommerce AS pc
                                    WHERE pc.prod_id = "'.$request->product.'") AS tokped_category_id'
                        )
                    )
                        ->join('00_category','00_category.category_id','=','00_category_per_ecommerce.category_id')
                        ->where('category_level',3)
                        ->where('organization_id',28)
                        ->where('00_category.active_flag', 1)
                        ->where('00_category_per_ecommerce.category_name','=', $tokped_kategori) //$tokped_kategori
                        ->first();

                    $tokped_kategori_id = "";
                    if (isset($tokopedia_category) && !empty($tokopedia_category)) {
                        $tokped_kategori_id = $tokopedia_category->category_id_ecommerce;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tokopedia tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek kategori produk
                    $category = DB::table('00_category')
                        ->where('category_name', '=', $prod_category)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $category_id = "";
                    if (isset($category) && !empty($category)) {
                        $category_id = $category->category_id;
                    } else {
                        $status = 500;
                        $err .= "Kategori produk tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek penanda
                    $tag_arr = explode(";", $prod_tag);
                    $tag_id = array();
                    $tag_status = 0;
                    foreach ($tag_arr as $tag_item) {
                        // cek master data
                        $tag = DB::table('00_tag')
                            ->where('tag_name', '=', trim($tag_item))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($tag) && !empty($tag)) {
                            array_push($tag_id, $tag->tag_id);
                        } else {
                            $tag_status = 1;
                        }
                    }
                    if ($tag_status != 0) {
                        $status = 500;
                        $err .= "Penanda tidak ditemukan";
                        $err .= "<br>";
                    }

                    // cek satuan pengukuran
                    $uom = DB::table('00_uom')
                        ->where('uom_name', '=', $satuan_pengukuran)
                        ->where('active_flag', '=', 1)
                        ->first();

                    $uom_id = "";
                    if (isset($uom) && !empty($uom)) {
                        $uom_id = $uom->uom_id;
                    } else {
                        $status = 500;
                        $err .= "Satuan pengukuran tidak ditemukan";
                        $err .= "<br>";
                    }
                    // cek gudang
                    $arr_gudang = explode(";", $data_gudang);
                    $warehouse_id = array();
                    $harga_tokopedia = 0;
                    $warehouse_status = 0;
                    $warehouse_keterangan = "";
                    foreach ($arr_gudang as $gudang_item) {
                        $arr_gudang_detail = explode(",", $gudang_item);
                        // cek nama gudang
                        $warehouse = DB::table('00_warehouse')
                            ->where('warehouse_name', '=', trim($arr_gudang_detail[0]))
                            ->where('active_flag', '=', 1)
                            ->first();

                        if (isset($warehouse) && !empty($warehouse)) {
                            // cek numeric
                            if (is_numeric(trim($arr_gudang_detail[1])) && is_numeric(trim($arr_gudang_detail[2]))) {
                                $temp_warehouse['warehouse_id'] = $warehouse->warehouse_id;
                                $temp_warehouse['warehouse_type_id'] = $warehouse->warehouse_type_id;
                                $temp_warehouse['harga'] = $arr_gudang_detail[1];
                                $temp_warehouse['min_stok'] = $arr_gudang_detail[2];
                                // cek tipe gudang untuk harga tokopedia
                                if ($warehouse->warehouse_type_id == 2) {
                                    if ($harga_tokopedia == 0) {
                                        $harga_tokopedia = $arr_gudang_detail[1];
                                    }
                                }
                                array_push($warehouse_id, $temp_warehouse);
                            } else {
                                $warehouse_status = 1;
                                $warehouse_keterangan .= "Harga produk atau minimum stok bukan numerik";
                                $warehouse_keterangan .= "<br>";
                            }

                        } else {
                            $warehouse_status = 1;
                            $warehouse_keterangan .= "Gudang tidak ditemukan";
                            $warehouse_keterangan .= "<br>";
                        }
                    }

                    if ($warehouse_status != 0) {
                        $status = 500;
                        $err .= $warehouse_keterangan;
                        $err .= "<br>";
                    }

                    if ($harga_tokopedia == 0) {
                        $status = 500;
                        $err .= "Tidak ada data untuk harga tokopedia. Harap periksa pilihan gudang.";
                        $err .= "<br>";
                    }

                    $id_tipe_stok = null;
                    if (isset($tipe_stok) && !empty($tipe_stok)) {
                        if (strtoupper('ya') == strtoupper($tipe_stok) && $varian_parent == null) {
                            $id_tipe_stok = 1;
                        } else if (strtoupper('tidak') == strtoupper($tipe_stok) && $varian_parent != null) {
                            $id_tipe_stok = 0;
                        } else {
                            $status = 500;
                            $err .= "Tipe stok tidak sesuai.";
                            $err .= "<br>";
                        }
                    } else {
                        $status = 500;
                        $err .= "Tidak ada data tipe stok.";
                        $err .= "<br>";
                    }

                    // validasi nama varian tidak lebih dari 15 char
                    // if (count($cek_name) > 1) {
                    //     $varian_length = strlen(trim(explode('-', $prod_name)[1]));
                    //     if ($varian_length > 15) {
                    //         $status = 500;
                    //         $err .= "Nama varian lebih dari 15 character.";
                    //         $err .= "<br>";
                    //     }
                    // }
                    // cek varian parent

                    if (isset($varian_parent) && !empty($varian_parent) && $varian_parent != "") {
                        $validation = 0;
                        $product = DB::table('00_product')
                            ->select('prod_id')
                            ->where('prod_code', '=', $varian_parent)
                            ->first();
                        if (isset($product) && !empty($product)) {
                            $validation = 1;
                        } else {
                            foreach ($prod_code_validation as $item){
                                if ($varian_parent == $item) {
                                    $validation = 1;
                                    break;
                                }
                            }
                        }
                        if ($validation == 0) {
                            $status = 500;
                            $err .= "Kode produk untuk stok tidak ditemukan.";
                            $err .= "<br>";
                        }
                    }
                    if (isset($err) && !empty($err)) {
                        $data_fail = (object)array(
                            'prod_code' => $prod_code,
                            'prod_name' => $prod_name,
                            'message' => $err,
                        );
                        array_push($res_fail, $data_fail);
                    }

                    if (count($arr_prod_ym) <= 0) {
                        if ($parent_prod_code == "" || empty($parent_prod_code)) {
                            $arr_prod_ym[$prod_code] = (object)array(
                                'prod_code' => $prod_code,
                                'parent_prod_code' => null,
                                'varian_parent' => null,
                                'prod_name' => $prod_name,
                                'prod_desc' => $prod_desc,
                                'tokped_etalase_id' => $tokped_etalase_id,
                                'tokped_kategori_id' => $tokped_kategori_id,
                                'harga_tokped' => $harga_tokopedia,
                                'prod_category' => $category_id,
                                'prod_tag' => $tag_id,
                                'prod_bruto' => $prod_bruto,
                                'satuan_jual' => $satuan_jual,
                                'satuan_pengukuran' => $uom_id,
                                'gudang' => $warehouse_id,
                                'stockable_flag' => $id_tipe_stok,
                                'variant' => array(),
                            );
                        } else {
                            $arr_prod_ym[$parent_prod_code] = (object)array(
                                'prod_code' => null,
                                'parent_prod_code' => null,
                                'prod_name' => null,
                                'prod_desc' => null,
                                'tokped_etalase_id' => null,
                                'tokped_kategori_id' => null,
                                'harga_tokped' => null,
                                'prod_category' => null,
                                'prod_tag' => null,
                                'prod_bruto' => null,
                                'satuan_jual' => null,
                                'satuan_pengukuran' => null,
                                'gudang' => null,
                                'stockable_flag' => null,
                                'variant' => array()
                            );
                            $obj = (object)array(
                                'prod_code' => $prod_code,
                                'parent_prod_code' => $parent_prod_code,
                                'varian_parent' => $varian_parent,
                                'prod_name' => $prod_name,
                                'prod_desc' => $prod_desc,
                                'tokped_etalase_id' => $tokped_etalase_id,
                                'tokped_kategori_id' => $tokped_kategori_id,
                                'harga_tokped' => $harga_tokopedia,
                                'prod_category' => $category_id,
                                'prod_tag' => $tag_id,
                                'prod_bruto' => $prod_bruto,
                                'satuan_jual' => $satuan_jual,
                                'satuan_pengukuran' => $uom_id,
                                'gudang' => $warehouse_id,
                                'stockable_flag' => $id_tipe_stok,
                            );
                            array_push($arr_prod_ym[$parent_prod_code]->variant, $obj);
                        }
                    } else {
                        if (isset($arr_prod_ym[$parent_prod_code])) {
                            $obj = (object)array(
                                'prod_code' => $prod_code,
                                'parent_prod_code' => $parent_prod_code,
                                'varian_parent' => $varian_parent,
                                'prod_name' => $prod_name,
                                'prod_desc' => $prod_desc,
                                'tokped_etalase_id' => $tokped_etalase_id,
                                'tokped_kategori_id' => $tokped_kategori_id,
                                'harga_tokped' => $harga_tokopedia,
                                'prod_category' => $category_id,
                                'prod_tag' => $tag_id,
                                'prod_bruto' => $prod_bruto,
                                'satuan_jual' => $satuan_jual,
                                'satuan_pengukuran' => $uom_id,
                                'gudang' => $warehouse_id,
                                'stockable_flag' => $id_tipe_stok,
                            );
                            array_push($arr_prod_ym[$parent_prod_code]->variant, $obj);
                        } elseif (isset($arr_prod_ym[$prod_code])) {
                            $arr_prod_ym[$prod_code]->prod_code = $prod_code;
                            $arr_prod_ym[$prod_code]->parent_prod_code = $parent_prod_code;
                            $arr_prod_ym[$prod_code]->varian_parent = $varian_parent;
                            $arr_prod_ym[$prod_code]->prod_name = $prod_name;
                            $arr_prod_ym[$prod_code]->prod_desc = $prod_desc;
                            $arr_prod_ym[$prod_code]->tokped_etalase_id = $tokped_etalase_id;
                            $arr_prod_ym[$prod_code]->tokped_kategori_id = $tokped_kategori_id;
                            $arr_prod_ym[$prod_code]->harga_tokped = $harga_tokopedia;
                            $arr_prod_ym[$prod_code]->prod_category = $category_id;
                            $arr_prod_ym[$prod_code]->prod_tag = $tag_id;
                            $arr_prod_ym[$prod_code]->prod_bruto = $prod_bruto;
                            $arr_prod_ym[$prod_code]->satuan_jual = $satuan_jual;
                            $arr_prod_ym[$prod_code]->satuan_pengukuran = $uom_id;
                            $arr_prod_ym[$prod_code]->gudang = $warehouse_id;
                            $arr_prod_ym[$prod_code]->stockable_flag = $id_tipe_stok;
                        } else {
                            if ($parent_prod_code == "" || empty($parent_prod_code)) {
                                $arr_prod_ym[$prod_code] = (object)array(
                                    'prod_code' => $prod_code,
                                    'parent_prod_code' => null,
                                    'varian_parent' => null,
                                    'prod_name' => $prod_name,
                                    'prod_desc' => $prod_desc,
                                    'tokped_etalase_id' => $tokped_etalase_id,
                                    'tokped_kategori_id' => $tokped_kategori_id,
                                    'harga_tokped' => $harga_tokopedia,
                                    'prod_category' => $category_id,
                                    'prod_tag' => $tag_id,
                                    'prod_bruto' => $prod_bruto,
                                    'satuan_jual' => $satuan_jual,
                                    'satuan_pengukuran' => $uom_id,
                                    'gudang' => $warehouse_id,
                                    'stockable_flag' => $id_tipe_stok,
                                    'variant' => array(),
                                );
                            } else {
                                $arr_prod_ym[$parent_prod_code] = (object)array(
                                    'prod_code' => null,
                                    'parent_prod_code' => null,
                                    'varian_parent' => null,
                                    'prod_name' => null,
                                    'prod_desc' => null,
                                    'tokped_etalase_id' => null,
                                    'tokped_kategori_id' => null,
                                    'harga_tokped' => null,
                                    'prod_category' => null,
                                    'prod_tag' => null,
                                    'prod_bruto' => null,
                                    'satuan_jual' => null,
                                    'satuan_pengukuran' => null,
                                    'gudang' => null,
                                    'stockable_flag' => null,
                                    'variant' => array()
                                );
                                $obj = (object)array(
                                    'prod_code' => $prod_code,
                                    'parent_prod_code' => $parent_prod_code,
                                    'varian_parent' => $varian_parent,
                                    'prod_name' => $prod_name,
                                    'prod_desc' => $prod_desc,
                                    'tokped_etalase_id' => $tokped_etalase_id,
                                    'tokped_kategori_id' => $tokped_kategori_id,
                                    'harga_tokped' => $harga_tokopedia,
                                    'prod_category' => $category_id,
                                    'prod_tag' => $tag_id,
                                    'prod_bruto' => $prod_bruto,
                                    'satuan_jual' => $satuan_jual,
                                    'satuan_pengukuran' => $uom_id,
                                    'gudang' => $warehouse_id,
                                    'stockable_flag' => $id_tipe_stok,
                                );
                                array_push($arr_prod_ym[$parent_prod_code]->variant, $obj);
                            }
                        }
                    }
                }
            }
            // echo "<Pre>";
            // print_r(json_decode(json_encode($arr_prod_ym)));
            // echo "</Pre>";
            // exit();
            if ($status == 500) {
                DB::rollback();
                $result = (object)array(
                    'isSuccess' => true,
                    'title' => 'Success',
                    'data' => $res,
                    'data_fail' => $res_fail,
                    'tokped' => null,
                    'message' => 'Get Data Import!',
                    'message' => 'Get Data Import!',
                    'icon' => 'success'
                );
                return response()->json($result);
            }

            $PushData = new PushData();
            $arr_ecommerce = array();
            $arr_ecommerce_update = array();
            $status_ym = 200;
            foreach ($arr_prod_ym as $prod_code_ym => $prod_ym) {
                if ($prod_ym->prod_code != '' or isset($prod_ym->prod_code)) {
                    $prod_name_implode = explode("-",$prod_ym->prod_name);
                    $prodName = count($prod_name_implode) > 1 ? $prod_name_implode[0] : $prod_ym->prod_name ;
                    $check_prod_parent = Product::where('prod_name', 'like', '%' . $prodName . '%')->first();
                }
                
                if (isset($check_prod_parent)) {
                    $status = 500;
                    $err = "Nama Produk '".trim($prodName)."' sudah ada didatabase atau diexcel.";
                    $err .= "<br>";
                    $data_fail = (object)array(
                        'prod_code' => $prod_ym->prod_code,
                        'prod_name' => $prod_ym->prod_name,
                        'message' => $err,
                    );
                    array_push($res_fail, $data_fail);
                } else {
                    $check_prod = Product::where('prod_code', $prod_code_ym)->first();
                    if (isset($check_prod)) { //JIKA PRODUK SUDAH ADA
                        if (isset($prod_ym->prod_code)) {
                            $status = 500;
                            $err = "Kode produk sudah ada.";
                            $err .= "<br>";
                            $data_fail = (object)array(
                                'prod_code' => $prod_ym->prod_code,
                                'prod_name' => $prod_ym->prod_name,
                                'message' => $err,
                            );
                            array_push($res_fail, $data_fail);
                        } else {
                            if (count($prod_ym->variant) > 0) {
                                foreach ($prod_ym->variant as $variant) {
                                    $check_variant = Product::where('prod_code', $variant->prod_code)->first();
                                    if (isset($check_variant)) {
                                        $status = 500;
                                        $err = "Kode produk sudah ada.";
                                        $err .= "<br>";
                                        $data_fail = (object)array(
                                            'prod_code' => $variant->prod_code,
                                            'prod_name' => $variant->prod_name,
                                            'message' => $err,
                                        );
                                        array_push($res_fail, $data_fail);
                                    } else {
                                        $push_data_ym = $PushData->pushToYm($check_prod->prod_id, $variant);
                                        if ($push_data_ym->status == 200) {
                                            if ($push_data_ym->data->flag_ecom) {
                                                if (!isset($arr_ecommerce_update[$push_data_ym->data->parent_id])){
                                                    $arr_ecommerce_update[$push_data_ym->data->parent_id] = (object)array(
                                                        'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                        'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                        'product_price' => $push_data_ym->data->harga_tokped,
                                                    );
                                                }
                                            } else {
                                               if (!isset($arr_ecommerce[$push_data_ym->data->parent_id])){
                                                    $arr_ecommerce[$push_data_ym->data->parent_id] = (object)array(
                                                        'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                        'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                        'product_price' => $push_data_ym->data->harga_tokped,
                                                    );
                                                }
                                            }
                                        } else {
                                            $status_ym = 500;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    } else { //TAMBAH PRODUK BARU

                        if (isset($prod_ym->prod_code)) {
                            $prod_name_implode = explode("-",$prod_ym->prod_name);
                            $prodName = count($prod_name_implode) > 1 ? $prod_name_implode[0] : $prod_ym->prod_name ;
                            $tokped_name = DB::table('00_product_ecommerce')
                                            ->where('prod_ecom_name', '=', $prodName)
                                            ->first();
                            if (isset($tokped_name)) {
                                $status = 500;
                                $err = "Nama Produk Sudah terdaftar di Tokopedia.";
                                $err .= "<br>";
                                $data_fail = (object)array(
                                    'prod_code' => $prod_ym->prod_code,
                                    'prod_name' => $prod_ym->prod_name,
                                    'message' => $err,
                                );
                                array_push($res_fail, $data_fail);
                            } else {
                                $push_data_ym = $PushData->pushToYm(null, $prod_ym);
                                if ($push_data_ym->status == 200) {
                                    if ($push_data_ym->data->flag_ecom) {
                                        if (!isset($arr_ecommerce_update[$push_data_ym->data->parent_id])){
                                            $arr_ecommerce_update[$push_data_ym->data->parent_id] = (object)array(
                                                'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                'product_price' => $push_data_ym->data->harga_tokped,
                                            );
                                        }
                                    } else {
                                       if (!isset($arr_ecommerce[$push_data_ym->data->parent_id])){
                                            $arr_ecommerce[$push_data_ym->data->parent_id] = (object)array(
                                                'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                'product_price' => $push_data_ym->data->harga_tokped,
                                            );
                                        }
                                    }
                                } else {
                                    $status_ym = 500;
                                    break;
                                }
                                if (count($prod_ym->variant) > 0) {
                                    foreach ($prod_ym->variant as $variant) {
                                        $check_variant = Product::where('prod_code', $variant->prod_code)->first();
                                        $check_parent_variant = Product::where('prod_code', $variant->parent_prod_code)->first();
                                        if (!isset($check_parent_variant)) {
                                            $status = 500;
                                            $err = "Kode produk induk tidak ditemukan.";
                                            $err .= "<br>";
                                            $data_fail = (object)array(
                                                'prod_code' => $variant->prod_code,
                                                'prod_name' => $variant->prod_name,
                                                'message' => $err,
                                            );
                                            array_push($res_fail, $data_fail);
                                        } else {
                                            if (isset($check_variant)) {
                                                $status = 500;
                                                $err = "Kode produk sudah ada.";
                                                $err .= "<br>";
                                                $data_fail = (object)array(
                                                    'prod_code' => $variant->prod_code,
                                                    'prod_name' => $variant->prod_name,
                                                    'message' => $err,
                                                );
                                                array_push($res_fail, $data_fail);
                                            } else {
                                                $push_data_ym = $PushData->pushToYm($check_parent_variant->prod_id, $variant);
                                                if ($push_data_ym->status == 200) {
                                                    if ($push_data_ym->data->flag_ecom) {
                                                        if (!isset($arr_ecommerce_update[$push_data_ym->data->parent_id])){
                                                            $arr_ecommerce_update[$push_data_ym->data->parent_id] = (object)array(
                                                                'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                                'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                                'product_price' => $push_data_ym->data->harga_tokped,
                                                            );
                                                        }
                                                    } else {
                                                       if (!isset($arr_ecommerce[$push_data_ym->data->parent_id])){
                                                            $arr_ecommerce[$push_data_ym->data->parent_id] = (object)array(
                                                                'tokopedia_category_id' => $push_data_ym->data->tokped_kategori_id,
                                                                'tokopedia_etalase' => $push_data_ym->data->tokped_etalase_id,
                                                                'product_price' => $push_data_ym->data->harga_tokped,
                                                            );
                                                        }
                                                    }
                                                } else {
                                                    $status_ym = 500;
                                                    break;
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        } else {
                            if (count($prod_ym->variant) > 0) {
                                foreach ($prod_ym->variant as $variant) {
                                    $status = 500;
                                    $err = "Induk produk tidak tersedia, silahkan periksa kembali.";
                                    $err .= "<br>";
                                    $data_fail = (object)array(
                                        'prod_code' => $variant->prod_code,
                                        'prod_name' => $variant->prod_name,
                                        'message' => $err,
                                    );
                                    array_push($res_fail, $data_fail);
                                }
                            }
                        }

                    }
                }
                // if (isset($err) && !empty($err)) {
                //     $data_fail = (object)array(
                //         'prod_code' => $prod_code,
                //         'prod_name' => $prod_name,
                //         'message' => $err,
                //     );
                //     array_push($res_fail, $data_fail);
                // }
            }

            // cek validasi fail?
            if ($status == 500 or $status_ym == 500) {
                DB::rollback();
                $result = (object)array(
                    'isSuccess' => true,
                    'title' => 'Success',
                    'data' => $res,
                    'data_fail' => $res_fail,
                    'tokped' => $status_ym,
                    'message' => 'Get Data Import!',
                    'message' => 'Get Data Import!',
                    'icon' => 'success'
                );
                return response()->json($result);
            }

            // PUSH KE TOKOPEDIA
            $push_for_delete = array();
            $push_data_ecommerce = array();
            if (count($arr_ecommerce) > 0) {
                $push_data_ecommerce = $PushData->pushEcommerce($arr_ecommerce);
                if ($push_data_ecommerce->status == 200) {
                    if ($push_data_ecommerce->error_rows > 0) {
                        foreach ($push_data_ecommerce->error as $error) {
                            $data_fail = (object)array(
                                'prod_code' => $error->sku,
                                'prod_name' => $error->product_name,
                                'message' => $error->error[0],
                            );
                            array_push($res_fail, $data_fail);
                        }
                    }
                    if ($push_data_ecommerce->success_rows > 0) {
                        foreach ($push_data_ecommerce->success_rows_data as $success_data) {
                            array_push($push_for_delete, $success_data->product_id);
                        }
                    }
                    $tokped = 200;
                } else {
                    $tokped = 500;
                }
            }

            sleep(6);
            $push_data_ecommerce_update = array();
            $update_for_delete = array();
            if (count($arr_ecommerce_update) > 0) {
                $push_data_ecommerce_update = $PushData->updateEcommerce($arr_ecommerce_update);
                if ($push_data_ecommerce_update->status == 200) {
                    if ($push_data_ecommerce_update->error_rows > 0) {
                        foreach ($push_data_ecommerce_update->error as $error) {
                            $data_fail = (object)array(
                                'prod_code' => $error->sku,
                                'prod_name' => $error->product_name,
                                'message' => $error->error[0],
                            );
                            array_push($res_fail, $data_fail);
                        }
                    }
                    if ($push_data_ecommerce_update->success_rows > 0) {
                        foreach ($push_data_ecommerce_update->success_rows_data as $success_data_update) {
                            array_push($update_for_delete, $success_data_update->product_id);
                        }
                    }
                    $tokped = 200;
                } else {
                    $tokped = 500;
                }
            }

            ob_clean();
            $delete_tokopedia = array();
            if (count($res_fail) > 0 ) {
                // if (count($push_for_delete) > 0) {
                //     $delete_tokopedia = $PushData->pushDelete($push_for_delete);
                // }
                DB::rollback();
            } elseif ($tokped == 500) {
                DB::rollback();
            } else {
                DB::commit();
            }

            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'tokped' => $tokped,
                'push_ecom' => $push_data_ecommerce,
                'push_for_delete' => $push_for_delete,
                'update_for_delete' => $update_for_delete,
                'update_ecom' => $push_data_ecommerce_update,
                'delete_tokopedia' => $delete_tokopedia,
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            DB::rollback();
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);
    }
}
