<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\OrderStatus;
use App\User;
use App\PaymentMethod;
use App\Organization;
use App\Warehouse;
use App\Helpers\Transaction;
use App\Payment;
use Validator;
use App;
use DB;
use Lang;
use App\Product;
use App\Category;
use Config;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $transaction = "";
    public function __construct($transaction = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $transaction = new Transaction();
        $this->transaction = $transaction;
    }

    public function inventory()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],37)[0]['access_status'];
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $agent = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_name')->join('00_organization','00_organization.organization_id','=','98_user.organization_id')->whereIn('00_organization.organization_type_id',[1,2])->where('98_user.active_flag',1)->get();
        $organization =  Organization::whereIn('organization_type_id',[1,2])->get();
        // USER ORGANIZATION & WAREHOUSE
            $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
            $data['user_wh_org'] = $user_wh_org;
        // USER ORGANIZATION & WAREHOUSE
        if($user_wh_org){
            $list_product = Product::join('00_stock_card','00_product.prod_id','00_stock_card.prod_id')
                                    ->where('00_stock_card.warehouse_id','warehouse_id')
                                    ->get();
        }
        $all_warehouse = Warehouse::where('active_flag',1)->get();
        $list_category = Category::where('active_flag',1)->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['agent'] = $agent;
        $data['organization'] = $organization;
        $data['list_product'] = $list_product;
        $data['all_warehouse'] = $all_warehouse;
        $data['list_category'] = $list_category;
        return view('admin::report/inventory', $data);
    }

    public function payment()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],38)[0]['access_status'];
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['payment_method'] = $payment_method;
        return view('admin::report/payment', $data);
    }

    public function sales()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],39)[0]['access_status'];
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $agent = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_name')->join('00_organization','00_organization.organization_id','=','98_user.organization_id')->whereIn('00_organization.organization_type_id',[1,2])->where('98_user.active_flag',1)->get();
        $organization =  Organization::select('00_organization.organization_id','00_organization.organization_name',DB::raw('(SELECT og.organization_name FROM 00_organization AS og)'))->whereIn('organization_type_id',[1,2,4])->get();
        $warehouse = Warehouse::get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['warehouse'] = $warehouse;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['agent'] = $agent;
        $data['organization'] = $organization;
        return view('admin::report/sales', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function ecommerce(Request $request)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],42)[0]['access_status'];
        $payment_method = PaymentMethod::where('active_flag',1)->get();
        $all_menu = $user_menu->get($user['id']);
        $agent = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_name')->join('00_organization','00_organization.organization_id','=','98_user.organization_id')->whereIn('00_organization.organization_type_id',[1,2])->where('98_user.active_flag',1)->get();
        $organization =  Organization::select('00_organization.organization_id','00_organization.organization_name',DB::raw('(SELECT og.organization_name FROM 00_organization AS og)'))->whereIn('organization_type_id',[1,2,4])->get();
        $warehouse = Warehouse::get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['warehouse'] = $warehouse;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['agent'] = $agent;
        $data['organization'] = $organization;
        return view('admin::report/ecommerce', $data);
    }

    public function get_total_ecommerce_report(Request $request){
        $total_gross_income = Payment::join('10_order','10_order.payment_id','=','10_payment.payment_id')->where('10_order.agent_user_id',28);
        $sum_query = '10_payment.grandtotal_payment';
        if($request->input('start_date') != null){
            $total_gross_income->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $total_gross_income->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $total_gross_income = $total_gross_income->sum(DB::raw($sum_query));

        $total_cashback = Payment::join('00_voucher_ecommerce_header','10_payment.voucher_id','=','00_voucher_ecommerce_header.voucher_ecom_h_id')->join('00_voucher_ecommerce_detail','00_voucher_ecommerce_header.voucher_ecom_h_id','=','00_voucher_ecommerce_detail.voucher_ecom_h_id')->whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        })->where('00_voucher_ecommerce_detail.voucher_type_id',4);
        if($request->input('start_date') != null){
            $total_cashback->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $total_cashback->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $total_cashback = $total_cashback->sum('00_voucher_ecommerce_detail.amount_after_discount');

        $total_shipping_fee = Payment::join('00_voucher_ecommerce_header','10_payment.voucher_id','=','00_voucher_ecommerce_header.voucher_ecom_h_id')->join('00_voucher_ecommerce_detail','00_voucher_ecommerce_header.voucher_ecom_h_id','=','00_voucher_ecommerce_detail.voucher_ecom_h_id')->whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        })->where('00_voucher_ecommerce_detail.voucher_type_id',2);
        
        if($request->input('start_date') != null){
            $total_shipping_fee->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $total_shipping_fee->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        
        $total_shipping_fee = $total_shipping_fee->sum('00_voucher_ecommerce_detail.amount_after_discount');

        $total_insurance = Payment::join('00_voucher_ecommerce_header','10_payment.voucher_id','=','00_voucher_ecommerce_header.voucher_ecom_h_id')->join('00_voucher_ecommerce_detail','00_voucher_ecommerce_header.voucher_ecom_h_id','=','00_voucher_ecommerce_detail.voucher_ecom_h_id')->whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        })->where('00_voucher_ecommerce_detail.voucher_type_id',5);
        if($request->input('start_date') != null){
            $total_insurance->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $total_insurance->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $total_insurance = $total_insurance->sum('00_voucher_ecommerce_detail.amount_after_discount');

        $total_admin_fee = Payment::whereIn('10_payment.payment_id',function($query){
            $query->select('10_order.payment_id')->from('10_order')->where('10_order.agent_user_id',28);
        });
        if($request->input('start_date') != null){
            $total_admin_fee->where(DB::raw('CAST(payment_date AS DATE)'),'>=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('start_date'))).'" AS DATE)'));
        }
        if($request->input('end_date') != null){
            $total_admin_fee->where(DB::raw('CAST(payment_date AS DATE)'),'<=',DB::raw('CAST("'.date('Y-m-d',strtotime($request->input('end_date'))).'" AS DATE)'));
        }
        $total_admin_fee = $total_admin_fee->sum('10_payment.admin_fee');

        $total_bruto = $total_gross_income - $total_cashback - $total_shipping_fee - $total_insurance - $total_admin_fee;

        return response()->json(array('total_gross_income' => $total_gross_income, 'total_top_ads_cost' => 0, 'total_cashback' => $total_cashback, 'total_shipping_fee' => $total_shipping_fee, 'total_insurance' => $total_insurance, 'total_bruto' => $total_bruto));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $payment_list = $request->input('payment_list');
        $payment_list = explode(",",$payment_list);

        Payment::whereIn('payment_id', $payment_list)->update(['reckon_flag' => 1]);

        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
