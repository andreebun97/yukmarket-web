<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Session;
use Config;
use App;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(){
        App::setLocale('in');
    }

    public function index()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],44)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::package/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function form(Request $request)
    {
        return view('admin::package/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
