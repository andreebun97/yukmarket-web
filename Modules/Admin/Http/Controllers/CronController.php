<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Session;
use App\Inventory;
use App\StockCard;
use App\Product;
use App\Category;
use App\Warehouse;
use App\ProductWarehouse;
use App\TransactionHeader;
use App\TransactionDetail;
use App\InputOpname;
use App\User;
use App\Helpers\UomConvert;
use App\UOM;
use App;
use Lang;
use DB;

class CronController extends Controller
{
    public function __construct(){

    }

    public function generateStockCard(){
        // NO TRANSAKSI
            $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
            $no_trx_first = substr($no_trx_shuffle,0,3);
            $no_trx_last = substr($no_trx_shuffle,-3);
            $stock_card_code = "SM".$no_trx_first."".$no_trx_last;
        // NO TRANSAKSI
        $query = StockCard::selectRaw('prod_id,
                                        warehouse_id,
                                        organization_id,
                                        warehouse_id,SUM(stock) as new_stock,
                                        ROUND(AVG(total_harga), 0) as new_price,
                                        SUM(total_harga) as new_total')
                            ->whereRaw('month(created_date) and MONTH(NOW())-1 and active_flag = 1')
                            ->groupBy('prod_id','warehouse_id','organization_id')
                            ->get();
        $data = array();
        DB::beginTransaction();
            foreach($query as $row):
                $check = StockCard::where('prod_id',$row->prod_id)
                                    ->where('organization_id',$row->organization_id)
                                    ->where('warehouse_id',$row->warehouse_id)
                                    ->where('transaction_type_id',1)
                                    ->whereRaw("month(created_date) = month(now()) and year(created_date) = year(now()) and active_flag = 1")
                                    ->get();
                if(!$check){
                    $stock_card = [
                        'stock_card_code' => $stock_card_code,
                        'organization_id' => $row->organization_id,
                        'warehouse_id' => $row->warehouse_id,
                        'prod_id' => $row->prod_id,
                        'stock' => $row->new_stock,
                        'transaction_type_id' => 1,
                        'price' => $row->new_price,
                        'total_harga' => $row->new_total,
                        'active_flag' => 1,
                        'created_by' => 'System',
                        'created_date' => date(now())
                    ];
                    StockCard::insert($stock_card);
                    array_push($data,$stock_card);
                }else{
                    array_push($data,"skip $row->prod_id, $row->organization_id, $row->warehouse_id");
                }

            endforeach;
        DB::commit();

        return $data;
    }
}
