<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Session;
use App\Helpers\Currency;
use App\Helpers\PhoneNumber;
use App;
use Lang;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Shipment;
use App\Order;
use App\OrderHistory;
use App\OrderDetail;
use App\AddressDetail;
use App\Preparation;
use App\OrderStatus;
use App\GlobalSettings;
use App\ShipmentEcommerce;
use Validator;
use DB;
use Mail;
use App\Product;
use App\Inventory;
use App\StockCard;
use Config;
use App\Notification;
use App\Http\Controllers\IntegrationTokopediaController;
use GuzzleHttp\Client;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use App\Helpers\UomConvert;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $phone_number;
    protected $currency;

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $phone_number = new PhoneNumber();
        $this->currency = $currency;
        $this->phone_number = $phone_number;
    }

    public function index()
    {
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],13)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::shipment/index', $data);
    }

    public function history(Request $request){
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],23)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $shipment_method = Shipment::select('shipment_method_id','shipment_method_name',DB::raw('NULL as organization_id'),DB::raw('NULL as organization_name'))->where('active_flag',1);
        $shipment_method_ecommerce = ShipmentEcommerce::select('shipment_method_ecom_id AS shipment_method_id','shipment_method_name','00_organization.organization_id','organization_name')->join('00_organization','00_organization.organization_id','=','00_shipment_method_ecommerce.organization_id')->where('00_shipment_method_ecommerce.active_flag',1);
        $all_shipment_method = $shipment_method->union($shipment_method_ecommerce)->get();
        $main_order_status = OrderStatus::where('order_status_id','>=',7)->get();
        $order_status = OrderStatus::whereIn('order_status_id',[7,8,9,10])->get();

        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['shipment_method'] = $all_shipment_method;
        $data['currency'] = $this->currency;
        $data['order_status'] = $order_status;
        $data['main_order_status'] = $main_order_status;
        return view('admin::shipment/history', $data);
    }

    public function history_list(Request $request){
        if($request->get('id') == null){
            return redirect('admin/shipment/history');
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],23)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $shipment_method = Shipment::where('active_flag',1)->get();
        $order = Order::select('10_order.order_id','10_order.order_code','00_customer.customer_id','00_customer.customer_name','10_order.shipment_price','00_address.address_detail','00_address.address_info','00_address.address_name','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'))->distinct()->join('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->join('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->where('order_status_id',7)->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->orderBy('10_preparation.preparation_status_id','DESC');
        if($request->get('ecommerce') != null){
            $order->where('10_order.agent_user_id', $request->get('ecommerce'));
        }else{
            if($user['organization_type_id'] > 1){
                $order->where('10_order.agent_user_id',$user['organization_id']);
            }
        }
        $order->where('10_order.shipment_method_id', $request->get('id'));
        $order = $order->get();
        $shipped_order = array();
        for ($b=0; $b < count($order); $b++) { 
            $obj = array(
                "order_id" => $order[$b]['order_id'],
                "order_code" => $order[$b]['order_code'],
                "customer_id" => $order[$b]['buyer_user_id'],
                "customer_name" => $order[$b]['customer_name'],
                "shipment_price" => "Rp. ".$this->currency->convertToCurrency($order[$b]['shipment_price']),
                "destination_address" => $order[$b]['destination_address'],
                "address_detail" => $order[$b]['address_detail'] . ', ' . $order[$b]['provinsi_name'] . ', ' . $order[$b]['kabupaten_kota_name'] . ', ' . $order[$b]['kecamatan_name'] . ', ' . $order[$b]['kelurahan_desa_name'] . ', ' . $order[$b]['kode_pos'],
                "shipment_method_name" => $order[$b]['shipment_method_name']
            );
            array_push($shipped_order, $obj);
        }

        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['shipment_method'] = $shipment_method;
        $data['currency'] = $this->currency;
        $data['shipment_list'] = $shipped_order;
        return view('admin::shipment/history_list', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/shipment');
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],13)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $activity = "";
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::shipment/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shipment_method_name' => ['required'],
            //'shipment_price' => ['required'],
            'shipment_logo' => ['required','mimes:png,pneg,svg,jpg,jpeg'],
            'shipment_description' => ['required'],
            //'product_weight' => ['required'],
            'dim_length' => ['required'],
            'dim_width' => ['required'],
            'dim_height' => ['required']
        ]);

        if($request->input('shipment_id') != null){
            $validator = Validator::make($request->all(), [
                'shipment_method_name' => ['required'],
                //'shipment_price' => ['required'],
                'shipment_description' => ['required'],
                //'product_weight' => ['required'],
                'dim_length' => ['required'],
                'dim_width' => ['required'],
                'dim_height' => ['required']
            ]);
            if($request->file('shipment_logo') != null){
                $validator = Validator::make($request->all(), [
                    'shipment_method_name' => ['required'],
                    //'shipment_price' => ['required'],
                    'shipment_logo' => ['required','mimes:png,pneg,svg,jpg,jpeg'],
                    'shipment_description' => ['required'],
                    //'product_weight' => ['required'],
                    'dim_length' => ['required'],
                    'dim_width' => ['required'],
                    'dim_height' => ['required']
                ]);
            }
        }

        if($validator->fails()){
            $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
            if($request->input('shipment_id') != null){
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
            }
            Session::flash('message_alert', $alert);
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            if($request->input('rates_apply') == 1){
                $price = (int)str_replace('.','',$request->input('distance_price')) + (int)str_replace('.','',$request->input('weight_price'));
                $price_km = str_replace('.','',$request->input('distance_unit'));
                $price_kg = str_replace('.','',$request->input('weight_unit'));
                $max_km = str_replace('.','',$request->input('distance_maximum'));
                $max_kg = str_replace('.','',$request->input('weight_maximum'));
                $max_price_km = str_replace('.','',$request->input('distance_price'));
                $max_price_kg = str_replace('.','',$request->input('weight_price'));
            }else{
                $price = str_replace('.','',$request->input('shipment_price'));
                $price_km = null;
                $price_kg = null;
                $max_km = null;
                $max_kg = null;
                $max_price_km = null;
                $max_price_kg = null;
            }
            $folder_name = 'img/uploads/shipment/';
            $file = $request->file('shipment_logo');
            $ext = $file == null ? '' : $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;
            $alert = "";
            if($request->input('shipment_id') == null){
                if($file !== null){
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(300,300)->save(public_path($folder_name.$name),80);
                    chmod($folder_name.$name, 0777);
                    $shipment_id = Shipment::insertGetId([
                        'shipment_method_name' => $request->input('shipment_method_name'),
                        'price' => $price,
                        // 'min_price' => $request->input('shipment_min_price') == "" ? null : str_replace('.','',$request->input('shipment_min_price')),
                        // 'max_distance' => $request->input('max_distance'),
                        // 'min_distance' => $request->input('min_distance'),
                        'is_fixed' => $request->input('rates_apply'),
                        'shipment_logo' => $folder_name.$name,
                        'shipment_method_desc' => $request->input('shipment_description'),
                        'active_flag' => 1,
                        'weight' => $request->input('product_weight'),
                        'dim_length' => $request->input('dim_length'),
                        'dim_width' => $request->input('dim_width'),
                        'dim_height' => $request->input('dim_height'),
                        'price_km' => $price_km,
                        'price_kg' => $price_kg,
                        'max_km' => $max_km,
                        'max_kg' => $max_kg,
                        'max_price_km' => $max_price_km,
                        'max_price_kg' => $max_price_kg,
                        'request_pickup_flag' => ($request->input('request_pickup_flag') == null ? 0 : 1)
                    ]);
                    
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    if($shipment_id > 0){
                        $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    }
                    Session::flash('message_alert', $alert);
                    return redirect('admin/shipment');
                }
            }else{
                $updated_obj = array(
                    'shipment_method_name' => $request->input('shipment_method_name'),
                    'price' => $price,
                    // 'min_price' => $request->input('shipment_min_price') == "" ? null : str_replace('.','',$request->input('shipment_min_price')),
                    // 'max_distance' => $request->input('max_distance'),
                    // 'min_distance' => $request->input('min_distance'),
                    'is_fixed' => $request->input('rates_apply'),
                    'shipment_method_desc' => $request->input('shipment_description'),
                    'weight' => $request->input('product_weight'),
                    'dim_length' => $request->input('dim_length'),
                    'dim_width' => $request->input('dim_width'),
                    'dim_height' => $request->input('dim_height'),
                    'price_km' => $price_km,
                    'price_kg' => $price_kg,
                    'max_km' => $max_km,
                    'max_kg' => $max_kg,
                    'max_price_km' => $max_price_km,
                    'max_price_kg' => $max_price_kg,
                    'request_pickup_flag' => ($request->input('request_pickup_flag') == null ? 0 : 1)
                );
                if($file !== null){
                    $updated_obj = array(
                        'shipment_method_name' => $request->input('shipment_method_name'),
                        'price' => $price,
                        'shipment_logo' => $folder_name.$name,
                        // 'min_price' => $request->input('shipment_min_price') == "" ? null : str_replace('.','',$request->input('shipment_min_price')),
                        // 'max_distance' => $request->input('max_distance'),
                        // 'min_distance' => $request->input('min_distance'),
                        'is_fixed' => $request->input('rates_apply'),
                        'shipment_method_desc' => $request->input('shipment_description'),
                        'weight' => $request->input('product_weight'),
                        'dim_length' => $request->input('dim_length'),
                        'dim_width' => $request->input('dim_width'),
                        'dim_height' => $request->input('dim_height'),
                        'price_km' => $price_km,
                        'price_kg' => $price_kg,
                        'max_km' => $max_km,
                        'max_kg' => $max_kg,
                        'max_price_km' => $max_price_km,
                        'max_price_kg' => $max_price_kg,
                        'request_pickup_flag' => ($request->input('request_pickup_flag') == null ? 0 : 1)
                    );
                    if (!file_exists($folder_name)) {
                        mkdir($folder_name, 777, true);
                    }
                    ini_set('memory_limit', '256M');
                    Image::make($file)->fit(300,300)->save(public_path($folder_name.$name),80);
                    chmod($folder_name.$name, 0777);
                    File::delete($request->input('shipment_logo_temp'));
                }
                $update = $shipment_id = Shipment::where('shipment_method_id', $request->input('shipment_id'))->update($updated_obj);
                    
                $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($update){
                    $alert = Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }

            Session::flash('message_alert', $alert);
            return redirect('admin/shipment');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */

    public function email($id = null){
        // $id = 96;
        $transaction = Order::select('10_order.order_id','00_payment_method.payment_method_name','10_order.order_date','10_order.order_code','10_order.destination_address','00_address.address_name','00_address.address_detail','00_address.contact_person','00_address.phone_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'))->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('order_id', $id)->first();
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $logo = Config::get('logo.email');
        $whatsapp_number = GlobalSettings::where('global_parameter_name','whatsapp_customer_service')->first();
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        $data['whatsapp_number'] = $whatsapp_number;
        Mail::send('shipment_confirmation_email',$data, function ($message) use ($transaction)
            {
                $message->subject('Pengiriman Barang ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        // return view("shipment_confirmation_email", $data);
    }

    public function arrival_email($id){
        // $id = 96;
        $transaction = Order::select('10_order.order_id','00_payment_method.payment_method_name','10_order.order_date','10_order.order_code','10_order.destination_address','00_address.address_name','00_address.address_detail','00_address.contact_person','00_address.phone_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'))->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('order_id', $id)->first();
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        Mail::send('arrival_confirmation_email',$data, function ($message) use ($transaction)
            {
                $message->subject('Konfirmasi Pesan Sampai ('.$transaction->order_code.')');
                $message->to($transaction->customer_email);
                $message->from('support@yukmarket.com', 'YukMarket');
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        // return view("arrival_confirmation_email", $data);
    }

    public function arrival_confirmation_email($id){
        // $id = 96;
        $transaction = Order::select('10_order.order_id','00_payment_method.payment_method_name','10_order.order_date','10_order.order_code','10_order.destination_address','00_address.address_name','00_address.address_detail','00_address.contact_person','00_address.phone_number','00_provinsi.provinsi_name','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_payment.customer_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_payment.customer_id) END) AS customer_email'))->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_payment.payment_method_id')->where('order_id', $id)->first();
        $transaction['status'] = "Sudah Bayar";
        $products = OrderDetail::join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id',$id)->get();
        $logo = Config::get('logo.email');
        $data['logo'] = $logo;
        $data['currency'] = $this->currency;
        $data['transaction'] = $transaction;
        $data['products'] = $products;
        Mail::send('customer_arrival_confirmation_email',$data, function ($message) use ($transaction)
            {
                $message->subject('Konfirmasi Barang Sampai (Dari Pelanggan Yang Bersangkutan): '.$transaction->order_code.'');
                $message->from('support@yukmarket.com', 'YukMarket');
                $message->to($transaction->customer_email);
                // $message->cc(['suci.putri@indocyber.co.id','vivi.maudiwati@indocyber.co.id','widiyanto.ramadhan@indocyber.co.id']);
            }
        );
        // return view("customer_arrival_confirmation_email", $data);
    }

    public function sendOrder(Request $request)
    {
        $obj = array(
            'shipping_receipt_num' => ['required']
        );

        if($request->input('having_inputted_shipping_receipt_num') != null){
            $obj = array(
                'shipping_receipt_num' => ['required', 'unique:10_order']
            );
        }
        $validator = Validator::make($request->all(), $obj);

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            DB::beginTransaction();
            $update = Order::where('order_id', $request->input('order_id'))->update(['shipping_receipt_num' => $request->input('shipping_receipt_num'),'order_status_id' => 8]);
        
            $preparation = Preparation::insertGetId([
                'preparation_status_id' => 5,
                'order_id' => $request->input('order_id'),
                'assigned_to' => Session::get('users')['id'],
                'updated_by' => Session::get('users')['id']
            ]);
        
            OrderHistory::insert([
                'order_id' => $request->input('order_id'),
                'order_status_id' => 8,
                'active_flag' => 1,
                'updated_by' => Session::get('users')['id']
            ]);
            
            $order = Order::select('10_order.order_id','10_order.buyer_user_id','10_order.order_code','10_order.is_agent',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'))->where('order_id',$request->input('order_id'))->first();
            // echo $order;
            // exit;
            $order_detail = OrderDetail::where('order_id',$request->input('order_id'))->get();
            foreach($order_detail as $row):
                $product = Product::where('prod_id',$row->prod_id)->first();
                $product->stockable_flag == '1' ? $stock_id = $row->prod_id : $stock_id = $product->parent_prod_id;
                
                $inventory = Inventory::where('prod_id',$stock_id)
                                ->where('warehouse_id',$row->warehouse_id)
                                ->first();
                // print_r($inventory);
                // exit;
                if($product->stockable_flag == '1'){
                    $stock_update = $inventory->stock - $row->quantity;
                    $booking_update = $inventory->booking_qty - $row->quantity;
                    $goods_in_transit_update = $inventory->goods_in_transit + $row->quantity;
                    $qty_buy = $row->quantity;
                }else{
                    $convert = new UomConvert;
                    $product = Product::select('uom_id','uom_value')->where('prod_id','=',$stock_id)->first();
                    $finalStock = $convert->convertToStock($row->uom_id, $row->uom_value, $product->uom_id, $product->uom_value);
                    $qty_buy = $row->quantity*$finalStock;
                    $stock_update = $inventory->stock - $qty_buy;
                    $booking_update = $inventory->booking_qty - $qty_buy;
                    $goods_in_transit_update = $inventory->goods_in_transit + $qty_buy;
                }

                Inventory::where('prod_id',$stock_id)
                        ->where('warehouse_id',$row->warehouse_id)
                        ->update([
                            // 'stock' => $stock_update,
                            'booking_qty' => $booking_update,
                            'goods_in_transit' => $goods_in_transit_update
                        ]);
                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                // $this->ValidateUpdateStockTokped($stock_id,$stock_update);                
                // $stock_for_card = 0 - $qty_buy;
                // StockCard::insertGetId([
                //     'stock_card_code' => $order->order_code,
                //     'organization_id' => $inventory->organization_id,
                //     'warehouse_id' => $row->warehouse_id,
                //     'prod_id' => $row->prod_id,
                //     'stock' => $stock_for_card,
                //     'transaction_type_id' => 3,
                //     'price' => $inventory->inventory_price,
                //     'total_harga' => $stock_for_card * $inventory->inventory_price,
                //     //'transaction_desc' => '',
                //     'created_by' => Session::get('users')['id']
                // ]);
            endforeach;
            DB::commit();

            $url = Config::get('fcm.url');
            $server_key = Config::get('fcm.token');
            $headers = array(
                'Content-Type'=>'application/json',
                'Authorization'=>'key='.$server_key
            );
            $title = "Pesanan Anda sedang dikirim (".$order->order_code.")";
            $message = "Terima kasih telah melakukan transaksi di YukMarket! Pesanan anda dengan kode pesanan (".$order->order_code.") sedang dikirim. Mohon menunggu!";
            $type = "transaction";
            if($order->fcm_token != null){
                $parameter = array(
                    "to" => $order->fcm_token,
                    "notification" => array(
                        "title" => $title,
                        "body" => $message,
                        "message" => "message",
                        "sound" => "default",
                        "badge" => "1",
                        "image" => "null",
                        "icon" => "@mipmap/ic_stat_ic_notification"
                    ),
                    "data" => array(
                        "target" => "MainActivity",
                        "notifId" => "1",
                        "dataId" => "1"
                    )
                );
                $response = Http::withHeaders($headers)->post($url, $parameter);
            }
            Notification::insert([
                "customer_id" => $order->is_agent == 0 ? $order->buyer_user_id : null,
                "user_id" => $order->is_agent == 1 ? $order->buyer_user_id : null,
                "order_id" => $request->input('order_id'),
                "message" => $message,
                "title" => $title,
                "type" => $type
            ]);
            $this->email($request->input('order_id'));
            return redirect('admin/shipment/history');
        }
    }

    public function confirmOrder(Request $request)
    {
        // print_r($request->all());
        // exit;
        $purchased_products = $request->input('purchased_products');
        $purchased_quantity = $request->input('purchased_quantity');
        // exit;
        $update = Order::where('order_id', $request->input('order_id'))->update(['order_status_id' => 9]);

        OrderHistory::insert([
            'order_id' => $request->input('order_id'),
            'order_status_id' => 9,
            'active_flag' => 1,
            'updated_by' => Session::get('users')['id']
        ]);
        // for ($k=0; $k < count($purchased_products); $k++) { 
        //     $inventory = Inventory::where('organization_id', 1)->where('prod_id', $purchased_products[$k])->first();

        //     if($inventory != null){
        //         $goods_arrived = $inventory['goods_arrived'] == null ? 0 : $inventory['goods_arrived'];
        //         Inventory::where('inventory_id',$inventory['inventory_id'])->update(array(
        //             'goods_in_transit' => ($inventory['goods_in_transit'] - $purchased_quantity[$k]),
        //             'goods_arrived' => ($goods_arrived + $purchased_quantity[$k])
        //         ));
        //     }
        // }
        $order_detail = OrderDetail::where('order_id',$request->input('order_id'))->get();
        foreach($order_detail as $row):
            $product = Product::where('prod_id',$row->prod_id)->first();
            $product->stockable_flag == '1' ? $stock_id = $row->prod_id : $stock_id =$product->parent_prod_id;
                
            $inventory = Inventory::where('prod_id',$stock_id)
                            ->where('warehouse_id',$row->warehouse_id)
                            ->first();
                // print_r($inventory);
                // exit;
            if($product->stockable_flag == '1'){
                $stock_update = $inventory->stock - $row->quantity;
                $goods_in_transit_update = $inventory->goods_in_transit - $row->quantity;
                $goods_arrived_update = $inventory->goods_arrived + $row->quantity;
                $qty_buy = $row->quantity;
            }else{
                $convert = new UomConvert;
                $product = Product::select('uom_id','uom_value')->where('prod_id','=',$stock_id)->first();
                $finalStock = $convert->convertToStock($row->uom_id, $row->uom_value,$product->uom_id, $product->uom_value);
                $qty_buy = $row->quantity*$finalStock;
                $stock_update = $inventory->stock - $qty_buy;
                // $booking_update = $inventory->booking_qty - $qty_buy;
                $goods_in_transit_update = $inventory->goods_in_transit - $qty_buy;
                $goods_arrived_update = $inventory->goods_arrived + $qty_buy;
            }

            Inventory::where('prod_id',$stock_id)
                    ->where('warehouse_id',$row->warehouse_id)
                    ->update([
                        // 'stock' => $stock_update,
                        'goods_in_transit' => $goods_in_transit_update,
                        'goods_arrived' => $goods_arrived_update
                    ]);
                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                // $this->ValidateUpdateStockTokped($stock_id,$stock_update);                
                // $stock_for_card = 0 - $qty_buy;
                // StockCard::insertGetId([
                //     'stock_card_code' => $order->order_code,
                //     'organization_id' => $inventory->organization_id,
                //     'warehouse_id' => $row->warehouse_id,
                //     'prod_id' => $row->prod_id,
                //     'stock' => $stock_for_card,
                //     'transaction_type_id' => 3,
                //     'price' => $inventory->inventory_price,
                //     'total_harga' => $stock_for_card * $inventory->inventory_price,
                //     //'transaction_desc' => '',
                //     'created_by' => Session::get('users')['id']
                // ]);
        endforeach;
        $alert = Lang::get('notification.has arrived', ['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            $this->arrival_email($request->input('order_id'));
            $alert = Lang::get('notification.has been sent', ['result' => strtolower(Lang::get('notification.successfully'))]);
            $order = Order::select('10_order.order_id','10_order.order_code','10_order.is_agent',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.fcm_token FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.fcm_token FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS fcm_token'))->where('order_id',$request->input('order_id'))->first();
    
            $url = Config::get('fcm.url');
            $server_key = Config::get('fcm.token');
            $headers = array(
                'Content-Type'=>'application/json',
                'Authorization'=>'key='.$server_key
            );
            $title = "Pesanan Anda telah sampai (".$order->order_code.")";
            $message = "Terima kasih telah melakukan transaksi di YukMarket! Pesanan anda dengan kode pesanan (".$order->order_code.") telah sampai. Mohon konfirmasi melalui aplikasi YukMarket!";
            $type = "transaction";
            if($order->fcm_token != null){
                $parameter = array(
                    "to" => $order->fcm_token,
                    "notification" => array(
                        "title" => $title,
                        "body" => $message,
                        "message" => "message",
                        "sound" => "default",
                        "badge" => "1",
                        "image" => "null",
                        "icon" => "@mipmap/ic_stat_ic_notification"
                    ),
                    "data" => array(
                        "target" => "MainActivity",
                        "notifId" => "1",
                        "dataId" => "1"
                    )
                );
                $response = Http::withHeaders($headers)->post($url, $parameter);
            }
            Notification::insert([
                "customer_id" => $order->is_agent == 0 ? $order->buyer_user_id : null,
                "user_id" => $order->is_agent == 1 ? $order->buyer_user_id : null,
                "order_id" => $request->input('order_id'),
                "message" => $message,
                "title" => $title,
                "type" => $type
            ]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/arrival');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/shipment');
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],13)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $shipment_by_id = Shipment::where('shipment_method_id', $request->input('id'))->first();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['shipment_by_id'] = $shipment_by_id;
        return view('admin::shipment/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */

    public function history_form(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/transaction');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],19)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $transactions = OrderDetail::select('10_order_detail.dim_length','00_product.prod_image','10_order_detail.dim_width','10_order_detail.dim_height','10_order_detail.price','10_order_detail.quantity','10_order_detail.order_id','00_product.prod_code','00_product.prod_name','10_order_detail.uom_value','10_order_detail.bruto','00_product.uom_id','00_uom.uom_name','10_order_detail.sku_status','00_product.variant_id')->join('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->where('order_id', $request->get('id'))->get();
        // print_r($transactions);
        // exit;
        // $midtrans_transaction = $this->getMidtransTransaction($request->get('id'));
        $order_master = Order::select('10_order.agent_user_id','10_order.order_id','10_order.order_code','10_order.order_date','10_order.admin_fee','10_order.admin_fee_percentage','10_order.transfer_date','10_order.pricing_include_tax','10_order.national_income_tax',DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'10_order.shipment_price','10_order.shipping_receipt_num','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.is_fixed','10_order.voucher_amount','10_order.destination_address','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','10_order.midtrans_transaction_id','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','10_order.max_price','10_order.weight AS total_weight','10_order.voucher_id','10_order.min_price','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.order_status_id',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.request_pickup_flag FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.request_pickup_flag FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS request_pickup_flag'),DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'00_order_status.order_status_name','10_order.payment_method_id','00_payment_method.payment_method_name')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->where('10_order.order_id', $request->get('id'))->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->first();
        // $address_detail = AddressDetail::join('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->join('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_customer','00_customer.customer_id','=','00_address.customer_id')->join('10_order','10_order.buyer_user_id','=','00_customer.customer_id')->where('order_id', $request->get('id'))->get();
        $preparation = Preparation::where('order_id', $request->get('id'))->orderBy('preparation_status_id','DESC')->first();
        // $verification_steps = array("Pesanan Diterima","Pembayaran Diterima","Verifikasi","Picking","Quality Control","Packing","Delivery","Pesanan Selesai");
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['transactions'] = $transactions;
        $data['order_master'] = $order_master;
        $data['currency'] = $this->currency;
        $data['preparation'] = $preparation;
        $data['phone_number'] = $this->phone_number;
        // $data['address_detail'] = $address_detail;
        return view('admin::shipment/history_form', $data);
    }

    public function activate(Request $request)
    {
        $update = Shipment::where('shipment_method_id', $request->get('id'))->update(['active_flag' => 1]);
        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been activated',['result' => (strtolowerLang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }

    public function label(Request $request)
    {
        $purchased_products = array();
        if($request->get('id') != null){
            $purchased_products = Order::select('10_order.order_id','10_order_detail.sku_status','10_order_detail.uom_value','10_order.shipping_receipt_num','10_order.shipping_receipt_label','10_order_detail.bruto','00_uom.uom_name','10_order.order_code','10_order.order_date','10_order.shipment_price','10_order.voucher_id','10_order.buyer_user_id','10_order.payment_method_id','10_order.pricing_include_tax','10_order.national_income_tax','10_order.shipment_method_id','00_organization.organization_type_id','00_organization.organization_logo','10_payment.invoice_no',DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_logo FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_logo FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_logo'),DB::raw('(CASE WHEN 00_organization.organization_type_id = 4 THEN (SELECT 00_shipment_method_ecommerce.shipment_method_name FROM 00_shipment_method_ecommerce WHERE 00_shipment_method_ecommerce.shipment_method_ecom_id = 10_order.shipment_method_id) ELSE (SELECT 00_shipment_method.shipment_method_name FROM 00_shipment_method WHERE 00_shipment_method.shipment_method_id = 10_order.shipment_method_id) END) AS shipment_method_name'),'10_order.destination_address','00_address.address_name','00_address.address_info','00_address.contact_person','00_address.phone_number','10_order.admin_fee','10_order.admin_fee_percentage','10_order.account_number','10_order.transfer_date','10_order.order_status_id','10_order.voucher_amount','10_order.is_fixed AS voucher_fix_status','10_order.max_price AS max_provided_price','10_order.min_price AS min_price_requirement','10_order.midtrans_transaction_id','10_order.invoice_status_id','10_order_detail.price AS purchased_price','10_order_detail.quantity AS purchased_quantity','00_product.prod_name AS purchased_product_name','10_order_detail.promo_value','00_vouchers.voucher_name','00_vouchers.voucher_code','10_order.buyer_name','10_order.buyer_address',DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_name FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_name FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_name'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_email FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_email FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_email'),DB::raw('(CASE WHEN 10_order.is_agent = 0 THEN (SELECT 00_customer.customer_phone_number FROM 00_customer WHERE 00_customer.customer_id = 10_order.buyer_user_id) ELSE (SELECT 98_user.user_phone_number FROM 98_user WHERE 98_user.user_id = 10_order.buyer_user_id) END) AS customer_phone_number'),'00_address.contact_person','00_address.phone_number','00_address.address_detail','00_kabupaten_kota.kabupaten_kota_name','00_kecamatan.kecamatan_name','00_kelurahan_desa.kelurahan_desa_name','00_kelurahan_desa.kode_pos','00_order_status.order_status_name','10_order.invoice_status_id','00_invoice_status.invoice_status_name','10_order.transfer_date','10_order.payment_method_id','00_payment_method.payment_method_name',DB::raw('(SELECT 98_user.user_name FROM 98_user JOIN 10_preparation ON 10_preparation.assigned_to = 98_user.user_id WHERE 10_preparation.order_id = 10_order.order_id AND preparation_status_id = 1 ORDER BY 10_preparation.preparation_id DESC LIMIT 1) AS picker_name'),'00_product_sku.product_sku_id','00_product_sku.product_sku_name')->leftJoin('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_product','00_product.prod_id','=','10_order_detail.prod_id')->leftJoin('00_vouchers','00_vouchers.voucher_id','=','10_order.voucher_id')->leftJoin('00_customer','00_customer.customer_id','=','10_order.buyer_user_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->leftJoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')->leftJoin('00_uom','00_uom.uom_id','=','10_order_detail.uom_id')->leftJoin('00_product_sku','00_product_sku.product_sku_id','=','00_product.product_sku_id')->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')->where('10_order.order_id',$request->get('id'))->get()->toArray();
        }
        $user = Session::get('users');
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $accessed_menu = $user_menu->get($user['id'],20)[0]['access_status'];
        // print_r($all_menu);
        // echo "<br>";
        // echo $accessed_menu;
        // exit;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $total_bruto = array_map(function($value){
            return $value['bruto'];
        }, $purchased_products);
        $total_bruto = array_sum($total_bruto);
        // echo $total_bruto;
        // print_r($purchased_products);
        // exit;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['user'] = $user;
        $data['purchased_products'] = $purchased_products;
        $data['currency'] = $this->currency;
        $data['phone_number'] = $this->phone_number;
        $data['total_bruto'] = $total_bruto;
        $data['reprint'] = 1;
        return view('admin::payment/label', $data);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = Shipment::where('shipment_method_id', $request->get('id'))->update(['active_flag' => 0]);
        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        if(!$update){
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect()->back();
    }
    //----------------------------INTEGRATION TOKPED
    public function validateUpdateStockTokped($prodId,$stock){
    //---------------check global config
        $config=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokopedia')
        ->select('global_parameter_desc')
        ->first();
        if($config->global_parameter_desc=='on'){
            $product =$this->checkProduct($prodId);
            if($product!=null){
               $url = config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$product->prod_ecom_code.'&productStocks='.round($stock).'&shopId='.$product->organization_code;
               $token=$this->generateToken('user_token_integrator');            
               try {
                   Http::withToken($token)->timeout(3)->retry(2, 100)->post($url);
               } catch (ConnectionException $e) {
                   return null;
               }                          
            }
        }
    }
    public function checkProduct($id){
        $data = DB::table('00_product_ecommerce AS p')
        ->select('p.prod_ecom_code','o.organization_code')
        ->join('00_organization AS o','o.organization_id','=','p.organization_id')
        ->where('p.prod_id',$id)
        ->first();
        return $data;        
    }
    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }
    public function generateToken($globalParam){
        $credential=$this->getGlobalParameter($globalParam);
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();                            
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);            
        } catch (ConnectionException $e) {
            $token=null;            
        }                
        return $token;
    }
    //----------------------------INTEGRATION TOKPED
}
