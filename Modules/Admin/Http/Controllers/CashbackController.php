<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use App;
use Lang;
use App\Cashback;
use App\Helpers\Currency;
use App\Customer;
use App\User;
use Session;
use Config;

class CashbackController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],35)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cashback/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function detail(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/cashback');
        }
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],35)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $customer = Customer::select('customer_id','customer_name','customer_phone_number','active_flag','customer_email')->where('customer_id', $request->get('id'))->first();
        $cashback = Cashback::select('10_cashback.cashback_id','10_cashback.issue_id','10_cashback.customer_id','00_customer.customer_name','00_customer.customer_phone_number','10_cashback.return_amount','10_cashback.transaction_flag','00_issue.ticketing_num','10_order.order_id','10_order.order_code','10_payment.payment_id','10_payment.invoice_no')->leftJoin('00_issue','00_issue.issue_id','=','10_cashback.issue_id')->leftJoin('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('10_payment','10_payment.payment_id','=','10_cashback.payment_id')->join('00_customer','00_customer.customer_id','=','10_cashback.customer_id')->where('10_cashback.customer_id', $request->get('id'))->where('10_cashback.active_flag',1)->whereIn('10_cashback.transaction_flag',[0,1])->get();
        if($request->get('role') == 'agent'){
            $customer = User::select('user_id AS customer_id','user_name AS customer_name','user_phone_number AS customer_phone_number','active_flag','user_email AS customer_email')->where('user_id', $request->get('id'))->first();
            $cashback = Cashback::select('10_cashback.cashback_id','10_cashback.issue_id','10_cashback.customer_id','98_user.user_name AS customer_name','98_user.user_phone_number AS customer_phone_number','10_cashback.return_amount','10_cashback.transaction_flag','00_issue.ticketing_num','10_order.order_id','10_order.order_code','10_payment.payment_id','10_payment.invoice_no')->leftJoin('00_issue','00_issue.issue_id','=','10_cashback.issue_id')->leftJoin('10_order','10_order.order_id','=','00_issue.order_id')->leftJoin('10_payment','10_payment.payment_id','=','10_cashback.payment_id')->join('98_user','98_user.user_id','=','10_cashback.agent_id')->where('10_cashback.agent_id', $request->get('id'))->where('10_cashback.active_flag',1)->whereIn('10_cashback.transaction_flag',[0,1])->get();
        }
        $currency = new Currency();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['currency'] = $currency;
        $data['customer'] = $customer;
        $data['cashback'] = $cashback;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::cashback/detail', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
