<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\AddressDetail;
use App\Warehouse;
use App\WarehouseType;
use App;
use Config;
use Lang;
use Session;
use Validator;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }

    public function index()
    {
        $user = Session::get('users');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],16)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::warehouse/index', $data);
    }

    public function get(Request $request){
        $warehouse = Warehouse::where('warehouse_id', $request->input('warehouse_id'))->first();

        return response()->json(array('data' => $warehouse));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        if($request->get('id') != null){
            return redirect('admin/warehouse');
        }
        $user = Session::get('users');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],16)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $api_key = Config::get('maps.key');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $warehouse_type = WarehouseType::where('active_flag',1)->get();
        $data['warehouse_type'] = $warehouse_type;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['api_key'] = $api_key;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        return view('admin::warehouse/form', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'warehouse_label' => ['required'],
            'warehouse_province' => ['required'],
            'warehouse_regency' => ['required'],
            'warehouse_district' => ['required'],
            'warehouse_type' => ['required'],
            'warehouse_village' => ['required'],
            'contact_person' => ['required'],
            'phone_number' => ['required'],
            'address_detail' => [' required']
            // 'main_flag' => ['required']
        ]);

        $alert = "";
        if($validator->fails()){
            $alert = Lang::get("notification.has been inserted",['result' => strtolower(Lang::get('notification.inaccurately'))]);
            if($request->input('store_id') != null){
                $alert = Lang::get("notification.has been updated",['result' => strtolower(Lang::get('notification.inaccurately'))]);
            }
            Session::flash('message_alert', $alert);
            return redirect()->back()->withErrors($validator);
        }else{
            // print_r($request->all());
            // exit;
            $gps_point = $request->input('latitude') . "," . $request->input('longitude');
            if($request->input('address_id') == null){
                $warehouse_id = AddressDetail::insertGetId([
                    'address_name' => $request->input('warehouse_label'),
                    'provinsi_id' => $request->input('warehouse_province'),
                    'kabupaten_kota_id' => $request->input('warehouse_regency'),
                    'kecamatan_id' => $request->input('warehouse_district'),
                    'kelurahan_desa_id' => $request->input('warehouse_village'),
                    'address_detail' => $request->input('address_detail'),
                    'user_id' => Session::get('users')['id'],
                    'active_flag' => 1,
                    'contact_person' => $request->input('contact_person'),
                    'phone_number' => $request->input('phone_number'),
                    'created_by' => Session::get('users')['id'],
                    // 'isMain' => $request->input('main_flag'),
                    'latitude' => $request->input('latitude'),
                    'longitude' => $request->input('longitude'),
                    'gps_point' => $gps_point
                ]);

                $alert = Lang::get("notification.has been inserted",['result' => strtolower(Lang::get('notification.inaccurately'))]);
                if($warehouse_id > 0){
                    $warehouse_id = Warehouse::insertGetId([
                        'warehouse_name' => $request->input('warehouse_label'),
                        'warehouse_type_id' => $request->input('warehouse_type'),
                        'user_id' => Session::get('users')['id'],
                        'address_id' => $warehouse_id,
                        'created_by' => Session::get('users')['id'],
                        'contact_person' => $request->input('contact_person'),
                        'phone_number' => $request->input('phone_number')
                    ]);
                    if($request->input('warehouse_type') == 2){
                        $children_or_parent = $request->input('children_warehouse');
                        // for ($b=0; $b < count($children_or_parent); $b++) { 
                        //     $
                        // }
                        Warehouse::whereIn('warehouse_id',$children_or_parent)->update(['parent_warehouse_id' => $warehouse_id]);
                    }else{
                        $children_or_parent = $request->input('children_warehouse');
                        // for ($b=0; $b < count($children_or_parent); $b++) { 
                        //     $
                        // }
                        Warehouse::where('warehouse_id',$warehouse_id)->update(['parent_warehouse_id' => $children_or_parent]);
                    }
                    $alert = Lang::get("notification.has been inserted",['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }else{
                $update = AddressDetail::where('address_id', $request->input('address_id'))->update([
                    'address_name' => $request->input('warehouse_label'),
                    'provinsi_id' => $request->input('warehouse_province'),
                    'kabupaten_kota_id' => $request->input('warehouse_regency'),
                    'kecamatan_id' => $request->input('warehouse_district'),
                    'kelurahan_desa_id' => $request->input('warehouse_village'),
                    'address_detail' => $request->input('address_detail'),
                    'contact_person' => $request->input('contact_person'),
                    'phone_number' => $request->input('phone_number'),
                    // 'address_id' => $warehouse_id,
                    // 'isMain' => $request->input('main_flag'),
                    'latitude' => $request->input('latitude'),
                    'longitude' => $request->input('longitude'),
                    'gps_point' => $gps_point
                ]);

                $alert = Lang::get("notification.has been updated",['result' => strtolower(Lang::get('notification.inaccurately'))]);

                if($update){
                    AddressDetail::where('address_id', $request->input('address_id'))->update([
                        'updated_by' => Session::get('users')['id']
                    ]);
                    Warehouse::where('address_id', $request->input('address_id'))->update([
                        'warehouse_name' => $request->input('warehouse_label'),
                        'warehouse_type_id' => $request->input('warehouse_type'),
                        'user_id' => Session::get('users')['id'],
                        'updated_by' => Session::get('users')['id'],
                        'contact_person' => $request->input('contact_person'),
                        'phone_number' => $request->input('phone_number')
                    ]);
                    $warehouse_id = $request->input('warehouse_id');
                    if($request->input('warehouse_type') == 2){
                        $children_or_parent = $request->input('children_warehouse');
                        // for ($b=0; $b < count($children_or_parent); $b++) { 
                        //     $
                        // }
                        Warehouse::whereIn('warehouse_id',$children_or_parent)->update(['parent_warehouse_id' => $warehouse_id]);
                    }else{
                        $children_or_parent = $request->input('children_warehouse');
                        // for ($b=0; $b < count($children_or_parent); $b++) { 
                        //     $
                        // }
                        Warehouse::where('warehouse_id',$warehouse_id)->update(['parent_warehouse_id' => $children_or_parent]);
                    }

                    $alert = Lang::get("notification.has been updated",['result' => strtolower(Lang::get('notification.successfully'))]);
                }
            }
            Session::flash('message_alert', $alert);
            return redirect('admin/warehouse');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        if($request->get('id') == null){
            return redirect('admin/warehouse');
        }
        $warehouse = AddressDetail::join('00_warehouse','00_warehouse.address_id','=','00_address.address_id')->where('00_address.address_id', $request->get('id'))->first();
        // print_r($warehouse);
        // echo $warehouse['gps_point'];
        // exit;
        // exit;
        if($warehouse['latitude']){
            $latitude = $warehouse['latitude'];
            $longitude = $warehouse['longitude'];
        }else{
            $gps_point = explode( ',', $warehouse['gps_point'] );
            $latitude = $gps_point[0];
            $longitude = $gps_point[1];
        }
        
        $warehouse_by_id = array(
            "warehouse_type" => $warehouse['warehouse_type_id'],
            "warehouse_label" => $warehouse['address_name'],
            "province_id" => $warehouse['provinsi_id'],
            "regency_id" => $warehouse['kabupaten_kota_id'],
            "district_id" => $warehouse['kecamatan_id'],
            "village_id" => $warehouse['kelurahan_desa_id'],
            "address_detail" => $warehouse['address_detail'],
            "contact_person" => $warehouse['contact_person'],
            "phone_number" => $warehouse['phone_number'],
            'parent_warehouse_id' => $warehouse['parent_warehouse_id'],
            'warehouse_id' => $warehouse['warehouse_id'],
            // "main_flag" => $warehouse['isMain'],
            'latitude' => $latitude,
            'longitude' => $longitude,
        );
        $user = Session::get('users');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],16)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $api_key = Config::get('maps.key');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $warehouse_type = WarehouseType::where('active_flag',1)->get();
        $data['warehouse_type'] = $warehouse_type;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['children_warehouse'] = Warehouse::where(['warehouse_type_id' => 1, 'active_flag' => 1])->get();
        $data['parent_warehouse'] = Warehouse::where(['warehouse_type_id' => 2, 'active_flag' => 1])->get();
        // $category_name = array_map(function($value){
        //     return $value['category_name'];
        // }, $category);
        $data['children_or_parent_warehouse_by_id'] = Warehouse::select('00_warehouse.*')->where('00_warehouse.parent_warehouse_id',function($query) use($request){
            $query->select('ww.warehouse_id')->from('00_warehouse AS ww')->JOIN('00_address AS ad','ad.address_id','=','ww.address_id')->where('ad.address_id',$request->get('id'));
        })->get()->toArray();
        // print_r($data['children_or_parent_warehouse_by_id']);
        // echo $data['children_or_parent_warehouse_by_id'];
        // exit;
        $data['api_key'] = $api_key;
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['warehouse_by_id'] = $warehouse_by_id;
        return view('admin::warehouse/form', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function activate(Request $request)
    {
        $update = AddressDetail::where('address_id', $request->get('id'))->where('user_id', Session::get('users')['id'])->update(['active_flag' => 1]);

        $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            Warehouse::where('address_id', $request->get('id'))->where('user_id', Session::get('users')['id'])->update(['active_flag' => 1]);
            $alert = Lang::get('notification.has been activated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/warehouse');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deactivate(Request $request)
    {
        $update = AddressDetail::where('address_id', $request->get('id'))->where('user_id', Session::get('users')['id'])->update(['active_flag' => 0]);

        $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.inaccurately'))]);
        if($update){
            Warehouse::where('address_id', $request->get('id'))->where('user_id', Session::get('users')['id'])->update(['active_flag' => 0]);
            $alert = Lang::get('notification.has been deactivated',['result' => strtolower(Lang::get('notification.successfully'))]);
        }
        Session::flash('message_alert', $alert);
        return redirect('admin/warehouse');
    }
}
