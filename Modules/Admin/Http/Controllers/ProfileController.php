<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Session;
use App\User;
use App;
use Validator;
use App\Rules\UsernameValidation;
use App\Rules\PasswordValidation;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Lang;
use Hash;
use Config;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(){
        App::setLocale('in');
        $this->middleware('admin_auth');
    }
    
    public function index()
    {
        $user = Session::get('users');
        $logged_in_user = User::where('user_id', $user['id'])->first();
        $data['user'] = $user;
        $data['logged_in_user'] = $logged_in_user;
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        return view('admin::profile', $data);
    }
    
    public function password(){
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $all_menu = $user_menu->get($user['id']);
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['all_menu'] = $all_menu;
        return view('admin::password', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function change(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_name' => ['required'],
            'user_email' => ['required','email:rfc,dns', new UsernameValidation(Session::get('users')['id'])],
            'user_phone_number' => ['required','numeric','digits_between:10,15', new UsernameValidation(Session::get('users')['id'])]
        ]);

        // if($request->file('profile_picture') !== null){
        //     $validator = Validator::make($request->all(),[
        //         'user_name' => ['required'],
        //         'user_email' => ['required','email:rfc,dns', new UsernameValidation(Session::get('users')['id'])],
        //         'user_phone_number' => ['required','numeric','digits_between:10,15', new UsernameValidation(Session::get('users')['id'])]
        //         'profile_picture' => ['required','mimes:jpeg,bmp,png,jpg,pneg']
        //     ]);
        // }

        if($validator->fails()){
            Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]));
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $folder_name = 'img/uploads/profile_picture/';
            $file = $request->file('profile_picture');
            // print_r($file);
            $ext = $file == null ? '' : $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;
            if($file !== null){
                if (!file_exists($folder_name)) {
                    mkdir($folder_name, 777, true);
                }
                ini_set('memory_limit', '256M');
                Image::make($file)->fit(300,300)->save(($folder_name.$name),80);
                chmod($folder_name.$name, 0777);
                User::where('user_id',Session::get('users')['id'])->update([
                    'user_name' => $request->input('user_name'),
                    'user_email' => $request->input('user_email'),
                    'user_phone_number' => $request->input('user_phone_number'),
                    'updated_by' => Session::get('users')['id'],
                    'user_image' => $folder_name.$name
                ]);
                if(Session::get('users')['profile_picture'] != null || Session::get('users')['profile_picture'] != ""){
                    File::delete(Session::get('users')['profile_picture']);
                }
                Session::put('users.fullname', $request->input('user_name'));
                Session::put('users.email', $request->input('user_email'));
                Session::put('users.profile_picture',$folder_name.$name);
            }else{
                User::where('user_id',Session::get('users')['id'])->update([
                    'user_name' => $request->input('user_name'),
                    'user_email' => $request->input('user_email'),
                    'user_phone_number' => $request->input('user_phone_number'),
                    'updated_by' => Session::get('users')['id']
                ]);
                Session::put('users.fullname', $request->input('user_name'));
                Session::put('users.email', $request->input('user_email'));
            }
            Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/profile');
        }
    }

    public function change_password(Request $request){
        $validator = Validator::make($request->all(),[
            'user_old_password' => ['required', new PasswordValidation($request->input('user_email'))],
            'user_new_password' => ['required','regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/','min:8'],
            'user_confirm_password' => ['required','same:user_new_password']
        ]);
        if($validator->fails()){
            // Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.inaccurately'))]));
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            User::where('user_id',Session::get('users')['id'])->update(['user_password' => Hash::make($request->input('user_new_password')), 'pwd_changed' => 1]);
            
            Session::flash('message_alert',Lang::get('notification.has been updated',['result' => strtolower(Lang::get('notification.successfully'))]));
            return redirect('admin/password');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
