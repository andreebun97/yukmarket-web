<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Menu;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function get($user_id, $menu_id = null)
    {
        $all_menu = Menu::select('98_menu.*',DB::raw('(CASE WHEN `98_menu`.`menu_id` IN (SELECT `98_role_menu`.`menu_id` FROM `98_role_menu` WHERE `98_role_menu`.`active_flag` = 1 AND `98_role_menu`.`role_id` = (SELECT `98_user_role`.`role_id` FROM `98_user_role` WHERE `98_user_role`.`user_id` = '.$user_id.')) THEN 1 ELSE 0 END) AS `access_status`'),DB::raw('(CASE WHEN 98_menu.menu_id IN (SELECT mn.parent_id FROM 98_menu AS mn WHERE mn.parent_id = 98_menu.menu_id) THEN (SELECT COUNT(mn.menu_id) FROM 98_menu AS mn WHERE mn.parent_id = 98_menu.menu_id) ELSE 0 END) AS total_children','98_menu.orderno'),DB::raw('(CASE WHEN 98_menu.menu_id IN (SELECT mn.parent_id FROM 98_menu AS mn JOIN 98_role_menu AS rm ON rm.menu_id = mn.menu_id WHERE mn.parent_id = 98_menu.menu_id AND rm.active_flag = 1 AND rm.role_id = (SELECT uro.role_id FROM 98_user_role AS uro WHERE uro.user_id = '.$user_id.')) THEN (SELECT COUNT(mn.menu_id) FROM 98_menu AS mn JOIN 98_role_menu AS rm ON rm.menu_id = mn.menu_id WHERE mn.parent_id = 98_menu.menu_id AND rm.active_flag = 1 AND rm.role_id = (SELECT uro.role_id FROM 98_user_role AS uro WHERE uro.user_id = '.$user_id.')) ELSE 0 END) AS total_active_children'))->orderByRaw("(CASE WHEN 98_menu.parent_id IS NULL THEN 98_menu.orderno ELSE (98_menu.parent_id) END) ASC")->where('98_menu.active_flag',1)->where('98_menu.is_menu',1);
        if($menu_id != null){
            $all_menu->where('98_menu.menu_id',$menu_id);
        }
        $all_menu = $all_menu->get()->toArray();
        // $parent_id = 17;
        // $menu_by_filter = array_filter($all_menu,function($value) use($parent_id){
        //     return ($value['parent_id'] == $parent_id);
        // });
        // print_r($all_menu);
        // exit;
        return $all_menu;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
