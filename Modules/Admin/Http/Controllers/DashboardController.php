<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use App\Helpers\Currency;
use App\Order;
use App\OrderDetail;
use App\Customer;
use App\Product;
use App\Category;
use App\OrderStatus;
use App\Notification;
use App\ComplaintStatus;
use App;
use Lang;
use DB;
use Config;
use Illuminate\Support\Str;
use Session;
use DateTime;
use DatePeriod;
use DateInterval;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    protected $currency;

    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $this->currency = $currency;
    }

    public function index(Request $request)
    {
        $user = Session::get('users');
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],1)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $time = $request->get("time");
        $category = Category::where('active_flag', 1)->get();
        if($time == null){
            $time = "today";
        }
        $customer = Customer::count();
        $today_date = date("Y-m-d");
        $last_month_start_date_range = date("Y-m-01", strtotime('-1 months'));
        $last_month_end_date_range = date("Y-m-d", strtotime('-1 months'));
        // $end_date_in_last_month = date("Y-m-t", strtotime('-1 months'));
        $current_month_start_date_range = date("Y-m-01");
        $current_month_end_date_range = date("Y-m-t");
        // echo $current_month_end_date_range;
        // exit;

        if(explode('-', $today_date)[2] == explode('-', $current_month_end_date_range)[2]){
            $last_month_end_date_range = date("Y-m-t", strtotime('-1 months'));
            $current_month_end_date_range = date("Y-m-t");
        }

        if(explode('-', $today_date)[2] < explode('-', $current_month_end_date_range)[2]){
            $current_month_end_date_range = $today_date;
        }

        // echo $last_month_end_date_range;
        // exit;
        // $end_date_in_last_month = date("Y-m-t", strtotime('-1 months'));
        $yesterday_date = date("Y-m-d", strtotime('-1 days'));
        $last_week_date = date("Y-m-d", strtotime('-1 weeks'));
        $current_dayname = date("l");
        $last_week_start_date_range = date("Y-m-d", strtotime("last week monday"));
        $last_week_end_date_range = date("Y-m-d", strtotime('-1 weeks'));

        $datefilterRevenue = $request->get('datefilterRevenue');
        $dates = explode('-', $datefilterRevenue);
        $from = date("Y-m-d", strtotime($dates[0]));
        if (!isset($dates[1])) {
            $to =null;
        }else{
            $to = date("Y-m-d", strtotime($dates[1]));
        }
        // echo $from;
        // exit;
        //  dd($dates);
        // echo "<br>";
        // dd($to);
        // exit;
        $current_week_start_date_range = date("Y-m-d",strtotime('monday this week'));
        $date_parameter = $today_date;
        if($time == 'yesterday'){
            $date_parameter = $yesterday_date;
        }else if($time == 'last_week'){
            $date_parameter = $last_week_date;
        }else if($time == 'last_month'){
            $date_parameter = $last_month_end_date_range;
        }else if($time == 'period'){
            $date_parameter = $dates;
        }
        
        $warehouse_id = $user['warehouse_id'];
        $organization_type_id = $user['organization_type_id'];
        $agent_id = $user['organization_id'];
        $parent_agent_id = $user['parent_organization_id'];
        $today_selling_product = Order::where(DB::raw('CAST(10_order.order_date AS DATE)'),$today_date)->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $today_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $today_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $today_selling_product = $today_selling_product->count('10_order.order_id');
        $yesterday_selling_product = Order::where(DB::raw('CAST(10_order.order_date AS DATE)'),$yesterday_date)->whereNotIn('10_order.order_status_id',[11,12]);
        // if($warehouse_id != null){
        //     $yesterday_selling_product->where('10_order.warehouse_id', $warehouse_id);
        // }
        if($warehouse_id != null){
            $yesterday_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $yesterday_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $yesterday_selling_product = $yesterday_selling_product->count('10_order.order_id');

        $last_week_selling_product = Order::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_week_end_date_range.'" AS DATE)')])->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $last_week_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $last_week_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $last_week_selling_product = $last_week_selling_product->count('10_order.order_id');

        $last_month_selling_product = Order::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_month_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_month_end_date_range.'" AS DATE)')])->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $last_month_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $last_month_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $last_month_selling_product = $last_month_selling_product->count('10_order.order_id');

        $current_month_selling_product = Order::where(DB::raw('MONTH(10_order.order_date)'),explode("-",$today_date)[1])->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $current_month_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $current_month_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $current_month_selling_product = $current_month_selling_product->count('10_order.order_id');
        // echo $current_month_selling_product;
        // exit;
        $this_week_selling_product = Order::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$current_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$today_date.'" AS DATE)')])->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $this_week_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $this_week_selling_product->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $this_week_selling_product = $this_week_selling_product->count('10_order.order_id');

        $today_selling_price = OrderDetail::where(DB::raw('CAST(10_order.order_date AS DATE)'),$today_date)->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $today_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $today_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $today_selling_price = $today_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        $yesterday_selling_price = OrderDetail::where(DB::raw('CAST(10_order.order_date AS DATE)'),$yesterday_date)->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $yesterday_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $yesterday_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $yesterday_selling_price = $yesterday_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        $last_week_selling_price = OrderDetail::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_week_end_date_range.'" AS DATE)')])->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $last_week_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $last_week_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $last_week_selling_price = $last_week_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        $last_month_selling_price = OrderDetail::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_month_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_month_end_date_range.'" AS DATE)')])->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $last_month_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $last_month_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $last_month_selling_price = $last_month_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        $current_month_selling_price = OrderDetail::where(DB::raw('MONTH(10_order.order_date)'),explode("-",$today_date)[1])->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $current_month_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $current_month_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $current_month_selling_price = $current_month_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        $this_week_selling_price = OrderDetail::whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$current_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$today_date.'" AS DATE)')])->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereNotIn('10_order.order_status_id',[11,12]);
        if($warehouse_id != null){
            $this_week_selling_price->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $this_week_selling_price->where('10_order.agent_user_id', $agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $this_week_selling_price = $this_week_selling_price->sum(DB::raw('(10_order_detail.price * 10_order_detail.quantity)'));

        // $products = Product::select('00_product.prod_name','00_product.uom_value','00_product.product_sku_id','00_inventory.stock','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.prod_id = 00_product.prod_id AND CAST(aa.created_date AS DATE) = "'.$date_parameter.'") AS sold_qty'),DB::raw('((CASE WHEN 00_inventory.stock - 00_inventory.minimum_stock IS NULL THEN 0 ELSE 00_inventory.stock - 00_inventory.minimum_stock END) ) AS running_out_of_stock'),'00_warehouse.warehouse_name','00_organization.organization_name')->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->orderBy('running_out_of_stock','ASC')->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')->join('00_warehouse','00_warehouse.warehouse_id','=','00_inventory.warehouse_id')->join('00_organization','00_organization.organization_id','=','00_inventory.organization_id')->offset(0)->limit(10)->where('00_product.active_flag',1)->whereColumn('00_inventory.stock','00_inventory.minimum_stock')->get();
        $stock_level = $request->get('stock_level');
        // echo $stock_level;
        // exit;
        $total_purchased_quantity = array();

        // echo strpos($request->get('datefilterRevenue'), '-');
        // exit;
        // if(strpos($request->get('datefilterRevenue'), '-') !== false){
        //     $total_purchased_quantity = DB::raw(
        //         '(  SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail
        //                 JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //                 AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //                 AND CAST(10_order.order_date AS date) BETWEEN CAST("'.$from.'" AS DATE) and CAST("'.$to.'" AS DATE)
        //                 AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //     );
        // }else{
        //     $total_purchased_quantity = DB::raw(
        //         '(  SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail
        //                 JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //                 AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //                 AND CAST(10_order.order_date AS date) = "'.$date_parameter.'"
        //                 AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //     );
        // }
        
        // $payment = Order::select(
        //     '
        //     10_order.order_id',
        //     '10_order.order_code',
        //     '10_order.shipment_price',
        //     '10_order.grand_total',
        //     '10_payment.voucher_id',
        //     '10_payment.voucher_amount',
        //     '10_payment.pricing_include_tax',
        //     '10_payment.admin_fee','10_payment.admin_fee_percentage',
        //     '10_payment.national_income_tax',
        //     '10_payment.invoice_no','10_cashback.return_amount')
        //     ->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')
        //     ->leftJoin('10_cashback','10_cashback.payment_id','=','10_payment.payment_id')
        //     // ->where('10_order.payment_id', function($query) use($request){
        //     // $query->select('pa.payment_id')
        //     ->from('10_payment AS pa')
        //     ->join('10_order AS od','od.payment_id','=','pa.payment_id')
        //     // ->where('od.order_id', $request->get('id'));
        //     // })
        // ->get();
        

        $products = Product::select(
            '00_product.prod_id',
            '00_product.prod_name',
            '00_product.prod_image',
            '00_product.product_sku_id',
            '00_category.category_id',
            '00_category.category_name',
            '00_uom.uom_id',
            '00_uom.uom_name',
            '00_product.uom_value',
            '00_product.product_sku_id',
            DB::raw(
                '(  SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
                    FROM 10_order_detail
                        JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
                    WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
                        AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
                        AND CAST(10_order.order_date AS date) = "'.date('Y-m-d').'"
                        AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
            ),
            DB::raw(
                '(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'
            ),
            DB::raw(
                '(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'
            ),
            DB::raw(
                '(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'
            ),
            '00_product_warehouse.warehouse_id',
            '00_warehouse.warehouse_name'
        )
        ->join('00_uom','00_uom.uom_id','=','00_product.uom_id')
        ->join('00_category','00_category.category_id','=','00_product.category_id')
        ->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id');

        // if($time == 'this_month'){
        //     $products = Product::select(
        //         '00_product.prod_id',
        //         '00_product.prod_name',
        //         '00_product.prod_image',
        //         '00_product.product_sku_id',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id','00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id'
        //         ,DB::raw(
        //             '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //                 AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //                 AND MONTH(10_order.order_date) = "'.explode("-",$today_date)[1].'"
        //                 AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'
        //         ),
        //         '00_product_warehouse.warehouse_id',
        //         '00_warehouse.warehouse_name'
        //     )
        //         ->join('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
        //         ->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')
        //         ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
        //         ->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id');

        // }else if($time == 'last_month'){
        //     $products = Product::select(
        //         '00_product.prod_id',
        //         '00_product.prod_name',
        //         '00_product.prod_image',
        //         '00_product.product_sku_id',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id',
        //         '00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw(
        //             '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail
        //                 JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //                 AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //                 AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE)
        //                 AND CAST("'.$last_month_end_date_range.'" AS DATE)
        //                 AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'
        //         ),
        //         '00_product_warehouse.warehouse_id',
        //         '00_warehouse.warehouse_name'
        //     )
        //         ->join('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
        //         ->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')
        //         ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
        //         ->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id');
        //     // if($warehouse_id != null){
        //     //     $products = Product::select('00_product.prod_id','00_product.prod_name','00_product.prod_image','00_product.product_sku_id','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 10_order_detail JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id AND 10_order_detail.prod_id = 00_product_warehouse.prod_id AND 00_inventory.organization_id = 10_order.agent_user_id AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE)) AS sold_qty'),DB::raw('(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'),DB::raw('(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'),DB::raw('(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'),'00_product_warehouse.warehouse_id','00_warehouse.warehouse_name')->join('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id')->where('00_product_warehouse.warehouse_id', $warehouse_id)->where('00_inventory.organization_id', $agent_id);
        //     // }
        // }else if($time == 'this_week'){

        //     $products = Product::select(
        //         '00_product.prod_id',
        //         '00_product.prod_name',
        //         '00_product.prod_image',
        //         '00_product.product_sku_id',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id',
        //         '00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw(
        //             '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail
        //                 JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //                 AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //                 AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE)
        //                 AND CAST("'.$today_date.'" AS DATE)
        //                 AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'
        //         ),
        //         '00_product_warehouse.warehouse_id',
        //         '00_warehouse.warehouse_name'
        //     )
        //         ->join('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
        //         ->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')
        //         ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
        //         ->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id');

        // }else if($time == 'last_week'){

        //     $products = Product::select(
        //         '00_product.prod_id',
        //         '00_product.prod_name',
        //         '00_product.prod_image',
        //         '00_product.product_sku_id',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id',
        //         '00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw(
        //             '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END)
        //             FROM 10_order_detail
        //                 JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //             WHERE 10_order.warehouse_id = 00_product_warehouse.warehouse_id
        //             AND 10_order_detail.prod_id = 00_product_warehouse.prod_id
        //             AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE)
        //             AND CAST("'.$last_week_end_date_range.'" AS DATE)
        //             AND 10_order.order_status_id NOT IN (11,12)) AS sold_qty'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN SUM(00_inventory.stock) IS NULL THEN 0 ELSE SUM(00_inventory.stock) END) AS total_available_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) IS NULL THEN 0 ELSE (00_product_warehouse.minimum_stock - SUM(00_inventory.stock)) END) AS running_out_of_stock'
        //         ),
        //         DB::raw(
        //             '(CASE WHEN 00_product_warehouse.minimum_stock IS NULL THEN 0 ELSE 00_product_warehouse.minimum_stock END) AS minimum_stock'
        //         ),
        //         '00_product_warehouse.warehouse_id',
        //         '00_warehouse.warehouse_name'
        //     )
        //         ->join('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->join('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
        //         ->join('00_warehouse','00_warehouse.warehouse_id','=','00_product_warehouse.warehouse_id')
        //         ->join('00_inventory','00_inventory.prod_id','=','00_product.prod_id')
        //         ->groupBy('00_product_warehouse.prod_id','00_product_warehouse.warehouse_id');

        // }
        if($warehouse_id != null){
            $products->where('00_product_warehouse.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $products->where('00_inventory.organization_id',$agent_id)->orWhere('00_inventory.organization_id',$parent_agent_id);
        }
        // echo $products->toSql();
        // exit;
        if($stock_level == "running_out_of_stock" || $stock_level == null){
            $products->orderBy('total_available_stock','DESC');
            $products = $products->get()->toArray();
            $products = array_filter($products, function($value){
                return $value['total_available_stock'] <= $value['minimum_stock'];
            });
        }elseif($stock_level == "best_selling_stock" || $stock_level == "unsold_product"){
            $products->orderBy("sold_qty","DESC");
            $products = $products->get()->toArray();
            if($stock_level == "unsold_product"){
                $products = array_filter($products, function($value){
                    return $value['sold_qty'] == 0;
                });
            }else{
                $products = array_filter($products, function($value){
                    return $value['sold_qty'] > 0;
                });
            }
        }
        $products = array_slice($products, 0, 10);
        // print_r($products);
        // exit;
        // $products = $products->toSql();
        // echo $products;
        // exit;
        // print_r($products);
        // exit;
        // echo count($products);
        // exit;

        //produk terlaris
        $datefilter = $request->get('datefilter');
        
        $dates = explode('-', $datefilter);
        $another_from = date("Y-m-d", strtotime($dates[0]));
        // $to = date("Y-m-d", strtotime($dates[1]));
        if (!isset($dates[1])) {
            $another_to =null;
        }else{
            $another_to = date("Y-m-d", strtotime($dates[1]));
        }
        //  dd($from, $to);
        $purchased_quantity_query = '';
        $total_transaction_query = '';
        $total_price_query = '';
        if($datefilter != null && Str::contains($datefilter,'-') == true){
            $purchased_quantity_query = '(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa JOIN 10_order AS bb ON bb.order_id = aa.order_id where aa.prod_id = 00_product.prod_id AND CAST(bb.order_date AS DATE) BETWEEN CAST("'.$another_from.'" AS DATE) AND CAST("'.$another_to.'" AS DATE)) AS sold_qty';

            $total_transaction_query = '(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 
            ELSE SUM((bb.quantity * bb.price)) END) 
            FROM 10_order_detail as bb 
            JOIN 10_order AS kk ON kk.order_id = bb.order_id 
            where bb.prod_id = 00_product.prod_id AND CAST(kk.order_date AS DATE) 
            BETWEEN CAST("'.$another_from.'" AS DATE) AND CAST("'.$another_to.'" AS DATE)) AS total_transaction';

            $total_price_query = '(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk JOIN 10_order AS bb ON bb.order_id = kk.order_id where kk.prod_id = 00_product.prod_id AND CAST(bb.order_date AS DATE) BETWEEN CAST("'.$another_from.'" AS DATE) AND CAST("'.$another_to.'" AS DATE)) AS total_price';
        }else{
            $purchased_quantity_query = '(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa JOIN 10_order AS bb ON bb.order_id = aa.order_id where aa.prod_id = 00_product.prod_id AND CAST(bb.order_date AS DATE) = "'.date('Y-m-d').'") AS sold_qty';
            $total_transaction_query = '(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb 
            JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND CAST(kk.order_date AS DATE) = "'.date('Y-m-d').'") AS total_transaction';
            $total_price_query = '(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk JOIN 10_order AS bb ON bb.order_id = kk.order_id 
            where kk.prod_id = 00_product.prod_id AND CAST(bb.order_date AS DATE) = "'.date('Y-m-d').'") AS total_price';
        }
        

        $best_selling_product = Product::select(
            '00_product.*',
            '00_category.category_id',
            '00_category.category_name',
            '00_uom.uom_id',
            '00_uom.uom_name',
            '00_product.uom_value',
            '00_product.product_sku_id',
            '10_order.order_status_id',

            DB::raw($purchased_quantity_query),

            DB::raw($total_transaction_query),

            DB::raw($total_price_query),
            '10_order.order_date'
            )

            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
            ->join('00_category','00_category.category_id','=','00_product.category_id')
            ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
            ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
            ->whereNotIn('10_order.order_status_id',[11,12])
            ->orderBy('10_order.order_date', 'ASC');

            if($datefilter != null && Str::contains($datefilter,'-') == true){
                $best_selling_product->whereBetween(DB::raw('CAST(10_order.order_date AS date)'), [DB::raw('CAST("'.$another_from.'" AS DATE)'),DB::raw('CAST("'.$another_to.'" AS DATE)')]);
            }else{
                $best_selling_product->where(DB::raw('CAST(10_order.order_date AS DATE)'),date('Y-m-d'));
            }
            // ->where(DB::raw('CAST(10_order.order_date AS DATE)'),$date_parameter)
        // if($warehouse_id != null){
        //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.order_id = 10_order.order_id AND aa.prod_id = 00_product.prod_id AND CAST(aa.created_date AS DATE) = "'.$date_parameter.'") AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND bb.warehouse_id = "'.$warehouse_id.'" AND kk.agent_user_id = "'.$agent_id.'" AND CAST(bb.created_date AS DATE) = "'.$date_parameter.'") AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk where kk.order_id = 10_order.order_id AND kk.prod_id = 00_product.prod_id AND CAST(kk.created_date AS DATE) = "'.$date_parameter.'") AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id', $agent_id)->where(DB::raw('CAST(10_order.order_date AS DATE)'),$date_parameter);
        // }
        // if($time == 'this_month'){
        //     $best_selling_product = Product::select(
        //         '00_product.*',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id','00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) 
        //         FROM 10_order_detail as aa where aa.prod_id = 00_product.prod_id AND MONTH(aa.created_date) = "'.explode("-",$today_date)[1].'") AS sold_qty'),

        //         DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) 
        //         FROM 10_order_detail as bb 
        //         JOIN 10_order AS kk ON kk.order_id = bb.order_id 
        //         where bb.prod_id = 00_product.prod_id AND MONTH(bb.created_date) = "'.explode("-",$today_date)[1].'") AS total_transaction'),

        //         DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) 
        //         FROM 10_order_detail as kk 
        //         where kk.prod_id = 00_product.prod_id AND MONTH(kk.created_date) = "'.explode("-",$today_date)[1].'") AS total_price')
        //         )
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
        //         ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
        //         ->where(DB::raw('MONTH(10_order_detail.created_date)'),explode("-",$today_date)[1]);
        //     // if($warehouse_id != null){
        //     //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.order_id = 10_order.order_id AND aa.prod_id = 00_product.prod_id AND MONTH(aa.created_date) = "'.explode("-",$today_date)[1].'") AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND kk.warehouse_id = "'.$warehouse_id.'" AND kk.agent_user_id = "'.$agent_id.'" AND MONTH(bb.created_date) = "'.explode("-",$today_date)[1].'") AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk where kk.prod_id = 00_product.prod_id and kk.order_id = 10_order.order_id AND MONTH(kk.created_date) = "'.explode("-",$today_date)[1].'") AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id', $agent_id)->where(DB::raw('MONTH(10_order.order_date)'),explode("-",$today_date)[1]);
        //     // }
        // }else if($time == 'last_month'){
        //     $best_selling_product = Product::select(
        //         '00_product.*',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id',
        //         '00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) 
        //         FROM 10_order_detail as aa JOIN 10_order AS bb ON bb.order_id = aa.order_id 
        //         where aa.prod_id = 00_product.prod_id AND (CAST(bb.order_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS sold_qty'),

        //         DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) 
        //         FROM 10_order_detail as bb 
        //         JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND (CAST(kk.order_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS total_transaction'),

        //         DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) 
        //         FROM 10_order_detail as kk 
        //         JOIN 10_order AS cc ON cc.order_id = kk.order_id 
        //         where kk.prod_id = 00_product.prod_id AND (CAST(cc.order_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS total_price')

        //         )
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
        //         ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
        //         ->where(DB::raw('MONTH(10_order.order_date)'),explode("-",$last_month_end_date_range)[1])
        //         ->whereNotIn('10_order.order_status_id',[11,12]);
        //     // if($warehouse_id != null){
        //     //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.order_id = 10_order.order_id AND aa.prod_id = 00_product.prod_id AND (CAST(aa.created_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND kk.warehouse_id = "'.$warehouse_id.'" AND kk.agent_user_id = "'.$agent_id.'" AND (CAST(bb.created_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk where kk.prod_id = 00_product.prod_id AND kk.order_id = 10_order.order_id AND (CAST(kk.created_date AS DATE) BETWEEN CAST("'.$last_month_start_date_range.'" AS DATE) AND CAST("'.$last_month_end_date_range.'" AS DATE))) AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id', $agent_id)->where(DB::raw('MONTH(10_order.order_date)'),explode("-",$last_month_end_date_range)[1]);
        //     // }
        // }else if($time == 'this_week'){
        //     $best_selling_product = Product::select(
        //         '00_product.*',
        //         '00_category.category_id',
        //         '00_category.category_name',
        //         '00_uom.uom_id',
        //         '00_uom.uom_name',
        //         '00_product.uom_value',
        //         '00_product.product_sku_id',
        //         DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) 
        //         FROM 10_order_detail as aa 
        //         JOIN 10_order AS bb ON bb.order_id = aa.order_id where aa.prod_id = 00_product.prod_id AND (CAST(bb.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) 
        //         AND CAST("'.$today_date.'" AS DATE))) AS sold_qty'),

        //         DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) 
        //         FROM 10_order_detail as bb 
        //         JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id 
        //         AND (CAST(kk.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) 
        //         AND CAST("'.$today_date.'" AS DATE))) AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) 
        //         FROM 10_order_detail as kk 
        //         JOIN 10_order AS ord ON ord.order_id = kk.order_id where kk.prod_id = 00_product.prod_id 
        //         AND (CAST(ord.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS total_price'),
        //         '10_order.order_date'
        //         )

        //         ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
        //         ->join('00_category','00_category.category_id','=','00_product.category_id')
        //         ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
        //         ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
        //         ->whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),
        //         [DB::raw('CAST("'.$current_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$today_date.'" AS DATE)')])
        //         ->whereNotIn('10_order.order_status_id',[11,12]);
        //     // if($warehouse_id != null){
        //     //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.prod_id = 00_product.prod_id AND aa.order_id = 10_order.order_id AND (CAST(aa.created_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND kk.warehouse_id = "'.$warehouse_id.'" AND kk.agent_user_id = "'.$agent_id.'" AND (CAST(bb.created_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk where kk.order_id = 10_order.order_id AND kk.prod_id = 00_product.prod_id AND (CAST(kk.created_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id',$agent_id)->whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$current_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$today_date.'" AS DATE)')]);
        //     // }
        // }else if($time == 'last_week'){
        //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa JOIN 10_order AS bb ON bb.order_id = aa.order_id where aa.prod_id = 00_product.prod_id AND (CAST(bb.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND (CAST(kk.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk JOIN 10_order AS ord ON ord.order_id = kk.order_id where kk.prod_id = 00_product.prod_id AND (CAST(ord.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_week_end_date_range.'" AS DATE)')])->whereNotIn('10_order.order_status_id',[11,12]);
        //     // if($warehouse_id != null){
        //     //     $best_selling_product = Product::select('00_product.*','00_category.category_id','00_category.category_name','00_uom.uom_id','00_uom.uom_name','00_product.uom_value','00_product.product_sku_id',DB::raw('(SELECT (CASE WHEN SUM(aa.quantity) IS NULL THEN 0 ELSE SUM(aa.quantity) END) FROM 10_order_detail as aa where aa.order_id = 10_order.order_id AND aa.prod_id = 00_product.prod_id AND (CAST(aa.created_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS sold_qty'),DB::raw('(SELECT (CASE WHEN SUM(bb.quantity * bb.price) IS NULL THEN 0 ELSE SUM((bb.quantity * bb.price)) END) FROM 10_order_detail as bb JOIN 10_order AS kk ON kk.order_id = bb.order_id where bb.prod_id = 00_product.prod_id AND kk.warehouse_id = "'.$warehouse_id.'" AND kk.agent_user_id = "'.$agent_id.'" AND (CAST(bb.created_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS total_transaction'),DB::raw('(SELECT (CASE WHEN SUM(kk.price) IS NULL THEN 0 ELSE SUM(kk.price) END) FROM 10_order_detail as kk where kk.prod_id = 00_product.prod_id AND kk.order_id = 10_order.order_id AND (CAST(kk.created_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS total_price'))->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')->join('00_category','00_category.category_id','=','00_product.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id', $agent_id)->whereBetween(DB::raw('CAST(10_order.order_date AS DATE)'),[DB::raw('CAST("'.$last_week_start_date_range.'" AS DATE)'),DB::raw('CAST("'.$last_week_end_date_range.'" AS DATE)')]);
        //     // }
        // }
        if($warehouse_id != null){
            $best_selling_product->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $best_selling_product->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }

        if($request->get('category') != null && $request->get('category') != 'all'){
            $best_selling_product->where('00_category.category_id', $request->get('category'));
        }

        if($request->get('value') == 'sold_item'){
            $best_selling_product->orderBy('sold_qty','DESC');
        }else if($request->get('value') == 'total_price'){
            $best_selling_product->orderBy('total_transaction','DESC');
        }else{
            $best_selling_product->orderBy('total_price','DESC');
        }
        
       
        // $datefilter = $request->get('datefilter');
        $best_selling_product->where('00_product.active_flag',1);
        // dd($request->all());

        $best_selling_product = $best_selling_product->offset(0)->limit(10)->distinct()->get();
        // dd($best_selling_product);
        // echo $best_selling_product;
        // print_r($best_selling_product);
        // exit;
        $months = array(
            array(
                'in' => 'Januari',
                'en' => 'January'
            ),
            array(
                'in' => 'Februari',
                'en' => 'February'
            ),
            array(
                'in' => 'Maret',
                'en' => 'March'
            ),
            array(
                'in' => 'April',
                'en' => 'April'
            ),
            array(
                'in' => 'Mei',
                'en' => 'May'
            ),
            array(
                'in' => 'Juni',
                'en' => 'June'
            ),
            array(
                'in' => 'Juli',
                'en' => 'July'
            ),
            array(
                'in' => 'Agustus',
                'en' => 'August'
            ),
            array(
                'in' => 'September',
                'en' => 'September'
            ),
            array(
                'in' => 'Oktober',
                'en' => 'October'
            ),
            array(
                'in' => 'November',
                'en' => 'November'
            ),
            array(
                'in' => 'Desember',
                'en' => 'December'
            )
        );
        $data['user'] = $user;
        $data['all_menu'] = $all_menu;
        $data['time'] = $time;
        $data['customer'] = $customer;
        $data['accessed_menu'] = $accessed_menu;
        $data['currency'] = $this->currency;
        $data['months'] = $months;
        $data['full_date'] = array(
            'today_date' => $today_date,
            'yesterday_date' => $yesterday_date,
            'last_week_start_date_range' => $last_week_start_date_range,
            'last_week_end_date_range' => $last_week_end_date_range,
            'current_week_start_date_range' => $current_week_start_date_range,
            'last_month_start_date_range' => $last_month_start_date_range,
            'last_month_end_date_range' => $last_month_end_date_range,
            'current_month_start_date_range' => $current_month_start_date_range,
            'current_month_end_date_range' => $current_month_end_date_range
        );
        $data['selling'] = array(
            'today_qty' => $today_selling_product,
            'yesterday_qty' => $yesterday_selling_product,
            'last_week_qty' => $last_week_selling_product,
            'last_month_qty' => $last_month_selling_product,
            'current_month_qty' => $current_month_selling_product,
            'this_week_qty' => $this_week_selling_product,
            'today_price' => $today_selling_price,
            'yesterday_price' => $yesterday_selling_price,
            'last_week_price' => $last_week_selling_price,
            'last_month_price' => $last_month_selling_price,
            'current_month_price' => $current_month_selling_price,
            'this_week_price' => $this_week_selling_price,
        );
        // $products = $products->get()->toArray();

        // $best_selling_stocks = $products;
        // usort($best_selling_stocks, function($a, $b){
        //     return $a['total_available_stock'] - $b['total_available_stock'];
        // });
        // $best_selling_stocks = array_filter($best_selling_stocks, function($value){
        //     return $value['sold_qty'] > 0;
        // });
        // $best_selling_stocks = count($best_selling_stocks);
        // $unsold_products = array_filter($products, function($value){
        //     return $value['sold_qty'] == 0;
        // });
        // $unsold_products = count($unsold_products);
        // $running_out_of_stock_list = array_filter($products, function($value){
        //     return $value['total_available_stock'] <= $value['minimum_stock'];
        // });
        // $running_out_of_stock = count($running_out_of_stock_list);
        // // echo $running_out_of_stock;
        // $running_out_of_stock_list = array_slice($running_out_of_stock_list, 0, 10);
        // $data['running_out_of_stock'] = $running_out_of_stock;
        // $data['best_selling_stocks'] = $best_selling_stocks;
        // $data['unsold_products'] = $unsold_products;
        $data['product'] = $products;
        $data['best_selling_product'] = $best_selling_product;
        $data['category'] = $category;

        //Revenue
        // $datefilterRevenue = $request->get('datefilterRevenue');
        // $dates = explode('-', $datefilterRevenue);
        // $from = date("Y-m-d", strtotime($dates[0]));
        // // $to = date("Y-m-d", strtotime($dates[1]));
        // if (!isset($dates[1])) {
        //     $to =null;
        //  }else{
        //     $to = date("Y-m-d", strtotime($dates[1]));
        //  }
        // echo $request->get('datefilterRevenue').'<br>';
        // echo $request->get('time');
        // exit;
        if($datefilterRevenue != null && $request->get('time') == 'period' && (Str::contains($datefilterRevenue,'-') == true)){
            $purchased_quantity_query = '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$from.'" AS DATE) AND CAST("'.$to.'" AS DATE)) AS `purchased_quantity`';
            $purchased_price_query = '(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
            FROM 00_product 
            JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
            JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
            WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$from.'" AS DATE) AND CAST("'.$to.'" AS DATE)) AS `purchased_price`';
        }else{
            $purchased_quantity_query = '(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) = "'.$date_parameter.'") AS `purchased_quantity`';
            $purchased_price_query = '(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product 
            JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) = "'.$date_parameter.'") AS `purchased_price`';
        }

        $category = Category::select(
            '00_category.category_id',
            '00_category.category_name',

            DB::raw($purchased_quantity_query),

            DB::raw($purchased_price_query)

            )
            // DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) 
            //     FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
            //     JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
            //     WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
            //     BETWEEN CAST("'.$from.'" AS DATE) 
            //     AND CAST("'.$to.'" AS DATE)) AS `purchased_quantity`'),

            // DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
            //     FROM 00_product 
            //     JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
            //     JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
            //     WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
            //     BETWEEN CAST("'.$from.'" AS DATE) 
            //     AND CAST("'.$to.'" AS DATE)) AS `purchased_price`')

            // )

            ->join('00_product','00_product.category_id','=','00_category.category_id')
            ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
            ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
            // ->whereBetween(DB::raw('CAST(10_order.order_date AS date)'), [DB::raw('CAST("'.$from.'" AS DATE)'),DB::raw('CAST("'.$to.'" AS DATE)')])
            ->distinct()->orderBy('purchased_quantity','DESC');
        // if($warehouse_id != null){
        //     // $category = Category::select('00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND CAST(10_order.order_date AS DATE) = "'.$date_parameter.'") AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) = "'.$date_parameter.'") AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id',$agent_id)->distinct()->orderBy('purchased_quantity','DESC')->get()->toArray();
        // }
        if($time == 'this_month'){
            $category = Category::select(
                '00_category.category_id',
                '00_category.category_name',

                DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) 
                FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
                WHERE 00_product.category_id = 00_category.category_id AND MONTH(10_order.order_date) = "'.explode("-",$today_date)[1].'") AS `purchased_quantity`'),

                DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
                FROM 00_product 
                JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
                WHERE 00_product.category_id = 00_category.category_id AND MONTH(10_order.order_date) = "'.explode("-",$today_date)[1].'") AS `purchased_price`')
                
                )
                ->join('00_product','00_product.category_id','=','00_category.category_id')
                ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
                ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
                ->distinct()->orderBy('purchased_quantity','DESC');
            // if($warehouse_id != null){
            //     $category = Category::select('00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND MONTH(10_order.order_date) = "'.explode("-",$today_date)[1].'") AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND MONTH(10_order.order_date) = "'.explode("-",$today_date)[1].'") AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id', $agent_id)->distinct()->orderBy('purchased_quantity','DESC')->get()->toArray();
            // }
        }else if($time == 'last_month'){
            $category = Category::select(
                '00_category.category_id',
                '00_category.category_name',
                DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) 
                FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
                WHERE 00_product.category_id = 00_category.category_id AND MONTH(10_order.order_date) = "'.explode("-",$last_month_end_date_range)[1].'") AS `purchased_quantity`'),

                DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
                FROM 00_product 
                JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
                WHERE 00_product.category_id = 00_category.category_id AND MONTH(10_order.order_date) = "'.explode("-",$last_month_end_date_range)[1].'") AS `purchased_price`')
                
                )
                ->join('00_product','00_product.category_id','=','00_category.category_id')
                ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
                ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
                ->distinct()->orderBy('purchased_quantity','DESC');
            // if($warehouse_id != null){
            //     $category = Category::select('00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 10_order_detail.order_id = 10_order.order_id AND 00_product.category_id = 00_category.category_id AND MONTH(10_order.order_date) = "'.explode("-",$last_month_end_date_range)[1].'") AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND MONTH(10_order.order_date) = "'.explode("-",$last_month_end_date_range)[1].'") AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id',$warehouse_id)->where('10_order.agent_user_id', $agent_id)->distinct()->orderBy('purchased_quantity','DESC')->get()->toArray();
            // }
        }else if($time == 'this_week'){
            $category = Category::select(
                '00_category.category_id',
                '00_category.category_name',
                DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) 
                FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
                WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
                BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) 
                AND CAST("'.$today_date.'" AS DATE)) AS `purchased_quantity`'),

                DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
                FROM 00_product 
                JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
                JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
                WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
                BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) 
                AND CAST("'.$today_date.'" AS DATE)) AS `purchased_price`')
                )
                ->join('00_product','00_product.category_id','=','00_category.category_id')
                ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
                ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
                ->distinct()
                ->orderBy('purchased_quantity','DESC');
            // if($warehouse_id != null){
            //     $category = Category::select('00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND (CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND (CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$current_week_start_date_range.'" AS DATE) AND CAST("'.$today_date.'" AS DATE))) AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id',$warehouse_id)->where('10_order.agent_user_id', $agent_id)->distinct()->orderBy('purchased_quantity','DESC')->get()->toArray();
            // }
        }else if($time == 'last_week'){
            $category = Category::select(
                '00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE)) AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE)) AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->distinct()->orderBy('purchased_quantity','DESC');
            // if($warehouse_id != null){
            //     $category = Category::select('00_category.category_id','00_category.category_name',DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND (CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS `purchased_quantity`'),DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id WHERE 00_product.category_id = 00_category.category_id AND 10_order_detail.order_id = 10_order.order_id AND (CAST(10_order.order_date AS DATE) BETWEEN CAST("'.$last_week_start_date_range.'" AS DATE) AND CAST("'.$last_week_end_date_range.'" AS DATE))) AS `purchased_price`'))->join('00_product','00_product.category_id','=','00_category.category_id')->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')->join('10_order','10_order.order_id','=','10_order_detail.order_id')->where('10_order.warehouse_id',$warehouse_id)->where('10_order.agent_user_id', $agent_id)->distinct()->orderBy('purchased_quantity','DESC')->get()->toArray();
            // }
        }
        // else if($time == 'period'){
        //     $category = Category::select(
        //         '00_category.category_id',
        //         '00_category.category_name',
    
        //         DB::raw('(SELECT (CASE WHEN SUM(10_order_detail.quantity) IS NULL THEN 0 ELSE SUM(10_order_detail.quantity) END) 
        //         FROM 00_product JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
        //         JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id 
        //         WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
        //         BETWEEN CAST("'.$from.'" AS DATE) 
        //         AND CAST("'.$to.'" AS DATE)) AS `purchased_quantity`'),

        //         DB::raw('(SELECT (CASE WHEN SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) IS NULL THEN 0 ELSE SUM((10_order_detail.quantity*10_order_detail.price)-10_order_detail.promo_value) END) 
        //         FROM 00_product 
        //         JOIN 10_order_detail ON 10_order_detail.prod_id = 00_product.prod_id 
        //         JOIN 10_order ON 10_order.order_id = 10_order_detail.order_id
        //         WHERE 00_product.category_id = 00_category.category_id AND CAST(10_order.order_date AS DATE) 
        //         BETWEEN CAST("'.$from.'" AS DATE) 
        //         AND CAST("'.$to.'" AS DATE)) AS `purchased_price`')

        //     )
    
        //         ->join('00_product','00_product.category_id','=','00_category.category_id')
        //         ->join('10_order_detail','10_order_detail.prod_id','=','00_product.prod_id')
        //         ->join('10_order','10_order.order_id','=','10_order_detail.order_id')
        //         // ->whereBetween(DB::raw('CAST(10_order.order_date AS date)'), [DB::raw('CAST("'.$from.'" AS DATE)'),DB::raw('CAST("'.$to.'" AS DATE)')])
        //         ->distinct()->orderBy('purchased_quantity','DESC');
            
        // }
        if($warehouse_id != null){
            $category->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $category->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $category = $category->get()->toArray();
        // print_r($datefilterRevenue);
        // echo '<br>';
        // echo 'from: '.$from.'<br>';
        // echo 'to: '.$to.'<br>';
        // echo $category;
        // exit;

        $category_name = array_map(function($value){
            return $value['category_name'];
        }, $category);
        $category_name = join(",", $category_name);

        $purchased_quantity = array_map(function($value){
            return $value['purchased_quantity'];
        }, $category);

        $purchased_quantity = join(",", $purchased_quantity);
        $purchased_price = array_map(function($value){
            return $value['purchased_price'];
        }, $category);
        $purchased_price = join(",", $purchased_price);
        $data['category_name'] = $category_name;
        $data['purchased_quantity'] = $purchased_quantity;
        $data['purchased_price'] = $purchased_price;


        $reportrange = $request->get('reportrange');
        if ($reportrange != null || $reportrange != '') {
            $dates = explode('-', $reportrange);
            $another_from = date("Y-m-d", strtotime($dates[0]));
            if (!isset($dates[1])) {
                $another_to =null;
            }else{
                $another_to = date("Y-m-d", strtotime($dates[1]));
            }
        }else{
           $dates = date("Y-m-d");
            $another_from = date("Y-m-d");
            $another_to =  date("Y-m-d");
        }
        $data['another_from'] = $another_from;
        $data['another_to'] = $another_to;
        // dd($data);
       
        // dd($reportrange);
        // $sales = Order::select(
        //     '10_order.order_id',
        //             '10_order.order_date',
        //             '10_order.payment_id',
        //             '10_order.order_status_id',
        //             '10_payment.payment_id',
        //             '10_payment.admin_fee',
        //             '10_payment.grandtotal_payment',
        //             (DB::raw("SUM(grandtotal_payment) as count"))
        //     )
        //     ->where('10_order.order_status_id', 10)
        //     ->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')
        //     ->groupBy(DB::raw("date(order_date)"))
        //     ->orderBy('10_order.order_date', 'ASC');
        //     if($reportrange != null && Str::contains($reportrange,'-') == true){
        //         $sales->whereBetween(DB::raw('CAST(10_order.order_date AS date)'), [DB::raw('CAST("'.$another_from.'" AS DATE)'),DB::raw('CAST("'.$another_to.'" AS DATE)')]);
        //     }else{
        //         $sales->where(DB::raw('CAST(10_order.order_date AS DATE)'),date('Y-m-d'));
        //     }

        //     $sales = $sales->get()->toArray();
        $sales = Order::select(
                    '10_order.order_id',
                    '10_order.order_date',
                    '10_order.order_status_id',
                    '10_order_detail.quantity',
                    '10_order_detail.price',
                    (DB::raw("SUM(10_order_detail.quantity * 10_order_detail.price ) as count"))
            )
            ->where('10_order.order_status_id', 10)
            // ->leftJoin('10_payment','10_payment.payment_id','=','10_order.payment_id')
            ->leftJoin('10_order_detail', '10_order_detail.order_id', '=', '10_order.order_id')
            ->groupBy(DB::raw("date(order_date)"))
            ->orderBy('10_order.order_date', 'ASC');
            if($reportrange != null && Str::contains($reportrange,'-') == true){
                $sales->whereBetween(DB::raw('CAST(10_order.order_date AS date)'), [DB::raw('CAST("'.$another_from.'" AS DATE)'),DB::raw('CAST("'.$another_to.'" AS DATE)')]);
            }else{
                $sales->where(DB::raw('CAST(10_order.order_date AS DATE)'),date('Y-m-d'));
            }

            $sales = $sales->get()->toArray();
            // dd($sales);
        $date = array_map(function($value){
            return  date('d-m-Y', strtotime($value['order_date']));
        }, $sales);
        // dd($date);
        
        $asdd = join(",", $date);
        $test = "";
        $testcount = array_key_last ( $date );
        foreach($date as $key=>$mo){
            $test .= '"';
            $test .= date('d-m-Y', strtotime($mo));
            $test .= '"';
            if($key != $testcount){
                $test .= ",";
            }
        }
        $data['bener'] = $test;
        // $asd = join(",", $date);
        
        $count = array_map(function($value){
            return $value['count'];
        }, $sales);
        $count = join(",", $count);

        $array_sales = array();
        $array_dates = array();
        // if ($another_from = null) {
        //     $begin = date("Y-m-d");
        // }
        $begin = new DateTime($another_from);
        $end = new DateTime($another_to);
        $end = $end->modify( '+1 day' );
        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
        foreach($daterange as $date){
            array_push($array_dates, $date->format("d-m-Y"));
        }
        // dd($array_dates);
        foreach($array_dates as $date){
            $total = 0;
            foreach($sales as $row){
                $order_date = date('d-m-Y', strtotime($row['order_date']));
                if($date == $order_date){
                    $total = $row['count'];
                    break ;
                }else{
                    $total = 0;
                }
            }
            array_push($array_sales, $total);

        }
        $data['date'] = "";
        $data['count'] = $count;
        $data['sales'] = $array_sales;
        $data['dates'] = $array_dates;
// dd($array_dates);
        $data_pesanan = Order::join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->distinct('10_order.order_id')->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_id DESC LIMIT 1)'),[1])->where('10_order.order_status_id',6)->orderBy('10_preparation.preparation_id','ASC')->where('is_confirmed',0)->whereIn('is_printed_label',[0,1]);
        // if($user['organization_type_id'] == 2){
        //     $organization_id = $user['organization_id'];
        //     // $data_pesanan->whereIn('00_kelurahan_desa.kode_pos', function($query) use($organization_id){
        //     //     $query->select('kd.kode_pos')->from('00_kelurahan_desa AS kd')->join('00_mapping_coverage','00_mapping_coverage.kelurahan_desa_id','=','kd.kelurahan_desa_id')->whereColumn('kd.kode_pos','00_kelurahan_desa.kode_pos')->where('00_mapping_coverage.organization_id', $organization_id)->whereNotNull('00_address.customer_id')->orderBy('00_mapping_coverage.created_date');
        //     // });
        // }

        // if($warehouse_id != null){
        //     $data_pesanan->where('10_order.warehouse_id', $warehouse_id)->where('10_order.agent_user_id',$agent_id);
        // }
        if($warehouse_id != null){
            $data_pesanan->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $data_pesanan->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $newOrder = $data_pesanan->count();
        $data_picking = Order::join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->distinct('10_order.order_id')->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_id DESC LIMIT 1)'),[1])->where('10_order.order_status_id',6)->orderBy('10_preparation.preparation_id','ASC')->where('is_confirmed',1)->where('is_printed_label','=',2);
        // $warehouse_id = $user['warehouse_id'];

        if($warehouse_id != null){
            $data_picking->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $data_picking->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $picking = $data_picking->count();

        $data_packing = Order::join('10_order_detail','10_order_detail.order_id','=','10_order.order_id')->distinct('10_order.order_id')->leftJoin('00_order_status','00_order_status.order_status_id','=','10_order.order_status_id')->leftJoin('00_payment_method','00_payment_method.payment_method_id','=','10_order.payment_method_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id')->join('00_preparation_status','00_preparation_status.preparation_status_id','=','10_preparation.preparation_status_id')->join('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[3])->where('10_order.order_status_id','7');
        // $warehouse_id = $user['warehouse_id'];
        if($warehouse_id != null){
            $data_packing->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $data_packing->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $packing = $data_packing->count();

        $dataShipping = Order::select('10_order.order_id')->distinct()->join('00_shipment_method','00_shipment_method.shipment_method_id','=','10_order.shipment_method_id')->where('order_status_id',7)->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id');
        $dataShippingEcommerce = Order::select('10_order.order_id')->distinct()->join('00_shipment_method_ecommerce','00_shipment_method_ecommerce.shipment_method_ecom_id','=','10_order.shipment_method_id')->where('order_status_id',7)->whereIn(DB::raw('(SELECT 10_preparation.preparation_status_id FROM 10_preparation WHERE 10_preparation.order_id = 10_order.order_id ORDER BY 10_preparation.preparation_status_id DESC LIMIT 1)'),[4])->leftJoin('00_address','00_address.address_id','=','10_order.destination_address')->leftJoin('00_provinsi','00_provinsi.provinsi_id','=','00_address.provinsi_id')->leftJoin('00_kabupaten_kota','00_kabupaten_kota.kabupaten_kota_id','=','00_address.kabupaten_kota_id')->leftJoin('00_invoice_status','00_invoice_status.invoice_status_id','=','10_order.invoice_status_id')->leftJoin('00_kecamatan','00_kecamatan.kecamatan_id','=','00_address.kecamatan_id')->join('10_preparation','10_preparation.order_id','=','10_order.order_id')->leftjoin('00_organization','00_organization.organization_id','=','10_order.agent_user_id')
        ->leftjoin('00_organization_type','00_organization_type.organization_type_id','=','00_organization.organization_type_id')->where('00_organization_type.organization_type_id',4)->leftJoin('00_kelurahan_desa','00_kelurahan_desa.kelurahan_desa_id','=','00_address.kelurahan_desa_id');
        // $warehouse_id = $user['warehouse_id'];
        if($warehouse_id != null){
            $dataShipping->where('10_order.warehouse_id', $warehouse_id);
        }
        if($organization_type_id != 1){
            $dataShipping->where('10_order.agent_user_id',$agent_id)->orWhere('10_order.agent_user_id',$parent_agent_id);
        }
        $dataShipping->union($dataShippingEcommerce);
        $shipping = $dataShipping->count();
        $datapengaduan = ComplaintStatus::select('00_issue_status.issue_status_id','00_issue_status.issue_status_name',
        DB::raw('COUNT(00_issue.issue_status_id) as jumlah'))
        ->leftJoin('00_issue','00_issue.issue_status_id','=','00_issue_status.issue_status_id')
        ->groupBy('00_issue_status.issue_status_id');

        $pengaduan = $datapengaduan->get();

        $data['count_new_order'] = $newOrder;
        $data['count_picking'] = $picking;
        $data['count_packing'] = $packing;
        $data['count_shipping'] = $shipping;
        $data['pengaduan'] = $pengaduan;
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        $data['reportrange'] = $reportrange;
        // dd($reportrange);
        // dd($monyet);
        
        // $test .= "";
        // foreach((array) $test as $wew){
        //     $sampah = $wew;
        // }
        // dd($sampah);
        // dd($test);
        // $data['anjing'] = $test;
        return view('admin::dashboard', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getNotifications() {
        $customer = Notification::join('00_customer', '00_customer.customer_id', '=', '00_notification.customer_id')
        ->select('00_notification.notification_id', 'customer_name', '00_notification.message', '00_notification.answer', '00_notification.type', '00_notification.read_by_cms', '00_notification.active_flag as activeFlag', '00_notification.created_date as createdDate')
        ->where('read_by_cms', '=', 0)
        ->whereIn('00_notification.type',['new_order','complaint'])
        ->where('00_notification.active_flag', '=', 1);

        $user = Notification::join('98_user', '98_user.user_id', '=', '00_notification.user_id')
        ->select('00_notification.notification_id', 'user_name AS customer_name', '00_notification.message', '00_notification.answer', '00_notification.type', '00_notification.read_by_cms', '00_notification.active_flag as activeFlag', '00_notification.created_date as createdDate')
        ->where('read_by_cms', '=', 0)
        ->whereIn('00_notification.type',['new_order','complaint'])
        ->where('00_notification.active_flag', '=', 1);

        $list = $user->unionAll($customer)
        // ->orderBy('created_date','asc')
        ->get()->toArray();

        usort($list, function($a, $b) {
            return strtotime($b['createdDate']) - strtotime($a['createdDate']);
        });
        echo json_encode($list);
    }
}
