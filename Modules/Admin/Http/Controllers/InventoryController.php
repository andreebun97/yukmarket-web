<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Session;
use App\Inventory;
use App\StockCard;
use App\Product;
use App\Category;
use App\Warehouse;
use App\ProductWarehouse;
use App\TransactionHeader;
use App\TransactionDetail;
use App\WarehouseMutationHeader;
use App\WarehouseMutationDetail;
use App\InputOpname;
use App\User;
use App\Helpers\UomConvert;
use App\Helpers\Currency;
use App\UOM;
use App;
use Lang;
use DB;
use Config;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\InventoryImport;

class InventoryController extends Controller
{
    public function __construct($currency = null){
        App::setLocale('in');
        $this->middleware('admin_auth');
        $currency = new Currency();
        $this->currency = $currency;
    }

    // BARANG MASUK
        public function index()
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "BM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/index', $data);
        }

        public function stockTransfer(){
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "MT".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
                // print_r($wh_product);
                // exit;
            // PRODUCT WAREHOUSE

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/stock_transfer', $data);
        }

        public function mutationWarehouse(){
            $user = Session::get('users');
            $data['user'] = $user;
            $all_menu = new MenuController();
            $accessed_menu = $all_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $all_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "MT".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
                // print_r($wh_product);
                // exit;
            // PRODUCT WAREHOUSE

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/mutation_warehouse', $data);
        }

        public function getChildrenWarehouse(Request $request){
            $warehouse_type = 1;
            if($request->input('parent_warehouse_type') == 1){
                $warehouse_type = 2;
            }
            $data = Warehouse::where(['active_flag' => 1,'warehouse_type_id' => $warehouse_type])->get();

            return response()->json(array('data' => $data));
        }

        public function getWarehouseInventory(Request $request){
            $inventory = Inventory::select('00_product.prod_id','00_product.prod_name','00_product.prod_code','00_warehouse.warehouse_id','00_warehouse.warehouse_name','00_product.uom_value','00_uom.uom_id','00_uom.uom_name','00_product.product_sku_id','00_inventory.stock','00_inventory.inventory_price')->join('00_product','00_product.prod_id','=','00_inventory.prod_id')->join('00_warehouse','00_warehouse.warehouse_id','=','00_inventory.warehouse_id')->join('00_uom','00_uom.uom_id','=','00_product.uom_id')->where('00_warehouse.warehouse_id',$request->input('warehouse_id'))->where('00_inventory.stock','>','0')->get();

            $warehouse = Warehouse::select('00_warehouse.*')->where('00_warehouse.warehouse_id',function($query) use($request){
                $query->select('wa.parent_warehouse_id')->from('00_warehouse AS wa')->where('wa.warehouse_id',$request->input('warehouse_id'));
            })->get();

            return response()->json(array('data' => array('inventory' => $inventory, 'warehouse' => $warehouse)));
        }

        public function createStockTransfer(){
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "WM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE


            $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');
            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id')->get();
            $data['list_uom'] = $uom;

            $data['all_category'] = Category::where('active_flag',1)->whereNull('parent_category_id')->get();
            $data['inv_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->get();
            $data['sales_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->where('warehouse_type_id',2)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/stock_transfer_form', $data);
        }

        public function createMutationWarehouse(){
            $user = Session::get('users');
            $data['user'] = $user;
            $all_menu = new MenuController();
            $accessed_menu = $all_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $all_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "WM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE


            $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');
            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id')->get();
            $data['list_uom'] = $uom;

            $data['all_category'] = Category::where('active_flag',1)->whereNull('parent_category_id')->get();
            $data['inv_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->get();
            $data['sales_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->where('warehouse_type_id',2)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/mutation_warehouse_form', $data);
        }

        public function stockTransferHistory(Request $request){
            if($request->get('id') == null){
                return redirect('admin/inventory/stock_transfer');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // // USER ORGANIZATION & WAREHOUSE
            //     $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
            //                     ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
            //                     ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
            //                     ->where('98_user.user_id',$user['id'])
            //                     ->first();
            //     $data['user_wh_org'] = $user_wh_org;
            // // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "WM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            $warehouse_mutation_header = WarehouseMutationHeader::select('10_warehouse_mutation_header.*', 'wh_from.warehouse_name as wh_from_name', 'wh_to.warehouse_name as wh_to_name')
                                        ->leftJoin('00_warehouse as wh_from','wh_from.warehouse_id','=','10_warehouse_mutation_header.warehouse_id')
                                        ->leftJoin('00_warehouse as wh_to','wh_to.warehouse_id','=','10_warehouse_mutation_header.destination_warehouse_id')
                                        ->where('10_warehouse_mutation_header.mutation_header_id', $request->get('id'))
                                        ->first();
            $data['warehouse_mutation_header'] = $warehouse_mutation_header;

            $warehouse_mutation_detail = WarehouseMutationDetail::select('10_warehouse_mutation_detail.prod_id',
                                            'prod_from.prod_code as prod_from_code',
                                            'prod_from.prod_name as prod_from_name',
                                            'prod_to.prod_code as prod_to_code',
                                            'prod_to.prod_name as prod_to_name',
                                            '10_warehouse_mutation_detail.stock',
                                            '10_warehouse_mutation_detail.destination_stock',
                                            // '00_uom.uom_id',
                                            'uom_from.uom_name as uom_from_name',
                                            'uom_to.uom_name as uom_to_name',
                                            '10_warehouse_mutation_detail.price')
                                        ->leftJoin('00_product as prod_from','prod_from.prod_id','=','10_warehouse_mutation_detail.prod_id')
                                        ->leftJoin('00_product as prod_to','prod_to.prod_id','=','10_warehouse_mutation_detail.destination_prod_id')
                                        ->leftJoin('00_uom as uom_from','uom_from.uom_id','=','10_warehouse_mutation_detail.stock_uom')
                                        ->leftJoin('00_uom as uom_to','uom_to.uom_id','=','10_warehouse_mutation_detail.destination_stock_uom')
                                        ->where('10_warehouse_mutation_detail.mutation_header_id', $request->get('id'))
                                        ->get();
            // dd($warehouse_mutation_detail);
            $data['warehouse_mutation_detail'] = $warehouse_mutation_detail;
            $data['currency'] = $this->currency;

            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/stock_transfer_history', $data);
        }

        public function mutationWarehouserHistory(Request $request){
            if($request->get('id') == null){
                return redirect('admin/inventory/stock_transfer/get');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $all_menu = new MenuController();
            $accessed_menu = $all_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $all_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // // USER ORGANIZATION & WAREHOUSE
            //     $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
            //                     ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
            //                     ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
            //                     ->where('98_user.user_id',$user['id'])
            //                     ->first();
            //     $data['user_wh_org'] = $user_wh_org;
            // // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "WM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // // PRODUCT WAREHOUSE
            //     if ($user_wh_org->warehouse_id != null) {
            //         $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
            //                                 ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
            //                                 ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
            //                                 ->whereNull('00_product.parent_prod_id')
            //                                 ->where('00_product.active_flag',1)
            //                                 ->get();
            //     } else {
            //         $wh_product = null;
            //     }
            //     $data['wh_product'] = $wh_product;
            // // PRODUCT WAREHOUSE

            // $data['all_category'] = Category::where('active_flag',1)->whereNull('parent_category_id')->get();
            // $data['inv_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->where('warehouse_type_id',1)->get();
            // $data['sales_warehouse'] = Warehouse::select('00_warehouse.*')->join('00_address','00_address.address_id','=','00_warehouse.address_id')->where('00_warehouse.active_flag',1)->where('warehouse_type_id',2)->get();
            $warehouse_mutation_header = WarehouseMutationHeader::select('10_warehouse_mutation_header.*')->where('10_warehouse_mutation_header.mutation_header_id', $request->get('id'))->first();
            $warehouse_mutation_detail = WarehouseMutationDetail::select('10_warehouse_mutation_detail.prod_id','00_product.prod_code','00_product.prod_name','10_warehouse_mutation_detail.stock','00_uom.uom_id','00_uom.uom_name','10_warehouse_mutation_detail.price')->leftJoin('00_product','00_product.prod_id','=','10_warehouse_mutation_detail.prod_id')->leftJoin('00_uom','00_uom.uom_id','=','10_warehouse_mutation_detail.uom_id')->where('10_warehouse_mutation_detail.mutation_header_id', $request->get('id'))->get();
            $inventory_warehouse = Warehouse::select('00_warehouse.*')->from('00_warehouse')->where('00_warehouse.warehouse_id', function($query) use($request){
                $query->select('10_warehouse_mutation_header.warehouse_id')->from('10_warehouse_mutation_header')->where('10_warehouse_mutation_header.mutation_header_id', $request->input('id'));
            })->first();
            $sales_warehouse = Warehouse::select('00_warehouse.*')->from('00_warehouse')->where('00_warehouse.warehouse_id', function($query) use($request){
                $query->select('10_warehouse_mutation_header.parent_warehouse_mutation_id')->from('10_warehouse_mutation_header')->where('10_warehouse_mutation_header.mutation_header_id', $request->input('id'));
            })->first();
            $data['inventory_warehouse'] = $inventory_warehouse;
            $data['sales_warehouse'] = $sales_warehouse;
            $data['warehouse_mutation_header'] = $warehouse_mutation_header;
            $data['warehouse_mutation_detail'] = $warehouse_mutation_detail;
            $data['currency'] = $this->currency;
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/stock_transfer_form', $data);
        }

        public function getIncomingGoods(Request $request) {
            $code_url = request()->segment(1);
            // $wh_id = $wh_id == "null" ? "" : $wh_id;
            // $category_id = $category_id == "null" ? "" : $category_id;
            $user = Session::get('users');
            $data['user'] = $user;
            // USER ORGANIZATION & WAREHOUSE
            $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
            ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
            ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
            ->where('98_user.user_id',$user['id'])
            ->first();
            $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE
            if($user['warehouse_id'] == null ){
                $warehouse_id = null;
                $organization_id = $user_wh_org->organization_id;
            }else{
                $warehouse_id = $user_wh_org->warehouse_id;
                $organization_id = $user_wh_org->organization_id;
            }
            $draw = $request->get('draw');
            $start = $request->has('start') ? $request->get('start') : 0;
            $length = $request->has('length') ? $request->get('length') : 10;
            $search = $request->has('search') ? $request->get('search')['value'] : '';
            $columns = $this->columns;
            $columns[count($columns) - 1] = DB::raw("(CASE WHEN 10_transaction_header.transaction_status = 0 THEN 'Tunda' ELSE 'Selesai' END)");

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            if ($order_column == 0) {
                $column_name = "10_transaction_header.updated_date";
                $order_type = "DESC";
            } elseif ($order_column == 1) {
                $column_name = "10_transaction_header.transaction_header_code";
            } else {
                $column_name = $request->get('columns')[$order_column]['data'];
            }

            $total_data = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code',
                            '10_transaction_header.transaction_date',
                            '10_transaction_header.transaction_desc',
                            '10_transaction_header.transaction_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',2)
                            ->when($organization_id, function ($query, $organization_id) {
                                return $query->where('10_transaction_header.organization_id', $organization_id);
                            })
                            ->when($warehouse_id, function ($query, $warehouse_id) {
                                return $query->where('10_transaction_header.warehouse_id', $warehouse_id);
                            })
                            ->where(function($query) use($columns, $search){
                                for ($b=0; $b < count($columns); $b++) {
                                    if($b > 0){
                                        $query->orWhere($columns[$b],'like','%'.$search.'%');
                                    }else{
                                        $query->where($columns[$b],'like','%'.$search.'%');
                                    }
                                }
                            })
                            ->count();

            $query = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code',
                            '10_transaction_header.transaction_date',
                            '10_transaction_header.transaction_desc',
                            '10_transaction_header.transaction_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',2)
                            ->when($organization_id, function ($query, $organization_id) {
                                return $query->where('10_transaction_header.organization_id', $organization_id);
                            })
                            ->when($warehouse_id, function ($query, $warehouse_id) {
                                return $query->where('10_transaction_header.warehouse_id', $warehouse_id);
                            })
                            ->where(function($query) use($columns, $search){
                                for ($b=0; $b < count($columns); $b++) {
                                    if($b > 0){
                                        $query->orWhere($columns[$b],'like','%'.$search.'%');
                                    }else{
                                        $query->where($columns[$b],'like','%'.$search.'%');
                                    }
                                }
                            })
                            ->limit($length)
                            ->offset($start)
                            ->orderBy($column_name,$order_type)
                            ->get();
            $array = array();
            $no = $start + 1;
            $url_edit = url('/admin/inventory/edit');
            $url_history = url('/admin/inventory/history');
            foreach ($query as $row):
                $url = $url_edit."/".$row->transaction_header_id;
                $url_h = $url_history."/".$row->transaction_header_id;
                if ($row->transaction_status < 1) {
                    $action = '<ul class="ico-block">';
                    $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a>';
                    $action .= '<a class="loader-trigger" href="'.$url.'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a></li>';
                    $action .= '</ul>';
                } else {
                    $action = '<ul class="ico-block">';
                    $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                    $action .= '</ul>';
                }
                $data_object = (object) array(
                    'no' => $no++,
                    'trx_no' => $row->transaction_header_code,
                    'trx_date' => date("Y-m-d", strtotime($row->transaction_date)),
                    'warehouse_name' => $row->warehouse_name,
                    'trx_status' => $row->transaction_status > 0 ? "Selesai" : "Tunda",
                    'trx_desc' => $row->transaction_desc,
                    'action' => $action
                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $draw,
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );

            echo json_encode($data);
        }
        public function shows(Request $request) {
            $parameter = array(
                'offset' => $request->has('start') ? $request->get('start') : 0,
                'limit' => $request->has('length') ? $request->get('length') : 10,
                'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
                'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc',
                'search' => $request->has('search') ? $request->get('search')['value'] : ''
            );

            $data = array(
                'draw' => $request->get('draw'),
                'recordsTotal' => $this->getTotal($parameter),
                'recordsFiltered' => $this->getTotal($parameter),
                'data' => $this->getData($parameter)
            );
            return response()->json($data, 200);
        }

        public function getStockMutation(Request $request) {
            $mutation_columns = array (
                '10_transaction_header.transaction_header_code',
                '10_transaction_header.transaction_date',
                '10_transaction_header.transaction_desc',
                '10_transaction_header.transaction_status',
                '00_warehouse.warehouse_name'
            );
            $parameter = array(
                'offset' => $request->has('start') ? $request->get('start') : 0,
                'limit' => $request->has('length') ? $request->get('length') : 10,
                'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
                'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc',
                'search' => $request->has('search') ? $request->get('search')['value'] : ''
            );

            $code_url = request()->segment(1);
            $user = Session::get('users');
            $data['user'] = $user;

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            if ($order_column == 0) {
                $column_name = "10_transaction_header.transaction_header_id";
                $order_type = "DESC";
            } else {
                $column_name = $request->get('columns')[$order_column]['data'];
            }
            $total_data = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code',
                            '10_transaction_header.transaction_date',
                            '10_transaction_header.transaction_desc',
                            '10_transaction_header.transaction_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',6)
                            ->where(function($query) use ($mutation_columns, $parameter){
                                for ($k=0; $k < count($mutation_columns); $k++) {
                                    if($k > 0){
                                        $query->orWhere($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                    }else{
                                        $query->where($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                    }
                                }
                            })
                            ->count();

            $query = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code as trx_no',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_desc as trx_desc',
                            '10_transaction_header.transaction_status as trx_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',6)
                            ->offset($parameter['offset'])
                            ->limit($parameter['limit'])
                            ->where(function($query) use ($mutation_columns, $parameter){
                                for ($k=0; $k < count($mutation_columns); $k++) {
                                    if($k > 0){
                                        $query->orWhere($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                    }else{
                                        $query->where($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                    }
                                }
                            })
                            ->orderBy($column_name,$order_type)
                            ->get();

            $array = array();
            $no = $parameter['offset'] + 1;
            $url_edit = url('/admin/inventory/stock/mutation/edit');
            $url_history = url('/admin/inventory/stock/mutation/history');
            foreach ($query as $row):
                $url = $url_edit."/".$row->transaction_header_id;
                $url_h = $url_history."/".$row->transaction_header_id;
                if ($row->transaction_status < 1) {
                    $action = '<ul class="ico-block">';
                    $action .= '<li><a class="loader-trigger" href="'.$url.'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>';
                    $action .= '<a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                    $action .= '</ul>';
                } else {
                    $action = '<ul class="ico-block">';
                    $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                    $action .= '</ul>';
                }
                $data_object = (object) array(
                    'no' => $no++,
                    'trx_no' => $row->trx_no,
                    'trx_date' => date("Y-m-d", strtotime($row->trx_date)),
                    'warehouse_name' => $row->warehouse_name,
                    'trx_status' => $row->trx_status > 0 ? "Selesai" : "Tunda",
                    'trx_desc' => $row->trx_desc,
                    'action' => $action
                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $request->get('draw'),
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );
            echo json_encode($data);
        }



        public function getStockTransfer(Request $request){
            $code_url = request()->segment(1);
            // $wh_id = $wh_id == "null" ? "" : $wh_id;
            // $category_id = $category_id == "null" ? "" : $category_id;
            $user = Session::get('users');
            $data['user'] = $user;

            $draw = $request->get('draw');
            $start = $request->has('start') ? $request->get('start') : 0;
            $length = $request->has('length') ? $request->get('length') : 10;
            $search = $request->has('search') ? $request->get('search')['value'] : '';

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            // if ($order_column == 0) {
            //     $column_name = "holiday_date";
            //     $order_type = "ASC";
            // } elseif ($order_column == 1) {
            //     $column_name = "holiday_name";
            // } else {
            //     $column_name = $request->get('columns')[$order_column]['data'];
            // }

            $total_data = WarehouseMutationHeader::select('10_warehouse_mutation_header.mutation_header_id',
                            '10_warehouse_mutation_header.mutation_header_code',
                            '10_warehouse_mutation_header.mutation_date',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_warehouse_mutation_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where(function ($query) use ($search) {
                                $query->where('10_warehouse_mutation_header.mutation_header_code', 'like',"%$search%")
                                      ->orWhere('00_warehouse.warehouse_name', 'like',"%$search%");
                            })
                            ->where('10_warehouse_mutation_header.active_flag',1)
                            ->count();

            $query = WarehouseMutationHeader::select('10_warehouse_mutation_header.mutation_header_id',
            '10_warehouse_mutation_header.mutation_header_code',
            '10_warehouse_mutation_header.mutation_date',
            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_warehouse_mutation_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where(function ($query) use ($search) {
                                $query->where('10_warehouse_mutation_header.mutation_header_code', 'like',"%$search%")
                                      ->orWhere('00_warehouse.warehouse_name', 'like',"%$search%");
                            })
                            ->where('10_warehouse_mutation_header.active_flag',1)
                            ->limit($length)
                            ->offset($start)
                            ->get();

            $array = array();
            $no = $start + 1;
            // $url_edit = url('/admin/inventory/stock/mutation/edit');
            $url_history = url('/admin/inventory/stock_transfer/history');
            foreach ($query as $row):
                // $url = $url_edit."/?id=".$row->mutation_header_id;
                $url_h = $url_history."/?id=".$row->mutation_header_id;
                // if ($row->transaction_status < 1) {
                //     $action = '<ul class="ico-block">';
                //     $action .= '<li><a class="loader-trigger" href="'.$url.'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>';
                //     $action .= '<a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                //     $action .= '</ul>';
                // } else {
                //     $action = '<ul class="ico-block">';
                //     $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                //     $action .= '</ul>';
                // }
                $action = '<ul class="ico-block">';
                $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                $action .= '</ul>';

                $data_object = (object) array(
                    'no' => $no++,
                    'mutation_header_code' => $row->mutation_header_code,
                    'mutation_date' => date("Y-m-d", strtotime($row->mutation_date)),
                    'warehouse_name' => $row->warehouse_name,
                    'action' => $action
                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $draw,
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );

            echo json_encode($data);
        }

        public function getWarehouseMutation(Request $request){
            $code_url = request()->segment(1);
            // $wh_id = $wh_id == "null" ? "" : $wh_id;
            // $category_id = $category_id == "null" ? "" : $category_id;
            $user = Session::get('users');
            $data['user'] = $user;

            $draw = $request->get('draw');
            $start = $request->has('start') ? $request->get('start') : 0;
            $length = $request->has('length') ? $request->get('length') : 10;
            $search = $request->has('search') ? $request->get('search')['value'] : '';

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            // if ($order_column == 0) {
            //     $column_name = "holiday_date";
            //     $order_type = "ASC";
            // } elseif ($order_column == 1) {
            //     $column_name = "holiday_name";
            // } else {
            //     $column_name = $request->get('columns')[$order_column]['data'];
            // }

            $total_data = WarehouseMutationHeader::select('10_warehouse_mutation_header.mutation_header_id',
                            '10_warehouse_mutation_header.mutation_header_code',
                            '10_warehouse_mutation_header.mutation_date',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_warehouse_mutation_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_warehouse_mutation_header.active_flag',1)
                            ->count();

            $query = WarehouseMutationHeader::select('10_warehouse_mutation_header.mutation_header_id',
            '10_warehouse_mutation_header.mutation_header_code',
            '10_warehouse_mutation_header.mutation_date',
            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_warehouse_mutation_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_warehouse_mutation_header.active_flag',1)
                            ->limit($length)
                            ->offset($start)
                            ->get();

            $array = array();
            $no = $start + 1;
            // $url_edit = url('/admin/inventory/stock/mutation/edit');
            $url_history = url('/admin/inventory/stock_transfer/history');
            foreach ($query as $row):
                // $url = $url_edit."/?id=".$row->mutation_header_id;
                $url_h = $url_history."/?id=".$row->mutation_header_id;
                // if ($row->transaction_status < 1) {
                //     $action = '<ul class="ico-block">';
                //     $action .= '<li><a class="loader-trigger" href="'.$url.'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a>';
                //     $action .= '<a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                //     $action .= '</ul>';
                // } else {
                //     $action = '<ul class="ico-block">';
                //     $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                //     $action .= '</ul>';
                // }
                $action = '<ul class="ico-block">';
                $action .= '<li><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye font-size-18"></i></a></li>';
                $action .= '</ul>';

                $data_object = (object) array(
                    'no' => $no++,
                    'mutation_header_code' => $row->mutation_header_code,
                    'mutation_date' => date("Y-m-d", strtotime($row->mutation_date)),
                    'warehouse_name' => $row->warehouse_name,
                    'action' => $action
                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $draw,
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );

            echo json_encode($data);
        }

        public function create(Request $request)
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $org_id = $user['organization_id'];
                $data_org = DB::table('00_organization as a')->where('a.organization_id', $org_id)->first();
                if (isset($data_org->parent_organization_id)) {
                    $org_id = $data_org->parent_organization_id;
                }
                $data['org_id'] = $org_id;

                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "BM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $first_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');

            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');

            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id');

            $data['satuan'] = $uom->get();

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/create', $data);
        }

        public function edit(Request $request,$id)
        {
            if($id == null){
                return redirect('admin/inventory');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id as trx_id',
                            '10_transaction_header.transaction_header_code as trx_code',
                            '10_transaction_header.organization_id',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_desc as trx_desc',
                            '10_transaction_header.transaction_status as trx_status',
                            '00_warehouse.warehouse_id',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->first();

            $trx_detail = TransactionDetail::select('10_transaction_detail.prod_id',
                            '10_transaction_detail.price',
                            '10_transaction_detail.stock',
                            '00_product.prod_code',
                            '10_transaction_detail.uom_id',
                            '00_product.uom_value',
                            '00_uom.uom_name'
                            )
                            ->leftJoin('00_product','10_transaction_detail.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                            ->where('10_transaction_detail.transaction_header_id',$id)
                            ->get();
            $data['trx_header'] = $trx_header;
            $data['trx_detail'] = $trx_detail;

            // PRODUCT WAREHOUSE
                if ($trx_header->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                            ->where('00_product_warehouse.warehouse_id',$trx_header->warehouse_id)
                                            // ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.stockable_flag',1)
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');

            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id');

            $data['satuan'] = $uom->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/edit', $data);
        }

        public function mutation(){
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "MT".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->limit($length)
                                            ->offset($start)
                                            ->orderBy($column_name,$order_type)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
                // print_r($wh_product);
                // exit;
            // PRODUCT WAREHOUSE

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/mutation', $data);
        }

        public function createStockMutation(Request $request)
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $uom = UOM::where('active_flag','=',1)->get();
            $data['satuan'] = $uom;
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "MT".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
             $draw = $request->get('draw');
             $start = $request->has('start') ? $request->get('start') : 0;
             $length = $request->has('length') ? $request->get('length') : 10;
             $search = $request->has('search') ? $request->get('search')['value'] : '';

                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id','00_uom.uom_name')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNotNull('00_product.parent_prod_id')
                                            ->where('00_product.stockable_flag',1)
                                            ->where('00_product.active_flag',1)
                                            ->whereIn('00_product.parent_prod_id', function($query2){
                                                $query2->select('00_inventory.prod_id')->from('00_inventory')->where('00_inventory.active_flag',1);
                                            })
                                            ->limit($length)
                                            ->offset($start)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/create_mutation_form', $data);
        }

        public function editStockMutation(Request $request,$id)
        {
            if($id == null){
                return redirect('admin/inventory/stock/mutation');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id as trx_id',
                            '10_transaction_header.transaction_header_code as trx_code',
                            '10_transaction_header.organization_id',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_desc as trx_desc',
                            '10_transaction_header.transaction_status as trx_status',
                            '00_warehouse.warehouse_id',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->first();

            $trx_detail = TransactionDetail::select('10_transaction_detail.prod_id',
                            '10_transaction_detail.price',
                            '10_transaction_detail.stock',
                            '00_product.prod_code')
                            ->leftJoin('00_product','10_transaction_detail.prod_id','=','00_product.prod_id')
                            ->where('10_transaction_detail.transaction_header_id',$id)
                            ->get()->toArray();
            $data['trx_header'] = $trx_header;
            $data['trx_detail'] = $trx_detail;
            // print_r($data['trx_detail']);
            // exit;

            // PRODUCT WAREHOUSE
                if ($trx_header->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id','00_product.uom_value','00_product.uom_id','00_uom.uom_name')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                            ->where('00_product_warehouse.warehouse_id',$trx_header->warehouse_id)
                                            ->whereNotNull('00_product.parent_prod_id')
                                            ->where('stockable_flag',1)
                                            ->where('00_product.active_flag',1)
                                            ->whereIn('00_product.parent_prod_id', function($query2){
                                                $query2->select('00_inventory.prod_id')->from('00_inventory')->where('00_inventory.active_flag',1);
                                            })
                                            ->get()->toArray();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
                // print_r($data['wh_product']);
                // exit;
            // PRODUCT WAREHOUSE
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/edit_mutation_form', $data);
        }

        public function stockMutationHistory(Request $request,$id)
        {
            if($id == null){
                return redirect('admin/inventory/stock/mutation');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id as trx_id',
                            '10_transaction_header.transaction_header_code as trx_code',
                            '10_transaction_header.organization_id',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_desc as trx_desc',
                            '10_transaction_header.transaction_status as trx_status',
                            '00_warehouse.warehouse_id',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->first();

            $data['trx_header'] = $trx_header;
            $data['id_detail'] = $id;
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            // PRODUCT WAREHOUSE
                if ($trx_header->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$trx_header->warehouse_id)
                                            // ->where('')
                                            ->whereNotNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/stock_mutation_history', $data);
        }

        public function form(Request $request)
        {
            // dd($request->all());
            // $request->validate([

            // ]);
            // $field = [
            //     'inventory_warehouse' => 'required',
            //     'inventory_product' => 'required',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'required',
            //     'inventory_agent' => 'required',
            //     'inventory_product_code' => 'required',
            //     'inventory_stock_type' => 'required',
            //     'inventory_date' => 'required'
            // ];

            // if($request->input('inventory_stock_type') == null or $request->input('inventory_stock_type') == 0) {
            //     $field['inventory_stock_in'] = 'required';
            //     $field['inventory_stock_min'] = 'required';
            // }

            // $validate = \Validator::make($request->all(), $field,
            // [
            //     'inventory_warehouse' => 'Gudang Wajib Di Isi',
            //     'inventory_product' => 'Nama Produk Wajib Di Isi',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'Berat Wajib Di Isi',
            //     'inventory_agent' => 'ID Agent Wajib Di Isi',
            //     'inventory_product_code' => 'ID Produk Wajib Di Isi',
            //     'inventory_stock_type' => 'Tipe Stok Wajib Di Isi',
            //     'inventory_date' => 'Tanggal Masuk Wajib Di Isi',
            //     'inventory_stock_in' => 'Stok Masuk Wajib Di Isi',
            //     'inventory_stock_min' => 'Stok Minimum Wajib Di Isi'
            // ]);

            // if($validate->fails()){
            //     $res = array();
            //     foreach($validate->errors()->messages() as $key => $validations) {
            //         $res[$key] = $validate->customMessages[$key];
            //     }
            //     $result = array(
            //         'code' => '500',
            //         'status' => 'error',
            //         'messages' => $res
            //     );
            //     // return redirect()->back()->withInput()->withErrors($validator);
            // }else{
            try{
                $user = Session::get('users');
                $inventory_org = $request->input('inventory_org');
                $inventory_trx = $request->input('inventory_trx');
                $inventory_date = date('Y-m-d',strtotime($request->input('inventory_date')));
                $inventory_status = $request->input('inventory_status');
                $inventory_warehouse = $request->input('inventory_warehouse');
                $inventory_warehouse_name = $request->input('inventory_warehouse_name');
                $inventory_desc = $request->input('inventory_desc');
                $inventory_product = $request->input('inventory_product');
                $inventory_price = $request->input('inventory_price');
                $inventory_stock = $request->input('inventory_stock');
                $inventory_uom = $request->input('satuan_produk');
                $inventory_final_stock = $request->input('final_stock');
               // dd($inventory_final_stock);
                // TRANSACTION HEADER
                DB::beginTransaction();
                    $check = TransactionHeader::where('transaction_header_code', $inventory_trx)->first();
                    if (!$check) {
                        $trx_id = TransactionHeader::insertGetId([
                            'transaction_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            // 'supplier_id' => $supplier_id,
                            'transaction_type_id' => 2,
                            'transaction_status' => $inventory_status,
                            'transaction_date' => $inventory_date,
                            'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now()),
                            'updated_date' => date(now())
                        ]);
                    } else  {
                        $trx_id = $check->transaction_header_id;
                        $updated_obj = [
                            'transaction_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            // 'supplier_id' => $supplier_id,
                            'transaction_type_id' => 2,
                            'transaction_status' => $inventory_status,
                            'transaction_date' => $inventory_date,
                            'transaction_desc' => $inventory_desc,
                            'updated_by' => $user['id'],
                            'updated_date' => date(now())
                        ];
                        TransactionHeader::where('transaction_header_id', $trx_id)->update($updated_obj);
                        TransactionDetail::where('transaction_header_id', $trx_id)->delete();
                    }
                // TRANSACTION HEADER

                if ($trx_id > 0) {
                    for ($i=0; $i < count($inventory_product) ; $i++) {
                        $prod_id = $inventory_product[$i];
                        $pord_stock = $inventory_stock[$i];
                        $uom_id = $inventory_uom[$i];
                        $final_stok = $inventory_final_stock[$i];
                        $prod_price = str_replace(".", "", $inventory_price[$i]);
                        $prod_price_total = (int)$prod_price * $pord_stock;
                        $new_price_in = $prod_price_total / $final_stok;
                        $trx_detail_id = TransactionDetail::insertGetId([
                            'transaction_header_id' => $trx_id,
                            'prod_id' => $prod_id,
                            'stock' => $pord_stock,
                            'price' => $prod_price,
                            'created_by' => $user['id'],
                            'uom_id' => $uom_id,
                            'created_date' => date(now()),
                            'updated_date' => date(now())
                        ]);
                        $price_stock_card = '';
                        if ($inventory_status == "1") :
                            $check = Inventory::where([
                                                ['organization_id', '=', $inventory_org],
                                                ['warehouse_id', '=', $inventory_warehouse],
                                                ['prod_id', '=', $prod_id],
                                                ['active_flag', '=', 1],
                                            ])
                                            ->first();
                            if (!$check) {
                                $price_stock_card = $new_price_in;
                                // $new_price_total = $prod_price * $final_stok;
                                // $new_price_total = $prod_price_total / $final_stok;
                                $inventory_id = Inventory::insertGetId([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $new_price_in,
                                    'stock' => $final_stok,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now()),
                                    'updated_date' => date(now())
                            ]);
                                $this->validateUpdateStockTokped($prod_id,$final_stok,$inventory_org,$inventory_warehouse);

                            } else {
                                $old_stock = $check->stock;
                                $new_stock = $final_stok + $old_stock;
                                $old_price = $check->inventory_price;
                                // $old_price_total = $old_price * $old_stock;
                                // $new_price_total = $prod_price_total / $final_stok;
                                //$avg_price = ($prod_price_total + $old_price_total)/$new_stock;
                                $avg_price = ($new_price_in + $old_price)/2;
                                $price_stock_card = $avg_price;

                                $inventory_id = $check->inventory_id;
                                $update = Inventory::where('inventory_id', $inventory_id)->update([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $avg_price,
                                    'stock' => $new_stock,
                                    'updated_by' => $user['id'],
                                    'updated_date' => date(now())
                                ]);
                                // dd($new_stock);
                                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                $this->validateUpdateStockTokped($prod_id,$new_stock,$inventory_org,$inventory_warehouse);
                            }

                            $StockCard = StockCard::insertGetId([
                                'stock_card_code' => $inventory_trx,
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $inventory_warehouse,
                                'prod_id' => $prod_id,
                                'stock' => $final_stok,
                                'transaction_type_id' => 2,
                                'price' => $price_stock_card,
                                'total_harga' => $price_stock_card*$final_stok,
                                'transaction_desc' => $inventory_desc,
                                'created_by' => $user['id'],
                                'created_date' => date(now()),
                            ]);
                        endif;
                    }
                    DB::commit();
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    $result = array(
                        'code' => '200',
                        'status' => 'success',
                        'messages' => $alert
                    );
                } else {
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    $result = array(
                        'code' => '500',
                        'status' => 'failed',
                        'messages' => $alert
                    );
                }
            } catch (\Exception $e) {
                $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                $result = array(
                    'code' => '500',
                    'status' => 'failed',
                    'messages' => $alert,
                    'error_messages' => $e->getMessage()
                );
            }
                // Session::flash('message_alert', $alert);
                // return redirect('admin/inventory');
            // }
            return response()->json($result);
        }

        public function mutationForm(Request $request)
        {
            // dd($request);
            // $request->validate([

            // ]);
            // $field = [
            //     'inventory_warehouse' => 'required',
            //     'inventory_product' => 'required',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'required',
            //     'inventory_agent' => 'required',
            //     'inventory_product_code' => 'required',
            //     'inventory_stock_type' => 'required',
            //     'inventory_date' => 'required'
            // ];

            // if($request->input('inventory_stock_type') == null or $request->input('inventory_stock_type') == 0) {
            //     $field['inventory_stock_in'] = 'required';
            //     $field['inventory_stock_min'] = 'required';
            // }

            // $validate = \Validator::make($request->all(), $field,
            // [
            //     'inventory_warehouse' => 'Gudang Wajib Di Isi',
            //     'inventory_product' => 'Nama Produk Wajib Di Isi',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'Berat Wajib Di Isi',
            //     'inventory_agent' => 'ID Agent Wajib Di Isi',
            //     'inventory_product_code' => 'ID Produk Wajib Di Isi',
            //     'inventory_stock_type' => 'Tipe Stok Wajib Di Isi',
            //     'inventory_date' => 'Tanggal Masuk Wajib Di Isi',
            //     'inventory_stock_in' => 'Stok Masuk Wajib Di Isi',
            //     'inventory_stock_min' => 'Stok Minimum Wajib Di Isi'
            // ]);

            // if($validate->fails()){
            //     $res = array();
            //     foreach($validate->errors()->messages() as $key => $validations) {
            //         $res[$key] = $validate->customMessages[$key];
            //     }
            //     $result = array(
            //         'code' => '500',
            //         'status' => 'error',
            //         'messages' => $res
            //     );
            //     // return redirect()->back()->withInput()->withErrors($validator);
            // }else{
                $user = Session::get('users');
                $inventory_org = $request->input('inventory_org');
                $inventory_trx = $request->input('inventory_trx');
                $inventory_date = date('Y-m-d',strtotime($request->input('inventory_date')));
                $inventory_status = $request->input('inventory_status');
                $inventory_warehouse = $request->input('inventory_warehouse');
                $inventory_warehouse_name = $request->input('inventory_warehouse_name');
                $inventory_desc = $request->input('inventory_desc');
                $inventory_product = $request->input('inventory_product');
                $inventory_price = $request->input('inventory_price');
                $inventory_stock = $request->input('inventory_stock');
                // return $inventory_status;
                // TRANSACTION HEADER
                    $check = TransactionHeader::where('transaction_header_code', $inventory_trx)->first();
                    if (!$check) {
                        $trx_id = TransactionHeader::insertGetId([
                            'transaction_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            // 'supplier_id' => $supplier_id,
                            'transaction_type_id' => 6,
                            'transaction_status' => $inventory_status,
                            'transaction_date' => $inventory_date,
                            'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                    } else  {
                        $trx_id = $check->transaction_header_id;
                        $updated_obj = [
                            'transaction_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            // 'supplier_id' => $supplier_id,
                            'transaction_type_id' => 6,
                            'transaction_status' => $inventory_status,
                            'transaction_date' => $inventory_date,
                            'transaction_desc' => $inventory_desc,
                            'updated_by' => $user['id'],
                            'updated_date' => date(now())
                        ];
                        TransactionHeader::where('transaction_header_id', $trx_id)->update($updated_obj);
                        TransactionDetail::where('transaction_header_id', $trx_id)->delete();
                    }
                // TRANSACTION HEADER

                if ($trx_id > 0) {
                    // echo $inventory_status;
                    // exit;
                    for ($i=0; $i < count($inventory_product) ; $i++) {
                        $prod_id = $inventory_product[$i];
                        $pord_stock = $inventory_stock[$i];
                        $prod_price = str_replace(".", "", $inventory_price[$i]);
                        $prod_price_total = (int)$prod_price * $pord_stock;
                        $trx_detail_id = TransactionDetail::insertGetId([
                            'transaction_header_id' => $trx_id,
                            'prod_id' => $prod_id,
                            'stock' => $pord_stock,
                            'price' => $prod_price,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                        if ($inventory_status == "1") :
                            $product = Product::where('prod_id',$prod_id)
                                        ->first();

                            $parent_product = Product::where('prod_id',$product['parent_prod_id'])
                                        ->first();

                            $check = Inventory::where([
                                                // ['organization_id', '=', $inventory_org],
                                                ['warehouse_id', '=', $inventory_warehouse],
                                                ['prod_id', '=', $prod_id],
                                                ['active_flag', '=', 1],
                                            ])
                                            ->first();

                            if (!$check) {

                                // $update = Inventory::where([
                                //     // ['organization_id', '=', $inventory_org],
                                //     ['warehouse_id', '=', $inventory_warehouse],
                                //     ['prod_id', '=', $parent_product_id]
                                // ])
                                // ->update([
                                //     'organization_id' => $inventory_org,
                                //     'warehouse_id' => $inventory_warehouse,
                                //     'prod_id' => $prod_id,
                                //     'inventory_date' => $inventory_date,
                                //     'inventory_price' => $avg_price,
                                //     'stock' => $new_stock,
                                //     'updated_by' => $user['id'],
                                //     'updated_date' => date(now())
                                // ]);
                                $parent_product_id = $product['parent_prod_id'];
                                $parent_detail = $update = Inventory::where([
                                        // ['organization_id', '=', $inventory_org],
                                        ['warehouse_id', '=', $inventory_warehouse],
                                        ['prod_id', '=', $parent_product_id],
                                        ['active_flag','=',1]
                                    ])
                                    ->first();

                                if($parent_detail != null){
                                    $old_price = $parent_detail->inventory_price;
                                    $old_stock = $parent_detail->stock;
                                    $recent_stock = ($product->uom_value * $pord_stock / $parent_product->uom_value);
                                    $old_price_total = $old_price * $old_stock;
                                    $new_stock = $old_stock - $recent_stock;
                                    $avg_price = ($prod_price_total + $old_price_total)/$new_stock;

                                    $update = Inventory::where('inventory_id', $parent_detail['inventory_id'])->update([
                                        'organization_id' => $inventory_org,
                                        'warehouse_id' => $inventory_warehouse,
                                        'prod_id' => $parent_product_id,
                                        'inventory_date' => $inventory_date,
                                        'inventory_price' => $avg_price,
                                        'stock' => $new_stock,
                                        'updated_by' => $user['id'],
                                        'updated_date' => date(now())
                                    ]);
                                    //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                    $this->validateUpdateStockTokped($prod_id,$new_stock,$inventory_org,$inventory_warehouse);

                                }

                                $inventory_id = Inventory::insertGetId([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $prod_price,
                                    'stock' => $pord_stock,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now())
                                ]);

                            } else {
                                $parent_product_id = $product['parent_prod_id'];
                                $parent_detail = $update = Inventory::where([
                                        // ['organization_id', '=', $inventory_org],
                                        ['warehouse_id', '=', $inventory_warehouse],
                                        ['prod_id', '=', $parent_product_id],
                                        ['active_flag','=',1]
                                    ])
                                    ->first();

                                if($parent_detail != null){
                                    $old_price = $parent_detail->inventory_price;
                                    $old_stock = $parent_detail->stock;
                                    $recent_stock = ($product->uom_value * $pord_stock /$parent_product->uom_value);
                                    $old_price_total = $old_price * $old_stock;
                                    $new_stock = $old_stock - $recent_stock;
                                    $avg_price = ($prod_price_total + $old_price_total)/$new_stock;

                                    $update = Inventory::where('inventory_id', $parent_detail['inventory_id'])->update([
                                        'organization_id' => $inventory_org,
                                        'warehouse_id' => $inventory_warehouse,
                                        'prod_id' => $parent_product_id,
                                        'inventory_date' => $inventory_date,
                                        'inventory_price' => $avg_price,
                                        'stock' => $new_stock,
                                        'updated_by' => $user['id'],
                                        'updated_date' => date(now())
                                    ]);
                                    //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                $this->validateUpdateStockTokped($prod_id,$new_stock,$inventory_org,$inventory_warehouse);
                                }

                                $old_price = $check->inventory_price;
                                $old_stock = $check->stock;
                                $old_price_total = $old_price * $old_stock;
                                $new_stock = $old_stock + $pord_stock;
                                $avg_price = ($prod_price_total + $old_price_total)/$new_stock;

                                $inventory_id = $check->inventory_id;
                                $update = Inventory::where('inventory_id', $inventory_id)->update([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $avg_price,
                                    'stock' => $new_stock,
                                    'updated_by' => $user['id'],
                                    'updated_date' => date(now())
                                ]);
                                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                $this->validateUpdateStockTokped($prod_id,$new_stock,$inventory_org,$inventory_warehouse);
                            }

                            if($product['parent_prod_id'] != null){
                                $recent_stock = ROUND($product['uom_value'] * $pord_stock / $parent_product['uom_value']);
                                $StockCard = StockCard::insertGetId([
                                    'stock_card_code' => $inventory_trx,
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $product['parent_prod_id'],
                                    'stock' => "-".$recent_stock,
                                    'transaction_type_id' => 2,
                                    'price' => $prod_price,
                                    'total_harga' => "-".($recent_stock * $prod_price),
                                    // 'transaction_desc' => $inventory_desc,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now())
                                ]);

                                $StockCard = StockCard::insertGetId([
                                    'stock_card_code' => $inventory_trx,
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'stock' => $pord_stock,
                                    'transaction_type_id' => 2,
                                    'price' => $prod_price,
                                    'total_harga' => $prod_price_total,
                                    // 'transaction_desc' => $inventory_desc,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now())
                                ]);
                            }else{
                                $StockCard = StockCard::insertGetId([
                                    'stock_card_code' => $inventory_trx,
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    'stock' => "-".$pord_stock,
                                    'transaction_type_id' => 2,
                                    'price' => $prod_price,
                                    'total_harga' => $prod_price_total,
                                    // 'transaction_desc' => $inventory_desc,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now())
                                ]);
                            }


                        endif;
                    }
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    $result = array(
                        'code' => '200',
                        'status' => 'success',
                        'messages' => $alert
                    );
                } else {
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    $result = array(
                        'code' => '500',
                        'status' => 'failed',
                        'messages' => $alert
                    );
                }
                // Session::flash('message_alert', $alert);
                // return redirect('admin/inventory');
            // }
            return response()->json($result);
        }

        public function stockTransferForm(Request $request)
        {
            // dd($request->all());
            // $inventory_date = date('Y-m-d',strtotime($request->input('inventory_date')));
            // return response()->json(array('date' => $inventory_date));
            // exit;
            // $request->validate([

            // ]);
            // $field = [
            //     'inventory_warehouse' => 'required',
            //     'inventory_product' => 'required',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'required',
            //     'inventory_agent' => 'required',
            //     'inventory_product_code' => 'required',
            //     'inventory_stock_type' => 'required',
            //     'inventory_date' => 'required'
            // ];

            // if($request->input('inventory_stock_type') == null or $request->input('inventory_stock_type') == 0) {
            //     $field['inventory_stock_in'] = 'required';
            //     $field['inventory_stock_min'] = 'required';
            // }

            // $validate = \Validator::make($request->all(), $field,
            // [
            //     'inventory_warehouse' => 'Gudang Wajib Di Isi',
            //     'inventory_product' => 'Nama Produk Wajib Di Isi',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'Berat Wajib Di Isi',
            //     'inventory_agent' => 'ID Agent Wajib Di Isi',
            //     'inventory_product_code' => 'ID Produk Wajib Di Isi',
            //     'inventory_stock_type' => 'Tipe Stok Wajib Di Isi',
            //     'inventory_date' => 'Tanggal Masuk Wajib Di Isi',
            //     'inventory_stock_in' => 'Stok Masuk Wajib Di Isi',
            //     'inventory_stock_min' => 'Stok Minimum Wajib Di Isi'
            // ]);

            // if($validate->fails()){
            //     $res = array();
            //     foreach($validate->errors()->messages() as $key => $validations) {
            //         $res[$key] = $validate->customMessages[$key];
            //     }
            //     $result = array(
            //         'code' => '500',
            //         'status' => 'error',
            //         'messages' => $res
            //     );
            //     // return redirect()->back()->withInput()->withErrors($validator);
            // }else{
                $user = Session::get('users');
                // print_r($request->all());
                // exit;
                $inventory_org = $user['organization_id'];
                $inventory_trx = $request->input('inventory_trx');
                $inventory_date = date('Y-m-d',strtotime($request->input('inventory_date')));
                $inventory_status = $request->input('inventory_status');
                $inventory_warehouse = $request->input('inventory_warehouse');
                $destination_warehouse = $request->input('sales_warehouse');
                $inventory_warehouse_name = $request->input('inventory_warehouse_name');
                $inventory_uom = $request->input('inventory_uom');
                // $inventory_desc = $request->input('inventory_desc');
                $inventory_product = $request->input('inventory_product');
                $inventory_product_to = $request->input('inventory_product_to');
                $inventory_price = $request->input('inventory_price');
                $inventory_stock = $request->input('inventory_stock');
                $satuan_produk = $request->input('satuan_produk');
                $inventory_stock_to = $request->input('inventory_stock_to');
                $satuan_produk_to = $request->input('satuan_produk_to');
                // return $inventory_status;
                // TRANSACTION HEADER
                    $check = WarehouseMutationHeader::where('mutation_header_code', $inventory_trx)->first();
                    if (!$check) {
                        $trx_id = WarehouseMutationHeader::insertGetId([
                            'mutation_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            'destination_warehouse_id' => $destination_warehouse,
                            // 'supplier_id' => $supplier_id,
                            // 'transaction_type_id' => 6,
                            // 'transaction_status' => $inventory_status,
                            'mutation_date' => $inventory_date,
                            // 'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                    } else  {
                        $trx_id = $check->mutation_header_id;
                        $updated_obj = [
                            'mutation_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            'destination_warehouse_id' => $destination_warehouse,
                            // 'supplier_id' => $supplier_id,
                            // 'transaction_type_id' => 6,
                            // 'transaction_status' => $inventory_status,
                            'mutation_date' => $inventory_date,
                            // 'transaction_desc' => $inventory_desc,
                            'updated_by' => $user['id'],
                            'updated_date' => date(now())
                        ];
                        WarehouseMutationHeader::where('mutation_header_id', $trx_id)->update($updated_obj);
                        WarehouseMutationDetail::where('mutation_header_id', $trx_id)->delete();
                    }
                // TRANSACTION HEADER

                if ($trx_id > 0) {
                    // echo $inventory_status;
                    // exit;
                    for ($i=0; $i < count($inventory_product) ; $i++) {
                        $prod_id = $inventory_product[$i];
                        $destination_prod_id = $inventory_product_to[$i];
                        $prod_stock = $inventory_stock[$i];
                        $prod_stock_uom = $satuan_produk[$i];
                        $destination_stock = $inventory_stock_to[$i];
                        $destination_stock_uom = $satuan_produk_to[$i];
                        // $uom_list = $inventory_uom[$i];
                        $prod_price = str_replace(".", "", $inventory_price[$i]);
                        $trx_detail_id = WarehouseMutationDetail::insertGetId([
                            'mutation_header_id' => $trx_id,
                            'prod_id' => $prod_id,
                            'destination_prod_id' => $destination_prod_id,
                            'stock' => $prod_stock,
                            'stock_uom' => $prod_stock_uom,
                            'destination_stock' => $destination_stock,
                            'destination_stock_uom' => $destination_stock_uom,
                            'price' => $prod_price,
                            // 'uom_id' => $uom_list,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                        // $product = Product::where('prod_id',$prod_id)
                        //             ->first();
                        $convert = new UomConvert;
                        // $parent_product = Product::where('prod_id',$product['parent_prod_id'])
                        //             ->first();

                        // FROM PRODUCT
                            $product_form = Product::select('uom_id','uom_value')->where('prod_id','=',$prod_id)->first();
                            $finalStock_from = $convert->convertToStock($prod_stock_uom, $prod_stock, $product_form->uom_id, $product_form->uom_value);
                            if (!isset($finalStock_from['flag'])) {
                                $finalStock_from = $finalStock_from;
                            } else {
                                $finalStock_from = $prod_stock;
                            }

                            // $prod_price_total = (int)$prod_price * $prod_stock;
                            $check = Inventory::where([
                                                    ['organization_id', '=', $inventory_org],
                                                    ['warehouse_id', '=', $inventory_warehouse],
                                                    ['organization_id', '=', $inventory_org],
                                                    ['prod_id', '=', $prod_id],
                                                    ['active_flag', '=', 1],
                                                ])
                                                ->first();
                            $stock_card_price = 0;
                            if ($check) {
                                $old_price = $check->inventory_price;
                                $old_stock = $check->stock;
                                // $old_price_total = $old_price * $old_stock;
                                $new_stock = $old_stock - $finalStock_from;
                                // $avg_price = $new_stock == 0 ? 0 : ($old_price_total - $prod_price_total)/$new_stock;
                                // $avg_price = $prod_price;

                                $inventory_id = $check->inventory_id;
                                $update = Inventory::where('inventory_id', $inventory_id)->update([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $inventory_warehouse,
                                    'prod_id' => $prod_id,
                                    // 'booking_qty' => $booking_qty,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $prod_price,
                                    'stock' => $new_stock,
                                    'updated_by' => $user['id'],
                                    'updated_date' => date(now())
                                ]);
                                $stock_card_price = $prod_price;
                                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                // $this->validateUpdateStockTokped($prod_id,$new_stock);
                            }

                            $recent_stock_from = $finalStock_from;
                            $StockCard_whFrom = StockCard::insertGetId([
                                'stock_card_code' => $inventory_trx,
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $inventory_warehouse,
                                'prod_id' => $prod_id,
                                // 'prod_id' => $product['parent_prod_id'],
                                'stock' => "-".$recent_stock_from,
                                'transaction_type_id' => 7,
                                'price' => $stock_card_price,
                                'total_harga' => '-'.$stock_card_price*$recent_stock_from,
                                // 'total_harga' => "-".($recent_stock * (int)$prod_price),
                                // 'transaction_desc' => $inventory_desc,
                                'created_by' => $user['id'],
                                'created_date' => date(now())
                            ]);
                        // FROM PRODUCT

                        // DESTINATION PRODUCT
                            $product_to = Product::select('uom_id','uom_value')->where('prod_id','=',$destination_prod_id)->first();
                            $finalStock_to = $convert->convertToStock($destination_stock_uom, $destination_stock, $product_to->uom_id, $product_to->uom_value);

                            if (!isset($finalStock_to['flag'])) {
                                $finalStock_to = $finalStock_to;
                            } else {
                                $finalStock_to = $destination_stock_uom;
                            }

                            $get_gram_from = $convert->convertToStock($product_form->uom_id, $product_form->uom_value, '2', '1');
                            if (!isset($get_gram_from['flag'])) {
                                $price_pergram = $prod_price/$get_gram_from;
                            } else {
                                $price_pergram = $prod_price;
                            }

                            $get_gram_to = $convert->convertToStock($product_to->uom_id, $product_to->uom_value, '2', '1');
                            if (!isset($get_gram_to['flag'])) {
                                $price_pergram_final = $price_pergram * $get_gram_to;
                            } else {
                                $price_pergram_final = $prod_price;
                            }

                            // $prod_price_to = (int)$prod_price / $destination_stock;
                            // $prod_price_total_to = (int)$prod_price;
                            $check_salles = Inventory::where([
                                        ['organization_id', '=', $inventory_org],
                                        ['warehouse_id', '=', $destination_warehouse],
                                        ['organization_id', '=', $inventory_org],
                                        ['prod_id', '=', $destination_prod_id],
                                        ['active_flag', '=', 1],
                                    ])
                                    ->first();

                            if (!$check_salles) {
                                $inventory_id = Inventory::insertGetId([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $destination_warehouse,
                                    'prod_id' => $destination_prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $price_pergram_final,
                                    'stock' => $destination_stock,
                                    'created_by' => $user['id'],
                                    'created_date' => date(now())
                                ]);
                                $stock_card_price_to = $price_pergram_final;
                            } else {

                                $old_price = $check_salles->inventory_price;
                                $old_stock = $check_salles->stock;

                                $new_stock = $old_stock + $finalStock_to;
                                $avg_price_to = ($price_pergram_final+$old_price)/2;
                                $inventory_id = $check_salles->inventory_id;

                                $update = Inventory::where('inventory_id', $inventory_id)->update([
                                    'organization_id' => $inventory_org,
                                    'warehouse_id' => $destination_warehouse,
                                    'prod_id' => $destination_prod_id,
                                    'inventory_date' => $inventory_date,
                                    'inventory_price' => $avg_price_to,
                                    'stock' => $new_stock,
                                    'updated_by' => $user['id'],
                                    'updated_date' => date(now())
                                ]);
                                //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                                // $this->validateUpdateStockTokped($prod_id,$new_stock);
                                $stock_card_price_to = $avg_price_to;
                            }

                            $StockCard_whTo = StockCard::insertGetId([
                                'stock_card_code' => $inventory_trx,
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $destination_warehouse,
                                'prod_id' => $destination_prod_id,
                                'stock' => $finalStock_to,
                                'transaction_type_id' => 7,
                                'price' => $stock_card_price_to,
                                'total_harga' => $stock_card_price_to * $finalStock_to,
                                // 'transaction_desc' => $inventory_desc,
                                'created_by' => $user['id'],
                                'created_date' => date(now())
                            ]);
                        // DESTINATION PRODUCT
                    }
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    $result = array(
                        'code' => '200',
                        'status' => 'success',
                        'messages' => $alert
                    );
                } else {
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    $result = array(
                        'code' => '500',
                        'status' => 'failed',
                        'messages' => $alert
                    );
                }
                // Session::flash('message_alert', $alert);
                return response()->json($result);
                // return redirect('admin/inventory');
            // }
            // return redirect('admin/inventory/stock_transfer');
        }

        public function mutationWarehouserForm(Request $request)
        {
            // dd($request->all());
            // $request->validate([

            // ]);
            // $field = [
            //     'inventory_warehouse' => 'required',
            //     'inventory_product' => 'required',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'required',
            //     'inventory_agent' => 'required',
            //     'inventory_product_code' => 'required',
            //     'inventory_stock_type' => 'required',
            //     'inventory_date' => 'required'
            // ];

            // if($request->input('inventory_stock_type') == null or $request->input('inventory_stock_type') == 0) {
            //     $field['inventory_stock_in'] = 'required';
            //     $field['inventory_stock_min'] = 'required';
            // }

            // $validate = \Validator::make($request->all(), $field,
            // [
            //     'inventory_warehouse' => 'Gudang Wajib Di Isi',
            //     'inventory_product' => 'Nama Produk Wajib Di Isi',
            //     // 'inventory_brand' => 'required',
            //     'inventory_variant' => 'Berat Wajib Di Isi',
            //     'inventory_agent' => 'ID Agent Wajib Di Isi',
            //     'inventory_product_code' => 'ID Produk Wajib Di Isi',
            //     'inventory_stock_type' => 'Tipe Stok Wajib Di Isi',
            //     'inventory_date' => 'Tanggal Masuk Wajib Di Isi',
            //     'inventory_stock_in' => 'Stok Masuk Wajib Di Isi',
            //     'inventory_stock_min' => 'Stok Minimum Wajib Di Isi'
            // ]);

            // if($validate->fails()){
            //     $res = array();
            //     foreach($validate->errors()->messages() as $key => $validations) {
            //         $res[$key] = $validate->customMessages[$key];
            //     }
            //     $result = array(
            //         'code' => '500',
            //         'status' => 'error',
            //         'messages' => $res
            //     );
            //     // return redirect()->back()->withInput()->withErrors($validator);
            // }else{
                $user = Session::get('users');
                // print_r($request->all());
                // exit;
                $inventory_org = $user['organization_id'];
                $inventory_trx = $request->input('inventory_trx');
                $inventory_date = $request->input('inventory_date');
                $inventory_status = $request->input('inventory_status');
                $inventory_warehouse = $request->input('inventory_warehouse');
                $sales_warehouse = $request->input('sales_warehouse');
                $inventory_warehouse_name = $request->input('inventory_warehouse_name');
                $inventory_uom = $request->input('inventory_uom');
                // $inventory_desc = $request->input('inventory_desc');
                $inventory_product = $request->input('inventory_product');
                $inventory_price = $request->input('inventory_price');
                $inventory_stock = $request->input('inventory_stock');
                // return $inventory_status;
                // TRANSACTION HEADER
                    $check = WarehouseMutationHeader::where('mutation_header_code', $inventory_trx)->first();
                    if (!$check) {
                        $trx_id = WarehouseMutationHeader::insertGetId([
                            'mutation_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            'destination_warehouse_id' => $sales_warehouse,
                            // 'supplier_id' => $supplier_id,
                            // 'transaction_type_id' => 6,
                            // 'transaction_status' => $inventory_status,
                            'mutation_date' => $inventory_date,
                            // 'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                    } else  {
                        $trx_id = $check->mutation_header_id;
                        $updated_obj = [
                            'mutation_header_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            'destination_warehouse_id' => $sales_warehouse,
                            // 'supplier_id' => $supplier_id,
                            // 'transaction_type_id' => 6,
                            // 'transaction_status' => $inventory_status,
                            'mutation_date' => $inventory_date,
                            // 'transaction_desc' => $inventory_desc,
                            'updated_by' => $user['id'],
                            'updated_date' => date(now())
                        ];
                        WarehouseMutationHeader::where('mutation_header_id', $trx_id)->update($updated_obj);
                        WarehouseMutationDetail::where('mutation_header_id', $trx_id)->delete();
                    }
                // TRANSACTION HEADER

                if ($trx_id > 0) {
                    // echo $inventory_status;
                    // exit;
                    for ($i=0; $i < count($inventory_product) ; $i++) {
                        $prod_id = $inventory_product[$i];
                        $prod_stock = $inventory_stock[$i];
                        $uom_list = $inventory_uom[$i];
                        $prod_price = str_replace(".", "", $inventory_price[$i]);
                        $prod_price_total = (int)$prod_price * $prod_stock;
                        $trx_detail_id = WarehouseMutationDetail::insertGetId([
                            'mutation_header_id' => $trx_id,
                            'prod_id' => $prod_id,
                            'stock' => $prod_stock,
                            'price' => $prod_price,
                            'uom_id' => $uom_list,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                        $product = Product::where('prod_id',$prod_id)
                                    ->first();

                        // $parent_product = Product::where('prod_id',$product['parent_prod_id'])
                        //             ->first();

                        $check = Inventory::where([
                                                ['organization_id', '=', $inventory_org],
                                                ['warehouse_id', '=', $inventory_warehouse],
                                                ['organization_id', '=', $inventory_org],
                                                ['prod_id', '=', $prod_id],
                                                ['active_flag', '=', 1],
                                            ])
                                            ->first();

                        if (!$check) {
                            $inventory_id = Inventory::insertGetId([
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $inventory_warehouse,
                                'prod_id' => $prod_id,
                                'inventory_date' => $inventory_date,
                                'inventory_price' => $prod_price,
                                'stock' => $prod_stock,
                                'created_by' => $user['id'],
                                'created_date' => date(now())
                            ]);

                        } else {
                            $old_price = $check->inventory_price;
                            $old_stock = $check->stock;
                            // $booking_qty = $check->booking_qty;
                            $old_price_total = $old_price * $old_stock;
                            $new_stock = $old_stock - $prod_stock;

                            // if($booking_qty == $new_stock){
                            //     $new_stock = 0;
                            // }
                            $avg_price = $new_stock == 0 ? 0 : ($old_price_total - $prod_price_total)/$new_stock;

                            $inventory_id = $check->inventory_id;
                            $update = Inventory::where('inventory_id', $inventory_id)->update([
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $inventory_warehouse,
                                'prod_id' => $prod_id,
                                // 'booking_qty' => $booking_qty,
                                'inventory_date' => $inventory_date,
                                'inventory_price' => $avg_price,
                                'stock' => $new_stock,
                                'updated_by' => $user['id'],
                                'updated_date' => date(now())
                            ]);
                            //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                            $this->validateUpdateStockTokped($prod_id,$new_stock);
                        }

                        $check_salles = Inventory::where([
                                    ['organization_id', '=', $inventory_org],
                                    ['warehouse_id', '=', $sales_warehouse],
                                    ['organization_id', '=', $inventory_org],
                                    ['prod_id', '=', $prod_id],
                                    ['active_flag', '=', 1],
                                ])
                                ->first();

                        if (!$check_salles) {
                            $inventory_id = Inventory::insertGetId([
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $sales_warehouse,
                                'prod_id' => $prod_id,
                                'inventory_date' => $inventory_date,
                                'inventory_price' => $prod_price,
                                'stock' => $prod_stock,
                                'created_by' => $user['id'],
                                'created_date' => date(now())
                            ]);
                        } else {

                            $old_price = $check_salles->inventory_price;
                            $old_stock = $check_salles->stock;
                            $booking_qty = $check_salles->booking_qty;
                            if($check_salles->booking_qty == null){
                                $booking_qty = 0;
                            }
                            $old_price_total = $old_price * $old_stock;
                            // $new_stock = $old_stock + $prod_stock;
                            // $new_stock = $old_stock + 0;
                            // if($booking_qty > 0){
                            //     if($booking_qty > $prod_stock){
                            //         $booking_qty = $booking_qty - $prod_stock;
                            //         $prod_stock = 0;
                            //         // $new_stock -= $prod_stock;
                            //     }elseif($booking_qty == $prod_stock){
                            //         $booking_qty = 0;
                            //         $prod_stock = 0;
                            //     }elseif($booking_qty < $prod_stock){
                            //         $prod_stock = $prod_stock - $booking_qty;
                            //         $booking_qty = 0;
                            //     }
                            //     // $new_stock -= $new_stock;
                            // }
                            // else{
                            // }
                            $new_stock = $old_stock + $prod_stock;
                            $avg_price = $new_stock == 0 ? 0 : (($prod_price_total + $old_price_total)/$new_stock);
                            $inventory_id = $check_salles->inventory_id;
                            $update = Inventory::where('inventory_id', $inventory_id)->update([
                                'organization_id' => $inventory_org,
                                'warehouse_id' => $sales_warehouse,
                                'prod_id' => $prod_id,
                                // 'booking_qty' => $booking_qty,
                                'inventory_date' => $inventory_date,
                                'inventory_price' => $avg_price,
                                'stock' => $new_stock,
                                'updated_by' => $user['id'],
                                'updated_date' => date(now())
                            ]);
                            //INTEGRATION TOKOPEDIA --------- validate push update ke tokopedia
                            $this->validateUpdateStockTokped($prod_id,$new_stock);
                        }

                        $recent_stock = $prod_stock;
                        $StockCard_whFrom = StockCard::insertGetId([
                            'stock_card_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $inventory_warehouse,
                            'prod_id' => $prod_id,
                            // 'prod_id' => $product['parent_prod_id'],
                            'stock' => "-".$recent_stock,
                            'transaction_type_id' => 7,
                            'price' => $prod_price,
                            'total_harga' => '-'.$prod_price_total,
                            // 'total_harga' => "-".($recent_stock * (int)$prod_price),
                            // 'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);

                        $StockCard_whTo = StockCard::insertGetId([
                            'stock_card_code' => $inventory_trx,
                            'organization_id' => $inventory_org,
                            'warehouse_id' => $sales_warehouse,
                            'prod_id' => $prod_id,
                            'stock' => $prod_stock,
                            'transaction_type_id' => 7,
                            'price' => $prod_price,
                            'total_harga' => $prod_price_total,
                            // 'transaction_desc' => $inventory_desc,
                            'created_by' => $user['id'],
                            'created_date' => date(now())
                        ]);
                    }
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    $result = array(
                        'code' => '200',
                        'status' => 'success',
                        'messages' => $alert
                    );
                } else {
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    $result = array(
                        'code' => '500',
                        'status' => 'failed',
                        'messages' => $alert
                    );
                }
                // Session::flash('message_alert', $alert);
                return response()->json($result);
                // return redirect('admin/inventory');
            // }
            // return redirect('admin/inventory/stock_transfer');
        }

        public function history(Request $request,$id)
        {
            if($id == null){
                return redirect('admin/inventory');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id as trx_id',
                            '10_transaction_header.transaction_header_code as trx_code',
                            '10_transaction_header.organization_id',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_desc as trx_desc',
                            '10_transaction_header.transaction_status as trx_status',
                            '00_warehouse.warehouse_id',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->first();

            $data['trx_header'] = $trx_header;
            $data['id_detail'] = $id;

            // PRODUCT WAREHOUSE
                if ($trx_header->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$trx_header->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                           ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/history', $data);
        }
    // BARANG MASUK

    // STOCK CARD
        public function stock_card()
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],30)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "BM".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    // $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                    //                         ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                    //                         ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                    //                         ->whereNull('00_product.parent_prod_id')
                    //                         ->where('00_product.active_flag',1)
                    //                         ->get();
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                ->where('00_product.stockable_flag',1)
                                ->where('00_product.active_flag',1)
                                ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE
            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/card', $data);
        }

        public function findSC(Request $request)
        {
            $code_url = request()->segment(1);
            // $user = Session::get('users');
            // $data['user'] = $user;

            // USER ORGANIZATION & WAREHOUSE
            $user = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                ->where('98_user.user_id',Session::get('users')['id'])
                ->first();
            $data['user'] = $user;
            // USER ORGANIZATION & WAREHOUSE

            $draw = $request->get('draw');
            $start = $request->has('start') ? $request->get('start') : 0;
            $length = $request->has('length') ? $request->get('length') : 10;
            $search = $request->has('search') ? $request->get('search')['value'] : '';

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            if ($order_column == 0) {
                $column_name = "00_stock_card.created_date";
                $order_type = "ASC";
            } else {
                $column_name = $request->get('columns')[$order_column]['data'];
            }

            $wh_id = $request->input('sc_warehouse');
            $prod_id = $request->input('sc_product');
            $date_from = $request->input('sc_date_from');
            $date_to = $request->input('sc_date_to');
            $organization_id = $user['organization_id'];

            // $yesterdayDate = date('Y-m-d 00:00:00', strtotime($date_from.'-1 day'));
            $startDate = date('Y-m-d 00:00:00', strtotime($date_from));
            $endDate = date('Y-m-d 23:59:59', strtotime($date_to));

            $total_data = StockCard::select("00_stock_card.stock_card_code",
                                    "00_stock_card.stock",
                                    "00_stock_card.price",
                                    "00_stock_card.total_harga",
                                    "00_stock_card.total_harga",
                                    "00_transaction_type.transaction_type_name",
                                    '00_product.uom_value',
                                    '00_uom.uom_name'
                                )
                                ->leftJoin('00_transaction_type','00_transaction_type.transaction_type_id','=','00_stock_card.transaction_type_id')
                                ->leftJoin('00_product','00_stock_card.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                ->where([
                                    ['00_stock_card.created_date','>=',$startDate],
                                    ['00_stock_card.created_date','<=',$endDate],
                                    ['00_stock_card.active_flag',1],
                                    ['00_stock_card.warehouse_id',$wh_id],
                                    ['00_stock_card.prod_id',$prod_id],
                                    ['00_stock_card.organization_id','=',$organization_id],
                                ])
                                ->count();

            $query = StockCard::select("00_stock_card.stock_card_code",
                                    "00_stock_card.stock",
                                    "00_stock_card.price",
                                    "00_stock_card.total_harga",
                                    "00_transaction_type.transaction_type_name",
                                    "00_stock_card.created_date",
                                    '00_product.uom_value',
                                    '00_uom.uom_name'
                                )
                                ->leftJoin('00_transaction_type','00_transaction_type.transaction_type_id','=','00_stock_card.transaction_type_id')
                                ->leftJoin('00_product','00_stock_card.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                ->where([
                                    ['00_stock_card.created_date','>=',$startDate],
                                    ['00_stock_card.created_date','<=',$endDate],
                                    ['00_stock_card.active_flag',1],
                                    ['00_stock_card.warehouse_id',$wh_id],
                                    ['00_stock_card.prod_id',$prod_id],
                                    ['00_stock_card.organization_id','=',$organization_id],
                                ])
                                ->limit($length)
                                ->offset($start)
                                ->orderBy($column_name,$order_type)
                                ->get();

            $sql = StockCard::selectRaw("SUM(00_stock_card.stock) AS balance, SUM(total_harga) AS total,00_product.uom_value,00_uom.uom_name")
                                    ->leftJoin('00_product','00_stock_card.prod_id','=','00_product.prod_id')
                                    ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                    ->where('00_stock_card.created_date', '<', $startDate)
                                    ->where([
                                        ['00_stock_card.active_flag',1],
                                        ['00_stock_card.warehouse_id',$wh_id],
                                        ['00_stock_card.prod_id',$prod_id],
                                        ['00_stock_card.organization_id','=',$organization_id],
                                    ])
                                    ->first();

            $array = array();
            $no = $start + 1;
            if(!empty($sql->total) && !empty($sql->balance)) {
                if($prod_id){
                    $uom = Product::select('00_product.prod_id','uom_value','uom_name')
                            ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                            ->where('00_product.prod_id',$prod_id)
                            ->first();
                }
                $hargaSatuan = round($sql->total / $sql->balance);
                $temp = (object) array(
                    'no' => $no,
                    'sc_date' => date('d-m-Y', strtotime($date_from)),
                    'sc_code' => null,
                    'sc_type' => 'Balance',
                    'sc_qty' => $sql->balance,
                    'sc_unit' => "$uom->uom_value $uom->uom_name",
                    'sc_price' => $hargaSatuan,
                    //'sc_tot_price' => 'Rp. '. number_format($sql->total)
                    'sc_tot_price' => $sql->total
                );
                array_push($array, $temp);
                $no += 1;
            }else{
                $temp = (object) array(
                    'no' => $no,
                    'sc_date' => date('d-m-Y', strtotime($date_from)),
                    'sc_code' => null,
                    'sc_type' => 'Balance',
                    'sc_qty' => 0,
                    'sc_unit' => 0,
                    'sc_price' => 0,
                    //'sc_tot_price' => 'Rp. '. number_format(0),
                    'sc_tot_price' => 0

                );
                array_push($array, $temp);
                $no += 1;
            }

            foreach ($query as $row):
                $data_object = (object) array(
                    'no' => $no++,
                    'sc_date' => date('d/m/Y', strtotime($row->created_date)),
                    'sc_code' => $row->stock_card_code,
                    'sc_type' => $row->transaction_type_name,
                    'sc_qty' => $row->stock,
                    'sc_unit' => "$row->uom_value $row->uom_name",
                    'sc_price' => $row->price,
                    //'sc_tot_price' => 'Rp. '. number_format($row->total_harga),
                    'sc_tot_price' => $row->total_harga

                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $draw,
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );

            echo json_encode($data);
        }
    // STOCK CARD

    // STOCK OPNAME
        public function stock_recording()
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "OP".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $data['all_category'] = Category::where('active_flag',1)->whereNull('parent_category_id')->get();
            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/indexOpname', $data);
        }

        public function stock_recording_create()
        {
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // NO TRANSAKSI
                $no_trx_shuffle = str_shuffle(strtotime(date("Y-m-d H:m:i")));
                $no_trx_first = substr($no_trx_shuffle,0,3);
                $no_trx_last = substr($no_trx_shuffle,-3);
                $data['no_trx'] = "OP".$no_trx_first."".$no_trx_last;
            // NO TRANSAKSI

            // PRODUCT WAREHOUSE
                if ($user_wh_org->warehouse_id != null) {
                    $wh_product = ProductWarehouse::select('00_product.prod_name','00_product.prod_id')
                                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                            ->where('00_product_warehouse.warehouse_id',$user_wh_org->warehouse_id)
                                            ->whereNull('00_product.parent_prod_id')
                                            ->where('00_product.active_flag',1)
                                            ->get();
                } else {
                    $wh_product = null;
                }
                $data['wh_product'] = $wh_product;
            // PRODUCT WAREHOUSE

            $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');
            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id')->get();
            $data['list_uom'] = $uom;

            $data['all_category'] = Category::where('active_flag',1)->get();
            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/createOpname', $data);
        }


        public function stock_recording_edit(Request $request,$id)
        {
            if($id == null){
                return redirect('admin/inventory/stock_recording');
            }
            $user = Session::get('users');
            $data['user'] = $user;
            $user_menu = new MenuController();
            $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
            $all_menu = $user_menu->get($user['id']);
            $data['all_menu'] = $all_menu;
            $data['accessed_menu'] = $accessed_menu;

            $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id as trx_id',
                            '10_transaction_header.transaction_header_code as trx_code',
                            '10_transaction_header.organization_id',
                            '10_transaction_header.transaction_date as trx_date',
                            '10_transaction_header.transaction_status as trx_status',
                            '10_transaction_header.category_id',
                            '00_warehouse.warehouse_id',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->first();
            $data_param = (object)array(
                'wh_id' => $trx_header->warehouse_id,
                'cat_id' => explode(",",$trx_header->category_id),
            );
            $getProd = self::getProductByWhCatIDUseParam(json_encode($data_param));
            $list_product = $getProd->getData();
            $data['list_productBase64'] = base64_encode(Json_encode($list_product));
            $data['list_product'] = $list_product;

            $trx_detail = TransactionHeader::select('00_product.prod_id',
                                                '00_product.prod_code',
                                                '00_product.uom_value',
                                                '00_uom.uom_name',
                                                '10_input_opname.rack',
                                                '10_input_opname.stock',
                                                '10_input_opname.final_stock',
                                                '10_input_opname.uom_id',
                                                'opname_uom.uom_name as opname_uom'
                                                )
                                        ->leftJoin('10_input_opname','10_transaction_header.transaction_header_code','=','10_input_opname.transaction_number')
                                        ->leftJoin('00_product','10_input_opname.prod_id','=','00_product.prod_id')
                                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                        ->leftJoin('00_uom as opname_uom','opname_uom.uom_id','=','10_input_opname.uom_id')
                                        ->where('10_transaction_header.transaction_header_id',$id)
                                        ->get();

            // $trx_detail = InputOpname::select('10_input_opname.prod_id',
            //                 '10_input_opname.rack',
            //                 '10_input_opname.stock',
            //                 '00_product.prod_code')
            //                 ->leftJoin('00_product','10_input_opname.prod_id','=','00_product.prod_id')
            //                 ->where('10_input_opname.transaction_number',$trx_header->trx_code)
            //                 ->get();
            $data['trx_header'] = $trx_header;
            $data['category_opname'] = explode(",",$trx_header->category_id);
            $data['trx_detail'] = $trx_detail;
            $data['list_item'] = count($trx_detail);

            // USER ORGANIZATION & WAREHOUSE
                $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                                ->where('98_user.user_id',$user['id'])
                                ->first();
                $data['user_wh_org'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');
            $second_uom = UOM::where('active_flag','=',1)
                ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
                ->select('00_uom.uom_id', '00_uom.uom_name');
            $uom = $first_uom->union($second_uom)
                ->groupBy('00_uom.uom_id')->get();

            $data['all_category'] = Category::where('active_flag',1)->get();
            $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
            $data['list_uom'] = $uom;
            $logo = Config::get('logo.menubar');
            $favicon = Config::get('logo.favicon');
            $data['logo'] = $logo;
            $data['favicon'] = $favicon;
            return view('admin::inventory/stock/editOpname', $data);
        }

        public function stock_recording_form(Request $request)
        {
            $user = Session::get('users');
            $opname_org = $request->input('opname_org');
            $opname_trx = $request->input('opname_trx');
            $opname_warehouse = $request->input('opname_warehouse');
            $opname_warehouse_name = $request->input('opname_warehouse_name');
            $opname_status = $request->input('opname_status');
            $opname_date = $request->input('opname_date');
            $opname_category = is_array($request->input('opname_category')) ? implode(",",$request->input('opname_category')) : $request->input('opname_category');
            // $opname_category = $request->input('opname_category');
            $opname_product = $request->input('opname_product');
            $opname_rack = $request->input('opname_rack');
            $opname_stock = $request->input('opname_stock');
            $opname_uom_id_list = $request->input('satuan_produk');
            $opname_stock_converted_list = $request->input('final_stock');
            // USER ORGANIZATION & WAREHOUSE
            $user_wh_org = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                        ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                        ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                        ->where('98_user.user_id',$user['id'])
                        ->first();
            //$data['user'] = $user_wh_org;
            // USER ORGANIZATION & WAREHOUSE

            // TRANSACTION HEADER
            DB::beginTransaction();
            try {
                $check = TransactionHeader::where('transaction_header_code', $opname_trx)->first();
                if (!$check) {
                    $trx_id = TransactionHeader::insertGetId([
                        'transaction_header_code' => $opname_trx,
                        'organization_id' => $opname_org,
                        'warehouse_id' => $opname_warehouse,
                        // 'supplier_id' => $supplier_id,
                        'category_id' => $opname_category,
                        'transaction_type_id' => 5,
                        'transaction_status' => $opname_status,
                        'transaction_date' => $opname_date,
                        'created_by' => $user['id'],
                        'created_date' => date(now()),
                        'updated_date' => date(now()),
                    ]);
                } else  {
                    $trx_id = $check->transaction_header_id;
                    $updated_obj = [
                        'transaction_header_code' => $opname_trx,
                        'organization_id' => $opname_org,
                        'warehouse_id' => $opname_warehouse,
                        // 'supplier_id' => $supplier_id,
                        'category_id' => $opname_category,
                        'transaction_type_id' => 5,
                        'transaction_status' => $opname_status,
                        'transaction_date' => $opname_date,
                        'updated_by' => $user['id'],
                        'updated_date' => date(now())

                    ];
                    TransactionHeader::where('transaction_header_id', $trx_id)->update($updated_obj);
                    TransactionDetail::where('transaction_header_id', $trx_id)->delete();
                    InputOpname::where('transaction_number', $opname_trx)->delete();
                }

                // TRANSACTION HEADER
                // dd($opname_product);
                if ($trx_id > 0) {
                    $diff_opname = array();
                    for ($i=0; $i < count($opname_product); $i++) {
                        $opname_prd_id = $opname_product[$i];
                        $opname_prd_stock = (int)$opname_stock[$i];
                        $opname_uom_id = $opname_uom_id_list[$i];
                        $opname_stock_converted = $opname_stock_converted_list[$i];
                        // $opname_rack = $opname_rack[$i];
                        $q_freeze = StockCard::selectRaw('SUM(stock) as freeze_stock')
                                                ->whereRaw('prod_id = '.$opname_prd_id.' and organization_id = '.$user_wh_org->organization_id.' and warehouse_id = '.$opname_warehouse.' and created_date < CURRENT_DATE()')
                                                ->first();

                        $q_inventory = Inventory::select('*')
                                                ->where('prod_id', $opname_prd_id)
                                                ->first();

                        if (isset($diff_opname[$opname_prd_id])) {
                            $freeze_stock = $diff_opname[$opname_prd_id] != null ? (int)$diff_opname[$opname_prd_id] : 0;
                            $different_stock = $opname_stock_converted + $freeze_stock;
                            $price = $q_freeze->inventory_price != null ? $q_freeze->inventory_price : 0;
                            $tot_price = $price * $different_stock;

                            $diff_opname[$opname_prd_id] = $different_stock; //SET FOR NEXT FREEZE STOCK IF PROD_ID SAME
                        } else {
                            $freeze_stock = $q_freeze->freeze_stock != null ? $q_freeze->freeze_stock : 0;
                            $different_stock = $opname_stock_converted - $freeze_stock;
                            $price = $q_freeze->inventory_price != null ? $q_freeze->inventory_price : 0;
                            $tot_price = $price * $different_stock;

                            $diff_opname[$opname_prd_id] = $different_stock; //SET FOR NEXT FREEZE STOCK IF PROD_ID SAME
                        }

                        $trx_detail_id = TransactionDetail::insertGetId([
                            'transaction_header_id' => $trx_id,
                            'prod_id' => $opname_prd_id,
                            'stock' => $freeze_stock,
                            'price' => $price,
                            'created_by' => $user['id'],
                            'created_date' => date(now()),
                            'updated_date' => date(now()),
                            'uom_id' => $opname_uom_id
                        ]);

                        $opname_id = InputOpname::insertGetId([
                            'transaction_number' => $opname_trx,
                            'organization_id' => $opname_org,
                            'prod_id' => $opname_prd_id,
                            'price' => $price,
                            'stock_difference' => $different_stock,
                            'stock' => $opname_prd_stock,
                            'rack' => $opname_rack[$i],
                            'created_by' => $user['id'],
                            'created_date' => date(now()),
                            'updated_date' => date(now()),
                            'uom_id' => $opname_uom_id,
                            'final_stock' => $opname_stock_converted
                        ]);

                        // if ($different_stock != 0 AND $opname_status == "1") {
                        //     $StockCard = StockCard::insertGetId([
                        //         'stock_card_code' => $opname_trx,
                        //         'organization_id' => $opname_org,
                        //         'warehouse_id' => $opname_warehouse,
                        //         'prod_id' => $opname_prd_id,
                        //         'stock' => $different_stock,
                        //         'transaction_type_id' => 5,
                        //         'price' => $price,
                        //         'total_harga' => $tot_price,
                        //         'created_by' => $user['id'],
                        //         'created_date' => date(now()),
                        //         'uom_id' => $opname_uom_id
                        //     ]);
                        // }
                    }
                    //dd($diff_opname);
                    foreach($diff_opname as $id=>$different_stock){
                        if ($different_stock != 0 AND $opname_status == "1") {
                            $check = Inventory::where([
                                ['organization_id', '=', $opname_org],
                                ['warehouse_id', '=', $opname_warehouse],
                                ['prod_id', '=', $id],
                                ['active_flag', '=', 1],
                            ])
                            ->first();
                            $product = Product::where('prod_id','=',$id)->first();

                            $new_stock = $check->stock + $different_stock;
                            $avg_price = ($check->inventory_price * $check->stock) / $new_stock;

                            $StockCard = StockCard::insertGetId([
                                'stock_card_code' => $opname_trx,
                                'organization_id' => $opname_org,
                                'warehouse_id' => $opname_warehouse,
                                'prod_id' => $id,
                                'stock' => $different_stock,
                                'transaction_type_id' => 5,
                                'price' => $check->inventory_price,
                                'total_harga' => $check->inventory_price * $different_stock,
                                'created_by' => $user['id'],
                                'created_date' => date(now()),
                                'uom_id' => $product->uom_id
                            ]);

                            $update = Inventory::where('inventory_id', $check->inventory_id)->update([
                                'inventory_price' => $avg_price,
                                'stock' => $new_stock,
                                'updated_by' => $user['id'],
                                'updated_date' => date(now())
                            ]);
                        }
                    }
                    DB::commit();
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.successfully'))]);
                    $result = array(
                        'code' => '200',
                        'status' => 'success',
                        'messages' => $alert
                    );
                } else {
                    $alert = Lang::get('notification.has been inserted',['result' => strtolower(Lang::get('notification.inaccurately'))]);
                    $result = array(
                        'code' => '500',
                        'status' => 'failed',
                        'messages' => $alert
                    );
                }
            } catch (\Exception $e) {
                DB::rollback();
                $result = array(
                    'code' => '500',
                    'status' => 'failed',
                    'error_messages' => $e->getMessage(),
                    'messages' => 'Ups, Something went wrong !!!'
                );
            }
            return response()->json($result);
        }

        var $columns = array(
            "transaction_header_code", "transaction_date", "warehouse_name"
        );

        public function geStockOP(Request $request) {
            $code_url = request()->segment(1);
            // $wh_id = $wh_id == "null" ? "" : $wh_id;
            // $category_id = $category_id == "null" ? "" : $category_id;
            // $user = Session::get('users');
            // $data['user'] = $user;
            // USER ORGANIZATION & WAREHOUSE
            $user = User::select('98_user.user_id','98_user.user_name','00_organization.organization_id','00_organization.organization_type_id','00_warehouse.warehouse_id','00_warehouse.warehouse_name')
                ->leftJoin('00_organization','00_organization.organization_id','=','98_user.organization_id')
                ->leftJoin('00_warehouse','00_warehouse.warehouse_id','=','98_user.warehouse_id')
                ->where('98_user.user_id',Session::get('users')['id'])
                ->first();
            $data['user'] = $user;
            $warehouse_id = $user->warehouse_id;
            // USER ORGANIZATION & WAREHOUSE
            $draw = $request->get('draw');
            $start = $request->has('start') ? $request->get('start') : 0;
            $length = $request->has('length') ? $request->get('length') : 10;
            $search = $request->has('search') ? $request->get('search')['value'] : '';

            $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
            $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

            if ($order_column == 0) {
                $column_name = "10_transaction_header.created_date";
                $order_type = "DESC";
            } else {
                $column_name = $request->get('columns')[$order_column]['data'];
            }

            $columns = $this->columns;
            $columns[count($columns) - 1] = DB::raw("(CASE WHEN 10_transaction_header.transaction_status = 0 THEN 'Tunda' ELSE 'Selesai' END)");
            // echo $search;
            // exit;

            $total_data = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code',
                            '10_transaction_header.transaction_date',
                            '10_transaction_header.transaction_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',5)
                            ->where('10_transaction_header.organization_id',$user->organization_id)
                            ->when($warehouse_id, function ($query, $warehouse_id) {
                                return $query->where('10_transaction_header.warehouse_id', $warehouse_id);
                            })
                            ->when($search, function ($query, $search) {
                                return $query->where('10_transaction_header.transaction_header_code', 'like',"%$search%");
                            })
                            ->where(function($query) use($columns, $search){
                                for ($b=0; $b < count($columns); $b++) {
                                    if($b > 0){
                                        $query->orWhere($columns[$b],'like','%'.$search.'%');
                                    }else{
                                        $query->where($columns[$b],'like','%'.$search.'%');
                                    }
                                }
                            });
            $total_data = $total_data->count();

            $query = TransactionHeader::select('10_transaction_header.transaction_header_id',
                            '10_transaction_header.transaction_header_code',
                            '10_transaction_header.transaction_date',
                            '10_transaction_header.transaction_status',
                            '00_warehouse.warehouse_name')
                            ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                            ->where('10_transaction_header.active_flag',1)
                            ->where('10_transaction_header.transaction_type_id',5)
                            ->where('10_transaction_header.organization_id',$user->organization_id)
                            ->when($warehouse_id, function ($query, $warehouse_id) {
                                return $query->where('10_transaction_header.warehouse_id', $warehouse_id);
                            })
                            ->when($search, function ($query, $search) {
                                return $query->where('10_transaction_header.transaction_header_code', 'like',"%$search%");
                            })
                            ->limit($length)
                            ->offset($start)
                            ->orderBy($column_name,$order_type)
                            ->where(function($query) use($columns, $search){
                                for ($b=0; $b < count($columns); $b++) {
                                    if($b > 0){
                                        $query->orWhere($columns[$b],'like','%'.$search.'%');
                                    }else{
                                        $query->where($columns[$b],'like','%'.$search.'%');
                                    }
                                }
                            });

            $query = $query->get();
            // echo $query;
            // exit;

            $array = array();
            $no = $start + 1;
            $url_edit = url('/admin/inventory/stock_recording/edit');
            $url_history = url('/admin/inventory/stock_recording/history');
            foreach ($query as $row):
                $url = $url_edit."/".$row->transaction_header_id;
                $url_h = $url_history."/".$row->transaction_header_id;
                if ($row->transaction_status < 1) {
                    $action = '<div row>';
                    $action .= '<div class="col-sm-6"><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye"></i></a></div>';
                    $action .= '<div class="col-sm-6"><a class="loader-trigger" href="'.$url.'" data-toggle="tooltip"  title="'.(__('page.edit')).'"><i class="fa fa-pencil"></i></a></div>';
                    $action .= '</div>';
                } else {
                    $action = '<div row>';
                    $action .= '<div class="col-sm-6"><a class="loader-trigger" href="'.$url_h.'" data-toggle="tooltip"  title="'.(__('page.history')).'"><i class="fa fa-eye"></i></a></div>';
                    $action .= '</div>';
                }
                $data_object = (object) array(
                    'no' => $no++,
                    'trx_no' => $row->transaction_header_code,
                    'trx_date' => date("Y-m-d", strtotime($row->transaction_date)),
                    'warehouse_name' => $row->warehouse_name,
                    'trx_status' => $row->transaction_status > 0 ? "Selesai" : "Tertunda",
                    'action' => $action
                );
                array_push($array, $data_object);
            endforeach;

            $data = array(
                'draw' => $draw,
                'recordsTotal' => $total_data,
                'recordsFiltered' => $total_data,
                'data' => $array
            );

            echo json_encode($data);
        }
    // STOCK OPNAME



    public function historyDetail(Request $request,$id)
    {
        $mutation_columns = array (
                        '10_transaction_detail.price',
                        '10_transaction_detail.stock',
                        '00_product.prod_code',
                        '00_product.prod_name',
                        '00_uom.uom_id',
                        '00_uom.uom_name',
                        '00_product.uom_value'
        );

        $parameter = array(
            'offset' => $request->has('start') ? $request->get('start') : 0,
            'limit' => $request->has('length') ? $request->get('length') : 10,
            'column' => $request->has('order') ? $request->get('order')[0]['column'] : 0,
            'order' => $request->has('order') ? $request->get('order')[0]['dir'] : 'desc',
            'search' => $request->has('search') ? $request->get('search')['value'] : ''
        );
        $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
        $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

        if ($order_column == 0) {
            $column_name = "10_transaction_detail.prod_id";
            $order_type = "DESC";
        } else {
            $column_name = $request->get('columns')[$order_column]['data'];
        }
        $total = TransactionDetail::select('10_transaction_detail.prod_id',
                        '10_transaction_detail.price',
                        '10_transaction_detail.stock',
                        '00_product.prod_code',
                        '00_product.prod_name',
                        '00_uom.uom_id',
                        '00_uom.uom_name',
                        '00_product.uom_value')
                        ->leftJoin('00_product','10_transaction_detail.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','10_transaction_detail.uom_id')
                        ->where('10_transaction_detail.transaction_header_id',$id)
                        ->count();

        $sql = TransactionDetail::select('10_transaction_detail.prod_id',
                        '10_transaction_detail.price',
                        '10_transaction_detail.stock',
                        '00_product.prod_code',
                        '00_product.prod_name',
                        '00_uom.uom_id',
                        '00_uom.uom_name',
                        '00_product.uom_value',
                        'uom_product.uom_name as uom_name_product')
                        ->leftJoin('00_product','10_transaction_detail.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','10_transaction_detail.uom_id')
                        ->leftJoin('00_uom as uom_product','uom_product.uom_id','=','00_product.uom_id')
                        ->where('10_transaction_detail.transaction_header_id',$id)
                        ->offset($parameter['offset'])
                        ->limit($parameter['limit'])
                        ->where(function($query) use ($mutation_columns, $parameter){
                            for ($k=0; $k < count($mutation_columns); $k++) {
                                if($k > 0){
                                    $query->orWhere($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                }else{
                                    $query->where($mutation_columns[$k],'like','%'.$parameter['search'].'%');
                                }
                            }
                        })
                        ->orderBy($column_name,$order_type)
                        ->get();
        $no = 1;
        $data = array();
        foreach ($sql as $row):
            $data_object = (object) array(
                'no' => $no++,
                'prod_name' => $row->prod_name,
                'prod_code' => $row->prod_code,
                'uom_id' => $row->uom_id,
                'uom_name' => $row->uom_name,
                'uom_value' => $row->uom_value,
                'formatted_product_name' => $row->prod_name . " " . $row->uom_value . " " . $row->uom_name,
                'price' => $row->price,
                'stock' => $row->stock,
                'total' => $row->price * $row->stock,
                'prod_unit' => "$row->uom_value $row->uom_name_product"
            );
            array_push($data, $data_object);
        endforeach;

        $data = array(
            'draw' => $request->get('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $data
        );

        echo json_encode($data);
    }

    public function stock()
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],29)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;
        $data['inventory_warehouse'] = Warehouse::where('active_flag',1)->where('warehouse_type_id',1)->get();
        $data['all_warehouse'] = Warehouse::where('active_flag',1)->get();
        $data['all_category'] = Category::where('active_flag',1)->whereNull('parent_category_id')->get();
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::inventory/stock/index', $data);
    }

    var $stock_columns = array(
        '00_product.prod_code','00_product.prod_name','00_warehouse.warehouse_name','00_product.prod_price','00_inventory.stock','00_category.category_name','00_product.uom_value','00_uom.uom_name'
    );

    public function getAllStock(Request $request,$wh_id,$category_id) {
        $code_url = request()->segment(1);
        $wh_id = $wh_id == null ? "" : $wh_id;
        // $sales_warehouse = $sales_warehouse == "null" ? "" : $sales_warehouse;
        $category_id = $category_id == "null" ? "" : $category_id;
        $user = Session::get('users');
        $data['user'] = $user;
        $organization_id = $user['organization_id'] == null ? null : $user['organization_id'];

        $draw = $request->get('draw');
        $start = $request->has('start') ? $request->get('start') : 0;
        $length = $request->has('length') ? $request->get('length') : 10;
        $search = $request->has('search') ? $request->get('search')['value'] : '';
        $stock_columns = $this->stock_columns;

        $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
        $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

        if ($order_column == 0) {
            $column_name = "00_inventory.updated_date";
            $order_type = "DESC";
        } else {
            $column_name = $request->get('columns')[$order_column]['data'];
        }

        $total_data = Inventory::select('00_inventory.prod_id')
                        ->distinct()
                        ->leftJoin('00_product','00_inventory.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                        ->leftJoin('00_warehouse','00_inventory.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                        // ->when([$wh_id, $sales_warehouse], function ($query, $wh_id, $sales_warehouse) {
                        //     if($sales_warehouse == ""){
                        //         return $query->where('00_inventory.warehouse_id', '=', $wh_id);
                        //     }else{
                        //         return $query->whereIn('00_inventory.warehouse_id', [$wh_id, $sales_warehouse]);
                        //     }
                        // })
                        // ->when($wh_id, function($query, $wh_id){
                        //     return $query->whereIn('00_inventory.warehouse_id', explode(',',$wh_id));
                        // })
                        ->when($category_id, function ($query, $category_id) {
                            return $query->where('00_product.category_id', '=', $category_id);
                        })
                        ->when($organization_id, function ($query, $organization_id) {
                            return $query->where('00_inventory.organization_id', $organization_id);
                        })
                        ->where('00_inventory.active_flag',1)
                        ->where(function($query) use($stock_columns, $search){
                            for ($b=0; $b < count($stock_columns); $b++) {
                                if($b > 0){
                                    $query->orWhere($stock_columns[$b],'like','%'.$search.'%');
                                }else{
                                    $query->where($stock_columns[$b],'like','%'.$search.'%');
                                }
                            }
                        });
                        // ->groupBy('00_inventory.prod_id');
                        if($wh_id != null){
                            $total_data->whereIn('00_inventory.warehouse_id', explode(',',$wh_id));
                        }
        // if($wh_id != ""){
        //     if($sales_warehouse != ""){
        //         $total_data->whereIn('00_inventory.warehouse_id', [$wh_id, $sales_warehouse]);
        //     }else{
        //         $total_data->where('00_inventory.warehouse_id', '=', $wh_id);
        //     }
        // }
        $total_data  = $total_data->count();
        // echo $total_data;
        // exit;

        $query = Inventory::select('00_inventory.inventory_id','00_inventory.inventory_price','00_product.prod_id','00_product.prod_code','00_category.category_name','00_product.prod_code','00_product.prod_name','00_warehouse.warehouse_name','00_product.prod_price','00_inventory.stock','00_product.uom_value','00_uom.uom_name')
                        ->leftJoin('00_product','00_inventory.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                        ->leftJoin('00_warehouse','00_inventory.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                        // ->when($wh_id, function ($query, $wh_id) {
                        //     return $query->where('00_inventory.warehouse_id', '=', $wh_id);
                        // })
                        ->when($wh_id, function($query, $wh_id){
                            return $query->whereIn('00_inventory.warehouse_id', explode(',',$wh_id));
                        })
                        ->when($category_id, function ($query, $category_id) {
                            return $query->where('00_product.category_id', '=', $category_id);
                        })
                        ->when($organization_id, function ($query, $organization_id) {
                            return $query->where('00_inventory.organization_id', $organization_id);
                        })
                        ->where('00_inventory.active_flag',1)
                        ->where(function($query) use($stock_columns, $search){
                            for ($b=0; $b < count($stock_columns); $b++) {
                                if($b > 0){
                                    $query->orWhere($stock_columns[$b],'like','%'.$search.'%');
                                }else{
                                    $query->where($stock_columns[$b],'like','%'.$search.'%');
                                }
                            }
                        })
                        // ->groupBy('00_inventory.prod_id')
                        ->limit($length)
                        ->offset($start)
                        ->orderBy($column_name,$order_type);
                        // if($wh_id != ""){
                        //     if($sales_warehouse != ""){
                        //         $query->whereIn('00_inventory.warehouse_id', [$wh_id, $sales_warehouse]);
                        //     }else{
                        //         $query->where('00_inventory.warehouse_id', '=', $wh_id);
                        //     }
                        // }
                        if($wh_id != null){
                            $query->whereIn('00_inventory.warehouse_id', explode(',',$wh_id));
                        }
                        $query = $query->get();
                        // echo $query;
                        // exit;

        $array = array();
        $no = $start + 1;
        foreach ($query as $row):
            $data_object = (object) array(
                'no' => $no++,
                'prod_code' => $row->prod_code,
                'category_name' => $row->category_name,
                'prod_name' => $row->prod_name,
                'prod_unit' => ($row->uom_value+0)." ".$row->uom_name,
                'warehouse_name' => $row->warehouse_name,
                'prod_price' => $row->inventory_price,
                'stock' => $row->stock,
                'total_price' => $row->inventory_price * $row->stock
            );
            array_push($array, $data_object);
        endforeach;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_data,
            'recordsFiltered' => $total_data,
            'data' => $array
        );

        echo json_encode($data);
    }

    public function getAllOutOfStock(Request $request,$wh_id,$category_id) {
        $code_url = request()->segment(1);
        $wh_id = $wh_id == "null" ? "" : $wh_id;
        $category_id = $category_id == "null" ? "" : $category_id;
        $user = Session::get('users');
        $data['user'] = $user;
        $organization_id = $user['organization_id'] == 1 ? null : $user['organization_id'];

        $draw = $request->get('draw');
        $start = $request->has('start') ? $request->get('start') : 0;
        $length = $request->has('length') ? $request->get('length') : 10;
        $search = $request->has('search') ? $request->get('search')['value'] : '';
        $stock_columns = $this->stock_columns;

        $order_column = $request->has('order') ? $request->get('order')[0]['column'] : '';
        $order_type = $request->has('order') ? $request->get('order')[0]['dir'] : '';

        if ($order_column == 0) {
            $column_name = "00_inventory.created_date";
            $order_type = "DESC";
        } else {
            $column_name = $request->get('columns')[$order_column]['data'];
        }

        $total_data = Inventory::select('00_inventory.inventory_id','00_product.prod_id','00_product.prod_code','00_product.prod_name','00_warehouse.warehouse_name','00_product.prod_price','00_inventory.stock','00_product.uom_value','00_uom.uom_name')
                        ->leftJoin('00_product','00_inventory.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                        ->leftJoin('00_warehouse','00_inventory.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                        ->when($wh_id, function ($query, $wh_id) {
                            return $query->where('00_inventory.warehouse_id', '=', $wh_id);
                        })
                        ->when($category_id, function ($query, $category_id) {
                            return $query->where('00_product.category_id', '=', $category_id);
                        })
                        ->when($organization_id, function ($query, $organization_id) {
                            return $query->where('00_inventory.organization_id', $organization_id);
                        })
                        ->where(function($query) use($stock_columns, $search){
                            for ($b=0; $b < count($stock_columns); $b++) {
                                if($b > 0){
                                    $query->orWhere($stock_columns[$b],'like','%'.$search.'%');
                                }else{
                                    $query->where($stock_columns[$b],'like','%'.$search.'%');
                                }
                            }
                        })
                        ->where('00_inventory.active_flag',1)
                        ->whereRaw('00_inventory.stock < 00_product.minimum_stock')
                        ->count();

        $query = Inventory::select('00_inventory.inventory_id','00_inventory.inventory_price','00_product.prod_id','00_product.prod_code','00_category.category_name','00_product.prod_code','00_product.prod_name','00_warehouse.warehouse_name','00_product.prod_price','00_inventory.stock','00_product.uom_value','00_uom.uom_name')
                        ->leftJoin('00_product','00_inventory.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_category','00_category.category_id','=','00_product.category_id')
                        ->leftJoin('00_warehouse','00_inventory.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                        ->leftJoin('00_product_warehouse', function($join){
                                         $join->on('00_inventory.prod_id', '=', '00_product_warehouse.prod_id');
                                         $join->on('00_inventory.warehouse_id', '=', '00_product_warehouse.warehouse_id');
                                     })
                        ->when($wh_id, function ($query, $wh_id) {
                            return $query->where('00_inventory.warehouse_id', '=', $wh_id);
                        })
                        ->when($category_id, function ($query, $category_id) {
                            return $query->where('00_product.category_id', '=', $category_id);
                        })
                        ->when($organization_id, function ($query, $organization_id) {
                            return $query->where('00_inventory.organization_id', $organization_id);
                        })
                        ->where(function($query) use($stock_columns, $search){
                            for ($b=0; $b < count($stock_columns); $b++) {
                                if($b > 0){
                                    $query->orWhere($stock_columns[$b],'like','%'.$search.'%');
                                }else{
                                    $query->where($stock_columns[$b],'like','%'.$search.'%');
                                }
                            }
                        })
                        ->where('00_inventory.active_flag',1)
                        ->whereRaw('00_inventory.stock < 00_product_warehouse.minimum_stock')
                        ->limit($length)
                        ->offset($start)
                        ->orderBy($column_name,$order_type)
                        ->get();

        $array = array();
        $no = $start + 1;
        foreach ($query as $row):
            $data_object = (object) array(
                'no' => $no++,
                'prod_code' => $row->prod_code,
                'category_name' => $row->category_name,
                'prod_name' => $row->prod_name,
                'prod_unit' => "$row->uom_value $row->uom_name",
                'warehouse_name' => $row->warehouse_name,
                'prod_price' => $row->inventory_price,
                'stock' => $row->stock,
                'total_price' => $row->inventory_price * $row->stock
            );
            array_push($array, $data_object);
        endforeach;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_data,
            'recordsFiltered' => $total_data,
            'data' => $array
        );

        echo json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProductByWhId($id, Request $request){
        $variant = $request->get('variant');
        $query = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                ->where('00_product_warehouse.warehouse_id',$id)
                                ->where('00_product.stockable_flag',1)
                                ->where('00_product.active_flag',1);
        // if($variant == null){
        //     $query->whereNull('00_product.parent_prod_id');
        // }else{
            // $query->whereNotNull('00_product.parent_prod_id')->where('00_product.stockable_flag',1)->whereIn('00_product.parent_prod_id', function($query2){
            //     $query2->select('00_inventory.prod_id')->from('00_inventory')->where('00_inventory.active_flag',1);
            // });
        // }
        $query = $query->get();

        $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $second_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $uom = $first_uom->union($second_uom)
            ->groupBy('00_uom.uom_id');

        $data = array('product' => $query, 'uom' => $uom->get());
        return response()->json($data);
    }

    public function getProductByWhStock($id, Request $request){
        $user = Session::get('users');
        $org_id = $user['organization_id'];
        $variant = $request->get('variant');
        $query = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                ->leftJoin('00_inventory', function($join){
                                                         $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                         $join->on('00_inventory.warehouse_id', '=', '00_product_warehouse.warehouse_id');
                                                         // $join->on('00_inventory.organization_id', '=', DB::raw($org_id));
                                                     })
                                ->where('00_product_warehouse.warehouse_id',$id)
                                ->where('00_product.stockable_flag',1)
                                ->where('00_inventory.organization_id',$org_id)
                                ->where('00_product.active_flag',1);
        // if($variant == null){
        //     $query->whereNull('00_product.parent_prod_id');
        // }else{
            // $query->whereNotNull('00_product.parent_prod_id')->where('00_product.stockable_flag',1)->whereIn('00_product.parent_prod_id', function($query2){
            //     $query2->select('00_inventory.prod_id')->from('00_inventory')->where('00_inventory.active_flag',1);
            // });
        // }
        $query = $query->get();

        $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $second_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $uom = $first_uom->union($second_uom)
            ->groupBy('00_uom.uom_id');

        $data = array('product' => $query, 'uom' => $uom->get());
        return response()->json($data);
    }

    public function getProductByWhTf($id, $id_to, Request $request){
        $user = Session::get('users');
        $org_id = $user['organization_id'];
        $variant = $request->get('variant');
        $warehouse_id = $id.','.$id_to;

        $product_form = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name','00_inventory.inventory_price','00_product.prod_code')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                ->leftJoin('00_inventory', function($join){
                                                         $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                                         $join->on('00_inventory.warehouse_id', '=', '00_product_warehouse.warehouse_id');
                                                         // $join->on('00_inventory.organization_id', '=', DB::raw($org_id));
                                                     })
                                ->where('00_product_warehouse.warehouse_id',$id)
                                ->where('00_product.stockable_flag',1)
                                ->where('00_inventory.organization_id',$org_id)
                                ->where('00_product.active_flag',1)
                                ->get();

        $product_to = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name','00_product.prod_code')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                // ->leftJoin('00_inventory', function($join){
                                //                          $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                                //                          $join->on('00_inventory.warehouse_id', '=', '00_product_warehouse.warehouse_id');
                                //                          // $join->on('00_inventory.organization_id', '=', DB::raw($org_id));
                                //                      })
                                ->where('00_product_warehouse.warehouse_id',$id_to)
                                ->where('00_product.stockable_flag',1)
                                // ->where('00_inventory.organization_id',$org_id)
                                ->where('00_product.active_flag',1)
                                ->get();

        // $string_query = "SELECT
        //     `00_product`.`prod_name`,
        //     `00_product`.`prod_id`,
        //     `00_product`.`uom_value`,
        //     `00_product`.`uom_id`,
        //     GROUP_CONCAT(`00_product_warehouse`.`warehouse_id`) as warehouse_id,
        //     `00_uom`.`uom_name`
        // FROM
        //     `00_product_warehouse`
        // LEFT JOIN `00_product` ON `00_product_warehouse`.`prod_id` = `00_product`.`prod_id`
        // LEFT JOIN `00_uom` ON `00_uom`.`uom_id` = `00_product`.`uom_id`
        // LEFT JOIN `00_inventory` ON `00_inventory`.`prod_id` = `00_product`.`prod_id`
        // AND `00_inventory`.`warehouse_id` = `00_product_warehouse`.`warehouse_id`
        // WHERE
        //     `00_product_warehouse`.`warehouse_id` in (".$warehouse_id.")
        // AND `00_product`.`stockable_flag` = 1
        // AND 00_inventory.organization_id = ".$org_id."
        // AND `00_product`.`active_flag` = 1
        // GROUP BY `00_product_warehouse`.`prod_id`";
        // $query = DB::table(DB::raw("({$string_query}) as sub"))
        //         ->where('sub.warehouse_id', '=', $warehouse_id)
        //         // ->orderBy('sub.last_connected', 'DESC')
        //         ->get();

        $first_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_first_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $second_uom = UOM::where('active_flag','=',1)
            ->join('00_uom_convertion', '00_uom.uom_id','=','00_uom_convertion.uom_second_id')
            ->select('00_uom.uom_id', '00_uom.uom_name');

        $uom = $first_uom->union($second_uom)
            ->groupBy('00_uom.uom_id');

        $data = array('product_form' => $product_form, 'product_to' => $product_to, 'uom' => $uom->get());
        return response()->json($data);
    }

    public function getProductByWhCatID($wh_id,$category_id){
        $wh_id = $wh_id == "null" ? "" : $wh_id;
        $category_id = $category_id == "null" ? "" : $category_id;

        $query = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                ->when($wh_id, function ($query, $wh_id) {
                                    return $query->where('00_product_warehouse.warehouse_id', '=', $wh_id);
                                })
                                ->when($category_id, function ($query, $category_id) {
                                    return $query->where('00_product.category_id', '=', $category_id);
                                })
                                // ->whereNull('00_product.parent_prod_id')
                                ->where('00_product.stockable_flag',1)
                                ->where('00_product.active_flag',1)
                                ->get();

        return response()->json($query);
    }

    public function getProductByWhCatIDUseParam($param_data){
        $param = json_decode($param_data);
        $wh_id = $param->wh_id;
        $category_id = $param->cat_id;

        $query = ProductWarehouse::select('00_product.prod_name','00_product.prod_id', '00_product.uom_value','00_product.uom_id', '00_uom.uom_name')
                                ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->leftJoin('00_uom','00_product.uom_id','=','00_uom.uom_id')
                                ->when($wh_id, function ($query, $wh_id) {
                                    return $query->where('00_product_warehouse.warehouse_id', '=', $wh_id);
                                })
                                ->when($category_id, function ($query, $category_id) {
                                    return $query->whereIn('00_product.category_id', $category_id);
                                })
                                // ->whereNull('00_product.parent_prod_id')
                                ->where('00_product.stockable_flag',1)
                                ->where('00_product.active_flag',1)
                                ->get();

        return response()->json($query);
    }

    public function getProductById($id){
        $query = Product::select('00_product.prod_price','00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.prod_id',$id)
                        ->first();

        $query_variant = Product::select('00_product.prod_price','00_product.prod_name as variant_name','00_product.prod_id as variant_id','00_product.prod_code','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.parent_prod_id',$id)
                        ->get();
        $query['variant'] = isset($query_variant[0]) ? $query_variant : null;

        return response()->json($query);
    }

    public function getProductByIdAndWh($id, $wh_Id){
        $query = Product::select('00_product.prod_price', '00_product_warehouse.prod_price as price_warehouse', '00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->leftJoin('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product_warehouse.warehouse_id',$wh_Id)
                        ->where('00_product.prod_id',$id)
                        ->first();

        $query_variant = Product::select('00_product.prod_price', '00_product_warehouse.prod_price as price_warehouse', '00_product.prod_name as variant_name','00_product.prod_id as variant_id','00_product.prod_code','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->leftJoin('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product_warehouse.warehouse_id',$wh_Id)
                        ->where('00_product.parent_prod_id',$id)
                        ->get();
        $query['variant'] = isset($query_variant[0]) ? $query_variant : null;

        return response()->json($query);
    }

    public function getProductByIdWh($id, $wh_id){
        $query = Product::select('00_product.prod_price','00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.warehouse_id = '.$wh_id.' AND (00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id) ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.prod_id',$id)
                        ->first();

        $query_variant = Product::select('00_product.prod_price','00_product.prod_name as variant_name','00_product.prod_id as variant_id','00_product.prod_code','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.warehouse_id = '.$wh_id.' AND (00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id) ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.parent_prod_id',$id)
                        ->get();
        $query['variant'] = isset($query_variant[0]) ? $query_variant : null;

        return response()->json($query);
    }

    public function listProductById($id){
        $check = Product::where('00_product.active_flag',1)
                        ->where('00_product.prod_id',$id)
                        ->first();
        $prod_id = isset($check->parent_prod_id) ? $check->parent_prod_id : $check->prod_id;

        $query_parent = Product::select('00_product.prod_price','00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.stockable_flag',1)
                        ->where('00_product.prod_id',$prod_id);

        $query_variant = Product::select('00_product.prod_price','00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->where('00_product.active_flag',1)
                        ->where('00_product.stockable_flag',1)
                        ->where('00_product.parent_prod_id',$prod_id);

        $query = $query_parent->union($query_variant)->get();
        // dd($query);

        // $query['variant'] = isset($query_variant[0]) ? $query_variant : null;

        return response()->json($query);
    }

    public function getProductCode($id){
        $query = Product::select('00_product.prod_name','00_product.prod_id','00_product.prod_code','00_inventory.stock as old_stock','00_inventory.stockable_flag','00_inventory.minimum_stock')
                        ->leftJoin('00_inventory', function($join){
                                $join->on('00_inventory.prod_id','=','00_product.prod_id')
                                ->where('00_inventory.active_flag', 1);
                            })
                        ->where('00_product.prod_id',$id)
                        ->first();

        return response()->json($query);
    }

    public function converterStock(Request $request) {
        $convert = new UomConvert;
        $product = Product::select('uom_id','uom_value')->where('prod_id','=',$request->input('productId'))->first();
        $finalStock = $convert->convertToStock($request->input('satuan_produk'), $request->input('inventory_stock'), $product->uom_id, $product->uom_value);
        return ($finalStock) ? response()->json($finalStock, 200) : response()->json($finalStock, 400);
    }

    public function stock_recording_history(Request $request,$id)
    {
        $user = Session::get('users');
        $data['user'] = $user;
        $user_menu = new MenuController();
        $accessed_menu = $user_menu->get($user['id'],28)[0]['access_status'];
        $all_menu = $user_menu->get($user['id']);
        $data['all_menu'] = $all_menu;
        $data['accessed_menu'] = $accessed_menu;

        $trx_header = TransactionHeader::select('10_transaction_header.transaction_header_id',
                        '10_transaction_header.transaction_header_code',
                        '10_transaction_header.organization_id',
                        '10_transaction_header.transaction_date',
                        '10_transaction_header.transaction_desc',
                        '10_transaction_header.transaction_status',
                        '10_transaction_header.category_id',
                        '10_transaction_header.updated_date',
                        '00_warehouse.warehouse_id',
                        '00_warehouse.warehouse_name',
                        '00_tracking_status.tracking_status_name',
                        '00_category.category_name'
                        )
                        ->leftJoin('00_warehouse','10_transaction_header.warehouse_id','=','00_warehouse.warehouse_id')
                        ->leftJoin('00_tracking_status','10_transaction_header.transaction_status','=','00_tracking_status.tracking_status_id')
                        ->leftJoin('00_category','10_transaction_header.category_id','=','00_category.category_id')
                        ->where('10_transaction_header.transaction_header_id',$id)
                        ->first();
        $category_name = '';
        if (isset($trx_header->category_id)) {
            $cat_op = explode(",",$trx_header->category_id);
            $cat_name = array();
            foreach ($cat_op as $value) {
                $data_cat = Category::where('active_flag',1)->where('category_id',$value)->first();
                array_push($cat_name, $data_cat->category_name);
            }
            $category_name = implode(", ",$cat_name);
        }

        $data['category_name'] = $category_name;
        $data['trx_header'] = $trx_header;
        $data['id_detail'] = $id;
        $logo = Config::get('logo.menubar');
        $favicon = Config::get('logo.favicon');
        $data['logo'] = $logo;
        $data['favicon'] = $favicon;
        return view('admin::inventory/stock/historyopname', $data);
    }

    public function stock_recording_history_detail(Request $request,$id)
    {
        $draw = $request->get('draw');
        $start = $request->has('start') ? $request->get('start') : 0;
        $length = $request->has('length') ? $request->get('length') : 10;
        $search = $request->has('search') ? $request->get('search')['value'] : '';

        $total = TransactionHeader::select('00_product.prod_name',
                                            '00_product.prod_code',
                                            '00_product.uom_value',
                                            '00_uom.uom_name',
                                            '10_input_opname.rack',
                                            '10_input_opname.stock',
                                            'opname_uom.uom_name as opname_uom'
                                            )
                                    ->leftJoin('10_input_opname','10_transaction_header.transaction_header_code','=','10_input_opname.transaction_number')
                                    ->leftJoin('00_product','10_input_opname.prod_id','=','00_product.prod_id')
                                    ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                    ->leftJoin('00_uom as opname_uom','opname_uom.uom_id','=','10_input_opname.uom_id')
                                    ->where('10_transaction_header.transaction_header_id',$id)
                                    ->count();

        $sql = TransactionHeader::select('00_product.prod_name',
                                    '00_product.prod_code',
                                    '00_product.uom_value',
                                    '00_uom.uom_name',
                                    '10_input_opname.rack',
                                    '10_input_opname.stock',
                                    'opname_uom.uom_name as opname_uom'
                                    )
                            ->leftJoin('10_input_opname','10_transaction_header.transaction_header_code','=','10_input_opname.transaction_number')
                            ->leftJoin('00_product','10_input_opname.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                            ->leftJoin('00_uom as opname_uom','opname_uom.uom_id','=','10_input_opname.uom_id')
                            ->where('10_transaction_header.transaction_header_id',$id)
                            ->limit($length)
                            ->offset($start)
                            ->get();

        $no = 1;
        $data = array();
        foreach ($sql as $row):
            $data_object = (object) array(
                'no' => $no++,
                'prod_name' => $row->prod_name,
                'prod_code' => $row->prod_code,
                'prod_unit' => "$row->uom_value $row->uom_name",
                'rack' => $row->rack,
                'stock' => $row->stock,
                'opname_uom' => $row->opname_uom,
            );
            array_push($data, $data_object);
        endforeach;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $data
        );

        echo json_encode($data);
    }

    public function importIncoming(Request $request)
    {
        // dd($request->all());
        $res = array();
        $res_fail = array();
        $status = 500;

        try {
            //code...
            $file = $request->file('file');
            $status = 200;
            $collection = Excel::toCollection(new InventoryImport, $file);
            foreach ($collection[0] as $key => $row) {
                if (isset($row[0]) or $row[0] != '') :
                    $prod_code = $row[0];
                    $check_prod = Product::select('00_product.prod_price','00_product.prod_name','00_product.prod_id','00_product.prod_code','00_brand.brand_id','00_brand.brand_name','00_product.uom_value','00_product.uom_id','00_uom.uom_name',DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                                ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                                ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                                ->leftJoin('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
                                ->where('00_product_warehouse.warehouse_id',$request->wh_id)
                                ->where('00_product.active_flag',1)
                                ->where('00_product.stockable_flag',1)
                                ->where('00_product.prod_code',$prod_code)
                                ->first();

                    $prod_id = '';
                    $prod_uom_id = '';
                    $prod_uom_name = '';
                    $prod_uom_value = '';
                    $prod_name = $row[1];
                    if (isset($check_prod)) {
                        $prod_id = $check_prod->prod_id;
                        $prod_name = $check_prod->prod_name.'('.$check_prod->uom_value.' '.$check_prod->uom_name.')';
                        $prod_uom_id = $check_prod->uom_id;
                        $prod_uom_name = $check_prod->uom_name;
                        $prod_uom_value = $check_prod->uom_value;
                        $harga_beli = isset($row[2]) ? $row[2] : 0;
                        $stock = isset($row[3]) ? $row[3] : 0;

                        $uom = $row[4];
                        $uom_id = '';
                        $check_uom = DB::table('00_uom')
                                    ->where('uom_name',$uom)
                                    ->first();
                        if (isset($check_uom)) {
                            $uom_id = $check_uom->uom_id;
                            $uom_av = 1;

                            $convert = new UomConvert;
                            $finalStock = $convert->convertToStock($uom_id, $stock, $prod_uom_id, $prod_uom_value);

                            if (is_numeric($finalStock)) {
                                if ($stock > 0 and $harga_beli > 0) {
                                    $data_arr = (object)array(
                                        'prod_code' => $prod_code,
                                        'prod_id' => $prod_id,
                                        'prod_name' => $prod_name,
                                        'harga_beli' => $harga_beli,
                                        'stock' => $stock,
                                        'uom' => $uom,
                                        'uom_id' => $uom_id,
                                        'prod_uom_name' => $prod_uom_name,
                                        'finalStock' => $finalStock,
                                    );
                                    array_push($res, $data_arr);
                                } else {
                                    if (isset($prod_code) or isset($prod_name)) {
                                        $data_fail = (object)array(
                                            'prod_code' => $prod_code,
                                            'prod_name' => $prod_name,
                                            'message' => 'Colom Stok atau Harga Beli Kosong Di excel',
                                        );
                                        array_push($res_fail, $data_fail);
                                    }
                                }
                            } else if (is_array($finalStock)){
                                $data_fail = (object)array(
                                    'prod_code' => $prod_code,
                                    'prod_name' => $prod_name,
                                    'message' => $finalStock['message'],
                                );
                                array_push($res_fail, $data_fail);
                            }
                        } else {
                            $data_fail = (object)array(
                                        'prod_code' => $prod_code,
                                        'prod_name' => $prod_name,
                                        'message' => 'Berat produk tidak sesuai.'
                                    );
                                    array_push($res_fail, $data_fail);
                        }
                    } else {
                        if (isset($prod_code) or isset($prod_name)) {
                            $data_fail = (object)array(
                                'prod_code' => $prod_code,
                                'prod_name' => $prod_name,
                                'message' => 'Produk Non Stok atau bukan warehouse tersebut.',
                            );
                            array_push($res_fail, $data_fail);
                        }
                    }
                endif;
            }
            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);
    }
    //----------------------------INTEGRATION TOKPED
    public function validateUpdateStockTokped($prodId,$stock,$org,$warehouse){
    //---------------check global config
        $checkInventory=DB::table('00_inventory')
        ->select('booking_qty','goods_in_transit','goods_arrived')
        ->where([
            ['prod_id',$prodId],
            ['warehouse_id',$warehouse],
            ['organization_id',$org]
        ])->first();
        $config=DB::table('99_global_parameter')
        ->where('global_parameter_name','tokopedia')
        ->select('global_parameter_desc')
        ->first();
        if($config->global_parameter_desc=='on'){
            $product =$this->checkProduct($prodId);
            if($product!=null){
                $namaToko=$this->getGlobalParameter('tokped_shop');
                $shopId=$this->getOrgTokped($namaToko->global_parameter_value);
                // $warehouseTokped=$this->getWarehouseTokped();
                $currentWarehouse=$this->checkCurrentWarehouse($warehouse);
                if($org==$shopId->parent_organization_id&&$currentWarehouse==$shopId->warehouse_id){
                    //---------------------------------------------get cold storage
                    $totalStock=$this->getStockColdStorage($org,$currentWarehouse,$prodId);
                    $url = config('global.tokopedia_integrator_url').'/updateProductStock?productIdsOrSkus='.$product->prod_ecom_code.'&productStocks='.$totalStock.'&shopId='.$shopId->organization_code;
                   $token=$this->generateToken('user_token_integrator');
                   try {
                       Http::withToken($token)->timeout(10)->retry(2, 1000)->post($url);
                   } catch (ConnectionException $e) {
                       return null;
                   }
                }
            }
        }
    }
    public function checkCurrentWarehouse($id){
        $data=DB::table('00_warehouse')
        ->where('warehouse_id',$id)
        ->select('warehouse_id','parent_warehouse_id')
        ->first();
        $warehouse=$data->parent_warehouse_id == null ? $data->warehouse_id : $data->parent_warehouse_id;
        return $warehouse;
    }
    public function checkProduct($id){
        $data = DB::table('00_product_ecommerce AS p')
        ->select('p.prod_ecom_code','o.organization_code')
        ->join('00_organization AS o','o.organization_id','=','p.organization_id')
        ->where('p.prod_id',$id)
        ->orderBy('p.prod_ecom_id','desc')
        ->first();
        return $data;
    }
    public function getGlobalParameter($param){
        return DB::table('99_global_parameter')
        ->select('global_parameter_value','global_parameter_desc','global_parameter_code')
        ->where('global_parameter_name','=',$param)
        ->first();
    }
    public function generateToken($globalParam){
        $credential=$this->getGlobalParameter($globalParam);
        $url=config('global.integrator_url_token').'/authenticate?username='.$credential->global_parameter_value.'&password='.$credential->global_parameter_code;
        try {
            $getToken = Http::timeout(3)->retry(2, 100)->get($url)->json();
            $getAuth = explode(" ",$getToken['data']['token']);
            $token = array_pop($getAuth);
        } catch (ConnectionException $e) {
            $token=null;
        }
        return $token;
    }
    public function getOrgTokped($shopname){
        $org=DB::table('00_organization')
        ->select('organization_id','organization_name','organization_desc','organization_code','parent_organization_id','warehouse_id')
        ->where('organization_name','=',$shopname)
        ->first();
        return $org;
    }
    public function getWarehouseTokped(){
        $warehouseTokped=DB::table('00_organization')
        ->select('warehouse_id')
        ->where('organization_name','TOKOPEDIA')
        ->first();
        return $warehouseTokped->warehouse_id;
    }
    public function getStorage($warehouseId){
        $warehouseType=$this->checkWarehouseType($warehouseId);
        if($warehouseType->warehouse_type_id==2){
            $paramWarehouseId='parent_warehouse_id';
            $paramWarehouseValue=$warehouseId;
        }else{
            $paramWarehouseId='warehouse_id';
            $paramWarehouseValue=$warehouseType->parent_warehouse_id;
        }
        $warehouseList=DB::table('00_warehouse')
        ->where($paramWarehouseId,$paramWarehouseValue)
        ->select('warehouse_id','warehouse_type_id')
        ->get();
        return $warehouseList;
    }
    public function checkInventoryColdStorage($orgId,$coldStorageId,$prodId){
        $coldInventory=DB::table('00_inventory')
        ->where([
            ['organization_id',$orgId],
            ['prod_id',$prodId]
        ])
        ->whereIn('warehouse_id',$coldStorageId)
        ->select('stock','booking_qty','goods_in_transit','goods_arrived')
        ->get();
        return $coldInventory;
    }
    public function getStockColdStorage($orgId,$coldStorageId,$prodId){
        $coldStock=0;
        // $coldStorageList=$this->getStorage($coldStorageId);
        $warehouseList=$this->checkWarehouseType($coldStorageId);
        $warehouseArray=array();
        for($i=0;$i<count($warehouseList);$i++){
            $warehouseArray[$i]=$warehouseList[$i]->warehouse_id;
        }
        $coldStorageInventory=$this->checkInventoryColdStorage($orgId,$warehouseArray,$prodId);
        for($i=0;$i<count($coldStorageInventory);$i++){
            $coldStock+=floor($coldStorageInventory[$i]->stock-$coldStorageInventory[$i]->booking_qty-$coldStorageInventory[$i]->goods_in_transit-$coldStorageInventory[$i]->goods_arrived);
        }
        return $coldStock;
    }
    public function checkWarehouseType($warehouseId){
        $warehouseType=DB::table('00_warehouse')
        ->orWhere('warehouse_id',$warehouseId)
        ->orWhere('parent_warehouse_id',$warehouseId)
        ->select('warehouse_id')
        ->get()->toArray();
        return $warehouseType;
    }
    //----------------------------INTEGRATION TOKPED

    public function importStokOpname(Request $request){
        $res = array();
        $res_fail = array();
        $status = 500;

        try {
            //code...
            $file = $request->file('file');
            $wh_id = $request->wh_id;
            $category_id = explode(",",$request->cat_id);
            $collection = Excel::toCollection(new InventoryImport, $file);
            $user = Session::get('users');
            $org_id = $user['organization_id'];
            $data_org = DB::table('00_organization as a')->where('a.organization_id', $org_id)->first();
            if (isset($data_org->parent_organization_id)) {
                $org_id = $data_org->parent_organization_id;
            }

            foreach ($collection[0] as $key => $row) {
                if (isset($row[0]) and !empty($row[0]) and $row[0] != '') :
                    $prod_code = trim($row[0]);
                    $prod_name = trim($row[1]);
                    $rack_no = trim($row[2]);
                    $stock = trim($row[3]);
                    $uom = trim($row[4]);

                // cek product
                    $check_prod = Product::select(
                        '00_product.prod_price',
                        '00_product.prod_name',
                        '00_product.prod_id', //
                        '00_product.prod_code', //
                        '00_brand.brand_id',
                        '00_brand.brand_name',
                        '00_product.uom_value', //
                        '00_product.category_id', //
                        '00_product_warehouse.warehouse_id', //
                        '00_product.uom_id', //
                        '00_uom.uom_name', //
                        DB::raw('(SELECT 00_inventory.inventory_price FROM 00_inventory WHERE 00_inventory.prod_id = 00_product.parent_prod_id OR 00_inventory.prod_id = 00_product.prod_id ORDER BY 00_product.parent_prod_id ASC LIMIT 1) AS inventory_price'))
                        ->leftJoin('00_brand','00_brand.brand_id','=','00_product.brand_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->leftJoin('00_product_warehouse','00_product_warehouse.prod_id','=','00_product.prod_id')
                        ->where('00_product_warehouse.warehouse_id',$wh_id)
                        // ->where('00_product.category_id',$category_id)
                        ->where('00_product.active_flag',1)
                        ->where('00_product.stockable_flag',1)
                        ->where('00_product.prod_code',$prod_code) // dari excel
                        ->first();

                    $prod_id = '';
                    $prod_uom_id = '';
                    $prod_uom_name = '';
                    $prod_uom_value = '';
                    if (isset($check_prod)) {
                        $prod_id = $check_prod->prod_id;
                        $prod_name = $check_prod->prod_name;
                        $prod_uom_id = $check_prod->uom_id;
                        $prod_uom_name = $check_prod->uom_name;
                        $prod_uom_value = $check_prod->uom_value;
                        $check_category = 1;
                        if ($category_id[0] != "" and count($category_id) > 0) {
                            if (!in_array($check_prod->category_id, $category_id)) {
                                $check_category = 0;
                            }
                        }

                        if ($check_category <= 0) {
                           $data_fail = (object)array(
                                'prod_code' => $prod_code,
                                'prod_name' => $prod_name,
                                'message' => 'Produk bukan kategori tersebut.',
                            );
                            array_push($res_fail, $data_fail);
                        } else {
                            $check_inven = Inventory::where([
                                                ['organization_id', '=', $org_id],
                                                ['warehouse_id', '=', $wh_id],
                                                ['prod_id', '=', $prod_id],
                                                ['active_flag', '=', 1],
                                            ])
                                            ->first();
                            if (isset($check_inven)) {
                                // cek UOM di db?
                                $uom_id = '';
                                $check_uom = DB::table('00_uom')
                                    ->where('uom_name',$uom)
                                    ->first();
                                if (isset($check_uom)) {
                                    $uom_id = $check_uom->uom_id;
                                    $uom_av = 1;
                                    // convert UOM
                                    $convert = new UomConvert;
                                    $finalStock = $convert->convertToStock($uom_id, $stock, $prod_uom_id, $prod_uom_value);

                                    if (is_numeric($finalStock)) {
                                        if ($stock > 0) {
                                            $data_arr = (object)array(
                                                'prod_id' => $prod_id,
                                                'prod_name' => $prod_name,
                                                'prod_uom_value' => $prod_uom_value,
                                                'prod_uom_name' => $prod_uom_name,
                                                'prod_code' => $prod_code,
                                                'opname_rack' => $rack_no,
                                                'opname_stock' => $stock,
                                                'final_stock' => $finalStock,
                                                'uom_id' => $uom_id,
                                            );
                                            array_push($res, $data_arr);
                                        } else {
                                            if (isset($prod_code) or isset($prod_name)) {
                                                $data_fail = (object)array(
                                                    'prod_code' => $prod_code,
                                                    'prod_name' => $prod_name,
                                                    'message' => 'Colom Stok atau Harga Beli Kosong Di excel',
                                                );
                                                array_push($res_fail, $data_fail);
                                            }
                                        }
                                    } else if (is_array($finalStock)){
                                        $data_fail = (object)array(
                                            'prod_code' => $prod_code,
                                            'prod_name' => $prod_name,
                                            'message' => $finalStock['message'],
                                        );
                                        array_push($res_fail, $data_fail);
                                    }
                                } else {
                                    if (isset($prod_code) or isset($prod_name)) {
                                        $data_fail = (object)array(
                                            'prod_code' => $prod_code,
                                            'prod_name' => $prod_name,
                                            'message' => 'Berat produk tidak sesuai.',
                                        );
                                        array_push($res_fail, $data_fail);
                                    }
                                }
                            } else {
                                $data_fail = (object)array(
                                    'prod_code' => $prod_code,
                                    'prod_name' => $prod_name,
                                    'message' => 'Produk belum memiliki stok, silahkan melakukan barang masuk terlebih dahulu.',
                                );
                                array_push($res_fail, $data_fail);
                            }
                        }
                    } else {
                        if (isset($prod_code) or isset($prod_name)) {
                            $data_fail = (object)array(
                                'prod_code' => $prod_code,
                                'prod_name' => $prod_name,
                                'message' => 'Produk non stok atau bukan warehouse tersebut.',
                            );
                            array_push($res_fail, $data_fail);
                        }
                    }
                endif;
            }
            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);
    }

    public function importTransferStok(Request $request){
        $res = array();
        $res_fail = array();
        $status = 500;

        try {
            //code...
            $file = $request->file('file');
            $wh_id = $request->wh_id;
            $wh_id_to = $request->wh_id_to;
            $collection = Excel::toCollection(new InventoryImport, $file);

            foreach ($collection[0] as $key => $row) {
                if (isset($row[0]) and !empty($row[0]) and $row[0] != '') :
                    $prod_code = trim($row[0]);
                    $prod_name = trim($row[1]);
                    $prod_code_to = trim($row[2]);
                    $prod_name_to = trim($row[3]);
                    $stock = trim($row[4]);
                    $uom = trim($row[5]);
                    $to_stock = trim($row[6]);
                    $to_uom = trim($row[7]);

                    $user = Session::get('users');
                    $org_id = $user['organization_id'];

                    // cek gudang
                    $product_form = ProductWarehouse::select(
                            '00_product.prod_name',
                            '00_product.prod_id',
                            '00_product.uom_value',
                            '00_product.uom_id',
                            '00_uom.uom_name',
                            '00_inventory.inventory_price',
                            '00_product.prod_code')
                        ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                        ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                        ->leftJoin('00_inventory', function($join){
                            $join->on('00_inventory.prod_id', '=', '00_product.prod_id');
                            $join->on('00_inventory.warehouse_id', '=', '00_product_warehouse.warehouse_id');
                            // $join->on('00_inventory.organization_id', '=', DB::raw($org_id));
                        })
                        ->where('00_product_warehouse.warehouse_id',$wh_id)
                        ->where('00_product.stockable_flag',1)
                        ->where('00_inventory.organization_id',$org_id)
                        ->where('00_product.active_flag',1)
                        ->where('00_product.prod_code', '=', $prod_code)
                        ->first();

                    $prod_id = '';
                    $prod_uom_id = '';
                    $prod_uom_name = '';
                    $prod_uom_value = '';
                    if (isset($product_form)) {
                        $prod_id = $product_form->prod_id;
                        $prod_name = $product_form->prod_name;
                        $prod_uom_id = $product_form->uom_id;
                        $prod_uom_name = $product_form->uom_name;
                        $prod_uom_value = $product_form->uom_value;
                        $prod_inventory_price = $product_form->inventory_price;

                        // cek gudang tujuan
                        $product_to = ProductWarehouse::select(
                                '00_product.prod_name',
                                '00_product.prod_id',
                                '00_product.uom_value',
                                '00_product.uom_id',
                                '00_uom.uom_name',
                                '00_product.prod_code')
                            ->leftJoin('00_product','00_product_warehouse.prod_id','=','00_product.prod_id')
                            ->leftJoin('00_uom','00_uom.uom_id','=','00_product.uom_id')
                            ->where('00_product_warehouse.warehouse_id',$wh_id_to)
                            ->where('00_product.stockable_flag',1)
                            // ->where('00_inventory.organization_id',$org_id)
                            ->where('00_product.active_flag',1)
                            ->where('00_product.prod_code', '=', $prod_code_to)
                            ->first();

                        if (isset($product_to)) {
                            $prod_id_to = $product_to->prod_id;
                            $prod_name_to = $product_to->prod_name;
                            $prod_uom_id_to = $product_to->uom_id;
                            $prod_uom_name_to = $product_to->uom_name;
                            $prod_uom_value_to = $product_to->uom_value;

                            // cek UOM from
                            $uom_id = '';
                            $check_uom = DB::table('00_uom')
                                ->where('uom_name', $uom)
                                ->first();

                            // cek UOM to
                            $uom_id_to = '';
                            $check_uom_to = DB::table('00_uom')
                                ->where('uom_name', $to_uom)
                                ->first();

                            if (isset($check_uom) && isset($check_uom_to)) {
                                $uom_id = $check_uom->uom_id;
                                $uom_id_to = $check_uom_to->uom_id;
                                $uom_av = 1;


                                // convert UOM
                                $convert = new UomConvert;
                                $finalStock = $convert->convertToStock($uom_id, $stock, $prod_uom_id, $prod_uom_value);
                                $finalStock_to = $convert->convertToStock($uom_id_to, $to_stock, $prod_uom_id_to, $prod_uom_value_to);

                                if (is_numeric($finalStock) && is_numeric($finalStock_to)) {
                                    // cek stok from & to > 0
                                    if ($stock > 0 and $to_stock > 0) {
                                        $data_arr = (object)array(
                                            'prod_id' => $prod_id,
//                                            'prod_code' => $prod_code,
                                            'prod_inventory_price' => $prod_inventory_price,
//                                            'prod_name' => $prod_name,
//                                            'prod_uom_value' => $prod_uom_value,
//                                            'prod_uom_name' => $prod_uom_name,
                                            'prod_id_to' => $prod_id_to,
//                                            'prod_code_to' => $prod_code_to,
//                                            'prod_name_to' => $prod_name_to,
//                                            'prod_uom_value_to' => $prod_uom_value_to,
//                                            'prod_uom_name_to' => $prod_uom_name_to,
                                            'uom_id' => $uom_id,
                                            'uom_id_to' => $uom_id_to,
                                            'stock' => $stock,
                                            'stock_to' => $to_stock,
                                            'final_stock' => $finalStock,
                                            'final_stock_to' => $finalStock_to,
                                        );
                                        array_push($res, $data_arr);
                                    } else {
                                        if (isset($prod_code) or isset($prod_name)) {
                                            $data_fail = (object)array(
                                                'prod_code' => $prod_code,
                                                'prod_name' => $prod_name,
                                                'prod_code_to' => $prod_code_to,
                                                'prod_name_to' => $prod_name_to,
                                                'message' => 'Colom Stok atau Harga Beli Kosong Di excel',
                                            );
                                            array_push($res_fail, $data_fail);
                                        }
                                    }
                                } else if (is_array($finalStock)) {
                                    $data_fail = (object)array(
                                        'prod_code' => $prod_code,
                                        'prod_name' => $prod_name,
                                        'prod_code_to' => $prod_code_to,
                                        'prod_name_to' => $prod_name_to,
                                        'message' => $finalStock['message'],
                                    );
                                    array_push($res_fail, $data_fail);
                                }
                            } else {
                                if (isset($prod_code) or isset($prod_name)) {
                                    $data_fail = (object)array(
                                        'prod_code' => $prod_code,
                                        'prod_name' => $prod_name,
                                        'prod_code_to' => $prod_code_to,
                                        'prod_name_to' => $prod_name_to,
                                        'message' => 'Berat produk tidak sesuai.',
                                    );
                                    array_push($res_fail, $data_fail);
                                }
                            }
                        } else {
                            if (isset($prod_code) or isset($prod_name)) {
                                $data_fail = (object)array(
                                    'prod_code' => $prod_code,
                                    'prod_name' => $prod_name,
                                    'prod_code_to' => $prod_code_to,
                                    'prod_name_to' => $prod_name_to,
                                    'message' => 'Tujuan Produk Non Stok atau bukan warehouse tersebut.',
                                );
                                array_push($res_fail, $data_fail);
                            }
                        }
                    } else {
                        if (isset($prod_code) or isset($prod_name)) {
                            $data_fail = (object)array(
                                'prod_code' => $prod_code,
                                'prod_name' => $prod_name,
                                'prod_code_to' => $prod_code_to,
                                'prod_name_to' => $prod_name_to,
                                'message' => 'Produk Non Stok atau bukan warehouse tersebut.',
                            );
                            array_push($res_fail, $data_fail);
                        }
                    }
                endif;
            }
            $result = (object)array(
                'isSuccess' => true,
                'title' => 'Success',
                'data' => $res,
                'data_fail' => $res_fail,
                'message' => 'Get Data Import!',
                'icon' => 'success'
            );
        } catch (\Exception $e) {
            $result = (object)array(
                'isSuccess' => false,
                'title' => 'Failed',
                'message' => 'Ups, Something Went Wrong !!!',
                'message_error' => $e->getMessage(),
                'icon' => 'warning'
            );
        }
        return response()->json($result);




    }
}
