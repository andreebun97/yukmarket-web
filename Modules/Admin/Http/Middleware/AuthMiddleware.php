<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Auth;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Session::get('users');
        if ($user == null) {
            return redirect('admin/auth/login');
        }else if($user['role_status'] == 'customer'){
            Session::flush();
            return redirect()->back();
        }
        return $next($request);
    }
}
