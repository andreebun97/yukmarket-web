<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/admin', function (Request $request) {
    return $request->user();
});

Route::prefix('tokopedia')->group(function(){
    // Route::get('getProductWeight/{category}','PushProductTokpedController@getBeratProductTokped');
    Route::post('push','TokopediaController@push')->name('admin/tokopedia/push');
    Route::post('pushByObject','TokopediaController@pushByObject')->name('admin/tokopedia/pushByObject');
});