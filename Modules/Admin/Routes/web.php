<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {
    Route::get('/', function(){
        return redirect('admin/dashboard');
    })->name('admin');
    Route::get('create/{role?}', 'AdminController@create')->name('admin/create');
    Route::get('edit/{user?}', 'AdminController@edit')->name('admin/edit');
    Route::get('activate/{user?}', 'AdminController@activate')->name('admin/activate');
    Route::get('deactivate/{user?}', 'AdminController@destroy')->name('admin/deactivate');
    Route::get('dashboard', 'DashboardController@index')->name('admin/dashboard');
    Route::prefix('product')->group(function(){
        Route::get('/', 'ProductController@index')->name('admin/product');
        Route::get('get','ProductController@get')->name('admin/product/get');
        Route::get('sort','ProductController@sort')->name('admin/product/sort');
        Route::get('create', 'ProductController@create')->name('admin/product/create');
        Route::get('activate/{id?}', 'ProductController@activate')->name('admin/product/activate');
        Route::get('deactivate/{id?}', 'ProductController@deactivate')->name('admin/product/deactivate');
        Route::get('delete/{id?}','ProductController@delete')->name('admin/product/delete');
        Route::get('edit/{product?}', 'ProductController@edit')->name('admin/product/edit');
        Route::post('form','ProductController@form')->name('admin/product/form');
        Route::prefix('promo')->group(function(){
            Route::get('create', 'PromoController@create')->name('admin/product/promo/create');
            Route::get('edit/{id?}', 'PromoController@edit')->name('admin/product/promo/edit');
            Route::post('form', 'PromoController@form')->name('admin/product/promo/form');
        });
        Route::post('checkUOM', 'ProductController@checkUom')->name('admin/product/checkUOM');
        Route::get('/prodWh/{id}', 'ProductController@prodWh');
        Route::get('export','ProductController@export')->name('admin/product/export');
        Route::post('variant','ProductController@addVariantForm')->name('admin/product/variant');
        Route::post('importProduct', 'ProductController@importProduct')->name('admin/product/importProduct');
        Route::post('importProductVariant', 'ProductController@importProductVariant')->name('admin/product/importProductVariant');
        Route::get('changeVariant','ProductController@changeVariant')->name('admin/product/changeVariant');
        Route::post('changeVariantForm','ProductController@changeVariantForm')->name('admin/product/changeVariantForm');
        Route::post('changeVariantPricePerWarehouse','ProductController@changeVariantPricePerWarehouse')->name('admin/product/changeVariantPricePerWarehouse');
    });
    Route::get('order', 'OrderController@index')->name('admin/order');
    Route::get('shipment', 'ShipmentController@index')->name('admin/shipment');
    Route::post('form', 'AdminController@form')->name('admin/form');
    Route::prefix('list')->group(function(){
        Route::get('/', 'AdminController@list')->name('admin/list');
        Route::get('{role?}', 'AdminController@list')->name('admin/list');
    });
    Route::prefix('cashback')->group(function(){
        Route::get('/','CashbackController@index')->name('admin/cashback');
        Route::get('detail/{id?}','CashbackController@detail')->name('admin/cashback/detail');
    });
    Route::prefix('role')->group(function(){
        Route::get('/','RoleController@index')->name('admin/role');
        Route::get('create','RoleController@create')->name('admin/role/create');
        Route::get('edit/{id?}','RoleController@edit')->name('admin/role/edit');
        Route::get('activate/{id?}','RoleController@activate')->name('admin/role/activate');
        Route::get('deactivate/{id?}','RoleController@deactivate')->name('admin/role/deactivate');
        Route::post('form','RoleController@form')->name('admin/role/form');
        Route::post('get','RoleController@get')->name('admin/role/get');
    });
    Route::prefix('menu_access')->group(function(){
        Route::post('form','RoleController@menu_access')->name('admin/menu_access/form');
    });
    Route::prefix('brand')->group(function(){
        Route::get('/','BrandController@index')->name('admin/brand');
        Route::get('create','BrandController@create')->name('admin/brand/create');
        Route::get('edit/{id?}','BrandController@edit')->name('admin/brand/edit');
        Route::get('activate/{id?}','BrandController@activate')->name('admin/brand/activate');
        Route::get('deactivate/{id?}','BrandController@deactivate')->name('admin/brand/deactivate');
        Route::post('form','BrandController@form')->name('admin/brand/form');
    });
    Route::prefix('profile')->group(function(){
        Route::get('/','ProfileController@index')->name('admin/profile');
        Route::post('change','ProfileController@change')->name('admin/profile/change');
    });
    Route::prefix('password')->group(function(){
        Route::get('/','ProfileController@password')->name('admin/password');
        Route::post('change','ProfileController@change_password')->name('admin/password/change');
    });
    Route::prefix('auth')->group(function(){
        Route::prefix('login')->group(function(){
            Route::get('/','AuthController@login')->name('admin/auth/login');
            Route::post('/','AuthController@authenticate')->name('admin/login');
        });
        Route::prefix('password')->group(function(){
            Route::prefix('forgot')->group(function(){
                Route::get('/','AuthController@forgot_password')->name('admin/auth/password/forgot');
                Route::post('/','AuthController@forgot_password_post')->name('admin/auth/password/forgot');
            });
            Route::prefix('otp')->group(function(){
                Route::get('/','AuthController@otp')->name('admin/auth/password/otp');
                Route::post('/','AuthController@input_otp')->name('admin/auth/password/otp');
            });
            Route::prefix('input')->group(function(){
                Route::get('/','AuthController@password')->name('admin/auth/password/input');
                Route::post('/','AuthController@input_password')->name('admin/auth/password/input');
            });
            // Route::post('/','AuthController@authenticate')->name('admin/login');
        });
    });
    Route::get('logout','AuthController@logout')->name('admin/logout');
    Route::prefix('yukmarket')->group(function(){
        Route::get('/',function(){
            return redirect('admin/yukmarket/list');
        })->name('admin/yukmarket');
        Route::post('filter','AdminController@filter')->name('admin/yukmarket/filter');
        Route::get('list/{role?}','AdminController@yukmarket')->name('admin/yukmarket/list');
        Route::prefix('user')->group(function(){
            Route::prefix('admin')->group(function(){
                Route::get('{id?}','AdminController@user_detail')->name('admin/yukmarket/user/admin');
                Route::post('form','AdminController@yukmarket_admin_form')->name('admin/yukmarket/user/admin/form');
            });
            Route::prefix('customer')->group(function(){
                Route::get('{id?}','AdminController@user_detail')->name('admin/yukmarket/user/customer');
                Route::post('form','AdminController@yukmarket_customer_form')->name('admin/yukmarket/user/customer/form');
            });
        });
    });
    Route::prefix('category')->group(function(){
        Route::get('/','CategoryController@index')->name('admin/category');
        Route::get('get/{id?}','CategoryController@get')->name('admin/category/get');
        Route::get('create','CategoryController@create')->name('admin/category/create');
        Route::get('edit/{category?}','CategoryController@edit')->name('admin/category/edit');
        Route::post('form','CategoryController@form')->name('admin/category/form');
        Route::get('deactivate/{category?}','CategoryController@delete')->name('admin/category/deactivate');
        Route::get('activate/{category?}','CategoryController@activate')->name('admin/category/activate');
    });
    Route::prefix('mapping_coverage')->group(function(){
        Route::get('/','MappingCoverageController@index')->name('admin/mapping_coverage');
        Route::get('create','MappingCoverageController@create')->name('admin/mapping_coverage/create');
        Route::get('activate/{id?}','MappingCoverageController@activate')->name('admin/mapping_coverage/activate');
        Route::get('deactivate/{id?}','MappingCoverageController@deactivate')->name('admin/mapping_coverage/deactivate');
        Route::get('delete/{id?}','MappingCoverageController@delete')->name('admin/mapping_coverage/delete');
        Route::get('edit/{id?}','MappingCoverageController@edit')->name('admin/mapping_coverage/edit');
        Route::post('form','MappingCoverageController@form')->name('admin/mapping_coverage/form');
    });
    Route::prefix('tokopedia')->group(function(){
        Route::get('getProductWeight/{category}','PushProductTokpedController@getBeratProductTokped');
        // Route::post('push','TokopediaController@push')->name('admin/tokopedia/push');
    });
    Route::prefix('banner')->group(function(){
        Route::get('/','BannerController@index')->name('admin/banner');
        Route::get('create','BannerController@create')->name('admin/banner/create');
        Route::get('edit/{id?}','BannerController@edit')->name('admin/banner/edit');
        Route::post('form','BannerController@form')->name('admin/banner/form');
        Route::get('deactivate/{id?}','BannerController@destroy')->name('admin/banner/deactivate');
        Route::get('activate/{id?}','BannerController@activate')->name('admin/banner/activate');
    });
    Route::prefix('uom')->group(function(){
        Route::get('/','UomController@index')->name('admin/uom');
        Route::get('create','UomController@create')->name('admin/uom/create');
        Route::get('edit/{id?}','UomController@edit')->name('admin/uom/edit');
        Route::get('deactivate/{id?}','UomController@destroy')->name('admin/uom/deactivate');
        Route::get('activate/{id?}','UomController@activate')->name('admin/uom/activate');
        Route::post('form','UomController@form')->name('admin/uom/form');
        Route::post('conversion/form','UomController@UomConvertionForm')->name('admin/uom/conversion/form');
        Route::get('get','UomController@getUom')->name('admin/uom/get');
    });
    Route::prefix('tag')->group(function(){
        Route::get('/','TagController@index')->name('admin/tag');
        Route::get('create','TagController@create')->name('admin/tag/create');
        Route::get('edit/{id?}','TagController@edit')->name('admin/tag/edit');
        Route::get('activate/{id?}','TagController@activate')->name('admin/tag/activate');
        Route::post('form','TagController@form')->name('admin/tag/form');
        Route::get('deactivate/{id?}','TagController@deactivate')->name('admin/tag/deactivate');
    });
    // Route::prefix('promo')->group(function(){
    //     Route::get('activate/{id?}','PromoController@activate')->name('admin/promo/activate');
    //     Route::get('deactivate/{id?}','PromoController@deactivate')->name('admin/promo/deactivate');
    //     Route::post('form','PromoController@form')->name('admin/promo/form');
    // });
    Route::prefix('transaction')->group(function(){
        Route::get('/','TransactionController@index')->name('admin/transaction');
        Route::get('offline','TransactionController@offline')->name('admin/transaction/offline');
        Route::get('create','TransactionController@create')->name('admin/transaction/create');
        Route::post('form','TransactionController@form')->name('admin/transaction/form');
        Route::get('getProductByWhId/{id}','TransactionController@getProductByWhId')->name('admin/transaction/getProductByWhId');
        Route::get('detail/{id?}','TransactionController@detail')->name('admin/transaction/detail');
        Route::post('confirm','TransactionController@confirmPayment')->name('admin/transaction/confirm');
        Route::post('filter','TransactionController@filter')->name('admin/transaction/filter');
        Route::get('invoice/{id?}','TransactionController@invoice')->name('admin/transaction/invoice');
        Route::get('label/{id?}','TransactionController@label')->name('admin/transaction/label');
        Route::get('reminder_email','TransactionController@reminder_email')->name('admin/transaction/reminder_email');
        Route::get('cancellation_email','TransactionController@cancellation_email')->name('admin/transaction/cancellation_email');
        Route::get('confirmation_email','TransactionController@confirmation_email')->name('admin/transaction/confirmation_email');
        Route::get('offline_detail/{id}','TransactionController@detailOffline')->name('admin/transaction/offline_detail');
        Route::get('offline_history/{id}','TransactionController@historyOffline')->name('admin/transaction/offline_history');
        Route::post('converterStock', 'TransactionController@converterStock')->name('admin/transaction/converterStock');
        Route::post('tracking','TransactionController@getTrackingHistory')->name('admin/transaction/tracking');
        Route::get('export','TransactionController@export')->name('admin/transaction/export');
        Route::post('import','PaymentController@import')->name('admin/transaction/import');
    });
    Route::prefix('report')->group(function(){
        Route::get('/','ReportController@index')->name('admin/report');
        Route::get('payment','ReportController@payment')->name('admin/report/payment');
        Route::get('inventory','ReportController@inventory')->name('admin/report/inventory');
        Route::get('sales','ReportController@sales')->name('admin/report/sales');
        Route::get('ecommerce','ReportController@ecommerce')->name('admin/report/ecommerce');
        Route::post('reckon','ReportController@form')->name('admin/report/reckon');
        Route::post('get_total_ecommerce_report','ReportController@get_total_ecommerce_report')->name('admin/report/get_total_ecommerce_report');
    });
    Route::prefix('package')->group(function(){
        Route::get('/','PackageController@index')->name('admin/package');
        Route::get('form','PackageController@form')->name('admin/package/form');
    });
    Route::prefix('arrival')->group(function(){
        Route::get('/','ArrivalController@index')->name('admin/arrival');
        Route::get('form/{id?}','ArrivalController@confirm')->name('admin/arrival/form');
    });
    Route::prefix('qualityControl')->group(function(){
        Route::get('/','OrderController@quality_control')->name('admin/quality_control');
        Route::post('/','OrderController@qualityControl')->name('admin/qualityControl');
    });
    Route::prefix('picking')->group(function(){
        Route::get('/','PickingController@index')->name('admin/picking');
        Route::get('notification/{id?}','PickingController@pickingNotification')->name('admin/picking/notification');
        Route::get('form/{id?}','PickingController@form')->name('admin/picking/form');
        Route::get('label/multiple/{id?}','PickingController@multiple_label')->name('admin/picking/label/multiple');
        Route::post('requestPickup','PickingController@requestPickup')->name('admin/picking/requestPickup');
        Route::get('acceptOrder','PickingController@acceptTokpedOrder')->name('admin/picking/acceptOrder');
    });
    Route::prefix('packing')->group(function(){
        Route::get('/','PackingController@index')->name('admin/packing');
        Route::get('form/{id?}','PackingController@form')->name('admin/packing/form');
        Route::get('label/{id?}','PackingController@label')->name('admin/packing/label');
        Route::post('submit_detail','PackingController@submitPackingDetail')->name('admin/packing/submit_detail');
        Route::post('printInvoice','PackingController@printInvoice')->name('admin/packing/printInvoice');
        Route::post('printLabel','PackingController@printLabel')->name('admin/packing/printLabel');
        Route::post('reject','PackingController@reject')->name('admin/packing/reject');
        Route::get('invoice/multiple/{id?}','PackingController@multiple_invoice')->name('admin/packing/invoice/multiple');
    });
    Route::prefix('order')->group(function(){
        Route::post('getOrderHistory','OrderController@getOrderHistory')->name('admin/order/history');
        Route::post('takeOrder','OrderController@takeOrder')->name('admin/order/takeOrder');
        Route::post('setPicker','OrderController@setPicker')->name('admin/order/setPicker');
        Route::post('submitPicking','OrderController@submitPicking')->name('admin/order/submitPicking');
        Route::post('pickUpOrder','OrderController@pickUpOrder')->name('admin/order/pickUpOrder');
        Route::post('qualityControl','OrderController@qualityControl')->name('admin/order/qualityControl');
        Route::post('packing','OrderController@packing')->name('admin/order/packing');
        Route::get('processing_email','OrderController@processing_email')->name('admin/order/processing_email');
    });
    Route::prefix('shipment')->group(function(){
        Route::get('/','ShipmentController@index')->name('admin/shipment');
        Route::prefix('history')->group(function(){
            Route::get('/','ShipmentController@history')->name('admin/shipment/history');
            Route::get('label/{id?}','ShipmentController@label')->name('admin/shipment/label');
            Route::get('list/{id?}','ShipmentController@history_list')->name('admin/shipment/history/list');
            Route::get('form/{id?}','ShipmentController@history_form')->name('admin/shipment/history/form');
        });
        Route::get('create','ShipmentController@create')->name('admin/shipment/create');
        Route::get('edit/{id?}','ShipmentController@edit')->name('admin/shipment/edit');
        Route::get('activate/{id?}','ShipmentController@activate')->name('admin/shipment/activate');
        Route::get('deactivate/{id?}','ShipmentController@deactivate')->name('admin/shipment/deactivate');
        Route::post('form','ShipmentController@form')->name('admin/shipment/form');
        Route::post('send','ShipmentController@sendOrder')->name('admin/shipment/send');
        Route::post('confirm','ShipmentController@confirmOrder')->name('admin/shipment/confirm');
        Route::get('email','ShipmentController@email')->name('admin/shipment/email');
        Route::get('arrival_email','ShipmentController@arrival_email')->name('admin/shipment/arrival_email');
        Route::get('arrival_confirmation_email','ShipmentController@arrival_confirmation_email')->name('admin/shipment/arrival_confirmation_email');
    });
    Route::prefix('complaint')->group(function(){
        Route::get('/','ComplaintController@index')->name('admin/complaint');
        Route::get('notification/{id?}','ComplaintController@complaintNotification')->name('admin/complaint/notification');
        Route::get('detail/{id?}','ComplaintController@detail')->name('admin/complaint/detail');
        Route::get('email','ComplaintController@email')->name('admin/complaint/email');
        Route::get('another_approval_method','ComplaintController@another_approval_method')->name('admin/complaint/another_approval_method');
        Route::get('processing_email','ComplaintController@processing_email')->name('admin/complaint/processing_email');
        Route::get('rejection_email','ComplaintController@rejection_email')->name('admin/complaint/rejection_email');
        Route::get('confirmation_email','ComplaintController@confirmation_email')->name('admin/complaint/confirmation_email');
        Route::post('process','ComplaintController@process')->name('admin/complaint/process');
        Route::post('submit','ComplaintController@submit')->name('admin/complaint/submit');
        Route::post('getUnreadResponse','ComplaintController@getUnreadResponse')->name('admin/complaint/getUnreadResponse');
    });
    Route::prefix('voucher')->group(function(){
        Route::get('/','VoucherController@index')->name('admin/voucher');
        Route::get('create','VoucherController@create')->name('admin/voucher/create');
        Route::get('edit/{id?}','VoucherController@edit')->name('admin/voucher/edit');
        Route::prefix('history')->group(function(){
            Route::get('/','VoucherController@history')->name('admin/voucher/history');
            Route::get('detail/{id?}','VoucherController@history_detail')->name('admin/voucher/history/detail');
        });
        Route::get('activate/{id?}','VoucherController@activate')->name('admin/voucher/activate');
        Route::get('deactivate/{id?}','VoucherController@deactivate')->name('admin/voucher/deactivate');
        Route::post('form','VoucherController@form')->name('admin/voucher/form');
    });
    Route::prefix('inventory')->group(function(){
        Route::get('/','InventoryController@index')->name('admin/inventory');
        Route::get('create','InventoryController@create')->name('admin/inventory/create');
        Route::get('edit/{id}','InventoryController@edit')->name('admin/inventory/edit');
        Route::get('history/{id}','InventoryController@history')->name('admin/inventory/history');
        Route::get('historyDetail/{id}','InventoryController@historyDetail')->name('admin/inventory/historyDetail');
        Route::post('form','InventoryController@form')->name('admin/inventory/form');
        Route::get('stock','InventoryController@stock')->name('admin/inventory/stock');
        Route::get('stock/mutation','InventoryController@mutation')->name('admin/inventory/stock/mutation');
        Route::get('stock/mutation/create','InventoryController@createStockMutation')->name('admin/inventory/stock/mutation/create');
        Route::get('stock/mutation/edit/{id}','InventoryController@editStockMutation')->name('admin/inventory/stock/mutation/edit');
        Route::get('stock/mutation/history/{id}','InventoryController@stockMutationHistory')->name('admin/inventory/stock/mutation/history');
        Route::get('stock_card','InventoryController@stock_card')->name('admin/inventory/stock/card');
        Route::post('stock/mutationForm','InventoryController@mutationForm')->name('admin/inventory/stock/mutationForm');
        Route::post('findSC','InventoryController@findSC');
        Route::get('stock_recording','InventoryController@stock_recording')->name('admin/inventory/stock/recording');
        Route::get('stock_recording/create','InventoryController@stock_recording_create')->name('admin/inventory/stock/recording/create');
        Route::get('stock_recording/edit/{id}','InventoryController@stock_recording_edit')->name('admin/inventory/stock/recording/edit');
        Route::post('stock_recording/form','InventoryController@stock_recording_form')->name('admin/inventory/stock/recording/form');
        Route::get('stock_recording/history/{id}','InventoryController@stock_recording_history')->name('admin/inventory/stock/recording/history');
        Route::get('stock_recording/historydetail/{id}','InventoryController@stock_recording_history_detail')->name('admin/inventory/stock/recording/historydetail');
        Route::get('getAllStock/{wh_id}/{category_id}','InventoryController@getAllStock');
        Route::get('getAllOutOfStock/{wh_id}/{category_id}','InventoryController@getAllOutOfStock');
        Route::get('getIncomingGoods','InventoryController@getIncomingGoods');
        Route::get('getStockMutation','InventoryController@getStockMutation');
        Route::get('geStockOP','InventoryController@geStockOP');
        Route::get('getProductByWhId/{id}/{variant?}','InventoryController@getProductByWhId');
        Route::get('getProductByWhStock/{id}/{variant?}','InventoryController@getProductByWhStock');
        Route::get('getProductByWhTf/{id}/{id_to}','InventoryController@getProductByWhTf');
        Route::get('getProductByWhCatID/{id}/{cat_id}','InventoryController@getProductByWhCatID');
        Route::get('getProductByWhCatIDUseParam/{param}','InventoryController@getProductByWhCatIDUseParam');
        Route::get('getProductById/{id}','InventoryController@getProductById');
        Route::get('getProductById/{id}/{wh_id}','InventoryController@getProductById');
        Route::get('getProductByIdAndWh/{id}/{wh_id}','InventoryController@getProductByIdAndWh');
        Route::get('listProductById/{id}','InventoryController@listProductById');
        Route::get('getProductCode/{id}','InventoryController@getProductCode');
        Route::post('importIncoming', 'InventoryController@importIncoming')->name('admin/inventory/importIncoming');
        Route::post('importStokOpname', 'InventoryController@importStokOpname')->name('admin/inventory/importStokOpname');
        Route::post('importTransferStok', 'InventoryController@importTransferStok')->name('admin/inventory/importTransferStok');
        Route::post('converterStock', 'InventoryController@converterStock')->name('admin/inventory/converterStock');
        Route::prefix('stock_transfer')->group(function(){
            Route::get('/','InventoryController@stockTransfer')->name('admin/inventory/stock_transfer');
            Route::get('create','InventoryController@createStockTransfer')->name('admin/inventory/stock_transfer/create');
            Route::post('form','InventoryController@stockTransferForm')->name('admin/inventory/stock_transfer/form');
            Route::get('history','InventoryController@stockTransferHistory')->name('admin/inventory/stock_transfer/history');
            Route::get('get','InventoryController@getStockTransfer')->name('admin/inventory/stock_transfer/get');
        });
        Route::prefix('mutation_warehouse')->group(function(){
            Route::get('/','InventoryController@mutationWarehouse')->name('admin/inventory/mutationWarehouse');
            Route::get('create','InventoryController@createMutationWarehouse')->name('admin/inventory/mutation_warehouse/create');
            Route::post('form','InventoryController@mutationWarehouserForm')->name('admin/inventory/mutation_warehouse/form');
            Route::get('history','InventoryController@mutationWarehouserHistory')->name('admin/inventory/mutation_warehouse/history');
            Route::get('get','InventoryController@getWarehouseMutation')->name('admin/inventory/stock_transfer/get');
        });
        Route::post('getChildrenWarehouse','InventoryController@getChildrenWarehouse')->name('admin/inventory/getChildrenWarehouse');
        Route::post('getWarehouseInventory','InventoryController@getWarehouseInventory')->name('admin/inventory/getWarehouseInventory');
    });
    Route::post('getMappingCoverage','MappingCoverageController@getMappingCoverage')->name('admin/getMappingCoverage');
    Route::get('getProvince','MappingCoverageController@getProvince')->name('admin/getProvince');
    Route::post('getRegency','MappingCoverageController@getRegency')->name('admin/getRegency');
    Route::post('getDistrict','MappingCoverageController@getDistrict')->name('admin/getDistrict');
    Route::post('getVillage','MappingCoverageController@getVillage')->name('admin/getVillage');
    Route::prefix('warehouse')->group(function(){
        Route::get('/','WarehouseController@index')->name('admin/warehouse');
        Route::get('create','WarehouseController@create')->name('admin/warehouse/create');
        Route::get('edit/{id?}','WarehouseController@edit')->name('admin/warehouse/edit');
        Route::get('activate/{id?}','WarehouseController@activate')->name('admin/warehouse/activate');
        Route::get('deactivate/{id?}','WarehouseController@deactivate')->name('admin/warehouse/deactivate');
        Route::post('form','WarehouseController@form')->name('admin/warehouse/form');
        Route::post('get','WarehouseController@get')->name('admin/warehouse/get');
    });
    Route::prefix('price_type')->group(function(){
        Route::get('/','PriceTypeController@index')->name('admin/price_type');
        Route::get('create','PriceTypeController@create')->name('admin/price_type/create');
        Route::get('edit/{id?}','PriceTypeController@edit')->name('admin/price_type/edit');
        Route::post('form','PriceTypeController@form')->name('admin/price_type/form');
        Route::get('activate/{id?}','PriceTypeController@activate')->name('admin/price_type/activate');
        Route::get('deactivate/{id?}','PriceTypeController@deactivate')->name('admin/price_type/deactivate');
    });
    Route::prefix('organization')->group(function(){
        Route::get('/','OrganizationController@index')->name('admin/organization');
        Route::get('create','OrganizationController@create')->name('admin/organization/create');
        Route::get('edit/{id?}','OrganizationController@edit')->name('admin/organization/edit');
        Route::post('form','OrganizationController@form')->name('admin/organization/form');
        Route::get('activate/{id?}','OrganizationController@activate')->name('admin/organization/activate');
        Route::get('deactivate/{id?}','OrganizationController@deactivate')->name('admin/organization/deactivate');
    });
    Route::prefix('settings')->group(function(){
        Route::get('/','GlobalSettingsController@index')->name('admin/settings');
        Route::post('form','GlobalSettingsController@form')->name('admin/settings/form');
    });
    Route::prefix('sku')->group(function(){
        Route::get('/','ProductController@stockKeepingUnit')->name('admin/sku');
        Route::get('form/{id?}','ProductController@stockKeepingUnitForm')->name('admin/sku/form');
        Route::post('form','ProductController@submitSkuForm')->name('admin/sku/form');
        Route::get('activate/{id?}','ProductController@activateStockKeepingUnit')->name('admin/sku/activate');
        Route::get('deactivate/{id?}','ProductController@deactivateStockKeepingUnit')->name('admin/sku/deactivate');
    });
    Route::get('notification', 'DashboardController@getNotifications')->name('admin/notification');
    Route::prefix('cms')->group(function(){
        Route::get('/header','CmsController@header')->name('admin/cms/header');
        Route::get('/aboutus','CmsController@aboutus')->name('admin/cms/aboutus');
        Route::post('/header/update','CmsController@updateheader')->name('admin/cms/header/update');
        Route::post('/aboutus/update','CmsController@updateaboutus')->name('admin/cms/aboutus/update');
        Route::get('/help','CmsController@help')->name('admin/cms/help');
        Route::get('/help/create','CmsController@createHelp')->name('admin/cms/help/create');
        Route::post('/help/insert','CmsController@insertHelp')->name('admin/cms/help/form');
        Route::get('/help/edit/{help?}','CmsController@editHelp')->name('admin/cms/help/edit');
        Route::get('/help/delete/{help?}','CmsController@deleteHelp')->name('admin/cms/help/deactivate');
    });
    Route::get('privacy', 'PrivacyController@index')->name('admin/privacy');
    Route::post('privacyUpdate', 'PrivacyController@privacyUpdate')->name('admin/privacy/update');
    Route::get('term', 'TermController@index')->name('admin/term');
    Route::post('termUpdate', 'TermController@termUpdate')->name('admin/term/update');
});
Route::get('generatestockcard','CronController@generateStockCard');
