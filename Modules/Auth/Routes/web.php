<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('auth')->group(function() {
    Route::get('/', 'AuthController@index');
    Route::get('login','AuthController@login')->name('auth/login');
    Route::get('register','AuthController@register')->name('auth/register');
    Route::get('forgot_password','AuthController@forgot_password')->name('auth/forgot_password');
    Route::get('change_password','AuthController@change_password')->name('auth/change_password');
});
