@extends('home::layouts.master')

@section('title','Masuk')



@section('content')
    <div class="container">
    <div class="row no-padding">
        <div class="col-md-5 col-md-offset-4  form-padding">
                <div class="separator"></div>
                <div class="clearfix"></div>
                <form class="auth-form mx-auto">
                @csrf
                    <a href="/">
                        <img class="d-block mx-auto" src="{{ asset('img/logo-login.svg') }}" style="width:16rem" alt="Logo Yotani">
                    </a>
                    <div class="form-group">
                        <label for="usernameInput" class="f-14">Username</label>
                        <input type="text" name="phone_number" class="form-control" id="usernameInput" value="" aria-describedby="usernameInput">
                    </div>
                    <div class="form-group">
                        <label for="passwordInput" class="f-14">Kata Sandi</label>
                        <input type="password" name="password" class="form-control" id="passwordInput">
                        <div class="input-group-append transparent">
                            <span class="input-group-text " id="change-password-input-type"><i class="fa fa-eye"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <a href="{{ route('auth/forgot_password') }}" class="form-check-label font-italic" for="exampleCheck1">Lupa Kata Sandi?</a>
                    </div>
                    <div class="form-group">
                        <span>Belum punya akun? </span><a class="muli-bold f-14" href="{{ route('auth/register') }}">Daftar Sekarang</a>
                      
                    </div>
                    <button type="submit" class="btn btn-orange btn-block no-margin">Selanjutnya</button>
            </form>
        </div>
    </div>
    </div>
@endsection 