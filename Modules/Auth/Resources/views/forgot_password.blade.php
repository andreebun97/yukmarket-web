@extends('home::layouts.master')

@section('title','Lupa Kata Sandi')

@section('footer','height-auto')

@section('content')
    <div class="container height-auto">
        <div class="body-content d-flex align-items-center">
            <form class="auth-form mx-auto">
                @csrf
                <a href="/">
                    <img class="d-block mx-auto" src="{{ asset('img/logo-login.svg') }}" style="width:16rem" alt="Logo Yotani">
                </a>
                <div class="form-group max-form-control-height mx-auto">
                    <label for="usernameInput" class="f-14">Username</label>
                    <input type="text" name="phone_number" class="form-control bg-white " id="usernameInput" value="" aria-describedby="usernameInput">
                </div>
                <button type="submit" class="btn btn-primary bg-green font-weight-bold mt-3 muli-bold f-16 d-block mx-auto text-white w-100" style="min-height:51px">Selanjutnya</button>
            </form>
        </div>
    </div>
@endsection