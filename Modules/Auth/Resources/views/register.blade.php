@extends('home::layouts.master')

@section('title','Daftar')

@section('footer','height-auto')

@section('content')
    <div class="container height-auto">
        <div class="body-content d-flex align-items-center">
            <form class="auth-form mx-auto">
                @csrf
                <a href="/">
                    <img class="d-block mx-auto" src="{{ asset('img/logo-login.svg') }}" style="width:16rem" alt="Logo Yotani">
                </a>
                <div class="form-group max-form-control-height mx-auto">
                    <label for="usernameInput" class="f-14">Username</label>
                    <input type="text" name="phone_number" class="form-control bg-white " id="usernameInput" value="" aria-describedby="usernameInput">
                </div>
                <div class="form-group max-form-control-height mx-auto">
                    <label for="passwordInput" class="f-14">Kata Sandi</label>
                    <div class="input-group">
                        <input type="password" name="password" class="password-input form-control bg-white " id="passwordInput">
                        <div class="input-group-append transparent">
                            <span class="input-group-text " id="change-password-input-type"><i class="fa fa-eye"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 f-14 max-form-control-height mx-auto">
                    <span>Sudah punya akun? </span><a class="muli-bold f-14" href="{{ route('auth/login') }}">Masuk Sekarang</a>
                </div>
                <button type="submit" class="btn btn-primary bg-green font-weight-bold mt-3 muli-bold f-16 d-block mx-auto text-white w-100" style="min-height:51px">Selanjutnya</button>
            </form>
        </div>
    </div>
@endsection