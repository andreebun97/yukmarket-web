@extends('home::layouts.master')

@section('title','Ganti Kata Sandi')

@section('footer','height-auto')

@section('content')
    <div class="container height-auto">
        <div class="body-content d-flex align-items-center">
            <form class="auth-form mx-auto">
                @csrf
                <a href="/">
                    <img class="d-block mx-auto" src="{{ asset('img/logo-login.svg') }}" style="width:16rem" alt="Logo Yotani">
                </a>
                <div class="form-group max-form-control-height mx-auto">
                    <div class="input-group">
                        <input type="password" name="password" class="password-input form-control bg-white" placeholder="Kata Sandi..." id="passwordInput">
                        <div class="input-group-append transparent">
                            <span class="input-group-text " id="change-password-input-type"><i class="fa fa-eye"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group max-form-control-height mx-auto">
                    <div class="input-group">
                        <input type="password" name="password_confirmation" class="password-input form-control bg-white" placeholder="Konfirmasi Kata Sandi..." id="passwordInput">
                        <div class="input-group-append transparent">
                            <span class="input-group-text " id="change-password-input-type"><i class="fa fa-eye"></i></span>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary bg-green font-weight-bold mt-3 muli-bold f-16 d-block mx-auto text-white w-100" style="min-height:51px">Ganti Kata Sandi</button>
            </form>
        </div>
    </div>
@endsection