/* ------------------------------------------------------------------------------
 *
 *  # C3.js - bars and pies
 *
 *  Demo setup of bars, pies and chart combinations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 *  Custom
 * ---------------------------------------------------------------------------- */

$(function () {
    // Bar chart
    // ------------------------------

    // Pie  chart
    var pie_chart = c3.generate({
        bindto: '#c3-pie-chart',
        size: { height: 400 },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336', '#A71F8B', '#fff000']
        },
        data: {
            columns: result,
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return "Rp" + d3.format(",.2f")(value)
                }
            }
        },
        // legend: {
        //     show: true,
        //     position: 'inset',
        //     inset: {
        //              anchor: 'bottom',
        //              x: 10,
        //              y: 0
        //            }
        // },
        tooltip: {
            format: {
                value: function(value) {
                    return d3.format(",.2f")(value)
                }
            }
        }
    });

    // Generate chart
    // var bar_chart = c3.generate({
    //     bindto: '#c3-bar-chart',
    //     size: { height: 400 },
    //     data: {
    //         columns: [
    //             purchasedQty.split(','),
    //             purchasedPrice.split(',')
    //         ],
    //         type: 'bar'
    //     },
    //     color: {
    //         pattern: ['#75ca7c', '#ecb057']
    //     },
    //     bar: {
    //         width: {
    //             ratio: 0.5
    //         }
    //     },
    //     grid: {
    //         y: {
    //             show: true
    //         }
    //     },
    //     axis: {
    //         x: {
    //             type: 'category',
    //             categories: categoryName.split(",")
    //         },
    //         y: {
    //             tick: {
    //                 format: function (d) {
    //                     console.log(d);
    //                     return convertToCurrency(d); 
    //                 },
    //                 outer: false
    //             }
    //         }
    //     },
    //     legend: {
    //         show: true,
    //         position: 'inset',
    //         inset: {
    //                  anchor: 'top-right',
    //                  x: 10,
    //                  y: 0
    //                }
    //      }
    // });

    
});