    var cancel = 0;
    var shown = 0;
    var modal = null;
    // password hide/show
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    function showLoader(){
        $('.loader-activity-div').removeClass('none');
    }

    function convertToCurrency(number)
    {
        var converted_string = "";
        var string = String(number);
        var total = string.length;
        for (var b=0; b < string.length; b++) { 
            total -= 1;
            converted_string += string[b];
            if(total > 0 && total % 3 == 0){
                converted_string += ".";
            }
            console.log(b+": "+converted_string);
        }
        return converted_string;
    }

    function hideLoader(){
        $('.loader-activity-div').addClass('none');
    }
    

    // $('form').submit(function(){
    //     showLoader() 
    // });

    $('.loader-trigger').click(function(){
        showLoader();
    });

    $('.panel-group li:not(".dropdown"), .dropdown-menu li').click(function(){
        showLoader();
    });

    $('.card-stock').click(function(){
        showLoader();
    });
    
    //  Front end 

    // Banner Carousel
    $('.banner-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })
    // Banner Carousel
    $('.menu-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:5
            },
            1000:{
                items:10
            }
        }
    })
    $(".price-display-block").hide();
    $(".btn-add").hide();
    $(".overlay-cart").hide();
    $(".btn-buy").click(function(){
        $(".price-display-block").show();
        $(".price-display").hide();
        $(".btn-buy").hide();
        $(".btn-add").show();
        $(".overlay-cart").show();
    });
    // Sticky Menu
    $(window).scroll(function () {
        if($(window).scrollTop() > 20) {
          $("#navbar").addClass('sticky');
        } else {
          $("#navbar").removeClass('sticky');
        }
      });
    // End Sticky Menu
    
    // Multiple select box 
    $(".single-select").select2();
    $(".multi-select").select2();
    // 
    $('input, textarea').keyup(function(){
        cancel = 1;
    });

    $('select').change(function(){
        cancel = 1;
    });

    function openModal(modal_id){
        console.log(modal_id);
        console.log(shown);
        $(modal_id).modal('show');
    }

    function closeModal(modal_id){
        $(modal_id).modal('hide');
    }
    
    function redirectToPage(page){
        window.location.href = page;
    }

    function clickCancelButton(page = null){
        $('.discard_changes_button').click(function(){
            if(cancel == 1){
                openModal('#cancelPopupModal');
            }else{
                if(modal == null){
                    redirectToPage(page)
                }else{
                    $(modal).modal('hide');
                }
            }
            
        });
    }

    function exitPopup(page){
        $('.exit_modal_button').click(function(){
            if(shown == 0){
                redirectToPage(page);
            }else if(shown == 1){
                $(modal).modal('hide');
            }
        });
    }

    function currencyFormat(input){
        var format = input.value.replace(/\./g,"").replace(/[^0-9]+/g, "");
        var total = format.length;
        var currencyTemp  = "";
        for (let k = 0; k < format.length; k++) {
            total -= 1;
            currencyTemp = currencyTemp + format[k];
            if(total > 0 && total % 3 == 0){
                currencyTemp = currencyTemp + ".";
            }
        }
        input.value = currencyTemp;
        return input.value;
    }
    //  Eka add 
    // Main Side menu navigation
    $('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
        e.preventDefault();
        $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active sub-menu').children('ul' );
        if ($('.navigation-main').hasClass('navigation-accordion')) {
            $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').children('ul');
        }
    });


    // Toggle mini sidebar
    $('.sidebar-main-toggle').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('sidebar-xs');
    });
    
    // Tooltip
    $(function () {
        $("body").tooltip({
            selector: '[data-toggle="tooltip"]',
            container: 'body'
        });
    });
    $(function () {
        $('.daterange-single').daterangepicker({ 
            singleDatePicker: true,
            locale: {
                format: 'YYYY/M/DD'
            }
        });
       
    });
    $(function () {
        $("#anytime-start").AnyTime_picker({
            format: "%H:%i"
        });
          $("#anytime-end").AnyTime_picker({
            format: "%H:%i"
        });
    });