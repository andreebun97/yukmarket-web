var _url = window.location.origin;

setInterval(function(){
    getListNotif()
}, 50000);

(function($) {
    bellsInvisible = function() {
        $('#bells').removeClass('bg-warning-400')
    }
})(jQuery);

function formatDates(param) {
    var hour = param.substr(10)
    var date = new Date(param)
    var temp = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear()
    result = temp+' '+hour
    return result
}

(function($) {
    getListNotif = function() {
        $.ajax({
            type: 'get',
            url: _url+'/admin/notification',
            success: function(raw){
                var html = ""
                response = JSON.parse(raw)
                var temp = $('#temp').val()

                if (response.length != 0) {
                    for (var x = 0; x < response.length; x++) {
                        if (temp != response[x].createdDate || temp == "") {
                            var date = formatDates(response[x].createdDate)
                            var path = (response[x].type == 'new_order') ? _url+'/admin/picking/notification/'+response[x].notification_id : _url+'/admin/complaint/notification/?id='+response[x].notification_id
                            html += '<li class="media">'
                            html += '<div class="media-body">'
                            html += '<a href="'+path+'" class="media-heading"><span class="text-semibold">'+response[x].customer_name+ ' - ' +response[x].message +'</span></a>'
                            // html += '<div class="text-muted margin-b5">'+ response[x].answer +'</div>'
                            html += '<div class="clearfix"></div>'
                            html += '<span class="media-annotation">'+ date +' WIB</span>'
                            html += '<div class="clearfix"></div>'
                            html += '<div class="border-dotted"></div>'
                            html += '</div>'
                            html += '</li>'

                            $('#bells').addClass('bg-warning-400').text(response.length)
                        } else {
                            break
                        }
                    }

                    $('#temp').val(response[0].createdDate)
                    $('#listNotif').append(html)
                }
            }
        });
    }
})(jQuery);